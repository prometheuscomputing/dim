#!/usr/bin/env ruby
require_relative 'load_path_manager'
if ARGV[0] =~ /test|dev|local/i
  ARGV.shift
  LoadpathManager.amend_load_path
end
LoadpathManager.display_paths

require 'common/color_output'

application_module = Foundation.setup_app('dim', __FILE__)
# require 'sequel_specific_associations'
# require 'sequel_change_tracker'
# require 'dim_generated'
# require 'dim_generated/model/driver'
# require 'dim/model_extensions'
#
require 'gui_site/launcher'
require 'gui_director'
require 'html_gui_builder'
Gui.run(application_module, :server => :setup_only)
require 'gui_site/server_config'

# load 'scripts/sync.rb' if MyDevice::PCDProfile.count == 0
ChangeTracker.delete_all_history
# migrate_all_attr_usage_data
# ChangeTracker.start

def loadme
  this_dir = File.expand_path(File.dirname __FILE__)
  # Can't load ruby_extensions twice.  Results in stack level too deep...
  # load File.join(this_dir, "../lib/dim/ruby_extensions.rb") # should be before any extensions on any class that is a Sequel::Model
  load File.join(this_dir, "../lib/dim/sequel_extensions.rb")
  load File.join(this_dir, "../lib/dim/hacks.rb") # ideally this file is empty.  It exists in order to isolate things that really need to be done differently
  load File.join(this_dir, "../lib/dim/profile_extensions.rb")
  load File.join(this_dir, "../lib/dim/dim_extensions.rb")
  load File.join(this_dir, "../lib/dim/asn1_extensions.rb")
  load File.join(this_dir, "../lib/dim/nomenclature_extensions.rb")
  load File.join(this_dir, "../lib/dim/metainfo_extensions.rb")
  load File.join(this_dir, "../lib/dim/json_getters.rb")
end

