# Foundation load_path_management can not be revectored to a development version, 
# becasue it's code is used to do the redirection (to reduce redundancy)
require 'Foundation/load_path_management'
require 'common/color_output'
module LoadpathManager
  def self.amend_load_path
    # Put gui_site on load path
    $:.unshift relative('../../gui_site/lib')
    # # # Put local repos for gui_director and html_gui_builder on load path
    $:.unshift relative('../../gui_director/lib')
    $:.unshift relative('../../gui_widgets/lib')
    $:.unshift relative('../../html_gui_builder/lib')
    # Uncomment these lines to use local SSA/SCT versions
    $:.unshift relative('../../sequel_specific_associations/lib')
    # $:.unshift relative('../../sequel_change_tracker/lib')
    $:.unshift relative('../../json_graph/lib')
    # $:.unshift relative('../../common/lib')
    $:.unshift relative('../../model_metadata/lib')
    $:.unshift relative('../../uml_metamodel/lib')
    $:.unshift relative('../../csv_tools/lib')
    # $:.unshift relative('../../alternate_inheritance/lib')
  end
  def self.display_paths
    puts_magenta "\nProjects running from development directory:"
    found = $:.select do |path|
      project = /(?<=projects\/).+/.match(path)
      puts_yellow "    #{project.to_s.gsub(/\/lib.*/, '')}" if project
      project
    end
    puts_green "    none" if found.empty?
    puts
  end
end