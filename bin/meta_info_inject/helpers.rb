module DIM_MetaInfoInject
  # This is shared by the :attributes, :associations, :enumerations, and :literals imports
  def self.tag_extraction
    {
      :attribute_id  => [:prometheus_tags, :attribute_id],
      :name          => [:name],
      :type          => [:type],
      :description   => [:prometheus_tags, :description],
      :multiplicity  => [:multiplicity],
      :bidirectional => [:bidirectional],
      :decoration    => [:decoration],
      :update_type   => [:prometheus_tags, :update_type],
      :dim_qualifier => [:prometheus_tags, :dim_qualifier],
      :optionality   => [:prometheus_tags, :to_optionality],
      # :groups        => [:prometheus_tags, :parse_groups],
      :containing_classifier => [:containing_classifier]
    }
  end
  
  # This is shared by the :attributes and the :associations imports
  # We would check the dim_qualifier without removing it, if the dim_qualifier was included in MetaInfo::Property.
  # This is bad because it really isn't the job of this to get rid of :dim_qualifier.  This should ONLY do a test! TODO
  def self.has_dim_qualifier
    lambda {|data|
      q = data.delete(:dim_qualifier) 
      # puts "Qualifer was: #{q.inspect}"
      # nil != q
      true
    } 
  end
end