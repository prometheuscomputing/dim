# copied from plugin.csvGen
AssocCSV_COLUMNS = [
  :has_mirror,
  :assoc_id,       # Comes from Property#getID
  :assoc_owner_id,
  :prometheus_tags,# result of inspecting get_tag_values_hash(:Prometheus)
  :direction,      # Comes from Property#direction_AB
  :a_classifier,   # Comes from containing_classifier.getQualifiedName
  :a_rolename,     # Comes from mirror.role_name
  :a_decoration,   # Comes from Property#decoration
  :a_multiplicity, # Comes from mirror.mult.to_s
  :b_classifier, :b_rolename, :b_decoration, :b_multiplicity  # These are similar to the :A_ versions, except for adding (or removing) 'mirror'
]
module DIM_MetaInfoInject
  def self.import_associations
    imp = Importer.new(:associations, MetaInfo::Property)
    imp.expected_headers = AssocCSV_COLUMNS
    imp.mapping_chain << {
      # :attribute_id => [:attribute_id, :fix],  # Use the 'attribute_id' tag (not reliably present) instead of the very long MagicDraw unique identifier string (getID)
      :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],  # The CSV currently doesn't contain any tags for associations. Maybe plugin.csvGen is looking in the wrong place. Or maybe we never created tags on associations. (MF -- no, we should have tags on associations...) 
      :name => [:b_rolename, :fix],
      :containing_classifier => [:a_classifier, :path_to_classifier],
      :type => [:b_classifier, :path_to_classifier],
      :multiplicity =>   [:b_multiplicity, :fix],
      :bidirectional => [:direction, :bidirectional?],
      :decoration => [:a_decoration, :to_decoration]
    }
    imp.mapping_chain << tag_extraction
    imp.test = has_dim_qualifier
  end
end