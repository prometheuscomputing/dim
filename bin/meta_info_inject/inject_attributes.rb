# copied from plugin.csvGen
AttribCSV_COLUMNS = [
  :attrib_id,             # Comes from Property#getID
  :prometheus_tags,       # result of inspecting get_tag_values_hash(:Prometheus)
  :containing_classifier, # Comes from containing_classifier.getQualifiedName
  :attribute_type,   
  :name,                  # Comes from mirror.role_name
  :multiplicity           # Comes from mirror.mult.to_s
]
module DIM_MetaInfoInject
  def self.import_attributes
    imp = Importer.new(:attributes, MetaInfo::Property)
    imp.expected_headers = AttribCSV_COLUMNS
    imp.mapping_chain << {
      # :attribute_id => [:attribute_id, :fix],    # Use the 'attribute_id' tag (not reliably present) instead of the very long MagicDraw unique identifier string (getID)
      :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],  # Keys are "description", "attribute_id", "dim_qualifier", "dim_groups", "update_type"
      :name => [:name, :fix],
      :containing_classifier => [:containing_classifier, :path_to_classifier],
      :type => [:attribute_type, :path_to_classifier_for_attr],
      :multiplicity =>   [:multiplicity, :fix],
      :bidirectional => false,
      :decoration => 'composition', # Maybe we should add a third option, such as 'attribute'
    }
    imp.mapping_chain << tag_extraction
    imp.test = has_dim_qualifier
  end
end