# copied from plugin.csvGen
ClassCSV_COLUMNS = [
  :classifier_id,
  :prometheus_tags,         # result of inspecting get_tag_values_hash(:Prometheus)
  :class,                   # A package path
  :super_classes,           # a "|" separated list of superclass package paths
  :interfaces_implemented,  # a "|" separated list of interface package paths
  :documentation,
  :is_abstract
]
module DIM_MetaInfoInject
  def self.import_classes
    imp = Importer.new(:classes, :instance)
    imp.expected_headers = ClassCSV_COLUMNS
    imp.mapping_chain << {
      # :classifier_id => [:classifier_id, :fix],  # Not from a tag. This was not previously populated: don't recall what was intended. Currently using very long MagicDraw unique identifier string (getID)
      :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],  # Keys are "pkg", "pkg_section", "clause_number", "clause_title", "name_binding", "registered_as", "description"
      :instance => [:class, :path_to_class],
      # :instance => [:class, :path_to_class_or_asn1_model],
    ##  :super_classifer => [:super_classes, :first_paths_to_classes], # Pipe separated
      :implemented_interfaces => [:interfaces_implemented, :paths_to_interfaces], # Pipe separated
      :description =>   [:documentation, :to_RichText],
      :is_abstract =>   [:is_abstract, :to_boolean]
    }
    imp.mapping_chain << {
      :instance =>  [:instance],
      :implemented_interfaces => [:implemented_interfaces],
      :description =>  [:description],
      :name_binding =>  [:prometheus_tags, :name_binding],
      :registered_as => [:prometheus_tags, :registered_as],
      :is_abstract =>   [:is_abstract]
    }
  end
end