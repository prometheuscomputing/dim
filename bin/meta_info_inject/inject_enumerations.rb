# copied from plugin.csvGen
EnumerationCSV_COLUMNS = [
  # In addition to being headers, these are keys in the hashes stored in AssocCSV_Builder@classifier_hashes
  # These hashes are computed in AssocCSV_Builder#
  :classifier_id,
  :prometheus_tags, # result of inspecting get_tag_values_hash(:Prometheus)
  :enumeration,           # A package path
  :super_enumerations,   # a "|" separated list of superclass package paths
  :literals,        # a "|" separated list of strings that are enumeration literals
  :documentation,
  :is_abstract
]
module DIM_MetaInfoInject
  def self.import_enumerations
    imp = Importer.new(:enumerations, :instance)
    imp.expected_headers = EnumerationCSV_COLUMNS
    imp.mapping_chain << {
      :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],  # Keys are "pkg", "pkg_section", "clause_number", "clause_title", "name_binding", "registered_as", "description"
      :instance => [:enumeration, :path_to_enumeration],
      :description =>   [:documentation, :to_RichText]
    }
    imp.mapping_chain << {
      :instance =>  [:instance],
      :asn1_type => [:prometheus_tags, :asn_type],
      :constraint => [:prometheus_tags, :constraint]
    }
  end
end