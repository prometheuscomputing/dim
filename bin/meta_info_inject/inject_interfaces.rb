# copied from plugin.csvGen
InterfaceCSV_COLUMNS = [
  :classifier_id,
  :prometheus_tags,     # result of inspecting get_tag_values_hash(:Prometheus)
  :interface,           # A package path
  :super_interfaces,    # a "|" separated list of superclass package paths
  :realizing_classes    # a "|" separated list of interface package paths
]
module DIM_MetaInfoInject
  def self.import_interfaces
    imp = Importer.new(:interfaces, :instance)
    imp.expected_headers = InterfaceCSV_COLUMNS
    imp.mapping_chain << {
      # :classifier_id => [:classifier_id, :fix],    # Not from a tag. This was not previously populated: don't recall what was intended. Currently using very long MagicDraw unique identifier string (getID).
      # :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],   # Keys are 'asn_choice_labels'. Not clear where to put that.
      :instance => [:interface, :path_to_interface],
    ##  :super_classifer => [:super_interfaces, :first_paths_to_interfaces], # Pipe separated
      :implementing_classes => [:realizing_classes, :paths_to_classes]     # Pipe separated
    }
  end
end