# copied from plugin.csvGen
LiteralCSV_COLUMNS = [
  :classifier_id,
  :enumeration, # A package path
  :literal, #string value of the literal
  :prometheus_tags, # result of inspecting get_tag_values_hash(:Prometheus)
  :documentation
]
module DIM_MetaInfoInject
  def self.import_literals
    imp = Importer.new(:literals, MetaInfo::Literal)
    imp.expected_headers = LiteralCSV_COLUMNS
    imp.mapping_chain << {
      :prometheus_tags =>  [:prometheus_tags, :to_stereotype_tags],
      :name => [:literal],
      :enumeration => [:enumeration, :path_to_classifier]
    }
    imp.mapping_chain << {
      :name =>  [:name],
      :code => [:prometheus_tags, :code],
      :description => [:prometheus_tags, :description],
      :enumeration => [:enumeration]
    }
  end
end