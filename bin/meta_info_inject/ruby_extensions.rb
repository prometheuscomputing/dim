# ==============================================================================================
# Define some methods used in mappings


class Array
  # Sent to an array of package names, ensures that they from a nested hierarchy, with the first entry at the top. Returns the last MetaInfo::Package on the path.
  # The argument only for diagnostic purposes
  def path_to_package(path_string)
    raise "path_to_package was applied to an empty Array during evaluationn of #{path_string.inspect}" if empty?
    container = inject(nil) {|pkg, step|
      next_pkg = nil
      if pkg
        kid = pkg.child_packages.detect {|kid| step==kid.name }
        next_pkg = kid || MetaInfo::Package.instantiate({:name => step, :parent_package => pkg}) # #instantiate is like #create, but args can include associations
      else
        attrs = {:name => step, :parent_package_id => nil}
        next_pkg = MetaInfo::Package[attrs] || MetaInfo::Package.create(attrs)
      end
      raise "Expected next_pkg, for step #{step.inspect} in path #{path_string.inspect} to return a MetaInfo::Package, but instead we have #{next_pkg.inspect}" unless next_pkg.instance_of?(MetaInfo::Package)
      next_pkg
      }
    raise "Expected path_to_package, applied to #{self.inspect} from path #{path_string.inspect} to return a MetaInfo::Package, but instead it found #{container.inspect}" unless container.instance_of?(MetaInfo::Package)
    container
  end
  
end

# {"pkg"=>["Common_Data_Types"], "pkg_section"=>["0"], "clause_number"=>["7.2"], "clause_title"=>["Top Object"], "name_binding"=>["-"], "registered_as"=>["MDC_MOC_TOP"], "description"=>["The Top object is the common inheritance base for all objects in the DIM."]}
class String
  def to_unqualified_name; split('::').last; end
  def to_stereotype_tags
    result = instance_eval(self)
    raise "Got a #{result.class}, but expected to get a Hash from #{self.inspect}" unless Hash==result.class
    result
  end
  # Ensures that the package path is present. Attempts to return the classifeir at the end of the path.
  # Creates the clasifier at the end of the path, if it does not already exist, and a klass is provided.
  def _path_to_classifier(classifier)
    steps = fix.split('::')
    classifier_name = steps.pop
    # puts "classifier_name: #{classifier_name}"
    container = steps.path_to_package(self) 
    # puts "container: #{container.inspect}"
    answer = container.classifiers.detect do |cls| 
      # puts "    #{cls.name}"
      classifier_name==cls.name
    end
    # puts "answer1: #{answer.inspect}"
    answer = classifier.instantiate({:name => classifier_name, :containing_package => container}) if classifier && !answer  # #instantiate is like #create, but args can include associations
    # puts "answer2: #{answer.inspect}"
    answer
  end
  # A few classifiers (such as "Boolean") are not specified by a path with a containing package.
  # Without a containing package to look inside of, we must know a class to search for.
  def _unpathed_classifier(classifier)
    classifier = MetaInfo::Class unless classifier
    attrs = {:name => self, :containing_package_id => nil}
    classifier[attrs] || classifier.create(attrs)
  end
  def path_to_classifier(classifier=nil)
    clsfr = nil
    ChangeTracker.commit { 
      clsfr = index('::') ? _path_to_classifier(classifier) : _unpathed_classifier(classifier)
    }
    clsfr
  end
  
  def path_to_classifier_for_attr(klass=nil)
    # klass = self if klass.nil? && index('::')
    cls = path_to_classifier(klass)
    # puts "****Path to classifier started with #{klass} and returned a class named #{cls.inspect} -- self is #{inspect}" unless klass == "MetaInfo::Class"
    cls
  end
  
  def paths_to_classifiers(classifier); split('|').collect {|path| path.path_to_classifier(classifier) }; end
  def path_to_class; path_to_classifier(MetaInfo::Class); end
  def path_to_asn1model; path_to_classifier(MetaInfo::ASN1::ASN1Model); end
  def path_to_interface; path_to_classifier(MetaInfo::Interface); end
  def path_to_enumeration; path_to_classifier(MetaInfo::Enumeration); end
  def paths_to_classes; paths_to_classifiers(MetaInfo::Class); end
  def paths_to_interfaces; paths_to_classifiers(MetaInfo::Interface); end
  def paths_to_enumearations; paths_to_classifiers(MetaInfo::Enumeration); end
  def path_to_class_or_asn1_model
    if self =~ /ASN/
      path_to_asn1model
    else
      paths_to_class
    end
  end
  def first_paths_to_classes; paths_to_classifiers(MetaInfo::Class).first; end         # CSV has multiple supers, but MetaInfo model only allows one (want to instantiate others anyway)
  def first_paths_to_interfaces; paths_to_classifiers(MetaInfo::Interface).first; end  # CSV has multiple supers, but MetaInfo model only allows one (want to instantiate others anyway)
  
  def bidirectional?; 'bidirectional'==self; end
  def to_decoration
    return self if ['composition', 'aggregation'].member?(self)
    nil
  end
  def to_boolean; 'true'==downcase; end
end

class Hash
  # MagicDraw treats all tags as arrays. The CSVs contain an inspection of a tags hash, so the values are arrays.
  # We typically use only one value.
  def first_tag_value(name)
    values = self[name] # This really is an Array: the Hash has be interpreted
    return nil unless values
    values.first
  end
  
  def asn_type; first_tag_value('asn_type'); end
  def code; first_tag_value('code'); end
  def constraint; first_tag_value('constraint'); end
  def name_binding; first_tag_value('name_binding'); end
  def registered_as; first_tag_value('registered_as'); end
  def attribute_id; first_tag_value('attribute_id'); end
  def dim_qualifier; first_tag_value('dim_qualifier'); end
=begin
  def parse_groups
    # FIXME I don't think we can do all we need to do here within the context of this silly importer thing.  This will need to be broken out into a separate script.
    
    # "dim_groups"=>["MDC_ATTR_GRP_VMO_STATIC__VMO, MDC_ATTR_GRP_VMO_STATIC__Alert, MDC_ATTR_GRP_VMO_STATIC__PM-Store, MDC_ATTR_GRP_VMO_STATIC__Metric, MDC_ATTR_GRP_VMO_STATIC__Channel, MDC_ATTR_GRP_VMO_STATIC__VMD, MDC_ATTR_GRP_VMO_STATIC__SCO, MDC_ATTR_GRP_VMO_STATIC__Alert_Monitor, MDC_ATTR_GRP_VMO_STATIC__Alert_Status, MDC_ATTR_GRP_VMO_STATIC__Complex_Metric, MDC_ATTR_GRP_VMO_STATIC__Enumeration, MDC_ATTR_GRP_VMO_STATIC__Sample_Array, MDC_ATTR_GRP_VMO_STATIC__Numeric, MDC_ATTR_GRP_VMO_STATIC__Distribution_Sample_Array, MDC_ATTR_GRP_VMO_STATIC__Time_Sample_Array, MDC_ATTR_GRP_VMO_STATIC__Real_Time_Sample_Array"]
    entry = first_tag_value('dim_groups')
    return [] unless entry
    groups_strings = entry.split(",").collect{|x| x.strip}
    attribute_groups = []
    groups_strings.each do |groups_string|
      ref_id, klass = groups_string.split("__")
      klass.gsub!(/-|_/,'')
      klass.strip!
      ref_id.strip!
      klass_metainfo_instance = MetaInfo::Class.where(Sequel.ilike(:name, klass)).first
      puts "Failed to find #{klass} while building groups" unless klass
      existing_groups = klass_metainfo_instance.attribute_groups
      existing_group = existing_groups.find{|g| g.attribute_group_id == ref_id}
      unless existing_group
        existing_group = MetaInfo::Group.new(:attribute_group_id => ref_id)
        klass_metainfo_instance.add_attribute_group existing_group
        klass_metainfo_instance.save
      end
      attribute_groups << existing_group
    end
    attribute_groups
  end
=end
  def to_optionality
    return nil unless dim_qualifier
    q = dim_qualifier.downcase.strip
    return nil if q == "n/a"
    qual = dim_qualifier.downcase.strip[0]
    case qual
    when "m"
      ret = "mandatory (11073)"
    when "o"
      ret = "optional"
    when "c"
      ret = "conditional"
    else
      ret = nil
      puts "dim qualifier had an unexpected value : #{dim_qualifier}"
    end
    # puts "qualifier was #{qual}. returning #{ret}"
    ret
  end
  def description
    fragments = self['description']
    return fragments.join.to_RichText if fragments
    nil
  end
  def update_type
    first_tag_value('update_type')#||'Dynamic, observational'
    # Most do not have this set. Looks like all the ones that do set it use value 'Static'. RANT -- We can't just assume that it is "Dynamic, observational" if the value is unset.  For one thing this affects all of the non-ASN1 properties and that just won't do.
  end
end
