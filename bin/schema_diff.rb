require 'pp'
require 'sequel/extensions/inflector'
require File.expand_path '~/projects/dim/scripts/store_metadata'
# This code is run under the assumption that the models corresponding to the target database are loaded!
@diff = nil # loading this file resets @diff

def model_for table
  Sequel::Model.descendants.each{|klass| return klass if klass.table_name == table}
  # puts "Did not find Sequel::Model for table :#{table}" unless table =~ /_deleted$/
  nil
end

def infer_type_from_column table, column
  if found_property = infer_property_from_column(table, column)
    found_property[:class]
  end
end

def schema_type_for_col db, table, col
  schema = db.schema(table)
  raise "Did not find schema for #{table}" unless schema
  col_schema = schema.find{|entry| entry[0] == col}
  unless col_schema
    schema_cols = schema.collect{|col_array| col_array[0]}
    raise "Did not find column #{col} in the #{table} schema: #{schema_cols}"
  end
  col_schema[1][:type]
end
  

def infer_property_from_column table, column
  if m = model_for(table)
    property = column.to_s.gsub(/_id|_class/, "").to_sym
    found_property = m.properties[property] || m.properties[property.to_s.pluralize.to_sym]
  end
end

def infer_mappings cols1, cols2, table
  klass = model_for table
  map = {}
  puts "Lets make a map for #{klass}:"
  # since the naming scheme has changed we need to handle that and be able to map back to the respective places from whence they come.
  orig_cols1 = cols1
  orig_cols2 = cols2
  desnaked_cols1 = []
  desnaked_cols2 = []
  cols1_desnake_to_orig = {}
  cols2_desnake_to_orig = {}
  orig_cols1.each do |col|
    desnaked = col.to_s.gsub("_","").to_sym
    cols1_desnake_to_orig[desnaked] = col
    desnaked_cols1 = desnaked
  end
  orig_cols2.each do |col|
    desnaked = col.to_s.gsub("_","").to_sym
    cols2_desnake_to_orig[desnaked] = col
    desnaked_cols2 = desnaked
  end
  
  # There are a few possibilities that we'll cover here.
  # 1. The naming convention for getters has changed.  TODO We may have to handle this earlier.
  
  # 2. It is an enumeration that was once a String and now is an assoc to the enum type
  new_enum_getters = klass.properties.select{|k,v| v[:enumeration]}.collect{|k,v| v[:getter]}
  directly_mappable_enum_getters = orig_cols1.select{|col| new_enum_getters.include? col}
  puts "Directly mappable enum getters: #{directly_mappable_enum_getters}"
  directly_mappable_enum_getters.each do |g|
    # add them to the mapping
    map[g] = {:enumeration => g}
    # remove these from the pool because they are done now
    orig_cols1.delete g
    orig_cols2.reject!{|col| col.to_s == (g.to_s + "_id") || col.to_s == (g.to_s + "_class")}
  end
    
  # 3. It was an assoc to a primitive and is now a column that is the Sequel type that is the supertype of said primitive.
  # TODO seperate based on whether _class is present or not.  must check value for class (meaning check all values ahead of time if there is class, otherwise keep it like it is.)
  possible_prims = orig_cols1.select{|col| orig_cols2.include?(col.to_s.gsub(/_id|_class/, "").to_sym)}
  prims_hash = Hash.new(0)
  possible_prims.each{|prim| prims_hash[prim.to_s.gsub(/_id|_class/, "").to_sym] += 1}
  poly_prims = prims_hash.select{|k,v| v > 1}.keys
  other_prims = prims_hash.select{|k,v| v == 1}.keys
  
  # TODO make this less sloppy
  # 3.a.  is not polymorphic
  puts "Possible simple primitives: #{other_prims}" if other_prims.any?
  other_prims.each do |col|
    id_column = (col.to_s + "_id").to_sym
    # puts "#{col}: #{schema_type_for_col $source, table, id_column}"
    if old_table_metadata = @old_properties[table]
      old_property_metadata = old_table_metadata[col]
      if old_property_metadata
        # old_property_metadata = old_property_metadata.reject{|k,v| k == :methods}
        old_table = old_property_metadata[:class].demodulize.downcase.pluralize.to_sym
        if old_table
          # check to see if this table really exists in the old db.  if it doesn't we indicate with nil
          old_table = nil unless $source.table_exists? old_table
          # puts "#{col}: #{old_table}"
          map[col.to_s.gsub(/_id|_class/, "").to_sym] = {:lookup => :simple, :table => old_table, :id_col => id_column}
          orig_cols1.delete id_column
          orig_cols2.delete col
        else
          puts "#{col}: NOT FOUND"
          map[col] = {}
        end
      end
    else
      puts "Could not find #{table} in #{@old_properties.keys.sort}"
    end
  end
    
  # 3.b. It is polymorphic
  puts "Possible polymorphic primitives: #{poly_prims}" if poly_prims.any?
  poly_prims.each do |col|
    # find all classes that fulfilled this property type
    class_column = (col.to_s + "_class").to_sym
    id_column = (col.to_s + "_id").to_sym
    types = $source[table].all.collect{|r| r[class_column]}.compact.uniq
    # puts "#{table}.#{class_column} had types: #{types}"
    type_map = {}
    types.each do |type|
      type_table = @old_properties.find{|k,v| v[:class] == type}.first
      type_map[type] = type_table
    end
    map[col.to_s.gsub(/_id|_class/, "").to_sym] = {:lookup => :polymorphic, :id_col => id_column, :class_column => class_column, :tables => type_map}
    orig_cols1.delete class_column
    orig_cols1.delete id_column
    orig_cols2.delete col
  end

  # 4. It is a to-many association to a primitive now.  Maybe we won't have any of these.
  # Ignore for now
  pp map
  pp "remaining cols1: #{orig_cols1}" if cols1.any?
  pp "remaining cols2: #{orig_cols2}" if cols2.any?
  puts "_____________________________"
end

def schema_hash db
  schema = {}
  db.tables.each do |table|
    schema[table] = convert_table_schema_to_hash(db.schema(table))
  end
  schema
end

# schema is an array of arrays
def convert_table_schema_to_hash table_schema
  return {} unless table_schema
  ret = {}
  table_schema.each{|a| ret[a[0]] = a[1]}
  ret
end

def subtract_arrays a1, a2, map = {}
  ret = a1.dup
  a1.each_with_index do |e,i|
    # the old database's table has been given a new name in the new database 
    if replacement = map[e]
      ret[i] = nil if a2.include? replacement
    else
      ret[i] = nil if a2.include? e
    end
  end
  ret.compact
end

def intersect_tables tables1, tables2, map = {}
  ret = []
  tables1.each_with_index do |t,i|
    # the old database's table has been given a new name in the new database 
    if replacement = map[t]
      ret << t if tables2.include? replacement
    else
      ret << t if tables2.include? t
    end
  end
  ret
end

def map_table table, map
  if replacement = map[table]
    replacement
  else
    table
  end
end

# at this point, ignore columns is a global thing so shouldn't really come into play.  ignore columns should evolve into both a global array and a per table hash
def table_schemas_identical? table_name, table1_schema, table2_schema, ignore_columns, column_map
  remapped_table1_schema = {}
  if cmt = column_map[table_name]
    # no ignored columns should really remap here because there is little point in recording a remapping for a globally ignored column
    remapped_ignore_columns = ignore_columns.collect{|c| cmt[c] || c}
    table1_schema.each do |k,v|
      if cmt[k]
        remapped_table1_schema[cmt[k]] = v
      else
        remapped_table1_schema[k] = v
      end
    end
    remapped_table1_schema.reject{|k,v| remapped_ignore_columns.include? k} == table2_schema.reject{|k,v| remapped_ignore_columns.include? k}
  else
    table1_schema.reject{|k,v| ignore_columns.include? k} == table2_schema.reject{|k,v| ignore_columns.include? k}
  end
end

def remap(array, map = {})
  ret = array.dup
  array.each_with_index do |e,i|
    if map[e]
      ret[i] = map[e]
    else
      ret[i] = e
    end
  end
end

def reverse_remap(array, map = {})
  ret = array.dup
  vals = map.values
  array.each_with_index do |e,i|
    if values.include? e
      if mapping = map.find{|k,v| v == e}
        ret[i] = mapping[0]
      end
    else
      ret[i] = e
    end
  end
end

def diff_schemas db1, db2, options = {}
  return @diff if @diff
  ignore_columns = options[:ignore_columns] || []
  ignore_tables  = options[:ignore_tables] || []
  table_map  = options[:manual_mapping][:tables] # assumes not nil
  column_map = options[:manual_mapping][:columns] # assumes not nil
  answer = options # in order to keep a record of the input options
  answer[:db1] = db1.inspect
  answer[:db2] = db2.inspect
  answer[:tables_with_differences] = {}
  answer[:identical_tables] = []
  db1_schema = schema_hash db1
  db2_schema = schema_hash db2
  tables_only_in_db1 = subtract_arrays(db1.tables, db2.tables, table_map)
  answer[:tables_only_in_db1] = tables_only_in_db1 - ignore_tables
  tables_only_in_db2 = db2.tables - remap(db1.tables, table_map)
  answer[:tables_only_in_db2] = tables_only_in_db2 - ignore_tables
  common_tables = intersect_tables(db1.tables, db2.tables, table_map) - ignore_tables
  common_tables.each do |table|
    # puts "----------processing common table #{table}"
    table1_schema = db1_schema[table]
    table2_schema = db2_schema[map_table(table, table_map)]
    schema_identical = table_schemas_identical?(table, table1_schema, table1_schema, ignore_columns, column_map)
    if schema_identical
      answer[:identical_tables] << table
    else
      answer[:tables_with_differences][table] ||= {}
    
      t1_uniq_columns = table1_schema.keys - reverse_remap(table2_schema.keys, column_map) - ignore_columns
      t2_uniq_columns = table2_schema.keys - remap(table1_schema.keys, column_map) - remap(ignore_columns, column_map)
      # FIXME starting here
      shared_columns = (table1_schema.keys & reverse_remap(table2_schema.keys, column_map)) - ignore_columns
  
      if t1_uniq_columns.any?
        answer[:tables_with_differences][table][:db1_unique_columns] = t1_uniq_columns
      end
      if t2_uniq_columns.any?
        answer[:tables_with_differences][table][:db2_unique_columns] = t2_uniq_columns
      end
      if shared_columns.any?
        answer[:tables_with_differences][table][:columns_in_common] = shared_columns
        shared_columns.each do |col|
          if table1_schema[col] == table2_schema[col]
            answer[:tables_with_differences][table][:columns_with_common_schemas] ||= []
            answer[:tables_with_differences][table][:columns_with_common_schemas] << col
          else         
            answer[:tables_with_differences][table][:columns_with_different_schemas] ||= []
            answer[:tables_with_differences][table][:columns_with_different_schemas] << col
            answer[:tables_with_differences][table][col] = {}
            # record differences in schema for each column
            c1_uniq_schema_keys = table1_schema[col].keys - table2_schema[col].keys
            c2_uniq_schema_keys = table2_schema[col].keys - table1_schema[col].keys
            # answer[:tables_with_differences][table][col][:common_schema] = c1_uniq_schema_keys.empty? && c2_uniq_schema_keys.empty?
            answer[:tables_with_differences][table][col][:db1_schema_unique_keys] = c1_uniq_schema_keys if c1_uniq_schema_keys.any?
            answer[:tables_with_differences][table][col][:db2_schema_unique_keys] = c2_uniq_schema_keys if c2_uniq_schema_keys.any?
            # These keys are the same between both schemas.  Now check the values for these keys.
            common_column_schema_keys = table1_schema[col].keys | table2_schema[col].keys
            common_column_schema_keys.each do |column_schema_key|
              if table1_schema[col][column_schema_key] == table2_schema[col][column_schema_key]
                # Not sure this is the best way to express that there was no difference....
                # TODO do we need to know this at all?
                answer[:tables_with_differences][table][col][column_schema_key] = {:db1 => :db2}
              else
                answer[:tables_with_differences][table][col][column_schema_key] = {:db1 => table1_schema[col][column_schema_key], :db2 =>  table2_schema[col][column_schema_key]}
              end
            end
          end
        end
      else
        answer[:tables_with_differences][table][:columns_in_common] = []
      end
    end
  end
  @diff = answer
end

# Assumes that differences in '_deleted' tables are not meaningful
# Assumes that db2 is backing Sequel::Models
def meaningful_differences db1, db2, options = {}
  diff = diff_schemas db1, db2, options
  
  db1_uniq_tables_with_values = []
  diff[:tables_only_in_db1].each{|table| db1_uniq_tables_with_values << table if db1[table].any? }
  db2_uniq_tables_with_values = []
  diff[:tables_only_in_db2].each{|table| db2_uniq_tables_with_values << table if db2[table].any? }
  
  tables_with_populated_columns = {}
  # maybe this should be segregated based on which db (db1, db2, or both) has populated values here.
  differing_tables_with_values = diff[:tables_with_differences].select{|table, hash| db1[table].any? || db2[table].any? }
  differing_tables_with_values.each do |table, columns_info|
    db1_info = columns_info[:db1_unique_columns] || [] # empty array so #each won't fail
    db1_info.each do |column|
      # we won't worry about this column unless there is a row in db1 that has data in that column
      if has_data = db1[table].exclude(column => nil).count > 0
        tables_with_populated_columns[table] ||= {}
        tables_with_populated_columns[table][:db1_unique_columns] ||= []
        # this is a column that we will need to migrate
        tables_with_populated_columns[table][:db1_unique_columns] << column
      end
    end
  end
  mapping = {}
  tables_with_populated_columns.each do |table, columns_info| # columns_info is presently just :db1_columns from above
    # puts "processing #{table}"
    # pp columns_info
    db1_uniq_column_info = columns_info[:db1_unique_columns] || []
    # going back to the input to get the list of db2 columns for this that we do not yet have a mapping for
    db2_uniq_column_info = diff[:tables_with_differences][table][:db2_unique_columns] || []
    infer_mappings db1_uniq_column_info, db2_uniq_column_info, table
    
    # add new key to columns_info
    columns_info[:db1_column_mapping] = {}
    db1_uniq_column_info.each do |column|
      corresponding_property_in_new_model = infer_property_from_column table, column
      corresponding_property_in_new_model = infer_property_from_column table, column
      prop = corresponding_property_in_new_model.reject{|k,v| [:methods, :singular_opp_getter, :opp_getter, :as, :documentation, :additional_data].include? k} if corresponding_property_in_new_model
      columns_info[:db1_column_mapping][column] = prop || "NOT FOUND"
    end
    # columns_info.delete :db1_columns
  end
  
  answer = {:db1_uniq_tables_with_values => db1_uniq_tables_with_values, :db2_uniq_tables_with_values => db2_uniq_tables_with_values, :tables_with_populated_columns => tables_with_populated_columns}
  answer
end

def migrate_tables_that_are_the_same db1, db2, options = {}
  table_map = options[:manual_mapping][:tables]
  answer = diff_schemas db1, db2, options
  answer[:identical_tables].each do |table|
    rows = db1[table].all
    if rows.any?
      puts "Simple migration #{table}"
      db1[table].all.each do |row|
        options[:ignore_columns].each{|col| row.delete col}
        db2[map_table(table, table_map)].insert row
      end
    end
  end
end

def migrate_tables_with_meaningful_differences db1, db2, options = {}
  answer = meaningful_differences db1, db2, options
  # Handle tables that are only in db1.  Requires a mapping.
  puts "These tables are found only in the source database: #{answer[:db1_uniq_tables_with_values]}"
  puts
  # Do we need to handle tables that are only in db2?  Requires a mapping.
  
  # Handle tables with differences
  answer[:tables_with_populated_columns].each do |table, column_info|
    puts "Migrating #{table} to #{map_table(table, options[:manual_mapping][:tables])}"
    pp column_info
    # putsa 
    # get the klass that maps to this table (if we can)
    klass = model_for db2[map_table(table, options[:manual_mapping])]
    # do the migration
    db1[table].all.each do |row|
      row = row.reject{|k,v| options[:ignore_columns].include? k}
      puts "  #{row}"
      # easily take care of identical columns
      if column_info[:columns_with_common_schemas]
        identical_columns = row.select{|col| column_info[:columns_with_common_schemas].include? col}
        puts "    inserting #{identical_columns}"
        db2[map_table(table, options[:manual_mapping][:tables])].insert identical_columns
      end
      # get model for newly created column
      instance = klass[row[:id]]
      # attempt to handle differing columns
      if column_info[:db1_unique_columns]
        column_info[:db1_unique_columns].each do |col|
          if instance.respond_to?(col) # then this is probably an enumeration
            puts "    Handling #{klass}.#{col} -- #{row[col]}"
            setter = (col.to_s + "=").to_sym
            klass.send(setter, row[col])
            klass.save
          else
            puts "  #{klass} doesn't know what to do with #{col} with value #{row[col]}"
          end
        end
      end
    end
  end
end

source_db = File.expand_path("~/Prometheus/data/dim_generated/bSQL.db")
$source = Sequel.sqlite(source_db)
$target = DB

def setup_options
  ignored_columns = [:created_at, :updated_at, :asn1model_id, :asn1model_class]
  ignored_tables = $source.tables.select{|t| t.to_s =~ /_deleted$/} + $target.tables.select{|t| t.to_s =~ /_deleted$/}
  ignored_tables += $target.tables.select{|k,v| m = model_for(k); m ? m.to_s =~ /ChangeTracker/ : false}
  ignored_tables += $target.tables.select{|k,v| m = model_for(k); m && m.respond_to?(:enumeration?) ? m.enumeration? : false}
  nomenclature_tables = Nomenclature.classes(:no_imports => true).collect{|c| c.table_name}
  ignored_tables += nomenclature_tables
  
  options = {:ignore_tables => ignored_tables.uniq, :ignore_columns => ignored_columns, :manual_mapping => manual_mapping}
end

# Table name changes are entered into the :tables hash as {old_name => new_name}.  Column name changes are entered into the columns hash where the old table name is the key and the mapping between columns is the value, e.g. {old_table_name => {old_column_name => new_column_name}}
def manual_mapping
  {
    :tables  => {},
    :columns => {
      :dev_alarm_entries => {:object_id => :obj_id}
    }
  }
end

def go
  answer = meaningful_differences $source, $target, setup_options
  # pp answer[:tables_with_populated_columns]
  pp answer
  nil
end

def migrate
  @diff = nil
  opts = setup_options
  migrate_tables_that_are_the_same $source, $target, opts;nil
  migrate_tables_with_meaningful_differences $source, $target, opts;nil
end
  
def populated_tables db, options
  (db.tables - options[:ignore_tables]).select{|table| db[table].count > 1}
end
 

# populated_tables $target, setup_options

# answer[:identical_tables].select{|table| db[table].count > 1}
# pre = DB.tables.select{|t| populated = DB[t].count > 0; puts t if populated; populated};nil
# post = DB.tables.select{|t| populated = DB[t].count > 0; puts t if populated; populated};nil

# diff_schemas $source, $target, setup_options
