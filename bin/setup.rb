raise 'we are not sure that setup.rb is correct.  have a look at it'
require 'ramaze'
require 'dim_generated'
require 'fileutils'
require 'Foundation/load_path_management'

# Put dim folder on load path so gui_director can find extensions and custom_spec
$:.unshift relative('.')
# Put dim on load path
$:.unshift relative('../lib')
# Put local repos for gui_director, gui_site, and html_gui_builder on load path
$:.unshift relative('../../gui_site/lib')
$:.unshift relative('../../gui_director/lib')
$:.unshift relative('../../html_gui_builder/lib')

WEB_ROOT = relative '../lib/dim/'

# Set up gui_builder_options since we're not using the launcher (Capybara needs a rackup file)
$gui_builder_options = {
                        # :license_agreement_path => relative('../lib/gui_site/view/gb_users/default_license_agreement.haml'),
                        :spec_short_title => 'Device Editor',
                        # :spec_url => 'https://carexample.prometheuscomputing.com',
                        :object_history => true,
                        :clone => true,
                        :color_coding => true,
                        :spec_title => 'Device Editor',
                        :spec_name => 'dim_generated.rb',
                        :spec_module => 'Dim_Generated',
                        :custom_spec => 'custom_spec.rb',
                        :model_extensions => 'model_extensions.rb',
                        :login => true,
                        :web_authentication => false,
                        :local_authentication => true,
                        :ssl => false,
                        :secure_cookie => false,
                        :multivocabulary => true,
                        :log_dir => File.expand_path('~/Prometheus/data/dim_generated/logs/'),
                        :data_dir => File.expand_path('~/Prometheus/data/dim_generated/'),
                        :tempfile_dir => File.expand_path('~/Prometheus/data/dim_generated/tmp/'),
                        :custom_pages => { },
                        # :custom_page_root => relative('pages'),
                        :mode => :dev}
$suppress_output = true

module Dim
  extend App_MetaInfo_Behaviors
  DEFAULT_APP_OPTIONS = $gui_builder_options
end
Foundation.setup_app('dim', __FILE__)

# TODO: make sure this line is right
$app, options = Rack::Builder.parse_file(relative('../lib/gui_site/config.ru'))