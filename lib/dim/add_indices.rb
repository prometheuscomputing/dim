# Warning: this script should not be considered bullet-proof, and you should *absolutely* back up your database before running it!!!

db = Sequel::Model.db

# migrate markup enumerations
require 'common/constants'
tlms = DimGenerated.top_level_modules.collect{|tlm| tlm.to_const }
add_indicies_errors = []
all_models = tlms.collect{|mod| mod.classes(:ignore_class_namespaces => true)}.flatten.select{|c| c.ancestors.include?(Sequel::Model)}
all_models.each do |model|
  model.properties.each do |name, property|
    next if model.interface? || model.abstract?
    next unless property[:association]
    next unless property[:type] == :many_to_one
    index_name = (model.table_name.to_s + '_' + (property[:column] || property[:getter]).to_s).to_sym
    next if db.indexes(model.table_name).keys.include?(index_name)
    id_col = (property[:column] || property[:getter]).to_s + '_id'
    class_col = (property[:column] || property[:getter]).to_s + '_class'
    indexed_cols = [id_col]
    indexed_cols << class_col if model.columns.include?(class_col)
    begin
      db.add_index(model.table_name, indexed_cols, :name => index_name)
      puts "Added #{property[:getter]} index to #{model}"
    rescue Exception => e
      add_indicies_errors << e.message
    end
  end
end
puts add_indicies_errors
