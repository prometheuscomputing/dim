class IEEE20101::ASN1::INTEGER
  include JSON_Graph::Serializeable
end
class IEEE20101::ASN1::OCTET_STRING
  include JSON_Graph::Serializeable
end

IEEE20101.classes(:ignore_class_namespaces => true).each do |klass|
  # klass.send(:include, ASN1Extensions)
  next unless klass < Sequel::Model
  
  # patch
  klass.class_eval do 
    def parent_profile
      parent = composer
      # parent = pending_composer unless parent # TODO this works but apparently we shouldn't have to do this.  #composer should handle pending associations.  Will investigate SSA.
      if parent
        if parent.is_a? MyDevice::PCDProfile
          return parent
        else
          return parent.parent_profile
        end
      else
        puts "#{self} had no composer.  How strange...."
        nil
      end
    end
    
    derived_attribute(:profile_summary, ::String)
    def profile_summary
      if pprof = parent_profile
        pprof.summary_for_children
      else
        "Summary should be displayed after saving this object.  In the near future it will be displayed without needed to save this new object."
      end
    end
    
    def display_name
      # note: #getter_used_by_composer is pretty nasty as of Jan21, 2015
      # g = getter_used_by_composer.to_s.strip.split(/ |_/).collect{|p| p.capitalize}.join
      c = self.class.name.demodulize
      # "#{g} - #{c}"
    end
  end
end

# getter and setter hooks don't seem to be inherited so we are handling them like this
asn1s = DIM.classes(:ignore_class_namespaces => true).select{|k| k.to_s =~ /ASN1/ && (k < Sequel::Model)}
# asn1s = []
asn1s.each do |klass|
  klass.class_eval do
    include DeviceClassifierBehavior
  end
  
  is_primitive_wrapper = klass.additional_data && klass.additional_data[:modeled_as] && klass.additional_data[:modeled_as] == :primitive_wrapper
  if is_primitive_wrapper
    # puts "primitive_wrapper: skipping #{klass}  -- #{klass.additional_data.inspect}\n"
    next
  end
  # setup hooks for proxied attributes of type OID-Type and TYPE
  proxied_properties = klass.properties.select{|k,v| v[:additional_data] && v[:additional_data][:prometheus] && v[:additional_data][:prometheus]["proxy_property"]  && v[:additional_data][:modeled_as] != :attribute_reciprocal}
  proxied_properties.each do |property_key, property_info|

    proxied_property_getter = property_info[:getter]
    proxy_getter            = property_info[:additional_data][:prometheus]["proxy_property"].first.gsub("-", "_").to_sym
    # add more direct path to proxy_getter in metainfo
    klass.properties[property_key][:proxy_getter] = proxy_getter
    
    attr_type               = (property_info[:wraps] || property_info[:class])&.to_const
    
    if attr_type <= DIM::CommonDataTypes::ASN1::OID_Type
      if property_info[:association] # i.e. we are handling a list of OID types
        proxied_property_adder   = (proxied_property_getter.to_s + "_add").to_sym
        proxied_property_remover = (proxied_property_getter.to_s + "_remove").to_sym
        klass.send(:add_association_adder, proxy_getter) do |term|
          # puts "sending #{self.class}.#{proxied_property_adder}(#{term.term_code.inspect})"
          self.send(proxied_property_adder, term.term_code)
          term
        end
        klass.send(:add_association_remover, proxy_getter) do |term|
          # puts "sending #{self.class}.#{proxied_property_remover}(#{term.term_code.inspect})"
          self.send(proxied_property_remover, term.term_code)
          term
        end
        # puts "Added assoc_adder to #{klass} -- proxy_getter for #{proxied_property_getter} is #{proxy_getter}\n  proxied_property_adder is #{proxied_property_adder}\n  proxied_property_remover is #{proxied_property_remover}\n#{property_info.reject{|k,v| k == :methods}.inspect}\n\n"
      else
        proxied_property_setter = (proxied_property_getter.to_s + "=").to_sym
        # puts "Adding assoc_adder to #{klass} -- proxy_getter for #{proxied_property_getter} is #{proxy_getter} -- proxied_property_setter is #{proxied_property_setter}\n#{property_info.reject{|k,v| k == :methods}.inspect}\n\n"
        klass.send(:add_association_adder, proxy_getter) do |term|
          self.send(proxied_property_setter, term.term_code)
          term
        end
        klass.send(:add_association_remover, proxy_getter) do |term|
          self.send(proxied_property_setter, nil)
          term
        end
      end
    else
      # raise "Class #{klass} had a proxy: #{proxy_getter}, for a property: #{proxied_property_getter}, that had unexpected type: #{property_info.reject{|k,v| k == :methods}.inspect}"
    end
  end
end
