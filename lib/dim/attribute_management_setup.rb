require 'json'
MetaInfo::Class.each do |klass|
  next if klass.is_asn1? || klass.is_abstract
  # #########################################
  # FIXME every model should be here.  Should just need to regenerate from UML to get them into the generated code
  model = klass.ruby_model rescue nil
  next unless model
  # #########################################
  props = klass.all_properties.select{|p| p.is_asn1?}
  model_props = model.properties
  available_attributes = {}
  props.each do |p| 
    ut = p.update_type ? p.update_type.value : nil 
    o  = p.optionality ? p.optionality.value : nil 
    available_attributes[p.name] = {"optionality" => o, "update_type" => ut}
    
    # Now find the corresponding property in the ssa properties hash
    model_prop = model_props.find{|k,v| v[:getter].to_s == p.name.downcase.gsub('-', '_')}
    if p.name == "Class" && model_prop.nil?
      model_prop = model_props.find{|k,v| v[:getter].to_s == "class_dim"}
    end
    if p.name == "Version" && model_prop.nil?
      model_prop = model_props.find{|k,v| v[:getter].to_s == "version_dim"}
    end
    if model_prop
      model_props[model_prop.first][:attribute_label] = p.name
      available_attributes[p.name]["getter"] = model_prop.first
    else
      puts_yellow "Could not find property matching #{model} #{p.name} (#{p.name.downcase.gsub('-', '_')}) amongst:"
      puts "  #{model_props.collect{|k,v| v[:getter].to_s}.sort}"
      puts
    end
  end
  mandatory_attributes = available_attributes.select{|k, v| v["optionality"].to_s =~ /mandatory/}
  
  asn1_getters = model_props.select{|k,v| v[:class].to_s =~ /ASN/}.collect{|k,v| v[:getter]}
  # FIXME the commented out stuff here actually put this stuff on the class and not on the instances so that was a big FAIL.  Let's try and accomplish this a different way somewhere else...
  model.instance_eval <<-codestring, __FILE__, __LINE__
    @available_attributes = available_attributes
  #   @mandatory_attributes = mandatory_attributes
  #   @used_attributes      = mandatory_attributes
    @asn1_getters         = asn1_getters
    def available_attributes; @available_attributes; end
    def available_attributes= value; @available_attributes = value; end
  #   def used_attributes; @used_attributes; end
  #   def used_attributes= value; @used_attributes = value; end
  #   def mandatory_attributes; @mandatory_attributes; end
  #   def mandatory_attributes= value; @mandatory_attributes = value; end
    def asn1_getters; @asn1_getters; end
  codestring
end

def migrate_attr_usage_data(obj)
  uas = {}
  obj.used_attributes.each do |attr_inclusion|
    p = attr_inclusion.referenced_property
    ut = p.update_type ? p.update_type.value : nil
    o  = p.optionality ? p.optionality.value : nil
    uas[p.name] = {"optionality" => o, "update_type" => ut}
  end
  ChangeTracker.start
  obj.available_attribute_list = obj.class.available_attributes.to_json.to_s
  obj.set_used_attributes
  obj.set_mandatory_attributes
  obj.save
  # puts obj.mandatory_attribute_list.inspect
  ChangeTracker.commit
  # obj.refresh
  # puts obj.mandatory_attribute_list.inspect
  # puts
end
def migrate_all_attr_usage_data
  MyDevice::INSTANTIABLE_DIM_CLASSES.each do |klass|
    klass.all.each do |obj|
      migrate_attr_usage_data(obj)
    end
  end
end

