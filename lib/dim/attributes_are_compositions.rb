DIM.classes(:no_imports => true).each do |klass|
  klass.properties.each do |key, info|
    if info[:additional_data]
      if info[:additional_data][:modeled_as] == :attribute
        # enumerations are inherently shared
        info[:composition] = true unless info[:enumeration]
      end
    end
  end
end