klasses = DIM.classes(:no_imports => true)
klasses.each do |klass|
  klass.properties.each do |property_key, property_info|
    property_type = property_info[:class].to_const
    next unless property_type.to_s =~ /::ASN1::/
    types_included = klass.type_of(property_key)
    if types_included.count > 1
      if property_type < Sequel::Model
        # genera = nil
        # genera = ' -- abstract'  if property_type.abstract?
        # genera = ' -- interface' if property_type.interface?
        # puts "#{klass}##{property_info[:getter]}(#{property_type})#{genera} --> #{types_included.inspect}\n\n"
        next if property_type.abstract?
        next if property_type.interface?
        next if property_type.enumeration?
        # puts "Constraining #{klass}##{property_info[:getter]}(#{property_type}) --> #{types_included.inspect}"
        type_constraint :on_type => klass, :on_property => property_key, :valid_types => [property_type]
      end
    end
  end
end
# type_constraint :on_type => :"CommonDataTypes::ASN1::ExtObjRelationEntry", :on_property => :relation_attributes, :valid_types => [:"CommonDataTypes::ASN1::AttributeList"]

# A Test of multiplicity constraints
# multiplicity_constraint(:Channel, :metric, :max => 3, :min => 1)


# _____________________________________________________________ Examples from Ceramics Project....
# set_scope :Workflow

# example of value_constraint usage
# test_filter = lambda {|value|
#   !(value =~ /monkey/)
# }
# value_constraint :on_type => :WorkflowItem, :on_property => :cas_number, &test_filter

# set_scope :Bibliography
#
# prefill :on_type => :CitationContainee, :on_property => :language, :containing_association => :contained_by
#
# # Constraints on what types various citations can contain
# type_constraint :on_type => :Journal, :on_property => :contains, :valid_types => [:JournalIssue, :JournalArticle, :ContentItem, :ProceedingsIssue, :UndeterminedPublication] + [:BookArticle, :ConferencePaper, :ReportPaper]
