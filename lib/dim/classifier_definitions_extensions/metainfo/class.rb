module MetaInfo
  class Class    
    def to_asn1_plaintext(opts = {})
      return nil unless qualified_name  =~ /ASN1/
      return nil if qualified_name  =~ /IEEE20101/
      lines = []
      if description&.safe_content
        lines << PlainText.asn1_data_type_comment(self)
      end
      apn = asn1_parent_name
      if apn == 'SEQUENCE OF'
        rm     = ruby_model
        ad     = rm.additional_data if rm
        pr     = ad[:prometheus] if ad
        answer = pr["sequence_of"] if pr
        answer = answer.first if answer && answer.any?
        puts "#{self.qualified_name} is a SEQUENCE OF what, exactly?" unless answer
        answer ||= "FIXME"
        lines << "#{name} ::= SEQUENCE OF #{answer}"
        lines.join("\n")
      else
        first_line = "#{name} ::= #{asn1_parent_name}"
        first_line << " {" if properties_count > 0    
        first_line << " #{constraint}" if constraint && !constraint.strip.empty?
        lines << first_line
        pd = plaintext_asn1_property_definitions
        lines << pd if pd
        lines << "}" if properties_count > 0
        lines.join("\n")
      end
    end
    
    def plaintext_asn1_property_definitions
      props = properties.reject { |p| p.bidirectional }
      return nil if props.empty?
      longest_name_property = props.first
      props.each { |p| longest_name_property = p if p.name.length > longest_name_property.name.length }
      longest_property_effective_size = PlainText.attribute_indent + longest_name_property.name.length + PlainText.min_separation
      # name_tabs = (longest_property_effective_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      longest_type_property = props.first
      props.each do |prop|
        next if prop.type.name =~ /ANY.DEFINED.BY/
        prop_type_length = prop.type.name.length
        prop_type_length += "[#{prop.context_specific_tag}] ".length if prop.context_specific_tag
        longest_type_property_length = longest_type_property.type.name.length
        longest_type_property_length += "[#{longest_type_property.context_specific_tag}] ".length if longest_type_property.context_specific_tag
        longest_type_property = prop if prop_type_length > longest_type_property_length
      end
      longest_type_effective_size = longest_type_property.type.name.length + PlainText.min_separation
      longest_type_effective_size += "[#{longest_type_property.context_specific_tag}] ".length if longest_type_property.context_specific_tag
      previous_identifier = nil
      lines = []

      props.each_with_index do |p, i|
        # previous identifier is necessary in order to properly include it when the type of an attribute is ANY DEFINED BY
        last_property = (i +1) == props.count
        definition_lines, previous_identifier = p.plaintext_asn1_definition(previous_identifier, last_property, longest_property_effective_size, longest_type_effective_size)
        lines += definition_lines
      end
      lines.join("\n")
    end  
  end # class Class
end