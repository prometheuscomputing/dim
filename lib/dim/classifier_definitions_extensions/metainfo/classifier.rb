module MetaInfo
  class Classifier
    # def to_definition(opts = {})
    #   raise "#to_definition must be implemented in concrete class"
    # end
    def to_definition(opts = {})
      return to_asn1_plaintext if is_asn1
      to_pseudocode
    end        
    
    def to_pseudocode
      output = []
      declaration = "#{self.class.name.demodulize} #{name}"
      if (sc = super_classifier)
        declaration << " specializes #{sc.name}"
      end
      declaration << " {"
      output << declaration
      asn1_props, dim_props = properties.partition { |p| p.is_asn1 }
      last_prop = asn1_props.last
      last_prop ||= dim_props.last # if there were no asn1 properties
      if dim_props.any?
        output << "  // ASSOCIATIONS:"
        dim_props.each do |p|
          p.to_pseudocode.each { |line| output << "  #{line}" }
          output << '' unless p == last_prop
        end
      end
      if asn1_props.any?
        output << "  // ATTRIBUTES:"
        asn1_props.each do |p|
          p.to_pseudocode.each { |line| output << "  #{line}" }
          output << '' unless p == last_prop
        end
      end
      output << "};"
      output
    end
  end # class Classifier
end
