module MetaInfo
  class Enumeration
    
    def to_definition
      # yields formatted plaintext
      if is_asn1
        lines = []
        if description&.safe_content
          lines << PlainText.asn1_data_type_comment(self)
        end
        lits = literals
        declaration = "#{name} ::= #{asn1_parent_name}"
        declaration << " {" if lits.any?
        lines << declaration
        if lits.any?
          longest_literal = lits.first
          # must account for the comma after each literal except the last one...
          lits.each { |lit| longest_literal = lit if (lit.name.size + (lit == lits.last ? 0 : 1)) > longest_literal.name.length }
          effective_longest_literal_length = PlainText.attribute_indent + longest_literal.name.size + PlainText.min_separation
          effective_longest_literal_length += 1 unless lits.last == longest_literal
          # puts "longest_literal is #{longest_literal.name} -- #{longest_literal.name.size}"
          # puts "effective_longest_literal_length is #{effective_longest_literal_length}"
          lits.each_with_index do |lit, i|
            definition_lines = lit.to_definition(effective_longest_literal_length, (i+1) == lits.count) # second param lets us know if this is the last one
            lines += definition_lines
          end
        end
        lines << "}" if lits.any?
        ret = lines.join("\n")
        # puts; puts ret; puts
        ret
      end
    end
    
  end # class Enumeration
end
