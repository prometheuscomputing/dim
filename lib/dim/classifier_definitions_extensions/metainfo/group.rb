module MetaInfo
  class Group
    def group_name
      if name.nil? || name.empty?
        parent_group.group_name
      else
        name
      end
    end
  end # class Group
end