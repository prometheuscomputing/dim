module MetaInfo
  class Interface
    def to_asn1_plaintext(opts = {})
      return nil unless qualified_name  =~ /ASN1/
      return nil if qualified_name  =~ /IEEE20101/
      lines = []
      if description&.safe_content
        lines << PlainText.asn1_data_type_comment(self)
      end
      apn = asn1_parent_name
      first_line = "#{name} ::= #{asn1_parent_name}"
      if implementing_classes_count > 0 
        first_line << " {" 
        lines << first_line
        implementing_classes.each do |implementer|
          line = PlainText.tab
          line << implementer.name
        end
        lines << "}"
        lines.join("\n")
      else
        first_line
      end
    end
    
    def plaintext_asn1_property_definitions
      props = properties.reject { |p| p.bidirectional }
      return nil if props.empty?
      longest_name_property = props.first
      props.each { |p| longest_name_property = p if p.name.length > longest_name_property.name.length }
      longest_property_effective_size = PlainText.attribute_indent + longest_name_property.name.length + PlainText.min_separation
      # name_tabs = (longest_property_effective_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      longest_type_property = props.first
      props.each do |p|
        next if p.type.name =~ /ANY.DEFINED.BY/
        longest_type_property = p if p.type.name.length > longest_type_property.type.name.length
      end
      longest_type_effective_size = longest_type_property.type.name.length + PlainText.min_separation
      # type_tabs = (longest_type_effective_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      previous_identifier = nil
      lines = []

      props.each_with_index do |p, i|
        # previous identifier is necessary in order to properly include it when the type of an attribute is ANY DEFINED BY
        last_property = (i +1) == props.count
        definition_lines, previous_identifier = p.plaintext_asn1_definition(previous_identifier, last_property, longest_property_effective_size, longest_type_effective_size)
        lines += definition_lines
      end
      lines.join("\n")
    end 
  end
end
