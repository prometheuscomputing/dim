module MetaInfo
  class Literal
    
    def to_definition(long_lit, is_last)
      lines = []
      line  = ''
      lit_name = "#{name}"
      lit_name << "," unless is_last
      line     << PlainText.tab + lit_name
      
      dc = description&.safe_content
      remaining_comment = nil
      if dc && !dc.empty?
        effective_literal_length = line.length + PlainText.min_separation
        # literal_tabs = (effective_literal_length.to_f / PlainText.tab.size.to_f)
        additional_space = long_lit - effective_literal_length
        line << (' ' * additional_space) + PlainText.tab
        comment, remaining_comment = PlainText.asn1_attribute_comment(dc, long_lit)
        line << comment
      end
      lines = [line]
      if remaining_comment && remaining_comment.any?
        remaining_comment.each do |rc|
          lines << (' ' * (long_lit - PlainText.tab.size)) + rc
        end
      end
      lines
    end
  end # class Literal
end