module MetaInfo
  class Package
    def to_definition(opts = {})
      to_pseudocode
    end
    
    def to_pseudocode
      return nil if qualified_name =~ /ASN1/
      output = []
      declaration = "package #{name} {"
      output << declaration
      undefined_classifiers = classifiers
      all_done_number = undefined_classifiers.count
      defined_classifiers = []
      while undefined_classifiers.any?
        start_count = undefined_classifiers.count
        undefined_classifiers.each do |uc|
          if (sc = uc.super_classifier)
            next if undefined_classifiers.include?(sc)
            unless defined_classifiers.include?(sc) || sc.name =~ /Top|VMO/
              raise "Whoa!!! #{uc.qualified_name}'s superclassifier is #{sc.qualified_name}"
            end
          end
          output << '' unless defined_classifiers.empty?
          uc.to_pseudocode.each { |line| output << "  #{line}" }
          defined_classifiers << uc
        end
        undefined_classifiers = undefined_classifiers - defined_classifiers
        raise "unable to define classifiers for package #{name} -- #{undefined_classifiers.inspect}" if undefined_classifiers.count == start_count
      end
      output << "};"
      output.join"\n"  
    end
  end # class Package
end