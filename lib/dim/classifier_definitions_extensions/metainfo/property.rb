module MetaInfo
  class Property
    def to_pseudocode
      # ["1..1", "0..1", "1..Infinity", "0..Infinity"]
      case multiplicity
      when "1..1", "1"
        mandatory = true
        plural    = false
      when "0..1"
        mandatory = false
        plural    = false
      when "1..Infinity"
        mandatory = true
        plural    = true
      when "0..Infinity"
        mandatory = false
        plural    = true
      else
        raise "Unknown multiplicity type '#{multiplicity}' for #{qualified_name}"
      end
      output = []
      d = description&.safe_content
      output << "// #{d}" if d
      declaration = "#{type.name}#{plural ? '[]' : ''} #{name.downcase}" if type
      declaration ||= "// Additional context attributes may be added. Please consult the standard." if name == 'Context Attributes'
      info = []
      info << "  attribute_id = #{attribute_id.strip.inspect};" if attribute_id
      if (opt = optionality&.value)
        opt = "mandatory" if opt =~ /mandatory/
        info << "  optionality  = #{opt.inspect};"
      end
      if (fn = footnote&.content) && name != 'Context Attributes'
        info << "  optionality_note  = #{fn.inspect};" unless fn.strip.empty?
      end
      if info.any?
        declaration << " {"
        output << declaration
        info.each { |i| output << i }
        output << "};"
      else
        output << declaration + ';'
      end
      output
    end
    
    def plaintext_asn1_definition(previous_identifier = nil, last_property, long_name, long_type)
      effective_name_size = PlainText.attribute_indent + name.length + PlainText.min_separation
      additional_chars    = long_name - effective_name_size

      line = PlainText.tab
      line << name.downcase
      line << ' ' * additional_chars
      line << PlainText.tab
      
      type_name = type.name
      type_name = type_name + "  #{previous_identifier}" if type_name == 'ANY DEFINED BY'
      type_name = "[#{context_specific_tag}] " + type_name if context_specific_tag
      type_name << "," unless last_property
      line      << type_name
      
      dc = description_content
      remaining_comment = nil
      if dc && !dc.empty?
        long_total       = long_name + long_type
        additional_chars = long_type - type_name.length
        line << PlainText.tab + (' ' * additional_chars)
        comment, remaining_comment = PlainText.asn1_attribute_comment(dc, long_total)        
        line << comment
      end
      lines = [line]
      if remaining_comment && remaining_comment.any?
        remaining_comment.each do |rc|
          lines << (' ' * long_total) + rc
        end
      end
      [lines, name]
    end
  end # class Property
end