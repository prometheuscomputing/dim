require_relative 'standard_builder/plaintext'
load relative('standard_builder/plaintext.rb')
require_relative 'standard/formatting_help/word_size'
load relative('standard/formatting_help/word_size.rb')

# Dir.glob(File.join(relative('classifier_definitions_extensions'), '**', '*.rb'), &method(:require))
Dir.glob(File.join(relative('classifier_definitions_extensions'), '**', '*.rb'), &method(:load))


def gencode(arg = nil)
  load File.expand_path(__FILE__)
  nil
end
