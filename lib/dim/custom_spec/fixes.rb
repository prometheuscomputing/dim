organizer(:Details, MyDevice::PCDProfile) { enable('parent_profile') }
organizer(:Details, MyDevice::PHDProfile) { enable('parent_profile') }

# Concrete implementations of MetaInfo::Classifier
organizer(:Details, MetaInfo::Class) { enable('super_classifier') }
organizer(:Details, MetaInfo::Interface) { enable('super_classifier') }
organizer(:Details, MetaInfo::Enumeration) { enable('super_classifier') }
organizer(:Details, MetaInfo::Primitive) { enable('super_classifier') }
organizer(:Details, MetaInfo::StructuredDataType) { enable('super_classifier') }

organizer(:Details, MetaInfo::Group) { enable('parent_group') }
organizer(:Details, MetaInfo::Package) { enable('parent_package') }

organizer(:Details, Standard::Clause) { enable('parent_clause') }

organizer(:Details, DIM::System::MultipleBedMDS) { enable('supersystem') }
organizer(:Details, DIM::System::SingleBedMDS) { enable('supersystem') }