require 'pp'
# this clears the terminal/console...at least on OS X it does.
def clear_terminal(erase_scrollback_history = nil)
  if erase_scrollback_history
    puts "\e[H\e[2J\e[3J"
  else # preserves scrollback
    puts "\e[H\e[2J"
  end
end
# clear_terminal true


# global_spec_modifier { |view, spec|
global_spec_modifier { |view|
  kontent = view.content
  view_classifier = view.data_classifier
  # this is copied from resolve_view_refs.  it normally happens after the global_spec_modifier is processed but because we need view_refs to have resolved views later in this block, we have to do it now.

# Added this when I thought we needed access to the loaded spec during global_spec_modifier
  # kontent.each do |widget|
  #   if widget.kind_of? Gui::Widgets::ViewRef
  #     widget.derive_view(view, spec)
  #   end
  # end


  # FIXME -- aggregation will change things here -> hide composers -- no need to navigate upwards. added complexity of doing so is unnecessary and potentially confusing
  composer_widgets = kontent.select do |w|
    begin
      w.data_classifier.to_s =~ /DIM::|PHD::|PCDProfile|PHDProfile/ && w.respond_to?(:composer) && w.composer == true
    rescue => e
      # puts_green view.pretty_inspect
      # puts_red w.class
      # puts_cyan w.respond_to?(:data_classifier)
      puts_yellow w.parent.pretty_inspect
      raise e
    end
  end.collect { |w| w.getter.to_s }
  view.hide(*composer_widgets, 'choice_group', 'name_in_profile')
  
  view.hide('mandatory_attribute_list', 'used_attribute_list', 'available_attribute_list')
  
  # Make sure that these widgets are HTML widgets!
  original_widgets = kontent.select { |w| w.is_a?(Gui::Widgets::Text) && view_classifier.to_s =~ /Standard::/ && w.getter != 'remarks' }
  original_widgets.each do |original_widget|
    kontent[kontent.index(original_widget)] = Gui::Widgets::Text.new(:getter => original_widget.getter, :label => original_widget.label, :language => :HTML, :always_collapsed => false)
  end
  
  # fix naming issues
  kontent.each do |w|
    lbl = w.label
    w.label = lbl.sub(/id(_| )dim/i, "ID") if lbl# =~ /id(_| )dim/i
    w.label = lbl.sub(/_?dim$/i, "") if lbl #should say _DIM
  end
  # fix typos caused by bad automatic parsing of NAMEs
  view.relabel('vmds', "VMDs")
  view.relabel('vmd_model', "VMD Model")
  view.relabel('mds_status', "MDS Status")
  view.relabel('pmstore', "PM-Stores")
  view.relabel('pm_store', "PM-Store")
  view.relabel('pm_segment', "PM-Segment")
  view.relabel('pm_stores', "PM-Stores")
  view.relabel('pm_segments', "PM-Segments")
  view.relabel('model_class', "DIM Class Information")
  
  view.hide('available_attributes') # this collection serves only to restrict what may be used in #used_attributes
  view.disable('json_source')
  view.hide('json_source') # FIXME do this only for :Summary views/
  # Prevent users from creating or deleting terms
  view.disable_deletion('allowed_units', 'allowed_enumerations')
  view.disable_creation('allowed_units', 'allowed_enumerations')

  view_is_dim  = view_classifier.to_s =~ /DIM|IEEE20101|PHD::/
  view_is_asn1 = view_classifier.to_s =~ /ASN1/
  # segregate attributes and associations into groups and order accordingly
  # I think we really only want to do this for views that map to Top objects as well so we need to take that into consideration.
  if view_is_dim && view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Details)
    # find getters that are proxied and for which their widgets will need to be hidden
    proxy_hash = {}
    proxied_properties = view_classifier.properties.select{|k,v| v[:additional_data] && v[:additional_data][:prometheus] && v[:additional_data][:prometheus]["proxy_property"]}

    proxied_getters = proxied_properties.collect{|k,v| v[:getter]} # only used later for possible error message / debug
    proxied_properties.each do |k,v|
      proxy_hash[v[:additional_data][:prometheus]["proxy_property"].first.gsub(/-/, "_")] = {:class => v[:class].demodulize.gsub("_", "-"), :proxied_getter => v[:getter]}
    end

    # proxy_getters   = proxied_properties.collect{|k,v| v[:additional_data][:prometheus]["proxy_property"]}.flatten
    # proxy_getters   = proxy_getters.collect{|g| g.gsub(/-/, "_")}
    
    asn1_kontent = []
    profile_kontent = []
    top_kontent = []
    asn1_simple_kontent = []

    widgets_to_hide = []
    kontent.map! do |contained_widget|
      # -------------------- find out the type of the property that this widget is for
      dc = contained_widget.data_classifier.to_s
      puts "dc is #{dc}" if view_classifier.to_s =~ /DevAlarmEntry/
      unless dc =~ /::/ # if it doesn't match this then it didn't resolve to something in our application namespace
        property_for_widget = view_classifier.properties[contained_widget.getter.to_sym]
        if property_for_widget
          class_for_widget = property_for_widget[:class] if property_for_widget
          dc = class_for_widget.to_s.gsub(/_/, "-") if class_for_widget
        else
          puts "#{view_classifier} had no property key for #{contained_widget.getter.inspect} but it did have #{view_classifier.properties.keys}."
          puts "    data_classifier: #{contained_widget.data_classifier}"
          puts "    label: #{contained_widget.label}"
          puts "    type: #{contained_widget.type}"
        end
      end
      classifier_for_label = dc.demodulize
      classifier_for_label = "OCTET STRING" if classifier_for_label =~ /OCTET-STRING/
      
      # -------------------- manipulate contained widgets
      profile_related_getters = ['allowed_units', 'allowed_enumerations','proposed_enums', 'proposed_type', 'proposed_units']
      case
      when dc =~ /MyDevice|MetaInfo/ || profile_related_getters.include?(contained_widget.getter.to_s)
        profile_kontent << contained_widget.getter.to_s
        
      # This is a widget that is a proxy for an OID-Type or a TYPE.  We will hide the widget for the proxied property and change the label of the widget for the property that is the proxy for the property typed as an OID-Type or TYPE.
      when proxy_hash.keys.include?(contained_widget.getter)
        # Make the label nice
        proxy_info = proxy_hash[contained_widget.getter]
        contained_widget.label.slice!(/(_|-| )Terms?$/)
        contained_widget.label.strip!
        contained_widget.label = "#{contained_widget.label} (#{proxy_info[:class]})"
        # TODO pluralize if 'Terms'
        
        proxy_getter = contained_widget.getter        
        # find the getter for which this is a proxy
        # proxied_getter = proxied_properties.select{|k,v| g = v[:additional_data][:prometheus]["proxy_property"].first; g == proxy_getter || g.gsub(/-/, "_") == proxy_getter}.collect{|k,v| v[:getter].to_sym}.first
        proxied_getter = proxy_info[:proxied_getter]
        if proxied_getter.nil?
          puts "--------------------------------------------------------------------------"
          puts "#{view_classifier} proxy_getters:   #{proxy_hash.keys}"
          puts "                   proxied_getters: #{proxied_getters}"
          puts "Proxy getter is:   #{proxy_getter}"
          puts "Proxied getter NOT FOUND"
        end
        # puts "searching for #{proxied_getter.inspect}" if debug
        property_for_widget = view_classifier.properties[proxied_getter]
        if property_for_widget
          class_for_widget = property_for_widget[:class]
          classifier_for_label = class_for_widget.to_s.gsub(/_/, "-").demodulize if class_for_widget
          widgets_to_hide << proxied_getter.to_s
          if class_for_widget =~ /TYPE/
            asn1_kontent.unshift contained_widget.getter.to_s
          else
            asn1_kontent << contained_widget.getter.to_s
          end
          view.disable_creation(contained_widget.getter.to_s)
          view.disable_deletion(contained_widget.getter.to_s)
        else
          puts "#{view_classifier} had no property key for proxied_getter #{proxied_getter.inspect} though it is proxied by #{proxy_getter}.  It did have #{view_classifier.properties.keys}"
        end
        
      when dc =~ /ASN1|IEEE20101/
        contained_widget.label = "#{contained_widget.label} (#{classifier_for_label})"
        asn1_kontent << contained_widget.getter.to_s
      
      when dc =~ /DIM|PHD::/
        top_kontent << contained_widget.getter.to_s # this should work but checking data_classifier.ancestors.include? DIM::Top::Top would perhaps be better
      else
        asn1_simple_kontent << contained_widget.getter.to_s
      end
      
      contained_widget # this MUST be the last thing in the map! block
    end

    widgets_to_hide.each{|widget_getter| view.hide widget_getter}
    profile_kontent.unshift asn1_simple_kontent.delete("cardinality")
    profile_kontent.unshift asn1_simple_kontent.delete("name_in_profile")
    profile_kontent.unshift asn1_simple_kontent.delete("notes")
    asn1_kontent = asn1_simple_kontent + asn1_kontent
    profile_kontent.push asn1_kontent.delete("model_class")
    profile_kontent.push asn1_kontent.delete("valid_terms")
    
    # TODO -- add to issue tracker -- Context Widgets don't form unique id_strings so we are giving them a label so they can be properly ordered by ViewDSL#order.  This is because id_string is formed by "#{self.class}_#{self.getter}" and the Context widgets have no getters.
    huge_font_size   = '24px'
    big_font_size    = '20px'
    medium_font_size = '16px'
    # if profile_kontent.any?
    if true
      # add xml download button to all pages
      unless view_classifier.to_s =~ /PHD/
        button = Gui::Widgets::Button.new(:action => 'download_full_xml_snippet', label: 'XML Snippet', button_text: 'Download Snippet', :save_page => true, :display_result => :file)
        view.content << button
        profile_kontent.unshift button.label
      end
      
      # add profile summary information
      style   = Gui::Widgets::Context.new(:style => {'font-size' => big_font_size, 'text-align' => 'center', 'display' => 'block', 'background-color' => 'lightblue'}, :label => 'Profile Information', :parent => view)
      summary = Gui::Widgets::Message.new(:getter => 'profile_summary', :parent => style)
      label   = Gui::Widgets::TextLabel.new(:text => 'Device Profile Information', :parent => style)
      style.content << label
      view.content  << style
      view.content  << summary
      profile_kontent.unshift summary.getter
      profile_kontent.unshift style.label
    end
    if top_kontent.any? && !view_is_asn1
      style = Gui::Widgets::Context.new(:style => {'font-size' => big_font_size}, :label => 'DIM Object Containment', :parent => view)
      label = Gui::Widgets::TextLabel.new(:text => 'Device Components', :parent => style)
      style.content << label
      view.content  << style
      top_kontent.unshift style.label
      style = Gui::Widgets::Context.new(:style => {'font-size' => huge_font_size, 'display' => 'block', 'background-color' => 'aquamarine', 'text-align' => 'center'}, :label => 'DIM Section Indicator', :parent => view)
      label = Gui::Widgets::TextLabel.new(:text => 'Everything Below This Point Is Defined By 11073:10201', :css_classes => 'dim_section_indicator', :parent => style)    
      style.content << label
      view.content  << style
      top_kontent.unshift style.label
    end
    if asn1_kontent.any? && !view_is_asn1
      instance_label       = view_classifier.metainfo_classifier ? view_classifier.metainfo_classifier.name : view_classifier.name.demodulize

      style_big    = Gui::Widgets::Context.new(:style => {'font-size' => big_font_size}, :label => 'Attributes (ASN.1)', :parent => view)
      style_medium = Gui::Widgets::Context.new(:style => {'font-size' => medium_font_size, 'color' => 'red'}, :label => 'mandatory attributes', :parent => view)
      asn1_label   = Gui::Widgets::TextLabel.new(:text => "ASN.1 Attributes For This #{instance_label} Instance", :parent => style_big)
      mandatory_attributes = Gui::Widgets::String.new(:getter => 'mandatory_attribute_list', :label => 'Mandatory Attributes', :disabled => true, :parent => style_medium)
      style_big.content    << asn1_label
      style_medium.content << mandatory_attributes
      view.content << style_big
      view.content << style_medium
      asn1_kontent.unshift style_medium.label
      asn1_kontent.unshift style_big.label
      # FIXME you badass dude.  This here is good except you have to fix the population of used_attrs so that it looks up to the parent to get ALL of the attributes, not just the ones defined on the immediate class.  Remember that you got rid of all of the attribute duplication among classes.
      # attribute_is_not_used = proc do |dim_obj, vertex|
      #   # this could make things really slow.  should we cash this collection of names? is there a different / better way to make these calculations
      #   attrs = dim_obj.used_attributes.collect{|ua| ua.name.downcase.gsub("-", "_")}.sort
      #   !attrs.include?(vertex.widget.getter.to_s)
      # end
      # asn1_kontent.each{|widget_getter| view.hide_if(attribute_is_not_used, widget_getter)}
    end
    
    view.order(*profile_kontent, *top_kontent, *asn1_kontent)
  end

  back_links_to_hide = []
  disabled_nomenclature_classes = ['Nomenclature::EnumerationItem', 'Nomenclature::UnitItem', 'Nomenclature::EnumerationGroupMember', 'Nomenclature::PrivateTerm', 'Nomenclature::Term', 'Nomenclature::Token', 'Nomenclature::UCUMUnit', 'Nomenclature::Unit', 'Nomenclature::UnitGroup', 'Nomenclature::Enumeration', 'Nomenclature::EnumerationGroup', 'Nomenclature::Literal', 'Nomenclature::Metric']
  if view_classifier.to_s =~ /Nomenclature::/
    if disabled_nomenclature_classes.include?(view_classifier.to_s) && (view.view_name == :Details)
      kontent.each do |w|
        g = w.getter.to_s
        if w.is_a?(Gui::Widgets::ViewRef)
          view.disable_creation(g)
          view.disable_addition(g)
          view.disable_deletion(g)
          view.disable_removal(g)
        else
          view.disable(g)
        end
        if g =~ /^for_/
          back_links_to_hide << g
        end
      end
      back_links_to_hide.each{|bl| view.hide(bl)}
    end
  end  
  
  # disable all fields in organizer summary widgets
  if view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Summary)
    getters = kontent.collect{|w| w.getter.to_s}
    view.disable(*getters)
  end
  
  tops = MyDevice::INSTANTIABLE_DIM_CLASSES + MyDevice::PHDTop.children
  # Add refids to the collection summaries for TOPs that respond to #type_term.  Note that MDS responds to #system_type_term but we aren't going to bother with those for now.
  if view.view_name == :Summary
    view.hide('name_in_profile')
    if tops.include?(view_classifier)
      next unless view_classifier.properties[:type_term]
      if view.respond_to?(:columns)
        view.hide('name_binding', 'handle', 'class_dim')
        view.columns.unshift({:label=>"RefID", :search_filter=>:disabled, :getter=>"type_term.reference_id", :getter_name=>"type_term.reference_id", :type=>"string"})
      else
        kontent.unshift(Gui::Widgets::String.new(:getter => 'type_term.reference_id', :label => 'RefID', :disabled => true, :search_filter => :disabled))
      end
    end
  end

  # No need to show proposed type if the object doesn't have a type
  view.hide('proposed_type') if kontent.find{|w| w&.getter&.to_sym == :type || w&.getter&.to_sym == :system_type}.nil?
  
  normative = proc{|dim_obj|  pprof = dim_obj.parent_profile; iu = pprof.intended_use if pprof; iu && iu.value =~ /Normative/}
  # view.hide_if(normative, 'allowed_units', 'allowed_enumerations')
  
  # FIXME This is for demonstration purposes only and should almost certainly be removed after demoing 
  # non_normative = proc{|dim_obj| pp = dim_obj.parent_profile; iu = pp.intended_use if pp; iu && iu.value !~ /Normative/}
  # view.hide_if(non_normative, 'attribute_specializations') if view_classifier.to_s =~ /DIM|PHD::/
  
  has_type = proc do |dim_obj|
    if dim_obj.respond_to?(:type)
      dim_obj.type_count > 0
    elsif dim_obj.respond_to?(:system_type)
      dim_obj.system_type_count > 0
    end
  end
  view.hide_if(has_type, 'proposed_type')

  view.disable_deletion('model_class')
  view.disable_creation('model_class')
  view.disable_removal('model_class')
  
  view.reorder('remark', 'remarks', :to_end => true)
}
