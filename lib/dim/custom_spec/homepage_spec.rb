class GuiBuilder::Home
  # alias_association :x73s, 'Nomenclature::RTMMSTerm', :type => :one_to_many, :alias_of => :terms, :filter => [Sequel.ilike(:sources_temp,'%x73%')]
end

#Homepage
homepage_item :'Nomenclature::UCUMUnit'
homepage_item :'Nomenclature::RTMMSTerm', :getter => 'rtms'
homepage_item :'Nomenclature::RTMMSTerm', :getter => 'hrtms'
homepage_item :'Nomenclature::RTMMSTerm', :getter => 'x73s'
homepage_item :'Nomenclature::Metric'
homepage_item :'Nomenclature::Enumeration'
homepage_item :'Nomenclature::Unit'
homepage_item :'Nomenclature::UnitGroup'
homepage_item :'Nomenclature::Literal'
homepage_item :'Nomenclature::EnumerationGroup'
homepage_item :'Nomenclature::Token'
homepage_item :'MetaInfo::Package', :getter => 'dim_pkg'
# homepage_item :'MetaInfo::Package', :getter => 'phd_pkg'
homepage_item :'MetaInfo::Classifier', :getter => 'dim_classifiers'
homepage_item :'MetaInfo::Classifier', :getter => 'asn1_classifiers'
# homepage_item :'MetaInfo::Class', :getter => 'asn1_classes'
# homepage_item :'MetaInfo::Enumeration', :getter => 'asn1_enums'
# homepage_item :'MetaInfo::Classifier', :getter => 'phd_dim_classifiers'
# homepage_item :'MetaInfo::Class', :getter => 'phd_asn1_classes'
# homepage_item :'MetaInfo::Enumeration', :getter => 'phd_asn1_enums'
# homepage_item :'MetaInfo::Interface', :getter => 'ifaces'
homepage_item :'Nomenclature::ProposedTerm', :getter => 'proposed_terms'

homepage_item :'Standard::Clause'
homepage_item :'Standard::Abbreviation'
homepage_item :'Standard::Definition'
homepage_item :'Standard::NormativeReference', :getter => 'normative_references'
homepage_item :'Standard::Table'
homepage_item :'Standard::Text'
homepage_item :'Standard::Example'
homepage_item :'Standard::Note'
homepage_item :'Standard::Code'
homepage_item :'Standard::Figure'


organizer(:Details, Home) {
  view_ref(:Rosetta, 'rtms',                    :label => 'Rosetta')
  view_ref(:hRTM, 'hrtms',                      :label => 'hRTM')
  view_ref(:x73, 'x73s',                        :label => 'x73')
  view_ref(:Summary, 'metrics',                 :label => 'Metrics')
  view_ref(:Summary, 'units',                   :label => 'Units')
  view_ref(:Summary, 'unitgroups',              :label => 'Unit Groups')
  view_ref(:Summary, 'enumerations',            :label => 'Enumerations')
  view_ref(:Summary, 'literals',                :label => 'Enumeration Literals')
  view_ref(:Summary, 'tokens',                  :label => 'Tokens')
  view_ref(:Summary, 'enumerationgroups',       :label => 'Enumeration Groups')
  view_ref(:Summary, 'ucumunits',               :label => 'UCUM Units')
  view_ref(:Summary, 'proposed_terms',          :label => 'Proposed Terms')
  view_ref(:dim_pkg, 'dim_pkg', :label => 'PoCD DIM Package')
  view_ref(:dim_classifiers, 'dim_classifiers', :label => 'DIM Classifiers')
  view_ref(:asn1_classifiers, 'asn1_classifiers',       :label => 'ASN.1 Datatypes')
  # view_ref(:asn1_classes, 'asn1_classes',       :label => 'ASN.1 Datatypes')
  # view_ref(:asn1_enums, 'asn1_enums',           :label => 'ASN.1 Enumerations')
  # view_ref(:phd_pkg, 'phd_pkg', :label => 'PHD DIM Package')
  # view_ref(:phd_dim_classifiers, 'phd_dim_classifiers', :label => 'PHD DIM Classifiers')
  # view_ref(:phd_asn1_classes, 'phd_asn1_classes',       :label => 'PHD ASN.1 Datatypes')
  # view_ref(:phd_asn1_enums, 'phd_asn1_enums',           :label => 'PHD ASN.1 Enumerations')
  # view_ref(:Summary, 'ifaces',                  :label => 'Interfaces')
  view_ref(:Summary, 'clauses',                 :label => 'Clauses')
  view_ref(:Summary, 'normative_references',    :label => 'Normative References')
  view_ref(:Summary, 'abbreviations',           :label => 'Abbreviations')
  view_ref(:Summary, 'definitions',             :label => 'Definitions')
  view_ref(:Summary, 'notes',                   :label => 'Notes')
  view_ref(:Summary, 'examples',                :label => 'Examples')
  view_ref(:Summary, 'figures',                 :label => 'Figures')
  view_ref(:Summary, 'tables',                  :label => 'Tables')
  view_ref(:Summary, 'codes',                   :label => 'Code Snippets')
  view_ref(:Summary, 'texts',                   :label => 'Text Sections')
  hidden_and_disabled = ['ics', 'ics_objects', 'interfaces', 'multipatientarchives','packages', 'tops','mds', 'asn1models', 'classifiers']
  hide(*hidden_and_disabled)
  disable(*hidden_and_disabled)
  hide('phdprofiles')
  disable('phdprofiles')
  relabel('classifiers', 'DIM Model')
  relabel('rtmmsterms', 'Nomenclature (All)')
  relabel('pcdprofiles', 'Device Profiles')
  # relabel('phdprofiles', 'PHD Device Profiles')
  relabel('profiles', 'Profiles (Experimental Work In Progress)')
  hide('profiles')
  create_delete_disabled = ['classifiers', 'terms', 'rtms', 'hrtms', 'x73s', 'metrics', 'units', 'unitgroups', 'enumerations', 'literals', 'tokens', 'enumerationgroups', 'ucumunits']
  disable_creation(*create_delete_disabled)
  disable_deletion(*create_delete_disabled)
  text_label('Nomenclature', :label => 'nomenclature', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold', 'border-radius' => '5px'})
  text_label('UML Classifiers', :label => 'uml_label', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold', 'border-radius' => '5px'})
  text_label('Document Builder', :label => 'docs_label', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold', 'border-radius' => '5px'})
  text_label('Device Profiles', :label => 'profiles_label', :style => {'font-size' => '20px', 'text-align' => 'center', 'display' => 'block', 'background-color' => 'gold', 'border-radius' => '5px'})
  html(:label => 'rtmms', :html => proc {
    haml = <<-EOS
    :sass
      $color:   #84f9eb
        
      .div_style
        // :text-align center
        :display block
        :font-size 16px
        :background-color #053aaa
        :border-radius 5px
        a
          color: $color
          text-align: center
          text-decoration: none
          margin-left: 20px

          &:visited
            color: #9cd5f3
          &:hover
            color: $color

        
    %div.div_style
      %a{:href => 'https://rtmms.nist.gov/rtmms/index.htm', :target=> '_blank'} NIST RTMMS (click to visit)
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  })
  reorder('profiles_label', 'pcdprofiles', 'phdprofiles', 'profiles', 'nomenclature', 'rtmms', 'rtmmsterms', 'rtms', 'hrtms', 'x73s', 'metrics', 'units', 'unitgroups', 'enumerations', 'literals', 'tokens', 'enumerationgroups', 'ucumunits', 'proposed_terms', 'uml_label', 'dim_pkg', 'dim_classifiers', 'asn1_classifiers',
   # 'asn1_enums', 'phd_pkg', 'phd_dim_classifiers', 'phd_asn1_classes', 'phd_asn1_enums',
    'ifaces', 'docs_label', 'ieeestandards', 'clauses', 'normative_references', 'definitions', 'abbreviations', 'texts', 'examples', 'notes', 'tables', 'figures', 'codes', :to_beginning => true)
}

collection(:dim_classifiers, MetaInfo::Classifier, :filter_value => {:boolean => {:is_asn1 => 'false'}, :integer => {'containing_package_id' => "4,6,8,10,12,14,16,18"}}) {
  inherits_from_spec(:Summary, MetaInfo::Classifier, :Collection, false)
}

collection(:asn1_classifiers, MetaInfo::Classifier, :filter_value => {:boolean => {:is_asn1 => 'true'}, :integer => {'containing_package_id' => "3,5,7,9,11,13,15,17,19"}}) {
  inherits_from_spec(:Summary, MetaInfo::Classifier, :Collection, false)
}

# collection(:asn1_classes, MetaInfo::Class, :filter_value => {:boolean => {:is_asn1 => 'true'}, :integer => {'containing_package_id' => "3,5,7,9,11,13,15,17,19"}}) {
#   inherits_from_spec(:Summary, MetaInfo::Class, :Collection, false)
# }
#
# collection(:asn1_enums, MetaInfo::Enumeration, :filter_value => {:boolean => {:is_asn1 => 'true'}, :integer => {'containing_package_id' => "3,5,7,9,11,13,15,17,19"}}) {
#   inherits_from_spec(:Summary, MetaInfo::Enumeration, :Collection, false)
# }

# collection(:phd_dim_classifiers, MetaInfo::Classifier, :filter_value => {:boolean => {:is_asn1 => 'false'}, :integer => {'containing_package_id' => "24"}}) {
#   inherits_from_spec(:Summary, MetaInfo::Classifier, :Collection, false)
# }
#
# collection(:phd_asn1_classes, MetaInfo::Class, :filter_value => {:boolean => {:is_asn1 => 'true'}, :integer => {'containing_package_id' => "25"}}) {
#   inherits_from_spec(:Summary, MetaInfo::Class, :Collection, false)
# }
#
# collection(:phd_asn1_enums, MetaInfo::Enumeration, :filter_value => {:boolean => {:is_asn1 => 'true'}, :integer => {'containing_package_id' => "25"}}) {
#   inherits_from_spec(:Summary, MetaInfo::Enumeration, :Collection, false)
# }
#
# collection(:phd_pkg, MetaInfo::Package, :filter_value => {:case_insensitive_like => {'name' => 'PHD'}}) {
#   inherits_from_spec(:Summary, MetaInfo::Package, :Collection, false)
# }

collection(:dim_pkg, MetaInfo::Package, :filter_value => {:case_insensitive_like => {'name' => 'DIM'}}) {
  inherits_from_spec(:Summary, MetaInfo::Package, :Collection, false)
}

collection(:Rosetta, Nomenclature::RTMMSTerm, :filter_value => {:case_insensitive_like => {'json_source_content' => '"RTM"'}}) {
  inherits_from_spec(:Summary, Nomenclature::RTMMSTerm, :Collection, false)
  hide('sources_temp', 'json_source')
}

collection(:hRTM, Nomenclature::RTMMSTerm, :filter_value => {:case_insensitive_like => {'json_source_content' => '"HRTM"'}}) {
  inherits_from_spec(:Summary, Nomenclature::RTMMSTerm, :Collection, false)
  hide('sources_temp', 'json_source')
}

collection(:x73, Nomenclature::RTMMSTerm, :filter_value => {:case_insensitive_like => {'json_source_content' => '"X73"'}}) {
  inherits_from_spec(:Summary, Nomenclature::RTMMSTerm, :Collection, false)
  hide('sources_temp', 'json_source')
}

collection(:Summary, Nomenclature::Unit) {
  hide('json_source', 'sources')
}

collection(:Summary, Nomenclature::Enumeration) {
  hide('json_source', 'sources')
}

collection(:Summary, Nomenclature::Token) {
  hide('json_source', 'sources')
}