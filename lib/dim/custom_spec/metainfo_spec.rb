# metainfo_classes = [MetaInfo::Class, MetaInfo::Interface, MetaInfo::Property, MetaInfo::Enumeration, MetaInfo::Event, MetaInfo::Method, MetaInfo::Package, MetaInfo::Group]
# metainfo_classes.each do |k|
#   organizer(:Details, k) {
#     not_admin = proc{ !Ramaze::Current.session[:is_site_admin] }
#     view_only_all_if(not_admin)
#   }
#   organizer(:Summary, k) {
#     not_admin = proc{ !Ramaze::Current.session[:is_site_admin] }
#     view_only_all_if(not_admin)
#   }
#   collection(:Summary, k) {
#     not_admin = proc{ !Ramaze::Current.session[:is_site_admin] }
#     view_only_all_if(not_admin)
#   }
# end

MetaInfo::Class

organizer(:Details, MetaInfo::Class) {
  is_asn1 = proc{ |klass| klass.is_asn1 }
  hide_if(is_asn1, 'name_binding', 'registered_as', 'is_abstract')  
}

organizer(:Details, MetaInfo::Enumeration) {
  is_asn1 = proc{ |klass| klass.is_asn1 }
  hide_if(is_asn1, 'name_binding', 'registered_as', 'is_abstract')  
}

organizer(:Admin, MetaInfo::Property) {
  inherits_from_spec(:Details, MetaInfo::ModelElement, nil, true)
  string('name', :label => 'Name')
  string('attribute_id', :label => 'Attribute Id')
  text('description', :label => 'Description')
  string('multiplicity', :label => 'Multiplicity')
  boolean('bidirectional', :label => 'Bidirectional')
  choice('decoration', 'deprecated argument', :label => 'Decoration')
  choice('update_type', 'deprecated argument', :label => 'Update Type')
  choice('optionality', 'deprecated argument', :label => 'Optionality')
  text('footnote', :label => 'Footnote')
  boolean('is_asn1', :label => 'Is Asn1')
  string('getter_for_property', :label => 'Getter For Property', :disabled => true)
  view_ref(:Summary, 'containing_classifier', :label => 'Containing Classifier')
  view_ref(:Summary, 'groups', :label => 'Groups')
  # view_ref(:Summary, 'specializations', :label => 'Specializations')
  view_ref(:Summary, 'type', :label => 'Type')
}

organizer(:Details, MetaInfo::Property) {
  # disable_all
}

collection(:Summary, MetaInfo::Property) {
  # hide('decoration', 'bidirectional', 'multiplicity')
}

organizer(:Details, MetaInfo::Group) {
  message('summary_html')#, :label => 'Group Elements')
  reorder('summary_html', :to_beginning =>  true)
}

# MetaInfo::PropertyType ===================================
collection(:Summary, MetaInfo::PropertyType) {
  inherits_from_spec(nil, MetaInfo::Classifier, nil, true)
}

organizer(:Summary, MetaInfo::PropertyType) {
  inherits_from_spec(nil, MetaInfo::Classifier, nil, false)
}

organizer(:Details, MetaInfo::PropertyType) {
  inherits_from_spec(nil, MetaInfo::Classifier, nil, false)
}