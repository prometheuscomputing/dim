organizer(:Summary, MyDevice::ProfileRoot) {
  hide('name_in_profile')
  relabel('uui', "UUID")
}

organizer(:Details, MyDevice::PCDProfile) {
  button('set_tree_view_root', label: 'Set Tree Root', button_text: 'Root', save_page: false, display_result: :none)
  order('set_tree_view_root', :to_beginning => true)
  disable('uuid', 'creation_time')
  # href("/tree_home/", text: 'Containment View', target: '_top')
  # string('test_method', :label => 'TEST', :show_if => :show_test) # this is here to test new functionality
  button('download_rch_html', label: 'Rosetta Containment HTML', button_text: 'RCH HTML', save_page: true, display_result: :file)
  # button('download_html_report', label: 'Report', button_text: 'Download HTML', save_page: true, display_result: :file)
  # button('download_report', label: 'Report', button_text: 'Download', save_page: true, display_result: :file)
  button('download_summary_hierarchy', label: 'Rosetta Containment XML', button_text: 'Download XML', save_page: true, display_result: :file)
  button('download_summary_hierarchy_with_codes', label: 'Rosetta Containment XML with codes', button_text: 'Download XML', save_page: true, display_result: :file)
  # button('download_verbose_hierarchy', label: 'Verbose Hierarchy:', button_text: 'Work In Progress', save_page: true, display_result: :file)
  # button('download_json', label: 'JSON Graph:', button_text: 'Download JSON', save_page: true, display_result: :file)
  button('download_full_xml', label: 'Full XML:', button_text: 'Download XML', save_page: true, display_result: :file)
  # button('download_c4mi_xml', label: 'C4MI XML:', button_text: 'Download XML', save_page: true, display_result: :file)
  file('source_xml', :label => 'Originally Uploaded XML', :disabled => true) 
  hide('name_in_profile')
  relabel('uuid', "UUID")
}

collection(:Summary, MyDevice::PCDProfile) {
  file('source_xml', :label => 'Originally Uploaded XML', :disabled => true) 
  hide('uuid', 'name_in_profile', 'source_xml')
  # relabel('uuid', "UUID")
  reorder('name', 'company_owner', 'purpose', 'creation_time', 'description', 'intended_use', :to_beginning => true)
}

organizer(:Details, MyDevice::PHDProfile) {
  disable('uuid', 'creation_time')
  # href("/tree_home/", text: 'Containment View', target: '_top')
  # string('test_method', :label => 'TEST', :show_if => :show_test) # this is here to test new functionality
  button('download_rch_html', label: 'Rosetta Containment HTML', button_text: 'RCH HTML', save_page: true, display_result: :file)
  # button('download_html_report', label: 'Report', button_text: 'Download HTML', save_page: true, display_result: :file)
  # button('download_report', label: 'Report', button_text: 'Download', save_page: true, display_result: :file)
  button('download_summary_hierarchy', label: 'Rosetta Containment XML', button_text: 'Download XML', save_page: true, display_result: :file)
  button('download_summary_hierarchy_with_codes', label: 'Rosetta Containment XML with codes', button_text: 'Download XML', save_page: true, display_result: :file)
  # button('download_verbose_hierarchy', label: 'Verbose Hierarchy:', button_text: 'Work In Progress', save_page: true, display_result: :file)
  # button('download_json', label: 'JSON Graph:', button_text: 'Download JSON', save_page: true, display_result: :file)
  button('download_full_xml', label: 'Full XML:', button_text: 'Download XML', save_page: true, display_result: :file)
  # button('download_c4mi_xml', label: 'C4MI XML:', button_text: 'Download XML', save_page: true, display_result: :file)
  file('source_xml', :label => 'Originally Uploaded XML', :disabled => true) 
  hide('name_in_profile')
  relabel('uuid', "UUID")
}

collection(:Summary, MyDevice::PHDProfile) {
  hide('source_xml')
  hide('uuid', 'name_in_profile', 'source_xml')
  relabel('uuid', "UUID")
  reorder('name', 'company_owner', 'purpose', 'creation_time', 'description', 'intended_use', :to_beginning => true)
}