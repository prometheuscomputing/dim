# Nomenclature::RTMMSTerm =======================================
collection(:Summary, Nomenclature::RTMMSTerm) {
  hide('update_date', 'json_source', 'sources')
  relabel('sources_temp', 'Sources')
}

collection(:TermChooser, Nomenclature::RTMMSTerm) {
  inherits_from_spec(:Summary, Nomenclature::RTMMSTerm, nil, false)
  disable_creation
  disable_deletion
}

organizer(:Summary, Nomenclature::RTMMSTerm) {
  hide('update_date', 'json_source', 'sources')
  relabel('sources_temp', 'Sources')
}

organizer(:Details, Nomenclature::RTMMSTerm) {
  hide('sources')
  relabel('sources_temp', 'Sources')
}

organizer(:Details, Nomenclature::Metric) {
  hide('model_element')
}

organizer(:Details, Nomenclature::Enumeration) {
  hide('model_element')
}

organizer(:Details, Nomenclature::Literal) {
  hide('model_element', 'allowed_for')
  relabel('enumeration_groups', 'Found In These Enumeration Groups')
  relabel('enumerations', 'Enumerations Using This Literal')
}

organizer(:Details, Nomenclature::Unit) {
  hide('model_element', 'allowed_for')
  relabel('unit_groups', 'Found In These Unit Groups')
  relabel('metrics', 'Metrics Using This Unit')
}

organizer(:Details, Nomenclature::EnumerationGroup) {
  hide('model_element', 'allowed_for')
  relabel('enumerations', 'Enumerations Using This Group')
}

organizer(:Details, Nomenclature::UnitGroup) {
  hide('model_element', 'allowed_for')
  relabel('metrics', 'Metrics Using This Group')
}

# Nomenclature::Token ======================================
collection(:Summary, Nomenclature::Token) {
  hide('update_date', 'json_source')
  relabel('sources_temp', 'Sources')
}

organizer(:Summary, Nomenclature::Token) {
  hide('update_date', 'json_source')
  relabel('sources_temp', 'Sources')
}

collection(:Summary, Nomenclature::EnumerationGroupMember) {
  string('reference_id', :label => 'RefID (or token)')
}

collection(:Summary, Nomenclature::EnumerationItem) {
  string('reference_id', :label => 'RefID (or token)')
}

organizer(:Details, Nomenclature::Token) {
  inherits_from_spec(nil, Nomenclature::EnumerationGroupMember, nil, true)
  relabel('sources_temp', 'Sources')
  hide('sources', 'allowed_for')
  string('name', :label => 'Name')
  choice('sources', 'deprecated argument', :label => 'Sources')
  choice('status', 'deprecated argument', :label => 'Status')
  timestamp('update_date', :label => 'Update Date')
  text('json_source', :label => 'JSON Source')
}

# Nomenclature::UnitItem ===================================
collection(:Summary, Nomenclature::UnitItem) {
  inherits_from_spec(nil, Nomenclature::RTMMSTerm, nil, false)
}
