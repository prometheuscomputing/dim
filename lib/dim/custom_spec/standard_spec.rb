module GuiViewTests
  def self.user_is_not_admin
    proc do
      user = Ramaze::Current.session[:user]
      !user.site_admin?
    end
  end
end
    

organizer(:Details, Standard::IEEEStandard) {
  # button('set_tree_view_root', label: 'Set Tree Root', button_text: 'Root', save_page: false, display_result: :none)
  view_ref(:Summary, 'content', :label => 'Contents', :orderable => true, :expanded => true)
  order('set_tree_view_root', 'title', 'subtitle1', 'subtitle2', 'content', :to_beginning => true)
  order('remark', :to_end => true)
  disable_deletion_if(GuiViewTests.user_is_not_admin, 'content') # so, how do we delete content then?  TODO create deletion logic that stops deletion of shared aggregation objects.
}

organizer(:Details, Standard::Clause) {
  view_ref(:Summary, 'content', :label => 'Contents', :orderable => true, :expanded => true)
  order('title', 'version', 'content', 'source_file', 'file_usage', 'render_as', 'footnotes', 'containers', 'ieee_standards', :to_beginning => true)
  relabel('ieee_standards', 'Used in these Standards')
  relabel('containers', 'Is Contained in these Parent Clauses')
  has_parent = proc{ |clause| clause.containers_count < 1 ? true : false }
  hide_if(has_parent, 'containers')
  top_level_clause = proc{ |clause| clause.ieee_standards_count < 1 ? true : false }
  hide_if(top_level_clause, 'ieee_standards')
  disable_deletion_if(GuiViewTests.user_is_not_admin, 'content')
}

collection(:Summary, Standard::ClauseContent) {
  string('content_display', :label => 'Content')
  string('parent_titles',    :label => 'Containers')
  order('content_display', 'parent_titles', :to_beginning => true)
}

collection(:Summary, Standard::Content) {
  string('content_display', :label => 'Content')
  string('parent_titles',    :label => 'Containers')
  order('content_display', 'parent_titles', :to_beginning => true)
}

collection(:Summary, Standard::Clause) {
  order('title', 'content', 'source_file', 'file_usage', 'render_as', :to_beginning => true)
}

collection(:Summary, Standard::Abbreviation) {
  order('symbol', 'meaning', :to_beginning => true)
  hide('content', 'render_as', 'source_file', 'file_usage')
}

collection(:Summary, Standard::Definition) {
  order('term', 'definition', :to_beginning => true)
  hide('content', 'render_as', 'source_file', 'file_usage')
}

collection(:Summary, Standard::NormativeReference) {
  order('source', 'description', :to_beginning => true)
  hide('content', 'render_as', 'source_file', 'file_usage')
}

collection(:Summary, Standard::Text) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('content', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

collection(:Summary, Standard::Note) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('content', 'note_type', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

collection(:Summary, Standard::Example) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('content', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

collection(:Summary, Standard::Table) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('title', 'caption', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

collection(:Summary, Standard::Figure) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('title', 'caption', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

collection(:Summary, Standard::Code) {
  string('parent_titles', :label => 'Containers (info only, non-searchable)', :search_filter => :disabled)
  order('title', 'code', 'parent_titles', 'render_as', 'source_file', 'file_usage', :to_beginning => true)
}

([Standard::IEEEStandard] + Standard::ClauseContent.implementors + Standard::Content.implementors).uniq do |imp|
  organizer(:Details, imp) {
    button('download_docx', label: nil, button_text: 'MS Word', save_page: true, display_result: :file)
    # button('test_docx', label: nil, button_text: 'Test', save_page: false, display_result: :none)
    reorder('download_docx', 'test_docx', :to_beginning => true)    
  }
end


