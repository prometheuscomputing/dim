# document(:Details, MyDevice::PCDProfile, :identifier => 'description', :title => 'PCD Profile') {|profile|
#   newline
#
#   header(1, 'text-align' => 'center') {
#     write 'PCD Profile for '
#     string 'description'
#     write '.'
#   }
#
#   # item('profile_root', :profile_root, :label => 'Useless') {|pr|
#   #   write 'Profile Root: '; string 'name';newline
#   #   write 'Class: '; string 'class.to_s', disabled: true
#   # }
#
#   list('profile_root', :profile_root, label: 'Useless') {|pr|
#     write 'Profile Root: '; string 'name';newline
#     write 'Class: '; string 'class.to_s', disabled: true
#   }
#   #
#   # write ' was born on '; date 'date_of_birth'; write ', weighs '; float 'weight'; write ' and is '; choice 'handedness', handedness; write '.'; newline
#   # # NOTE: It would be nice to assign custom choices for booleans, but this will require an update to the JS for document widgets.
#   # #       We could no longer just copy the value of an input to the pretty views.
#   # #string 'name'; write ' '; boolean 'dependent', :choices => ["is", "is not"]; write ' a dependent, and his/her family size is '; integer 'family_size'; write '.'
#   # write 'It is '; boolean 'dependent'; write ' that '; string 'name'; write ' is a dependent.'; newline
#   # string 'name'; write ' has a family size of '; integer 'family_size'; write '.'; newline
#   # write 'A photo of this employee: '; file 'photo'; newline
#   # # write 'A photo of this employee:'; image 'photo'
#   #
#   # newline
#   # write 'He is best described by "'
#   # richtext 'description'
#   # write '".'
#   # newline
#   #
#   # newline
#   # string 'name'
#   # write ' drives the following vehicles:'
#   # # TODO: should not need to specify label here, but it currently is required
#   # list('drives', :drives_summary, :label => 'Drives') { |vehicle, driving|
#   #   write 'A '
#   #   choice 'make', vehiclemaker
#   #   write ' '
#   #   string 'vehicle_model'
#   #   write ' with the VIN: '
#   #   integer 'vin.vin', :label => 'VIN'
#   # }
#   # newline
#   # string 'name'
#   # write ' also owns the following vehicles:'
#   # list('vehicles', :owned_vehicles_summary, :label => "Owned Vehicles") { |vehicle, ownership|
#   #   write 'A '
#   #   choice 'make', vehiclemaker
#   #   write ' '
#   #   string 'vehicle_model'
#   #   write ' with the V.I.N.: '
#   #   integer 'vin.vin', :label => 'VIN'
#   #   newline
#   #   # write 'Percent Ownership is: '; float 'percent_ownership', :context => ownership
#   #   # newline
#   #   write 'With the warranties:'
#   #   list('components', :component_warranty_table) {|component|
#   #     write 'The '
#   #     #string 'vehicle_model', :context => vehicle; write "'s "
#   #     string 'name'; write ' has '
#   #     string 'warranty.coverage', :default => 'no', :label => 'Coverage'; write ' coverage.'
#   #     # context(nil, nil, :getter => 'warranty'){
#   #     #   string 'coverage', :default => 'no', :label => 'Coverage'; write ' coverage.'
#   #     # }
#   #   }
#   # }
#   # newline
#   # write 'He or she is currently occupying a:'
#   # list('occupying', :occupying_summary, :label => 'Occupying', :list_style_type => 'none') { |vehicle|
#   #   choice 'make', vehiclemaker
#   #   write ' '
#   #   string 'vehicle_model'
#   # }
#   # newline
#   #
#   # string 'name'; write ' shares his vehicles with the following co-owners:'
#   # list('co_owners', :co_owners_summary, :label => 'Co-owners', :list_style_type => 'decimal') { |co_owner|
#   #   string 'name'
#   # }
#   # write 'Their names are: '; string 'co_owner_names'
#   # # list('co_owners', :co_owners_summary, :label => 'Co_Owners') { |vehicle, co_owner|
#   # #   string 'name'
#   # # }
#   # newline
#   # newline
#   #
#   # # write 'These vehicles are registerd at the listed addresses for this employee:'
#   # # table('addresses.registered_vehicles', :vehicles_at_addresses, :label => 'Vehicles At Addresses', :merge => ['street_name', 'street_number']) {|address, vehicle|
#   # #   string 'street_name', :context => address
#   # #   string 'street_number', :context => address, :justify_data => :left
#   # #   string 'make', :context => vehicle
#   # #   string 'vehicle_model'
#   # # }
#   # write 'Table summary of owned vehicles:'
#   # table('vehicles', :owned_vehicles_table, :label => 'Vehicles Table', :merge => ['make']) {|ownership, vehicle|
#   #   choice 'make', vehiclemaker
#   #   string 'vehicle_model', :context => vehicle
#   #   integer 'cost'
#   #   #float 'vehicles_association.percent_ownership'
#   #   #float 'percent_ownership', :context => ownership
#   # }
#   # newline
#   # newline
#   #
#
# }