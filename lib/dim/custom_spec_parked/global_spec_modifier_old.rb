require 'pp'
global_spec_modifier { |view|
  kontent = view.content
  view_classifier = view.data_classifier
  
# debug = view_classifier == DIM::System::MDS || view_classifier == DIM::System::SingleBedMDS || view_classifier == DIM::System::VMS
# debug = view_classifier == DIM::CommonDataTypes::ASN1::Locale
# debug = debug && view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Details)
# debug = view_classifier == DIM::System::SingleBedMDS
debug = false
  # hide composers -- no need to navigate upwards. added complexity of doing so is unnecessary and potentially confusing
  composer_widgets = kontent.select{|w| w.respond_to?(:composer) && w.composer == true }.collect{|w| w.getter.to_s}
  view.hide(*composer_widgets)
  
  # fix naming issues
  kontent.each do |w|
    lbl = w.label
    w.label = lbl.sub(/id(_| )dim/i, "ID") if lbl# =~ /id(_| )dim/i
  end
  # fix typos caused by bad automatic parsing of names
  # view.relabel('vmds', "VMDs")
  # view.relabel('vmd_model', "VMD Model")
  # view.relabel('mds_status', "MDS Status")
  
  view.hide('ref_id') # FIXME  I think this needs to be removed from the model if the proxy stuff works
  
  view.hide('available_attributes') # this collection serves only to restrict what may be used in #used_attributes
  
  if debug
    # puts "\n******* Processing Details view for #{view_classifier} *******"
    # puts "This is the content for the #{view_classifier} details organizer"
    # view.content.each do |vc|
    #   puts "#{vc.class} -- #{vc.getter}"
    # end
    # puts "****************************************************************"
  end
  # # Put strings, numbers, and choices at the top.  There are more types of widgets but they are generally rare within the object types of interset for this project
  # choice_getters = kontent.select{|contained_widget|
  #   yes = contained_widget.is_a?(Gui::Widgets::Choice)
  #   # pp contained_widget if yes
  #   # puts if yes
  # }.collect{|widget| widget.getter.to_s}
  # string_getters = kontent.select{|contained_widget| contained_widget.is_a?(Gui::Widgets::String)}.collect{|widget| widget.getter.to_s}
  # numeric_getters = kontent.select{|contained_widget| contained_widget.is_a?(Gui::Widgets::Numeric) || contained_widget.is_a?(Gui::Widgets::Fixedpoint)}.collect{|widget| widget.getter.to_s}
  # the_getters = string_getters + numeric_getters + choice_getters
  # view.reorder(*the_getters, :to_beginning => true)
  
  view_is_dim = view_classifier.to_s =~ /DIM/
  # segregate attributes and associations into groups and order accordingly
  # this isn't completely bulletproof because simple widgets don't have data_classifier
  # I think we really only want to do this for views that map to Top objects as well so we need to take that into consideration.
  if view_is_dim && view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Details)
    view_is_asn1 = view_classifier.to_s =~ /ASN1/
    if true#!view_is_asn1
      puts "\n******* Processing Details view content for #{view_classifier} *******" if debug
      asn1_kontent = []
      profile_kontent = []
      top_kontent = []
      asn1_simple_kontent = []
      kontent.map! do |contained_widget|
        dc = contained_widget.data_classifier.to_s
        gitter = contained_widget.getter
puts "input widget: #{dc} -- #{gitter} -- #{contained_widget.inspect}\n" if debug
        case
        
        when dc =~ /MyDevice|MetaInfo/
          profile_kontent << contained_widget.getter.to_s
        
        # Here we are dealing with all of the OIDType proxies
        # when dc =~ /OIDType/
        #   puts "making OIDType proxy for #{gitter}" if debug
        #   # puts; pp contained_widget
        #   # view.hide(contained_widget.getter) # instead we will replace it
        #   proxy_getter = contained_widget.getter + "_proxy"
        #   lbl = contained_widget.label.gsub("RESERVED","").strip
        #   proxy_view_ref = Gui::Widgets::ViewRef.new(parent: contained_widget.parent, getter: proxy_getter, view_name: contained_widget.view_name, label: lbl + " (Term as proxy for OID)", filter_value: contained_widget.filter_value)
        #   # view.content << proxy_view_ref
        #   contained_widget = proxy_view_ref
        
        # Here we are dealing with all of the TYPE proxies
        # when dc =~ /TYPE/
        #   puts "making TYPE proxy for #{gitter}" if debug
        #   # puts; pp contained_widget
        #   # view.hide(contained_widget.getter) # instead we will replace it
        #   proxy_getter = contained_widget.getter + "_proxy"
        #   lbl = contained_widget.label.gsub("RESERVED","").strip
        #   proxy_view_ref = Gui::Widgets::ViewRef.new(parent: contained_widget.parent, getter: proxy_getter, view_name: contained_widget.view_name, label: lbl + " (Term as proxy for TYPE)", filter_value: contained_widget.filter_value)
        #   # view.content << proxy_view_ref
        #   contained_widget = proxy_view_ref
      
        # Proxy for types that need to be treated as primitives.  Should be obsolete since these really are primitives now.
        when dc =~ /ASN1|IEEE20101/
          puts "ASN1 Content << #{gitter}\n" if debug
          classifier_for_label = dc.demodulize
          # if (dc.to_const == IEEE20101::ASN1::INTEGER) || (dc.to_const.parents.include?(IEEE20101::ASN1::INTEGER))
          #   puts "making Integer proxy for #{gitter}" if debug
          #   # view.hide(contained_widget.getter)
          #   proxy_getter = contained_widget.getter + "_proxy"
          #   proxy_integer = Gui::Widgets::Numeric.new(type: :Integer, parent: contained_widget.parent, getter: proxy_getter, label: "#{contained_widget.label} (#{classifier_for_label})")
          #   # view.content << proxy_integer
          #   contained_widget = proxy_integer
          # elsif (dc.to_const == IEEE20101::ASN1::OCTET_STRING) || (dc.to_const.parents.include?(IEEE20101::ASN1::OCTET_STRING))
          #   puts "making String proxy for #{gitter}" if debug
          #   # view.hide(contained_widget.getter)
          #   proxy_getter = contained_widget.getter + "_proxy"
          #   lbl = contained_widget.label.gsub(/ Dim$/,"").strip
          #   proxy_string = Gui::Widgets::String.new(parent: contained_widget.parent, getter: proxy_getter, label: "#{lbl} (#{classifier_for_label})")
          #   # view.content << proxy_string
          #   contained_widget = proxy_string
          # else
            contained_widget.label = "#{contained_widget.label} (#{classifier_for_label})"
            # puts "Data Classifier is #{dc} and getter is #{contained_widget.getter}" if contained_widget.getter.to_s =~ /system_id/i
          end
          asn1_kontent << contained_widget.getter.to_s
        
        when dc =~ /DIM/
          puts "Top Content << #{gitter}\n" if debug
          top_kontent << contained_widget.getter.to_s # this should work but checking data_classifier.ancestors.include? DIM::Top::Top would perhaps be better
        else
          asn1_simple_kontent << contained_widget.getter.to_s
          # FIXME you will need to turn this back on
          # puts "Widget is a view on a #{dc}"
        end
        
        contained_widget # this MUST be the last thing in the map! block
      end
      # if debug
      #   puts "****************************************************************"
      #   puts "And this is now the content for the #{view_classifier} details organizer"
      #   view.content.each do |vc|
      #     puts "#{vc.class} -- #{vc.getter}"
      #   end
      #   puts "****************************************************************"
      # end
      profile_kontent.unshift asn1_simple_kontent.delete("cardinality")
      profile_kontent.unshift asn1_simple_kontent.delete("name")
      asn1_kontent = asn1_simple_kontent + asn1_kontent
      profile_kontent.push asn1_kontent.delete("model_class")
      profile_kontent.push asn1_kontent.delete("valid_terms")
      
      # TODO -- add to issue tracker -- Context Widgets don't form unique id_strings so we are giving them a label so they can be properly ordered by ViewDSL#order.  This is because id_string is formed by "#{self.class}_#{self.getter}" and the Context widgets have no getters.
      big_font_size = '20px'
      medium_font_size = '16px'
      if profile_kontent.any?
        # summary = Gui::Widgets::String.new(getter: 'profile_summary', label: '', disabled: true)
        summary = Gui::Widgets::Message.new(getter: 'profile_summary')
        label = Gui::Widgets::TextLabel.new(text: 'PCD Profile Information')
        style = Gui::Widgets::Context.new(style: {'font-size' => big_font_size}, label: 'Profile Information')
        style.content << label
        view.content << style
        view.content << summary
        profile_kontent.unshift summary.getter
        profile_kontent.unshift style.label
      end
      if top_kontent.any? && !view_is_asn1
        label = Gui::Widgets::TextLabel.new(text: 'Device Components')
        style = Gui::Widgets::Context.new(style: {'font-size' => big_font_size}, label: 'DIM Object Containment')
        style.content << label
        view.content << style
        top_kontent.unshift style.label
      end
      if asn1_kontent.any? && !view_is_asn1
        asn1_label = Gui::Widgets::TextLabel.new(text: 'ASN.1 Attributes For This Object')
        mandatory_attributes = Gui::Widgets::String.new(getter: 'mandatory_attribute_names', label: 'Mandatory Attributes', disabled: true)
        style_big = Gui::Widgets::Context.new(style: {'font-size' => big_font_size}, label: 'Attributes (ASN.1 )')
        style_medium = Gui::Widgets::Context.new(style: {'font-size' => medium_font_size, 'color' => 'red'}, label: 'mandatory attributes')
        style_big.content << asn1_label
        style_medium.content << mandatory_attributes
        view.content << style_big
        view.content << style_medium
        asn1_kontent.unshift style_medium.label
        asn1_kontent.unshift style_big.label
      end
      view.order(*profile_kontent, *top_kontent, *asn1_kontent)
    end
  end  
  # if view_classifier.to_s =~ /SingleBed/
  #   puts asn1_kontent.inspect
  #   view.content.each do |vc|
  #     puts "#{vc.class} -- #{vc.getter}"
  #   end
  # end 
  
  # view.relabel('class_reserved_proxy', "Class")
  
  # disable all fields in organizer summary widgets
  if view.is_a?(Gui::Widgets::Organizer) && (view.view_name == :Summary)
    getters = kontent.collect{|w| w.getter.to_s}
    view.disable(*getters)
  end
}

# OLD STUFF KEPT HERE FOR REFERENCE

# hide all ASN.1 types
# vrefs = kontent.select{|contained_widget| contained_widget.is_a?(Gui::Widgets::ViewRef)}
# asn1_getters = vrefs.select{|w| w.data_classifier.to_s =~ /ASN1|Common_Data_Type|OCTET_STRING/}.collect{|w| w.getter.to_s}
# view.hide(*asn1_getters)

# put commonly used things at the top and things that ought to be ignored (for now) at the bottom
# view.reorder('ref_id', 'valid_terms', 'attribute_specializations', 'used_attributes', :to_beginning => true)
# view.reorder('handle', 'name_binding', 'class_reserved', 'model_class', to_end: true)
