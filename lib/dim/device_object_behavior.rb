module DeviceObjectBehavior

  def locally_defined_getters(added_getters = [])
    [:cardinality] | []
  end
  
  def used_attributes
    puts "used_attribute_list is nil for #{self.pretty_inspect}" unless used_attribute_list
    # FIXME this really bad but it is here because I am frustrated and lazy.  used_attribute_list should not be nil.
    json = JSON.parse(used_attribute_list || "{}")
  end
  
  def used_getters(added_getters = [])
    used_asn1_getters = used_attributes.collect{|name, info| info["getter"].to_sym}
    dim_assoc_getters = self.class.associations.select{|k, v| MyDevice::ALL_DIM_CLASSES.include? v[:class].to_const}.collect{|k, v| v[:getter]}
    used_dim_assoc_getters = dim_assoc_getters.select{|dag| assoc_count = (dag.to_s + "_count").to_sym; self.send(assoc_count) > 0}
    used_dim_assoc_getters | added_getters | used_asn1_getters | locally_defined_getters
  end

  def after_change_tracker_create
    set_metainfo

    # FIXME TODO this will need to be more sophisticated when the objects have parents that might have additional used attributes or may have attributes that have been designated as mandatory in a parent profile.  Of course, these attributes may become properties of the profile and not the class instance.  We'll have to see how that works out
    self.available_attribute_list = self.class.available_attributes.to_json
    set_mandatory_attributes
    super
  end
  
  def after_change_tracker_save
    set_used_attributes
    super
  end
  
  # TODO TODO TODO TODO write code to go through all of the asn1_getters (of every top class) and make sure that we can match a MetaInfo::Property to a getter for every getter (see below). if that works then set an instance variable on each AttributeInclusion that tells what its getter is.
  
  # TODO this has some hinky stuff in it for a temporary workaround
  def set_used_attributes
    initialized_getters = asn1_getters.select do |getter|
      counter = (getter.to_s + "_count").to_sym
      c = self.send(counter) if self.respond_to?(counter)
      c = self.send(getter)  if self.respond_to?(getter) && c.nil?
      if c.nil?
        plural_getter = (getter.to_s + "s").to_sym # Well that is pretty terrible but we should need to do this anyway
        c = self.send(plural_getter) if self.respond_to? plural_getter
      end
      c = nil if c == 0 || c == ''
      c
    end
    # puts "#{self}[#{self.id}] initialized_getters are: #{initialized_getters}\n#{caller[0].pretty_inspect}"
    # puts "initialized_getters for #{self} are #{initialized_getters.inspect}"
    # FIXME TODO will there ever be a situation in which the list of available attributes for an instance of a class with be differnt than the list of available attributes held by a class?  This may be true in the case of mutually exclusive conditional attributes where one or the other has been chosen by the parent profile / instance.
    if self.available_attribute_list
      aa = JSON.parse(self.available_attribute_list)
    else  
      aa = self.class.available_attributes
    end

    ual = aa.select do |label,data|
      (data["optionality"] =~ /mandatory/) || initialized_getters.include?(data["getter"].to_sym)
    end
    json = ual.to_json
    self.used_attribute_list = json
  end
  
  def set_mandatory_attributes
    if self.available_attribute_list
      aa = JSON.parse(self.available_attribute_list)
    else
      aa = self.class.available_attributes
    end
    mal = aa.select{|k,v| v["optionality"] =~ /mandatory/i}.collect{|k,v| k}.sort.join(', ')
    # puts mal.inspect
    self.mandatory_attribute_list = mal
  end

  def asn1_getters
    self.class.asn1_getters
  end

  # TODO this should really do something more useful and intelligent when there isn't one matching meta_class
  # FIXME with the introduction of PHD (and SDC soon) there will be multiple instances of MetaInfo::Class that have the same value for #name
  # def get_meta_properties
  #   meta_klasses = MetaInfo::Class.where(:name => self.simple_class_name).all
  #   if meta_klasses.count > 1
  #     puts "Too many meta_classes for class #{self.class}: #{meta_klasses.inspect}"
  #     return []
  #   end
  #   if meta_klasses.count < 1
  #     puts "No meta_class for class #{self.class}"
  #     return []
  #   end
  #   meta_klass = meta_klasses.first
  #   meta_klass.all_properties
  # end

  def set_metainfo
    # puts "calling set_metainfo from #{self.class}-#{self.object_id}"
    if self.model_class_count < 1
      mc = MetaInfo::Class.where(:name => self.simple_class_name).first
      if mc
        # puts "about to set model_class from #{self.class}-#{self.object_id}"
        mc.instances_add self
        # self.model_class = mc
        # puts "set #{self.class}.model_class to: #{mc.inspect}"
        if mc.name_binding && mc.name_binding.strip =~ /^handle.*$/i
          # handle_term_code = MetaInfo::Class.where(:name => 'Handle').first.term.term_code.to_s # This illustrates the point
          # self.name_binding =
          # Note that the term with this refid really should be a Term so we are not searching RTMMSTerm
          t = Nomenclature::Term.where(:reference_id => 'MDC_ATTR_ID_HANDLE').first
          self.name_binding_term = t
          t.save
          save
        end
        if term = mc.term
          self.class_term = term if self.respond_to?(:class_term) # because there are a few classes that don't!
          term.save
          save
        end
        mc.save
      end
    end
  end

  # FIXME
  def normative?
    # return false # TODO comment out in order to switch behavior on type of PCD_Profile
    
    # TODO: this is currently always returning nil for object creation pages.
    # assoc-id and assoc-classifier URL params must instead be specified using pending association addition functionality
    parent = composer
    return false unless parent
    parent.normative?
  end

  def _type_term
    if self.respond_to?(:system_type)
      typ = self.system_type_term
    elsif self.respond_to?(:type)
      typ = self.type_term
    else # not everything has a type or system_type attribute
      typ = nil
    end
  end
  
  def display_type(alert = true)
    if self.respond_to?(:type) || self.respond_to?(:system_type)
      if term = _type_term
        if ref_id = term.reference_id
          ref_id
        else
          if alert
            "TERM IS MISSING REFID"
          else
            nil
          end
        end
      elsif pt = proposed_type
        pt.reference_id
      end
    else
      nil
    end
  end
  
  def html_display_type
    if self.respond_to?(:type) || self.respond_to?(:system_type)
      if term = _type_term
        if ref_id = term.reference_id
          if term&.status&.value =~ /Approved/
            "<font style='background-color: chartreuse'>#{ref_id}</font> "
          elsif term&.status&.value =~ /Proposed/
            "<font style='background-color: yellow'>#{ref_id}</font> "
          else
            "<font style='background-color: violet'>#{ref_id}</font> "
          end
        else
          "<font style='background-color: red'>term needed</font> "
        end
      elsif pt = proposed_type
        if pt.found_in_rtmms
          "<font style='background-color: magenta'>#{pt.reference_id}</font> "
        else
          "<font style='background-color: orange'>#{pt.reference_id}</font> "
        end
      end
    else
      nil
    end
  end

  def display_name
    if c = cardinality
      "#{html_display_type}#{self.class.name.demodulize} (#{c})"
    else
      "#{html_display_type}#{self.class.name.demodulize}"
    end
  end

  def class_ref_id
    if mc = model_class
      mc.registered_as
    else
      begin
        message = "unknown error"
        meta_classes = MetaInfo::Class.where(:name => self.simple_class_name).all
        message = "too many meta_classes match #{self.simple_class_name}: #{meta_classes.inspect}" if meta_classes.count > 1
        message =  "can not find a meta_class for #{self.simple_class_name}" if meta_classes.count < 1
        meta_class = meta_classes.first
        if answer = meta_class.registered_as
          answer
        else
          # puts "meta_class is #{meta_class}"
          term = meta_class.term
          message = "could not find term for metaclass #{meta_class.inspect}" unless term
          ref_id = term.reference_id if term
          message = "the term of the metaclass for #{self.simple_class_name} did not have a value for reference_id" unless ref_id
          message = "the term of the metaclass for #{self.simple_class_name} had an empty value for reference_id" if ref_id.nil? || ref_id.empty?
          ref_id
        end
      rescue Exception => e
        puts message
        puts e.message
        puts e.backtrace
        "ref_id needed"
      end
    end
  end

  def composition_cardinality
    cardinality_hash = {}
    # getters = self.class.associations.collect{|a| a[1][:getter]}
    specialization_assocs = self.property_specializations_associations
    specialization_assocs.each do |sa|
      # keys are getters, values are cardinality of that getter
      cardinality_hash[sa[:to][:name].downcase.to_sym] = sa[:through][:cardinality]
    end
    cardinality_hash
  end

  def containment_tag_name
    case
    when kind_of?(DIM::Medical::Metric)
      "Metric"
    when kind_of?(DIM::System::MDS)
      "MDS"
    else
      simple_class_name
    end
  end

  def simple_class_name
    return self.class.to_s.demodulize
  end

  def fix_dim_attribute_name name
    parts = name.gsub(/_dim$/, "").gsub(/_RESERVED/, "").split(/_|-/)
    fixed_name = parts.each{|p| p.capitalize!}.join("-")
  end

  def dim_obj_aggregations
    self.aggregations.select{|c| MyDevice::ALL_DIM_CLASSES.include? c}
  end

  def tops type_hash = {}
    dim_obj_aggregations.each { |aggs| aggs.tops(type_hash) }
    type_hash[self.class] ||= []
    type_hash[self.class] << self
    type_hash
  end

  def tops_report_hash
    report = {ref_ids: [], classes: {}}
    tops.each do |klass, instances|
      # TODO we could do some sanity checking here, e.g. make sure each obj has a name (not empty or nil)
      # names = instances.collect{|instance| instance.name_in_profile}.compact
      common_asn1_attributes = instances.collect{|instance| instance.used_attributes.collect{|ua| ua.name}}.inject(:&)
      report[:classes][klass.to_s.demodulize] = {count: instances.uniq.count, common_attributes: common_asn1_attributes}
      instances.each do |instance|
        term = instance._type_term
        r_id = term.reference_id if term
        report[:ref_ids] << r_id if r_id
      end
    end
    report
  end

  def tops_report
    trh = tops_report_hash
    str = ""
    str << "Contained Components:\n" if trh[:classes].any?
    trh[:classes].to_a.reverse.each do |klass|
      klass_name = klass[0]
      info_hash = klass[1]
      singular = info_hash[:count] < 2 
      # str << "  There #{singular ? 'is' : 'are'} #{info_hash[:count]} #{klass_name} object#{singular ? '' : 's'}: #{info_hash[:names].uniq.to_sentence_list}\n"
    end
    # str << "Terms used in profile: #{trh[:ref_ids].uniq.to_sentence_list}" if trh[:ref_ids].any?
    str << "Terms used in profile:\n" if trh[:ref_ids].any?
    trh[:ref_ids].uniq.sort.each {|term| str << "  #{term}\n"}
    str
  end
  
  def ref_id_checked
    if self.respond_to?(:type) || self.respond_to?(:system_type)
      r = _type_term
      str = r.reference_id if r
      str ||= "NO TERM PROVIDED"
      str
    else
      "N/A"
    end
  end
  
  def containment_report
    lines = ["#{name} (#{self.class.name.demodulize} => #{ref_id_checked})"]
    aggs = dim_obj_aggregations
    if aggs.any?
      aggs_lines = aggs.collect{ |agg| agg.channel_report }.flatten
      aggs_lines.each{ |al| lines << indent_for_containment_report(al) }
    end
    lines
  end
  
  def containment_report_textile_table_rows(stop_at_channels = true)
    textile = "| #{self.class.name.demodulize} | #{ref_id_checked} |\n".textile_indent_table_cell
    if stop_at_channels && self.is_a?(DIM::Medical::Channel)
      # do nothing at this point
    else
      aggs = dim_obj_aggregations
      if aggs.any?
        aggs_textile = aggs.collect{|agg| agg.containment_report_textile_table_rows}
        aggs_textile.each{|at| at.each_line{|line| textile << "#{line.textile_indent_table_cell}"}}
      end
    end
    textile
  end
  
  def channel_report
    # term_obj = _type_term
    if self.respond_to?(:type) || self.respond_to?(:system_type)
      term = ref_id_checked
      lines = ["Reference ID:  #{term}"]
    else
      lines = ["Class: #{self.class.name}"]
    end
    aggs = dim_obj_aggregations
    if aggs.any?
      aggs_lines = aggs.collect{|agg| agg.channel_report}.flatten
      aggs_lines.each{|al| lines << indent_for_channel_report(al)}
    end
    lines << "\n"
    lines
  end
  
  # build textile for a report that is styled after the one that ICS Generator provided
  def channel_report_textile
    # term_obj = _type_term
    if self.respond_to?(:type) || self.respond_to?(:system_type)
      term = ref_id_checked
      textile = "h3. #{simple_class_name}\n\nh5. #{term}\n\n"
    else
      textile = "h3. #{simple_class_name}\n\n"
    end
    aggs = dim_obj_aggregations
    if aggs.any?
      aggs_textiles = aggs.collect{|agg| agg.channel_report_textile}
      aggs_textiles.each{|at| textile << at}
    end
    textile << "\n\n"
    textile
  end
  
  # TODO methods to optional attributes.  alias mandatory_attribute_names to mandatory_attributes so that it makes for a nice tag in the xml.  Also must get these methods included in json_getters
  
  def indent_for_channel_report str
    "  " + str
  end
  
  def indent_for_containment_report str
    "  " + str
  end
  
  def parent_profile
    parent = composer
    # parent = pending_composer unless parent # TODO this works but apparently we shouldn't have to do this.  #composer should handle pending associations.  Will investigate SSA.
    if parent
      if parent.is_a? MyDevice::PCDProfile
        return parent
      else
        return parent.parent_profile
      end
    else
      nil
    end
  end
  
  def profile_summary
    if pprof = parent_profile
      pprof.summary_for_children
    else
      "Summary should be displayed after saving this object.  In the near future it will be displayed without needed to save this new object."
    end
  end
end # module DeviceObjectBehavior

# So far this all has to do with producing XML
module DeviceClassifierBehavior
  # TODO FIXME much of this is a (slightly modified) copy of do_xml_graph from profile_extensions.rb.  All of this stuff needs to be extracted into a single module.
  def snippet_file_name
    dt = display_type(false)
    file_name = self.class.name.demodulize
    file_name << "_#{dt}" if dt
    file_name
  end
  
  def download_full_xml_snippet
    {:content => do_xml_graph_snippet(:contextual => true), :filename => sanitize_filename(snippet_file_name) + '.xml', :type => 'xml'}
  end
  def do_xml_graph_snippet(opts = {})
    overrides = opts[:map] || MyDevice.default_overrides # if you wanted to merge these then do it beforehand and pass in as opts[:map]
    include_nulls = opts[:include_nulls] # deprecated?  not used AFAICT
    GraphState.clear
    GraphState.clear_config
    GraphState.output_type = :xml
    GraphState.classname_key = 'xsi:type'
    GraphState.getter_overrides_by_class = overrides
    GraphState.no_references!
    GraphState.getter_tests << attribute_usage_proc    
    
    xml_options = {:allow_duplicates => true}#, :tag_is_class => true}
    content = XML_Graph.pretty_generate(self, nil, 'http://prometheuscomputing.com/DIM/MyDevice', xml_options)
    content.gsub!(/_dim|_reserved/, "")
    if opts[:contextual]
      parent = self.composer
      parent_infos = parent.class.properties.values
      # HACK FIXME implementing classes are not reporting their implementors correctly (specifically DIM::Alert::AlertMonitor is not).  This code might be different if that weren't broken
      possible_roles = parent_infos.select do |info|
        type_klass = info[:class].to_const
        if type_klass.respond_to?(:interface?) && type_klass.interface?
          puts type_klass.implementors.inspect
          puts self
          puts self.id
          puts type_klass.implementors.include?(self.class)
        end
        self.is_a?(type_klass) || (type_klass.respond_to?(:interface?) && type_klass.interface? && type_klass.implementors.include?(self.class))
      end
      
      # Note that the same object could be playing more than one role and it is impossible to tell which one is relevant.  We just find the first one.
      actual_role_info = possible_roles.find do |info|
        parents_objs = Array(parent.send(info[:getter])).compact
        # TODO would it be better to use #is? here?
        found = parents_objs.find{ |obj| obj.class == self.class && obj.id == self.id }
        puts "FOUND" if found
        found
      end
      actual_role = actual_role_info[:getter].to_s if actual_role_info
      unless actual_role
        possible_roles.each do |info|
          x = parent.send(info[:getter])
          puts "#{info[:getter]} - #{x.inspect}"
        end
        # puts "Could not find role for #{self.class} among:\n #{parent_infos.collect{|v| "#{v[:getter]} -- #{v[:class]}\n"}.sort.join}"
      end
      actual_role ||= "ERROR_no_role_found"
      content.strip!
      content = content.gsub('<?xml version="1.0" encoding="UTF-8"?>', '')
      content = content.gsub(/xmlns=".+?"/, '').gsub(/xmlns:xsi=".+?"/, '')
      content = content.gsub(/^<#{self.class.name.demodulize}/, "<#{actual_role}")
      content = content.gsub(/\/#{self.class.name.demodulize}>$/, "/#{actual_role}>")
    end
    content
  end
  
  # Same as the one in profile_extensions, just refactored
  def attribute_usage_proc
    Proc.new do |instance, getter|
      if instance.respond_to? :used_getters
        ag = instance.asn1_getters
        ug = instance.used_getters
        if ag.include?(getter)
          if ug.include?(getter)
            true
          else
            false
          end
        else
          true
        end
      else
        true
      end
    end
    # Proc.new do |instance, getter|
    #   instance.respond_to?(:used_getters) ? (instance.asn1_getters.include?(getter) && instance.used_getters.include?(getter)) : true
    # end
  end
  
  private
  def sanitize_filename(filename)
    ret = filename.strip
    # NOTE: File.basename doesn't work right with Windows paths on Unix
    # get only the filename, not the whole path
    ret = ret.gsub(/^.*(\\|\/)/, '')
    # Strip out the non-ascii character
    ret = ret.gsub(/[^0-9A-Za-z.\-]/, '_')
    ret = ret.gsub(/_+/, '_')
  end
end
