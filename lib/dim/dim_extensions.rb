require_relative 'device_object_behavior'

MyDevice::INSTANTIABLE_DIM_CLASSES.each do |klass|
  
  # Change all compositions to aggregations so domain_objects are reusable
  # klass.properties.each_value do |info|
  #   info[:shared] = info.delete(:composition) if info[:composition]
  #   info[:opp_is_shared] = info.delete(:opp_is_composition) if info[:opp_is_composition]
  # end
  
  klass.class_eval do 
    include DeviceObjectBehavior
    include DeviceClassifierBehavior
    
    derived_attribute(:profile_summary, ::String)
    derived_attribute(:download_full_xml_snippet, ::Hash)
    
    add_attribute_setter(:cardinality) do |card|
      card = "1" if (card == "0" || card == "" || card == nil) # Should this happen? TODO
      return card = "1" if card =~ /[^0-9\-\.\* ]/
      card = card.gsub(/-+/, "..").gsub(/\.+/, "..").gsub(" ","") if card
      card
    end
    
    add_attribute_getter(:cardinality) do |cardinality|
      cardinality || "1"
    end
  end

  # setup hooks for proxied attributes of type OID-Type and TYPE
  proxied_properties = klass.properties.select{|k,v| v[:additional_data] && v[:additional_data][:prometheus] && v[:additional_data][:prometheus]["proxy_property"]  && v[:additional_data][:modeled_as] != :attribute_reciprocal}
  proxied_properties.each do |property_key, property_info|
    
    proxied_property_getter = property_info[:getter]
    proxied_property_setter = (proxied_property_getter.to_s + "=").to_sym
    proxy_getter = property_info[:additional_data][:prometheus]["proxy_property"].first.gsub("-", "_").to_sym
    
    # add more direct path to proxy_getter in metainfo
    klass.properties[property_key][:proxy_getter] = proxy_getter

    if property_info[:class].to_const <= DIM::CommonDataTypes::ASN1::OID_Type
      # puts "Adding assoc_adder to #{klass} -- proxy_getter is #{proxy_getter} -- proxied_property_setter is #{proxied_property_setter}"
      klass.send(:add_association_adder, proxy_getter) do |term|
        self.send(proxied_property_setter, term.term_code)
        term
      end
      klass.send(:add_association_remover, proxy_getter) do |term|
        self.send(proxied_property_setter, nil)
        term
      end
    elsif property_info[:class].to_const <= DIM::CommonDataTypes::ASN1::TYPE
      klass.send(:add_association_adder, proxy_getter) do |term|
        type = self.send(proxied_property_getter) || DIM::CommonDataTypes::ASN1::TYPE.create
        type.code = term.term_code
        type.code_term = term
        type.partition = term.partition
        type.save # is this necessary? TODO
        send(proxied_property_setter, type)
        if respond_to?(:allowed_enumerations) && (term.respond_to?(:collected_literals))
          if allowed_enumerations.empty?
            term.collected_literals.each{|t| add_allowed_enumeration t}
          end
        elsif respond_to?(:allowed_units) && (term.respond_to?(:collected_units))
          if allowed_units.empty?
            term.collected_units.each{|t| add_allowed_unit t}
          end
        end
        term
      end
      klass.send(:add_association_remover, proxy_getter) do |term|
        type = self.send(proxied_property_getter)
        self.send(proxied_property_setter, nil)
        type.destroy if type
        if respond_to?(:allowed_enumerations)
          remove_all_allowed_enumerations
        elsif respond_to?(:allowed_units)
          remove_all_allowed_units
        end
        term
      end
    else
      raise "Class #{klass} had a proxy: #{proxy_getter}, for a property: #{proxied_property_getter}, that had unexpected type: #{property_info.reject{|k,v| k == :methods}.inspect}"
    end
  end
end

module DIM
  SCHEMA_PREFIX = 'MyDevice' unless defined?(SCHEMA_PREFIX)
  SCHEMA_URI = "http://prometheuscomputing.com/schemas/DIM/MyDevice" unless defined?(SCHEMA_URI)
  
  module Medical    
    class Channel
      
      # pluralizing metric for some more oft used methods
      # alias_method :metrics, :metric
      # alias_method :metrics=, :metric=
      # alias_method :metrics_add, :metric_add
      # alias_method :metrics_count, :metric_count
      
      # build textile for a report that is styled after the one that ICS Generator provided
      def channel_report_textile
        # term_obj = _type_term
        term = ref_id_checked
        textile = "h3. #{simple_class_name}\n\nh5. #{term}\n\n"
        aggs = dim_obj_aggregations
        if aggs.any?
          textile << "table(tableborders){border:2px solid black; border-collapse:collapse}.\n"
          textile << "|_. Reference ID |_.  Units  |_.  Enumerations |\n"
          aggs_textiles = aggs.collect{|agg| agg.channel_report_textile}
          aggs_textiles.each{|at| textile << at}
        end
        textile << "\n\n"
        textile
      end
      
    end # class Channel
    
    class Metric

      # build string data for a report that is styled after the one that ICS Generator provided
      def channel_report
        term_obj = _type_term
        term = ref_id_checked
        term_class = term_obj.class if term_obj
        if term_class =~ /Metric$/
          linked_metrics_label = "Units: "
          linked_metrics = term_obj.units.collect{|u| u.reference_id}
        elsif term_class =~ /Enumeration$/
          linked_metrics_label = "Enumerations: "
          linked_metrics = term_obj.enums.collect{|u| u.reference_id}
        else
          if term_obj.respond_to?(:units) && term_obj.units && term_obj.units.any?
            linked_metrics_label = "Units: "
            linked_metrics = term_obj.units.collect{|u| u.reference_id}
          elsif term_obj.respond_to?(:enums) && term_obj.enums && term_obj.enums.any?
            linked_metrics_label = "Enumerations: "
            linked_metrics = term_obj.enums.collect{|u| u.reference_id}
          end
        end
        lines = ["Reference ID:  #{term}"]
        if term_class
          lines << "#{linked_metrics_label}:"
          if linked_metrics
            linked_metrics.each{|lm| lines << indent_for_channel_report(lm)}
          else
            lines << "*none found*"
          end
        end
        lines << "\n"
        aggs = dim_obj_aggregations
        if aggs.any?
          aggs_lines = aggs.collect{|agg| agg.channel_report}.flatten
          aggs_lines.each{|al| lines << indent_for_channel_report(al)}
        end
        lines << "\n"
        lines
      end
      
      def channel_report_textile
        term_obj = _type_term
        term = ref_id_checked
        term_code = term_obj.term_code if term_obj
        term_code ||= ""
        term_class = term_obj.class if term_obj
        if term_class.to_s =~ /Metric$/
          linked_metrics = term_obj.units.collect{|u| u.reference_id}
        elsif term_class.to_s =~ /Enumeration$/
          linked_metrics = term_obj.enums.collect{|u| u.reference_id}
        end
        textile = "|^.   #{term}(#{term_code})  |^.   " 
        if term_class =~ /Metric$/
          linked_metrics = term_obj.units.collect{|u| u.reference_id}
          linked_metrics.each{|lm| textile << " #{lm}<br> "}
          textile << "|^.   |\n"
        elsif term_class =~ /Enumeration$/
          textile << "|^.   "
          linked_metrics = term_obj.literals.collect{|u| u.reference_id}
          linked_metrics.each{|lm| textile << " #{lm}<br> "}
          textile << "|\n"
        else
          textile << "|^.   |\n"
        end
        # puts; puts "table row before: #{textile.inspect}"
        textile = textile.textile_pad_table_row
        # puts "table row after: #{textile.inspect}"
        aggs = dim_obj_aggregations # i.e. this metric is a complex metric and contains other metrics
        if aggs.any?
          aggs_textiles = aggs.collect{|agg| agg.channel_report_textile}
          aggs_textiles.each{|at| textile << at} # how to indent the entire bit here within a table? TODO
        end
        textile
      end
      
    end # class Metric
  end # module Medical
end # module DIM

# class AbsoluteTime < SEQUENCE
#    add_attribute_setter(:day) {|num|
#         raise(BusinessRules::RuleError, "AbsoluteTime.day should be non-negative") if num<0
#         num
#       }
#   add_attribute_setter(:hour) {|num|
#         raise(BusinessRules::RuleError, "AbsoluteTime.hour should be non-negative") if num<0
#         num
#       }
# end
