module Docx
  module Elements
    module Containers
      class Body
        include Container
        include Elements::Element
        
        def self.tag
          'body'
        end

        def initialize(node, document_properties = {})
          @node = node
          @properties_tag = 'sectPr'
          @document_properties = document_properties
          @font_size = @document_properties[:font_size]
        end
      end
    end
  end
  
  class Document
    def body
      @doc.xpath('//w:body').map{|node| parse_body_from node}.first
    end
    
    # generate Elements::Containers::Paragraph from paragraph XML node
    def parse_body_from(node)
      Elements::Containers::Body.new(node, document_properties)
    end
  end
end