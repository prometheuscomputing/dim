Htmltoword.configure do |config|
  # config.custom_templates_path = 'path_for_custom_templates'
  # If you modify this path, there should be a 'default.docx' file in there
  # config.default_templates_path = 'path_for_default_template'
  # If you modify this path, there should be a 'html_to_wordml.xslt' file in there
  config.default_xslt_path = relative '../standard/xslt/'
  # The use of additional custom xslt will come soon
  # config.custom_xslt_path = 'some_path'
end