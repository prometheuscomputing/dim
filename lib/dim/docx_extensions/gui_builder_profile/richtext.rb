module Gui_Builder_Profile
  class RichText
    def to_docx(opts = {})
      html = markup_language&.value == :HTML
      html ||= content.include?("<p>")
      if html
        Htmltoword::Document.snippet(content, opts)
      else
        nil
      end
    end
  end
end