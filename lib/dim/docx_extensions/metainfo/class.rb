module MetaInfo
  class Class
    def to_ieee_asn1# clause = nil      
      asn1_definition
    end
    
    def asn1_definition(opts = {})
      return nil unless qualified_name  =~ /ASN1/
      # <w:ind w:left="ASN1Indent"/>
      # <!-- <w:ind w:left="180"/> -->
      # <!-- <w:ind w:left="540"/> -->
      # <!-- <w:ind w:left="187"/> -->
      lines = [] # a single line ends up being several xml paragraph elements...
      desc = description&.safe_content
      if desc && !desc.strip.empty?
        lines << IEEE.asn1_data_type_comment(desc) 
      end
      apn = asn1_parent_name
      if apn == 'SEQUENCE OF'
        rm     = ruby_model
        ad     = rm.additional_data if rm
        pr     = ad[:prometheus] if ad
        answer = pr["sequence_of"] if pr
        answer = answer.first if answer && answer.any?
        puts "#{self.qualified_name} is a SEQUENCE OF what, exactly?" unless answer
        answer ||= "FIXME"
        lines << IEEE.asn1_line(180, IEEE.asn1_text_run("#{name} ::= SEQUENCE OF #{answer}"), false)
        lines.join
      else
        first_line_string = "#{name} ::= #{asn1_parent_name}"
        first_line_string << " #{constraint}" if constraint && !constraint.empty?
        if properties_count > 0
          run = IEEE.asn1_text_run("#{first_line_string} {")
          first_line = IEEE.asn1_line(180, run)
        else
          run = IEEE.asn1_text_run(first_line_string)
          first_line = IEEE.asn1_line(180, run, false)
        end
        lines << first_line
        pd = property_definitions
        lines << pd if pd
        if properties_count > 0
          lines << IEEE.asn1_line(187, IEEE.asn1_text_run("}"), false)
        end
        lines.join
      end
    end
    
    def property_definitions
      props = properties.reject { |p| p.bidirectional }
      if props.any?
# puts "#{qualified_name}:"
        longest_name_property = props.first
        props.each{|p| longest_name_property = p if TimesNewRoman.width_of(p.name) > TimesNewRoman.width_of(longest_name_property.name)}
        longest_property_effective_size = (StandardBuilder.format(:attribute_indent) + TimesNewRoman.width_of(longest_name_property.name) + StandardBuilder.format(:min_separation))
        name_tabs = (longest_property_effective_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      
        longest_type_property = props.first
        props.each do |p|
          next if p.type.name =~ /ANY.DEFINED.BY/
          longest_type_property = p if TimesNewRoman.width_of(p.type.name) > TimesNewRoman.width_of(longest_type_property.type.name)
        end
        longest_type_effective_size = (TimesNewRoman.width_of(longest_type_property.type.name) + StandardBuilder.format(:min_separation))
        type_tabs = (longest_type_effective_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      
        previous_identifier = nil
        lines = []

# puts "  longest_name_property: #{longest_name_property.name}; effective size: #{longest_property_effective_size}; tabs: #{name_tabs}"
# puts "  effective_size = (#{StandardBuilder.format(:attribute_indent)} + #{TimesNewRoman.width_of(longest_name_property.name)} + #{StandardBuilder.format(:min_separation)})"


        props.each_with_index do |p, i|
          # previous type is necessary in order to properly include it when the type of an attribute is ANY DEFINED BY
          last_property = (i +1) == props.count
          definition_lines, previous_identifier = p.asn1_definition(previous_identifier, last_property, name_tabs, type_tabs)
          lines += definition_lines
          # definition_lines.each do |definition|
          #   # puts "START";puts definition; puts "END"; puts
          #   lines << definition.gsub('ASN1Indent', '540')
          # end
        end
        lines.join
      else
        nil
      end
    end # property_definitions
  
  end # class Class
end