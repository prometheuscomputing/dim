module MetaInfo
  class Classifier
    def to_ieee
      raise "#to_ieee must be implemented in concrete class"
    end  
    def to_ieee_asn1
      raise "#to_ieee_asn1 must be implemented in concrete class"
    end

    def notifications_table(xref)
      rows = defined_events.collect { |de| de.notifications_table_row(:notifications_table) }.join
      opts = {
        :caption       => "—#{name} events",
        :table_format  => :notifications_table,
        :rows          => rows,
        :xref          => xref
      }
      IEEE.table(opts) + StandardBuilder.skip_line
    end

    def behaviors_table(xref, opts)
      opts ||= {}
      rows = defined_methods.collect { |dm| dm.behaviors_table_row(:behaviors_table, opts) }.join
      opts = {
        :caption       => "—#{name} instance methods",
        :table_format  => :behaviors_table,
        :rows          => rows,
        :xref          => xref
      }
      IEEE.table(opts) + StandardBuilder.skip_line
    end

    def attributes_table(xref, annotation_opts)
      asn1_properties = properties.select{|p| p.is_asn1?}
      return '' unless asn1_properties.any?
      rows = []
      after_table_notes = []
      rows = asn1_properties.collect { |p| p.attributes_table_row(:attributes_table) }.join
      opts = {
        :caption       => "—#{name} class attributes",
        :table_format  => :attributes_table,
        :rows          => rows,
        :xref          => xref
      }
      xml = IEEE.table(annotation_opts.merge(opts))
      # xml << after_table_notes if after_table_notes.any?
      xml << StandardBuilder.skip_line
      xml
    end
    
    def attribute_groups_table(xref)
      rows = attribute_groups.collect { |g| g.attribute_groups_table_row(:attribute_groups_table) }.join
      opts = {
        # TODO make sure this is actually missing in original XML
        # :table_indent => -24,
        # FIXME these were incorrectly using attributes table column widths,  make sure all is cool still
        :caption       => "—#{name} class attribute groups",
        :table_format  => :attribute_groups_table,
        :rows          => rows,
        :fixed_layout  => true,
        :xref          => xref
      }
      IEEE.table(opts) + StandardBuilder.skip_line
    end
  end # class Classifier
end