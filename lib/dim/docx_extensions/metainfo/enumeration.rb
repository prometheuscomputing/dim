module MetaInfo
  class Enumeration

    def to_ieee clause = nil
      asn1_definition clause # only because all of the enumerations in the DIM standard are ASN1.  They are either INT-U8/16/32 or BITS-8/16/32
    end
    def asn1_parent_name
      if sc = super_classifier
        sc.name
      else
        puts "#{self.qualified_name} had no parent name"
      end
    end
    def asn1_definition clause = nil
      # return '' # FIXME after you get classes working
      # return nil unless qualified_name  =~ /ASN1/
      # <w:ind w:left="ASN1Indent"/>
      # <!-- <w:ind w:left="180"/> -->
      # <!-- <w:ind w:left="540"/> -->
      # <!-- <w:ind w:left="187"/> -->
      lines = []
      desc = description&.safe_content
      if desc && !desc.strip.empty?
        lines << IEEE.asn1_data_type_comment(desc)
        # FIXME how do I get a blank line in here?
      end
      lits = literals
      declaration = "#{name} ::= #{asn1_parent_name}"
      declaration << " {" if lits.any?
      lines << IEEE.asn1_line(180, IEEE.asn1_text_run(declaration))
      if lits.any?
        longest_name_string = lits.first.name
        lits.each_with_index do |lit, i| 
          name_string = lit.name.dup
          name_string << ',' unless (i + 1 == lits.count)
          if TimesNewRoman.width_of(name_string) > TimesNewRoman.width_of(longest_name_string)
            longest_name_string = name_string
          end 
        end
        effective_longest_literal_ems = (TimesNewRoman.width_of(longest_name_string) + StandardBuilder.format(:attribute_indent) + StandardBuilder.format(:min_separation))
        ts   = StandardBuilder.format(:asn_1_tab_size)
        tabs = (effective_longest_literal_ems/ts).to_i + 1
  # puts StandardBuilder.format(:asn_1_tab_size)
  # puts TimesNewRoman.width_of("\t")
  # puts TimesNewRoman.width_of(" "*32)
  # puts TimesNewRoman.width_of("time-capab-real-time-clock(0),")
  # puts "#{name}: longest - #{longest_name_string}  (#{effective_longest_literal_ems}; tabs - #{tabs}); "
        lits.each_with_index do |lit, i|
          definition_lines = lit.asn1_definition(tabs, (i+1) == lits.count) # second param lets us know if this is the last one
          lines += definition_lines
          # definition_lines.each do |line|
          #   lines << line.gsub('ASN1Indent', '540')
          # end
        end
      end
      lines << IEEE.asn1_line(187, IEEE.asn1_text_run("}"), false) if lits.any?
      ret = StandardBuilder.remove_blank_lines(lines.join)
      # puts; puts ret; puts
      ret
      # StandardBuilder.minimize lines.join
    end
    
  end # class Enumeration
end
