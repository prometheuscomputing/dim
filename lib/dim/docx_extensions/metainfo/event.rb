module MetaInfo
  class Event
    
    
    def notifications_table_row(table_format)
      widths = StandardBuilder.format(table_format)[:column_widths]
      cells = [
        event_cell(widths[0]),
        mode_cell(widths[1]),
        id_cell(widths[2]),
        parameter_cell(widths[3]),
        result_cell(widths[4])
      ]
      cells = cells.join
      StandardBuilder.table_row(cells)
    end

    def cell(width, content_or_getter)
      if content_or_getter.is_a?(Symbol)
        content = send(content_or_getter)
      else
        content = content_or_getter
      end
      content ||= ""      
      lines = IEEE.paragraph(content, :table_line_subhead, :left_indent => 0)
      cell  = StandardBuilder.table_cell(width, lines)
    end
    def event_cell(width)
      cell(width, name || '')
    end
    def mode_cell(width)
      cell(width, mode&.value || '')
    end
    def id_cell(width)
      i_d = (event_id || '').gsub('_', "_\u200B")
      cell(width, i_d)
    end
    def parameter_cell(width)
      n = parameter_type&.name || ''
      n << ' (optional)' if parameter_multiplicity.to_s =~ /0\.\.1/
      cell(width, n)
    end
    def result_cell(width)
      cell(width, '—')
    end
  end # class Event
end