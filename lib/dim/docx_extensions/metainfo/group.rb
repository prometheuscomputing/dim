module MetaInfo
  class Group
    
    # FIXME does the standard ever define what the meaning of 'extensible attribute group' is?
    def group_name
      if name.nil? || name.empty?
        parent_group.group_name
      else
        name
      end
    end
    
    def attribute_groups_table_row(table_format)
      widths = StandardBuilder.format(table_format)[:column_widths]
      cells = [
        name_cell(widths[0]),
        ref_id_cell(widths[1]),
        elements_cell(widths[2])

        # elements_cell(widths[2]),
        # ref_id_cell(widths[1])
      ]
      # WASLINEBREAK
      cells = cells.join(" ")
      xml = StandardBuilder.table_row(cells)
      xml
    end
    
    def cell(width, content_or_getter)
      if content_or_getter.is_a?(Symbol)
        content = send(content_or_getter)
      else
        content = content_or_getter
      end
      content ||= ""
      paragraphs = IEEE.paragraph(content, :table_line_subhead, :left_indent => 0)
      cell  = StandardBuilder.table_cell(width, paragraphs)
      IEEE.remove_comments(cell)
    end
    
    def properly_sized_paragraphs(content, width, splitter = /(?<= |_|-)(?=[A-Za-a])/)
      max_line_size = StandardBuilder.max_cell_line_size(width)
      parts = content.split(splitter)
      lines = []
      # line is not static.  we take the first part and add to it unless the addition would result in a line that is too long.  the value of line shifts to being the next part throughout this process.  we are left at the end with line holding the last line, to which we have nothing left to add.
      line = parts.shift
      parts.each do |part|
        if TimesNewRoman.width_of(line) + TimesNewRoman.width_of(part) < max_line_size
          line += part
        else
          lines << String.new(line)
          line   = String.new(part)
        end
      end
      lines << line      
      paragraphs = lines.collect { |line| IEEE.paragraph(line, :table_line_subhead, :left_indent => 0) }
    end
    
    def name_cell(width)
      paragraphs = [IEEE.paragraph(group_name || "", :table_line_subhead, :left_indent => 0, :bold => true)]
      if extensible
        paragraphs << IEEE.paragraph("(extensible attribute group)", :table_line_subhead, :left_indent => 0)
      end  
      cell  = StandardBuilder.table_cell(width, paragraphs)
      IEEE.remove_comments(cell)
    end
    def ref_id_cell(width)
      cell(width, attribute_group_id || "Group ID is missing!")
    end
    def elements_cell(width)
      paragraphs = elements_cell_paragraphs(width)
      cell       = StandardBuilder.table_cell(width, paragraphs)
      IEEE.remove_comments(cell)
    end
    def elements_cell_paragraphs(width, opts = {})
      parent_paragraphs = parent_group ? parent_group.elements_cell_paragraphs(width, :parent_group => true) : nil
      paragraphs = [parent_paragraphs].compact
      if properties.any?
        paragraphs << IEEE.paragraph("from #{owner_class.name}: ", :table_line_subhead, :underline => true, :left_indent => 0)
        props      = properties.collect { |p| p.name.strip }.join(', ')
        paragraphs = paragraphs + properly_sized_paragraphs(props, width)
      end
      paragraphs
    end
  end # class Group
end