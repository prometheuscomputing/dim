module MetaInfo
  class Interface
    def to_ieee
      return asn1_definition if qualified_name  =~ /ASN1/

      xml
    end
    def asn1_definition
      # <w:ind w:left="ASN1Indent"/>
      # <!-- <w:ind w:left="180"/> -->
      # <!-- <w:ind w:left="540"/> -->
      # <!-- <w:ind w:left="187"/> -->
      lines = []
      if d = description
        lines << IEEE.asn1_comment_wrapper
        lines << IEEE.asn1_line(180, asn1_comment("#{d.content}"))
        lines << IEEE.asn1_comment_wrapper
        # lines << StandardBuilder.get_xml('asn1_line').gsub('ASN1Indent', '180').gsub('ASN1_RUNS', "\n") # FIXME how do I get a blank line in here?
      end
      lines << IEEE.asn1_line(180, IEEE.asn1_text_run("#{name} ::= CHOICE {"))
      implementing_classes.each do |ic|
        lines << IEEE.asn1_line(540, IEEE.asn1_text_run("#{ic.name}"))
      end
      lines << IEEE.asn1_line(187, IEEE.asn1_text_run("}"), true)
      lines.join
    end
  end
end
