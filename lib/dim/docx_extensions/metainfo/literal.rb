module MetaInfo
  class Literal
    # "\t"    => 0.89111328125
    def asn1_definition(long_tabs, is_last)
      
      runs = []
      lit_name = "#{name}"
      lit_name << "," unless is_last
      name_run = IEEE.asn1_text_run(lit_name, :preserve_space => true)
      runs << name_run
      
      dc = description&.safe_content
      remaining_comment = nil
      if dc && !dc.empty?
        effective_literal_ems = StandardBuilder.format(:attribute_indent) + TimesNewRoman.width_of(lit_name) + StandardBuilder.format(:min_separation)
        literal_tabs = (effective_literal_ems / StandardBuilder.format(:asn_1_tab_size)).ceil
        additional_tabs        = long_tabs - literal_tabs
# puts_green "  #{lit_name}\n  long_tabs - #{long_tabs}\n  ems - #{effective_literal_ems}\n  literal_tabs - #{literal_tabs}\n  additional_tabs - #{additional_tabs}\n  additional_tabs.ceil - #{additional_tabs.ceil}\n\n"
        runs << IEEE.asn1_tab
        # FIXME Don't know if this should be to_i or ceiling or floor????
        additional_tabs.ceil.times {runs << IEEE.asn1_tab}
        comment, remaining_comment = IEEE.asn1_attribute_comment(dc, long_tabs)      
        runs << comment
      end
      lines = [IEEE.asn1_line(540, runs)]
      if remaining_comment && remaining_comment.any?
        remaining_comment.each do |rc|
          remaining_comment_runs = []
          (literal_tabs + additional_tabs.ceil).times {remaining_comment_runs << IEEE.asn1_tab}
          remaining_comment_runs << rc
          # FIXME
          lines << IEEE.asn1_line(540, remaining_comment_runs)
        end
      end
      lines
    end
  end # class Literal
end