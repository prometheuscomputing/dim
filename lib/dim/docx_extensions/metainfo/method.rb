module MetaInfo
  class Method
    
    
    def behaviors_table_row(table_format, opts = {})
      annotation = opts.dig(:annotation, name) || {}
      widths = StandardBuilder.format(table_format)[:column_widths]
      cells = [
        action_cell(widths[0], annotation),
        mode_cell(widths[1], annotation),
        id_cell(widths[2], annotation),
        parameter_cell(widths[3], annotation),
        result_cell(widths[4], annotation)
      ]
      cells = cells.join
      StandardBuilder.table_row(cells)
    end

    def cell(width, content_or_getter, annotation = nil)
      if content_or_getter.is_a?(Symbol)
        content = send(content_or_getter)
      else
        content = content_or_getter
      end
      content ||= ""
      
      lines = [IEEE.paragraph(content, :table_line_subhead, :left_indent => 0)]
      if annotation && !annotation.empty?
        lines << IEEE.paragraph(annotation, :table_line_subhead, :left_indent => 0)
      end
      cell  = StandardBuilder.table_cell(width, lines)
    end
    def action_cell(width, opts = {})
      cell(width, name || '', opts["action"])
    end
    def mode_cell(width, opts = {})
      cell(width, mode&.value || '', opts["mode"])
    end
    def id_cell(width, opts = {})
      i_d = (action_id || '').gsub('_', "_\u200B")
      cell(width, i_d, opts["action_id"])
    end
    def parameter_cell(width, opts = {})
      n = parameter_type&.name || ''
      n << ' (optional)' if parameter_multiplicity.to_s =~ /0\.\.1/
      cell(width, n, opts[:action_parameter])
    end
    def result_cell(width, opts = {})
      n = result_type&.name || '—'
      n << ' (optional)' if result_multiplicity.to_s =~ /0\.\.1/
      cell(width, n, opts["action_result"])
    end
  end # class Method
end