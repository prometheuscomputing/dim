module MetaInfo
  class Property
    # IDENTIFIER_MAX_LENGTH     = 30
    # BEFORE_COMMENT_MAX_LENGTH = 60
    
    def attributes_table_row(table_format)
      widths = StandardBuilder.format(table_format)[:column_widths]
      cells = [
        name_cell(widths[0]),
        ref_id_cell(widths[1]),
        type_cell(widths[2]),
        remark_cell(widths[3]),
        qualifier_cell(widths[4])
      ]
      cells = cells.join("\n  ")
      StandardBuilder.table_row(cells)
    end
    # def test_cell num
    #   cell = StandardBuilder.get_xml('attributes_table_cell').gsub('CellLines', test_line).gsub('CellWidth', StandardBuilder::ATCW[num].to_s )
    # end
    # def test_line
    #   StandardBuilder.get_xml('attributes_table_cell')
    # end
    def cell(width, content_or_getter, type = nil)
      # We will have to split on size to get lines FIXME
      # We will need to go one char at a time and add up the widths until we are either at the end of the word or we come to the maximum allowed length of chars.  If we come to the maximum allowed length of chars then we will need to back up to the last allowable line-break character and split there.  We also may have to add hyphens or underscores.
  
      # Just for now...*I think* that 12pt font would mean 240twips per letter
      if content_or_getter.is_a?(Symbol)
        content = send(content_or_getter)
      else
        content = content_or_getter
      end
      
      content ||= ""      
      # size    = (width - StandardBuilder::CELL_BUFFER)/StandardBuilder::TWIPS_ADJUSTER
      # parts   = content.scan(/.{1,#{size}}/)
      # parts   = [''] if parts.empty?
      # lines   = parts.collect{|l| StandardBuilder.get_xml('attributes_table_cell').gsub('LineValue', l)}.join
      # FIXME testing what happens if I don't split the lines.  This is just a single line even though the variable is called 'lines'
      # Oddly enough, this is working here but fails to work for the attribute groups refid cell.  The attribute groups refid cell is wrapping in the middle of a word in the rendered docx
      paragraph    = IEEE.paragraph(content, :table_line_subhead, :left_indent => 0)
      cell_content = add_footnote_to_cell_paragraph(paragraph, type)
      cell         = StandardBuilder.remove_blank_lines(StandardBuilder.table_cell(width, cell_content))
      cell
    end
    def name_cell(width)
      cell(width, :name, :name)
    end
    def ref_id_cell(width)
      aid = attribute_id || ""
      # Holy smoke this is a gross hack...Alternative is to build a proper way to handle this but is it worth it to do that just to handle a single instance of this situation?
      aid = aid.strip.gsub(' ', '').gsub('_', "_\u200B") unless name =~ /Context Attributes/
      cell(width, aid, :refid)
    end
    def type_cell(width)
      content = type.name if type
      # Also another rather ugly hack...
      if footnote_content.to_s =~ /^AttributeType/
        content = footnote_content.slice(/(?<=AttributeType =>).*/).strip
      end
      cell(width, content, :attributetype)
    end
    def remark_cell(width)
      remark = description.content if description
      cell(width, remark, :remark)
    end
    def qualifier_cell(width)
      v = optionality.value if optionality      
      v = v[0].upcase if v && v[0]
      cell(width, v, :qualifier)
    end
    
    def add_footnote_to_cell_paragraph(paragraph, cell_type = nil)
      footnote_content = footnote&.safe_content&.strip
      return paragraph if footnote_content.to_s =~ /(AttributeType) =>/
      if footnote_content && !footnote_content.empty?
        directive = footnote_content.slice!(/.+ => /)
        directive = directive&.delete(' :=>')&.strip&.downcase.to_sym
        if directive == cell_type
          raise "#{self.class}.footnote_markup_language.value must be 'Plain'!  Please see Property[#{id}]" unless footnote_markup_language.value == 'Plain'
          opts = {:style => IEEE.paragraph_style(:paragraph), :left_indent => 187, :spacing_after => 0 }
          footnote_symbol_run = StandardBuilder.add_table_footnote(footnote_content, opts)
          paragraph = paragraph.gsub(/(?<=<\/w:r>)(\s*)(?=<\/w:p>)/m, footnote_symbol_run)
        else
          paragraph
        end
      else
        paragraph
      end
    end
    
    def asn1_definition(previous_identifier = nil, last_property, long_name_tabs, long_type_tabs)

      effective_name_size = StandardBuilder.format(:attribute_indent) + TimesNewRoman.width_of(name) + StandardBuilder.format(:min_separation)
      name_tabs = (effective_name_size/StandardBuilder.format(:asn_1_tab_size)).to_i + 1
      additional_tabs = long_name_tabs - name_tabs
      # puts "#{name}: effective size: #{effective_name_size}; additional_tabs = #{additional_tabs}"
      # puts "         effective size = #{StandardBuilder.format(:attribute_indent)} + #{TimesNewRoman.width_of(name)} +#{StandardBuilder.format(:min_separation)}"

      runs = []
      runs << IEEE.asn1_text_run(name, :preserve_space => true)
      runs << IEEE.asn1_tab
      additional_tabs.times {runs << IEEE.asn1_tab}
      
      type_name       = type.name
      type_name = type_name + "  #{previous_identifier}" if type_name == 'ANY DEFINED BY'
      type_name = "[#{context_specific_tag}] " + type_name if context_specific_tag
      type_name << "," unless last_property
      runs << IEEE.asn1_text_run(type_name)
      dc = description_content
      remaining_comment = nil
      if dc && !dc.empty?
        longchars_2     = long_type_tabs * StandardBuilder.format(:asn_1_tab_size)
        total_tabs      = long_name_tabs + long_type_tabs
        additional_tabs = ((longchars_2 - TimesNewRoman.width_of(type_name))/StandardBuilder.format(:asn_1_tab_size))
        runs << IEEE.asn1_tab
        additional_tabs.to_i.times {runs << IEEE.asn1_tab}
        comment, remaining_comment = IEEE.asn1_attribute_comment(dc, total_tabs)        
        runs << comment
      end
      lines = [IEEE.asn1_line(540, runs)]
      if remaining_comment && remaining_comment.any?
        remaining_comment.each do |rc|
          remaining_comment_runs = []
          total_tabs.times {remaining_comment_runs << IEEE.asn1_tab}
          remaining_comment_runs << rc
          lines << IEEE.asn1_line(540, remaining_comment_runs)
        end
      end
      [lines, name]
    end
  end # class Property
end