module Standard  
  class AbbreviationSection
    def to_ieee(opts, depth = 1)
      xml = IEEE.clause(title, depth = 1)
      abbreviations.each { |item| xml = xml + item.to_ieee(opts, depth + 1) }
      xml
    end
  end
  
  class Abbreviation
    def to_ieee(opts, depth)
      xml = IEEE.template('abbreviation')
      xml = xml.gsub('SYMBOL', symbol).gsub('MEANING', meaning)
    end
  end
end
