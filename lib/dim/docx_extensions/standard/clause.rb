module Standard    
  class Clause
    def to_ieee(opts, depth = 1)
      # FIXME YUX this sucks.  undo it once you work out the revised model and SSA.  Codename YUX.
      ghost_clause = content.find { |c| c.respond_to?(:render_as) && c.render_as&.value&.to_s&.match(/unnumbered/) }
      placeholder =  content.find { |c| c.respond_to?(:render_as) && c.render_as&.value&.to_s&.match(/placeholder/) }
      if placeholder || content.empty?
        return ''
      elsif ghost_clause
        xml = ''
      else      
        xml = IEEE.clause(title, depth)
      end
      # FIXME end of YUX hack
      
      content.each do |c|
        # puts "*"*10
        # puts c.pretty_inspect
        xml = xml + c.to_ieee(opts.dup, depth + 1)
      end
      xml
    end
  end
end
