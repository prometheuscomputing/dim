(Standard::ClauseContent.implementors + Standard::Content.implementors).uniq.each do |imp|
  imp.class_eval do
    derived_attribute(:download_docx, ::Hash)
    def download_docx
      docx = temp_standard { |temp| File.read(temp.temp_docx) }
      {:content => docx, :filename => docx_filename + '.docx', :type => 'application/msword'}
    end
    
    derived_attribute(:test_docx, ::NilClass)
    def test_docx
      temp_standard { |temp| puts_green temp.inspect; temp.make_docx }
      nil
    end
    
    def temp_standard &block
      ChangeTracker.start
      temp = Standard::IEEEStandard.new(:title => docx_filename)
      temp.content_add(self)
      ret = yield(temp)
      temp.destroy
      ChangeTracker.commit
      ChangeTracker.start unless ChangeTracker.started? # because #process_action_button fails like a POS
      ret
    end
    
    def docx_filename
      # FIXME this is sloppy
      t = respond_to?(:title) ? title : nil
      fn = (t || "#{self.class.name.demodulize}[#{self.id}]").strip
      # essentially copied from #sanitize_filename FIXME make it DRY
      fn = fn.gsub(/^.*(\\|\/)/, '')
      # Strip out the non-ascii character
      fn = fn.gsub(/[^0-9A-Za-z.\-]/, '_')
      fn = fn.gsub(/_+/, '_')
    end
  end
end
