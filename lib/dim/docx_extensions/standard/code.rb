module Standard  
  class Code
    def to_ieee(opts, depth = 0)
      case render_as&.value
      when 'ASN.1'
        xml = ''
        comment = code&.content
        if comment && !comment.empty?
          xml << IEEE.asn1_data_type_comment(comment) + StandardBuilder.skip_line
        end
        me  = model_element
        xml << me.asn1_definition if me
        xml
      else
        puts "Standard::Code.render_as is #{render_as}"
        IEEE.paragraph(code&.content)
      end
    end
  end
end