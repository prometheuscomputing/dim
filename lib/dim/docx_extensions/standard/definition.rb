module Standard  
  class DefinitionSection
    def to_ieee(opts, depth = 1)
      xml = IEEE.clause(title, depth = 1)
      content.each do |c|
        xml = xml + c.to_ieee(opts, depth + 1)
      end
      definitions.each { |item| xml = xml + item.to_ieee(opts, depth + 1) }
      xml
    end
  end
  class Definition
    def to_ieee(opts, depth)
      runs = []
      runs << StandardBuilder.text_run(term, :bold => true)
      runs << StandardBuilder.text_run(definition)
      xml = IEEE.paragraph(runs)
    end
  end
end