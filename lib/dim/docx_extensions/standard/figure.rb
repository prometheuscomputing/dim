module Standard  
  class Figure
    def to_ieee(opts, depth = nil)
      xml = ''
      image = source_file
      if image
        # StandardBuilder.add_media(image)
        r_id = StandardBuilder.next_relationship_id.to_s
        # put the image file in the media folder -- HOW??
        # put the rels_paragraph in wor/_rels/document.rels.xml
        rels_paragraph = IEEE.template('image_relationship').gsub('_FILENAME_', image.filename).gsub('_R_ID_VALUE_', r_id)
        StandardBuilder.add_relationship(rels_paragraph)
        xml  = IEEE.template('image').gsub('_R_ID_VALUE_', r_id)
      end
      caption_paragraph = IEEE.template('image_caption').gsub('_CAPTION_', caption)
      xml = xml + caption_paragraph
    end
  end
end