module Standard  
  class Footnote
    # sends the footnote to StandardBuilder which will insert it into footnotes.xml and returns a run with a footnote reference in it to the caller
    def to_ieee(opts = {})
      footnote_id        = StandardBuilder.next_footnote_id.to_s
      footnote_from_html = content&.to_docx
      if footnote_from_html
        # TODO see docx/word/_rels/footnotes.xml.rels to see how to start getting these hyperlinks to actually work
        footnote_from_html = footnote_from_html.gsub(/<w:hyperlink r:id=".*">/, '<w:hyperlink>')
        # This is a rather gnarly way to get the runs out of the converted HTML so we can put them into the contextually correct XML
        footnote_runs = footnote_from_html.slice(/(<w:r>.*(<w:t.*<\/w:t>).*<\/w:r>)/m)
      else
        footnote_runs = IEEE.template('footnote_content_run').gsub('_FOOTNOTE_TEXT_', content&.safe_content)
      end
      footnote_xml = IEEE.template('footnote').gsub('_FOOTNOTE_ID_', footnote_id).gsub('_FOOTNOTE_RUNS_', footnote_runs)
      if footnote_from_html
        # puts_cyan footnote_xml
        # puts "*"*44
        # return ''
      end
      StandardBuilder.add_footnote(footnote_xml)
      footnote_reference_xml = IEEE.template('footnote_reference').gsub('_FOOTNOTE_ID_', footnote_id)
      footnote_reference_xml
    end
  end
end