module Standard  
  class NormativeReferenceSection
    def to_ieee(opts, depth = 1)
      xml = IEEE.clause(title, depth = 1)
      content.each do |c|
        xml = xml + c.to_ieee(opts, depth + 1)
      end
      normative_references.each { |item| xml = xml + item.to_ieee(opts, depth + 1) }
      xml
    end
  end
  class NormativeReference
    def to_ieee(opts, depth)
      desc  = description&.safe_content || ''
      nr = ["#{source}, #{desc}"] + footnotes.collect { |f| f.to_ieee }
      IEEE.paragraph(nr.compact)
    end
  end
end
