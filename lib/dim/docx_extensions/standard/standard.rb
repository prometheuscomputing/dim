require 'zip/filesystem'
module Standard  
  class IEEEStandard
    # Interesting --     <w:lastRenderedPageBreak/>
    attr_accessor :rendering_options
    
    derived_attribute(:download_docx, ::Hash)
    def download_docx
      {:content => File.read(temp_docx), :filename => draft_name + '.docx', :type => 'application/msword'}
    end
    
    def draft_name
      "11073_10201_generated_draft_#{Time.now.strftime("%F_%H%M%S")}"
    end
    
    def make_docx(options = {})
      @rendering_options = options
      IEEE.build(self, rendering_options)
    end
    
    # require absolute path for filename
    def save_docx(options = {})
      options[:filename] ||= File.expand_path("~/Desktop/DIM/#{draft_name}.docx")
      make_docx(options)
      # options[:filename]
    end
    
    def open_docx(options = {})
      file = save_docx(options)
      system "open #{file}"
    end
    
    def temp_docx(options = {})
      options[:tempfile] = true
      save_docx(options)
    end
  
    def to_ieee
      xml  = IEEE.template('main')
      body = to_ieee_body
      # puts; puts_cyan body; puts
      StandardBuilder.minimize(xml.gsub('BODY', body))
    end
    
    def to_ieee_body
      body = ''
      # body = body + boilerplate_content
      body = body + object_content
      # body = body + IEEE.boilerplate('original_service_model')
      # body = body + IEEE.boilerplate('original_mdib')
      # body = body + IEEE.boilerplate('original_conformance_model')
      xml = IEEE.template('body_empty').gsub('_BODY_CONTENT_', body)
      StandardBuilder.remove_blank_lines(xml)
    end
    
    def object_content
      content.collect { |item| item.to_ieee(rendering_options) }.join
    end
    
    def boilerplate_content
      xml = ''
      xml = xml + IEEE.boilerplate('original_title_page')
      xml = xml + IEEE.boilerplate('original_abstract')
      xml = xml + IEEE.boilerplate('original_keywords')
      xml = xml + publication_reference
      xml = xml + IEEE.boilerplate('original_disclaimer')
      xml = xml + IEEE.boilerplate('original_introduction')
      xml = xml + IEEE.boilerplate('original_user_notice')
      xml = xml + IEEE.boilerplate('original_laws')
      xml = xml + IEEE.boilerplate('original_copyrights')
      xml = xml + IEEE.boilerplate('original_updating')
      xml = xml + IEEE.boilerplate('original_errata')
      xml = xml + IEEE.boilerplate('original_interpretations')
      xml = xml + IEEE.boilerplate('original_patents')
      xml = xml + IEEE.boilerplate('original_participants')
      xml = xml + toc
      xml = xml + IEEE.boilerplate('original_title')
      xml
    end
    
    def publication_reference
      StandardBuilder.add_footnote(IEEE.boilerplate('publication_footnote'))
      StandardBuilder.use_footnote_id(1)
      IEEE.boilerplate('publication_footnote_reference')
    end

    def toc
      return ''
      IEEE.boilerplate('original_contents')
    end
  end
end