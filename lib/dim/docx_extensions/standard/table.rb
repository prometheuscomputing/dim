require 'json'
module Standard  
  class Table
    def to_ieee(opts, depth = 0)
      me = model_element
      unless me
        container_model_elements = containers.collect { |c| c.model_element }.compact.uniq
        me = container_model_elements.first if container_model_elements.count == 1
      end
      
      # This is where the HACK begins...Getting extra stuff into the table cells.
      rc = remarks&.content&.gsub(/#.*$/, '')
      annotations = JSON.parse(rc) if rc && !rc.empty?
      opts = {:annotation => annotations}
      
      case render_as&.value
      when 'attributes table'
        raise "no model element for Table - #{parent_titles}" unless me
        me.attributes_table(xref, opts)
      when 'attribute groups table'
        raise "no model element for Table - #{parent_titles}" unless me
        me.attribute_groups_table(xref)
      when 'behaviors'
        raise "no model element for Table - #{parent_titles}" unless me        
        me.behaviors_table(xref, opts)
      when 'notifications'
        unless me && me.kind_of?(MetaInfo::Classifier)
          puts me.inspect
          raise "no model element for Table[#{self.id}] - #{parent_titles}"
        end
        me.notifications_table(xref)
      else
        IEEE.paragraph(title)
      end
    end
  end
end