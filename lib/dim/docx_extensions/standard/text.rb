module Standard  
  class Text
    def to_ieee(opts, depth = 0)
      return '' if render_as&.value == 'placeholder'
      
      if source_file && file_usage&.value == 'file only'
        xml = source_file.binary_data.data
        # puts xml.inspect
        return xml
      end
          
      case render_as&.value
      when 'class definition'
        xml = StandardBuilder.get_xml('class')
        me  = model_element
        raise "no model element for Text - #{parent_titles}" unless me
        xml = xml.gsub('CLASS_TITLE', me.name)
        dc = me.description&.safe_content
        if dc.nil? || dc.empty?
          xml = xml.gsub('DESCRIPTION', "--")
        else
          xml = xml.gsub('DESCRIPTION', '"' + dc + '"')
        end        
        xml = xml.gsub('PARENT_CLASS', me.super_classifier ? me.super_classifier.name : '--')
        xml = xml.gsub('CHILD_CLASSES', me.sub_classifiers.any? ? me.sub_classifiers.collect{|sc| sc.name}.join(', ') : '--')
        xml = xml.gsub('NAME_BINDING', me.name_binding ? me.name_binding : '--')
        xml = xml.gsub('REGISTERED_AS', me.registered_as ? me.registered_as : 'missing ref_id') # FIXME missing ref_id
      when 'interface definition'
        xml = StandardBuilder.get_xml('interface')
        me  = model_element
        raise "no model element for Text - #{parent_titles}" unless me
        xml = xml.gsub('_TITLE_', me.name)
        dc = me.description&.safe_content
        dc = "--" if dc.nil? || dc.empty?
        xml = xml.gsub('_DESCRIPTION_', dc)
        xml = xml.gsub('_PARENT_', me.super_classifier ? me.super_classifier.name : '--')
        xml = xml.gsub('_CHILDREN_', me.sub_classifiers.any? ? me.sub_classifiers.collect{|sc| sc.name}.join(', ') : '--')
        xml = xml.gsub('_IMPLEMENTORS_', me.implementing_classes.any? ? me.implementing_classes.collect{|sc| sc.name}.join(', ') : '--')
      when 'editorial note'
        _to_ieee(opts.merge({:highlight_yellow => true}))
      else
        _to_ieee(opts)
      end
    end
    
    def _to_ieee(opts)
      style = opts[:style] || :paragraph
      # the #to_docx method returns nil if the content isn't HTML and it therefore can't create XML from it.  If the content is actually XML and not HTML then we might have real problems...        
      xml = (opts[:content] || content)&.to_docx(opts)
      if xml
        if opts[:preface]
          # We stuff the preface into the content and then reconvert it.  This is a bit silly...
          prefaced_content = content.dup
          prefaced_content.content.sub!('<p>', "<p>#{opts.delete(:preface)}")
          xml = prefaced_content.to_docx
        end
        xml = StandardBuilder.process_references(xml)
        ieee_style = IEEE.paragraph_style(style)
        xml = xml.gsub(IEEE.paragraph_style(:paragraph), ieee_style) unless style == :paragraph

        # now merge extra styling with the styling already there (crazy stuff, right?)
        p_properties = StandardBuilder.paragraph_properties(opts)
        if !p_properties.empty?
          proper_indent = " " * xml.slice(/^\s*#{ieee_style}/).rstrip.slice(/\s*/).length
          # dicey?..
          p_properties = p_properties.split("\n")[1..-2].collect{ |l| proper_indent + l.strip }.join
          xml = xml.gsub(ieee_style, ieee_style + "\n" + p_properties)
        end
        
        # puts_red style; puts_magenta xml; puts
        # now we insert the footnotes.  A bit hamfisted, eh?
        if respond_to?(:footnotes) && footnotes.any?
          fns = footnotes.collect { |f| f.to_ieee }.join("\n")
          xml = xml.gsub(/(?<=<\/w:r>)(\s*)(?=<\/w:p>)/m, fns)
        end
        xml = StandardBuilder.inject_run_properties(xml, opts)
      else
        text_content = content&.safe_content
        # FIXME should we just ignore this if it is nil?
        text_content ||= "FIXME: text content for #{containers.collect { |c| c.title }.inspect}"
        text_content = opts.delete(:preface) + text_content if opts[:preface]
        runs = [text_content] + footnotes.collect { |f| f.to_ieee }
        xml = StandardBuilder.remove_blank_lines(IEEE.paragraph(runs, style, :preserve_space => true))
        # puts_red style; puts_cyan xml; puts
        xml = StandardBuilder.process_references(xml)
      end
      xml
    end
  end
  
  class Note
    def to_ieee(opts, depth = 0)
      opts[:style] ||= :note
      # if opts[:style] == :multiple_notes
      #   list = opts[:parent]
      #   raise unless list && list.is_a?(Standard::List)
      # else
      #   opts[:preface] = 'NOTE—'
      # end
      nt = note_type&.value&.gsub(/note /, '')&.strip
      preface = 'NOTE'
      preface << " #{nt}" if nt
      preface << "—"      
      opts[:preface] = preface if opts[:style] == :note
      _to_ieee(opts)
    end
  end
  
  class Example
    def to_ieee(opts, depth = 0)
      opts[:left_indent] = 360 # value from 2010 DIM Draft
      example_content = content.content
      example_content = '<p></p>' if example_content.empty?
      ex_label = 'Example'
      ex_num = render_as&.value
      if ex_num && ex_num =~ /example \d+/
        ex_num = ex_num.gsub(/example /, '')&.strip
        ex_label << " #{ex_num}"
      end
      if render_as&.value == "example content indented"
        rt = Gui_Builder_Profile::RichText.new(:content => "<p><b>#{ex_label}:</b></p>")
        opts[:spacing_after] = 0
        opts[:content] = rt
        example_label = _to_ieee(opts)
        opts[:left_indent] = 720 # value from 2010 DIM Draft
        opts[:content] = nil
        example_text = _to_ieee(opts)
        # remove last occurrence
        example_text.gsub!(/<w:spacing w:after="0"\/>(?!.*w:spacing w:after=)/m, '')
        example_label + example_text
      else
        example_content.sub!('<p>', "<p><b>#{ex_label}:</b> ")
        rt = Gui_Builder_Profile::RichText.new(:content => example_content)
        opts[:content] = rt
        _to_ieee(opts)
      end
    end
  end
  
  class List
    def to_ieee(opts, depth = 0)
      case type&.value
      when 'unordered list'
        opts = opts.merge({:style => :ul})
      when 'note list'
        opts = opts.merge({:style => :multiple_notes, :parent => self})
      when 'numbered list level 1'
        opts = opts.merge({:style => :li1})
      else
        opts = opts.merge({:style => :ul})
      end
      list_items.collect { |li| li.to_ieee(opts) }.join
    end
  end
end