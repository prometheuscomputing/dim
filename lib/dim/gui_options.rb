tree_view_ignored_classes = DIM.classes(:no_imports => true).select{|klass| klass.to_s =~ /::ASN1::/}.collect{|klass| klass.to_s}
# *PHD*
# tree_view_ignored_classes += PHD.classes(:no_imports => true).select{|klass| klass.to_s =~ /::ASN1::/}.collect{|klass| klass.to_s}
tree_view_ignored_classes += IEEE20101.classes(:no_imports => true).select{|klass| klass.to_s =~ /::ASN1::/}.collect{|klass| klass.to_s}
tree_view_ignored_classes += Gui_Builder_Profile.classes(:no_imports => true).collect{|klass| klass.to_s}
tree_view_ignored_classes += ["String", "Integer"]
Gui.option(:tree_view_options, {:ignored_classes => tree_view_ignored_classes})