# Really no way around this one since SSA does not consider attributes to be compositions
# require_relative 'attributes_are_compositions'
# module Sequel
#   class Model
#     def getter_used_by_composer
#       comp = composer
#       possible_getters = comp.class.compositions.select{|c| c[:class].to_const == self.class}.collect{|c| c[:getter]}
#       # puts "possible_getters: #{possible_getters}"
#       answer = nil
#       possible_getters.each do |getter|
#         gotten = comp.send(getter)
#         next unless gotten
#         if gotten.is_a? Array
#           # answer = getter if gotten.select{|g| g.id == self.id && g.class == self.class}.any?
#           answer = getter if gotten.member?(self)
#         else
#           # answer = getter if gotten.id == self.id && gotten.class == self.class
#           answer = getter if gotten == self
#         end
#         break if answer
#       end
#       answer || alternate_getter_used_by_composer
#     end
#     def alternate_getter_used_by_composer
#       comp = composer
#       composer_class = comp.class
#       # puts "composer class for #{self.class}: #{composer_class}"
#       possible_composers = self.class.composers.select{|c| x = c[:class].to_const; puts x; x == composer_class} # there should be just one, right?
#       possible_composer_opp_getters = possible_composers.collect{|c| c[:opp_getter]}
#       # puts "possible_composer_opp_getters: #{possible_composer_opp_getters}"
#       answer = nil
#       possible_composer_opp_getters.each do |getter|
#         gotten = comp.send(getter)
#         next unless gotten
#         if gotten.is_a? Array
#           # answer = getter if gotten.select{|g| g.id == self.id && g.class == self.class}.any?
#           answer = getter if gotten.member?(self)
#         else
#           # answer = getter if gotten.id == self.id && gotten.class == self.class
#           answer = getter if gotten == self
#         end
#         break if answer
#       end
#       answer
#     end
#   end
# end