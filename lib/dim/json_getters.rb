require 'common'
#  FIXME this stuff ought to be namespaced...


# FIXME FIXME FIXME FIXME FIXME FIXME FIXME FIXME write code to go through all of the asn1_getters (of every top class) and make sure that we can match a MetaInfo::Property to a getter for every getter (see below). if that works then set an instance variable on each AttributeInclusion that tells what its getter is.

# Includes PHD classes (because that is really a DIM as well...)
#*PHD*
all_dim_classes = DIM.classes(:ignore_class_namespaces => true)# + PHD.classes(:ignore_class_namespaces => true)
#  FIXME why were we rejecting primitives?  The XSD needs them.  Is there a reason NOT to have them in the mapping used to generate XML?
ieee    = (IEEE20101.classes(:ignore_class_namespaces => true) + IEEE20101.classes(:ignore_class_namespaces => true).each{|c| c.children}).flatten.uniq
asn1    = all_dim_classes.select{|klass| klass.to_s =~ /ASN1/} 
klasses = ieee+all_dim_classes
# initially populate with what we want in there
klasses.each do |klass|
  # next unless klass < Sequel::Model
  getter_hash = {}
  association_composition_getters = klass.compositions.select{|c| c[:class] =~ /DIM|PHD::/}.collect{|c| c[:getter]}
  
  attribute_getters = klass.properties.select{|k,v| v[:class] =~ /DIM|IEEE|PHD::/}.select{|k,v| v[:additional_data] && v[:additional_data][:modeled_as] == :attribute}.collect{|k,v| v[:getter]}
  
  (association_composition_getters + attribute_getters).each{|getter| getter_hash[getter] = nil}
  # TODO this is certainly a more elegant way of doing the initial population of each override mapping...
  MyDevice.default_overrides[klass] = getter_hash.dup
  MyDevice.c4mi_overrides[klass]    = getter_hash.dup
end
# and now we tweak the json_getters.  attributes of type OID-TYPE and type TYPE will get a ref_id via creating a proxy getter
klasses.each do |klass|
  proxied_properties = klass.properties.select{|k,v| v[:additional_data] && v[:additional_data][:prometheus] && v[:additional_data][:prometheus]["proxy_property"] && v[:additional_data][:modeled_as] != :attribute_reciprocal}
  proxied_properties.each do |getter, property_data|
    # Build the method that substitues the value of an OID-Type or TYPE with <ref_id>::<term_code>
    term_proxy_getter = property_data[:additional_data][:prometheus]["proxy_property"].first.gsub("-", "_").to_sym
    json_proxy_getter1 = (getter.to_s + "_json_proxy").to_sym
    if ['system_type', 'type'].include?(getter.to_s)
      klass.class_eval do
        define_method(json_proxy_getter1) do |*args|          
          if proxy_term = send(term_proxy_getter)
            # TODO should this be cf_term_code or term_code?
            [proxy_term.reference_id, proxy_term.cf_term_code].join("::")
          elsif pt = proposed_type
            if pt.found_in_rtmms
              pt.reference_id
            else
              "#{pt.reference_id} (unapproved)"
            end
          end
        end
        derived_attribute(json_proxy_getter1, ::String)
        # add_json_getter_override getter, json_proxy_getter1
        MyDevice.default_overrides[klass][getter] = json_proxy_getter1
      end
    else
      klass.class_eval do
        define_method(json_proxy_getter1) do |*args|
          proxy_term = send(term_proxy_getter)
          if proxy_term
            if proxy_term.is_a? Array
              proxy_term.collect do |term|
                # Right here is where the term_code gets addeded into the reference_id
                # This is a little bit odd for attributes of type OID-Type because we are adding cfcode10 instead of code10.  OID-Type really doesn't have information about the partition.
                [term.reference_id, term.term_code].join("::")
              end.join(", ")
            else
              [proxy_term.reference_id, proxy_term.term_code].join("::")
            end
          end
        end
        derived_attribute(json_proxy_getter1, ::String)
        # add_json_getter_override getter, json_proxy_getter1
        MyDevice.default_overrides[klass][getter] = json_proxy_getter1
      end
    end
    
    # Build the method that returns the ref_id for a given oid type and add it to the c4mi mapping
    if property_data[:class].to_const <= DIM::CommonDataTypes::ASN1::OID_Type # only for oid-types
      term_proxy_getter = property_data[:additional_data][:prometheus]["proxy_property"].first.gsub("-", "_").to_sym
      ref_id_getter = (getter.to_s + "_ref_id").to_sym
      klass.class_eval do
        define_method(ref_id_getter) do |*args|
          # puts "evaled class #{klass} to define method\n" + self.class.to_s + "." + ref_id_getter + "\nproxy_term: " + term_proxy_getter.to_s + "\n#{property_data.reject{|k,v| k == :methods}}"
          proxy_term = send(term_proxy_getter)
          if proxy_term
            if proxy_term.is_a? Array
              proxy_term.collect{|term| term.reference_id}.join(", ")
            else
              proxy_term.reference_id
            end
          end
        end
        derived_attribute(ref_id_getter, ::String)
        MyDevice.c4mi_overrides[klass][ref_id_getter] = nil
      end
    end
  end
end
relevant_packages = [DIM, Nomenclature, MyDevice, IEEE20101, PHD]
relevant_classes = relevant_packages.collect{|rp| rp.classes(:ignore_class_namespaces => true)}.flatten.uniq
getters_info = {}
relevant_classes.each do |rc|
  getters_info[rc] ||= {}
  if rc < Sequel::Model # otherwise it should be a primitive
    # Get only the properties that are typed as one of the relevant classes
    props = rc.properties.select{|k,v| relevant_classes.include?(v[:class].to_const)}
    props.each do |k,v|
      getters_info[rc][v[:getter]] = v[:class] # v[:class] is a String
    end
  end
end


=begin
GraphState.ignored_getters_by_class.clear
relevant_classes.each do |rc|
  if rc < Sequel::Model # otherwise it should be a primitive
    rejected_properties = rc.properties.select{|k,v| v[:derived] || v[:getter].to_s =~ /proxy/ || v[:getter].to_s =~ /_term$/ || !relevant_classes.include?(v[:class].to_const)}.collect{|k,v| v[:getter]}
    GraphState.ignored_getters_by_class[rc] = rejected_properties
  end
end
=end


relevant_classes.each do |rc|
  if rc < Sequel::Model # otherwise it should be a primitive
    # Reject property if:
    # * it is derived
    # * it is a proxy getter
    # * it is a term getter (these exist for attributes of type TYPE and OID-Type)
    # * its type is not one of those in the collection of relevant classes.    
    rejected_properties = rc.properties.select{|k,v| v[:derived] || v[:getter].to_s =~ /proxy/ || v[:getter].to_s =~ /_terms?$/ || !relevant_classes.include?(v[:class].to_const)}.collect{|k,v| v[:getter]}
    # remove all of these properties from the collection of properties that will be used in graph.  *I think* that MyDevice.default_overrides is actually the governing hash of what will get processed -- which means that its name is a bit misleading. 
    rejected_properties.each{|rp| MyDevice.default_overrides[rc].delete(rp)} if MyDevice.default_overrides[rc]
  end
end


# Manually put some stuff in there that we want anyway
MyDevice.default_overrides[MyDevice::PCDProfile] = MyDevice.c4mi_overrides[MyDevice::PCDProfile] = {:profile_root=>nil, :uuid => nil, :creation_time => nil, :application_version => nil }
# MyDevice.default_overrides[MyDevice::Profiled] = MyDevice.c4mi_overrides[MyDevice::Profiled] = nil
MyDevice.default_overrides[MyDevice::ProfileRoot] = MyDevice.c4mi_overrides[MyDevice::ProfileRoot] = {:pcd_profile => nil}
# pp MyDevice.default_overrides[MyDevice::PCDProfile]

getters_info.each do |klass, getter_info|
  next unless MyDevice.default_overrides[klass]
  getter_info.each do |getter, type|
    if type =~ /RichText/
      MyDevice.default_overrides[klass][getter] = "#{getter.to_s}_content"
      MyDevice.c4mi_overrides[klass][getter]    = "#{getter.to_s}_content"
    end
    if type.to_const.enumeration?
      # define method to get value from enumeration
        value_method = "#{getter}_enum_value".to_sym
        klass.class_eval do
        define_method(value_method) do |*args|
          ret = send(getter)
          if ret.is_a? Array
            # ret.collect{|r| r.value}
            # This will cause them to be a list of values that are associated with a single 'attribute= "<list>"' as opposed to a repeated list of tags where each one has one of the values.
            ret.collect{|r| r.value}.join(' ')
          else
            ret.value if ret
          end
        end
        derived_attribute(value_method, ::String)
      end 
      MyDevice.default_overrides[klass][getter] = value_method
      MyDevice.c4mi_overrides[klass][getter]    = value_method
    end
  end
end