# must not have the word m-o-d-u-l-e above the next line (so that a Regexp can figure out the m-o-d-u-l-e name)
module Dim
  
  # For more information about meta_info.rb, please see project MM, lib/mm/meta_info.rb
  
  # Deprecated meta-information:
  # TESTS_NEED_SELENIUM  -  replaced by aModule.selenium?
  # PACKAGING - replaced by aModule.packaging # Slightly different, this is an Array of Symbols
  # WEBAPP - replaced by aModule.webapp?
  # RUBYFORGE_PROJECT - replaced by aModule.rubyforge_project
  # PLATFORM - replaced by RUNTIME_VERSIONS

  
  # SUGGESTION: Treat "Optional" as meaning "can be nil", and define all constants, even if the value happens to be nil.
  
  # Required String
  GEM_NAME = "dim"
  # Required String
  VERSION = '6.4.0'
  # Optional String or Array of Strings
  AUTHORS = ["Michael Faughn"]
  # Optional String or Array of Strings
  EMAILS = ["m.faughn@prometheuscomputing.com"]
  # Optional String
  HOMEPAGE = nil
  # Required String
  SUMMARY = %Q{Edits DIM related information}
  # Optional String
  DESCRIPTION = %Q{Edits DIM objects, meta-information, conformance statements, and RTMMS data.}
  
  # Required Symbol
  # This specifies the language the project is written in (not including the version, which is in LANGUAGE_VERSION).
  # A project should only have one LANGUAGE (not including, for example DSLs such as templating languages).
  # If a project has more than one language (not including DSLs), it should be split.
  # The reason is that mixing up languages in one project complicates packaging, deployment, metrics, directory structure, and many other aspects of development.
  # Choices are currently:
  #   * :ruby - project contains ZERO java code
  #           it may contain JRuby code and depend or jars or :java projects,  if RUNTIME_VERSIONS has a :jruby key
  #           implies packaging as gem
  #   * :java - contains ZERO ruby code (with exception of meta_info.rb), and depends on zero Ruby code.
  #           implies packaging as jar - may eventually also support ear, war, sar, etc
  LANGUAGE = :ruby
  # This differs from Runtime version - this specifies the version of the syntax of LANGUAGE
  LANGUAGE_VERSION = ['> 2.2.0']
  # This is different from aGem::Specification.platform, which appears to be concerned with OS.
  # This defines which implentation of Ruby, Java, etc can be used.
  # Required Hash, in same format as DEPENDENCIES_RUBY.
  # The version part is used by required_ruby_version
  # Allowable keys depend on LANGUAGE. They are in VALID_<language.upcase>_RUNTIMES
  RUNTIME_VERSIONS = {
    :mri => ['> 2.2.0']
  }
  # DEPRECATGED Symbol - code is now looking at APP_TYPES instead of this.
  # Choices are currently:
  #   * :library - reusable functionality, not intended to stand alone. Might be a plugin.
  #   * :utility - intended for use on command line
  #   * :web_app - an application that uses a web browser for it's GUI
  #   * :service - listens on some port. May include command line tools to manage the server.
  #   * :gui_app - has a Swing, Fox, WXwidget, etc GUI
  TYPE = :web_app
  # Array of Symbols specifying what type of app to build. Empty for no Apps.
  # If LANGUAGE is ruby, you can use :zip, :jar, :appBundler, or :platypus
  # If LANGUAGE is java, you can use :zip, :jar, :appBundler
  #   * :jar        - A jar containing all gems and the contents of all jars. To avoid problems with signed code, may need to change to http://one-jar.sourceforge.net/, http://code.google.com/p/jarjar/, JarSplice, etc
  #   * :zip        - A zip file containing all required jars, and all required unpacked gems. Can run app by catapult.rb
  #   * :appBundler - Makes a Windows .exe, and a .zip of a Mac application.  In both cases, the application is based on a jar.
  #   * :platypus   - A binary made by Platypus from the contents of :zip. Does not yet include Ruby. Uses whatever ruby is specified by "which ruby"
  APP_TYPES = []
  # Specifies what to invoke when the user runs it. Not meaningful for libraries (which are invoked by API) or utilities (invoked by shell script in 'bin' directory).
  # When :ruby==LANGUAGE
  #    LAUNCHER must be a String path, relative to either bin or lib. If present in both, bin is used.
  #    If in bin, this will be invoked through the operating system (after setting up PATH and CLASSPATH).
  # When :java==LANGUAGE
  #    LAUNCHER must be a String path, relative to lib, that specifies Main-Class in the jar manifest.
  LAUNCHER_METHOD = :load
  LAUNCHER = 'dim'
  # LAUNCHER = '#{lib_dir}/gems/root_project_template-' + VERSION + '/bin/root_project_template' 
 
  # Optional Hashes - if nil, presumed to be empty.
  # There may be additional dependencies, for platforms (such as :maglev) other than :mri and :jruby
  # If JRuby platform Ruby code depends on a third party Java jar, that goes in DEPENDENCIES_JAVA
  DEPENDENCIES_RUBY = { 
    :gui_site => '7.2.3',
    :gui_director => '6.11.2', # Necessary since dim relies on gui_widgets' link method, which is deprecated
    :json_graph => '~> 2.0', # FIXME when json_graph is actually fixed...
    :dim_generated => '>= 6.9',
    :builder => '~> 3.2',
    :sqlite3 => '~> 1.3',
    :htmltoword => '~> 0.5',
    :curb => '~> 0.9',
    :docx => '0.2.07',
    :ttfunk => '~> 1.5',
    :valuable => '~> 0.9',
    :'ruby-prof' => '~> 0.16',
    # Required by htmltoword, but newer versions need conflicting Rack versions
    :actionpack => '~> 4.2',
    :actionview => '~> 4.2',
    :activesupport => '~> 4.2',
    :rack => '~> 1.6'
  }

  DEPENDENCIES_MRI = { }
  DEPENDENCIES_JRUBY = { }
  DEVELOPMENT_DEPENDENCIES_RUBY = { 
    :csv_tools => '~> 0.0', # For metainfo import
    :db_io => '~> 0.4'
  } 
  DEVELOPMENT_DEPENDENCIES_MRI = { }
  DEVELOPMENT_DEPENDENCIES_JRUBY = { }
  
  # Java dependencies are harder to handle because Java does not have an equivalent of Ruby gem servers.
  # The closest thing is Maven repositories, which are growing in popularity, but not yet ubiquitous enough to warrant supporting in this tool.
  # Currently only the keys are used, version info is ignored.
  # Keys can be the names of Jars (complete, including any version info embedded in name) that must be located in JARCHIVE (key must end in .jar), or the name of a constant (key must not end in .jar).
  # Constsants must be defined in this module. Constants must not be computed from absolute paths (use environmental variables if necessary).
  # Support for constants is provided primarily to accomodate MagicDraw, which requires a large number of jars.
  DEPENDENCIES_JAVA = { }
  DEPENDENCIES_JAVA_SE = { }
  DEPENDENCIES_JAVA_ME = { }
  DEPENDENCIES_JAVA_EE = { }
  
  # An Array of strings that YARD will interpret as regular expressions of files to be excluded.
  YARD_EXCLUDE = []
  # ==========================================================
  # Additional meta-information for apps
  
  require 'Foundation/conventions'
  extend App_MetaInfo_Behaviors
  
crankwheel_script = %Q[<style media="screen" type="text/css">
 .demo_button {
     width: 102px;
     cursor: pointer;
     float: left;
     position: fixed;
     bottom: 15px;
     left: 20px;
     color: #fff;
     background-color: #5cb85c;
     border-color: #4cae4c;
     border-radius: 4px;
     text-align: center;
     padding: 10px 0;
 }
 .news_button {
     width: 102px;
     cursor: pointer;
     float: left;
     position: fixed;
     bottom: 15px;
     left: 200px;
     color: #fff;
     background-color: #5cb85c;
     border-color: #4cae4c;
     border-radius: 4px;
     text-align: center;
     padding: 20px 0;
 }
</style>
<script>
(function(i,c,a,n,s,h,o,w,u){
   if(typeof i.showu === 'object'){i.showu.reInitialize()}else{
   i.showu = {'_q':[]}; i._ishowuSettings={id:'KQWjq-Wy',d:a}; s=c.getElementsByTagName('head')[0];
   h=c.createElement('script');h.async=1; h.type = 'text/javascript'; h.src=a+n; s.appendChild(h);
   o = ['launch', 'onEvent', 'getCapacity', 'populateFields', 'reInitialize']; function p(f) {i.showu[f] = function()
   {i.showu._q.push([f].concat(Array.prototype.slice.call(arguments, 0)));};} for (w = 0; w < o.length; w++) {p(o[w]);}}
   })(window,document,'https://meeting.is','/ss/js/showu_app.js')
</script> <div class='demo_button crankwheel-com-showu-launch-button'>Click for technical support or a live demo</div>
]
  
  # Defaults specified here can be overriden on the command line.
  DEFAULT_APP_OPTIONS = {
    # :string_html_scripts => [crankwheel_script].join("\n"),
    :change_tracker_mode => :extra_limited,
    :favicon                      => '/images/x73.ico',
    :port                         => '7123',
    :spec_name                    => 'dim_generated.rb',
    :spec_module                  => 'DimGenerated',
    :spec_title                   => 'PoCD Profile Editor (11073:10201)', # spec_title,
    :custom_spec                  => File.join(File.expand_path(File.dirname(__FILE__)), "custom_spec"),
    :model_extensions             => model_extensions_path,
    :pre_schema_model_extensions  => File.join(File.expand_path(File.dirname(__FILE__)), "pre_schema_model_extensions.rb"),
    :multivocabulary              => false,
    :default_page_size            => 50,
    :default_code_language        => 'ASN.1',
    :enable_concurrency           => false,
    :clone                        => true,
    :color_coding                 => true,
    :clear_view_selection_page    => false,
    :tree_view                    => {:enabled => true, :show_roles => false, :no_open_all => true},
    :custom_page_root             => relative('pages'),
    :login_or_register_blurb_path => File.join(File.expand_path(File.dirname(__FILE__)), "pages/view/dim_blurb.haml"),
    :license_agreement_path       => File.join(File.expand_path(File.dirname(__FILE__)), "pages/view/dim_agreement.haml"),
    # :text_languages               => ['Markdown', 'HTML'],
    :show_text_languages          => false,
    :default_text_language        => :Plain,
    :text_always_collapsed        => true,
    :custom_home_page             => {:url => '/x73/devices', :name => 'Devices'},
    :load_all_controllers         => true,
    :show_advanced_view           => false,
    :custom_pages => {
      :homepage => {
        :name => "Nomenclature",
        :url => "/x73/nomenclature",
        :show_in_nav_bar => true
      }
    }
  }
end