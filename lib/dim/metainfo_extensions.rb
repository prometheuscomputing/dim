module MetaInfo
  class Property
    def qualified_name
      containing_classifier.qualified_name + "::" + name
    end
    
    def defined_in_asn1_package
      return @asn1_checked if @asn1_checked
      @asn1_checked = check_asn1
    end
    
    def check_asn1
      packages.each{|p| return true if p.name =~ /ASN1/}
      false
    end
      
    
    def packages
      t = type
      if t
        t.packages
      else
        []
      end
    end
    
    # FIXME hack here because is_asn1 not correctly set on some properties
    def is_asn1?
      set_asn1_status if is_asn1.nil?
      is_asn1
    end
    
    def set_asn1_status
      if is_asn1.nil?
        ChangeTracker.start unless ChangeTracker.started?
        self.is_asn1 = containing_classifier.is_asn1? || type.is_asn1?
        self.save
        ChangeTracker.commit; ChangeTracker.start
      end
    end
  end # class Property
  
  class Classifier
    
    def asn1_parent_name
      if sc = super_classifier
        sc.name
      else
        puts "#{self.qualified_name} had no parent name" # returns nil
      end
    end
    
    def qualified_name
      containing_package.qualified_name + "::" + name
    end
    
    def ruby_model
      return qualified_name.gsub(/\s|-/, "").to_const rescue NameError
      begin
        return qualified_name.gsub(/\s|-/, "_").to_const
      rescue NameError => ne
        puts_cyan "No ruby model for #{qualified_name}!"
        puts ne.message
        nil
      end
    end
    
    # Sending the type param as nil because the type will be different for every instance of class Classifier
    derived_association :dim_instances, nil, :type => :one_to_many
    def dim_instances
      ruby_model&.all || []
    end
    
    def is_asn1?
      set_asn1_status if is_asn1.nil?
      is_asn1
    end
    
    def set_asn1_status
      if is_asn1.nil?
        ChangeTracker.start unless ChangeTracker.started?
        pkgs = packages
        self.is_asn1 = pkgs.select{|p| p.name == "DIM" || p.name == "IEEE20101"}.any? && pkgs.select{|p| p.name == "ASN1"}.any?
        self.save
        ChangeTracker.commit; ChangeTracker.start
      end
    end
    
    def packages
      return [] unless containing_package # FIXME this is because things like String have no package but is String really supposed to be there???? or should it really be OCTET_STRING and such...also check Integer and so forth (i.e. other primitives)
      [containing_package] + containing_package.packages
    end
    
    def ancestors
      if parent = super_classifier
        (parent.ancestors + [parent]).flatten.uniq
      else
        []
      end
    end
    
    def all_properties
      (ancestors << self).collect{|classifier| classifier.properties}.flatten.uniq
    end
  end # class Classifier
  
  class Package
    def qualified_name
      if p = parent_package
        p.qualified_name + "::" + name
      else
        name
      end
    end
    def ruby_model
      begin
        qualified_name.gsub(" ", "_").gsub("-", "_").to_const
      rescue NameError => ne
        puts ne.message
        nil
      end
    end
    def packages
      if ppkg = parent_package
        [ppkg] + ppkg.packages
      else
        []
      end
    end
    def all_classifiers
      (classifiers + [child_packages.collect{|cp| cp.all_classifiers}].flatten).uniq
    end
  end # class Package
  
  class Group
    # returns array of parents ordered from most most super to most immediate.  assumes single inheritance.
    def parents
      if parent_group
        return parent_group.parents << parent_group
      else
        []
      end
    end # parents
    
    # used in GUI and printed standard(.docx)
    def summary_array
      answer = []
      (parents << self).each do |group|
        answer << [group.owner_class.name, group.properties.collect{|p| p.name}]
      end
      answer
    end # summary_array
    
    derived_attribute(:summary_html, ::String)
    def summary_html
      html = ["<h6>Group Elements for #{owner_class.name} attribute group '#{name}'</h6>"]
      summary_array.each do |entry|
        html << "<u>from #{entry.first}:</u>"
        html << entry.last.join(", ")
      end
      html << '<br>'
      html.join("<br>")
    end # summary_html
  end # class Group
end