# GuiBuilderProfile = Gui_Builder_Profile unless defined? GuiBuilderProfile # because I am lazy
# require 'sequel_specific_associations/model/primitive_extensions'
require 'net/https'
require 'common/constants'
require 'uri'
require 'json'
require 'time'
require 'pp'
require 'json_graph'
require 'json_graph/sequel_concise_marshalling'
require 'fileutils'
require 'htmltoword'
require_relative 'ruby_extensions' # should be before any extensions on any class that is a Sequel::Model
require_relative 'sequel_model_extensions'
require_relative 'hacks' # ideally this file is empty.  It exists in order to isolate things that really need to be done differently
require_relative 'profile_extensions'
require_relative 'dim_extensions'
#*PHD*
# require_relative 'phd_extensions'
require_relative 'asn1_extensions'
require_relative 'nomenclature_extensions'
require_relative 'metainfo_extensions'
require_relative 'standard_extensions'
require_relative 'json_getters'
require_relative 'rch_report'
require_relative 'xml_parse'
require_relative 'parse_dsv'
require_relative 'rch_xml'
require_relative 'add_indices'
require_relative 'attribute_management_setup'
require_relative 'gui_options'
require_relative 'word_generation'
require_relative 'classifier_definitions_generation'
require_relative 'utility'

# business_rules_filename = File.expand_path(File.join(File.dirname(__FILE__), "business_rules.rb"))
# BusinessRules::RuleDsl.instance_eval(File.read(business_rules_filename))
BusinessRules::RuleDsl.instance_eval(File.read(relative('business_rules.rb')))

Htmltoword.configure do |config|
  # config.custom_templates_path = 'path_for_custom_templates'
  # If you modify this path, there should be a 'default.docx' file in there
  # config.default_templates_path = 'path_for_default_template'
  # If you modify this path, there should be a 'html_to_wordml.xslt' file in there
  config.default_xslt_path = relative 'standard/xslt/'
  # The use of additional custom xslt will come soon
  # config.custom_xslt_path = 'some_path'
end

module Gui_Builder_Profile
  module SafeContent
    def safe_content
      html = markup_language&.value == :HTML
      # This would be fine except we are programmatically entering stuff into description content so even if the default langauge is HTML it still isn't going to be in HTML unless it has been saved in the GUI.
      html ||= content.include?("<p>")
      html ? Nokogiri::HTML(content).text : content
    end
  end
    
  class RichText
    include SafeContent
  end
end