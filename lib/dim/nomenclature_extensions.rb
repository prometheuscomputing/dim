# FIXME curb requires native extensions and installing it on Windows may be tricky.  See https://github.com/taf2/curb#installation
require 'curb'
# require 'os'
module Nomenclature
  # ROOT_CA = case
  # when OS.linux?
  #   '/etc/ssl/certs'
  # when OS.mac?
  #   # I have no idea if this will work on my computer, let alone anyone else's
  #   # '/usr/local/etc/openssl/cert.pem'
  #   File.join('..', __dir__)
  # when OS.windows?
  #   raise 'Where are the ssl certs on a Windows machine??'
  # else
  #   raise 'You are running the application on an unknown operating system and the ssl certs are in an unknown location.'
  # end
  
  TERM_CLASSES          = Nomenclature::RTMMSTerm.children
  PROPOSED_TERM_CLASSES = Nomenclature::ProposedTerm.children
  def self.find_term_by_ref_id(refid)
    return nil unless refid
    TERM_CLASSES.each do |klass|
      term = klass.where(:reference_id => refid.strip).first
      return term if term
    end
    nil # didn't find a term
  end
  def self.find_term_by_cf_code(code)
    return nil unless code && !code.to_s.empty?
    TERM_CLASSES.each do |klass|
      term = klass.where(:cf_term_code => code.to_s.strip.to_i).first
      return term if term
    end
    nil # didn't find a term
  end
  def self.find_token(name)
    Nomenclature::Token.where(:name => name.strip).first
  end
  def self.find_proposed_term_by_ref_id(refid)
    return nil unless refid
    PROPOSED_TERM_CLASSES.each do |klass|
      term = klass.where(:reference_id => refid.strip).first
      return term if term
    end
    nil # didn't find a term
  end
  def self.clear_tables
    klasses = Nomenclature.classes(:ignore_class_namespaces => true)
    klasses.each do |klass|
    # Nomenclature.classes(:no_imports => true).each do |klass|
      if DB.table_exists? klass.table_name
        DB[klass.table_name].delete
      else
        puts "#{klass} with table_name #{klass.table_name} was not found in the DB!" unless klass.abstract?
      end
      DB[(klass.table_name.to_s + "_deleted").to_sym].delete if DB.table_exists? (klass.table_name.to_s + "_deleted").to_sym
    end
  end
  
  def self.renew_terms
    clear_tables
    update_terms
  end
  def self.last_date_added
    last_date = Time.parse("Jan 1, 1970")
    TERM_CLASSES.each do |klass|
      klass_last = klass.max :update_date
      klass_last_date = Time.parse(klass_last) if klass_last
      last_date = klass_last_date if klass_last_date && klass_last_date > last_date
    end
    puts "Last term was added: #{last_date}"
    last_date
  end
    
  def self.fetch_json(last_term_time = nil, use_ssl = true)
    if last_term_time && !last_term_time.empty?
      puts "Last term was added: #{last_term_time}"
      date = Time.parse(last_term_time) # Need to handle errors?
    else
      date = last_date_added
    end
    time_string = date.strftime("%d%m%Y%H%M%S")
    url = "https://rtmms.nist.gov/rtmms/getTermsJson.do?fromDate=#{time_string}"
    puts "Fetching terms from #{url}"
    c = Curl::Easy.new(url)
    c.ssl_verify_peer = false
    c.get
    json = JSON.parse(c.body_str)
  end
  
  # FIXME if we fail in the middle of a term then we have a situation where the last_term_time will be whatever the update_date of the last successful term was.  That means that any terms that were in the list of new terms that were also on that same date but didn't get processed will never make it into the system.  Basically, this can't fail that way.  Any failing terms need to get logged and it would even be worthwhile for the system to e-mail a report of the failure.
  # TODO -- this thing is still fetching terms that are ostensibly already in the system.  I'm not sure why.  It isn't adding them again so that isn't a problem.  Either I need to adjust the date in the 'get since' request or RTMMS is sending stuff that it shouldn't be sending.
  def self.update_terms(last_term_time = nil)
    json = fetch_json
    puts "Fetched #{json.count} items"
    units_and_literals, json_remainder1 = json.partition{|term| term["type"] == "UNIT" || term["type"] == "LITERAL" || term["type"] == "TOKEN"}
    groups, json_remainder2 = json_remainder1.partition{|term| term["type"] =~ /GROUP/}
    # units and literals need to be added first
    success_msgs, warning_msgs, failure_msgs, added_refids, notifications, problem_msgs = [], [], [], [], [], []
    (units_and_literals + groups + json_remainder2).each do |rtm_term|
      status = add_term rtm_term
      failure_msgs << status[:failure] if status[:failure]
      warning_msgs << status[:warning] if status[:warning]
      success_msgs << status[:message] if status[:message]
      problem_msgs << status[:problem] if status[:problem]
      added_refids << status[:term].reference_id if status[:term]
      # unless# status[:failure] || status[:warning] || status[:message] || status[:problem] || status[:term]
      #   puts rtm_term.inspect
      #   puts status.inspect
      #   puts
      # end
    end
    added_refids.each do |refid|
      if proposed_term = find_proposed_term_by_ref_id(refid)
        notifications << "Locally proposed term: #{refid} is now found in RTMMS"
        ChangeTracker.commit do
          proposed_term.found_in_rtmms = true
          proposed_term.save
        end
      end
    end
    if json.empty?
      pretty_response = "<h3>No new terms were found</h3>"
    else
      pretty_response = ""
      pretty_response << "<h3>Failed to Add Terms:</h3><br>" + failure_msgs.sort.join('<br>') unless failure_msgs.empty?
      pretty_response << "<br><br><h3>Problems:</h3><br>" + problem_msgs.sort.join('<br>') unless problem_msgs.empty?
      pretty_response << "<br><br><h3>Warnings:</h3><br>" + warning_msgs.sort.join('<br>') unless warning_msgs.empty?
      pretty_response << "<br><br><h3>Notifications:</h3><br>" + notifications.sort.join('<br>') unless notifications.empty?
      pretty_response << "<br><br><h3>Successfully Added Terms:</h3><br>" + success_msgs.sort.join('<br>') unless success_msgs.empty?
      if pretty_response.empty?
        pretty_response = "No new terms." if pretty_response.empty?
      else
        Gui::Emailer.send_email('m.faughn@prometheuscomputing.com', pretty_response, 'Nomenclature Update')
      end
    end
    pretty_response
  end    
  
  def self.find_or_convert_or_create_term(ref_id, cfcode10, klass)
    if klass == Nomenclature::Token
      term = find_token(ref_id)
    else
      term = find_term_by_ref_id(ref_id)
      term ||= find_term_by_cf_code(cfcode10)
    end
    if term
      convert_term(term, klass)
    else
      if klass == Nomenclature::Token
        {:term => klass.create(:name => ref_id), :message => "Created a new #{klass.name.demodulize} with value #{ref_id}"}
      else
        {:term => klass.create(:reference_id => ref_id), :message => "Created a new #{klass.name.demodulize} with refid #{ref_id}"}
      end
    end
  end
  
  def self.convert_term(term, klass)
    if term.class == klass
      {:term => term}      
    else
      if term.class == Nomenclature::Term
        started = ChangeTracker.started?
        ChangeTracker.commit if started
        term = SpecificAssociations.convert_object_to_type(term, klass)
        ChangeTracker.start if started
        {:term => term, :message => "Converted #{term.reference_id} into a #{term.class.name.demodulize}"}
      elsif klass == Nomenclature::Term
        message = "Not converting #{term.reference_id} from a #{term.class} into a #{klass}."
        {:term => term, :warning => message}
      else
        message = "Failed to convert #{term.reference_id} from a #{term.class} into a #{klass}.  Contact the developer."
        {:term => term, :warning => message}
      end
    end
  end
  
  def self.add_term rtm_term
    # Keys that Nicolas is using as of Jul24,2014
    # ["commonTerm", "referenceId", "status", "systematicName", "termCode", "termDescription",  "updateDate", "type", "units", "enums", "acronym"]
    # not parsing acronym because it doesn't look useful or relevant within the context of MyDevice
    status = {}
    ref_id       = rtm_term["referenceId"]
    cf_term_code = rtm_term["cfcode10"]
    ref_id.strip! if ref_id
    return {:failure => "RTMMS entry had no value for referenceId or cfcode10: #{rtm_term.inspect}"} unless ref_id || cf_term_code
    status[:problem] = "RTMMS entry had no value for referenceId: #{rtm_term.inspect}" unless ref_id
    #   puts "rtm_term had no ref_id: #{rtm_term.inspect}"
    # end
    ChangeTracker.start unless ChangeTracker.started?
    case rtm_term["type"]
    when "ENUMERATION"
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Enumeration)
    when "ENUMGROUP"
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::EnumerationGroup)
    when "LITERAL"
    # Because the web service seems to think that a thing with a code10 can be TOKEN...facepalm.
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Literal)
    when "TOKEN"
      if rtm_term["code10"]
        status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Literal)
      else
        status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Token)
      end
    when "METRIC"
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Metric)
    when "UNITGROUP"
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::UnitGroup)
    when "UNIT"
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Unit)
    else
      status = status.merge find_or_convert_or_create_term(ref_id, cf_term_code, Nomenclature::Term)
    end
    if failure_message = status[:failure]
      ChangeTracker.cancel if ChangeTracker.started?
      return status
    end
    term = status[:term]
    case term.class
    when Nomenclature::Enumeration
      term.enums = process_enums(rtm_term["enums"], true)
    when Nomenclature::EnumerationGroup
      term.members = process_enums(rtm_term["enums"], false)
    when Nomenclature::Metric
      term.units = process_units(rtm_term["units"], true)
    when Nomenclature::UnitGroup
      term.members = process_units(rtm_term["units"], false)
    end
    
    unless term.class == Nomenclature::Token
      if cf_term_code && !cf_term_code.to_s.empty?
        term.cf_term_code = cf_term_code.to_i
        binary_cftc = cf_term_code.to_i.to_s(2)
        lower16 = binary_cftc.slice!(/\d{16}$/) # or binary_cftc[-16..-1]
        unless lower16
          lower16 = binary_cftc.dup
          binary_cftc = "0"
        end
        puts rtm_term unless lower16
        term.term_code = lower16.to_i(2)
        # term.term_code = rtm_term["code10"]
        puts "derived term code does not match specified term code for #{ref_id}" unless rtm_term["code10"] == term.term_code
        upper16 = binary_cftc # because we did slice!
        partition_number = upper16.to_i(2)
        partition = DIM::CommonDataTypes::ASN1::NomPartition.where(Sequel.ilike(:value, "%(#{partition_number})")).first
        term.partition = partition if partition
      end
      common_term = rtm_term["commonTerm"]
      term.common_term = common_term.encode('UTF-16', 'UTF-8', :invalid => :replace, :replace => '').encode('UTF-8', 'UTF-16') if common_term
      tsysname = rtm_term["systematicName"]
      term.systematic_name = tsysname.encode('UTF-16', 'UTF-8', :invalid => :replace, :replace => '').encode('UTF-8', 'UTF-16') if tsysname
      tdesc = rtm_term["termDescription"]
      term.term_description = tdesc.encode('UTF-16', 'UTF-8', :invalid => :replace, :replace => '').encode('UTF-8', 'UTF-16') if tdesc
    end
    tstatus = rtm_term["status"]
    tstatus = tstatus.downcase.capitalize if tstatus
    term.status = tstatus unless term.is_a?(Nomenclature::Token) # FIXME revisit this when model is updated
    term.update_date = Time.parse(rtm_term["updateDate"]) if rtm_term["updateDate"]
    term.json_source = Gui_Builder_Profile::RichText.new(:content => rtm_term.to_s)
    if sources = rtm_term["sources"]
      term.sources_temp = sources.join(" ")
      source_literals = sources.collect do |s|
        Nomenclature::TermSource.where(Sequel.ilike(:value, s)).first
      end
      source_literals.compact!
      term.sources = source_literals
    end
    acronym = rtm_term["acronym"]
    begin # FIXME does the model need to change? do Tokens have acronyms?
      term.acronym = acronym.encode('UTF-16', 'UTF-8', :invalid => :replace, :replace => '').encode('UTF-8', 'UTF-16') if acronym
    rescue Exception => e
      raise e unless term.is_a?(Nomenclature::Token)
    end
      
    term.save
    begin
      ChangeTracker.commit
    rescue Exception => e
      puts e.message
      puts e.backtrace
      puts term.inspect
      ChangeTracker.cancel
      return {:failure => "Failed to update or create #{rtm_term['ref_id']}.  Contact the Site Administrator!"}
    end
    status || {:term => term}
  end
    
  def self.process_enums enums, include_groups = false
    enums ||= []
    literals    = enums.collect{|lrefid| Nomenclature::Literal.where(:reference_id => lrefid).first}.compact
    tokens      = enums.collect{|lrefid| Nomenclature::Token.where(:name => lrefid).first}.compact
    enum_groups = enums.collect{|lrefid| Nomenclature::EnumerationGroup.where(:reference_id => lrefid).first} if include_groups
    enum_groups ||= []
    enum_groups.compact
    (enum_groups | tokens | literals).compact
  end
  
  def self.process_units units_array, include_groups = false
    units_array
    units = units_array.collect{|urefid| Nomenclature::Unit.where(:reference_id => urefid).first}.compact
    groups = units_array.collect{|urefid| Nomenclature::UnitGroup.where(:reference_id => urefid).first} if include_groups
    groups ||= []
    groups.compact!
    (groups | units).compact
  end
  
  class RTMMSTerm
    is_unique :reference_id
    # def self.ignored_json_getters_for_profile
    #   # We might need to get the units and enums in here at some point.  Not sure.  Maybe users would just key into RTMMS for them??
    #
    #   # all_property_names = [:reference_id, :partition, :term_code, :term_description, :systematic_name, :common_term, :status, :update_date, :term_type, :source, :valid_term_for, :literals, :units_for, :units, :term_for, :modelelement, :objectidentifiers, :literal_for]
    #   [:term_description, :systematic_name, :common_term, :status, :update_date, :term_type, :source, :literals, :units_for, :units, :modelelement, :objectidentifiers, :literal_for]
    # end
    alias_method :name, :reference_id 

  end

  class Token
    is_unique :reference_id
    alias_method :reference_id, :name
  end
  
  class ProposedTerm
    def note_creation
      ChangeTracker.start
      self.proposed_on = Date.today
      if defined?(Ramaze)
        user = Ramaze::Current.session[:user]
        name = "#{user.first_name} #{user.last_name}" if user
        name ||= ""
        name = user.login if name.strip.empty?
        name = Ramaze::Current.session[:username]
        self.proposed_by = name
        self.save
      end
      ChangeTracker.commit
    end
  end
end
