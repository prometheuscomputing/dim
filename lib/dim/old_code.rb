################# begin oid proxy madness ###################
# This is being done because the stakeholders don't want to actually deal with numbers every time there is and association to class OIDType.  Instead they want to see a Term.  Setting this up at the UML level would be onerous because it would require the creation of a large number of associations by hand.  Also note that it isn't a matter of simply changing everything that is typed as OIDType to type Term because the model really does need to stay the way it is.  The model is the standard for how medical devices communicate and they use OIDType (an integer) but the humans that want to create device profiles don't want to see integers, they want to see the Term (i.e. a string, a.k.a. 'Ref_ID').  Note that Term has an attribute, 'term_code' which is an integer and which fulfills the exact role that OIDType does...so there is a very clear and meaningful correlation.  Note also that this can't be done as a derived association because OIDType doesn't have an association (in the DIM) to Term and even if it did we would be trying to derive the Term association from the OIDType association which, at the instance level, isn't going to have a value at the point in time when one wants to use a Term instead of an OIDType (because it is the very thing we are trying to replace).  Talk about convoluted...
def create_oid_proxies_for klass
  original_methods = klass.associations.select{|getter, vals_hash| vals_hash[:class] == "DIM::CommonDataTypes::ASN1::OIDType"}.keys
  original_methods.each do |original_method|
    
    original_method_setter = (original_method.to_s + "=").to_sym
    add_original = ("add_" + original_method.to_s).to_sym
    remove_original = ("remove_" + original_method.to_s).to_sym
    proxy_getter = (original_method.to_s + "_proxy").to_sym
    proxy_getter_all = (original_method.to_s + "_proxy_all").to_sym
    proxy_setter = (original_method.to_s + "_proxy=").to_sym
    proxy_count = (original_method.to_s + "_proxy_count").to_sym
    add_proxy = ("add_" + original_method.to_s + "_proxy").to_sym
    proxy_add = (original_method.to_s + "_proxy_add").to_sym
    remove_proxy = ("remove_" + original_method.to_s + "_proxy").to_sym
    proxy_remove = (original_method.to_s + "_proxy_remove").to_sym
    remove_all_proxy = ("remove_all_" + original_method.to_s + "_proxy").to_sym
    proxy_type = (original_method.to_s + "_proxy_type").to_sym
    proxy_unassociated = (original_method.to_s + "_proxy_unassociated").to_sym
    proxy_unassociated_count = (original_method.to_s + "_proxy_unassociated_count").to_sym

    assoc_type = klass.associations[original_method][:type]
    to_one = assoc_type == :one_to_one || assoc_type == :many_to_one

    if to_one
      klass.class_eval do
        derived_association proxy_getter, Nomenclature::RTMMSTerm, :type => :many_to_one
        # So, this is a bit interesting.  We are deriving the Term from the association to OID.  OIDType to Term is really a to-many relationship but we are treating it as a to-one by returning just the first associated term.  There are valid reasons for doing this.  If you don't believe me then try doing it the other way and watch users start to remove associations between Terms and OIDType when they really shouldn't be doing so.  Anyway, the upshot of what we are doing is that if a user selects one term they may get back a different term later on.  Hopefully this will be the preferred synonym of the Term.  The Terms associated with an OIDType are orderable and the preferred synonym should be the first in the list.
        define_method(proxy_getter) do |*args|
          oid = send(original_method)
          oid ? oid.terms.first : nil
        end
        # And we need this method for times when we really do need all of the terms associated with the OIDType...
        define_method(proxy_getter_all) do |*args|
          oid = send(original_method)
          oid ? oid.terms : []
        end
        define_method(proxy_setter) do |nomenclature_term|
          unless nomenclature_term
            self.send(original_method_setter, nil)
            return
          end
          number = nomenclature_term.term_code.to_s
          oid = nomenclature_term.oid
          if oid.nil?
            # This really should be impossible if we are correctly creating an OIDType for every Term when the term is created.
            oid = DIM::CommonDataTypes::ASN1::OIDType.create(value: number)
          end
          self.send(original_method_setter, oid)
          save
          nomenclature_term
        end
        define_method(remove_proxy) do |not_used = nil|
          send(proxy_setter, nil)
        end
        alias_method proxy_remove, remove_proxy
        define_method(add_proxy) do |nomenclature_term|
          send(proxy_setter, nomenclature_term)
        end
        alias_method proxy_add, add_proxy
        define_method(proxy_count) do |*args|
          send(proxy_getter, *args) ? 1 : 0
        end
        define_method(remove_all_proxy) do
          send(original_method_setter, nil)
        end
        define_method(proxy_unassociated) do |filter = {}, limit = nil, offset = 0, order = {}|
          type_filters = []
          if filter.is_a?(Array)
            filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
            filter.delete({})
          else
            (type_filters << filter.delete('type')) if filter.is_a?(Hash)
          end
  
          all_terms = (Nomenclature::RTMMSTerm.filter(filter).all - send(proxy_getter_all))
          limit ? all_terms[offset..offset+limit-1] : all_terms
        end
      end
    else # to_many
      klass.class_eval do
        derived_association proxy_getter, Nomenclature::RTMMSTerm, type: :many_to_many
        define_method(proxy_getter) do |*args|
          oids = send(original_method)
          oids.collect{|oid| oid.terms.first}
        end
        define_method(proxy_setter) do |nomenclature_terms|
          oids = []
          numbers = []
          nomenclature_terms.each do |nt|
            oid = nt.oid
            oids << oid if oid
            numbers << nt.term_code.to_s unless oid
          end
          numbers.each do |num| # There really should not be any if we are correctly creating an OIDType for every Term when the term is created.
            oid = DIM::CommonDataTypes::ASN1::OIDType.create(value: num)
            oids << oid
          end
          self.send(original_method_setter, oids)
          save
          nomenclature_terms
        end
        define_method(proxy_count) do |*args|
          send(proxy_getter, *args).count
        end
        define_method(add_proxy) do |nomenclature_term|
          oid = nomenclature_term.oid
          unless oid
            if num = nomenclature_term.term_code
              oid = DIM::CommonDataTypes::ASN1::OIDType.where(value: num).first || DIM::CommonDataTypes::ASN1::OIDType.create(value: num)
            end
          end 
          send(add_original, oid)
        end
        alias_method proxy_add, add_proxy
        define_method(remove_proxy) do |nomenclature_term|
          oid = nomenclature_term.oid
          send(remove_original, oid)
        end
        define_method(remove_all_proxy) do
          send(original_method_setter, [])
        end
        define_method(proxy_unassociated) do |filter = {}, limit = nil, offset = 0, order = {}|
          type_filters = []
          if filter.is_a?(Array)
            filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
            filter.delete({})
          else
            (type_filters << filter.delete('type')) if filter.is_a?(Hash)
          end
    
          all_terms = (Nomenclature::RTMMSTerm.filter(filter).all - send(proxy_getter))
          limit ? all_terms[offset..offset+limit-1] : all_terms
        end
      end
    end
    # same for to-one and to-many
    klass.class_eval do
      define_method(proxy_type) do
        [Nomenclature::RTMMSTerm]
      end
      define_method(proxy_unassociated_count) do |*args|
        send(proxy_unassociated, *args).count
      end
    end
  end # original_methods.each
end # def create_oid_proxies_for

=begin
def create_concrete_oid_proxies_for klass
  original_methods = klass.associations.select{|getter, vals_hash| vals_hash[:class] == "DIM::CommonDataTypes::ASN1::OIDType"}
  original_methods.each do |original_method, assoc_hash| # key is original_method
    proxy_getter = (original_method.to_s + "_proxy").to_sym
    as_proxy = (assoc_hash[:opp_getter].to_s + "_as_proxy").to_sym
    assoc_type = klass.associations[original_method][:type]
    to_one = assoc_type == :one_to_one || assoc_type == :many_to_one

    if to_one
      klass.class_eval do
        many_to_one :'Nomenclature::RTMMSTerm', :one_to_one => true, :prefix => as_proxy, :other_prefix => proxy_getter
      end
    else # to_many
      klass.class_eval do
        many_to_many :'Nomenclature::RTMMSTerm', :prefix => as_proxy, :other_prefix => proxy_getter
      end
    end
  end # original_methods.each
end
=end

def create_type_proxies_for klass
  original_methods = klass.associations.select{|getter, vals_hash| vals_hash[:class] == "DIM::CommonDataTypes::ASN1::TYPE"}.keys
  original_methods.each do |original_method|
    original_method_setter = (original_method.to_s + "=").to_sym
    add_original = ("add_" + original_method.to_s).to_sym
    remove_original = ("remove_" + original_method.to_s).to_sym
    proxy_getter = (original_method.to_s + "_proxy").to_sym
    proxy_getter_all = (original_method.to_s + "_proxy_all").to_sym
    proxy_setter = (original_method.to_s + "_proxy=").to_sym
    proxy_count = (original_method.to_s + "_proxy_count").to_sym
    add_proxy = ("add_" + original_method.to_s + "_proxy").to_sym
    proxy_add = (original_method.to_s + "_proxy_add").to_sym
    remove_proxy = ("remove_" + original_method.to_s + "_proxy").to_sym
    proxy_remove = (original_method.to_s + "_proxy_remove").to_sym
    remove_all_proxy = ("remove_all_" + original_method.to_s + "_proxy").to_sym
    proxy_type = (original_method.to_s + "_proxy_type").to_sym
    proxy_unassociated = (original_method.to_s + "_proxy_unassociated").to_sym
    proxy_unassociated_count = (original_method.to_s + "_proxy_unassociated_count").to_sym

    assoc_type = klass.associations[original_method][:type]
    to_one = assoc_type == :one_to_one || assoc_type == :many_to_one

    if to_one
      klass.class_eval do
        derived_association proxy_getter, Nomenclature::RTMMSTerm, :type => :many_to_one
        # See notes in #create_oid_proxies_for
        define_method(proxy_getter) do |*args|
          type = send(original_method)
          oid = type ? type.code : nil
          oid ? oid.terms.first : nil
        end
        # And we need this method for times when we really do need all of the terms associated with the TYPE...
        define_method(proxy_getter_all) do |*args|
          type = send(original_method)
          oid = type ? type.code : nil
          oid ? oid.terms : []
        end
        define_method(proxy_setter) do |nomenclature_term|
          unless nomenclature_term
            self.send(original_method_setter, nil)
            return
          end
          tc = nomenclature_term.term_code
          number = tc.to_s
          oid = nomenclature_term.oid
          if oid.nil?
            # This really should be impossible if we are correctly creating an OIDType for every Term when the term is created.
            oid = DIM::CommonDataTypes::ASN1::OIDType.create(value: number)
          end
          type = oid.type
          if type.nil?
            # This really should be impossible if we are correctly creating an OIDType for every Term when the term is created.
            # FIXME when we know what the partition is from the term, put that in the TYPE
            type = DIM::CommonDataTypes::ASN1::TYPE.create
          end
          self.send(original_method_setter, type)
          save
          nomenclature_term
        end
        define_method(remove_proxy) do |not_used = nil|
          send(proxy_setter, nil)
        end
        alias_method proxy_remove, remove_proxy
        define_method(add_proxy) do |nomenclature_term|
          send(proxy_setter, nomenclature_term)
        end
        alias_method proxy_add, add_proxy
        define_method(proxy_count) do |*args|
          send(proxy_getter, *args) ? 1 : 0
        end
        define_method(remove_all_proxy) do
          send(original_method_setter, nil)
        end
        define_method(proxy_unassociated) do |filter = {}, limit = nil, offset = 0, order = {}|
          type_filters = []
          if filter.is_a?(Array)
            filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
            filter.delete({})
          else
            (type_filters << filter.delete('type')) if filter.is_a?(Hash)
          end
  
          all_terms = (Nomenclature::RTMMSTerm.filter(filter).all - send(proxy_getter_all))
          limit ? all_terms[offset..offset+limit-1] : all_terms
        end
      end
    else # to_many # I don't think this ever happens but whatever, we'll handle it anyway
      klass.class_eval do
        derived_association proxy_getter, Nomenclature::RTMMSTerm, type: :many_to_many
        define_method(proxy_getter) do |*args|
          types = send(original_method)
          oids = types.collect{|type| type.oid}
          oids.collect{|oid| oid.terms}.flatten # I don't really know if this is what is appropriate....
        end
        define_method(proxy_setter) do |nomenclature_terms|
          types = []
          oids = []
          numbers = []
          nomenclature_terms.each do |nt|
            oid = nt.oid
            oids << oid if oid
            numbers << nt.term_code.to_s unless oid
          end
          numbers.each do |num| # There really should not be any if we are correctly creating an OIDType for every Term when the term is created.
            oid = DIM::CommonDataTypes::ASN1::OIDType.create(value: num)
            oids << oid
          end
          oids.each do |oid|
            # FIXME again, we should be setting the partition value on the TYPE but we can't really do that yet.  And also...again...I don't think we should ever be executing this method because it just doesn't make sense within the context of the data model / domain
            types << oid.type || DIM::CommonDataTypes::ASN1::TYPE.create
          end
          self.send(original_method_setter, types)
          save
          nomenclature_terms
        end
        define_method(proxy_count) do |*args|
          send(proxy_getter, *args).count
        end
        define_method(add_proxy) do |nomenclature_term|
          oid = nomenclature_term.oid
          unless oid
            if num = nomenclature_term.term_code
              oid = DIM::CommonDataTypes::ASN1::OIDType.where(value: num).first || DIM::CommonDataTypes::ASN1::OIDType.create(value: num)
            end
          end
          # FIXME old familiar song about wanting to set the partition on the type, see above
          type = oid.type || DIM::CommonDataTypes::ASN1::TYPE.create
          send(add_original, type)
        end
        alias_method proxy_add, add_proxy
        define_method(remove_proxy) do |nomenclature_term|
          oid = nomenclature_term.oid
          if oid
            type = oid.type
            send(remove_original, type)
          else
            raise "Attempted to remove TYPE by removing a Term acting as a proxy for the TYPE but unable to find a TYPE associated to the Term"
          end
        end
        define_method(remove_all_proxy) do
          send(original_method_setter, [])
        end
        define_method(proxy_unassociated) do |filter = {}, limit = nil, offset = 0, order = {}|
          type_filters = []
          if filter.is_a?(Array)
            filter.each {|f| (type_filters << f.delete('type')) if f.is_a?(Hash) }
            filter.delete({})
          else
            (type_filters << filter.delete('type')) if filter.is_a?(Hash)
          end
    
          all_terms = (Nomenclature::RTMMSTerm.filter(filter).all - send(proxy_getter))
          limit ? all_terms[offset..offset+limit-1] : all_terms
        end
      end
    end
    # same for to-one and to-many
    klass.class_eval do
      define_method(proxy_type) do
        [Nomenclature::RTMMSTerm]
      end
      define_method(proxy_unassociated_count) do |*args|
        send(proxy_unassociated, *args).count # TODO this insanely inefficient and could take wayyy too long
      end
    end
  end # original_methods.each
end # def create_type_proxies

def create_integer_proxies_for klass
  original_methods = klass.associations.select{|getter, vals_hash| (vals_hash[:class] != "DIM::CommonDataTypes::ASN1::OIDType") && (vals_hash[:class].to_const.parents.include?(IEEE20101::INTEGER))}.keys
  original_methods.each do |original_method|
    
    original_method_setter = "#{original_method}=".to_sym
    original_method_type = "#{original_method}_type".to_sym
    proxy_getter = (original_method.to_s + "_proxy").to_sym
    proxy_setter = (original_method.to_s + "_proxy=").to_sym
    proxy_count = (original_method.to_s + "_proxy_count").to_sym
    remove_all_proxy = ("remove_all_" + original_method.to_s + "_proxy").to_sym
    proxy_type = (original_method.to_s + "_proxy_type").to_sym
    
    klass.class_eval do
      derived_attribute(proxy_getter, Fixnum)
      define_method(proxy_getter) do |*args|
        proxied_obj = send(original_method)
        proxied_obj ? proxied_obj.value : nil
      end
      define_method(proxy_setter) do |proxy_value|
        proxied_obj = send(original_method) || klass.associations[original_method][:class].to_const.new
        proxied_obj.value = proxy_value
        proxied_obj.save
        send(original_method_setter, proxied_obj)
      end
      define_method(proxy_count) do |*args|
        send(proxy_getter, *args) ? 1 : 0
      end
      define_method(remove_all_proxy) do
        send(original_method_setter, nil)
      end
      define_method(proxy_type) do
        [Fixnum]
      end
    end
  end
end

def create_string_proxies_for klass
  original_methods = klass.associations.select{|getter, vals_hash| (vals_hash[:class] == "IEEE20101::OCTET_STRING") || (vals_hash[:class].to_const.parents.include?(IEEE20101::OCTET_STRING))}.keys
  original_methods.each do |original_method|
    
    original_method_setter = "#{original_method}=".to_sym
    original_method_type = "#{original_method}_type".to_sym
    proxy_getter = (original_method.to_s + "_proxy").to_sym
    proxy_setter = (original_method.to_s + "_proxy=").to_sym
    proxy_count = (original_method.to_s + "_proxy_count").to_sym
    remove_all_proxy = ("remove_all_" + original_method.to_s + "_proxy").to_sym
    proxy_type = (original_method.to_s + "_proxy_type").to_sym
    # puts "Making #{proxy_getter} for #{klass.name}"
    klass.class_eval do
      derived_attribute(proxy_getter, String)
      define_method(proxy_getter) do |*args|
        proxied_obj = send(original_method)
        proxied_obj ? proxied_obj.value : nil
      end
      define_method(proxy_setter) do |proxy_value|
        proxied_obj = send(original_method) || klass.associations[original_method][:class].to_const.new
        proxied_obj.value = proxy_value
        proxied_obj.save
        send(original_method_setter, proxied_obj)
      end
      define_method(proxy_count) do |*args|
        send(proxy_getter, *args) ? 1 : 0
      end
      define_method(remove_all_proxy) do
        send(original_method_setter, nil)
      end
      define_method(proxy_type) do
        [String]
      end
    end
  end
end

# and now handle all proxies for klass
# FIXME must remove primitives from this array
DIM.classes(:ignore_class_namespaces => true).each do |klass|
  # create_oid_proxies_for klass
  # create_type_proxies_for klass
  # create_integer_proxies_for klass
  # create_string_proxies_for klass
end
################# end oid proxy madness ###################