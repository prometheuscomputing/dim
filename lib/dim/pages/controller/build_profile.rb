# class ProfileWizardController < Gui::Controller
#   layout :default
#   map '/profile/'
#   def public_page?; true; end
#   CHOICE_TYPES = ['one of', 'any of']
#   CHOICE_OR_OBJ = ['objects', 'choice groups']
#   comps = DIM.classes(:ignore_class_namespaces => true).collect{|klass| klass.compositions}.flatten.select{|comp| comp[:class].to_s =~ /DIM/ && comp[:class] !~ /ASN1/}.flatten.uniq
#   klasses = comps.collect{|comp| comp[:class].to_const}.collect do |classifier|
#     if classifier.interface?
#       classifier.implementors
#     else
#       classifier.children << classifier
#     end
#   end.flatten.reject{|klass| klass.abstract? || klass.interface?}.uniq
#   PROFILEABLE_CLASSES = {}
#   klasses.collect{|klass| klass.name.split(/(?<=[a-z])(?=[A-Z])/).join(" ")}.sort
#
#   def index
#     step1
#   end
#
#   # What is the best way to hang on to all of the information we collect as we go through these pages?
#
#   def step1
#     if request.post?
#       params = request.safe_params
#       pp params
#       puts "*"*30 + "step1"
#       if klass = params['profile_class']
#         raw_redirect "step2/#{klass}"
#       end
#     end
#   end
#
#   # We would like to skip this step if there is only one possibility for the role
#   def step2(klass_name)
#     klass = klass_name.to_const
#     @roles = klass.composers.collect{|comp| comp[:singular_opp_getter].to_s}
#     if @roles.count < 1
#       raise "Oh darn.  This didn't work"
#     elsif @roles.count < 2
#       raw_redirect "/profile/step3/#{klass_name}__#{@roles.first}"
#     end
#     if request.post?
#       params = request.safe_params
#       pp params
#       puts "*"*30 + "step1"
#       if role = params['profile_role']
#         raw_redirect "/profile/step3/#{klass_name}__#{role}"
#       end
#     end
#   end
#
#   def step3(klass_name_plus_role)
#     @klass_name, @role = klass_name_plus_role.split('__')
#     klass = @klass_name.to_const
#     home_getter = Gui::Home.home_method_for_type(Nomenclature::RTMMSTerm).to_sym
#     render_options = {
#       :selection_param_name => 'selected_terms',
#       :unassociated_only => false,
#       :expanded => true,
#       :hide_collection_header => true,
#       :standalone => true,
#       :selection_create => false# ,
# #       :object_view_type => @selected_clear_view[:view_type],
# #       :object_view_name => @selected_clear_view[:view_name]
#     }
#     @term_selection = Gui::Director.new.render(Gui::Home.new, home_getter, :Collection, :TermChooser, Nomenclature::RTMMSTerm, render_options)
#     params = request.safe_params
#     # pp params if params
#
#   end
# end