module Gui
  class DevicesHomeController < Gui::Controller
    map '/x73'
    layout('default') { |path, wish| !request.xhr? }
    
    def index
    end
    
    def devices
      specs   = []
      specs << add_spec('pcdprofiles', nil, :expanded => true)
      # specs << Gui::Director.new.render(nil, nil, :Collection, :Summary, MyDevice::PCDProfile)
      
      @devices = render_specs(specs, 'Device Profiles')
    end
    
    def nomenclature(update = false)
      params = request.safe_params
      pp params
      @updated_terms = Nomenclature.update_terms if params['update']
      # if request.post?
      #   @response = Nomenclature.update_terms
      # end
      specs   = []
      specs << add_spec('rtmmsterms', nil, :label => 'Nomenclature (All)')
      specs << add_spec('rtms', :Rosetta, :label => 'Rosetta')
      specs << add_spec('hrtms', :hRTM, :label => 'Harmonized Rosetta')
      specs << add_spec('x73s', :x73, :label => 'x73')
      specs << add_spec('metrics')
      specs << add_spec('units')
      specs << add_spec('unitgroups')
      specs << add_spec('ucumunits')
      specs << add_spec('enumerations')
      specs << add_spec('literals')
      specs << add_spec('tokens')
      specs << add_spec('enumerationgroups')
      specs << add_spec('proposed_terms', nil, :label => 'Newly Propsed Terms (Found only on this website)')
      # @nomenclature = render_specs(specs, 'Nomenclature')
      @nomenclature = render_specs(specs)
    end
    
    def update_terms
      redirect('x73/nomenclature?update=true')
    end
    
    def uml
      specs   = []
      specs << add_spec('dim_classifiers', :dim_classifiers, :label => 'DIM Classes')
      specs << add_spec('asn1_classifiers', :asn1_classifiers, :label => 'ASN.1 Data Types')
      # specs << add_spec('asn1_enums', :asn1_enums, :label => 'ASN.1 Enumerations (INT and BIT STRING)')
      @uml = render_specs(specs, 'Classifer Definitions (UML)')
    end
    
    def standard
      specs   = []
      ['ieeestandards', 'clauses', 'normative_references', 'definitions', 'abbreviations', 'texts', 'examples', 'notes', 'tables', 'figures', 'codes'].each { |getter| specs << add_spec(getter) }
      @standard = render_specs(specs, 'Standard Editor')
    end
    
    def add_spec(getter, view_name = nil, options = {})
      @home  ||= Gui::Home.new
      @props ||= Gui::Home.properties
      view_name ||= :Summary
      view_type = options[:view_type] || :Collection
      # indexing into properties by getter is not foolproof.  we should be alright here due to the way that Gui::Home#add_method works.
      klass = @props[getter.to_sym][:class].to_const
      options[:label] ||= klass.to_title.pluralize
      spec = Gui::Director.new.render(@home, getter, view_type, view_name, klass, options)
      counter = 1
      # spec.each_line {|l| puts "#{counter}: #{l}"; counter += 1}; puts "*"*20
      spec
    end
    
    def render_specs(specs, title = nil)
      locals = {:page_title => title}
      rendered_specs = MainController.render_view(:object_wrapper, :output => specs.join("\n"), :breadcrumbs => [], :locals => locals) { |action| action.options[:is_layout] = true }
      counter = 1
      # rendered_specs.each_line {|l| puts_green "#{counter}: #{l}"; counter += 1}
      rendered_specs
    end
  
  end
end # Gui
