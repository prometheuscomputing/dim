require 'ruby-prof'

class UploadProfile < Gui::Controller
  layout :default
  map '/upload_profile'
  KLASS_TAGS = MyDevice::PCDProfileBuilder::LOOKUP_TAG_BY_CLASS.sort_by do |klass, tags|
    m = klass.metainfo_classifier
    m ||= klass.metainfo_classifier(:fuzzy => true)
    puts_red klass unless m
    m.name
  end
  def public_page?; true; end
  
  # def index
  # end
  
  def pcd_xml
    params = request.safe_params
    if request.post?
      # pp params
      # puts "*"*30
      upload   = params['xml_upload']
      name     = params['profile_name'] || upload[:filename].delete(".xml")
      type     = params['profile_type']
      # TODO convention to distinguish from other profiles with same name
      file     = upload[:tempfile]
      # RubyProf.start
      result   = MyDevice::PCDProfileBuilder.new(file, :submitted_name => name, :submitted_type => type, :device_type => :pcd, :xml_format => :full).build
      # performance = RubyProf.stop
      # printer = RubyProf::MultiPrinter.new(performance)
      # printer.print(:path => ".", :profile => "performance")
      profile  = result[:profile]
      error    = result[:error]
      warnings = result[:warnings]
      if error
        flash[:xml_parse_error] = error
        raw_redirect "/upload_profile/pcd_xml/"
      elsif profile
        flash[:error_messages] = warnings if warnings.any?
        raw_redirect "/MyDevice/PCDProfile/#{profile.id}/"
      else
        raise 'No result from PCD Profile Builder'
      end
    end
  end
  
  def pcd_rch
    @klass_tags = KLASS_TAGS
    params = request.safe_params
    if request.post?
      # pp params
      # puts "*"*30
      upload   = params['rch_upload']
      name     = params['profile_name'] || upload[:filename].delete(".xml")
      type     = params['profile_type']
      # TODO convention to distinguish from other profiles with same name
      file     = upload[:tempfile]
      # RubyProf.start
      result   = MyDevice::PCDProfileBuilder.new(file, :submitted_name => name, :submitted_type => type, :device_type => :pcd, :xml_format => :rch).build
      # performance = RubyProf.stop
      # printer = RubyProf::MultiPrinter.new(performance)
      # printer.print(:path => ".", :profile => "performance")
      profile  = result[:profile]
      error    = result[:error]
      warnings = result[:warnings]
      if error
        flash[:rch_parse_error] = error
        raw_redirect "/upload_profile/pcd_rch/"
      elsif profile
        flash[:error_messages] = warnings if warnings.any?
        raw_redirect "/MyDevice/PCDProfile/#{profile.id}/"
      else
        raise 'No result from PCD Profile Builder'
      end
    end
  end
  
  # *PHD*
  # def phd_rch
  #   params = request.safe_params
  #
  #   if request.post?
  #     # pp params
  #     # puts "*"*30
  #     upload   = params['rch_upload']
  #     name     = params['profile_name'] || upload[:filename].delete(".xml")
  #     type     = params['profile_type']
  #     # TODO convention to distinguish from other profiles with same name
  #     file     = upload[:tempfile]
  #     # RubyProf.start
  #     result   = MyDevice::PHDProfileBuilder.new(file, :submitted_name => name, :submitted_type => type, :device_type => :phd, :xml_format => :rch).build
  #     # performance = RubyProf.stop
  #     # printer = RubyProf::MultiPrinter.new(performance)
  #     # printer.print(:path => ".", :profile => "performance")
  #     profile  = result[:profile]
  #     error    = result[:error]
  #     warnings = result[:warnings]
  #     if error
  #       flash[:rch_parse_error] = error
  #       raw_redirect "/upload_profile/phd_rch/"
  #     elsif profile
  #       flash[:error_messages] = warnings if warnings.any?
  #       raw_redirect "/MyDevice/PHDProfile/#{profile.id}/"
  #     else
  #       raise 'No result from PHD Profile Builder'
  #     end
  #   end
  # end
  #
  def pcd_dsv
    params = request.safe_params

    if request.post?
      # pp params
      # puts "*"*30
      upload   = params['dsv_upload']
      name     = params['profile_name'] || upload[:filename].delete(".xml") #FIXME delete file extension whatever it is...
      type     = params['profile_type']
      # TODO convention to distinguish from other profiles with same name
      file     = upload[:tempfile]
      # RubyProf.start
      result   = MyDevice::DeviceTableParser.build(file, name, type, :pcd)
      # performance = RubyProf.stop
      # printer = RubyProf::MultiPrinter.new(performance)
      # printer.print(:path => ".", :profile => "performance")
      profile  = result[:profile]
      error    = result[:errors] && result[:errors].any?
      warnings = result[:warnings]
      if error
        flash[:dsv_parse_error] = error
        raw_redirect "/upload_profile/pcd_dsv/"
      elsif profile
        flash[:error_messages] = warnings if warnings.any?
        raw_redirect "/MyDevice/PCDProfile/#{profile.id}/"
      else
        raise 'No result from PCD Profile Builder'
      end
    end
  end
  
  # *PHD*  
  # def phd_dsv
  #   params = request.safe_params
  #
  #   if request.post?
  #     # pp params
  #     # puts "*"*30
  #     upload   = params['dsv_upload']
  #     name     = params['profile_name'] || upload[:filename].delete(".xml")#FIXME delete file extension whatever it is...
  #     type     = params['profile_type']
  #     # TODO convention to distinguish from other profiles with same name
  #     file     = upload[:tempfile]
  #     # RubyProf.start
  #     result   = MyDevice::DeviceTableParser.build(file, name, type, :phd)
  #     # performance = RubyProf.stop
  #     # printer = RubyProf::MultiPrinter.new(performance)
  #     # printer.print(:path => ".", :profile => "performance")
  #     profile  = result[:profile]
  #     error    = result[:errors] && result[:errors].any?
  #     warnings = result[:warnings]
  #     if error
  #       flash[:dsv_parse_error] = error
  #       raw_redirect "/upload_profile/phd_dsv/"
  #     elsif profile
  #       flash[:error_messages] = warnings if warnings.any?
  #       raw_redirect "/MyDevice/PHDProfile/#{profile.id}/"
  #     else
  #       raise 'No result from PHD Profile Builder'
  #     end
  #   end
  # end
end