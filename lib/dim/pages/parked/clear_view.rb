class ClearView < Gui::Controller
  map '/clear_view/'
  
  def public_page?
    false
  end
  
  def index
    raw_redirect '/clear_view/select_pcd_profile'
  end
  
  def select_pcd_profile
    @pcd_profiles = MyDevice::PCDProfile.all
  end
  
  def pcd_profile_clear_view
    #error_404 unless request.post?
    pcd_profile_id = request.safe_params['select_pcd_profile']
    raw_redirect "/MyDevice/PCDProfile/#{pcd_profile_id}/?view-type=Document"
  end
end