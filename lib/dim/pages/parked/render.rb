
require 'date'
require 'slave'

# Redefine exit to call exit! so that we don't run into issues with at_exit callbacks from child.
class Slave
  def exit(*args)
    exit!(*args)
  end
end


# Moved PREFACE to model_extensions.rb


# Inherit from gui_builder's customized Ramaze::Controller
class PCDProfile_Render < Gui::Controller
  DEFAULT_PAGE_SIZE = 8
  MAX_PAGE_SIZE = 500
  
  layout :default
  map '/render'  
  def public_page?; true; end
  
  def index
    params = request.safe_params
    first_id = params['pcd_profile']
    if params['render_pcd_profile']
      render_pcd_profile_by_id(first_id.to_i)
    # elsif params['compare_pcd_profiles']
    #   second_id = params['second_use_case']
    #   if second_id && !second_id.empty? && second_id.to_i > 0
    #     compare_pcd_profiles_by_id(first_id.to_i, second_id.to_i)
    #   else
    #     # Handle no selection here
    #   end
    else # No submission. Go to selection page
      @pcd_profiles = sorted_pcd_profiles
      template_path = File.join(File.dirname(__FILE__), '../view/render/pcd_profile_select.haml')
      puts "Found HAML template at #{template_path.inspect}"
      File.read(template_path)
    end
  end
  
  def render_pcd_profile_by_id(id)
    respond render_pcd_profile(MyDevice::PCDProfile[:id => id.to_i])
  end
  
  # def compare_pcd_profiles_by_id(first_id, second_id)
  #   respond compare_pcd_profiles(MyDevice::PCDProfile[:id => first_id.to_i], MyDevice::PCDProfile[:id => second_id.to_i])
  # end
  
  # Currently not implemented fully or used
  # def abort_pcd_profile_comparison(first_id, second_id)
  #   slave = session[:slaves][[MyDevice::PCDProfile[:id => first_id.to_i], MyDevice::PCDProfile[:id => second_id.to_i]]]
  #   slave.shutdown('quiet' => true) if slave
  # end
    
  private
  
  def render_pcd_profile(pcd_profile)
    # Note: Slave#exit has been overridden to not call at_exit hooks (calls exit! instead)
    Slave.object(:psname => "render_pcd_profile_#{pcd_profile.id} (ruby slave)"){pcd_profile.context_insensitive_render_as_html}
  end
  
  # def compare_pcd_profiles(pcd_profile_one, pcd_profile_two)
  #   # Note: Slave#exit has been overridden to not call at_exit hooks (calls exit! instead)
  #   Slave.object(:psname => "compare_pcd_profiles_#{pcd_profile_one.id}_#{pcd_profile_two.id} (ruby slave)"){pcd_profile_one.context_insensitive_render_comparison_with(pcd_profile_two)}
  # end
  
  # TODO: This is copied from gui_builders main.rb, figure out why it isn't inherited
  def send_file_stream(name, file_data, content_type = nil, content_disposition = nil)
    content_type ||= Rack::Mime.mime_type(File.extname(name))
    content_disposition ||= 'attachment'
    
    header = {}
    header['Content-Length'] = file_data.length.to_s
    header['Content-Type'] = content_type
    header['Content-Disposition'] = "#{content_disposition}; filename = #{name}"

    respond file_data, 200, header
  end
  
  def sorted_pcd_profiles
    MyDevice::PCDProfile.all.sort{|a,b| a.description.to_s <=> b.description.to_s}
  end
end