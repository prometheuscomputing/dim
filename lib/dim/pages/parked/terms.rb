class Terms < Gui::Controller
  layout :default
  map '/terms'  
  def public_page?; true; end
  def index
    params = request.safe_params
    pp params
    @response ||= nil
    if request.post?
      @response = Nomenclature.update_terms
    end
  end
end
