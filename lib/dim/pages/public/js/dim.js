function block_form() {
  // big_spinning_blocker_on();
  if ($("form :invalid").length > 0) {
    event.preventDefault();
  }
  $.blockUI({  overlayCSS: { backgroundColor: '#B0B0B0' }, message: '<img src="/images/dim_spinner_caduceus.gif" />' });
}
$(document).ready(function() {
  $('#submit_profile_upload').submit(function(event){
    block_form();
  });
  
  $('#update_terms_form').submit(function(event){
    block_form();
  });
});
