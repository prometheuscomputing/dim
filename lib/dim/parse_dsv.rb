require 'csv'
require 'pp'
module MyDevice
  class DeviceTableParser
    DEFAULT_COL_SEP = "\t"
    class << self
      # filepath = File.expand_path("~/projects/dim_reference/tsv_parse_test.txt")
      def build(file, name, type, pcd_or_phd)
        opts = {:name => name, :type => type}
        opts[:pcd] = true if pcd_or_phd == :pcd
        opts[:phd] = true if pcd_or_phd == :phd
        # FIXME this only gives back the first profile!
        profile = parse_dsv(file, opts).first
        {:profile => profile, :errors => [], :warnings => []}
      end

      def parse_dsv(filepath, opts = {})
        raise 'You must specify either opts[:pcd] or opts[:phd] when parsing delimiter separated values' unless opts[:pcd] || opts[:phd]
        hashes = dsv_to_hash(filepath, opts)
        # return devices
        devices = []
        hashes.each{|hash| hash.each {|root_element_type, data| devices << (opts[:pcd] ?  build_pcd_device(root_element_type, data, opts) : build_phd_device(root_element_type, data, opts))}}
        # Now (for each device) make a profile of the correct type and stuff the device into it
        profiles = devices.collect{|device| opts[:pcd] ? wrap_device_in_pcd_profile(device, opts) : wrap_device_in_phd_profile(device, opts)}
        profiles
      end

      def wrap_device_in_pcd_profile(device, opts)
        ChangeTracker.start
        profile              = MyDevice::PCDProfile.new
        profile.profile_root = device
        profile.name         = opts[:name] if opts[:name]
        profile.profile_type = opts[:type] if opts[:type]
        profile.save
        ChangeTracker.commit
        profile
      end
      
      def get_term(data)
        refid = data[:refid]
        code  = data[:code]
        if refid && !refid.empty?
          term = Nomenclature.find_term_by_ref_id(refid)
        elsif code
          term = Nomenclature.find_term_by_cf_code(code)
          # puts "Code: #{code.inspect} -- #{term.inspect}"
        else
          # FIXME
          # "No refid or code provided for #{data}"
        end
      end
      
      def get_proposed_term(data)
        refid = data[:refid]
        pterm = Nomenclature.find_proposed_term_by_ref_id(refid)
        # results[:warnings] << "#{refid} is not available in the Device Profiling Tool. An existing Proposed Term was used"
        if refid && !pterm
          ChangeTracker.start
          pterm = Nomenclature::ProposedType.create(:reference_id => refid)
          ChangeTracker.commit
          pterm.note_creation
          # results[:warnings] << "#{refid} is not available in the Device Profiling Tool. A new Proposed Term was created"
        end
        pterm
      end

      # Need to figure out (if necessary) how to deal with implied vmds and channels
      def build_phd_device(element_type, data, opts)
        case element_type
        when :mds

        when :vmd
        when :channel
        when :metric
        end
      end

      # Need to figure out (if necessary) how to deal with implied vmds and channels
      def build_pcd_device(element_type, data, opts)
        # puts "Element type is #{element_type}"
        # pp data
        case element_type
        when :mds
          # We will assume that it is a SingleBedMDS
          ChangeTracker.start
            component = DIM::System::SingleBedMDS.new
            component.save
          ChangeTracker.commit
          term = get_term(data)
          if term
            ChangeTracker.start
              component.system_type_term = term
              component.save
            ChangeTracker.commit
          else
            ChangeTracker.start
              pterm = get_proposed_term(data)
              component.proposed_type = pterm
              component.save
            ChangeTracker.commit
          end
          vmds = Array(data[:children]).collect{|child| build_pcd_device(:vmd, child[:vmd], opts)}
          ChangeTracker.start
            puts "The MDS gets #{vmds.count} vmds: #{vmds.inspect}"
            component.vmds = vmds
            component.save
          ChangeTracker.commit
          component
        when :vmd          
          ChangeTracker.start
            component = DIM::Medical::VMD.new
            component.save
          ChangeTracker.commit
          term = get_term(data)
          if term
            ChangeTracker.start
              component.type_term = term
              component.save
            ChangeTracker.commit
          else
            ChangeTracker.start
              pterm = get_proposed_term(data)
              component.proposed_type = pterm
              component.save
            ChangeTracker.commit
          end
          channels = Array(data[:children]).collect{|child| build_pcd_device(:channel, child[:channel], opts)}
          ChangeTracker.start
            puts "The VMD gets #{channels.count} channels: #{channels.inspect}"
            component.channels = channels
            component.save
          ChangeTracker.commit
          component
        when :channel          
          ChangeTracker.start
            component = DIM::Medical::Channel.new
            component.save
          ChangeTracker.commit
          term = get_term(data)
          if term
            ChangeTracker.start
              component.type_term = term
              component.save
            ChangeTracker.commit
          else
            ChangeTracker.start
              pterm = get_proposed_term(data)
              component.proposed_type = pterm
              component.save
            ChangeTracker.commit
          end
          metrics = Array(data[:children]).collect{|child| build_pcd_device(:metric, child[:metric], opts)}
          ChangeTracker.start
            component.metrics = metrics
            component.save
          ChangeTracker.commit
          component
        when :metric
          # puts "METRIC"
          # puts; pp data
          term = get_term(data)
          if term
            ChangeTracker.start
              # puts "Found term is: #{term.inspect}"
              component = DIM::Medical::Numeric.new
              component.type_term = term
              component.save
            ChangeTracker.commit
          else
            pterm = get_proposed_term(data)
            ChangeTracker.start
              component = DIM::Medical::Numeric.new
              component.proposed_type = pterm
              component.save
            ChangeTracker.commit
          end
          puts "\nRETURNING A #{component.class}\n" if component.class.to_s =~ /VMD/
          component
        end
      end

      def dsv_to_hash(filepath, opts)
        parsed_rows = []
        CSV.foreach(filepath, :col_sep => opts[:delimiter] || DEFAULT_COL_SEP) do |row|
          parsed_rows << row if row.any?
        end

        roots           = []
        current_mds     = nil
        current_vmd     = nil
        current_channel = nil

        # These defaults should really be set in constants or something instead of being magic numbers
        refid_col     = opts[:refid_col] || 0
        cf_code10_col = opts[:cf_code10_col] || 1
        uom_col       = opts[:uom_col] || 2
        enums_col     = opts[:enums_col] || 5

        parsed_rows.each do |row|
          refid = row[refid_col]
          next if refid.strip == 'REFID' # this gets rid of the header if the first column header is in fact, 'REFID'
          this_rows_dot_level = refid.count('.') if refid # this assumes that there are no dots in the refid

          refid     = refid.delete('.').strip if refid
          cf_code10 = row[cf_code10_col]

          case this_rows_dot_level
          when 0 # mds
            current_mds = {:mds => {:refid => refid, :children => []}}
            current_vmd = current_channel = nil
            current_mds[:mds][:cf_code10] = cf_code10 if cf_code10
            roots << current_mds
          when 1 # vmd
            current_vmd = {:vmd => {:refid => refid, :children => []}}
            current_mds[:mds][:children] << current_vmd
            current_channel = nil
            current_mds[:vmd][:cf_code10] = cf_code10 if cf_code10
          when 2 # channel
            current_channel = {:channel => {:refid => refid, :children => []}}
            case # PHD isn't bound to the same strict hierarchy of MDS > VMD > Channel > Metric that PCD is
            when current_vmd
              current_vmd[:vmd][:children] << current_channel
            else
              current_mds[:mds][:children] << current_channel
            end
            current_channel[:channel][:cf_code10] = cf_code10 if cf_code10
          when 3 # metric
            metric = {:metric => {:refid => refid}}
            case # PHD isn't bound to the same strict hierarchy of MDS > VMD > Channel > Metric that PCD is
            when current_channel
              current_channel[:channel][:children] << metric
            when current_vmd
              current_vmd[:vmd][:children] << metric
            else
              current_mds[:mds][:children] << metric
            end
            units  = row[uom_col]
            enums  = row[enums_col]
            metric[:metric][:cf_code10] = cf_code10 if cf_code10
            metric[:metric][:units] = units if units
            metric[:metric][:enums] = enums if enums
          end
        end
        roots
      end
    end
  end
end