


# getter and setter hooks don't seem to be inherited so we are handling them like this
phd_classes = MyDevice::PHDTop.children
phd_classes.each do |klass|

  klass.send(:add_attribute_setter, :cardinality) do |card|
    card = "1" if (card == "0" || card == "" || card == nil) # Should this happen? TODO
    return card = "1" if card =~ /[^0-9\-\.\* ]/
    card = card.gsub(/-+/, "..").gsub(/\.+/, "..").gsub(" ","") if card
    card
  end
  
  klass.send(:add_attribute_getter, :cardinality) do |cardinality|
    cardinality || "1"
  end

  # setup hooks for proxied attributes of type OID-Type and TYPE
  proxied_properties = klass.properties.select{|k,v| v[:additional_data] && v[:additional_data][:prometheus] && v[:additional_data][:prometheus]["proxy_property"]  && v[:additional_data][:modeled_as] != :attribute_reciprocal}
  proxied_properties.each do |property_key, property_info|
    
    proxied_property_getter = property_info[:getter]
    proxied_property_setter = (proxied_property_getter.to_s + "=").to_sym
    proxy_getter = property_info[:additional_data][:prometheus]["proxy_property"].first.gsub("-", "_").to_sym

    if property_info[:class].to_const <= PHD::ASN1::CommonDataTypes::TYPE
      # puts "Adding assoc_adder to #{klass} -- proxy_getter is #{proxy_getter} -- proxied_property_setter is #{proxied_property_setter}"
      klass.send(:add_association_adder, proxy_getter) do |term|
        self.send(proxied_property_setter, term.term_code)
        term
      end
      klass.send(:add_association_remover, proxy_getter) do |term|
        self.send(proxied_property_setter, nil)
        term
      end
    elsif property_info[:class].to_const <= PHD::ASN1::CommonDataTypes::TYPE
      klass.send(:add_association_adder, proxy_getter) do |term|
        type = self.send(proxied_property_getter) || PHD::ASN1::CommonDataTypes::TYPE.create
        type.code = term.term_code
        type.code_term = term
        type.partition = term.partition
        type.save # is this necessary? TODO
        send(proxied_property_setter, type)
        if respond_to?(:allowed_enumerations) && (term.respond_to?(:collected_literals))
          if allowed_enumerations.empty?
            term.collected_literals.each{|t| add_allowed_enumeration t}
          end
        elsif respond_to?(:allowed_units) && (term.respond_to?(:collected_units))
          if allowed_units.empty?
            term.collected_units.each{|t| add_allowed_unit t}
          end
        end
        term
      end
      klass.send(:add_association_remover, proxy_getter) do |term|
        type = self.send(proxied_property_getter)
        self.send(proxied_property_setter, nil)
        type.destroy if type
        if respond_to?(:allowed_enumerations)
          remove_all_allowed_enumerations
        elsif respond_to?(:allowed_units)
          remove_all_allowed_units
        end
        term
      end
    else
      raise "Class #{klass} had a proxy: #{proxy_getter}, for a property: #{proxied_property_getter}, that had unexpected type: #{property_info.reject{|k,v| k == :methods}.inspect}"
    end
  end
end

module MyDevice
  SCHEMA_PREFIX = 'MyDevice' unless defined?(SCHEMA_PREFIX)
  SCHEMA_URI = "http://prometheuscomputing.com/schemas/PHD/MyDevice" unless defined?(SCHEMA_URI)
  class PHDTop
    # TODO Verify that this is fixed... this needs to be here but it stops us from populating #used_attributes at the same time we populate #available_attributes

    # add_association_options(:used_attributes, :subset_of => 'available_attributes')

    # def self.ignored_json_getters_for_profile
    #   # TODO we may need to grab attribute specializations depending on what type of profile or something
    #
    #   # model_class is probably redundant by dint of the fact that we already know what the class is because we are an instance of that class.  Problem being that we do loose namespacing...
    #
    #   #all_property_names = [:profile_summary, :name, :cardinality, :valid_terms, :class_reserved, :name_binding, :model_class, :ref_id, :available_attributes, :used_attributes, :attribute_specializations]
    #   composer_getters = composers.collect{|c| c[:getter]}
    #   modeled_getters = [:profile_summary, :valid_terms, :model_class, :available_attributes, :attribute_specializations, :used_attributes, :name_in_profile]
    #   proxy_getters = instance_methods.select{|m| m.to_s =~ /_(term|proxy)$/}
    #   modeled_getters | proxy_getters | composer_getters
    # end

    def locally_defined_getters(added_getters = [])
      [:cardinality] | []
    end

    # def used_getters(added_getters = [])
    #   used_asn1_getters = used_attributes.collect{|a| a.referenced_property.name.downcase.gsub(/-+/,"_").to_sym}
    #   top_getters = self.class.associations.select{|k, v| v[:class].to_const.ancestors.include? DIM::Top::Top}.collect{|k, v| v[:getter]}
    #   used_top_getters = top_getters.select{|tg| count = (tg.to_s + "_count").to_sym; self.send(count) > 0}
    #   used_top_getters | added_getters | used_asn1_getters | locally_defined_getters
    # end

    # def after_change_tracker_create
    #   setup_instance
    #   super
    # end
    #
    # def after_change_tracker_save
    #   set_used_attributes
    #   super
    # end
    #
    # def setup_instance
    #   # puts "calling setup_instance from #{self.class}-#{self.object_id}"
    #   # populate things that really should be meta_info
    #   set_metainfo
    #
    #   # setup attribute specializations and useage
    #   needs_specializations = self.attribute_specializations_count == 0
    #   needs_attribute_usage = available_attributes_count == 0 #&& !normative?
    #   used_attrs = []
    #   if needs_specializations
    #     meta_properties = get_meta_properties
    #     specializations = []
    #     available_attrs = []
    #     meta_properties.each do |mp|
    #       next unless mp.defined_in_asn1_package
    #       name = fix_dim_attribute_name(mp.name)
    #
    #       opt = mp.optionality
    #       specialization = MyDevice::AttributeSpecialization.create(:name => name)
    #       specialization.optionality = opt
    #       specialization.referenced_property = mp
    #       specialization.save
    #       specializations << specialization
    #
    #       available_attr = MyDevice::AttributeInclusion.create(:name => name)
    #       available_attr.referenced_property = mp
    #       available_attr.save
    #       available_attrs << available_attr
    #     end
    #     self.attribute_specializations = specializations
    #     self.available_attributes = available_attrs
    #   end
    #   # If this is really slow then there should be a derived field for optionality on AttributeInclusion
    #   if needs_attribute_usage
    #     self.used_attributes = self.available_attributes.select do |attr|
    #       enum_literal = attr.referenced_property.optionality
    #       selected = enum_literal.value =~ /^m/i if enum_literal
    #     end
    #   end
    #
    # end
    #
    # # TODO TODO TODO TODO write code to go through all of the asn1_getters (of every top class) and make sure that we can match a MetaInfo::Property to a getter for every getter (see below). if that works then set an instance variable on each AttributeInclusion that tells what its getter is.
    #
    # # TODO this has some hinky stuff in it for a temporary workaround
    # def set_used_attributes
    #   initialized_getters = asn1_getters.select do |getter|
    #     counter = (getter.to_s + "_count").to_sym
    #     c = self.send(counter) if self.respond_to?(counter)
    #     c = self.send(getter) if self.respond_to?(getter) && c.nil?
    #     if c.nil?
    #       plural_getter = (getter.to_s + "s").to_sym # Well that is pretty terrible but we should need to do this anyway
    #       c = self.send(plural_getter) if self.respond_to? plural_getter
    #     end
    #     c = nil if c == 0
    #     c
    #   end
    #   # FIXME if you can...this bit here about needing to do a gsub is just scary.  Instead we should have a naming service that ensures that the getter name is properly computed.  We might even store the getter on the Property (in addition to the name)
    #   # puts "#{self.class} -- initialized_getters #{initialized_getters}"
    #   inclusions_by_initialization = available_attributes.select{|aa| initialized_getters.include? aa.referenced_property.name.downcase.gsub("-","_").to_sym}
    #   # puts "#{self.class} -- inclusions_by_initialization #{inclusions_by_initialization.collect{|i| i.referenced_property.name.downcase.gsub("-","_").to_sym}}"
    #   newly_used_attributes = inclusions_by_initialization - used_attributes
    #   # puts "#{self.class} -- newly_used_attributes #{newly_used_attributes.collect{|i| i.referenced_property.name.downcase.gsub("-","_").to_sym}}"; puts
    #   newly_used_attributes.each{|nua| used_attributes_add nua; nua.save}
    # end

    derived_attribute(:mandatory_attribute_names, ::String)
    def mandatory_attribute_names
      return "UNDER CONSTRUCTION" # FIXME
      mandatories = attribute_specializations.select{|as| opt = as.optionality; opt ? opt.value =~ /mandatory/i : false}.collect{|as| as.name}.join(", ")
    end
    
    
    def self.asn1_getters
      if @asn1_getters.nil? || @asn1_getters.empty?
        @asn1_getters = properties.select{|k,v| v[:class].to_s =~ /ASN/}.collect{|k,v| v[:getter]}
      end
      @asn1_getters ||= []
    end

    def asn1_getters
      self.class.asn1_getters
    end

    # TODO this should really do something more useful and intelligent when there isn't one matching meta_class
    # def get_meta_properties
    #   meta_klasses = MetaInfo::Class.where(:name => self.simple_class_name).all
    #   if meta_klasses.count > 1
    #     puts "Too many meta_classes for class #{self.class}: #{meta_klasses.inspect}"
    #     return []
    #   end
    #   if meta_klasses.count < 1
    #     puts "No meta_class for class #{self.class}"
    #     return []
    #   end
    #   meta_klass = meta_klasses.first
    #   meta_klass.all_properties
    # end
    #
    # def set_metainfo
    #   # puts "calling set_metainfo from #{self.class}-#{self.object_id}"
    #   if self.model_class_count < 1
    #     mc = MetaInfo::Class.where(:name => self.simple_class_name).first
    #     if mc
    #       # puts "about to set model_class from #{self.class}-#{self.object_id}"
    #       mc.instances_add self
    #       # self.model_class = mc
    #       # puts "set #{self.class}.model_class to: #{mc.inspect}"
    #       if mc.name_binding && mc.name_binding.strip =~ /^handle.*$/i
    #         # handle_term_code = MetaInfo::Class.where(:name => 'Handle').first.term.term_code.to_s # This illustrates the point
    #         # self.name_binding =
    #         t = Nomenclature::RTMMSTerm.where(:reference_id => 'MDC_ATTR_ID_HANDLE').first
    #         self.name_binding_term = t
    #         t.save
    #         save
    #       end
    #       if term = mc.term
    #         self.class_term = term
    #         term.save
    #         save
    #       end
    #       mc.save
    #     end
    #   end
    # end

    # FIXME
    def normative?
      # return false # TODO comment out in order to switch behavior on type of PCD_Profile
      
      # TODO: this is currently always returning nil for object creation pages.
      # assoc-id and assoc-classifier URL params must instead be specified using pending association addition functionality
      parent = composer
      return false unless parent
      parent.normative?
    end

    def _type_term
      if self.respond_to?(:system_type) || self.respond_to?(:type)
        typ = self.type_term
        puts "typ is #{typ.inspect}"
        typ
      else # not everything has a type or system_type attribute
        puts "NOOOOOOO"
        typ = nil
      end
    end
    
    def display_type
      if self.respond_to?(:type) || self.respond_to?(:system_type)
        if term = _type_term
          puts term.inspect
          if ref_id = term.reference_id
            ref_id
          else
            "TERM IS MISSING REFID"
          end
        elsif pt = proposed_type
          pt.reference_id
        end
      else
        nil
      end
    end
    
    def html_display_type
      if self.respond_to?(:type) || self.respond_to?(:system_type)
        if term = _type_term
          puts "#{term.reference_id} -- #{term.status.value}"
          if ref_id = term.reference_id
            if term.status.value =~ /Approved/
              "<font style='background-color: chartreuse'>#{ref_id}</font> "
            elsif term.status.value =~ /Proposed/
              "<font style='background-color: yellow'>#{ref_id}</font> "
            else
              "<font style='background-color: violet'>#{ref_id}</font> "
            end
          else
            "<font style='background-color: red'>term needed</font> "
          end
        elsif pt = proposed_type
          if pt.found_in_rtmms
            "<font style='background-color: magenta'>#{pt.reference_id}</font> "
          else
            "<font style='background-color: orange'>#{pt.reference_id}</font> "
          end
        end
      else
        nil
      end
    end

    def display_name
      if c = cardinality
        "#{html_display_type}#{self.class.name.demodulize} (#{c})"
      else
        "#{html_display_type}#{self.class.name.demodulize}"
      end
    end

    def class_ref_id
      if mc = model_class
        mc.registered_as
      else
        begin
          message = "unknown error"
          meta_classes = MetaInfo::Class.where(:name => self.simple_class_name).all
          message = "too many meta_classes match #{self.simple_class_name}: #{meta_classes.inspect}" if meta_classes.count > 1
          message =  "can not find a meta_class for #{self.simple_class_name}" if meta_classes.count < 1
          meta_class = meta_classes.first
          if answer = meta_class.registered_as
            answer
          else
            # puts "meta_class is #{meta_class}"
            term = meta_class.term
            message = "could not find term for metaclass #{meta_class.inspect}" unless term
            ref_id = term.reference_id if term
            message = "the term of the metaclass for #{self.simple_class_name} did not have a value for reference_id" unless ref_id
            message = "the term of the metaclass for #{self.simple_class_name} had an empty value for reference_id" if ref_id.nil? || ref_id.empty?
            ref_id
          end
        rescue Exception => e
          puts message
          puts e.message
          puts e.backtrace
          "ref_id needed"
        end
      end
    end

    # def composition_cardinality
    #   cardinality_hash = {}
    #   # getters = self.class.associations.collect{|a| a[1][:getter]}
    #   specialization_assocs = self.property_specializations_associations
    #   specialization_assocs.each do |sa|
    #     # keys are getters, values are cardinality of that getter
    #     cardinality_hash[sa[:to][:name].downcase.to_sym] = sa[:through][:cardinality]
    #   end
    #   cardinality_hash
    # end

    def containment_tag_name
      case
      when kind_of?(PHD::Metric)
        "Metric"
      when kind_of?(PHD::MDS)
        "MDS"
      else
        simple_class_name
      end
    end

    def simple_class_name
      return self.class.to_s.demodulize
    end

    # def fix_dim_attribute_name name
    #   parts = name.gsub(/_dim$/, "").gsub(/_RESERVED/, "").split(/_|-/)
    #   fixed_name = parts.each{|p| p.capitalize!}.join("-")
    # end

    def dim_obj_aggregations
      self.aggregations.select{|c| c.is_a? PHD::PHDTop}
    end

    def tops type_hash = {}
      dim_obj_aggregations.each{|tc| tc.tops type_hash}
      type_hash[self.class] ||= []
      type_hash[self.class] << self
      type_hash
    end

    # def tops_report_hash
    #   report = {ref_ids: [], classes: {}}
    #   tops.each do |klass, instances|
    #     # TODO we could do some sanity checking here, e.g. make sure each obj has a name (not empty or nil)
    #     names = instances.collect{|instance| instance.name_in_profile}.compact
    #     common_asn1_attributes = instances.collect{|instance| instance.used_attributes.collect{|ua| ua.name}}.inject(:&)
    #     report[:classes][klass.to_s.demodulize] = {count: instances.uniq.count, names: names.uniq.sort, common_attributes: common_asn1_attributes}
    #     instances.each do |instance|
    #       term = instance._type_term
    #       r_id = term.reference_id if term
    #       report[:ref_ids] << r_id if r_id
    #     end
    #   end
    #   report
    # end

    # def tops_report
    #   trh = tops_report_hash
    #   str = ""
    #   str << "Contained Components:\n" if trh[:classes].any?
    #   trh[:classes].to_a.reverse.each do |klass|
    #     klass_name = klass[0]
    #     info_hash = klass[1]
    #     singular = info_hash[:count] < 2
    #     str << "  There #{singular ? 'is' : 'are'} #{info_hash[:count]} #{klass_name} object#{singular ? '' : 's'}: #{info_hash[:names].uniq.to_sentence_list}\n"
    #   end
    #   # str << "Terms used in profile: #{trh[:ref_ids].uniq.to_sentence_list}" if trh[:ref_ids].any?
    #   str << "Terms used in profile:\n" if trh[:ref_ids].any?
    #   trh[:ref_ids].uniq.sort.each {|term| str << "  #{term}\n"}
    #   str
    # end
    
    def ref_id_checked
      if self.respond_to?(:type) || self.respond_to?(:system_type)
        r = _type_term
        str = r.reference_id if r
        str ||= "NO TERM PROVIDED"
        str
      else
        "N/A"
      end
    end
    
    def containment_report
      lines = ["#{name} (#{self.class.name.demodulize} => #{ref_id_checked})"]
      aggs = dim_obj_aggregations
      if aggs.any?
        aggs_lines = aggs.collect{|agg| agg.channel_report}.flatten
        aggs_lines.each{|al| lines << indent_for_containment_report(al)}
      end
      lines
    end
    
    def containment_report_textile_table_rows(stop_at_channels = true)
      textile = "|#{name_in_profile} (#{self.class.name.demodulize})| #{ref_id_checked} |\n".textile_indent_table_cell
      if stop_at_channels && self.is_a?(DIM::Medical::Channel)
        # do nothing at this point
      else
        aggs = dim_obj_aggregations
        if aggs.any?
          aggs_textile = aggs.collect{|agg| agg.containment_report_textile_table_rows}
          aggs_textile.each{|at| at.each_line{|line| textile << "#{line.textile_indent_table_cell}"}}
        end
      end
      textile
    end
    
    def channel_report
      # term_obj = _type_term
      if self.respond_to?(:type) || self.respond_to?(:system_type)
        term = ref_id_checked
        lines = ["Name:  #{name_in_profile}", "Reference ID:  #{term}"]
      else
        lines = ["Name:  #{name_in_profile}", "Class: #{self.class.name}"]
      end
      aggs = dim_obj_aggregations
      if aggs.any?
        aggs_lines = aggs.collect{|agg| agg.channel_report}.flatten
        aggs_lines.each{|al| lines << indent_for_channel_report(al)}
      end
      lines << "\n"
      lines
    end
    
    # build textile for a report that is styled after the one that ICS Generator provided
    def channel_report_textile
      # term_obj = _type_term
      if self.respond_to?(:type) || self.respond_to?(:system_type)
        term = ref_id_checked
        textile = "h3. #{simple_class_name}: #{name_in_profile}\n\nh5. #{term}\n\n"
      else
        textile = "h3. #{simple_class_name}: #{name_in_profile}\n\n"
      end
      aggs = dim_obj_aggregations
      if aggs.any?
        aggs_textiles = aggs.collect{|agg| agg.channel_report_textile}
        aggs_textiles.each{|at| textile << at}
      end
      textile << "\n\n"
      textile
    end
    
    # TODO methods to optional attributes.  alias mandatory_attribute_names to mandatory_attributes so that it makes for a nice tag in the xml.  Also must get these methods included in json_getters
    
    def indent_for_channel_report str
      "  " + str
    end
    
    def indent_for_containment_report str
      "  " + str
    end
    
    def parent_profile
      parent = composer
      # parent = pending_composer unless parent # TODO this works but apparently we shouldn't have to do this.  #composer should handle pending associations.  Will investigate SSA.
      if parent
        if parent.is_a? MyDevice::PCDProfile
          return parent
        else
          return parent.parent_profile
        end
      else
        nil
      end
    end
    
    derived_attribute(:profile_summary, ::String)
    def profile_summary
      if pprof = parent_profile
        pprof.summary_for_children
      else
        "Summary should be displayed after saving this object.  In the near future it will be displayed without needed to save this new object."
      end
    end
    
  end # class PHDTop
end

module PHD
  class Metric

    # build string data for a report that is styled after the one that ICS Generator provided
    def channel_report
      term_obj = _type_term
      term = ref_id_checked
      term_class = term_obj.class if term_obj
      if term_class =~ /Metric$/
        linked_metrics_label = "Units: "
        linked_metrics = term_obj.units.collect{|u| u.reference_id}
      elsif term_class =~ /Enumeration$/
        linked_metrics_label = "Enumerations: "
        linked_metrics = term_obj.enums.collect{|u| u.reference_id}
      end
      lines = ["Name:  #{name_in_profile}", "Reference ID:  #{term}"]
      if term_class
        lines << "#{linked_metrics_label}:"
        if linked_metrics
          linked_metrics.each{|lm| lines << indent_for_channel_report(lm)}
        else
          lines << "*none found*"
        end
      end
      lines << "\n"
      aggs = dim_obj_aggregations
      if aggs.any?
        aggs_lines = aggs.collect{|agg| agg.channel_report}.flatten
        aggs_lines.each{|al| lines << indent_for_channel_report(al)}
      end
      lines << "\n"
      lines
    end
    
    def channel_report_textile
      term_obj = _type_term
      term = ref_id_checked
      term_code = term_obj.term_code if term_obj
      term_code ||= ""
      term_class = term_obj.class if term_obj
      if term_class.to_s =~ /Metric$/
        linked_metrics = term_obj.units.collect{|u| u.reference_id}
      elsif term_class.to_s =~ /Enumeration$/
        linked_metrics = term_obj.enums.collect{|u| u.reference_id}
      end
      textile = "|^.   #{name_in_profile}  |^.   #{term}(#{term_code})  |^.   " 
      if term_class =~ /Metric$/
        linked_metrics = term_obj.units.collect{|u| u.reference_id}
        linked_metrics.each{|lm| textile << " #{lm}<br> "}
        textile << "|^.   |\n"
      elsif term_class =~ /Enumeration$/
        textile << "|^.   "
        linked_metrics = term_obj.literals.collect{|u| u.reference_id}
        linked_metrics.each{|lm| textile << " #{lm}<br> "}
        textile << "|\n"
      else
        textile << "|^.   |\n"
      end
      # puts; puts "table row before: #{textile.inspect}"
      textile = textile.textile_pad_table_row
      # puts "table row after: #{textile.inspect}"
      aggs = dim_obj_aggregations # i.e. this metric is a complex metric and contains other metrics
      if aggs.any?
        aggs_textiles = aggs.collect{|agg| agg.channel_report_textile}
        aggs_textiles.each{|at| textile << at} # how to indent the entire bit here within a table? TODO
      end
      textile
    end
    
  end # class Metric
end # module PHD