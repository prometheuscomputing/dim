# These associations are ordered for the purposes of the device profile editor application.  They are not denoted as being ordered in the UML because that would mean changing the standard and we don't want to do that.
# SCO, Channel to Alert
class DIM::System::MDS < DIM::System::VMS
  one_to_many :'DIM::Medical::VMD', :getter => :vmds, :opp_getter => :mds, :ordered => true, :composition => true, :display_name => 'VMDs', :additional_data => {:modeled_as=>:association, :cardinality_lower=>1, :cardinality_upper=>Infinity}
  one_to_many :'DIM::ExtendedServices::Scanner', :getter => :scanners , :opp_getter => :mds, :ordered => true, :composition => true, :display_name => 'Scanners', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>Infinity}
  one_to_many :'DIM::System::Battery', :getter => :batteries , :opp_getter => :mds , :composition => true, :ordered => true, :display_name => 'Batteries', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  # one_to_many :'DIM::System::Log', :getter => :logs , :opp_getter => :mds , :composition => true, :ordered => true, :display_name => 'Logs', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  # one_to_many :'DIM::Communication::CommunicationController', :getter => :communication_controllers , :opp_getter => :mds , :composition => true, :display_name => 'Communication Controllers', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>Infinity}
  one_to_many :'DIM::Control::SCO', :getter => :scos , :opp_getter => :mds, :ordered => true, :composition => true, :display_name => 'Scos', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>Infinity}
end

class DIM::Control::SCO < DIM::Medical::VMO
  many_to_one :'DIM::System::MDS', :getter => :mds , :opp_getter => :scos , :opp_is_ordered => true, :display_name => 'MDS', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end

class DIM::System::Battery < DIM::Top::Top
  many_to_one :'DIM::System::MDS', :getter => :mds , :opp_getter => :batteries, :opp_is_ordered => true, :display_name => 'Mds', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end

class DIM::ExtendedServices::Scanner < DIM::Top::Top
  many_to_one :'DIM::System::MDS', :getter => :mds , :opp_getter => :scanners , :opp_is_ordered => true, :display_name => 'MDS', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end

class DIM::Medical::VMD < DIM::Medical::VMO
  one_to_many :'DIM::Medical::Channel', :getter => :channels, :opp_getter => :vmd, :ordered => true, :composition => true, :display_name => 'Channels', :additional_data => {:prometheus=>{"dim_qualifier"=>[nil]}, :modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  many_to_one :'DIM::System::MDS', :getter => :mds , :opp_getter => :vmds , :display_name => 'MDS', :opp_is_ordered => true, :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end

class DIM::Medical::Channel < DIM::Medical::VMO
  many_to_one :'DIM::Medical::VMD', :getter => :vmd , :opp_getter => :channels, :opp_is_ordered => true, :display_name => 'Vmd', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  one_to_many :'DIM::Medical::Metric', :getter => :metrics , :opp_getter => :channel , :composition => true, :display_name => 'Metrics', :ordered => true, :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  one_to_many :'DIM::Alert::Alert', :getter => :alerts , :opp_getter => :channel, :ordered => true, :composition => true, :display_name => 'Alerts', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>Infinity}
end

class DIM::Medical::Metric < DIM::Medical::VMO
  many_to_one :'DIM::Medical::Channel', :getter => :channel , :opp_getter => :metrics , :opp_is_ordered => true, :display_name => 'Channel', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
  many_to_one :'DIM::Medical::ComplexMetric', :getter => :complex_metric , :opp_getter => :metrics, :opp_is_ordered => true, :display_name => 'Complex Metric', :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end

class DIM::Medical::ComplexMetric < DIM::Medical::Metric
  one_to_many :'DIM::Medical::Metric', :getter => :metrics , :opp_getter => :complex_metric , :ordered => true, :composition => true, :display_name => 'Metrics', :additional_data => {:modeled_as=>:association, :cardinality_lower=>1, :cardinality_upper=>Infinity}
end

class DIM::Alert::Alert < DIM::Medical::VMO
  many_to_one :'DIM::Medical::Channel', :getter => :channel , :opp_getter => :alerts , :display_name => 'Channel', :opp_is_ordered => true, :additional_data => {:modeled_as=>:association, :cardinality_lower=>0, :cardinality_upper=>1}
end