require 'builder'
require 'json_graph/xml_support'
    
module MyDevice
  ALL_DIM_CLASSES = DIM.classes(no_imports:true).uniq.reject{|k| k.to_s =~ /ASN1/ || k.join_class?}
  INSTANTIABLE_DIM_CLASSES = ALL_DIM_CLASSES.reject{|k| k.interface? || k.abstract?}
  
  DIM_GETTER_OVERRIDES_BY_CLASS = {}
  def self.default_overrides
    DIM_GETTER_OVERRIDES_BY_CLASS
  end

  C4MI_GETTER_OVERRIDES_BY_CLASS = {}
  def self.c4mi_overrides
    C4MI_GETTER_OVERRIDES_BY_CLASS
  end
  
  class DeviceProfile
    # TODO
    # - functionality to get a list of all the terms used in a profile
    # - xml that represents a base profile (cardinality, etc.)
    # - exchange format -- json
    # - exchange format -- xml ???????????
    IGNORED_CLASSES = [] unless defined?(IGNORED_CLASSES)
    unless defined?(PROFILED_CLASSES)
      #*PHD*
      # PROFILED_CLASSES = [Nomenclature, MyDevice, DIM, IEEE20101, PHD].collect do |mod|
      PROFILED_CLASSES = [Nomenclature, MyDevice, DIM, IEEE20101].collect do |mod|
        mod.classes(:ignore_class_namespaces => true)
      end.flatten - IGNORED_CLASSES
    end
    
    def application_version
      DimGenerated::VERSION
    end
    derived_attribute(:application_version, ::String)
    
    derived_attribute(:set_tree_view_root, ::NilClass)
    def set_tree_view_root
      puts "CALLED SET TREEVIEWROOT"
      Gui::Controller.session[:tree_view_roots] ||= []
      Gui::Controller.session[:tree_view_roots].unshift(self)
      Gui::Controller.session[:tree_view_roots].uniq!
    end
    
    def on_gui_root_load
      set_tree_view_root
    end
    
    # add_attribute_getter(:uuid, :store_on_get => true) {|uuid| puts "UUID is #{uuid}"; val = uuid || SecureRandom.uuid; val}
    # add_attribute_getter(:creation_time, :store_on_get => true) {|time| val = time || Time.now; val}
    # FIXME I would rather use the lines above and have that value get saved but it isn't getting saved and I don't know why
    def after_change_tracker_create
      self.uuid = self.uuid || SecureRandom.uuid
      self.creation_time = self.creation_time || Time.now
      super
    end
    
    def do_json_graph(elide_nulls = false)
      GraphState.getter_overrides_by_class = MyDevice.default_overrides
      GraphState.formatting_rules << Proc.new{|str| str = str.strip.gsub("_", "-").gsub(/-dim$/, "")}
      options = {:max_nesting => 1000}
      content = JSON_Graph.pretty_generate(self, options)
      if elide_nulls
        content.gsub!(/(,\n)? *"[a-z_]+": null/, "")
        content.gsub!(/(,\n)? *"[a-z_]+": \[[ \n]*\]/, "")
        content.gsub!(/^ *\n^/, "")
      end
      filename = name.strip
      filename.gsub!(" ", "_") if filename
      filename = "unnamed_pcd_profile" if filename.nil? || filename.empty?
      dirname = File.expand_path("~/Prometheus/data/dim_generated/json")
      time = Time.now.strftime("%F_%H%M%S")
      unless File.directory?(dirname)
        FileUtils.mkdir_p(dirname)
      end
      File.open(File.join(dirname, "#{filename}_#{time}.json"), 'w') {|f| f << content }
      content
    end
    
    def do_xml_graph(opts = {}) # not sure how nulls will be handled in XML
      # FIXME this shouldn't have to be cloned.  JSON_Graph should not be stomping on the hash
      # overrides = opts[:map].clone || MyDevice.default_overrides.clone
      overrides = opts[:map] || MyDevice.default_overrides # if you wanted to merge these then do it beforehand and pass in as opts[:map]
      include_nulls = opts[:include_nulls] # deprecated?  not used AFAICT
      GraphState.clear
      GraphState.clear_config
      GraphState.output_type = :xml
      GraphState.classname_key = 'xsi:type'
      GraphState.getter_overrides_by_class = overrides
      GraphState.no_references!
      GraphState.getter_tests << attribute_usage_proc    
      
      xml_options = {:allow_duplicates => true}#, :tag_is_class => true}
      content = XML_Graph.pretty_generate(self, nil, 'http://prometheuscomputing.com/DIM/MyDevice', xml_options)
      # content.gsub!("profile_root", self.profile_root.class.to_s.demodulize.snakecase)
      content.gsub!(/_dim|_reserved/, "")
      # filename = name.strip
      # filename.gsub!(" ", "_") if filename
      # filename = "unnamed_pcd_profile" if filename.nil? || filename.empty?
      # dirname = File.expand_path("~/Prometheus/data/dim_generated/xml")
      # time = Time.now.strftime("%F_%H%M%S")
      # unless File.directory?(dirname)
      #   FileUtils.mkdir_p(dirname)
      # end
      # File.open(File.join(dirname, "#{filename}_#{time}.xml"), 'w') {|f| f << content }
      # pp overrides
      content
    end

    def attribute_usage_proc
      Proc.new do |instance, getter|
        if instance.respond_to? :used_getters
          ag = instance.asn1_getters
          ug = instance.used_getters
          if ag.include?(getter)
            if ug.include?(getter)
              true
            else
              false
            end
          else
            true
          end
        else
          true
        end
      end
    end

    # def render_as_html
    #   locals = {}
    #   locals[:effective_date] = Date.today
    #   #puts "========="
    #   template_path = File.join(File.dirname(__FILE__), 'pages/view/render/pcd_profile_content.haml')
    #   puts "Found HAML template at #{template_path.inspect}"
    #   Haml::Engine.new(File.read(template_path)).render(self, locals)
    # end

    def context_insensitive_render_as_html
      setup_context
      self.render_as_html
    end

    
    def setup_context
      ChangeTracker.session_hash = {}
    end


    def tops
      profile_root.tops
    end

    def report_header
      str = ""
      # str << "Device Specialization\n" if is_device_specialization # TODO
      str << "PCD Profile: #{name}\n"
      str << "Purpose: #{purpose.or_none_provided}\n"
      dc = description.content
      str << "Description: #{dc}\n" if dc and !dc.empty? # might need to deal w/ indentation here?
      str << "Owning Company: #{company_owner}" if company_owner && !company_owner.empty?
      str
    end
    
    def report_header_textile
      str = ""
      # str << "Device Specialization\n" if is_device_specialization # TODO
      str << "h1. PCD Profile: #{name}\n\n"
      str << "Purpose: #{purpose.or_none_provided}\n"
      iu = intended_use
      str << "Intended Use: #{iu.value}\n" if iu
      str << "Owning Company: #{company_owner}\n\n" if company_owner && !company_owner.empty?
      dc = description.content
      str << "- Description: :=  #{dc}\n" if dc and !dc.empty?
      str
    end
    
    def summary_for_children
      html_table = "<table style = 'background-color:beige';>"
      html_table << '<col width="100px">'
      html_table << "<tr>"
      html_table << "<td>" << 'Profile Name:' << "</td>"
      html_table << "<td>" << name.or_none_provided << "</td>"
      html_table << "<tr>"
      html_table << "<td>" << 'Intended Use:' << "</td>"
      html_table << "<td>" << (intended_use ? intended_use.value : nil).or_none_provided << "</td>"
      html_table << "<tr>"
      html_table << "<td>" << 'Purpose:' << "</td>"
      html_table << "<td>" << purpose.or_none_provided << "</td>"
      if company_owner && !company_owner.empty?
        html_table << "<tr>"
        html_table << "<td>" << 'Owner:' << "</td>"
        html_table << "<td>" << company_owner << "</td>"
      end      
      # html_table << "</tbody> </table>"
      html_table << "</table>"
      html_table
    end
    
    def total_report
      str = report_header + "\n"
      str << containment_report + "\n\n"
      str << channel_report + "\n\n"
      str << tops_report
      str.gsub(/^( *\n *)+$/m, "\n")
    end
    
    def total_report_html
      return channel_report_html
      str = report_header_textile + "\n"
      # str << containment_report + "\n\n"
      # str << channel_report + "\n\n"
      # str << tops_report
      # str = str.gsub(/^( *\n *)+$/m, "\n")
      html = RedCloth.new(str).to_html
      html
    end
    
    def textile_css
      str = <<-END
      
        <style> 
          <!--
      	    .tableborders td {border:2px solid black;}
          -->
        </style>
      
      END
      str
    end
    
    def channel_report_html
      html = ""
      html << "<html>\n<head>\n"
      html << textile_css + "\n"
      html << '</head>' + "\n" +'<body>' +"\n"
      textile = ""
      textile << report_header_textile + "\n\n"
      textile << containment_report_textile + "\n\n"
      textile << channel_report_textile + "\n\n"
      # textile << tops_report_textile
      html << RedCloth.new(textile).to_html
      html << "\n" + '</body>' + "\n" + '</html>' + "\n"
      html
    end
    
    
    def tops_report
      profile_root.tops_report
    end
    
    def tops_report_textile
      profile_root.tops_report_textile
    end
    
    def containment_report
      str = ""
      contained = profile_root.containment_report
      contained.each{|line| str << "  #{line}\n"}
      str
    end
    
    def containment_report_textile(stop_at_channels = true)
      textile = ""
      textile << "h2. Containment Tree\n\n"
      textile << "table(tableborders){border:2px solid black; border-collapse:collapse}.\n"
      textile << profile_root.containment_report_textile_table_rows(stop_at_channels) if profile_root
      puts textile      
      textile
    end
    
    # build string data for a report that is styled after the one that ICS Generator provided
    def channel_report
      str = ""
      contained = profile_root.channel_report
      contained.each{|line| str << "  #{line}\n"}
      str
    end
    
    # build string data for a report that is styled after the one that ICS Generator provided
    def channel_report_textile
      textile = ""
      textile << "h2. Object Details\n\n"
      textile << profile_root.channel_report_textile if profile_root
      textile
    end

    def normative?
      intended_use && intended_use.value =~ /normative/i
    end

    def display_name
      ret = name
      ret = 'Unnamed' if ret.nil? || ret.empty?
      "Device Profile: #{ret}"
    end
    
    derived_attribute(:xml_date, ::String)
    def xml_date
      "#{Time.now}"
    end
    
    derived_attribute(:test_method, ::String)
    def test_method
      "This is a test"
    end
    
    def show_test
      false
    end

    derived_attribute(:download_summary_hierarchy, ::Hash)
    def download_summary_hierarchy(opts = {})
      {:content => make_rosetta_containment_hierarchy(opts), :filename => sanitize_filename(display_name) + '_rosetta_containment_hierarchy.xml', :type => 'xml'}
    end
    
    derived_attribute(:download_summary_hierarchy_with_codes, ::Hash)
    def download_summary_hierarchy_with_codes
      download_summary_hierarchy(:term_codes => true)
    end

    derived_attribute(:download_verbose_hierarchy, ::Hash)
    def download_verbose_hierarchy
      {:content => make_containment_hierarchy, :filename => sanitize_filename(display_name) + '.xml', :type => 'xml'}
    end

    derived_attribute(:download_json, ::Hash)
    def download_json
      {:content => do_json_graph("elide_nulls"), :filename => sanitize_filename(display_name) + '.json', :type => 'json'}
    end
    
    derived_attribute(:download_full_xml, ::Hash)
    def download_full_xml
      {:content => do_xml_graph, :filename => sanitize_filename(display_name) + '.xml', :type => 'xml'}
    end
    
    derived_attribute(:download_c4mi_xml, ::Hash)
    def download_c4mi_xml
      {:content => do_xml_graph(:map => MyDevice.c4mi_overrides), :filename => sanitize_filename(display_name) + '_for_c4mi.xml', :type => 'xml'}
    end
    
    derived_attribute(:download_report, ::Hash)
    def download_report
      {:content => total_report, :filename => sanitize_filename(display_name) + '.pcd', :type => 'txt/plain'}
    end

    derived_attribute(:download_html_report, ::Hash)
    def download_html_report
      {:content => total_report_html, :filename => sanitize_filename(display_name) + '.html', :type => 'txt/html'}
    end

    def make_containment_hierarchy
      xml = ""
      xml_builder = Builder::XmlMarkup.new(:target=>xml, :indent=>2)
      xml_builder.instruct! :xml, :version =>'1.0', :encoding => 'UTF-8', :standalone => 'yes'

      profile_attributes = {name: description_content}
      profile_attributes[:normative] = true if normative?
      profile_attributes[:company_owner] = company_owner if company_owner
      profile_attributes[:mode] = profile_mode if profile_mode
      profile_attributes[:type] = profile_type if profile_type
      profile_attributes[:date] = Time.new.strftime("%Y-%m-%d %H:%M:%S")
      profile_attributes[:dim_version] = "FIXME"
      profile_attributes[:nomenclature_version] = "FIXME"

      xml_builder.rosetta_containment_hierarchy(profile_attributes)
      containment_recurse xml_builder, [profile_root]
      xml
    end

 #    def make_interchange_xml
 #      xml = ""
 #      xml_builder = Builder::XmlMarkup.new(:target=>xml, :indent=>2)
 #      xml_builder.instruct! :xml, :version =>'1.0', :encoding => 'UTF-8', :standalone => 'yes'
 #      profile_recurse xml_builder, [{profile_root => nil}]
 #      xml
 #    end

    # Not sure if this will ever be used.  Currently part of an unfinished exercise
  #   def make_nomenclature_validity_hierarchy
  #     xml = ""
  #     xml_builder = Builder::XmlMarkup.new(:target=>xml, :indent=>2)
  #     xml_builder.instruct! :xml, :version =>'1.0', :encoding => 'UTF-8', :standalone => 'yes'
  #     nvh_recurse xml_builder, [profile_root]
  #     xml
  #   end

    def containment_recurse xml_builder, nodes
      nodes.each do |node|

        if MyDevice::ALL_DIM_CLASSES.include?(node.class)
          # pp node
          aggs = node.aggregations
          puts "#{node.inspect} had a nil composition" if aggs.include? nil
          aggs.compact!
          # remove ASN.1 types
          aggs.select!{|c| MyDevice::ALL_DIM_CLASSES.include?(c.class)}
          attribute_hash = {ref_id: node.display_type, description: node.name_in_profile, :dim_ref_id => node.class_ref_id, :dim_name => node.simple_class_name}
          card = node.cardinality
          attribute_hash[:card] = card if card && !card.empty?

          asn1_assoc_getters = node.class.associations.collect do |k,v|
            getter = v[:getter]
            type_getter = (getter.to_s + "_type").to_sym
            type = node.send(type_getter).first # this is assuming there is a single type
            if type.to_s =~ /ASN1/
              # puts "#{getter.inspect} is ASN1"
              getter
            else
              nil
            end
          end
          asn1_assoc_getters.compact!
          # puts "asn1_assoc_getters: #{asn1_assoc_getters.inspect}"
          asn1_assoc_objs = asn1_assoc_getters.collect{|getter| node.send(getter)}.flatten

          sub_nodes = aggs + asn1_assoc_objs.compact

          if sub_nodes.any?
            xml_builder.__send__(node.containment_tag_name.to_sym, attribute_hash){containment_recurse xml_builder, sub_nodes}
          else
            xml_builder.__send__(node.containment_tag_name.to_sym, attribute_hash)
          end
        else # then it must be an ASN1 object
          attribute_hash = {}

          attribute_getters = node.class.attributes.keys # well, we hope so at least
          attribute_getters.each do |getter|
            val = node.send(getter)
            attribute_hash[getter] = val unless val.nil?
          end

          asn1_assoc_getters = node.class.associations.collect do |k,v|
            getter = v[:getter]
            type_getter = (getter.to_s + "_type").to_sym
            type = node.send(type_getter).first # this is assuming there is a single type
            if type.to_s =~ /ASN1/
              getter
            else
              nil
            end
          end
          asn1_assoc_getters.compact!
          asn1_assoc_objs = asn1_assoc_getters.collect{|getter| node.send(getter)}.flatten

          sub_nodes = asn1_assoc_objs.compact
        end
      end
    end

    private
    def sanitize_filename(filename)
      ret = filename.strip
      # NOTE: File.basename doesn't work right with Windows paths on Unix
      # get only the filename, not the whole path
      ret = ret.gsub(/^.*(\\|\/)/, '')
      # Strip out the non-ascii character
      ret = ret.gsub(/[^0-9A-Za-z.\-]/, '_')
      ret = ret.gsub(/_+/, '_')
    end
    
    # lifted from Apellation#role (though very condensed / streamlined)
    def tagify(str)
      str.gsub("-", "_").gsub(" ", "_").split(/_|\s+|(?=[A-Z][a-z])|(?<=[a-z])(?=[A-Z])|(?=\d+)|(?<=\d)/).collect{|part| part.downcase}.join("_")
    end
  end # class PCDProfile
end # module MyDevice
