require 'active_support/core_ext/hash'
require 'json'

module MyDevice
  class DeviceProfileBuilder;end
  class PCDProfileBuilder < DeviceProfileBuilder
    ALIASES = {
      DIM::Alert::Alert=>["alert"],
      DIM::Alert::AlertMonitor=>["alertmonitor", "alert_monitor"],
      DIM::Alert::AlertStatus=>["alertstatus", "alert_status"],
      DIM::Archival::Ancillary=>["ancillary"],
      DIM::Archival::MultiPatientArchive=>["multipatientarchive", "multi_patient_archive"],
      DIM::Archival::PatientArchive=>["patientarchive", "patient_archive"],
      DIM::Archival::Physician=>["physician"],
      DIM::Archival::SessionArchive=>["sessionarchive", "session_archive"],
      DIM::Archival::SessionNotes=>["sessionnotes", "session_notes"],
      DIM::Archival::SessionTest=>["sessiontest", "session_test"],
      DIM::Communication::BCC=>["bcc"],
      DIM::Communication::DCC=>["dcc"],
      DIM::Communication::DeviceInterface=>["deviceinterface", "device_interface"],
      DIM::Communication::DeviceInterfaceMibElement=>["deviceinterfacemibelement", "device_interface_mib_element"],
      DIM::Communication::GeneralCommunicationStatisticsMibElement=>["generalcommunicationstatisticsmibelement", "general_communication_statistics_mib_element"],
      DIM::Control::ActivateOperation=>["activateoperation", "activate_operation"],
      DIM::Control::LimitAlertOperation=>["limitalertoperation", "limit_alert_operation"],
      DIM::Control::SCO=>["sco"],
      DIM::Control::SelectItemOperation=>["selectitemoperation", "select_item_operation"],
      DIM::Control::SetRangeOperation=>["setrangeoperation", "set_range_operation"],
      DIM::Control::SetStringOperation=>["setstringoperation", "set_string_operation"],
      DIM::Control::SetValueOperation=>["setvalueoperation", "set_value_operation"],
      DIM::Control::ToggleFlagOperation=>["toggleflagoperation", "toggle_flag_operation"],
      DIM::ExtendedServices::AlertScanner=>["alertscanner", "alert_scanner"],
      DIM::ExtendedServices::ContextScanner=>["contextscanner", "context_scanner"],
      DIM::ExtendedServices::EpiCfgScanner=>["epicfgscanner", "epi_cfg_scanner"],
      DIM::ExtendedServices::FastPeriCfgScanner=>["fastpericfgscanner", "fast_peri_cfg_scanner"],
      DIM::ExtendedServices::OperatingScanner=>["operatingscanner", "operating_scanner"],
      DIM::ExtendedServices::PeriCfgScanner=>["pericfgscanner", "peri_cfg_scanner"],
      DIM::Medical::Channel=>["c", "chan", "channel"],
      DIM::Medical::ComplexMetric=>["complexmetric", "complex_metric"],
      DIM::Medical::DistributionSampleArray=>["distributionsamplearray", "distribution_sample_array", "dsa"],
      DIM::Medical::Enumeration=>["enum", "enumeration"],
      DIM::Medical::Metric=>["metric"],
      DIM::Medical::Numeric=>["num", "numeric"],
      DIM::Medical::PMSegment=>["pmsegment", "pm_seg", "pm_segment"],
      DIM::Medical::PMStore=>["pmstore", "pm_store"],
      DIM::Medical::RealTimeSampleArray=>["realtimesamplearray", "real_time_sample_array", "rtsa"],
      DIM::Medical::SampleArray=>["numeric-sa", "numeric_sa", "sa", "samplearray", "sample_array"],
      DIM::Medical::TimeSampleArray=>["timesamplearray", "time_sample_array", "tsa"],
      DIM::Medical::VMD=>["v", "vmd"],
      DIM::Patient::PatientDemographics=>["patientdemographics", "patient_demographics"],
      DIM::System::Battery=>["battery"],
      DIM::System::Clock=>["clock"],
      DIM::System::EventLog=>["eventlog", "event_log"],
      DIM::System::MultipleBedMDS=>["multiplebedmds", "multiple_bed_mds"],
      DIM::System::SingleBedMDS=>["mds", "singlebedmds", "single_bed_mds"]

    }
  end # PCDProfile
  class PHDProfileBuilder < DeviceProfileBuilder
    ALIASES = {
      PHD::MDS             => ["mds"],
      PHD::Numeric         => ["num", "numeric"],
      PHD::PMSegment       => ["pmsegment", "pm_seg", "pm_segment"],
      PHD::PMStore         => ["pmstore", "pm_store"],
      PHD::RealTimeSA      => ["real_time_sa", "realtimesamplearray", "real_time_sample_array", "rtsa"],
      PHD::Enumeration     => ["enum", "enumeration"],
      PHD::Scanner::EpiCfgScanner   => ["epicfgscanner", "epi_cfg_scanner"],
      PHD::Scanner::PeriCfgScanner  => ["pericfgscanner", "peri_cfg_scanner"],
      PHD::PulseOxNumeric  => ["pulse_ox_numeric"]
    }
  end # PHDProfile
end # MyDevice


