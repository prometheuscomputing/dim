module MyDevice
  class DeviceProfile

    derived_attribute(:download_rch_html, ::Hash)
    def download_rch_html
      name = sanitize_filename(display_name)
      puts name
      {:content => rch_html, :filename => sanitize_filename(display_name) + '_rch.html', :type => 'html'}
    end # download_rch_html

    def make_rosetta_containment_hierarchy(opts = {})
      xml = ""
      xml_builder = Builder::XmlMarkup.new(:target=>xml, :indent=>2)
      xml_builder.instruct! :xml, :version =>'1.0', :encoding => 'UTF-8', :standalone => 'yes'

      profile_attributes = {name: name}
      profile_attributes[:description] = description_content if description_content && !description_content.strip.empty?
      profile_attributes[:normative] = true if normative?
      profile_attributes[:owner] = company_owner if company_owner
      profile_attributes[:mode] = profile_mode.value if profile_mode
      profile_attributes[:type] = profile_type.value if profile_type
      profile_attributes[:download_time] = Time.new.strftime("%Y-%m-%d %H:%M:%S")
      profile_attributes[:creation_time] = creation_time.strftime("%Y-%m-%d %H:%M:%S")
      profile_attributes[:uuid] = uuid
      profile_attributes[:application_version] = DimGenerated::VERSION
      profile_attributes[:nomenclature_version] = Nomenclature::RTMMSTerm.all.collect{|t| t.update_date}.sort.last.to_s rescue nil
      xml_builder.comment! "Please, do not edit the attributes of the RCH element (uuid, application_version, nomenclature_version, etc.)."
      root = self.is_a?(MyDevice::PCDProfile) ? profile_root : mds
      xml_builder.RCH(profile_attributes) {rch_recurse(xml_builder, [root], opts)}
      xml
    end # make_rosetta_containment_hierarchy

    def rch_recurse xml_builder, nodes, opts = {}
      nodes.compact.each do |node|
        # pp node;puts
        aggs = node.aggregations
        puts "#{node.inspect} had a nil composition" if aggs.include? nil
        aggs.compact!
        # remove ASN.1 types
        # *PHD*
        # aggs.select!{|c| MyDevice::ALL_DIM_CLASSES.include?(c.class) || c.kind_of?(MyDevice::PHDTop)}
        aggs.select!{ |c| MyDevice::ALL_DIM_CLASSES.include?(c.class) }
        # attribute_hash = {:ref_id => node.display_name, :dim_ref_id => node.class_ref_id, :dim_name => node.simple_class_name}
        attribute_hash = {}
        if node.respond_to?(:type) || node.respond_to?(:system_type)
          attribute_hash[:refid] = node.display_type
          if opts[:term_codes]
            # TODO refactor this into a method e.g. #_type_code
            term = node._type_term
            attribute_hash[:code] = term.cf_term_code if term
          end
        end
        if node.respond_to?(:allowed_enumerations)
          if t = node.type_term
            if t.respond_to?(:collected_literals)
              aes = node.allowed_enumerations.uniq.sort_by{|e| e.reference_id}
              unless t.collected_literals.uniq.sort_by{|e| e.reference_id} == aes
                if aes.any?
                  attribute_hash[:e] = aes.collect{|ae| ae.reference_id}.join(" ")
                end
              end
            end
            if pt = node.proposed_type
              if pt.proposed_enums.any?
                pt.proposed_enums.each{|pe| attribute_hash[:e] << pe.reference_id << " "}
                attribute_hash[:e].strip!
              end
            end
          elsif pt = node.proposed_type
            if pt.proposed_enums.any?
              attribute_hash[:e] = pt.proposed_enums.collect{|pe| pe.reference_id}.join(" ")
            end
          end
        elsif node.respond_to?(:allowed_units)
          if t = node.type_term
            if t.respond_to?(:collected_units)
              aus = node.allowed_units.uniq.sort_by{|u| u.reference_id}
              unless t.collected_units.uniq.sort_by{|u| u.reference_id} == aus
                if aus.any?
                  attribute_hash[:u] = aus.collect{|au| au.reference_id}.join(" ")
                end
              end
            end
            if pt = node.proposed_type
              if pt.proposed_units.any?
                pt.proposed_units.each{|pu| attribute_hash[:u] << pu.reference_id << " "}
                attribute_hash[:u].strip!
              end
            end
          elsif pt = node.proposed_type
            if pt.proposed_units.any?
              attribute_hash[:u] = pt.proposed_units.collect{|pu| pu.reference_id}.join(" ")
            end
          end
        end
        # if nip = node.name_in_profile
        #   attribute_hash[:description] = nip
        # end
        card = node.cardinality
        attribute_hash[:card] = card if card && !card.empty?
        if aggs.any?
          xml_builder.__send__(tagify(node.simple_class_name).to_sym, attribute_hash){rch_recurse xml_builder, aggs, opts}
        else
          xml_builder.__send__(tagify(node.simple_class_name).to_sym, attribute_hash)
        end
      end
    end # rch_recurse
  end # class PCDProfile
end # module MyDevice