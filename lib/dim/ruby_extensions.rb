# module FakeMetaInfo
#   def properties; {};end
#   def attributes; {};end
#   def associations; {}; end
#   def interface?; false; end
#   def enumeration?; false; end
#   def abstract?; false; end
# end

class Object
  # extend FakeMetaInfo
end

class NilClass
  def is_asn1?; false; end
  def or_none_provided; String.new.or_none_provided; end
end

class String
  alias_method :old_plus, :+
  def +(arg)
    old_plus(arg.to_s)
  end
  
  def or_none_provided
    if self[0]
      self
    else
      "none provided"
    end
  end
  
  def textile_indent
    return "" if empty?
    return "" if self == "\n"
    puts "Input line---#{self}"
    if self[0..1] == "p("
      ret = "p(#{self[1..-1]}"
    else
      ret = "p(. #{self}"
    end
    puts "Output line--#{ret}"
    ret
  end
  
  def padding_amount
    20
  end
  
  def textile_pad_table_row
    return "" if empty?
    return "" if self == "\n"
    replace_newline = self =~ /\n$/
    if self =~ /^| *\^\./ #assumes that all cells would be top justified if leftmost one is
      ret = self.strip.gsub(/\| *?\^\.(?=[^|])/, "|^{padding-left:#{padding_amount}px; padding-right:#{padding_amount}}. ")
    else
      ret = self.strip.gsub(/\| *?(?=[^|])/, "|^{padding-left:#{padding_amount}px; padding-right:#{padding_amount}}. ")
    end
    ret = ret + "\n" if replace_newline
    ret
  end
  
  def textile_indent_table_cell
    return "" if empty?
    return "" if self == "\n"
    # puts "Input line---#{self}"
    padding_regexp = Regexp.new("\^?{padding-left: *([0-9]+)px; padding-right:#{padding_amount}}\. *")
    existing_indentation = self =~ padding_regexp
    if existing_indentation = /\|\^?{padding-left: *([0-9]+)px; padding-right:#{padding_amount}}\. */.match(self)
      ret = self.sub(/\^?{padding-left: *#{existing_indentation[1]}px; padding-right:#{padding_amount}}\. */, "^{padding-left: #{existing_indentation[1].to_i + padding_amount}px; padding-right:#{padding_amount}}. ")
    else
      ret = self.gsub(/\| *(?=[^|\n])/, "|^{padding-left:#{padding_amount}px; padding-right:#{padding_amount}}. ")
    end
    # puts "Output line--#{ret}"
    ret
  end
  
  def snakecase
    #gsub(/::/, '/').
    gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
    gsub(/([a-z\d])([A-Z])/,'\1_\2').
    tr('-', '_').
    gsub(/\s/, '_').
    gsub(/__+/, '_').
    downcase
  end
end

class Symbol
  def +(other)
    to_s + other.to_s
  end
end

class Array
  def to_sentence_list
    if size < 1
      ""
    elsif size == 1
      "#{first}"
    elsif size == 2
      "#{first} and #{last}"
    else
      tail = pop
      "#{join(', ')}, and #{tail}"
    end
  end
end
