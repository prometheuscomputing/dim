require 'json_graph/xml_schema_builder'

def generate_schemas(mapping)
  mapping ||= :default_overrides
  mapping = mapping.to_sym # because it was probably passed in as a string
  GraphState.classname_key = 'xsi:type'
  Schema_Builder.default_include_object_id = false
  Schema_Builder.element_map = send(mapping) # this is the same map used by GraphState.getter_overrides_by_class
  # Schema_Builder.default_top_comment = Schema_Builder::PROMETHEUS_COPYRIGHT
  # Specify json/ruby getter mappings (for the use case we want to generate a schema for)
  # GraphState.getter_overrides_by_class is specified in model_extensions.rb
  # Remove any existing Schema_Builder instances
  Schema_Builder.clear
  # Specify which Strings are actually Enumerations (must be done before instance creation)
  # Schema_Builder.type_override is specified below.
  # Specify roots (this creates one or more Schema_Builder instances as a side effect)
  Schema_Builder.root_classes = [MyDevice::PCDProfile]
  # Assign namespaces. This step is optional.
  Schema_Builder.find('MyDevice').namespace= {DIM::SCHEMA_PREFIX => DIM::SCHEMA_URI}
  # Resolves references and then generates a schema corresponding to each Schema_Builder instance. If a directory is provided, the generated schemas are written
  folder = "./temp/schemas/#{mapping.to_s}"
  Schema_Builder.make_schemas folder
  folder = File.expand_path folder
  files = Dir.glob(File.join(folder, "**/*")).reject{|fn| File.directory?(fn)}
  files.each do |file_name|
    text = File.read(file_name)
    new_contents = text.gsub(/_dim|_reserved/, "")
    File.open(file_name, "w") {|file| file.puts new_contents }
  end
end