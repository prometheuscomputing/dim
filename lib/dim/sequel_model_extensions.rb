module Sequel
  class Model
    def self.modeled_attributes(renew = false)
      @modeled_attributes = nil if renew
      @modeled_attributes ||= properties.select{|k,v| v[:attribute] || v[:additional_data][:modeled_as] == :attribute}.values
    end
    
    def self.tree_properties(renew = false)
      @tree_properties = nil if renew
      @tree_properties ||= modeled_attributes(renew) + aggregations
    end
    
    include JSON_Graph::Serializeable
    def self.add_json_getter(getter)
      added_json_getters << getter
    end
    # def self.added_json_getters
    #   @added_json_getters ||= []
    # end
    # def self.json_getters
    #   default_json_getters | added_json_getters
    # end
    # def self.default_json_getters
    #   @default_json_getters ||= all_property_names(:content) - GraphState.excludes_for(self)
    #   # @default_json_getters ||= (attributes.collect{|a,v| v[:name].to_s} | associations.collect{|a,v| v[:getter].to_s})
    # end
    # def json_getters
    #   self.class.json_getters
    # end
    # def self.json_getter_overrides
    #   @json_getter_overrides ||= {}
    # end
    def self.add_json_getter_override overridden_getter, overriding_getter
      json_getter_overrides[overridden_getter] = overriding_getter
    end
    # def json_getter_overrides
    #   self.class.json_getter_overrides
    # end
    def self.metainfo_classifier(opts = {})
      modules = self.name.split("::")
      classifier = modules.pop
      current_metainfo_module = nil
      modules.each do |mod|
        if current_metainfo_module.nil?
          current_metainfo_module = MetaInfo::Package.where(:name => mod).first
        else
          current_metainfo_module = current_metainfo_module.child_packages.find{|p| p.name == mod}
        end
        return nil unless current_metainfo_module
      end
      metainfo_classifier = current_metainfo_module.classifiers.find{|c| c.name == classifier}
      # HACK FIXME (you shouldn't have to do this!)
      if !metainfo_classifier && opts[:fuzzy]        
        metainfo_classifier = current_metainfo_module.classifiers.find{ |c| c.name.gsub(/[-_]/, '') == classifier.gsub(/[-_]/, '') }
      end
      metainfo_classifier
    end
    
    def self.join_class?
      !!(properties.find{|k,v| v[:associates]})
    end
  end
end