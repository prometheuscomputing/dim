module IEEEDSL

  class MigrationDSL # 6.7.4 to 6.8.0 models
    attr_accessor :stack
    def self.load(filename)
      new.instance_eval(File.read(filename), filename)
    end
    
    def initialize
      @stack = []
    end
    
    def process(klass, property_values, parent_getter, &block)
      ChangeTracker.start
      obj = klass.new
      property_values.each { |getter, value| obj.send(getter, value) unless value.nil? }
      obj.save
      ChangeTracker.commit
      ChangeTracker.start
      stack.last.send(parent_getter, obj) if parent_getter && stack.any?
      ChangeTracker.commit
      if block_given?
        stack << obj
        instance_eval(&block)
        stack.pop
      end
    end
    
    def standard(&block)
      process(Standard::IEEEStandard, {}, nil, &block)
    end
    
    # Right now, #title should only be called in a block that is passed to #standard -- i.e. the title is for the IEEEStandard and nothing else.
    def title(str)
      @stack.last.title = str
      # stack.find { |member| member.is_a?(Standard::IEEEStandard) }.send(:title=, str)
    end
    
    def normative_reference_section(&block)
      process(Standard::NormativeReferenceSection, {}, :content_add, &block)
    end

    def normative_reference(source, description, &block)
      ChangeTracker.start
      rt = Gui_Builder_Profile::RichText.create(:content => description)
      nr  = Standard::NormativeReference.create(:source => source, :description => rt)
      stack.last.send(:normative_references_add, nr)
      ChangeTracker.commit
      if block_given?
        stack << nr
        instance_eval(&block)
        stack.pop
      end
    end
    
    def definition_section(&block)
      process(Standard::DefinitionSection, {}, :content_add, &block)
    end

    def definition(term, definition)
      ChangeTracker.start
      d = Standard::Definition.create(:term => term, :definition => definition)
      stack.last.send(:definitions_add, d)
      ChangeTracker.commit
    end

    def abbreviations_section(&block)
      process(Standard::AbbreviationSection, {}, :content_add, &block)
    end

    def abbreviation(symbol, meaning)
      ChangeTracker.start
      a = Standard::Abbreviation.create(:symbol => symbol, :meaning => meaning)
      stack.last.send(:abbreviations_add, a)
      ChangeTracker.commit
    end

    def clause(str, property_values = {}, &block)
      property_values[:title=] = str if str && !property_values[:title=]
      process(Standard::Clause, property_values, :content_add, &block)
    end
    
    def all_classifiers
      return @all_classifiers if @all_classifiers
      @all_classifiers = {}
      MetaInfo::Classifier.all do |classifier|
        @all_classifiers[classifier.qualified_name] = classifier
      end
      # puts all_classifiers.keys.sort
      @all_classifiers
    end
    
    def model_element(constant_string)
      meta_info_model = all_classifiers.fetch(constant_string)
      unless constant_string
        puts "Could not find #{constant_string}"
        return
      end
      ChangeTracker.start
      @stack.last.model_element = meta_info_model
      ChangeTracker.commit
    end

    def text(str, property_values = {}, &block)
      property_values[:content=] = Gui_Builder_Profile::RichText.create(:content => str) if str && !property_values[:content=]
      process(Standard::Text, property_values, :content_add, &block)
    end
    
    def table(str, property_values = {}, &block)
      property_values[:title=] = str if str && !property_values[:title=]
      process(Standard::Table, property_values, :content_add, &block)
    end
    
    def code(str, property_values = {}, &block)
      property_values[:title=] = str if str && !property_values[:title=]
      process(Standard::Code, property_values, :content_add, &block)
    end
    
    def example(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(Standard::Example, {:content= => val}, :content_add, &block)
    end
    
    def figure(str = nil, &block)
      process(Standard::Figure, {:caption= => str}, :content_add, &block)
    end
    
    def note(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(Standard::Note, {:content= => val}, :content_add, &block)
    end

    def footnote(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str) if str
      process(Standard::Footnote, {:content= => val}, :footnotes_add, &block)
    end
    
    def render_as(value)
      ChangeTracker.start
      @stack.last.render_as = Standard::RenderingType.where(:value => value).first
      ChangeTracker.commit
    end
    def source_file(value); end # FIXME if you need to...but you don't need to right now
    def file_usage(value)
      ChangeTracker.start
      @stack.last.file_usage = Standard::FileUsage.where(:value => value).first
      ChangeTracker.commit
    end
    
    # # TODO -- make this right when the model is updated
    # def ordered_list(&block)
    #   if block_given?
    #     instance_eval(&block)
    #   end
    # end
    
    # # TODO -- make this right when the model is updated
    # def unordered_list(&block)
    #   if block_given?
    #     instance_eval(&block)
    #   end
    # end
    
    # # TODO -- make this right when the model is updated
    # def unordered_list_item(str, &block)
    #   str = 'ULI -- ' + str
    #   paragraph(str, &block)
    #   # puts 'unordered_list_item'; puts str; puts
    #   # val = Gui_Builder_Profile::RichText.create(:content => str)
    #   # process(val, Standard::Footnote, :content=, :footnotes_add, &block)
    # end
    
    # # TODO -- make this right when the model is updated
    # def ordered_list_item1(str, &block)
    #   str = 'LI -- ' + str
    #   paragraph(str, &block)
    #   # puts 'ordered_list_item1'; puts str; puts
    #   # val = Gui_Builder_Profile::RichText.create(:content => str)
    #   # process(val, Standard::Footnote, :content=, :footnotes_add, &block)
    # end
  end
end
