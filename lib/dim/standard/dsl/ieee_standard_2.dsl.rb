standard {
  title("DIM")
  scope()
  abbreviations_section {
    abbreviation("ACSE", "association control service element")
    abbreviation("ASN.1", "Abstract Syntax Notation One")
    abbreviation("BCC", "bedside communication controller")
    abbreviation("BER", "basic encoding rules")
    abbreviation("BMP", "basic multilingual plane")
    abbreviation("CMDIP", "Common Medical Device Information Protocol")
    abbreviation("CMDISE", "common medical device information service element")
    abbreviation("CMIP", "Common Management Information Protocol")
    abbreviation("CMISE", "common management information service element")
    abbreviation("DCC", "device communication controller")
    abbreviation("DICOM", "digital imaging and communications in medicine")
    abbreviation("DIM", "domain information model")
    abbreviation("ECG", "electrocardiogram")
    abbreviation("EEG", "electroencephalogram")
    abbreviation("EBWW", "eyeball and wristwatch")
    abbreviation("FSM", "finite state machine")
    abbreviation("GMDN", "Global Medical Device Nomenclature")
    abbreviation("GMT", "Greenwich mean time")
    abbreviation("IANA", "Internet Assigned Numbers Authority")
    abbreviation("ICS", "implementation conformance statement")
    abbreviation("ICU", "intensive care unit")
    abbreviation("ID", "identifier or identification")
    abbreviation("LAN", "local area network")
    abbreviation("LSB", "least significant bit")
    abbreviation("MDIB", "medical data information base")
    abbreviation("MDS", "medical device system")
    abbreviation("MEDICOM", "medical imaging communication")
    abbreviation("MIB or Mib", "management information base")
    abbreviation("MOC", "managed object class")
    abbreviation("OID", "object identifier")
    abbreviation("OR", "operating room")
    abbreviation("OSI", "open systems interconnection")
    abbreviation("PC", "personal computer")
    abbreviation("PDU", "protocol data unit")
    abbreviation("PM", "persistent metric")
    abbreviation("SCADA", "supervisory control and data acquisition")
    abbreviation("SCP ECG", "Standard Communications Protocol [for computer-assisted] Electrocardiography")
    abbreviation("SNMP", "Simple Network Management Protocol")
    abbreviation("SNTP", "Simple Network Time Protocol")
    abbreviation("UML", "unified modeling language")
    abbreviation("UTC", "coordinated universal time")
    abbreviation("VMD", "virtual medical device")
    abbreviation("VMO", "virtual medical object")
    abbreviation("VMS", "virtual medical system")
  }
  definition_section {
    definition("agent", "Device that provides data in a manager-agent communicating system.")
    definition("alarm", "Signal that indicates abnormal events occurring to the patient or the device system.")
    definition("alert", "Synonym for the combination of patient-related physiological alarms, technical alarms, and equip- ment-user advisory signals.")
    definition("alert monitor", "Object representing the output of a device or system alarm processor and as such the overall device or system alarm condition.")
    definition("alert status", "Object representing the output of an alarm process that considers all alarm conditions in a scope that spans one or more objects.")
    definition("archival", "Relating to the storage of data over a prolonged period.")
    definition("association control service element (ACSE)", "Method used to establish logical connections between medical device systems (MDSs).")
    definition("channel", "Umbrella object in the object model that groups together physiological measurement data and data derived from these data.")
    definition("class", "Description of one or more objects with a uniform set of attributes and services including a description of how to create new objects in the class.")
    definition("communication controller", "Part of a medical device system (MDS) responsible for communications.")
    definition("communication party", "Actor of the problem domain that participates in the communication in that domain.")
    definition("communication role", "Role of a party in a communication situation defining the party¡¯s behavior in the communication. Associated with a communication role is a set of services that the party provides to other parties.")
    definition("data agent", "As a medical device, a patient data acquisition system that provides the acquired data for other devices.")
    definition("data format", "Arrangement of data in a file or stream.")
    definition("data logger", "A medical device that is functioning in its capacity as a data storage and archival system.")
    definition("data structure", "Manner in which application entities construct the data set information resulting from the use of an information object.")
    definition("dictionary", "Description of the contents of the medical data information base (MDIB) containing vital signs information, device information, demographics, and other elements of the MDIB.")
    definition("discrete parameter", "Vital signs measurement that can be expressed as a single numeric or textual value.")
    definition("domain information model (DIM)", "The model describing common concepts and relationships for a problem domain.")
    definition("event", " A change in device status that is communicated by a notification reporting service.")
    definition("event report", "Service [provided by the common medical device information service element (CMDISE)] to report an event relating to a managed object instance.")
    definition("framework", "A structure of processes and specifications designed to support the accomplishment of a specific task.")
    definition("graphic parameter", "Vital signs measurement that requires a number of regularly sampled data points in order to be expressed properly.")
    definition("host system", "Term used as an abstraction of a medical system to which measurement devices are attached.")
    definition("information object", "An abstract data model applicable to the communication of vital signs information and related patient data. The attributes of an information object definition describe its properties. Each information object definition does not represent a specific instance of real-world data, but rather a class of data that share the same properties.")
    definition("information service element", "Instances in the medical data information base (MDIB).")
    definition("instance", "The realization of an abstract concept or specification, e.g., object instance, application instance, information service element instance, virtual medical device (VMD) instance, class instance, operating instance.")
    definition("intensive care unit (ICU)", "The unit within a medical facility in which patients are managed using multiple modes of monitoring and therapy.")
    definition("interchange format", "The representation of the data elements and the structure of the message containing those data elements while in transfer between systems. The interchange format consists of a data set of construction elements and a syntax. The representation is technology specific.")
    definition("interoperability", "Idealized scheme where medical devices of differing types, models, or manufacturers are capable of working with each other, whether connected to each other directly or through a communication system.")
    definition("latency", "In a communications scenario, the time delay between sending a signal from one device and receiving it by another device.")
    definition("lower layers", "Layer 1 to Layer 4 of the International Organization for Standardization (ISO)/open systems interconnection (OSI) reference model. These layers cover mechanical, electrical, and general communication protocol specifications.")
    definition("manager", "Device that receives data in a manager-agent communicating system.")
    definition("manager-agent model", "Communication model where one device (i.e., agent) provides data and another device (i.e., manager) receives data.")
    definition("medical data information base (MDIB)", "The concept of an object-oriented database storing (at least) vital signs information.")
    definition("medical device", "A device, apparatus, or system used for patient monitoring, treatment, or therapy, which does not normally enter metabolic pathways. For the purposes of this standard, the scope of medical devices is further limited to patient-connected medical devices that provide support for electronic communications.")
    definition("medical device system (MDS)", "Abstraction for system comprising one or more medical functions. In the context of this standard, the term is specifically used as an object-oriented abstraction of a device that provides medical information in the form of objects that are defined in this standard.")
    definition("monitor", "A medical device designed to acquire, display, record, and/or analyze patient data and to alert caregivers of events needing their attention.")
    definition("object", "A concept, an abstraction, or a thing with crisp boundaries and a meaning for the problem at hand.")
    definition("object attributes", "Data that, together with methods, define an object.")
    definition("object class", "A descriptor used in association with a group of objects with similar properties (i.e., attributes), common behavior (i.e., operations), common relationships to other objects, and common semantics.")
    definition("object diagram", "Diagram showing connections between objects in a system.")
    definition("object method", "A procedure or process acting upon the attributes and states of an object class.")
    definition("object-oriented analysis", "Method of analysis where the problem domain is modelled in the form of objects and their interactions.")
    definition("open system", "A set of protocols allowing computers of different origins to be linked together.")
    definition("operation", "A function or transformation that may be applied to or by objects in a class (sometimes also called service).")
    definition("problem domain", " The field of health care under consideration in a modeling process.")
    definition("protocol", "A standard set of rules describing the transfer of data between devices. It specifies the format of the data and specifies the signals to start, control, and end the transfer.")
    definition("scanner", "An observer and ¡°summarizer¡± of object attribute values.")
    definition("scenario", "A formal description of a class of business activities including the semantics of business agreements, conventions, and information content.")
    definition("service", "A specific behavior that a communication party in a specific role is responsible for exhibiting.")
    definition("syntax (i.e., of an interchange format)", "The rules for combining the construction elements of the interchange format.")
    definition("system", "The demarcated part of the perceivable universe, existing in time and space, that may be regarded as a set of elements and relationships between these elements.")
    definition("timestamp", "An attribute or field in data that denotes the time of data generation.")
    definition("top object", "The ultimate base class for all other objects belonging to one model.")
    definition("upper layers", "Layer 5 to Layer 7 of the International Organization for Standardization (ISO)/open systems interconnection (OSI) reference model. These layers cover application, presentation, and session specifications and functionalities.")
    definition("virtual medical device (VMD)", "An abstract representation of a medical-related subsystem of a medical device system (MDS).")
    definition("virtual medical object (VMO)", "An abstract representation of an object in the Medical Package of the domain information model (DIM).")
    definition("vital sign", "Clinical information, relating to one or more patients, that is measured by or derived from apparatus connected to the patient or otherwise gathered from the patient.")
    definition("waveform", "Graphic data, typically vital signs data values varying with respect to time, usually presented to the clinician in a graphical form.")
  }
  general_requirements()
  introduction()
  normative_reference_section {
    normative_reference("CEN EN 1064", "<p>Medical informatics &mdash; Standard communication protocol &mdash; computer-assisted electrocardiography.</p>") {
      footnote("CEN publications are available from the European Committee for Standardization (CEN), 36, rue de Stassart, B-1050 Brussels, Belgium (http://www.cenorm.be).")
    }
    normative_reference("CEN ENV 12052", "Medical informatics —  Medical imaging communication (MEDICOM).")
    normative_reference("IEEE Std 1073\\texttrademark", "IEEE Standard for Medical Device Communications— Overview and Framework.") {
      footnote("IEEE publications are available from the Institute of Electrical and Electronics Engineers, 445 Hoes Lane, Piscataway, NJ 08854, USA (http://www.standards.ieee.org/).")
    }
    normative_reference("IETF RFC 1155", "Structure and Identification of Management Information for TCP/IP-Based Internets.") {
      footnote("Internet requests for comment (RFCs) are available from the Internet Engineering Task Force at http://www.ietf.org/.")
    }
    normative_reference("ISO 639-1", "Code for the representation of names of languages —  Part 1: Alpha-2 code.") {
      footnote("ISO publications are available from the ISO Central Secretariat, Case Postale 56, 1 rue de Varemb'{e}, CH-1211, Gen\\`{e}ve 20, Switzerland/Suisse (http://www.iso.ch/). ISO publications are also available in the United States from the Sales Department, American National Standards Institute, 25 West 43rd Street, 4th Floor, New York, NY 10036, USA (http://www.ansi.org/).")
    }
    normative_reference("ISO 639-2", "Codes for the representation of names of languages —  Part 2: Alpha-3 code.")
    normative_reference("ISO 3166-1", "Codes for the representation of names of countries and their subdivisions —  Part 1: Country codes.")
    normative_reference("ISO 3166-2", "Codes for the representation of names of countries and their subdivisions —  Part 2: Country subdivision code.")
    normative_reference("ISO 3166-3", "Codes for the representation of names of countries and their subdivisions —  Part 3: Code for formerly used names of countries.")
    normative_reference("ISO 8601", "Data elements and interchange formats —  Information interchange —  Representation of dates and times.")
    normative_reference("ISO 15225", "Nomenclature —  Specification for a nomenclature system for medical devices for the purpose of regulatory data exchange.")
    normative_reference("ISO/IEC 646", "Information technology —  ISO 7-bit coded character set for information interchange.") {
      footnote("ISO/IEC documents can be obtained from the ISO office, 1 rue de Varemb'{e}, Case Postale 56, CH-1211, Gen\\`{e}ve 20, Switzerland/ Suisse (http://www.iso.ch/) and from the IEC office, 3 rue de Varemb'{e}, Case Postale 131, CH-1211, Gen\\`{e}ve 20, Switzerland/Suisse (http://www.iec.ch/). ISO/IEC publications are also available in the United States from the Sales Department, American National Standards Institute, 25 West 43rd Street, 4th Floor, New York, NY 10036, USA (http://www.ansi.org/).")
    }
    normative_reference("ISO/IEC 2022", "Information technology —  Character code structure and extension techniques.")
    normative_reference("ISO/IEC 5218", "Information technology —  Codes for the representation of human sexes.")
    normative_reference("ISO/IEC 7498-1", "Information technology —  Open systems interconnection —  Basic reference model —  Part 1: The basic model.")
    normative_reference("ISO/IEC 7498-2", "Information processing systems —  Open systems interconnection —  Basic reference model —  Part 2: Security architecture.")
    normative_reference("ISO/IEC 7498-3", "Information processing systems —  Open systems interconnection —  Basic reference model —  Part 3: Naming and addressing.")
    normative_reference("ISO/IEC 7498-4", "Information processing systems —  Open systems interconnection —  Basic reference model —  Part 4: Management framework.")
    normative_reference("ISO/IEC 8649", "Information processing systems —  Open systems interconnection —  Service definition for the Association Control Service Element.")
    normative_reference("ISO/IEC 8650-1", "Information technology —  Open systems interconnection —  Connection-Oriented Protocol for the Association Control Service Element —  Part 1: Protocol.")
    normative_reference("ISO/IEC 8650-2", "Information technology —  Open systems interconnection —  Protocol Specification for Association Control Service Element —  Part 2: Protocol Implementation Conformance Statements (PICS) proforma.")
    normative_reference("ISO/IEC 8824-1", "Information technology —  Abstract Syntax Notation One (ASN.1) —  Part 1: Specification of basic notation.")
    normative_reference("ISO/IEC 8824-2", "Information technology —  Abstract Syntax Notation One (ASN.1) —  Part 2: Information object specification.")
    normative_reference("ISO/IEC 8859-n", "Information processing —  8-bit single-byte coded graphic character sets —  Part 1 to Part 15: Various alphabets.")
    normative_reference("ISO/IEC 9545", "Information technology —  Open systems interconnection —  Application layer structure.")
    normative_reference("ISO/IEC 9595", "Information technology —  Open systems interconnection —  Common management information service definition.")
    normative_reference("ISO/IEC 9596-1", "Information technology —  Open systems interconnection —  Common Management Information Protocol —  Part 1: Specification.")
    normative_reference("ISO/IEC 10040", "Information technology —  Open systems interconnection —  Systems management overview.")
    normative_reference("ISO/IEC 10164-1", "Information technology —  Open systems interconnection —  Systems management —  Part 1: Object management function.")
    normative_reference("ISO/IEC 10164-2", "Information technology —  Open systems interconnection —  Systems management —  Part 2: State management function.")
    normative_reference("ISO/IEC 10164-3", "Information technology —  Open systems interconnection —  System management —  Part 3: Attributes for representing relationships.")
    normative_reference("ISO/IEC 10164-4", "Information technology —  Open systems interconnection —  Systems management —  Part 4: Alarm reporting function.")
    normative_reference("ISO/IEC 10164-5", "Information technology —  Open systems interconnection —  Systems management —  Part 5: Event management function.")
    normative_reference("ISO/IEC 10164-6", "Information technology —  Open systems interconnection —  Systems management —  Part 6: Log control function.")
    normative_reference("ISO/IEC 10164-7", "Information technology —  Open systems interconnection —  Systems management —  Part 7: Security alarm reporting function.")
    normative_reference("ISO/IEC 10164-8", "Information technology —  Open systems interconnection —  Systems management —  Part 8: Security audit trail function.")
    normative_reference("ISO/IEC 10164-9", "Information technology —  Open systems interconnection —  Systems management —  Part 9: Objects and attributes for access control.")
    normative_reference("ISO/IEC 10164-10", "Information technology —  Open systems interconnection —  Systems management —  Part 10: Usage metering function for accounting purposes.")
    normative_reference("ISO/IEC 10164-11", "Information technology —  Open systems interconnection —  Systems management —  Part 11: Metric objects and attributes.")
    normative_reference("ISO/IEC 10164-12", "Information technology —  Open systems interconnection —  Systems management —  Part 12: Test management function.")
    normative_reference("ISO/IEC 10164-13", "Information technology —  Open systems interconnection —  Systems management —  Part 13: Summarization function.")
    normative_reference("ISO/IEC 10164-14", "Information technology —  Open systems interconnection —  Systems management —  Part 14: Confidence and diagnostic test categories.")
    normative_reference("ISO/IEC 10165-1", "Information technology —  Open systems interconnection —  Structure of management information —  Part 1: Management information model.")
    normative_reference("ISO/IEC 10165-2", "Information technology —  Open systems interconnection —  Structure of management information —  Part 2: Definition of management information.")
    normative_reference("ISO/IEC 10646-1", "Information technology —  Universal multiple-octet coded character set (UCS) —  Part 1: Architecture and basic multilingual plane.")
    normative_reference("ISO/IEEE 11073-10101", "Health informatics —  Point-of-care medical device communication —  Part 10101: Nomenclature.")
    normative_reference("ISO/IEEE 11073-20101", "Health informatics —  Point-of-care medical device communication —  Part 20101: Application profiles ¨C Base standard.")
    normative_reference("NEMA PS 3", "Digital imaging and communications in medicine (DICOM).") {
      footnote("NEMA publications are available from Global Engineering Documents, 15 Inverness Way East, Englewood, Colorado 80112, USA (http://global.ihs.com/).")
    }
  }
  clause("DIM Programmatic") {
    clause("General") {
      clause("Modeling concept") {
        text("The DIM is an object-oriented model that consists of objects, their attributes, and their methods, which are abstractions of real-world entities in the domain of (vital signs information communicating) medical devices.")
        text("The information model and the service model for communicating systems defined and used in this standard are conceptually based on the International Organization for Standardization (ISO)/open systems interconnection (OSI) system management model. Objects defined in the information model are considered managed (here, medical) objects. For the most part, they are directly available to management (i.e., access) services provided by the common medical device information service element (CMDISE) as defined in this standard.")
        text("For communicating systems, the set of object instances available on any medical device that complies with the definitions of this standard forms the medical data information base (MDIB). The MDIB is a structured collection of managed medical objects representing the vital signs information provided by a particular medical device. Attribute data types, hierarchies, and behavior of objects in the MDIB are defined in this standard.")
        text("The majority of objects defined here represent generalized vital signs data and support information. Specialization of these objects is achieved by defining appropriate attributes. Object hierarchies and relations between objects are used to express device configuration and device capabilities.")
        example("A generalized object is defined to represent vital signs in the form of a real-time waveform. A set of object attributes is used to specify a particular waveform as an invasive arterial blood pressure. The position in the hierarchy of all objects defines the subsystem that derives the waveform.")
        text("Figure 1 shows the relation between managed medical objects, MDIB, CMDISE, application processes, and communication systems.")
        figure("MDIB in communicating systems")
        text("In the case of communicating systems, managed medical objects are accessible only through services provided by CMDISE. The way that these objects are stored in the MDIB in any specific system and the way applications and the CMDISE access these objects are implementation issues and as such not normative.")
        text("In the case of a vital signs archived data format that complies with the definitions in this standard, object instances are stored, together with their dynamic attribute value changes, over a certain time period on archival media. Attribute data types and hierarchies of objects in the archival data format are again defined in this standard.")
        text("Figure 2shows the relationship between managed medical objects, the data archive, and archive access services.")
        text("In the case of the archived data format, the way managed medical objects are stored on a medium is the subject of standardization. The access services are a local implementation issue and as such are not intended to be governed by this standard.")
        figure("Managed medical objects in a vital signs archive")
      }
      clause("Scope of the DIM") {
        clause("General") {
          text("Vital signs information objects that are defined in this standard encompass digitized biosignals that are derived by medical measurement devices used, for example, in anaesthesia, surgery, infusion therapy, intensive care, and obstetrical care.")
          text("Biosignal data within the scope of this standard include direct and derived, quantitative and qualitative measurements, technical and medical alarms, and control settings. Patient information relevant for the interpretation of these signals is also defined in the DIM.")
        }
        clause("Communicating systems") {
          text("Communicating systems within the scope of this standard include physiological meters and analysers, especially systems providing real-time or continuous monitoring. Data processing capabilities are required for these systems.")
          text("Information management objects that provide capabilities and concepts for cost-effective communication (specifically data summarization objects) and objects necessary to enable real-time communication are also within the scope of the information model in this standard.")
          text("Interoperability issues, specifically lower communication layers, temporal synchronization between multiple devices, etc., are outside the scope of this standard.")
        }
        clause("Archived vital signs") {
          text("Context information objects that describe the data acquisition process and organize a vital signs archive are within the scope of this standard.")
        }
      }
      clause("Approach") {
        text("For the object-oriented modeling, the unified modeling language (UML) technique is used. The domain is first subdivided into different packages, and this subdivision permits the organization of the model into smaller packages. Each package is then defined in the form of object diagrams. Objects are briefly introduced, and their relations and hierarchies are defined in the object diagram.")
        text("For the object definitions, a textual approach is followed. Attributes are defined in attribute definition tables. Attribute data types are defined using Abstract Syntax Notation One (ASN.1). Object behavior and notifications generated by objects are also defined in definition tables. These definitions directly relate to the service model specified in Clause 8.")
      }
      clause("Extension of the model") {
        text("It is expected that over time extensions of the model may be needed to account for new developments in the area of medical devices. Also, in special implementations, there may be a requirement to model data that are specific for a particular device or a particular application (and that are, therefore, not covered by the general model).")
        text("In some cases, it may be possible to use the concept of external object relations. Most objects defined in this standard provide an attribute group (e.g., the Relationship Attribute Group) that can be used to supply information about related objects that are not defined in the DIM. Supplying such information can be done by specifying a relation to an external object and assigning attributes to this relation (see 7.1.2.20).")
        text("In other cases, it may be necessary to define completely new objects or to add new attributes, new methods, or new events to already defined objects. These extensions are considered private or manufacturer-specific extensions. Dealing with these extensions is primarily a matter of an interoperability standard that is based on this standard on vital signs representation.")
        text("In general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. The nomenclature code space (i.e., code values) leaves room for private extensions. As a general rule, an interoperability standard that is based on this DIM should be able to deal with private or manufacturer-specific extensions by ignoring objects, attributes, etc., with unknown identifiers (i.e., nomenclature codes).")
      }
    }
    clause("Package diagram–overview") {
      text("The package diagram organizes the problem domain into separate groups. It shows the major objects inside each package and defines the relationships between these packets.")
      text("The package diagram depicted in Figure 3contains only a small subset of all objects defined in the DIM. Common base objects except the Top object are not shown in this diagram. Also, not all relations are shown between the different packages. Refer to the detailed package diagrams for more information.")
      text("The numbers in the packages refer to the corresponding subclauses in this clause about models and in Clause 7 about the object definitions.")
      text("The Top object is an abstract base class and at the same time the ultimate base class for all objects defined in the model. For editorial convenience, the modeling diagrams in this standard do not show this inheritance hierarchy.")
      text("The more detailed models for these packages are contained in 6.3through 6.10.")
    }
    clause("Model for the Medical Package") {
      text("The Medical Package deals with the derivation and representation of biosignals and contextual information that is important for the interpretation of measurements.")
      text("Figure 4 shows the object model of the Medical Package.")
      figure("Packages of the DIM")
      figure("Medical Package model")
      note("Instances of the Channel object and the PM-Store object shall be contained in exactly one superior object instance.  Refer to the Alert Package for information about alarm-related objects.") {
          footnote("Notes in text, tables, and figures are given for information only, and do not contain requirements needed to implement the standard.")
      }
      text("The Medical Package model contains the objects described in 6.3.1 through 6.3.13.")
      clause("VMO (i.e., virtual medical object)") {
        text("The VMO is the base class for all medical-related objects in the model. It provides consistent naming and identification across the Medical Package model.")
        text("As a base class, the VMO cannot be instantiated.")
      }
      clause("VMD (i.e., virtual medical device) object") {
        text("The VMD object is an abstraction for a medical-related subsystem (e.g., hardware or even pure software) of a medical device. Characteristics of this subsystem (e.g., modes, versions) are captured in this object. At the same time, the VMD object is a container for objects representing measurement and status information.")
        example("A modular patient monitor provides measurement modalities in the form of plug-in modules. Each module is represented by a VMD object.")
      }
      clause("Channel object") {
        text("The Channel object is used for grouping Metric objects and, thus, allows hierarchical information organization. The Channel object is not mandatory for representation of Metric objects in a VMD.")
        example("A blood pressure VMD may define a Channel object to group together all metrics that deal with the blood pressure (e.g., pressure value, pressure waveform). A second Channel object can be used to group together metrics that deal with heart rate.")
      }
      clause("Metric object") {
        text("The Metric object is the base class for all objects representing direct and derived, quantitative and qualitative biosignal measurement, status, and context data.")
        text("Specializations of the Metric object are provided to deal with common representations (e.g., single values, array data, status indications) and presentations (e.g., on a display) of measurement data.")
        text("As a base class, the Metric object cannot be instantiated.")
      }
      clause("Numeric object") {
        text("The Numeric object represents numerical measurements and status information, e.g., amplitude measures, counters.")
        example("A heart rate measurement is represented by a Numeric object.")
        note("A compound Numeric object is defined as an efficient model, for example, for arterial blood pressure, which usually has three associated values (i.e., systolic, diastolic, mean). The availability of multiple values in a single Numeric (or other Metric) object can be indicated in a special structure attribute in the Metric object.")
      }
      clause("Sample Array object") {
        text("The Sample Array object is the base class for metrics that have a graphical, curve type presentation and, therefore, have their observation values reported as arrays of data points by communicating systems.")
        text("As a base class, the Sample Array object cannot be instantiated.")
      }
      clause("Real Time Sample Array object") {
        text("The Real Time Sample Array object is a sample array that represents a real-time continuous waveform. As such, it has special requirements in communicating systems, e.g., processing power, low latency, high bandwidth. Therefore, it requires the definition of a specialized object.")
        example("An electrocardiogram (ECG) real-time wave is represented as a Real Time Sample Array object.")
      }
      clause("Time Sample Array object") {
        text("The Time Sample Array object is a sample array that represents noncontinuous waveforms (i.e., a wave snippet). Within a single observation (i.e., a single array of sample values), samples are equidistant in time.Example: Software for ST segment analysis may use the Time Sample Array object to represent snippets of ECG real-time waves that contain only a single QRS complex. Within this wave snippet, the software can locate the ST measurement points. It generates a new snippet, for example, every 15 seconds.")
      }
      clause("Distribution Sample Array object") {
        text("The Distribution Sample Array object is a sample array that represents linear value distributions in the form of arrays containing scaled sample values. The index of a value within an observation array denotes a spatial value, not a time point.")
        example("An electroencephalogram (EEG) application may use a Fourier transformation to derive a frequency distribution (i.e., a spectrum) from the EEG signal. It then uses the Distribution Sample Array object to represent that spectrum in the MDIB.")
      }
      clause("Enumeration object") {
        text("The Enumeration object represents status information and/or annotation information. Observation values may be presented in the form of normative codes (that are included in the nomenclature defined in this standard or in some other nomenclature scheme) or in the form of free text.")
        example("An ECG rhythm qualification may be represented as an Enumeration object. A ventilator may provide information about its current ventilation mode as an Enumeration object.")
      }
      clause("Complex Metric object") {
        text("In special cases, the Complex Metric object can be used to group a larger number of strongly related Metric objects in one single container object for performance or for modeling convenience. The Complex Metric object is a composition of Metric objects, possibly recursive.")
        example("A ventilator device may provide extensive breath analysis capabilities. For each breath, it calculates various numerical values (e.g., volumes, I:E ratio, timing information) as well as enumerated information (e.g., breath type classification, annotation data). For efficiency, all this information is grouped together in one Complex Metric object instance, which is updated upon each breath.")
      }
      clause("PM-Store (i.e., persistent metric) object") {
        text("The PM-Store object provides long-term storage capabilities for metric data. It contains a variable number of PM-Segment objects that can be accessed only through the PM-Store object. Without further specialization, the PM-Store object is intended to store data of a single Metric object only.")
        example("A device stores the numerical value of an invasive blood pressure on a disk. It uses the PM-Store object to represent this persistent information. Attributes of the PM-Store object describe the sampling period, the sampling algorithm, and the storage format. When the label of the pressure measurement is changed (e.g., during a wedge procedure), the storage process opens a new PM-Segment to store the updated context data (here: the label).")
      }
      clause("PM-Segment object") {
        text("The PM-Segment object represents a continuous time period in which a metric is stored without any changes of relevant metric context attributes (e.g., scales, labels).")
        text("The PM-Segment object is accessible only through the PM-Store object (e.g., for retrieving stored data, the PM-Store object has to be accessed).")
      }
    }
    clause("Model for the Alert Package") {
      text("The Alert Package deals with objects that represent status information about patient condition and/or technical conditions influencing the measurement or device functioning. Alert-related information is often subject to normative regulations and, therefore, requires special handling.")
      text("In the model, all alarm-related object-oriented items are identified by the term alert. The term alert is used in this standard as a synonym for the combination of patient-related physiological alarms, technical alarms, and equipment user-advisory signals.")
      text("An alarm is a signal that indicates abnormal events occurring to the patient or the device system. A physiological alarm is a signal that either indicates that a monitored physiological parameter is out of specified limits or indicates an abnormal patient condition. A technical alarm is a signal that indicates a device system is either not capable of accurately monitoring the patient’s condition or no longer monitoring the patient’s condition.")
      text("The model defines three different levels of alarming. These levels represent different sets of alarm processing steps, ranging from a simple context-free alarm event detection to an intelligent device system alarm process. This process is required to prioritize all device alarms, to latch alarms if needed (a latched alarm does not stop when the alarm condition goes away), and to produce audible and visual alarm indications for the user.")
      text("For consistent system-wide alarming, a particular medical device may provide either no alarming capability or exactly one level of alarming, which is dependent on the capabilities of the device. Each level is represented by one specific object class. In other words, either zero or one alarm object class (e.g., only Alert or only Alert Status or only Alert Monitor; no combinations) is instantiated in the device containment tree. Multiple instances of a class are allowed.")
      note("Medical device alarming is subject to various national and international safety standards (e.g., IEC 60601 series, ISO 9703 series). Considering requirements of current safety standards, objects in this standard define information contents only. Any implementation shall, therefore, follow appropriate standards for dynamic alarming behavior.")
      text("Figure 5 shows the object model of the Alert Package.")
      note("Instances of objects in the Alert Package area shall be contained in exactly one superior object.")
      text("The Alert Package model contains the objects described in 6.4.1 through 6.4.3.")
      clause("Alert object") {
        text("The Alert object stands for the status of a simple alarm condition check. As such, it represents a single alarm only. The alarm can be either a physiological alarm or a technical alarm condition of a related object [e.g., MDS (i.e., medical device system), VMD, Metric]. If a device instantiates an Alert object, it shall not instantiate the Alert Status or the Alert Monitor object. A single Alert object is needed for each alarm condition that the device is able to detect.")
        text("The Alert object has a reference to an object instance in the Medical Package to which the alarm condition relates.")
        note("An Alert object instance is not dynamically created or deleted in cases where alarm conditions start or stop. Rather, an existing Alert object instance changes attribute values in these cases.")
        example("An Alert object may represent the status of a process that checks for a limit violation physiological alarm of the heart rate signal. In the case of a violation of the limit, the object generates an event (i.e., attribute update) that represents this alarm condition in the form of attribute value changes.")
        figure("Alert Package model")
      }
      clause("Alert Status object") {
        text("The Alert Status object represents the output of an alarm process that considers all alarm conditions in a scope that spans one or more objects. In contrast to the Alert object, the Alert Status object collects all alarm conditions related to a VMD object hierarchy or related to an MDS object and provides this information in list-structured attributes. Collecting all alarms together allows the implementation of first-level alarm processing where knowledge about the VMD or MDS can be used to prioritize alarm conditions and to suppress known false alarm indications.")
        text("For larger scale devices without complete alarm processing, the Alert Status object greatly reduces the overhead of a large number of Alert object instances.")
        text("If a device instantiates an Alert Status object, it shall not instantiate the Alert or the Alert Monitor object. Each VMD or MDS in the MDIB is able to contain at most one Alert Status object instance.")
        example("An ECG VMD derives a heart rate value. As the VMD is able to detect that the ECG leads are disconnected from the patient, its Alert Status object reports only a technical alarm and suppresses a heart rate limit violation alarm in this case.")
      }
      clause("Alert Monitor object") {
        text("The Alert Monitor object represents the output of a device or system alarm processor. As such, it represents the overall device or system alarm condition and provides a list of all alarm conditions of the system in its scope. This list includes global state information and individual alarm state information that allows the implementation of a safety-standard-compliant alarm display on a remote system.")
        text("If a device instantiates an Alert Monitor object, it shall not instantiate the Alert or the Alert Status object. An MDS shall contain not more than one Alert Monitor object instance.")
        example("A patient-monitoring system provides alarm information in the form of an Alert Monitor object to a central station. Alert information includes the current global maximum severity of audibleand visual alarm conditions on the monitor display as well as a list of active technical and physiological alarm conditions. The alarm processor operates in a latching mode where physiological alarm conditions are buffered until they are explicitly acknowledged by a user.")
      }
    }
    clause("Model for the System Package") {
      text("The System Package deals with the representation of devices that derive or process vital signs information and comply with the definitions in this standard.")
      text("Figure 6 shows the object model for the System Package.")
      figure("System Package model")
      text("The System Package model contains the objects described in 6.5.1 through 6.5.10.")
      clause("VMS (i.e., virtual medical system) object") {
        text("The VMS object is the abstract base class for all System Package objects in this model. It provides consistent naming and identification of system-related objects.")
        text("As a base class, the VMS object cannot be instantiated.")
      }
      clause("MDS object") {
        text("The MDS object is an abstraction of a device that provides medical information in the form of objects that are defined in the Medical Package of the DIM.")
        text("The MDS object is the top-level object in the device’s MDIB and represents the instrument itself. Composite")
        text("devices may contain additional MDS objects in the MDIB.")
        text("Further specializations of this class are used to represent differences in complexity and scope.")
        text("As a base class, the MDS object cannot be instantiated.")
      }
      clause("Simple MDS object") {
        text("The Simple MDS object represents a medical device that contains a single VMD instance only (i.e., a single-purpose device).")
      }
      clause("Hydra MDS object") {
        text("The Hydra MDS object represents a device that contains multiple VMD instances (i.e., a multipurpose device).")
      }
      clause("Composite Single Bed MDS object") {
        text("The Composite Single Bed MDS object represents a device that contains (or interfaces with) one or more Simple or Hydra MDS objects at one location (i.e., a bed).")
      }
      clause("Composite Multiple Bed MDS object") {
        text("The Composite Multiple Bed MDS object represents a device that contains (or interfaces with) multiple MDS objects at multiple locations (i.e., multiple beds).")
      }
      clause("Log object") {
        text("The Log object is a base class that is a storage container for important local system notifications and events. It is possible to define specialized classes for specific event types.")
        text("As a base class, the Log object cannot be instantiated.")
      }
      clause("Event Log object") {
        text("The Event Log object is a general Log object that stores system events in a free-text representation.")
        example("An infusion device may want to keep track of mode and rate changes by remote systems. When a remote operation is invoked, it creates an entry in its event log.")
      }
      clause("Battery object") {
        text("For battery-powered devices, some battery information is contained in the MDS object in the form of attributes. If the battery subsystem is either capable of providing more information (i.e., a smart battery) or manageable, then a special Battery object is provided.")
      }
      clause("Clock object") {
        text("The Clock object provides additional capabilities for handling date-related and time-related information beyond the basic capabilities of an MDS object. It models the real-time clock capabilities of an MDS object.")
        text("The Clock object is used in applications where precise time synchronization of medical devices is needed. This object provides resolution and accuracy information so that applications can synchronize real-time data streams between devices.")
      }
    }
    clause("Model for the Control Package") {
      text("The Control Package contains objects that allow remote measurement control and device control.")
      text("The model for remote control defined in this standard provides the following benefits:")
      text("ULI -- A system that allows remote control is able to explicitly register which attributes or features can be accessed or modified by a remote system.")
      text("ULI -- For attributes that can be remotely modified, a list of possible legal attribute values is provided to the controlling system.")
      text("ULI -- It is not mandatory that a remote-controllable item correspond to an attribute of an medical object.")
      text("ULI -- Dependence of a controllable item on internal system states is modeled.")
      text("ULI -- A simple locking transaction scheme allows the handling of transient states during remote control.")
      text("At least two different uses of remote control are considered:")
      text("ULI -- Automatic control may be done by some processes running on the controlling device. Such a process has to be able to discover automatically how it can modify or access the controllable items to provide its function.")
      text("ULI -- It is also possible to use remote control to present some form of control interface to a human operator. For this use, descriptions of functions, and possibly help information, need to be provided.")
      text("The basic concept presented here is based on Operation objects. An Operation object allows modification of a virtual attribute. This virtual attribute may, for example, be a measurement label, a filter state (on/off), or a gain factor. The attribute is called virtual because it need not correspond to any attribute in other objects instantiated in the system.")
      text("Different specializations of the Operation object define how the virtual attribute is modified. A Select Item operation, for example, allows the selection of an item from a given list of possible item values for the attribute. A Set Value operation allows the setting of the attribute to a value from a defined range with a specific step width (i.e., resolution).")
      text("The idea is that the Operation object provides all necessary information about legal attribute values. Furthermore, the Operation object defines various forms of text string to support a human user of the operation. It also contains grouping information that allows logical grouping of multiple Operation objects together when they are presented as part of a human interface.")
      text("Operation objects cannot directly be accessed by services defined in the service model in Clause 8. Instead, all controls shall be routed through the SCO (i.e., service and control object). This object supports a simple locking mechanism to prevent side effects caused by simultaneous calls.")
      text("The SCO groups together all Operation objects that belong to a specific entity (i.e., MDS, VMD). The SCO also allows feedback to a controlled device, for example, for a visual indication that the device is currently remote-controlled.")
      text("Figure 7 shows the object model for the Control Package:")
      text("The Control Package model contains the objects described in 6.6.1 through 6.6.9.")
      figure("Control Package model")
      clause("SCO") {
        text("The SCO is responsible for managing all remote-control capabilities that are supported by a medical device.")
        text("Remote control in medical device communication is sensitive to safety and security issues. The SCO provides means for the following:")
        text("LI -- Simple transaction processing, which prevents inconsistencies when a device is controlled from multiple access points (e.g., local and remote) and during the processing of control commands.")
        text("LI -- State indications, which allows local and remote indication of ongoing controls.")
      }
      clause("Operation object") {
        text("The Operation object is the abstract base class for classes that represent remote-controllable items. Each Operation object allows the system to modify some specific item (i.e., a virtual attribute) in a specific way defined by the Operation object. Operation objects are not directly accessible by services defined in the service model in Clause 8. All controls shall be routed through the SCO object (i.e., the parent) to allow a simple form of transaction processing.")
        text("The set of Operation objects instantiated by a particular medical device defines the complete remote control interface of the device. This way a host system is able to discover the remote control capabilities of a device in the configuration phase.")
      }
      clause("Select Item Operation object") {
        text("The Select Item Operation object allows the selection of one item out of a given list.")
        example("The invasive pressure VMD may allow modification of its label. It uses a Select Item Operation object for this function. The list of legal values supplied by the operation may be, for example, {ABP, PAP, CVP, LAP}. By invoking the operation, a user is able to select one value out of this list.")
      }
      clause("Set Value Operation object") {
        text("The Set Value Operation object allows the adjustment of a value within a given range with a given resolution.")
        example("A measurement VMD may allow adjustment of a signal gain factor. It uses the Set Value Operation object for this function. The operation provides the supported value range and step width within this range.")
      }
      clause("Set String Operation object") {
        text("The Set String Operation object allows the system to set the contents of an opaque string variable of a given maximum length and format.")
        example("An infusion device may allow a remote system to set the name of the infused drug in free-text form to show it on a local display. It defines an instance of the Set String Operation object for this function. The operation specifies the maximum string length and the character format so that the device is able to show the drug name on a small display.")
      }
      clause("Toggle Flag Operation object") {
        text("The Toggle Flag Operation object allows operation of a toggle switch (with two states, e.g., on/off).")
        example("An ECG VMD may support a line frequency filter. It uses the Toggle Flag Operation object for switching the filter on or off.")
      }
      clause("Activate Operation object") {
        text("The Activate Operation object allows a defined activity to be started (e.g., a zero pressure).")
        example("The zero procedure of an invasive pressure VMD may be started with an Activate Operation object.")
      }
      clause("Limit Alert Operation object") {
        text("The Limit Alert Operation object allows adjustment of the limits of a limit alarm detector and the switching of the limit alarm to on or off.")
      }
      clause("Set Range Operation object") {
        text("The Set Range Operation object allows the selection of a value range by the simultaneous adjustment of a low and high value within defined boundaries.")
        example("A measurement VMD may provide an analog signal input for which the signal input range can be adjusted with a Set Range Operation object.")
      }
    }
    clause("Model for the Extended Services Package") {
      text("The Extended Services Package contains objects that provide extended medical object management services that allow efficient access to medical information in communicating systems. Such access is achieved by a set of objects that package attribute data from multiple objects in a single event message.")
      text("The objects providing extended services are conceptually derived from ISO/OSI system management services defined in the ISO/IEC 10164 family of standards (specifically Part 5 and Part 13). The definitions have been adapted to and optimized for specific needs in the area of vital signs communication between medical devices.")
      text("Figure 8 shows the object model for the Extended Services Package.")
      figure("Extended Services Package model")
      text("The Extended Services Package model contains the objects described in 6.7.1 through 6.7.9.")
      clause("Scanner object") {
        text("A Scanner object is a base class that is an observer and “summarizer” of object attribute values. It observes attributes of managed medical objects and generates summaries in the form of notification event reports. These event reports contain data from multiple objects, which provide a better communication performance compared to separate polling commands (e.g., GET service) or multiple individual event reports from all object instances.")
        text("Objects derived from the Scanner object may be instantiated either by the agent system itself or by the manager system (e.g., dynamic scanner creation by using the CREATE service).")
        text("As a base class, the Scanner object cannot be instantiated.")
      }
      clause("CfgScanner (i.e., configurable scanner) object") {
        text("The CfgScanner object is a base class that has a special attribute (i.e., the ScanList attribute) that allows the system to configure which object attributes are scanned. The ScanList attribute may be modified either by the agent system (i.e., auto-configuration or pre-configuration) or by the manager system (i.e., full dynamic configuration by using the SET service).")
        text("A CfgScanner object may support different granularity for scanning:")
        text("LI -- Attribute group (i.e., a defined set of attributes): The ScanList attribute contains the identifiers (IDs) of attribute groups, and all attributes in the group are scanned.")
        text("LI -- Individual attribute: The ScanList attribute contains the IDs of all attributes that are scanned.")
        text("In order to deal efficiently with optional object attributes, the attribute group scan granularity is recommended for CfgScanner objects.")
        text("As a base class, the CfgScanner object cannot be instantiated.")
      }
      clause("EpiCfgScanner (i.e., episodic configurable scanner) object") {
        text("The EpiCfgScanner object is responsible for observing attributes of managed medical objects and for reporting attribute changes in the form of unbuffered event reports.")
        text("The unbuffered event report is triggered only by object attribute value changes. If the EpiCfgScanner object uses attribute group scan granularity, the event report contains all attributes of the scanned object that belong to this attribute group if one or more of these attributes changed their value.")
        example("A medical device provides heart beat detect events in the form of an Enumeration object. A display application creates an instance of the EpiCfgScanner object and adds the observed value of the Enumeration object to the ScanList attribute. The scanner instance afterwards sends a notification when the Enumeration object reports a heart beat.")
      }
      clause("PeriCfgScanner (i.e., periodic configurable scanner) object") {
        text("The PeriCfgScanner object is responsible for observing attributes of managed medical objects and for periodically reporting attribute values in the form of buffered event reports. A buffered event report contains the attribute values of all available attributes that are specified in the scan list, independent of attribute value changes.")
        example("If the scanner operates in a special superpositive mode, the buffered event report contains all value changes of attributes that occurred in the reporting period; otherwise, the report contains only the most recent attribute values.Example: A data logger creates an instance of the PeriCfgScanner object and configures the scanner so that it sends an update of the observed value attributes of all Numeric objects in the MDIB every 15 seconds.")
      }
      clause("FastPeriCfgScanner (i.e., fast periodic configurable scanner) object") {
        text("The FastPeriCfgScanner object is a specialized object class for scanning the observed value attribute of the Real Time Sample Array object. This special scanner object is further optimized for low-latency reporting and efficient communication bandwidth utilization, which is required to access real-time waveform data.")
        example("A real-time display application (e.g., manager system) wants to display ECG waveforms. It creates a FastPeriCfgScanner object on the agent system (e.g., server device) and requests periodic updates of all ECG leads.")
      }
      clause("UcfgScanner (i.e., unconfigurable scanner) object") {
        text("The UcfgScanner object is a base class that scans a predefined set of managed medical objects that cannot be modified. In other words, an UcfgScanner object typically is a reporting object that is specialized for one specific purpose.")
        text("As a base class, the UcfgScanner object cannot be instantiated.")
      }
      clause("Context Scanner object") {
        text("The Context Scanner object is responsible for observing device configuration changes. After instantiation, the Context Scanner object is responsible for announcing the object instances in the device’s MDIB. The scanner provides the object instance containment hierarchy and static object attribute values.")
        text("In case of dynamic configuration changes, the Context Scanner object sends notifications about new object instances or deleted object instances.")
        example("A data logger creates an instance of the Context Scanner object in an agent MDIB to receive notifications about MDS configuration changes when new measurement modules are plugged in (i.e., new VMD instance) or when such a module is unplugged (i.e., VMD instance deleted).")
      }
      clause("Alert Scanner object") {
        text("The Alert Scanner object is responsible for observing the alert-related attribute groups of objects in the Alert Package. As alarming in general is security-sensitive, the scanner is not configurable (i.e., all or no Alert objects are scanned).")
        text("The Alert Scanner object sends event reports periodically so that timeout conditions can be checked.")
      }
      clause("Operating Scanner object") {
        text("The Operating Scanner object is responsible for providing all information about the operating and control system of a medical device.")
        text("In other words, the scanner maintains the configuration of Operation objects contained in SCOs (by sending CREATE notifications for Operation objects), it scans transaction-handling-related SCO attributes, and it scans Operation object attributes. Because SCOs and Operation objects may have dependencies, the scanner is not configurable.")
      }
    }
    clause("Model for the Communication Package") {
      text("The Communication Package deals with objects that enable and support basic communication.")
      text("Figure 9 shows the object model for the Communication Package.")
      figure("Communication Package model")
      text("The Communication Package model contains the objects described in 6.8.1 through 6.8.6.")
      clause("Communication Controller object") {
        text("The Communication Controller object represents the upper layer and lower layer communication profile of a medical device.")
        text("The Communication Controller object is the access point for retrieving Device Interface attributes and management information base element (MibElement) attributes for obtaining management information related to data communications.")
        text("As a base class, the Communication Controller object cannot be instantiated. Two instantiable specializations, however, are defined: BCC and DCC. A medical device MDIB contains either no Communication Controller object or one BCC object or one DCC object (depending on its role).")
      }
      clause("DCC (i.e., device communication controller) object") {
        text("The DCC object is a Communication Controller object used by medical devices operating as agent systems (i.e., association responders).")
        text("The DCC object shall contain one or more Device Interface objects.")
      }
      clause("BCC (i.e., bedside communication controller) object") {
        text("The BCC object is a Communication Controller object used by medical devices operating as manager systems (i.e., association requestors).")
        text("The BCC object shall contain one or more Device Interface objects.")
      }
      clause("Device Interface object") {
        text("The Device Interface object represents a particular interface, i.e., port. The port is either a logical or a physical end point of an association for which (e.g., statistical) data captured in the MibElement objects can be independently collected.")
        text("Both an agent system and a manager system can have multiple logical or physical ports, depending on the selected implementation of the lower layer communication system.")
        text("The Device Interface object is not accessible by CMDISE services. This object contains at least one Mib-Element object (i.e., the Device Interface MibElement object, which represents device interface properties), which can be accessed by a special method defined by the Communication Controller object.")
      }
      clause("MibElement object") {
        text("The MibElement object contains statistics and performance data for one Device Interface object. The MibElement object is a base class for specialized MibElement objects only. It cannot be instantiated.")
        text("Various MibElement object types are defined to group management information in defined packages, which can be generic or dependent on specific transport profiles.")
        text("The MibElement object is not directly accessible. Its attributes can be accessed only through a Communication Controller object. The MibElement object is not part of the device’s MDIB.")
      }
      clause("Specialized MibElement object") {
        text("Management information for a communication link is dependent on the lower layers of the communication stack (i.e., lower layers profile).")
        text("This standard, however, defines only two generic MibElement objects:")
        text("ULI -- The mandatory Device Interface MibElement object, which describes properties of the device interface")
        text("ULI -- An optional general communication statistics MibElement object, which models typical communication statistics that are generally applicable")
        text("Specialized MibElement objects are defined in IEEE P1073.2.1.2.")
      }
    }
    clause("Model for the Archival Package") {
      text("The Archival Package deals with storage and representation of biosignals, status, and context information inan on-line or an off-line archive.")
      text("Figure 10 shows the object model of the Archival Package.")
      text("The Archival Package model contains the objects described in 6.9.1 through 6.9.7.")
      figure("Archival Package model")
      clause("Multipatient Archive object") {
        text("The Multipatient Archive object groups together multiple Patient Archive objects referring to different patients.")
        example("A drug study may be documented in the form of a Multipatient Archive object containing multiple Patient Archive objects that show how the drug affected the monitored vital signs.")
      }
      clause("Patient Archive object") {
        text("The Patient Archive object groups patient-related information (e.g., vital signs data, treatment data, and patient demographics) together in a single archive object. This object relates to static (i.e., invariant) data in a Patient Demographics object only.")
        example("A hospital may store data about multiple visits of a single patient in a Patient Archive object that contains a number of Session Archive objects, each documenting vital signs information recorded during a specific visit in a hospital department.")
      }
      clause("Session Archive object") {
        text("The Session Archive object represents a patient visit or a continuous stay in the a hospital or hospital department. Diagnostic treatments performed during this time period are represented by Session Test objects contained in the Session Archive object. The Session Archive object refers to dynamic (i.e., variant) data in a Patient Demographics object.")
      }
      clause("Physician Archive object") {
        text("The Physician object represents the physician responsible for the set of diagnostic and therapeutic activities during the time period represented by the Session Archive object.")
      }
      clause("Session Test object") {
        text("The Session Test object contains vital signs information of a single patient that is recorded during a single examination or diagnostic treatment. This object contains vital signs metrics in form of PM-Store objects. It also may contain information about equipment that was used for recording (in the form of relations to MDS and Ancillary objects).")
        example("Vital signs information recorded during a ECG stress test examination is organized in a Session Test object.")
      }
      clause("Session Notes object") {
        text("The Session Notes object is a container for diagnostic data, patient care details, and treatment-related information in the form of textual data.")
      }
      clause("Ancillary object") {
        text("The Ancillary object is not further defined in this standard. This object is present in the model to indicate that information from sources other than devices within the scope of this standard are permitted to be included in (or referenced by) the Session Test object.")
        example("Image data that complies with the MEDICOM (CEN ENV 12052) or DICOM (NEMA PS 3) standard are permitted to be included in the Session Test object as ancillary data.")
      }
    }
    clause("Model for the Patient Package") {
      text("The Patient Package deals with all patient-related information that is relevant in the scope of this standard,")
      text("but is not vital signs information modeled in the Medical Package.")
      text("Figure 11 shows the object model for the Patient Package:")
      text("The Patient Package model contains one object (see 6.10.1).")
      figure("Patient Package model")
      clause("Patient Demographics object") {
        text("The Patient Demographics object stores patient census data.")
        text("This standard provides minimal patient information as typically required by medical devices. A complete patient record is outside the scope of this standard.")
      }
    }
    clause("DIM—dynamic model") {
      clause("General") {
        text("Subclause 6.11 defines global dynamic system behavior.")
        text("Note that dynamic object behavior resulting from invocation of object management services is part of the object definitions in Clause 7.")
      }
      clause("MDS communication finite state machine (FSM)") {
        text("Figure 12 shows the MDS FSM for a communicating medical device that complies with the definitions in this standard. The FSM is used to synchronize the operational behavior of manager (i.e., client) systems and agent (i.e., server) systems.")
        text("After power-up, the device performs all necessary local initializations (i.e, boot phase) and ends up in the disconnected state, where it waits for connection events.")
        text("When a connection event is detected, the device tries to establish a logical connection (i.e, an association) with the other device. A manager (i.e., client) system is the association requester, and an agent (i.e, server) system is the association responder. Basic compatibility checks are performed in the associating state.")
        figure("MDS FSM")
        text("After successful association, configuration data (i.e., the MDIB structure) is exchanged by the use of services and extended services (in particular, the Context Scanner object) as defined in this standard. Additional information (e.g., MDS attributes) is supplied that allows further compatibility and state checks.")
        text("After configuration, medical data are exchanged by using services and extended services as defined in this standard. Dynamic reconfiguration is allowed in the operating state. If the device or the type of reconfiguration does not allow dynamic handling in the operating state, a special “reconfiguring” state is provided.")
        text("If an event indicates an intention to disconnect, the disassociating state is entered.")
        text("The diagram does not show error events. Fatal error events take the state machine out of the operating state.")
        note("This state machine describes the behavior of the MDS communication system only. Usually the device must perform its medical function independent of the communication system.")
        text("The FSM is considered a part of the MDS object. The MDS Status attribute reflects the state of the machine. The MDS may announce state changes in the form of attribute change event reports.")
        text("Specific application profiles shall use this state machine as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.")
      }
      clause("Communicating systems—startup object interaction diagram") {
        text("Figure 13 presents the object interaction diagram that visualizes the startup phase after connecting two devices.")
        figure("Startup after connection")
        text("It is assumed here that, conceptually, messages are exchanged between the Communication Controller objects (using the device interface).")
        text("Some form of connection indication is necessary to make the manager system aware of a new agent on the network. This mechanism is dependent on the specific lower layer implementation; therefore, it is not further defined in this standard.")
        text("Specific application profiles shall use this interaction diagram as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.")
      }
      clause("Communication Package—MibElement data access") {
        text("Figure 14 presents the object interaction diagram that shows how a manager system accesses the MibElement data using the objects defined in the Communication Package (see 6.8).")
        figure("MibElement data access")
        text("The diagram assumes the following:")
        text("ULI -- An association is established between the agent and the manager.")
        text("ULI -- The configuration phase is finished, and the manager has an image of the agent’s MDIB.")
        text("ULI -- The DCC object is part of the agent’s MDIB.")
        text("The manager first uses the GET service to retrieve all DCC object attributes and their values. The attributes specify how many Device Interface objects exist.")
        text("The manager uses the ACTION service with the Communication Controller object-defined method to retrieve the attributes of the mandatory Device Interface MibElement object. The MibElement attribute variables specify if any additional MibElement objects are available for the interface.")
        text("If so, the manager can use the same ACTION command to retrieve the additional management information represented in the MibElement objects.")
      }
      clause("Dynamic object relations") {
        text("This subclause deals with relations between managed medical objects (i.e., objects that are defined as managed objects in this standard).")
        text("Generally, the relationships between object instances that are defined in the package models are dynamic.")
        example("A modular patient monitor is modeled as an MDS. Measurement modules are modeled as VMDs. If a new module is connected to the monitor, there is also a new relationship between the MDS and the new VMD instance.")
        text("Communicating agent systems (i.e., agents) use services defined in this standard to announce configuration change events to other connected systems. These manager systems (i.e., managers) modify their view of the agent MDIB.")
        text("Not only does a vital signs information archive have to update its configuration, but it also has to permanently store these connection and disconnection events.")
        example("An instance of the Session Archive object represents the stay of a patient in the intensive care unit (ICU). During that period, new devices are connected to the patient to increase the number of recorded vital signs. They are removed again as soon as the patient’s condition stabilizes. The Session Archive object shall not delete recorded data when the recording device is disconnected.")
        text("Thus, in certain applications (e.g., archival applications), object instance relationships have associated information that must be captured.")
        text("When required, the relationships themselves can be considered to be special managed objects as shown in Figure 15.")
        figure("Example of a relationship represented by an object")
        text("The example in Figure 15 shows a relation between a Session Archive object and an MDS object. The relation is represented as an object. This object has attributes that provide information, for example, about time of connection and disconnection.")
        text("Modeling the relations as objects has the advantage that information can be defined in the form of attributes. It is not necessary to assign the attributes to one or both objects that are related.")
        text("How dynamic object relations are handled by systems that comply with the definitions in this standard is defined in 6.11.5.1 and 6.11.5.2.")
        clause("Dynamic object relations in communicating systems") {
          text("Relations between object instances that are defined in the package models are considered configuration information. The Context Scanner object provides configuration information in the form of object create notifications and object delete notifications. No means for persistent storage of past (i.e., old) configuration information is defined for communicating systems.")
          text("Other relations between object instances (e.g., to reference a source signal in derived data) are specified in the form of attributes of the corresponding objects (e.g., the Vmo-Source-List attribute of the Metric object). Dynamic changes of these attributes are announced by attribute change notification events.")
        }
        clause("Dynamic object relations in archival systems") {
          text("An archival system may need to provide persistent storage for configuration information. In this case, the corresponding relations are considered to be represented by objects, as shown in 6.11.5.")
          text("An archival system that uses a data format in compliance with the definitions in this standard has to provide means to store (i.e., archive) the attributes of dynamic relationship objects.")
        }
      }
    }
  }
  clause("DIM class definitions") {
    clause("General") {
      text("<p>Clause <!--[if supportFields]><span style='font-size:12.0pt;mso-bidi-font-size:\r\n10.0pt;font-family:\"Times New Roman\";mso-fareast-font-family:\"Times New Roman\";\r\nmso-ansi-language:EN-US;mso-fareast-language:JA;mso-bidi-language:AR-SA'><span\r\nstyle='mso-element:field-begin'></span><span\r\nstyle='mso-spacerun:yes'> </span>REF _Ref277416385 \\r \\h <span\r\nstyle='mso-element:field-separator'></span></span><![endif]-->7<!--[if gte mso 9]><xml>\r\n <w:data>08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000E0000005F005200650066003200370037003400310036003300380035000000</w:data>\r\n</xml><![endif]--><!--[if supportFields]><span style='font-size:12.0pt;\r\nmso-bidi-font-size:10.0pt;font-family:\"Times New Roman\";mso-fareast-font-family:\r\n\"Times New Roman\";mso-ansi-language:EN-US;mso-fareast-language:JA;mso-bidi-language:\r\nAR-SA'><span style='mso-element:field-end'></span></span><![endif]--> contains the object definitions for all objects in the DIM. The packages defined in the model are used to categorize objects. Attributes, behavior, and notifications are defined for each object class.</p>")
      clause("Notation") {
        text("<p>Each object is defined in a separate subclause (see <!--[if supportFields]><span\r\nstyle='mso-element:field-begin'></span><span\r\nstyle='mso-spacerun:yes'> </span>REF _Ref278218167 \\r \\h <span\r\nstyle='mso-element:field-separator'></span><![endif]-->7.2<!--[if gte mso 9]><xml>\r\n <w:data>08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000E0000005F005200650066003200370038003200310038003100360037000000</w:data>\r\n</xml><![endif]--><!--[if supportFields]><span style='mso-element:field-end'></span><![endif]--> through <!--[if supportFields]><span style='mso-element:field-begin'></span><span\r\nstyle='mso-spacerun:yes'> </span>REF _Ref278218195 \\r \\h <span\r\nstyle='mso-element:field-separator'></span><![endif]-->7.10<!--[if gte mso 9]><xml>\r\n <w:data>08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000E0000005F005200650066003200370038003200310038003100390035000000</w:data>\r\n</xml><![endif]--><!--[if supportFields]><span style='mso-element:field-end'></span><![endif]-->). Further subclauses define attributes, behavior, and notifications for the objects.</p>\r\n\r\n<p>The object is defined in a subclause as follows:</p>\r\n\r\n<p><strong>Object</strong>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Defines the name of the object.</p>\r\n\r\n<p><strong>Description</strong>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &ldquo;Gives a short, informative textual description of the object.&rdquo;</p>\r\n\r\n<p><strong>Derived From</strong>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Defines potential base classes of the object.</p>\r\n\r\n<p><strong>Name Binding</strong>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Defines the attribute that uniquely identifies an instance of the object in a given context. For manageable objects, this definition is the Handle attribute and the context is the device system (i.e., single MDS context). See also <!--[if supportFields]><span\r\nstyle='font-size:9.0pt'><span style='mso-element:field-begin'></span><span\r\nstyle='mso-spacerun:yes'> </span>REF _Ref278218227 \\r \\h <span\r\nstyle='mso-element:field-separator'></span></span><![endif]-->7.1.2.5<!--[if gte mso 9]><xml>\r\n <w:data>08D0C9EA79F9BACE118C8200AA004BA90B02000000080000000E0000005F005200650066003200370038003200310038003200320037000000</w:data>\r\n</xml><![endif]--><!--[if supportFields]><span style='font-size:9.0pt'><span\r\nstyle='mso-element:field-end'></span></span><![endif]-->.</p>\r\n\r\n<p><strong>Registered As</strong>: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Defines a term that is defined in the nomenclature to allow unique identification [e.g., object identifier (OID), code] of the object.</p>\r\n\r\n<p>Each object attribute is defined in an attribute subclause. Tables define attribute names, unique attribute IDs, attribute data types, and certain qualifiers. The qualifiers have the following meaning:</p>\r\n\r\n<p><strong>M&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong>attribute is mandatory</p>\r\n\r\n<p><strong>O</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; attribute is optional</p>\r\n\r\n<p><strong>C</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; attribute is conditional; availability of attribute depends on a predefined condition</p>\r\n\r\n<p>Unless otherwise noted, the attribute definition tables do not show inherited attributes again. In other words, the attribute lists of all base classes have to be checked for a complete list of object attributes.</p>\r\n\r\n<p>Attributes are assigned to, or grouped together in. attribute groups so attributes can be classified according to their use (e.g., static context information, dynamic context information, value observations). The grouping also makes it possible to effectively deal with optional attributes: A GET service facilitates the retrieval of all members of the group so an application is able to determine which attributes are actually present in a particular object instance.</p>\r\n\r\n<p>Attribute groups may be extensible. In other words, a derived object class is able to add additional members to an inherited attribute group.</p>\r\n\r\n<p>Attribute groups are also defined in tables that specify group identification and the list of group members. Inherited attribute groups are not shown in the attribute group tables again unless these groups are extensible.</p>\r\n\r\n<p>Special methods or functions that are provided by an object class are defined in a behavior subclause. These methods can be invoked by the CMDISE ACTION service.</p>\r\n\r\n<p>Events generated by an object class (other than a generic attribute change notification) are defined in a notifications subclause. An object reports these events by using the CMDISE EVENT REPORT service.</p>")
      } #"
      clause("Common data types") {
        text("<p>This subclause defines a set of ASN.1 data types that are used in the object definitions.</p>")
        text("<p>The external nomenclature reference data type is a special data type that is defined for this function as follows:</p>")
        clause("Integer and bit string data types") {
          text("<p>For representing integer numbers, the object definitions use fixed-size data types only. The bit string data type represents a bit field where each single bit has a defined meaning (i.e., flag fields). The following inte- ger data types and bit string data types are used:</p>")
          code("Data Type: INT-U8") {
            model_element("DIM::CommonDataTypes::ASN1::INT-U8")
          }
          code("Data Type: INT-I8") {
            model_element("DIM::CommonDataTypes::ASN1::INT-I8")
          }
          code("Data Type: INT-U16") {
            model_element("DIM::CommonDataTypes::ASN1::INT-U16")
          }
          code("Data Type: INT-I16") {
            model_element("DIM::CommonDataTypes::ASN1::INT-I16")
          }
          code("Data Type: INT-U32") {
            model_element("DIM::CommonDataTypes::ASN1::INT-U32")
          }
          code("Data Type: INT-I32") {
            model_element("DIM::CommonDataTypes::ASN1::INT-I32")
          }
          code("Data Type: BITS-8") {
            model_element("DIM::CommonDataTypes::ASN1::BITS-8")
          }
          code("Data Type: BITS-16") {
            model_element("DIM::CommonDataTypes::ASN1::BITS-16")
          }
          code("Data Type: BITS-32") {
            model_element("DIM::CommonDataTypes::ASN1::BITS-32")
          }
          note("<p>When interpreting integer numbers, the representation (e.g., little endian versus big endian) has to be considered. Communicating systems negotiate this representation at association (i.e., transfer syntax). Archival data formats have to provide a mechanism to uniquely identify integer representation (e.g., a field in a specification header).</p>")
          note("<p>In the object definitions, integer and bit string data types with named constants or named bits also use the above notation for simplicity. The above notation is illegal ASN.1 syntax, but it can be easily transformed to the correct syntax.</p>")
        }
        clause("Identification data type") {
          text("<p>All elements (e.g., classes, objects, measurement types) that need unique identification are assigned an OID. The set of valid OIDs for this standard is defined in ISO/IEEE 11073-10101. The nomenclature is split into a set of partitions, and each partition has its own range of 16-bit codes. In other words, the 16-bit code is context-sensitive.</p>")
          text("<p>The 16-bit identification data type is defined as follows:</p>")
          code("OID-Type") {  
            model_element("DIM::CommonDataTypes::ASN1::OID-Type")
          }
          text("<p>For IDs that are not part of the standard nomenclature (i.e., private or manufacturer-specific codes), a special type is defined as follows:</p>")
          code("PrivateOid") {
            model_element("DIM::CommonDataTypes::ASN1::PrivateOid")
          }
        }
        clause("Handle data type") {
          text("<p>The handle data type is used for efficient, locally unique identification of all managed medical object instances. (<em>Locally unique </em>means unique within one MDS context.) This data type is defined as follows:</p>")
          code("Handle") {
            model_element("DIM::CommonDataTypes::ASN1::HANDLE")
          }
        }
        clause("Instance number data type") {
          text("<p>The instance number data type is used to distinguish class or object instances of the same type or object instances that are not directly manageable (i.e., used, e.g., as the Name Binding attribute for Operation objects). This data type is defined as follows:</p>")
          code("InstNumber") {
            model_element("DIM::CommonDataTypes::ASN1::InstNumber")
          }
        }
        clause("Global object identification") {
          text("<p>Handle and instance number data types must be unique inside one specific naming context (e.g., handles are unique within at least one MDS context). This uniqueness allows the identification of an object instance within its naming context by one single, small identifier.</p>")
          code("GLB-HANDLE") {
            model_element("DIM::CommonDataTypes::ASN1::GLB-HANDLE")
          }
          code("ManagedObjectId") {
            model_element("DIM::CommonDataTypes::ASN1::ManagedObjectId")
          }
          example("<p>A medical device may interface with further medical devices (i.e., sub-devices). In the MDIB, this device may model these sub-devices as individual MDS objects with their own naming context. In this way, name space collisions (e.g., duplicate handle values, duplicate nomenclature codes) can be avoided without reassigning handle values. A manager system needs to interpret the MDS context IDs together with handle values to uniquely identify object instances within this composite MDIB. The con- text IDs are assigned when the MDIB is created by Context Scanner object create notifications.</p>")
          text("<p>Assumptions and possible restrictions about different naming contexts within an MDIB are profile depen- dent. They are, therefore, defined in the ISO/IEEE P11073-202xx series.</p>")
        }
        clause("Type ID data type") {
          text("<p>The type ID data type is used in the VMOs and VMS objects to provide specific static information about the type of an object instance (e.g., blood pressure could be the type of a Numeric object). Codes defined in the nomenclature are used. The nomenclature contains a number of partitions, and code values are unique only within one partition. As the type ID data type should be context-free, the partition of the nomenclature code is also provided. This data type is defined as follows:</p>")
          code("Data Type: TYPE") {
            model_element("DIM::CommonDataTypes::ASN1::TYPE")
          }
          code("Data Type: NomPartition") {
            model_element("DIM::CommonDataTypes::ASN1::NomPartition")
          }
        }
        clause("Attribute value assertion data type") {
          text("<p>A number of services defined in the service model in Clause 8 provide access to object attributes (e.g., GET, SET). Typically, the attribute has to be identified by means of an attribute ID. The attribute data type itself is dependent on this ID. The attribute value assertion data type represents this ID-value pair and is defined as follows:</p>")
          code("AVA-Type") {
            model_element("DIM::CommonDataTypes::ASN1::AVA-Type")
          }
        }
        clause("Attribute list data type") {
          text("<p>Frequently, a list of attribute ID&ndash;attribute value pairs is needed. The attribute list data type is a special data type that is provided for this situation and is defined as follows:</p>")
          code("AttributeList") {
            model_element("DIM::CommonDataTypes::ASN1::AttributeList")
          }
        }
        clause("Attribute ID list data type") {
          text("<p>Frequently, a list of attribute IDs is used. The attribute ID list data type is a special type that is provided for convenience and is defined as follows:</p>")
          code("AttributeIdList") {
            model_element("DIM::CommonDataTypes::ASN1::AttributeIdList")
          }
        }
        clause("Floating point type data type") {
          text("<p>For performance and efficiency, the object definitions use the floating point type data type, which is a special data type for representing floating point numbers. It is assumed that this data type is 32 bits. This data type is defined as follows:</p>")
          code("FLOAT-Type") {
            model_element("DIM::CommonDataTypes::ASN1::FLOAT-Type")
          }
          text("<p>The concrete floating point number format is either explicitly negotiated at association or implicitly defined by the association application context.</p>")
        }
        clause("Relative time data type") {
          text("<p>The relative time data type is a high-resolution time definition relative to some event (e.g., a synchronization event at startup). This data type is used to position events relative to each other. It is defined as follows:</p>")
          code("RelativeTime") {
            model_element("DIM::CommonDataTypes::ASN1::RelativeTime")
          }
          text("<p>Note that the time accuracy is defined by the system itself.</p>")
        }
        clause("High-resolution relative time data type") {
          text("<p>If either the resolution or the time span of the previously defined relative time data type is not sufficient, a high-resolution relative time data type is defined. The data type is 64 bits long. However, as there is no 64- bit integer data type defined, an opaque (i.e., string) data structure is used. The type is defined as follows:</p>")
          code("HighResRelativeTime") {
            model_element("DIM::CommonDataTypes::ASN1::HighResRelativeTime")
          }
          text("<p>Note that the time accuracy is defined by the system itself.</p>")
        }
        clause("Absolute time data type") {
          text("<p>Absolute time data type specifies the time of day with at least a resolution of 1 s. For efficiency, the values in the structure are BCD-encoded (i.e., 4-bit nibbles). The year 1996, for example, is represented by the hexa- decimal value 0x19 in the century field and the hexadecimal value 0x96 in the year field. This format can easily be converted to character-based or integer-based representations. The absolute time data type is defined as follows:</p>")
          code("AbsoluteTime") {
            model_element("DIM::CommonDataTypes::ASN1::AbsoluteTime")
          }
        }
        clause("Date data type") {
          text("<p>The date data type is used to specify a certain calendar date. For ease of transformation, the data type has the same encoding (i.e., BCD) as the absolute time data type. The date data type is defined as follows:</p>")
          code("Date") {
            model_element("DIM::CommonDataTypes::ASN1::Date")
          }
        }
        clause("Data Type: OperationalState") {
          text("<p>The operational state data type defines if a certain object or other property is enabled or disabled. The defini- tions are derived from ISO/IEC 10164-2 and are as follows:</p>")
          code("OperationalState") {
            model_element("DIM::CommonDataTypes::ASN1::OperationalState")
          }
        }
        clause("Administrative state data type") {
          text("<p>The administrative state data type defines if a certain object is locked or unlocked. The definitions are derived from ISO/IEC 10164-2 and are as follows:</p>")
          code("AdministrativeState") {
            model_element("DIM::CommonDataTypes::ASN1::AdministrativeState")
          }
        }
        clause("Color data type") {
          text("<p>The color data type represents the basic RGB colors and is defined as follows:</p>")
          code("SimpleColour") {
            model_element("DIM::CommonDataTypes::ASN1::SimpleColour")
          }
        }
        clause("Locale data type") {
          text("<p>The locale data type shall be used to specify language and encoding information for data types that represent human-readable text strings. This data type is defined as follows:</p>")
          code("Locale") {
            model_element("DIM::CommonDataTypes::ASN1::Locale")
          }
          code("CharSet") {
            model_element("DIM::CommonDataTypes::ASN1::CharSet")
          }
          code("StringSpec") {
            model_element("DIM::CommonDataTypes::ASN1::StringSpec")
          }
          code("StringFlags") {
            model_element("DIM::CommonDataTypes::ASN1::StringFlags")
          }
          text("<p>The field Locale::language shall represent the lowercase ISO/IEC 646 representation of a two-character lan- guage ID code from ISO 639-1 or ISO 639-2. For processing convenience, the language ID is stored in a 32- bit integer field. The first octet of the code is stored in the most significant byte of this field. Unused octets in the field are filled with NULL bytes.</p>")
          example("<p>Language: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &ldquo;English&rdquo;</p>\r\n\r\n<p>Language identifier: &nbsp; &nbsp; &ldquo;en&rdquo;</p>\r\n\r\n<p>Encoding: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;65 6E 00 00h</p>\r\n\r\n<p>&nbsp;</p>")
          text("<p>The field Locale::country shall represent the uppercase ISO/IEC 646 representation of a two-character coun- try ID code from ISO 3166-1, ISO 3166-2, or ISO 3166-3. For processing convenience, the country ID is stored in a 32-bit integer field. The first octet of the code is stored in the most significant byte of this field. Unused octets of the field are filled with NULL bytes.</p>\r\n\r\n<p>The country code can be used to distinguish between certain aspects of the same language used in different countries, e.g., English in the United States versus English in the United Kingdom.</p>\r\n\r\n<p>If no specific country is defined, this field shall be set to 0.</p>")
          example("<p>Country: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &ldquo;United States&rdquo;</p>\r\n\r\n<p>Country identifier: &nbsp; &nbsp; &nbsp;&ldquo;US&rdquo;</p>\r\n\r\n<p>Encoding:&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;55 53 00 00h</p>")
          text("<p>The field Locale::charset denotes the encoding scheme of the characters used in string data types represent- ing readable text.</p>\r\n\r\n<p>For interoperability, the character encoding scheme iso-10646-ucs-2 is recommended. This encoding scheme corresponds to ISO/IEC 10646 with a 2-octet (i.e., 16-bit per character) big-endian encoding, repre- senting the basic multilingual plane (BMP). The character codes within ISO/IEC 10646 do not correspond directly with glyphs, i.e., the graphical representation of a character. Also the ISO/IEC 10646 is language independent. Other Locale::charset values may be more language dependent because they also specify a cer- tain character repertoire.</p>")
        }
        clause("External nomenclature reference data type") {
          text("<p>In certain cases, it is required to refer to standard coding systems (i.e., nomenclatures) that are outside the scope of this standard.</p>")
          example("<p>The nomenclature defined in this standard does not define diagnostic codes or procedure codes. However, it is possible to reference a different coding system and provide the information in the form of an external code.</p>")
          example("<p>The nomenclature defined in this standard does not define diagnostic codes or procedure codes. However, it is possible to reference a different coding system and provide the information in the form of an external code.</p>")
          code("ExtNomenRef") {
            model_element("DIM::CommonDataTypes::ASN1::ExtNomenRef")
          }
        }
        clause("External object relation list data type") {
          text("<p>In certain cases, managed medical objects defined in the DIM may have relations to other objects that are not defined in this standard (i.e., they are external to the definitions).</p>\r\n\r\n<p>The external object relation list data type can be used to provide information about these objects and the par- ticular relation. This data type is defined as follows:</p>")
          code("ExtObjRelationList") {
            model_element("DIM::CommonDataTypes::ASN1::ExtObjRelationList")
          }
          code("ExtObjRelationEntry") {
            model_element("DIM::CommonDataTypes::ASN1::ExtObjRelationEntry")
          }
          example("<p>In certain situations, it is necessary to record specific production information (e.g., serial number) of a transducer that is used to derive a measurement. The transducer in this standard is not defined as a managed medical object. Therefore, the VMD object instances use a relation entry to supply the information, e.g., {relation-type = is-connected; related-object = Transducer; relation-attributes = {model, &ldquo;A-Model,&rdquo; serial-number = &ldquo;12345&rdquo;}}.</p>")
          example("<p>A certain numerical measurement value is manually validated by a nurse. A charting system keeps information about manual validations. The nurse is not modeled as an object in this standard. Therefore, the charting system uses a relation entry as an additional attribute of the Numeric object, e.g., {relation-type = validated-by; related-object = Nurse; relation-attributes = {name, &ldquo;C. Smith,&rdquo; date, &ldquo;041295&rdquo;}}</p>")
          text("<p>The external object relation list data type is a very powerful concept to extend the information model with- out really defining additional objects.</p>")
        }
      }
    }
    clause("Class Top") {
      model_element("DIM::Top::Top")
    }
    clause("Package Medical") {
      clause("Class VMO") {
        model_element("DIM::Medical::VMO")
      }
      clause("Class VMD") {
        model_element("DIM::Medical::VMD")
      }
      clause("Class Channel") {
        model_element("DIM::Medical::Channel")
      }
      clause("Class Metric") {
        model_element("DIM::Medical::Metric")
      }
      clause("Class Numeric") {
        model_element("DIM::Medical::Numeric")
      }
      clause("Class SampleArray") {
        model_element("DIM::Medical::SampleArray")
      }
      clause("Class RealTimeSampleArray") {
        model_element("DIM::Medical::RealTimeSampleArray")
      }
      clause("Class TimeSampleArray") {
        model_element("DIM::Medical::TimeSampleArray")
      }
      clause("Class DistributionSampleArray") {
        model_element("DIM::Medical::DistributionSampleArray")
      }
      clause("Class Enumeration") {
        model_element("DIM::Medical::Enumeration")
      }
      clause("Class ComplexMetric") {
        model_element("DIM::Medical::ComplexMetric")
      }
      clause("Class PMSegment") {
        model_element("DIM::Medical::PMSegment")
      }
      clause("Class PMStore") {
        model_element("DIM::Medical::PMStore")
      }
      clause("ASN1 Data Types for the Medical Package") {
        code("Data Type: EnumObsValueIface") {
          model_element("DIM::Medical::ASN1::EnumObsValueIface")
        }
        code("Data Type: NuObsValueIface") {
          model_element("DIM::Medical::ASN1::NuObsValueIface")
        }
        code("Data Type: SaCalibrationData_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::SaCalibrationData_ANON_CHOICE")
        }
        code("Data Type: SaMarkerList_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::SaMarkerList_ANON_CHOICE")
        }
        code("Data Type: SaObsValueIface") {
          model_element("DIM::Medical::ASN1::SaObsValueIface")
        }
        code("Data Type: SaPhysiologicalRange_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::SaPhysiologicalRange_ANON_CHOICE")
        }
        code("Data Type: ChannelStatus") {
          model_element("DIM::Medical::ASN1::ChannelStatus")
        }
        code("Data Type: ScaleAndRangeSpecification_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::ScaleAndRangeSpecification_ANON_CHOICE")
        }
        code("Data Type: CmplxFlags") {
          model_element("DIM::Medical::ASN1::CmplxFlags")
        }
        code("Data Type: SegmentData_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::SegmentData_ANON_CHOICE")
        }
        code("Data Type: CmplxObsElemFlags") {
          model_element("DIM::Medical::ASN1::CmplxObsElemFlags")
        }
        code("Data Type: SiteList_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::SiteList_ANON_CHOICE")
        }
        code("Data Type: MeasurementStatus") {
          model_element("DIM::Medical::ASN1::MeasurementStatus")
        }
        code("Data Type: VisualGrid_ANON_CHOICE") {
          model_element("DIM::Medical::ASN1::VisualGrid_ANON_CHOICE")
        }
        code("Data Type: MetricAccess") {
          model_element("DIM::Medical::ASN1::MetricAccess")
        }
        code("Data Type: MetricCalState") {
          model_element("DIM::Medical::ASN1::MetricCalState")
        }
        code("Data Type: MetricCalType") {
          model_element("DIM::Medical::ASN1::MetricCalType")
        }
        code("Data Type: MetricCategory") {
          model_element("DIM::Medical::ASN1::MetricCategory")
        }
        code("Data Type: MetricRelevance") {
          model_element("DIM::Medical::ASN1::MetricRelevance")
        }
        code("Data Type: MetricStatus") {
          model_element("DIM::Medical::ASN1::MetricStatus")
        }
        code("Data Type: AbsTimeRange") {
          model_element("DIM::Medical::ASN1::AbsTimeRange")
        }
        code("Data Type: MetricStructureMSStruct_ANON_ENUM") {
          model_element("DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM")
        }
        code("Data Type: AbsoluteRange") {
          model_element("DIM::Medical::ASN1::AbsoluteRange")
        }
        code("Data Type: MsmtPrinciple") {
          model_element("DIM::Medical::ASN1::MsmtPrinciple")
        }
        code("Data Type: CmplxDynAttr") {
          model_element("DIM::Medical::ASN1::CmplxDynAttr")
        }
        code("Data Type: SaCalDataType") {
          model_element("DIM::Medical::ASN1::SaCalDataType")
        }
        code("Data Type: CmplxDynAttrElem") {
          model_element("DIM::Medical::ASN1::CmplxDynAttrElem")
        }
        code("Data Type: SaFilterEntryFilterType_ANON_ENUM") {
          model_element("DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM")
        }
        code("Data Type: CmplxDynAttrElemList") {
          model_element("DIM::Medical::ASN1::CmplxDynAttrElemList")
        }
        code("Data Type: SaFlags") {
          model_element("DIM::Medical::ASN1::SaFlags")
        }
        code("Data Type: CmplxElemInfo") {
          model_element("DIM::Medical::ASN1::CmplxElemInfo")
        }
        code("Data Type: StoSampleAlg") {
          model_element("DIM::Medical::ASN1::StoSampleAlg")
        }
        code("Data Type: CmplxElemInfoList") {
          model_element("DIM::Medical::ASN1::CmplxElemInfoList")
        }
        code("Data Type: StorageFormat") {
          model_element("DIM::Medical::ASN1::StorageFormat")
        }
        code("Data Type: CmplxMetricInfo") {
          model_element("DIM::Medical::ASN1::CmplxMetricInfo")
        }
        code("Data Type: VMDStatus") {
          model_element("DIM::Medical::ASN1::VMDStatus")
        }
        code("Data Type: CmplxObsElem") {
          model_element("DIM::Medical::ASN1::CmplxObsElem")
        }
        code("Data Type: CmplxObsElemList") {
          model_element("DIM::Medical::ASN1::CmplxObsElemList")
        }
        code("Data Type: CmplxObsValue") {
          model_element("DIM::Medical::ASN1::CmplxObsValue")
        }
        code("Data Type: CmplxStaticAttr") {
          model_element("DIM::Medical::ASN1::CmplxStaticAttr")
        }
        code("Data Type: CmplxStaticAttrElem") {
          model_element("DIM::Medical::ASN1::CmplxStaticAttrElem")
        }
        code("Data Type: CmplxStaticAttrElemList") {
          model_element("DIM::Medical::ASN1::CmplxStaticAttrElemList")
        }
        code("Data Type: DispResolution") {
          model_element("DIM::Medical::ASN1::DispResolution")
        }
        code("Data Type: DsaRangeSpec") {
          model_element("DIM::Medical::ASN1::DsaRangeSpec")
        }
        code("Data Type: EnumMsmtRange") {
          model_element("DIM::Medical::ASN1::EnumMsmtRange")
        }
        code("Data Type: EnumMsmtRangeLabel") {
          model_element("DIM::Medical::ASN1::EnumMsmtRangeLabel")
        }
        code("Data Type: EnumMsmtRangeLabels") {
          model_element("DIM::Medical::ASN1::EnumMsmtRangeLabels")
        }
        code("Data Type: EnumObsValue") {
          model_element("DIM::Medical::ASN1::EnumObsValue")
        }
        code("Data Type: EnumObsValueCmp") {
          model_element("DIM::Medical::ASN1::EnumObsValueCmp")
        }
        code("Data Type: EnumRecordMetric") {
          model_element("DIM::Medical::ASN1::EnumRecordMetric")
        }
        code("Data Type: EnumRecordOo") {
          model_element("DIM::Medical::ASN1::EnumRecordOo")
        }
        code("Data Type: EnumVal") {
          model_element("DIM::Medical::ASN1::EnumVal")
        }
        code("Data Type: MarkerEntryIndex") {
          model_element("DIM::Medical::ASN1::MarkerEntryIndex")
        }
        code("Data Type: MarkerEntryRelTim") {
          model_element("DIM::Medical::ASN1::MarkerEntryRelTim")
        }
        code("Data Type: MarkerEntrySaVal16") {
          model_element("DIM::Medical::ASN1::MarkerEntrySaVal16")
        }
        code("Data Type: MarkerEntrySaVal32") {
          model_element("DIM::Medical::ASN1::MarkerEntrySaVal32")
        }
        code("Data Type: MarkerEntrySaVal8") {
          model_element("DIM::Medical::ASN1::MarkerEntrySaVal8")
        }
        code("Data Type: MarkerListIndex") {
          model_element("DIM::Medical::ASN1::MarkerListIndex")
        }
        code("Data Type: MarkerListRelTim") {
          model_element("DIM::Medical::ASN1::MarkerListRelTim")
        }
        code("Data Type: MarkerListSaVal16") {
          model_element("DIM::Medical::ASN1::MarkerListSaVal16")
        }
        code("Data Type: MarkerListSaVal32") {
          model_element("DIM::Medical::ASN1::MarkerListSaVal32")
        }
        code("Data Type: MarkerListSaVal8") {
          model_element("DIM::Medical::ASN1::MarkerListSaVal8")
        }
        code("Data Type: MetricCalEntry") {
          model_element("DIM::Medical::ASN1::MetricCalEntry")
        }
        code("Data Type: MetricCalibration") {
          model_element("DIM::Medical::ASN1::MetricCalibration")
        }
        code("Data Type: MetricMeasure") {
          model_element("DIM::Medical::ASN1::MetricMeasure")
        }
        code("Data Type: MetricSourceList") {
          model_element("DIM::Medical::ASN1::MetricSourceList")
        }
        code("Data Type: MetricSpec") {
          model_element("DIM::Medical::ASN1::MetricSpec")
        }
        code("Data Type: MetricStructure") {
          model_element("DIM::Medical::ASN1::MetricStructure")
        }
        code("Data Type: NuObsValue") {
          model_element("DIM::Medical::ASN1::NuObsValue")
        }
        code("Data Type: NuObsValueCmp") {
          model_element("DIM::Medical::ASN1::NuObsValueCmp")
        }
        code("Data Type: SaCalData16") {
          model_element("DIM::Medical::ASN1::SaCalData16")
        }
        code("Data Type: SaCalData32") {
          model_element("DIM::Medical::ASN1::SaCalData32")
        }
        code("Data Type: SaCalData8") {
          model_element("DIM::Medical::ASN1::SaCalData8")
        }
        code("Data Type: SaFilterEntry") {
          model_element("DIM::Medical::ASN1::SaFilterEntry")
        }
        code("Data Type: SaFilterSpec") {
          model_element("DIM::Medical::ASN1::SaFilterSpec")
        }
        code("Data Type: SaGridEntry16") {
          model_element("DIM::Medical::ASN1::SaGridEntry16")
        }
        code("Data Type: SaGridEntry32") {
          model_element("DIM::Medical::ASN1::SaGridEntry32")
        }
        code("Data Type: SaGridEntry8") {
          model_element("DIM::Medical::ASN1::SaGridEntry8")
        }
        code("Data Type: SaObsValue") {
          model_element("DIM::Medical::ASN1::SaObsValue")
        }
        code("Data Type: SaObsValueCmp") {
          model_element("DIM::Medical::ASN1::SaObsValueCmp")
        }
        code("Data Type: SaSignalFrequency") {
          model_element("DIM::Medical::ASN1::SaSignalFrequency")
        }
        code("Data Type: SaSpec") {
          model_element("DIM::Medical::ASN1::SaSpec")
        }
        code("Data Type: SaVisualGrid16") {
          model_element("DIM::Medical::ASN1::SaVisualGrid16")
        }
        code("Data Type: SaVisualGrid32") {
          model_element("DIM::Medical::ASN1::SaVisualGrid32")
        }
        code("Data Type: SaVisualGrid8") {
          model_element("DIM::Medical::ASN1::SaVisualGrid8")
        }
        code("Data Type: SampleType") {
          model_element("DIM::Medical::ASN1::SampleType")
        }
        code("Data Type: ScaleRangeSpec16") {
          model_element("DIM::Medical::ASN1::ScaleRangeSpec16")
        }
        code("Data Type: ScaleRangeSpec32") {
          model_element("DIM::Medical::ASN1::ScaleRangeSpec32")
        }
        code("Data Type: ScaleRangeSpec8") {
          model_element("DIM::Medical::ASN1::ScaleRangeSpec8")
        }
        code("Data Type: ScaledRange16") {
          model_element("DIM::Medical::ASN1::ScaledRange16")
        }
        code("Data Type: ScaledRange32") {
          model_element("DIM::Medical::ASN1::ScaledRange32")
        }
        code("Data Type: ScaledRange8") {
          model_element("DIM::Medical::ASN1::ScaledRange8")
        }
        code("Data Type: SegDataGen") {
          model_element("DIM::Medical::ASN1::SegDataGen")
        }
        code("Data Type: SegDataNuOpt") {
          model_element("DIM::Medical::ASN1::SegDataNuOpt")
        }
        code("Data Type: SegmIdList") {
          model_element("DIM::Medical::ASN1::SegmIdList")
        }
        code("Data Type: SegmSelection") {
          model_element("DIM::Medical::ASN1::SegmSelection")
        }
        code("Data Type: SegmentAttr") {
          model_element("DIM::Medical::ASN1::SegmentAttr")
        }
        code("Data Type: SegmentAttrList") {
          model_element("DIM::Medical::ASN1::SegmentAttrList")
        }
        code("Data Type: SegmentInfo") {
          model_element("DIM::Medical::ASN1::SegmentInfo")
        }
        code("Data Type: SegmentInfoList") {
          model_element("DIM::Medical::ASN1::SegmentInfoList")
        }
        code("Data Type: SiteList") {
          model_element("DIM::Medical::ASN1::SiteList")
        }
        code("Data Type: SiteListExt") {
          model_element("DIM::Medical::ASN1::SiteListExt")
        }
        code("Data Type: VmoSourceEntry") {
          model_element("DIM::Medical::ASN1::VmoSourceEntry")
        }
        code("Data Type: VmoSourceList") {
          model_element("DIM::Medical::ASN1::VmoSourceList")
        }
        code("Data Type: SegDataRtsaOpt") {
          model_element("DIM::Medical::ASN1::SegDataRtsaOpt")
        }
      }
    }
    clause("Package Alert") {
      clause("Interface AnyAlert") {
        model_element("DIM::Alert::AnyAlert")
      }
      clause("Interface AlertOrAlertStatus") {
        model_element("DIM::Alert::AlertOrAlertStatus")
      }
      clause("Class Alert") {
        model_element("DIM::Alert::Alert")
      }
      clause("Class AlertStatus") {
        model_element("DIM::Alert::AlertStatus")
      }
      clause("Class AlertMonitor") {
        model_element("DIM::Alert::AlertMonitor")
      }
      clause("ASN1 Data Types for the Alert Package") {
        code("Data Type: AlarmRepFlags_ANON_ENUM") {
          model_element("DIM::Alert::ASN1::AlarmRepFlags_ANON_ENUM")
        }
        code("Data Type: AlertControls") {
          model_element("DIM::Alert::ASN1::AlertControls")
        }
        code("Data Type: AlertFlags") {
          model_element("DIM::Alert::ASN1::AlertFlags")
        }
        code("Data Type: AlertState") {
          model_element("DIM::Alert::ASN1::AlertState")
        }
        code("Data Type: AlertType") {
          model_element("DIM::Alert::ASN1::AlertType")
        }
        code("Data Type: AlStatChgCnt") {
          model_element("DIM::Alert::ASN1::AlStatChgCnt")
        }
        code("Data Type: AlertCapabEntry") {
          model_element("DIM::Alert::ASN1::AlertCapabEntry")
        }
        code("Data Type: AlertCapabList") {
          model_element("DIM::Alert::ASN1::AlertCapabList")
        }
        code("Data Type: AlertCondition") {
          model_element("DIM::Alert::ASN1::AlertCondition")
        }
        code("Data Type: AlertEntry") {
          model_element("DIM::Alert::ASN1::AlertEntry")
        }
        code("Data Type: AlertList") {
          model_element("DIM::Alert::ASN1::AlertList")
        }
        code("Data Type: DevAlarmEntry") {
          model_element("DIM::Alert::ASN1::DevAlarmEntry")
        }
        code("Data Type: DevAlarmList") {
          model_element("DIM::Alert::ASN1::DevAlarmList")
        }
        code("Data Type: DevAlertCondition") {
          model_element("DIM::Alert::ASN1::DevAlertCondition")
        }
        code("Data Type: LimitSpecEntry") {
          model_element("DIM::Alert::ASN1::LimitSpecEntry")
        }
        code("Data Type: LimitSpecList") {
          model_element("DIM::Alert::ASN1::LimitSpecList")
        }
      }
    }
    clause("Package System") {
      clause("Class VMS") {
        model_element("DIM::System::VMS")
      }
      clause("Class MDS") {
        model_element("DIM::System::MDS")
      }
      clause("Class SingleBedMDS") {
        model_element("DIM::System::SingleBedMDS")
      }
      clause("Class MultipleBedMDS") {
        model_element("DIM::System::MultipleBedMDS")
      }
      clause("Class Log") {
        model_element("DIM::System::Log")
      }
      clause("Class EventLog") {
        model_element("DIM::System::EventLog")
      }
      clause("Class Battery") {
        model_element("DIM::System::Battery")
      }
      clause("Class Clock") {
        model_element("DIM::System::Clock")
      }
      clause("ASN1 Data Types for the System Package") {
        code("Data Type: ApplicationArea") {
          model_element("DIM::System::ASN1::ApplicationArea")
        }
        code("Data Type: BatteryStatus") {
          model_element("DIM::System::ASN1::BatteryStatus")
        }
        code("Data Type: ClearLogOptions") {
          model_element("DIM::System::ASN1::ClearLogOptions")
        }
        code("Data Type: ClearLogResult") {
          model_element("DIM::System::ASN1::ClearLogResult")
        }
        code("Data Type: DateTimeUsage") {
          model_element("DIM::System::ASN1::DateTimeUsage")
        }
        code("Data Type: EventLogInfo") {
          model_element("DIM::System::ASN1::EventLogInfo")
        }
        code("Data Type: LineFrequency") {
          model_element("DIM::System::ASN1::LineFrequency")
        }
        code("Data Type: MDSStatus") {
          model_element("DIM::System::ASN1::MDSStatus")
        }
        code("Data Type: MdsSetStateResult") {
          model_element("DIM::System::ASN1::MdsSetStateResult")
        }
        code("Data Type: NomenclatureVersionNomMajorVersion_ANON_ENUM") {
          model_element("DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM")
        }
        code("Data Type: PowerStatus") {
          model_element("DIM::System::ASN1::PowerStatus")
        }
        code("Data Type: ProdSpecEntrySpecType_ANON_ENUM") {
          model_element("DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM")
        }
        code("Data Type: SystemCapability") {
          model_element("DIM::System::ASN1::SystemCapability")
        }
        code("Data Type: TimeCapability") {
          model_element("DIM::System::ASN1::TimeCapability")
        }
        code("Data Type: AbsoluteRelativeTimeSync") {
          model_element("DIM::System::ASN1::AbsoluteRelativeTimeSync")
        }
        code("Data Type: BatMeasure") {
          model_element("DIM::System::ASN1::BatMeasure")
        }
        code("Data Type: ClearLogRangeInvoke") {
          model_element("DIM::System::ASN1::ClearLogRangeInvoke")
        }
        code("Data Type: ClearLogRangeResult") {
          model_element("DIM::System::ASN1::ClearLogRangeResult")
        }
        code("Data Type: ClockStatusUpdateInfo") {
          model_element("DIM::System::ASN1::ClockStatusUpdateInfo")
        }
        code("Data Type: DateTimeStatus") {
          model_element("DIM::System::ASN1::DateTimeStatus")
        }
        code("Data Type: DaylightSavingsTransition") {
          model_element("DIM::System::ASN1::DaylightSavingsTransition")
        }
        code("Data Type: EventLogEntry") {
          model_element("DIM::System::ASN1::EventLogEntry")
        }
        code("Data Type: EventLogEntryList") {
          model_element("DIM::System::ASN1::EventLogEntryList")
        }
        code("Data Type: ExtTimeStamp") {
          model_element("DIM::System::ASN1::ExtTimeStamp")
        }
        code("Data Type: ExtTimeStampList") {
          model_element("DIM::System::ASN1::ExtTimeStampList")
        }
        code("Data Type: GetEventLogEntryInvoke") {
          model_element("DIM::System::ASN1::GetEventLogEntryInvoke")
        }
        code("Data Type: GetEventLogEntryResult") {
          model_element("DIM::System::ASN1::GetEventLogEntryResult")
        }
        code("Data Type: LeapSecondsTransition") {
          model_element("DIM::System::ASN1::LeapSecondsTransition")
        }
        code("Data Type: MdsAttributeChangeInfo") {
          model_element("DIM::System::ASN1::MdsAttributeChangeInfo")
        }
        code("Data Type: MdsCreateInfo") {
          model_element("DIM::System::ASN1::MdsCreateInfo")
        }
        code("Data Type: MdsErrorInfo") {
          model_element("DIM::System::ASN1::MdsErrorInfo")
        }
        code("Data Type: MdsSetStateInvoke") {
          model_element("DIM::System::ASN1::MdsSetStateInvoke")
        }
        code("Data Type: NomenclatureVersion") {
          model_element("DIM::System::ASN1::NomenclatureVersion")
        }
        code("Data Type: ProdSpecEntry") {
          model_element("DIM::System::ASN1::ProdSpecEntry")
        }
        code("Data Type: ProductionSpec") {
          model_element("DIM::System::ASN1::ProductionSpec")
        }
        code("Data Type: SNTPTimeStamp") {
          model_element("DIM::System::ASN1::SNTPTimeStamp")
        }
        code("Data Type: SetLeapSecondsInvoke") {
          model_element("DIM::System::ASN1::SetLeapSecondsInvoke")
        }
        code("Data Type: SetTimeInvoke") {
          model_element("DIM::System::ASN1::SetTimeInvoke")
        }
        code("Data Type: SetTimeZoneInvoke") {
          model_element("DIM::System::ASN1::SetTimeZoneInvoke")
        }
        code("Data Type: SystemModel") {
          model_element("DIM::System::ASN1::SystemModel")
        }
        code("Data Type: SystemSpec") {
          model_element("DIM::System::ASN1::SystemSpec")
        }
        code("Data Type: SystemSpecEntry") {
          model_element("DIM::System::ASN1::SystemSpecEntry")
        }
        code("Data Type: TimeProtocolIdList") {
          model_element("DIM::System::ASN1::TimeProtocolIdList")
        }
        code("Data Type: TimeSupport") {
          model_element("DIM::System::ASN1::TimeSupport")
        }
        code("Data Type: UTCTimeZone") {
          model_element("DIM::System::ASN1::UTCTimeZone")
        }
        code("Data Type: AbsoluteTimeISO") {
          model_element("DIM::System::ASN1::AbsoluteTimeISO")
        }
        code("Data Type: TimeProtocolId") {
          model_element("DIM::System::ASN1::TimeProtocolId")
        }
        code("Data Type: TimeStampId") {
          model_element("DIM::System::ASN1::TimeStampId")
        }
      }
    }
    clause("Package Control") {
      clause("Class SCO") {
        model_element("DIM::Control::SCO")
      }
      clause("Class Operation") {
        model_element("DIM::Control::Operation")
      }
      clause("Class SelectItemOperation") {
        model_element("DIM::Control::SelectItemOperation")
      }
      clause("Class SetValueOperation") {
        model_element("DIM::Control::SetValueOperation")
      }
      clause("Class SetStringOperation") {
        model_element("DIM::Control::SetStringOperation")
      }
      clause("Class ToggleFlagOperation") {
        model_element("DIM::Control::ToggleFlagOperation")
      }
      clause("Class ActivateOperation") {
        model_element("DIM::Control::ActivateOperation")
      }
      clause("Class LimitAlertOperation") {
        model_element("DIM::Control::LimitAlertOperation")
      }
      clause("Class SetRangeOperation") {
        model_element("DIM::Control::SetRangeOperation")
      }
      clause("ASN1 Data Types for the Control Package") {
        code("Data Type: AlOpCapab") {
          model_element("DIM::Control::ASN1::AlOpCapab")
        }
        code("Data Type: CurLimAlStat") {
          model_element("DIM::Control::ASN1::CurLimAlStat")
        }
        code("Data Type: OpInvResult") {
          model_element("DIM::Control::ASN1::OpInvResult")
        }
        code("Data Type: OpLevel") {
          model_element("DIM::Control::ASN1::OpLevel")
        }
        code("Data Type: OpModType") {
          model_element("DIM::Control::ASN1::OpModType")
        }
        code("Data Type: OpOptions") {
          model_element("DIM::Control::ASN1::OpOptions")
        }
        code("Data Type: ScoActivityIndicator") {
          model_element("DIM::Control::ASN1::ScoActivityIndicator")
        }
        code("Data Type: ScoCapability") {
          model_element("DIM::Control::ASN1::ScoCapability")
        }
        code("Data Type: ScoOperInvokeError_op_error_ANON_ENUM") {
          model_element("DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM")
        }
        code("Data Type: SetStrOpt") {
          model_element("DIM::Control::ASN1::SetStrOpt")
        }
        code("Data Type: ToggleState") {
          model_element("DIM::Control::ASN1::ToggleState")
        }
        code("Data Type: AlertOpTextString") {
          model_element("DIM::Control::ASN1::AlertOpTextString")
        }
        code("Data Type: CtxtHelp") {
          model_element("DIM::Control::ASN1::CtxtHelp")
        }
        code("Data Type: CtxtHelpRequest") {
          model_element("DIM::Control::ASN1::CtxtHelpRequest")
        }
        code("Data Type: CtxtHelpResult") {
          model_element("DIM::Control::ASN1::CtxtHelpResult")
        }
        code("Data Type: CurLimAlVal") {
          model_element("DIM::Control::ASN1::CurLimAlVal")
        }
        code("Data Type: CurrentRange") {
          model_element("DIM::Control::ASN1::CurrentRange")
        }
        code("Data Type: FLOAT-TypeList") {
          model_element("DIM::Control::ASN1::FLOAT-TypeList")
        }
        code("Data Type: InstNumberList") {
          model_element("DIM::Control::ASN1::InstNumberList")
        }
        code("Data Type: OCTET_STRING_List") {
          model_element("DIM::Control::ASN1::OCTET_STRING_List")
        }
        code("Data Type: OID-TypeList") {
          model_element("DIM::Control::ASN1::OID-TypeList")
        }
        code("Data Type: OpGrouping") {
          model_element("DIM::Control::ASN1::OpGrouping")
        }
        code("Data Type: OpInvokeElement") {
          model_element("DIM::Control::ASN1::OpInvokeElement")
        }
        code("Data Type: OpInvokeList") {
          model_element("DIM::Control::ASN1::OpInvokeList")
        }
        code("Data Type: OpSetValueRange") {
          model_element("DIM::Control::ASN1::OpSetValueRange")
        }
        code("Data Type: OpValStepWidth") {
          model_element("DIM::Control::ASN1::OpValStepWidth")
        }
        code("Data Type: OperSpec") {
          model_element("DIM::Control::ASN1::OperSpec")
        }
        code("Data Type: OperTextStrings") {
          model_element("DIM::Control::ASN1::OperTextStrings")
        }
        code("Data Type: OperationInvoke") {
          model_element("DIM::Control::ASN1::OperationInvoke")
        }
        code("Data Type: OperationInvokeResult") {
          model_element("DIM::Control::ASN1::OperationInvokeResult")
        }
        code("Data Type: RangeOpText") {
          model_element("DIM::Control::ASN1::RangeOpText")
        }
        code("Data Type: ScoOperInvokeError") {
          model_element("DIM::Control::ASN1::ScoOperInvokeError")
        }
        code("Data Type: ScoOperReqSpec") {
          model_element("DIM::Control::ASN1::ScoOperReqSpec")
        }
        code("Data Type: SelectList") {
          model_element("DIM::Control::ASN1::SelectList")
        }
        code("Data Type: SelectUValueEntry") {
          model_element("DIM::Control::ASN1::SelectUValueEntry")
        }
        code("Data Type: SelectUValueEntryList") {
          model_element("DIM::Control::ASN1::SelectUValueEntryList")
        }
        code("Data Type: SetStringSpec") {
          model_element("DIM::Control::ASN1::SetStringSpec")
        }
        code("Data Type: StepWidthEntry") {
          model_element("DIM::Control::ASN1::StepWidthEntry")
        }
        code("Data Type: ToggleLabelStrings") {
          model_element("DIM::Control::ASN1::ToggleLabelStrings")
        }
      }
    }
    clause("Package ExtendedServices") {
      clause("Class Scanner") {
        model_element("DIM::ExtendedServices::Scanner")
      }
      clause("Class CfgScanner") {
        model_element("DIM::ExtendedServices::CfgScanner")
      }
      clause("Class EpiCfgScanner") {
        model_element("DIM::ExtendedServices::EpiCfgScanner")
      }
      clause("Class PeriCfgScanner") {
        model_element("DIM::ExtendedServices::PeriCfgScanner")
      }
      clause("Class FastPeriCfgScanner") {
        model_element("DIM::ExtendedServices::FastPeriCfgScanner")
      }
      clause("Class UcfgScanner") {
        model_element("DIM::ExtendedServices::UcfgScanner")
      }
      clause("Class ContextScanner") {
        model_element("DIM::ExtendedServices::ContextScanner")
      }
      clause("Class AlertScanner") {
        model_element("DIM::ExtendedServices::AlertScanner")
      }
      clause("Class OperatingScanner") {
        model_element("DIM::ExtendedServices::OperatingScanner")
      }
      clause("ASN1 Data Types for the Extended Services Package") {
        code("Data Type: ConfirmMode") {
          model_element("DIM::ExtendedServices::ASN1::ConfirmMode")
        }
        code("Data Type: ContextMode") {
          model_element("DIM::ExtendedServices::ASN1::ContextMode")
        }
        code("Data Type: ScanConfigLimit") {
          model_element("DIM::ExtendedServices::ASN1::ScanConfigLimit")
        }
        code("Data Type: ScanExtend") {
          model_element("DIM::ExtendedServices::ASN1::ScanExtend")
        }
        code("Data Type: CreateEntry") {
          model_element("DIM::ExtendedServices::ASN1::CreateEntry")
        }
        code("Data Type: CreateEntryList") {
          model_element("DIM::ExtendedServices::ASN1::CreateEntryList")
        }
        code("Data Type: CreatedObject") {
          model_element("DIM::ExtendedServices::ASN1::CreatedObject")
        }
        code("Data Type: CreatedObjectList") {
          model_element("DIM::ExtendedServices::ASN1::CreatedObjectList")
        }
        code("Data Type: FastScanReportInfo") {
          model_element("DIM::ExtendedServices::ASN1::FastScanReportInfo")
        }
        code("Data Type: ManagedObjectIdList") {
          model_element("DIM::ExtendedServices::ASN1::ManagedObjectIdList")
        }
        code("Data Type: ObjCreateInfo") {
          model_element("DIM::ExtendedServices::ASN1::ObjCreateInfo")
        }
        code("Data Type: ObjDeleteInfo") {
          model_element("DIM::ExtendedServices::ASN1::ObjDeleteInfo")
        }
        code("Data Type: ObservationScan") {
          model_element("DIM::ExtendedServices::ASN1::ObservationScan")
        }
        code("Data Type: ObservationScanList") {
          model_element("DIM::ExtendedServices::ASN1::ObservationScanList")
        }
        code("Data Type: OpAttributeInfo") {
          model_element("DIM::ExtendedServices::ASN1::OpAttributeInfo")
        }
        code("Data Type: OpAttributeScan") {
          model_element("DIM::ExtendedServices::ASN1::OpAttributeScan")
        }
        code("Data Type: OpAttributeScanList") {
          model_element("DIM::ExtendedServices::ASN1::OpAttributeScanList")
        }
        code("Data Type: OpCreateEntry") {
          model_element("DIM::ExtendedServices::ASN1::OpCreateEntry")
        }
        code("Data Type: OpCreateEntryList") {
          model_element("DIM::ExtendedServices::ASN1::OpCreateEntryList")
        }
        code("Data Type: OpCreateInfo") {
          model_element("DIM::ExtendedServices::ASN1::OpCreateInfo")
        }
        code("Data Type: OpDeleteEntry") {
          model_element("DIM::ExtendedServices::ASN1::OpDeleteEntry")
        }
        code("Data Type: OpDeleteEntryList") {
          model_element("DIM::ExtendedServices::ASN1::OpDeleteEntryList")
        }
        code("Data Type: OpDeleteInfo") {
          model_element("DIM::ExtendedServices::ASN1::OpDeleteInfo")
        }
        code("Data Type: OpElem") {
          model_element("DIM::ExtendedServices::ASN1::OpElem")
        }
        code("Data Type: OpElemAttr") {
          model_element("DIM::ExtendedServices::ASN1::OpElemAttr")
        }
        code("Data Type: OpElemAttrList") {
          model_element("DIM::ExtendedServices::ASN1::OpElemAttrList")
        }
        code("Data Type: OpElemList") {
          model_element("DIM::ExtendedServices::ASN1::OpElemList")
        }
        code("Data Type: RefreshObjEntry") {
          model_element("DIM::ExtendedServices::ASN1::RefreshObjEntry")
        }
        code("Data Type: RefreshObjList") {
          model_element("DIM::ExtendedServices::ASN1::RefreshObjList")
        }
        code("Data Type: RtsaObservationScan") {
          model_element("DIM::ExtendedServices::ASN1::RtsaObservationScan")
        }
        code("Data Type: RtsaObservationScanList") {
          model_element("DIM::ExtendedServices::ASN1::RtsaObservationScanList")
        }
        code("Data Type: ScanEntry") {
          model_element("DIM::ExtendedServices::ASN1::ScanEntry")
        }
        code("Data Type: ScanList") {
          model_element("DIM::ExtendedServices::ASN1::ScanList")
        }
        code("Data Type: ScanReportInfo") {
          model_element("DIM::ExtendedServices::ASN1::ScanReportInfo")
        }
        code("Data Type: SingleCtxtFastScan") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtFastScan")
        }
        code("Data Type: SingleCtxtFastScanList") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtFastScanList")
        }
        code("Data Type: SingleCtxtOperScan") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtOperScan")
        }
        code("Data Type: SingleCtxtOperScanList") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtOperScanList")
        }
        code("Data Type: SingleCtxtScan") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtScan")
        }
        code("Data Type: SingleCtxtScanList") {
          model_element("DIM::ExtendedServices::ASN1::SingleCtxtScanList")
        }
      }
    }
    clause("Package Communication") {
      clause("Class CommunicationController") {
        model_element("DIM::Communication::CommunicationController")
      }
      clause("Class DCC") {
        model_element("DIM::Communication::DCC")
      }
      clause("Class BCC") {
        model_element("DIM::Communication::BCC")
      }
      clause("Class DeviceInterface") {
        model_element("DIM::Communication::DeviceInterface")
      }
      clause("Class MibElement") {
        model_element("DIM::Communication::MibElement")
      }
      clause("Class DeviceInterfaceMibElement") {
        model_element("DIM::Communication::DeviceInterfaceMibElement")
      }
      clause("Class GeneralCommunicationStatisticsMibElement") {
        model_element("DIM::Communication::GeneralCommunicationStatisticsMibElement")
      }
      clause("ASN1 Data Types for the Communication Package") {
        code("Data Type: CcCapability") {
          model_element("DIM::Communication::ASN1::CcCapability")
        }
        code("Data Type: CcExtMgmtProto") {
          model_element("DIM::Communication::ASN1::CcExtMgmtProto")
        }
        code("Data Type: DifMibPortState") {
          model_element("DIM::Communication::ASN1::DifMibPortState")
        }
        code("Data Type: MibCcCommMode") {
          model_element("DIM::Communication::ASN1::MibCcCommMode")
        }
        code("Data Type: GetMibDataRequest") {
          model_element("DIM::Communication::ASN1::GetMibDataRequest")
        }
        code("Data Type: GetMibDataResult") {
          model_element("DIM::Communication::ASN1::GetMibDataResult")
        }
        code("Data Type: MibDataEntry") {
          model_element("DIM::Communication::ASN1::MibDataEntry")
        }
        code("Data Type: MibDataList") {
          model_element("DIM::Communication::ASN1::MibDataList")
        }
        code("Data Type: MibElementList") {
          model_element("DIM::Communication::ASN1::MibElementList")
        }
        code("Data Type: MibIdList") {
          model_element("DIM::Communication::ASN1::MibIdList")
        }
        code("Data Type: SupportedProfileList") {
          model_element("DIM::Communication::ASN1::SupportedProfileList")
        }
        code("Data Type: CC-Oid") {
          model_element("DIM::Communication::ASN1::CC-Oid")
        }
        code("Data Type: MibCcCounter") {
          model_element("DIM::Communication::ASN1::MibCcCounter")
        }
        code("Data Type: MibCcGauge") {
          model_element("DIM::Communication::ASN1::MibCcGauge")
        }
      }
    }
    clause("Package Archival") {
      clause("Class MultiPatientArchive") {
        model_element("DIM::Archival::MultiPatientArchive")
      }
      clause("Class PatientArchive") {
        model_element("DIM::Archival::PatientArchive")
      }
      clause("Class SessionArchive") {
        model_element("DIM::Archival::SessionArchive")
      }
      clause("Class Physician") {
        model_element("DIM::Archival::Physician")
      }
      clause("Class SessionTest") {
        model_element("DIM::Archival::SessionTest")
      }
      clause("Class SessionNotes") {
        model_element("DIM::Archival::SessionNotes")
      }
      clause("ASN1 Data Types for the Archival Package") {
        code("Data Type: ArchiveProtection") {
          model_element("DIM::Archival::ASN1::ArchiveProtection")
        }
        code("Data Type: Authorization") {
          model_element("DIM::Archival::ASN1::Authorization")
        }
        code("Data Type: ExtNomenRefList") {
          model_element("DIM::Archival::ASN1::ExtNomenRefList")
        }
      }
    }
    clause("Package Patient") {
      clause("Class PatientDemographics") {
        model_element("DIM::Patient::PatientDemographics")
      }
      clause("ASN1 Data Types for the Patient Package") {
        code("Data Type: PatDemoState") {
          model_element("DIM::Patient::ASN1::PatDemoState")
        }
        code("Data Type: PatientRace") {
          model_element("DIM::Patient::ASN1::PatientRace")
        }
        code("Data Type: PatientSex") {
          model_element("DIM::Patient::ASN1::PatientSex")
        }
        code("Data Type: PatientType") {
          model_element("DIM::Patient::ASN1::PatientType")
        }
        code("Data Type: AdmitPatInfo") {
          model_element("DIM::Patient::ASN1::AdmitPatInfo")
        }
        code("Data Type: PatMeasure") {
          model_element("DIM::Patient::ASN1::PatMeasure")
        }
      }
    }
  }
  clause("Service model for communicating systems") {
    source_file("original_service_model.xml")  }
  clause("MDIB nomenclature") {
    source_file("original_mdib.xml")  }
  clause("Conformance model") {
    source_file("original_conformance_model.xml")  }

}
