module IEEEDSL
  class OneOffDSL
    attr_accessor :stack
    def self.load(filename)
        dsl = new
        dsl.instance_eval(File.read(filename), filename)
        dsl
    end
    
    def initialize
      @stack = []
    end
    
    def process(val, klass, getter, parent_getter, &block)
      ChangeTracker.start
      obj = klass.new
      obj.send(getter, val) if getter && val
      obj.save
      stack.last.send(parent_getter, obj) if stack.any?
      ChangeTracker.commit
      if block_given?
        stack << obj
        instance_eval(&block)
        stack.pop
      end
      # puts obj.inspect;puts
    end
    def clause(str, &block)
      process(str, Standard::Clause, :title=, :subclauses_add, &block)
    end
    
    def paragraph(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(val, Standard::Text, :content=, :content_add, &block)
    end
    
    def example(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(val, Standard::Example, :content=, :content_add, &block)
    end
    
    def figure(str, &block)
      # TODO - fix this when model is updated for figure to have caption
      val = nil #Gui_Builder_Profile::RichText.create(:content => str)
      process(val, Standard::Figure, :caption=, :content_add, &block)
    end
    
    def note(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(val, Standard::Note, :content=, :content_add, &block)
    end

    def footnote(str, &block)
      val = Gui_Builder_Profile::RichText.create(:content => str)
      process(val, Standard::Footnote, :content=, :footnotes_add, &block)
    end
    
    # TODO -- make this right when the model is updated
    def ordered_list(&block)
      if block_given?
        instance_eval(&block)
      end
    end
    
    # TODO -- make this right when the model is updated    
    def unordered_list(&block)
      if block_given?
        instance_eval(&block)
      end
    end
    
    # TODO -- make this right when the model is updated
    def unordered_list_item(str, &block)
      str = 'ULI -- ' + str
      paragraph(str, &block)
      # puts 'unordered_list_item'; puts str; puts
      # val = Gui_Builder_Profile::RichText.create(:content => str)
      # process(val, Standard::Footnote, :content=, :footnotes_add, &block)
    end
    
    # TODO -- make this right when the model is updated    
    def ordered_list_item1(str, &block)
      str = 'LI -- ' + str
      paragraph(str, &block)
      # puts 'ordered_list_item1'; puts str; puts
      # val = Gui_Builder_Profile::RichText.create(:content => str)
      # process(val, Standard::Footnote, :content=, :footnotes_add, &block)
    end
  end
    
  #   def clause(str, &block)
  #     ChangeTracker.start
  #     c = Standard::Clause.create(:title => str)
  #     c.save
  #     stack.last.subclauses_add(c) if stack.any?
  #     ChangeTracker.commit
  #     stack << c
  #     instance_eval(&block)
  #     stack.pop
  #   end
  #
  #   def paragraph(str, &block)
  #     ChangeTracker.start
  #     t = Standard::Text.new
  #     t.content = Gui_Builder_Profile::RichText.create(:content => str)
  #     t.save
  #     stack.last.content_add(t)
  #     ChangeTracker.commit
  #     stack << t
  #     instance_eval(&block)
  #     stack.pop
  #   end
  #
  #   def example(str, &block)
  #     ChangeTracker.start
  #     e = Standard::Example.new
  #     e.content = Gui_Builder_Profile::RichText.create(:content => str)
  #     e.save
  #     stack.last.content_add(e)
  #     ChangeTracker.commit
  #     stack << e
  #     instance_eval(&block)
  #     stack.pop
  #   end
  #
  #   def figure(str, &block)
  #     ChangeTracker.start
  #     f = Standard::Figure.new
  #     # f.content = Gui_Builder_Profile::RichText.create(:content => str)
  #     f.save
  #     stack.last.content_add(f)
  #     ChangeTracker.commit
  #     stack << f
  #     instance_eval(&block)
  #     stack.pop
  #   end
  #
  #   def note(str, &block)
  #     ChangeTracker.start
  #     n = Standard::Note.new
  #     n.content = Gui_Builder_Profile::RichText.create(:content => str)
  #     n.save
  #     stack.last.content_add(n)
  #     ChangeTracker.commit
  #     stack << n
  #     instance_eval(&block)
  #     stack.pop
  #   end
  #
  #   def footnote(str, &block)
  #     ChangeTracker.start
  #     f = Standard::Footnote.new
  #     f.content = Gui_Builder_Profile::RichText.create(:content => str)
  #     f.save
  #     stack.last.content_add(f)
  #     ChangeTracker.commit
  #   end
  # end
end
