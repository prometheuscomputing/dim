clause(%Q{DIM Programmatic})  {
  clause(%Q{General}) {
    clause(%Q{Modeling concept}) {
      paragraph(%Q{The DIM is an object-oriented model that consists of objects, their attributes, and their methods, which are abstractions of real-world entities in the domain of (vital signs information communicating) medical devices.})
      paragraph(%Q{The information model and the service model for communicating systems defined and used in this standard are conceptually based on the International Organization for Standardization (ISO)/open systems interconnection (OSI) system management model. Objects defined in the information model are considered managed (here, medical) objects. For the most part, they are directly available to management (i.e., access) services provided by the common medical device information service element (CMDISE) as defined in this standard.})
      paragraph(%Q{For communicating systems, the set of object instances available on any medical device that complies with the definitions of this standard forms the medical data information base (MDIB). The MDIB is a structured collection of managed medical objects representing the vital signs information provided by a particular medical device. Attribute data types, hierarchies, and behavior of objects in the MDIB are defined in this standard.})
      paragraph(%Q{The majority of objects defined here represent generalized vital signs data and support information. Specialization of these objects is achieved by defining appropriate attributes. Object hierarchies and relations between objects are used to express device configuration and device capabilities.})
      example(%Q{A generalized object is defined to represent vital signs in the form of a real-time waveform. A set of object attributes is used to specify a particular waveform as an invasive arterial blood pressure. The position in the hierarchy of all objects defines the subsystem that derives the waveform.})
      paragraph(%Q{Figure 1 shows the relation between managed medical objects, MDIB, CMDISE, application processes, and communication systems.})
      figure(%Q{—MDIB in communicating systems})
      paragraph(%Q{In the case of communicating systems, managed medical objects are accessible only through services provided by CMDISE. The way that these objects are stored in the MDIB in any specific system and the way applications and the CMDISE access these objects are implementation issues and as such not normative.})
      paragraph(%Q{In the case of a vital signs archived data format that complies with the definitions in this standard, object instances are stored, together with their dynamic attribute value changes, over a certain time period on archival media. Attribute data types and hierarchies of objects in the archival data format are again defined in this standard.})
      paragraph(%Q{Figure 2shows the relationship between managed medical objects, the data archive, and archive access services.})
      paragraph(%Q{In the case of the archived data format, the way managed medical objects are stored on a medium is the subject of standardization. The access services are a local implementation issue and as such are not intended to be governed by this standard.})
      figure(%Q{—Managed medical objects in a vital signs archive})
    }
    clause(%Q{Scope of the DIM}) {
      clause(%Q{General}) {
        paragraph(%Q{Vital signs information objects that are defined in this standard encompass digitized biosignals that are derived by medical measurement devices used, for example, in anaesthesia, surgery, infusion therapy, intensive care, and obstetrical care.})
        paragraph(%Q{Biosignal data within the scope of this standard include direct and derived, quantitative and qualitative measurements, technical and medical alarms, and control settings. Patient information relevant for the interpretation of these signals is also defined in the DIM.})
      }
      clause(%Q{Communicating systems}) {
        paragraph(%Q{Communicating systems within the scope of this standard include physiological meters and analysers, especially systems providing real-time or continuous monitoring. Data processing capabilities are required for these systems.})
        paragraph(%Q{Information management objects that provide capabilities and concepts for cost-effective communication (specifically data summarization objects) and objects necessary to enable real-time communication are also within the scope of the information model in this standard.})
        paragraph(%Q{Interoperability issues, specifically lower communication layers, temporal synchronization between multiple devices, etc., are outside the scope of this standard.})
      }
      clause(%Q{Archived vital signs}) {
        paragraph(%Q{Context information objects that describe the data acquisition process and organize a vital signs archive are within the scope of this standard.})
      }
    }
    clause(%Q{Approach}) {
      paragraph(%Q{For the object-oriented modeling, the unified modeling language (UML) technique is used. The domain is first subdivided into different packages, and this subdivision permits the organization of the model into smaller packages. Each package is then defined in the form of object diagrams. Objects are briefly introduced, and their relations and hierarchies are defined in the object diagram.})
      paragraph(%Q{For the object definitions, a textual approach is followed. Attributes are defined in attribute definition tables. Attribute data types are defined using Abstract Syntax Notation One (ASN.1). Object behavior and notifications generated by objects are also defined in definition tables. These definitions directly relate to the service model specified in Clause 8.})
    }
    clause(%Q{Extension of the model}) {
      paragraph(%Q{It is expected that over time extensions of the model may be needed to account for new developments in the area of medical devices. Also, in special implementations, there may be a requirement to model data that are specific for a particular device or a particular application (and that are, therefore, not covered by the general model).})
      paragraph(%Q{In some cases, it may be possible to use the concept of external object relations. Most objects defined in this standard provide an attribute group (e.g., the Relationship Attribute Group) that can be used to supply information about related objects that are not defined in the DIM. Supplying such information can be done by specifying a relation to an external object and assigning attributes to this relation (see 7.1.2.20).})
      paragraph(%Q{In other cases, it may be necessary to define completely new objects or to add new attributes, new methods, or new events to already defined objects. These extensions are considered private or manufacturer-specific extensions. Dealing with these extensions is primarily a matter of an interoperability standard that is based on this standard on vital signs representation.})
      paragraph(%Q{In general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. The nomenclature code space (i.e., code values) leaves room for private extensions. As a general rule, an interoperability standard that is based on this DIM should be able to deal with private or manufacturer-specific extensions by ignoring objects, attributes, etc., with unknown identifiers (i.e., nomenclature codes).})
    }
  }
  clause(%Q{Package diagram–overview}) {
    paragraph(%Q{The package diagram organizes the problem domain into separate groups. It shows the major objects inside each package and defines the relationships between these packets.})
    paragraph(%Q{The package diagram depicted in Figure 3contains only a small subset of all objects defined in the DIM. Common base objects except the Top object are not shown in this diagram. Also, not all relations are shown between the different packages. Refer to the detailed package diagrams for more information.})
    paragraph(%Q{The numbers in the packages refer to the corresponding subclauses in this clause about models and in Clause 7 about the object definitions.})
    paragraph(%Q{The Top object is an abstract base class and at the same time the ultimate base class for all objects defined in the model. For editorial convenience, the modeling diagrams in this standard do not show this inheritance hierarchy.})
    paragraph(%Q{The more detailed models for these packages are contained in 6.3through 6.10.})
  }
  clause(%Q{Model for the Medical Package}) {
    paragraph(%Q{The Medical Package deals with the derivation and representation of biosignals and contextual information that is important for the interpretation of measurements.})
    paragraph(%Q{Figure 4 shows the object model of the Medical Package.})
    figure(%Q{—Packages of the DIM})
    figure(%Q{—Medical Package model})
    note(%Q{Instances of the Channel object and the PM-Store object shall be contained in exactly one superior object instance.  Refer to the Alert Package for information about alarm-related objects.})
    footnote('')
    paragraph(%Q{The Medical Package model contains the objects described in 6.3.1 through 6.3.13.})
    clause(%Q{VMO (i.e., virtual medical object)}) {
      paragraph(%Q{The VMO is the base class for all medical-related objects in the model. It provides consistent naming and identification across the Medical Package model.})
      paragraph(%Q{As a base class, the VMO cannot be instantiated.})
    }
    clause(%Q{VMD (i.e., virtual medical device) object}) {
      paragraph(%Q{The VMD object is an abstraction for a medical-related subsystem (e.g., hardware or even pure software) of a medical device. Characteristics of this subsystem (e.g., modes, versions) are captured in this object. At the same time, the VMD object is a container for objects representing measurement and status information.})
      example(%Q{A modular patient monitor provides measurement modalities in the form of plug-in modules. Each module is represented by a VMD object.})
    }
    clause(%Q{Channel object}) {
      paragraph(%Q{The Channel object is used for grouping Metric objects and, thus, allows hierarchical information organization. The Channel object is not mandatory for representation of Metric objects in a VMD.})
      example(%Q{A blood pressure VMD may define a Channel object to group together all metrics that deal with the blood pressure (e.g., pressure value, pressure waveform). A second Channel object can be used to group together metrics that deal with heart rate.})
    }
    clause(%Q{Metric object}) {
      paragraph(%Q{The Metric object is the base class for all objects representing direct and derived, quantitative and qualitative biosignal measurement, status, and context data.})
      paragraph(%Q{Specializations of the Metric object are provided to deal with common representations (e.g., single values, array data, status indications) and presentations (e.g., on a display) of measurement data.})
      paragraph(%Q{As a base class, the Metric object cannot be instantiated.})
    }
    clause(%Q{Numeric object}) {
      paragraph(%Q{The Numeric object represents numerical measurements and status information, e.g., amplitude measures, counters.})
      example(%Q{A heart rate measurement is represented by a Numeric object.})
      note(%Q{A compound Numeric object is defined as an efficient model, for example, for arterial blood pressure, which usually has three associated values (i.e., systolic, diastolic, mean). The availability of multiple values in a single Numeric (or other Metric) object can be indicated in a special structure attribute in the Metric object.})
    }
    clause(%Q{Sample Array object}) {
      paragraph(%Q{The Sample Array object is the base class for metrics that have a graphical, curve type presentation and, therefore, have their observation values reported as arrays of data points by communicating systems.})
      paragraph(%Q{As a base class, the Sample Array object cannot be instantiated.})
    }
    clause(%Q{Real Time Sample Array object}) {
      paragraph(%Q{The Real Time Sample Array object is a sample array that represents a real-time continuous waveform. As such, it has special requirements in communicating systems, e.g., processing power, low latency, high bandwidth. Therefore, it requires the definition of a specialized object.})
      example(%Q{An electrocardiogram (ECG) real-time wave is represented as a Real Time Sample Array object.})
    }
    clause(%Q{Time Sample Array object}) {
      paragraph(%Q{The Time Sample Array object is a sample array that represents noncontinuous waveforms (i.e., a wave snippet). Within a single observation (i.e., a single array of sample values), samples are equidistant in time.Example: Software for ST segment analysis may use the Time Sample Array object to represent snippets of ECG real-time waves that contain only a single QRS complex. Within this wave snippet, the software can locate the ST measurement points. It generates a new snippet, for example, every 15 seconds.})
    }
    clause(%Q{Distribution Sample Array object}) {
      paragraph(%Q{The Distribution Sample Array object is a sample array that represents linear value distributions in the form of arrays containing scaled sample values. The index of a value within an observation array denotes a spatial value, not a time point.})
      example(%Q{An electroencephalogram (EEG) application may use a Fourier transformation to derive a frequency distribution (i.e., a spectrum) from the EEG signal. It then uses the Distribution Sample Array object to represent that spectrum in the MDIB.})
    }
    clause(%Q{Enumeration object}) {
      paragraph(%Q{The Enumeration object represents status information and/or annotation information. Observation values may be presented in the form of normative codes (that are included in the nomenclature defined in this standard or in some other nomenclature scheme) or in the form of free text.})
      example(%Q{An ECG rhythm qualification may be represented as an Enumeration object. A ventilator may provide information about its current ventilation mode as an Enumeration object.})
    }
    clause(%Q{Complex Metric object}) {
      paragraph(%Q{In special cases, the Complex Metric object can be used to group a larger number of strongly related Metric objects in one single container object for performance or for modeling convenience. The Complex Metric object is a composition of Metric objects, possibly recursive.})
      example(%Q{A ventilator device may provide extensive breath analysis capabilities. For each breath, it calculates various numerical values (e.g., volumes, I:E ratio, timing information) as well as enumerated information (e.g., breath type classification, annotation data). For efficiency, all this information is grouped together in one Complex Metric object instance, which is updated upon each breath.})
    }
    clause(%Q{PM-Store (i.e., persistent metric) object}) {
      paragraph(%Q{The PM-Store object provides long-term storage capabilities for metric data. It contains a variable number of PM-Segment objects that can be accessed only through the PM-Store object. Without further specialization, the PM-Store object is intended to store data of a single Metric object only.})
      example(%Q{A device stores the numerical value of an invasive blood pressure on a disk. It uses the PM-Store object to represent this persistent information. Attributes of the PM-Store object describe the sampling period, the sampling algorithm, and the storage format. When the label of the pressure measurement is changed (e.g., during a wedge procedure), the storage process opens a new PM-Segment to store the updated context data (here: the label).})
    }
    clause(%Q{PM-Segment object}) {
      paragraph(%Q{The PM-Segment object represents a continuous time period in which a metric is stored without any changes of relevant metric context attributes (e.g., scales, labels).})
      paragraph(%Q{The PM-Segment object is accessible only through the PM-Store object (e.g., for retrieving stored data, the PM-Store object has to be accessed).})
    }
  }
  clause(%Q{Model for the Alert Package}) {
    paragraph(%Q{The Alert Package deals with objects that represent status information about patient condition and/or technical conditions influencing the measurement or device functioning. Alert-related information is often subject to normative regulations and, therefore, requires special handling.})
    paragraph(%Q{In the model, all alarm-related object-oriented items are identified by the term alert. The term alert is used in this standard as a synonym for the combination of patient-related physiological alarms, technical alarms, and equipment user-advisory signals.})
    paragraph(%Q{An alarm is a signal that indicates abnormal events occurring to the patient or the device system. A physiological alarm is a signal that either indicates that a monitored physiological parameter is out of specified limits or indicates an abnormal patient condition. A technical alarm is a signal that indicates a device system is either not capable of accurately monitoring the patient’s condition or no longer monitoring the patient’s condition.})
    paragraph(%Q{The model defines three different levels of alarming. These levels represent different sets of alarm processing steps, ranging from a simple context-free alarm event detection to an intelligent device system alarm process. This process is required to prioritize all device alarms, to latch alarms if needed (a latched alarm does not stop when the alarm condition goes away), and to produce audible and visual alarm indications for the user.})
    paragraph(%Q{For consistent system-wide alarming, a particular medical device may provide either no alarming capability or exactly one level of alarming, which is dependent on the capabilities of the device. Each level is represented by one specific object class. In other words, either zero or one alarm object class (e.g., only Alert or only Alert Status or only Alert Monitor; no combinations) is instantiated in the device containment tree. Multiple instances of a class are allowed.})
    note(%Q{Medical device alarming is subject to various national and international safety standards (e.g., IEC 60601 series, ISO 9703 series). Considering requirements of current safety standards, objects in this standard define information contents only. Any implementation shall, therefore, follow appropriate standards for dynamic alarming behavior.})
    paragraph(%Q{Figure 5 shows the object model of the Alert Package.})
    note(%Q{Instances of objects in the Alert Package area shall be contained in exactly one superior object.})
    paragraph(%Q{The Alert Package model contains the objects described in 6.4.1 through 6.4.3.})
    clause(%Q{Alert object}) {
      paragraph(%Q{The Alert object stands for the status of a simple alarm condition check. As such, it represents a single alarm only. The alarm can be either a physiological alarm or a technical alarm condition of a related object [e.g., MDS (i.e., medical device system), VMD, Metric]. If a device instantiates an Alert object, it shall not instantiate the Alert Status or the Alert Monitor object. A single Alert object is needed for each alarm condition that the device is able to detect.})
      paragraph(%Q{The Alert object has a reference to an object instance in the Medical Package to which the alarm condition relates.})
      note(%Q{An Alert object instance is not dynamically created or deleted in cases where alarm conditions start or stop. Rather, an existing Alert object instance changes attribute values in these cases.})
      example(%Q{An Alert object may represent the status of a process that checks for a limit violation physiological alarm of the heart rate signal. In the case of a violation of the limit, the object generates an event (i.e., attribute update) that represents this alarm condition in the form of attribute value changes.})
      figure(%Q{—Alert Package model})
    }
    clause(%Q{Alert Status object}) {
      paragraph(%Q{The Alert Status object represents the output of an alarm process that considers all alarm conditions in a scope that spans one or more objects. In contrast to the Alert object, the Alert Status object collects all alarm conditions related to a VMD object hierarchy or related to an MDS object and provides this information in list-structured attributes. Collecting all alarms together allows the implementation of first-level alarm processing where knowledge about the VMD or MDS can be used to prioritize alarm conditions and to suppress known false alarm indications.})
      paragraph(%Q{For larger scale devices without complete alarm processing, the Alert Status object greatly reduces the overhead of a large number of Alert object instances.})
      paragraph(%Q{If a device instantiates an Alert Status object, it shall not instantiate the Alert or the Alert Monitor object. Each VMD or MDS in the MDIB is able to contain at most one Alert Status object instance.})
      example(%Q{An ECG VMD derives a heart rate value. As the VMD is able to detect that the ECG leads are disconnected from the patient, its Alert Status object reports only a technical alarm and suppresses a heart rate limit violation alarm in this case.})
    }
    clause(%Q{Alert Monitor object}) {
      paragraph(%Q{The Alert Monitor object represents the output of a device or system alarm processor. As such, it represents the overall device or system alarm condition and provides a list of all alarm conditions of the system in its scope. This list includes global state information and individual alarm state information that allows the implementation of a safety-standard-compliant alarm display on a remote system.})
      paragraph(%Q{If a device instantiates an Alert Monitor object, it shall not instantiate the Alert or the Alert Status object. An MDS shall contain not more than one Alert Monitor object instance.})
      example(%Q{A patient-monitoring system provides alarm information in the form of an Alert Monitor object to a central station. Alert information includes the current global maximum severity of audibleand visual alarm conditions on the monitor display as well as a list of active technical and physiological alarm conditions. The alarm processor operates in a latching mode where physiological alarm conditions are buffered until they are explicitly acknowledged by a user.})
    }
  }
  clause(%Q{Model for the System Package}) {
    paragraph(%Q{The System Package deals with the representation of devices that derive or process vital signs information and comply with the definitions in this standard.})
    paragraph(%Q{Figure 6 shows the object model for the System Package.})
    figure(%Q{—System Package model})
    paragraph(%Q{The System Package model contains the objects described in 6.5.1 through 6.5.10.})
    clause(%Q{VMS (i.e., virtual medical system) object}) {
      paragraph(%Q{The VMS object is the abstract base class for all System Package objects in this model. It provides consistent naming and identification of system-related objects.})
      paragraph(%Q{As a base class, the VMS object cannot be instantiated.})
    }
    clause(%Q{MDS object}) {
      paragraph(%Q{The MDS object is an abstraction of a device that provides medical information in the form of objects that are defined in the Medical Package of the DIM.})
      paragraph(%Q{The MDS object is the top-level object in the device’s MDIB and represents the instrument itself. Composite})
      paragraph(%Q{devices may contain additional MDS objects in the MDIB.})
      paragraph(%Q{Further specializations of this class are used to represent differences in complexity and scope.})
      paragraph(%Q{As a base class, the MDS object cannot be instantiated.})
    }
    clause(%Q{Simple MDS object}) {
      paragraph(%Q{The Simple MDS object represents a medical device that contains a single VMD instance only (i.e., a single-purpose device).})
    }
    clause(%Q{Hydra MDS object}) {
      paragraph(%Q{The Hydra MDS object represents a device that contains multiple VMD instances (i.e., a multipurpose device).})
    }
    clause(%Q{Composite Single Bed MDS object}) {
      paragraph(%Q{The Composite Single Bed MDS object represents a device that contains (or interfaces with) one or more Simple or Hydra MDS objects at one location (i.e., a bed).})
    }
    clause(%Q{Composite Multiple Bed MDS object}) {
      paragraph(%Q{The Composite Multiple Bed MDS object represents a device that contains (or interfaces with) multiple MDS objects at multiple locations (i.e., multiple beds).})
    }
    clause(%Q{Log object}) {
      paragraph(%Q{The Log object is a base class that is a storage container for important local system notifications and events. It is possible to define specialized classes for specific event types.})
      paragraph(%Q{As a base class, the Log object cannot be instantiated.})
    }
    clause(%Q{Event Log object}) {
      paragraph(%Q{The Event Log object is a general Log object that stores system events in a free-text representation.})
      example(%Q{An infusion device may want to keep track of mode and rate changes by remote systems. When a remote operation is invoked, it creates an entry in its event log.})
    }
    clause(%Q{Battery object}) {
      paragraph(%Q{For battery-powered devices, some battery information is contained in the MDS object in the form of attributes. If the battery subsystem is either capable of providing more information (i.e., a smart battery) or manageable, then a special Battery object is provided.})
    }
    clause(%Q{Clock object}) {
      paragraph(%Q{The Clock object provides additional capabilities for handling date-related and time-related information beyond the basic capabilities of an MDS object. It models the real-time clock capabilities of an MDS object.})
      paragraph(%Q{The Clock object is used in applications where precise time synchronization of medical devices is needed. This object provides resolution and accuracy information so that applications can synchronize real-time data streams between devices.})
    }
  }
  clause(%Q{Model for the Control Package}) {
    paragraph(%Q{The Control Package contains objects that allow remote measurement control and device control.})
    paragraph(%Q{The model for remote control defined in this standard provides the following benefits:})
    unordered_list {
      unordered_list_item(%Q{A system that allows remote control is able to explicitly register which attributes or features can be accessed or modified by a remote system.})
      unordered_list_item(%Q{For attributes that can be remotely modified, a list of possible legal attribute values is provided to the controlling system.})
      unordered_list_item(%Q{It is not mandatory that a remote-controllable item correspond to an attribute of an medical object.})
      unordered_list_item(%Q{Dependence of a controllable item on internal system states is modeled.})
      unordered_list_item(%Q{A simple locking transaction scheme allows the handling of transient states during remote control.})      
    }
    paragraph(%Q{At least two different uses of remote control are considered:})
    unordered_list {
      unordered_list_item(%Q{Automatic control may be done by some processes running on the controlling device. Such a process has to be able to discover automatically how it can modify or access the controllable items to provide its function.})
      unordered_list_item(%Q{It is also possible to use remote control to present some form of control interface to a human operator. For this use, descriptions of functions, and possibly help information, need to be provided.})      
    }
    paragraph(%Q{The basic concept presented here is based on Operation objects. An Operation object allows modification of a virtual attribute. This virtual attribute may, for example, be a measurement label, a filter state (on/off), or a gain factor. The attribute is called virtual because it need not correspond to any attribute in other objects instantiated in the system.})
    paragraph(%Q{Different specializations of the Operation object define how the virtual attribute is modified. A Select Item operation, for example, allows the selection of an item from a given list of possible item values for the attribute. A Set Value operation allows the setting of the attribute to a value from a defined range with a specific step width (i.e., resolution).})
    paragraph(%Q{The idea is that the Operation object provides all necessary information about legal attribute values. Furthermore, the Operation object defines various forms of text string to support a human user of the operation. It also contains grouping information that allows logical grouping of multiple Operation objects together when they are presented as part of a human interface.})
    paragraph(%Q{Operation objects cannot directly be accessed by services defined in the service model in Clause 8. Instead, all controls shall be routed through the SCO (i.e., service and control object). This object supports a simple locking mechanism to prevent side effects caused by simultaneous calls.})
    paragraph(%Q{The SCO groups together all Operation objects that belong to a specific entity (i.e., MDS, VMD). The SCO also allows feedback to a controlled device, for example, for a visual indication that the device is currently remote-controlled.})
    paragraph(%Q{Figure 7 shows the object model for the Control Package:})
    paragraph(%Q{The Control Package model contains the objects described in 6.6.1 through 6.6.9.})
    figure(%Q{—Control Package model})
    clause(%Q{SCO}) {
      paragraph(%Q{The SCO is responsible for managing all remote-control capabilities that are supported by a medical device.})
      paragraph(%Q{Remote control in medical device communication is sensitive to safety and security issues. The SCO provides means for the following:})
      ordered_list {
        ordered_list_item1(%Q{Simple transaction processing, which prevents inconsistencies when a device is controlled from multiple access points (e.g., local and remote) and during the processing of control commands.})
        ordered_list_item1(%Q{State indications, which allows local and remote indication of ongoing controls.}) 
      }
    }
    clause(%Q{Operation object}) {
      paragraph(%Q{The Operation object is the abstract base class for classes that represent remote-controllable items. Each Operation object allows the system to modify some specific item (i.e., a virtual attribute) in a specific way defined by the Operation object. Operation objects are not directly accessible by services defined in the service model in Clause 8. All controls shall be routed through the SCO object (i.e., the parent) to allow a simple form of transaction processing.})
      paragraph(%Q{The set of Operation objects instantiated by a particular medical device defines the complete remote control interface of the device. This way a host system is able to discover the remote control capabilities of a device in the configuration phase.})
    }
    clause(%Q{Select Item Operation object}) {
      paragraph(%Q{The Select Item Operation object allows the selection of one item out of a given list.})
      example(%Q{The invasive pressure VMD may allow modification of its label. It uses a Select Item Operation object for this function. The list of legal values supplied by the operation may be, for example, {ABP, PAP, CVP, LAP}. By invoking the operation, a user is able to select one value out of this list.})
    }
    clause(%Q{Set Value Operation object}) {
      paragraph(%Q{The Set Value Operation object allows the adjustment of a value within a given range with a given resolution.})
      example(%Q{A measurement VMD may allow adjustment of a signal gain factor. It uses the Set Value Operation object for this function. The operation provides the supported value range and step width within this range.})
    }
    clause(%Q{Set String Operation object}) {
      paragraph(%Q{The Set String Operation object allows the system to set the contents of an opaque string variable of a given maximum length and format.})
      example(%Q{An infusion device may allow a remote system to set the name of the infused drug in free-text form to show it on a local display. It defines an instance of the Set String Operation object for this function. The operation specifies the maximum string length and the character format so that the device is able to show the drug name on a small display.})
    }
    clause(%Q{Toggle Flag Operation object}) {
      paragraph(%Q{The Toggle Flag Operation object allows operation of a toggle switch (with two states, e.g., on/off).})
      example(%Q{An ECG VMD may support a line frequency filter. It uses the Toggle Flag Operation object for switching the filter on or off.})
    }
    clause(%Q{Activate Operation object}) {
      paragraph(%Q{The Activate Operation object allows a defined activity to be started (e.g., a zero pressure).})
      example(%Q{The zero procedure of an invasive pressure VMD may be started with an Activate Operation object.})
    }
    clause(%Q{Limit Alert Operation object}) {
      paragraph(%Q{The Limit Alert Operation object allows adjustment of the limits of a limit alarm detector and the switching of the limit alarm to on or off.})
    }
    clause(%Q{Set Range Operation object}) {
      paragraph(%Q{The Set Range Operation object allows the selection of a value range by the simultaneous adjustment of a low and high value within defined boundaries.})
      example(%Q{A measurement VMD may provide an analog signal input for which the signal input range can be adjusted with a Set Range Operation object.})
    }
  }
  clause(%Q{Model for the Extended Services Package}) {
    paragraph(%Q{The Extended Services Package contains objects that provide extended medical object management services that allow efficient access to medical information in communicating systems. Such access is achieved by a set of objects that package attribute data from multiple objects in a single event message.})
    paragraph(%Q{The objects providing extended services are conceptually derived from ISO/OSI system management services defined in the ISO/IEC 10164 family of standards (specifically Part 5 and Part 13). The definitions have been adapted to and optimized for specific needs in the area of vital signs communication between medical devices.})
    paragraph(%Q{Figure 8 shows the object model for the Extended Services Package.})
    figure(%Q{—Extended Services Package model})
    paragraph(%Q{The Extended Services Package model contains the objects described in 6.7.1 through 6.7.9.})
    clause(%Q{Scanner object}) {
      paragraph(%Q{A Scanner object is a base class that is an observer and “summarizer” of object attribute values. It observes attributes of managed medical objects and generates summaries in the form of notification event reports. These event reports contain data from multiple objects, which provide a better communication performance compared to separate polling commands (e.g., GET service) or multiple individual event reports from all object instances.})
      paragraph(%Q{Objects derived from the Scanner object may be instantiated either by the agent system itself or by the manager system (e.g., dynamic scanner creation by using the CREATE service).})
      paragraph(%Q{As a base class, the Scanner object cannot be instantiated.})
    }
    clause(%Q{CfgScanner (i.e., configurable scanner) object}) {
      paragraph(%Q{The CfgScanner object is a base class that has a special attribute (i.e., the ScanList attribute) that allows the system to configure which object attributes are scanned. The ScanList attribute may be modified either by the agent system (i.e., auto-configuration or pre-configuration) or by the manager system (i.e., full dynamic configuration by using the SET service).})
      paragraph(%Q{A CfgScanner object may support different granularity for scanning:})
      ordered_list {
        ordered_list_item1(%Q{Attribute group (i.e., a defined set of attributes): The ScanList attribute contains the identifiers (IDs) of attribute groups, and all attributes in the group are scanned.})
        ordered_list_item1(%Q{Individual attribute: The ScanList attribute contains the IDs of all attributes that are scanned.})        
      }
      paragraph(%Q{In order to deal efficiently with optional object attributes, the attribute group scan granularity is recommended for CfgScanner objects.})
      paragraph(%Q{As a base class, the CfgScanner object cannot be instantiated.})
    }
    clause(%Q{EpiCfgScanner (i.e., episodic configurable scanner) object}) {
      paragraph(%Q{The EpiCfgScanner object is responsible for observing attributes of managed medical objects and for reporting attribute changes in the form of unbuffered event reports.})
      paragraph(%Q{The unbuffered event report is triggered only by object attribute value changes. If the EpiCfgScanner object uses attribute group scan granularity, the event report contains all attributes of the scanned object that belong to this attribute group if one or more of these attributes changed their value.})
      example(%Q{A medical device provides heart beat detect events in the form of an Enumeration object. A display application creates an instance of the EpiCfgScanner object and adds the observed value of the Enumeration object to the ScanList attribute. The scanner instance afterwards sends a notification when the Enumeration object reports a heart beat.})
    }
    clause(%Q{PeriCfgScanner (i.e., periodic configurable scanner) object}) {
      paragraph(%Q{The PeriCfgScanner object is responsible for observing attributes of managed medical objects and for periodically reporting attribute values in the form of buffered event reports. A buffered event report contains the attribute values of all available attributes that are specified in the scan list, independent of attribute value changes.})
      example(%Q{If the scanner operates in a special superpositive mode, the buffered event report contains all value changes of attributes that occurred in the reporting period; otherwise, the report contains only the most recent attribute values.Example: A data logger creates an instance of the PeriCfgScanner object and configures the scanner so that it sends an update of the observed value attributes of all Numeric objects in the MDIB every 15 seconds.})
    }
    clause(%Q{FastPeriCfgScanner (i.e., fast periodic configurable scanner) object}) {
      paragraph(%Q{The FastPeriCfgScanner object is a specialized object class for scanning the observed value attribute of the Real Time Sample Array object. This special scanner object is further optimized for low-latency reporting and efficient communication bandwidth utilization, which is required to access real-time waveform data.})
      example(%Q{A real-time display application (e.g., manager system) wants to display ECG waveforms. It creates a FastPeriCfgScanner object on the agent system (e.g., server device) and requests periodic updates of all ECG leads.})
    }
    clause(%Q{UcfgScanner (i.e., unconfigurable scanner) object}) {
      paragraph(%Q{The UcfgScanner object is a base class that scans a predefined set of managed medical objects that cannot be modified. In other words, an UcfgScanner object typically is a reporting object that is specialized for one specific purpose.})
      paragraph(%Q{As a base class, the UcfgScanner object cannot be instantiated.})
    }
    clause(%Q{Context Scanner object}) {
      paragraph(%Q{The Context Scanner object is responsible for observing device configuration changes. After instantiation, the Context Scanner object is responsible for announcing the object instances in the device’s MDIB. The scanner provides the object instance containment hierarchy and static object attribute values.})
      paragraph(%Q{In case of dynamic configuration changes, the Context Scanner object sends notifications about new object instances or deleted object instances.})
      example(%Q{A data logger creates an instance of the Context Scanner object in an agent MDIB to receive notifications about MDS configuration changes when new measurement modules are plugged in (i.e., new VMD instance) or when such a module is unplugged (i.e., VMD instance deleted).})
    }
    clause(%Q{Alert Scanner object}) {
      paragraph(%Q{The Alert Scanner object is responsible for observing the alert-related attribute groups of objects in the Alert Package. As alarming in general is security-sensitive, the scanner is not configurable (i.e., all or no Alert objects are scanned).})
      paragraph(%Q{The Alert Scanner object sends event reports periodically so that timeout conditions can be checked.})
    }
    clause(%Q{Operating Scanner object}) {
      paragraph(%Q{The Operating Scanner object is responsible for providing all information about the operating and control system of a medical device.})
      paragraph(%Q{In other words, the scanner maintains the configuration of Operation objects contained in SCOs (by sending CREATE notifications for Operation objects), it scans transaction-handling-related SCO attributes, and it scans Operation object attributes. Because SCOs and Operation objects may have dependencies, the scanner is not configurable.})
    }
  }
  clause(%Q{Model for the Communication Package}) {
    paragraph(%Q{The Communication Package deals with objects that enable and support basic communication.})
    paragraph(%Q{Figure 9 shows the object model for the Communication Package.})
    figure(%Q{—Communication Package model})
    paragraph(%Q{The Communication Package model contains the objects described in 6.8.1 through 6.8.6.})
    clause(%Q{Communication Controller object}) {
      paragraph(%Q{The Communication Controller object represents the upper layer and lower layer communication profile of a medical device.})
      paragraph(%Q{The Communication Controller object is the access point for retrieving Device Interface attributes and management information base element (MibElement) attributes for obtaining management information related to data communications.})
      paragraph(%Q{As a base class, the Communication Controller object cannot be instantiated. Two instantiable specializations, however, are defined: BCC and DCC. A medical device MDIB contains either no Communication Controller object or one BCC object or one DCC object (depending on its role).})
    }
    clause(%Q{DCC (i.e., device communication controller) object}) {
      paragraph(%Q{The DCC object is a Communication Controller object used by medical devices operating as agent systems (i.e., association responders).})
      paragraph(%Q{The DCC object shall contain one or more Device Interface objects.})
    }
    clause(%Q{BCC (i.e., bedside communication controller) object}) {
      paragraph(%Q{The BCC object is a Communication Controller object used by medical devices operating as manager systems (i.e., association requestors).})
      paragraph(%Q{The BCC object shall contain one or more Device Interface objects.})
    }
    clause(%Q{Device Interface object}) {
      paragraph(%Q{The Device Interface object represents a particular interface, i.e., port. The port is either a logical or a physical end point of an association for which (e.g., statistical) data captured in the MibElement objects can be independently collected.})
      paragraph(%Q{Both an agent system and a manager system can have multiple logical or physical ports, depending on the selected implementation of the lower layer communication system.})
      paragraph(%Q{The Device Interface object is not accessible by CMDISE services. This object contains at least one Mib-Element object (i.e., the Device Interface MibElement object, which represents device interface properties), which can be accessed by a special method defined by the Communication Controller object.})
    }
    clause(%Q{MibElement object}) {
      paragraph(%Q{The MibElement object contains statistics and performance data for one Device Interface object. The MibElement object is a base class for specialized MibElement objects only. It cannot be instantiated.})
      paragraph(%Q{Various MibElement object types are defined to group management information in defined packages, which can be generic or dependent on specific transport profiles.})
      paragraph(%Q{The MibElement object is not directly accessible. Its attributes can be accessed only through a Communication Controller object. The MibElement object is not part of the device’s MDIB.})
    }
    clause(%Q{Specialized MibElement object}) {
      paragraph(%Q{Management information for a communication link is dependent on the lower layers of the communication stack (i.e., lower layers profile).})
      paragraph(%Q{This standard, however, defines only two generic MibElement objects:})
      unordered_list {
        unordered_list_item(%Q{The mandatory Device Interface MibElement object, which describes properties of the device interface})
        unordered_list_item(%Q{An optional general communication statistics MibElement object, which models typical communication statistics that are generally applicable})        
      }
      paragraph(%Q{Specialized MibElement objects are defined in IEEE P1073.2.1.2.})
    }
  }
  clause(%Q{Model for the Archival Package}) {
    paragraph(%Q{The Archival Package deals with storage and representation of biosignals, status, and context information inan on-line or an off-line archive.})
    paragraph(%Q{Figure 10 shows the object model of the Archival Package.})
    paragraph(%Q{The Archival Package model contains the objects described in 6.9.1 through 6.9.7.})
    figure(%Q{—Archival Package model})
    clause(%Q{Multipatient Archive object}) {
      paragraph(%Q{The Multipatient Archive object groups together multiple Patient Archive objects referring to different patients.})
      example(%Q{A drug study may be documented in the form of a Multipatient Archive object containing multiple Patient Archive objects that show how the drug affected the monitored vital signs.})
    }
    clause(%Q{Patient Archive object}) {
      paragraph(%Q{The Patient Archive object groups patient-related information (e.g., vital signs data, treatment data, and patient demographics) together in a single archive object. This object relates to static (i.e., invariant) data in a Patient Demographics object only.})
      example(%Q{A hospital may store data about multiple visits of a single patient in a Patient Archive object that contains a number of Session Archive objects, each documenting vital signs information recorded during a specific visit in a hospital department.})
    }
    clause(%Q{Session Archive object}) {
      paragraph(%Q{The Session Archive object represents a patient visit or a continuous stay in the a hospital or hospital department. Diagnostic treatments performed during this time period are represented by Session Test objects contained in the Session Archive object. The Session Archive object refers to dynamic (i.e., variant) data in a Patient Demographics object.})
    }
    clause(%Q{Physician Archive object}) {
      paragraph(%Q{The Physician object represents the physician responsible for the set of diagnostic and therapeutic activities during the time period represented by the Session Archive object.})
    }
    clause(%Q{Session Test object}) {
      paragraph(%Q{The Session Test object contains vital signs information of a single patient that is recorded during a single examination or diagnostic treatment. This object contains vital signs metrics in form of PM-Store objects. It also may contain information about equipment that was used for recording (in the form of relations to MDS and Ancillary objects).})
      example(%Q{Vital signs information recorded during a ECG stress test examination is organized in a Session Test object.})
    }
    clause(%Q{Session Notes object}) {
      paragraph(%Q{The Session Notes object is a container for diagnostic data, patient care details, and treatment-related information in the form of textual data.})
    }
    clause(%Q{Ancillary object}) {
      paragraph(%Q{The Ancillary object is not further defined in this standard. This object is present in the model to indicate that information from sources other than devices within the scope of this standard are permitted to be included in (or referenced by) the Session Test object.})
      example(%Q{Image data that complies with the MEDICOM (CEN ENV 12052) or DICOM (NEMA PS 3) standard are permitted to be included in the Session Test object as ancillary data.})
    }
  }
  clause(%Q{Model for the Patient Package}) {
    paragraph(%Q{The Patient Package deals with all patient-related information that is relevant in the scope of this standard,})
    paragraph(%Q{but is not vital signs information modeled in the Medical Package.})
    paragraph(%Q{Figure 11 shows the object model for the Patient Package:})
    paragraph(%Q{The Patient Package model contains one object (see 6.10.1).})
    figure(%Q{—Patient Package model})
    clause(%Q{Patient Demographics object}) {
      paragraph(%Q{The Patient Demographics object stores patient census data.})
      paragraph(%Q{This standard provides minimal patient information as typically required by medical devices. A complete patient record is outside the scope of this standard.})
    }
  }
  clause(%Q{DIM—dynamic model}) {
    clause(%Q{General}) {
      paragraph(%Q{Subclause 6.11 defines global dynamic system behavior.})
      paragraph(%Q{Note that dynamic object behavior resulting from invocation of object management services is part of the object definitions in Clause 7.})
    }
    clause(%Q{MDS communication finite state machine (FSM)}) {
      paragraph(%Q{Figure 12 shows the MDS FSM for a communicating medical device that complies with the definitions in this standard. The FSM is used to synchronize the operational behavior of manager (i.e., client) systems and agent (i.e., server) systems.})
      paragraph(%Q{After power-up, the device performs all necessary local initializations (i.e, boot phase) and ends up in the disconnected state, where it waits for connection events.})
      paragraph(%Q{When a connection event is detected, the device tries to establish a logical connection (i.e, an association) with the other device. A manager (i.e., client) system is the association requester, and an agent (i.e, server) system is the association responder. Basic compatibility checks are performed in the associating state.})
      figure(%Q{—MDS FSM})
      paragraph(%Q{After successful association, configuration data (i.e., the MDIB structure) is exchanged by the use of services and extended services (in particular, the Context Scanner object) as defined in this standard. Additional information (e.g., MDS attributes) is supplied that allows further compatibility and state checks.})
      paragraph(%Q{After configuration, medical data are exchanged by using services and extended services as defined in this standard. Dynamic reconfiguration is allowed in the operating state. If the device or the type of reconfiguration does not allow dynamic handling in the operating state, a special “reconfiguring” state is provided.})
      paragraph(%Q{If an event indicates an intention to disconnect, the disassociating state is entered.})
      paragraph(%Q{The diagram does not show error events. Fatal error events take the state machine out of the operating state.})
      note(%Q{This state machine describes the behavior of the MDS communication system only. Usually the device must perform its medical function independent of the communication system.})
      paragraph(%Q{The FSM is considered a part of the MDS object. The MDS Status attribute reflects the state of the machine. The MDS may announce state changes in the form of attribute change event reports.})
      paragraph(%Q{Specific application profiles shall use this state machine as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.})
    }
    clause(%Q{Communicating systems—startup object interaction diagram}) {
      paragraph(%Q{Figure 13 presents the object interaction diagram that visualizes the startup phase after connecting two devices.})
      figure(%Q{—Startup after connection})
      paragraph(%Q{It is assumed here that, conceptually, messages are exchanged between the Communication Controller objects (using the device interface).})
      paragraph(%Q{Some form of connection indication is necessary to make the manager system aware of a new agent on the network. This mechanism is dependent on the specific lower layer implementation; therefore, it is not further defined in this standard.})
      paragraph(%Q{Specific application profiles shall use this interaction diagram as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.})
    }
    clause(%Q{Communication Package—MibElement data access}) {
      paragraph(%Q{Figure 14 presents the object interaction diagram that shows how a manager system accesses the MibElement data using the objects defined in the Communication Package (see 6.8).})
      figure(%Q{—MibElement data access})
      paragraph(%Q{The diagram assumes the following:})
      unordered_list {
        unordered_list_item(%Q{An association is established between the agent and the manager.})
        unordered_list_item(%Q{The configuration phase is finished, and the manager has an image of the agent’s MDIB.})
        unordered_list_item(%Q{The DCC object is part of the agent’s MDIB.})        
      }
      paragraph(%Q{The manager first uses the GET service to retrieve all DCC object attributes and their values. The attributes specify how many Device Interface objects exist.})
      paragraph(%Q{The manager uses the ACTION service with the Communication Controller object-defined method to retrieve the attributes of the mandatory Device Interface MibElement object. The MibElement attribute variables specify if any additional MibElement objects are available for the interface.})
      paragraph(%Q{If so, the manager can use the same ACTION command to retrieve the additional management information represented in the MibElement objects.})
    }
    clause(%Q{Dynamic object relations}) {
      paragraph(%Q{This subclause deals with relations between managed medical objects (i.e., objects that are defined as managed objects in this standard).})
      paragraph(%Q{Generally, the relationships between object instances that are defined in the package models are dynamic.})
      example(%Q{A modular patient monitor is modeled as an MDS. Measurement modules are modeled as VMDs. If a new module is connected to the monitor, there is also a new relationship between the MDS and the new VMD instance.})
      paragraph(%Q{Communicating agent systems (i.e., agents) use services defined in this standard to announce configuration change events to other connected systems. These manager systems (i.e., managers) modify their view of the agent MDIB.})
      paragraph(%Q{Not only does a vital signs information archive have to update its configuration, but it also has to permanently store these connection and disconnection events.})
      example(%Q{An instance of the Session Archive object represents the stay of a patient in the intensive care unit (ICU). During that period, new devices are connected to the patient to increase the number of recorded vital signs. They are removed again as soon as the patient’s condition stabilizes. The Session Archive object shall not delete recorded data when the recording device is disconnected.})
      paragraph(%Q{Thus, in certain applications (e.g., archival applications), object instance relationships have associated information that must be captured.})
      paragraph(%Q{When required, the relationships themselves can be considered to be special managed objects as shown in Figure 15.})
      figure(%Q{—Example of a relationship represented by an object})
      paragraph(%Q{The example in Figure 15 shows a relation between a Session Archive object and an MDS object. The relation is represented as an object. This object has attributes that provide information, for example, about time of connection and disconnection.})
      paragraph(%Q{Modeling the relations as objects has the advantage that information can be defined in the form of attributes. It is not necessary to assign the attributes to one or both objects that are related.})
      paragraph(%Q{How dynamic object relations are handled by systems that comply with the definitions in this standard is defined in 6.11.5.1 and 6.11.5.2.})
      clause(%Q{Dynamic object relations in communicating systems}) {
        paragraph(%Q{Relations between object instances that are defined in the package models are considered configuration information. The Context Scanner object provides configuration information in the form of object create notifications and object delete notifications. No means for persistent storage of past (i.e., old) configuration information is defined for communicating systems.})
        paragraph(%Q{Other relations between object instances (e.g., to reference a source signal in derived data) are specified in the form of attributes of the corresponding objects (e.g., the Vmo-Source-List attribute of the Metric object). Dynamic changes of these attributes are announced by attribute change notification events.})
      }
      clause(%Q{Dynamic object relations in archival systems}) {
        paragraph(%Q{An archival system may need to provide persistent storage for configuration information. In this case, the corresponding relations are considered to be represented by objects, as shown in 6.11.5.})
        paragraph(%Q{An archival system that uses a data format in compliance with the definitions in this standard has to provide means to store (i.e., archive) the attributes of dynamic relationship objects.})
      }
    }
  }
}

