module Standard
  module DSLHelper
    def content_info(str, depth)
      block_items = []
      block_items << ("  " * (depth + 1)) + "render_as(#{render_as.value.inspect})\n" if render_as&.value
      block_items << ("  " * (depth + 1)) + "source_file(#{source_file_filename.inspect})\n" if source_file
      block_items << ("  " * (depth + 1)) + "file_usage(#{file_usage.value.inspect})\n" if file_usage&.value
      block_items << ("  " * (depth + 1)) + "model_element(#{model_element.qualified_name.inspect})\n" if model_element
      footnotes.each { |f| block_items << f.to_dsl(depth + 1) }
      if block_items.any?
        str << "  {\n"
        block_items.each { |bi| str << bi }
        str << ("  " * depth) + "}"
      end
      str << "\n"
    end
  end
  
  class IEEEStandard
    def to_dsl
      output = "standard {\n"
      output << "  title(#{title.inspect})\n" if title
      output << "  subtitle1(#{subtitle1.inspect})\n" if subtitle1
      output << "  subtitle2(#{subtitle2.inspect})\n" if subtitle2
      content.each { |c| output << c.to_dsl(1) } 
      output << "\n}"
    end
  end
  
  class Clause
    def to_dsl(depth)
      output = ("  " * depth) + "clause(#{title.inspect}) {\n"
      output << ("  " * (depth + 1)) + "model_element(#{model_element.qualified_name.inspect})\n" if model_element
      content.each do |ce|
        ce_dslified = ce.to_dsl(depth + 1)
        if ce_dslified
          output << ce_dslified
        else
          puts ce.inspect
          raise
        end
      end
      output << ("  " * depth) + "}\n"
    end
  end
    
  class AbbreviationSection
    def to_dsl(depth)
      output = ("  " * depth) + "abbreviations_section {\n"
      abbreviations.each { |x| output << x.to_dsl(depth + 1)}
      output << ("  " * depth) + "}\n"
    end
  end
  class NormativeReferenceSection
    def to_dsl(depth)
      output = ("  " * depth) + "normative_reference_section {\n"
      normative_references.each { |x| output << x.to_dsl(depth + 1)}
      output << ("  " * depth) + "}\n"
    end
  end
  class DefinitionSection
    def to_dsl(depth)
      output = ("  " * depth) + "definition_section {\n"
      definitions.each { |x| output << x.to_dsl(depth + 1)}
      output << ("  " * depth) + "}\n"
    end
  end
  class Abbreviation
    def to_dsl(depth)
      output = ("  " * depth) + "abbreviation(#{symbol.inspect}, #{meaning.inspect})\n"
    end
  end
  class NormativeReference
    def to_dsl(depth)
      output = ("  " * depth) + "normative_reference(#{source.inspect}, #{description_content.inspect})"
      if footnotes.any?
        output << "  {\n"
        footnotes.each { |f| output << f.to_dsl(depth + 1) }
        output << ("  " * depth) + "}"
      end
      output << "\n"
    end
  end
  class Definition
    def to_dsl(depth)
      output = ("  " * depth) + "definition(#{term.inspect}, #{definition.inspect})\n"
    end
  end
  class Text
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "text(#{content_content.inspect})"
      content_info(output, depth)
    end
  end
  class Table
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "table(#{title.inspect})"
      content_info(output, depth)
    end
  end
  class Code
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "code(#{title.inspect})"
      content_info(output, depth)
    end
  end
  class Note
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "note(#{content_content.inspect})"
      content_info(output, depth)
    end
  end
  class Example
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "example(#{content_content.inspect})"
      content_info(output, depth)
    end
  end
  class Footnote
    def to_dsl(depth)
      output = ("  " * depth) + "footnote(#{content_content.inspect})\n"
    end
  end
  class Figure
    include DSLHelper
    def to_dsl(depth)
      output = ("  " * depth) + "figure(#{caption.inspect})"
      content_info(output, depth)
    end
  end
end

# Standard::GeneralRequirements # general_requirements_original.xml
# Standard::Introduction # just paste this in
# Standard::Scope # original_scope.xml