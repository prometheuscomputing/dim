# based on http://stackoverflow.com/a/28725575/1025695

# Note:  1 em == 239.101853847 twips
# 1440 twips = 1 inch
# 1 inch = 
require 'ttfunk'
require 'valuable'
# Everything you never wanted to know about glyphs:
# http://chanae.walon.org/pub/ttf/ttf_glyphs.htm

# this code is a substantial reworking of:
# https://github.com/prawnpdf/ttfunk/blob/master/examples/metrics.rb

class FontHelper
  attr_reader :file

  def initialize(path_to_file)
    @file = TTFunk::File.open(path_to_file)
  end

  def width_of( string )
    string.split('').map{|char| character_width( char )}.inject{|sum, x| sum + x}
  end

  def character_width( character )
    width_in_units = ( horizontal_metrics.for( glyph_id( character )).advance_width )
    # puts_green width_in_units
    ret = width_in_units.to_f / units_per_em
    # puts_magenta(ret * (1440/239.101853847))
    ret
  end

  def units_per_em
    @u_per_em ||= file.header.units_per_em
  end

  def horizontal_metrics
    @hm = file.horizontal_metrics
  end

  def glyph_id(character)
    character_code = character.unpack("U*").first
    file.cmap.unicode.first[character_code]
  end
end
class TimesNewRoman < FontHelper
  font_file = relative "Times_New_Roman_Normal.ttf"
  # font_file = relative "TimesNewRoman.ttf"
  FONT = new(font_file) unless defined?(FONT)
  def self.width_of(string)
    FONT.width_of(string)
  end
end

# din = Font.new("#{File.dirname(__FILE__)}/../../fonts/DIN/DINPro-Light.ttf")
# din = Font.new("Times_New_Roman_Normal.ttf")
# puts din.width_of("Hypertension")
