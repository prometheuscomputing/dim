module StandardBuilder
  def self.footnotes
    @footnotes ||= []
  end
  
  def self.clear_footnotes
    @footnotes        = nil
    @next_footnote_id = nil
    @used_ids         = []
  end
  
  def self.collect_footnotes
    footnotes.join
  end
  
  def self.add_footnote(xml)
    footnotes << xml
  end
  
  def self.use_footnote_id(val)
    @used_ids << val
    val
  end
  
  def self.next_footnote_id
    # @next_footnote_id ||= 1
    # ret = @next_footnote_id
    # @next_footnote_id += 1
    # ret
    val = rand(1_000..9_999)
    if @used_ids.include?(val)
      next_footnote_id
    else
      use_footnote_id(val)
    end
  end
end