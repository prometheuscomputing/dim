module StandardBuilder
  def self.format(key, val = nil)
    @formats ||= {}
    if val
      @formats[key.to_sym] = val
    else
      @formats[key.to_sym]
    end
  end
  # FIXME look here!! http://stackoverflow.com/questions/378887/how-do-i-calculate-a-strings-width-in-ruby

  # http://officeopenxml.com/WPindentation.php  Values are in twentieths of a point (aka twips).  
  # 72 points = 6 pica = 1 inch.

  format(:em_in_twips, 239.101853847)
  format(:cell_buffer, format(:em_in_twips))
  format(:twips_adjuster, (format(:em_in_twips) / 1.3))  # divisor determined by trial and error...I wish I understood this better.  1.5 is too big.
end
