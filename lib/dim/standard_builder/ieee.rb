require_relative 'standard_builder'
load relative('standard_builder.rb')
require_relative 'ieee_settings'
load relative('ieee_settings.rb')

module IEEE
  def self.build(standard_obj, options = {})
    clear_styles
    clear_templates
    StandardBuilder.clear_footnotes
    StandardBuilder.clear_relationships
    StandardBuilder.clear_references
    StandardBuilder.clear_table_footnotes
    options[:footnotes_template]     = IEEE.template('footnotes')
    options[:relationships_template] = IEEE.template('relationships')
    original = StandardBuilder.directory(:original_ieee_docx)
    replacement = standard_obj.to_ieee
    StandardBuilder.build(original, replacement, options)
  end

  # FIXME using opts here for pPr stuff is not great
  def self.paragraph(runs, style_key = :paragraph, opts = {})
    p_style = paragraph_style(style_key)
    opts.merge!({:style => p_style})
    xml = StandardBuilder.paragraph(runs, opts)
    xml
  end
  
  def self.table_caption(runs, xref, opts = {})
    xref ||= SecureRandom.hex(5) # because I don't want to know what MS Word does if it has a bookmark without a ref value...
    opts.merge!({:style => paragraph_style(:table_caption), :xref => xref})
    xml = StandardBuilder.paragraph(runs, opts)
    xml
  end
  
  def self.clause(title, depth)
    d = depth
    d = "h#{depth}".to_sym unless depth.is_a?(Symbol)
    xml = StandardBuilder.remove_blank_lines(paragraph(title, d))
    xml
  end
  
  def self.paragraph_style(style_key)
    style = styles(:paragraph, style_key.to_sym)
    if style
      template('paragraph_style').gsub('_STYLE_', style)
    else
      style(style_key)
    end
  end
  
  def self.clear_styles
    @styles = nil
  end  
  
  # TODO a bit sloppy to have @style_file hard-coded here....
  def self.styles(*path)
    @style_file ||= File.read(File.join(StandardBuilder.directory(:ieee_templates), "styles.yaml"))
    @styles     ||= YAML.load(@style_file)
    style = @styles.dig(*path)
    unless style.is_a?(String)
      puts path.pretty_inspect
      puts style.pretty_inspect
      raise
    end
    style
  end
  
  def self.table(rows:, table_format:, caption:, **opts)
    params = {
      :table_format => table_format,
      :headers      => table_headers(table_format),
      :rows         => rows
    }
    xref = opts[:xref]
    xml = table_caption(caption, xref) + StandardBuilder.table(opts.merge(params))
  end
  
  def self.table_headers(table_format)
    names  = StandardBuilder.format(table_format)[:names]
    widths = StandardBuilder.format(table_format)[:column_widths]
    raise unless names.size == widths.size
    cells = []
    names.each_with_index do |name, i|
      heading  = paragraph(name, :table_column_head)
      cells << StandardBuilder.table_cell(widths[i], heading)
    end
    xml = StandardBuilder.table_row(cells.join, :header => true)
  end
  
  def self.asn1_line(indent_val, runs, no_space = true)
    xml = StandardBuilder.get_xml('asn1_line').gsub('ASN1Indent', indent_val.to_s)
    unless no_space
      xml = xml.gsub('_SPACING_AFTER_', '')
    else
      xml = xml.gsub('_SPACING_AFTER_', StandardBuilder.get_xml('spacing_after').gsub('_SPACING_', '0'))
    end
    xml = xml.gsub("ASN1_RUNS", Array(runs).join)
    xml
  end
  
  def self.asn1_text_run(txt, opts = {})
    opts.merge!({:wvals => {:sz => 18, :szCs => 18}})
    StandardBuilder.text_run(txt, opts)
  end
  
  def self.remove_comments(str)
    StandardBuilder.remove_blank_lines(str.gsub(/^\s*<!--.*?-->\s*$/, ''))
  end
  
  def self.asn1_tab
    tab = StandardBuilder.get_xml('tab')
    StandardBuilder.run(tab, :wvals => {:sz => 18, :szCs => 18})
  end
  
  def self.asn1_comment_wrapper
    asn1_line(180, asn1_text_run("--"))
  end
  
  def self.comment_to_lines(string, ems_available)
    string.gsub!("\r", "")
    # wow, this is ugly...
    string.gsub!("\n", " _LINE_BREAK_ ")
    words = string.split(/ +|(?<=-)/)
    line = ''
    lines = []
    words.each_with_index do |w,i|
      # add back in space unless last word or hyphenated
      if w =~ /_LINE_BREAK_/
        lines << line
        line = ''
        next
      end
      w << " " unless (i + 1) == words.length || w[-1] == "-"
      if TimesNewRoman.width_of(line + w) < ems_available
        line << w
      else
        lines << line
        line = w
      end
      lines << line if (i + 1) == words.length
    end
    lines
  end
  
  # FIXME so that it wraps additional lines to start at the proper place
  def self.asn1_attribute_comment(string, tabs_used)
    return nil if string.nil? || string.empty?
    ems_available = StandardBuilder.format(:max_comment) - tabs_used * StandardBuilder.format(:asn_1_tab_size)
    comment_lines = comment_to_lines(string, ems_available)
    runs = []
    comment_lines.each do |cl|
      runs << asn1_text_run("-- #{cl}")
    end
    [runs.shift, runs]
  end
  
  # FIXME somehow we need to get leftovers continued on the next line
  def self.asn1_data_type_comment(string)
    return asn1_comment_wrapper if string.nil? || string.empty?
    lines = [asn1_comment_wrapper]
    comment_lines = comment_to_lines(string, StandardBuilder.format(:max_comment))
    comment_lines.each do |cl|
      runs = asn1_text_run("-- #{cl}")
      lines << asn1_line(180, runs)
    end
    lines << asn1_comment_wrapper
    lines.join
  end
end