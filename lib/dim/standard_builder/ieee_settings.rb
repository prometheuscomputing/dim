require_relative '../standard/formatting_help/word_size'
# load relative('standard/formatting_help/word_size.rb')
require_relative 'standard_builder'
load relative('standard_builder.rb')
module IEEE
  StandardBuilder.directory(:ieee_templates, File.join(StandardBuilder.directory(:standard), 'ieee_templates'))
  StandardBuilder.directory(:boilerplate, File.join(StandardBuilder.directory(:standard), 'ieee_boilerplate'))
  StandardBuilder.directory(:default_docx_output, File.join(StandardBuilder.directory(:standard), '11073_generated.docx'))
  StandardBuilder.directory(:replacement_content, File.join(StandardBuilder.directory(:standard), 'replacement.xml'))
  StandardBuilder.directory(:original_ieee_docx, File.join(StandardBuilder.directory(:standard), 'IEEESTD-WORDTEMPLATE_v1_2015.docx'))
  
  StandardBuilder.format(:asn_1_tab_size, 8.4)
  # StandardBuilder.format(:asn_1_tab_size, TimesNewRoman.width_of())
  # StandardBuilder.format(:attribute_indent, 2.6)
  StandardBuilder.format(:attribute_indent, TimesNewRoman.width_of('        '))
  StandardBuilder.format(:min_separation, 1) # This is four spaces.  In TimesNewRoman a space is apparently 0.25em
  StandardBuilder.format(:max_comment, 48)
  
  StandardBuilder.format(:attributes_table, {
    :column_widths => [1552, 2000, 1439, 2865, 1014],
    :names => ['Attribute name', 'Attribute ID', 'Attribute type', 'Remark', 'Qualifier']
  })
  
  StandardBuilder.format(:attribute_groups_table, {
    :column_widths => [2635, 2520, 3701],
    :names => ['Attribute group', 'Attribute group ID', 'Group elements']
  })
  
  StandardBuilder.format(:notifications_table, {
    :column_widths => [2095, 1260, 2160, 1620, 1735],
    :names => ['Event', 'Mode', 'Event ID', 'Event parameter', 'Event result']
  })
  
  StandardBuilder.format(:behaviors_table, {
    :column_widths => [1487, 1610, 2418, 2197, 1158],
    :names => ['Action', 'Mode', 'Action ID', 'Action parameter', 'Action result']
  })
  
  # uses chopped up IEEE template.  Caches them because we might use them a lot
  def self.template template_name
    @templates ||= {}
    return @templates[template_name] if @templates[template_name]
    @templates[template_name] = File.read(File.join(StandardBuilder.directory(:ieee_templates), "#{template_name}.xml"))
    @templates[template_name]
  end
  
  def self.clear_templates
    @templates = nil
  end
  
  # Not caching because each one should only be used once
  def self.boilerplate file_name
    File.read(File.join(StandardBuilder.directory(:boilerplate), "#{file_name}.xml"))
  end
end