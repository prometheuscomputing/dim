module PlainText
  def self.min_separation; 4; end
  
  def self.attribute_indent; tab.size; end

  def self.max_line_length; 80; end

  def self.tab
    '  '
  end
  
  def self.asn1_comment_wrapper
    "--"
  end
  #
  def self.comment_to_lines(string, chars_available, new_lines = true)
    # For some reason so of the stored comments still have newlines.  We need to get rid of them.
    string = string.gsub(/(?<=[^-])\n/, ' ').gsub(/(?<=-)\n/, '') unless new_lines
    comment_sections = string.split("\n")
    lines = comment_sections.collect { |sect| _comment_to_lines(sect, chars_available) }.flatten
  end

  def self._comment_to_lines(string, chars_available)
    words = string.split(/ +|(?<=-)/)
    comment_preface = asn1_comment_wrapper + ' '
    line  = comment_preface.dup
    lines = []
    words.each_with_index do |w,i|
      # add back in space unless last word or hyphenated
      w << " " unless (i + 1) == words.length || w[-1] == "-"
      if (line + w).length < chars_available
        line << w
      else
        lines << line
        line = comment_preface.dup + w
      end
      lines << line if (i + 1) == words.length
    end
    lines
  end
  
  def self.asn1_attribute_comment(string, chars_used)
    return nil if string.nil? || string.empty?
    chars_available = max_line_length - chars_used
    comment_lines = comment_to_lines(string, chars_available)
    runs = []
    comment_lines.each do |cl|
      runs << cl
    end
    [runs.shift, runs]
  end
  
  def self.asn1_data_type_comment(element)
    string = element.description&.safe_content
    return nil if string.nil? || string.empty?
    if string.slice(/FIX BY HAND/)
      puts "#{element.name} has a funky comment"
      string = string.gsub('FIX BY HAND', '').lstrip
    end
    lines = [asn1_comment_wrapper]
    comment_lines = comment_to_lines(string, max_line_length)
    comment_lines.each do |cl|
      lines << cl.strip
    end
    lines << asn1_comment_wrapper
    lines.join("\n")
  end
end