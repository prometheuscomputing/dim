module StandardBuilder
  def self.process_references(xml)
    regex = /\\xref_[\w-]+\\/ 
    xref  = regex.match(xml)
    if xref
      # puts_green xref
      xref = "_#{xref.to_s.gsub(/xref_|\\/, '')}"
      parts = xml.split(regex)
      # reference = FIXME
      parts.each_with_index do |part, i|
        # puts_green part if i.odd?
        # puts_cyan part  if i.even?
      end
      # TODO this needs more work in order to be able to handle multiple references in a single paragraph
      new_xml = []
      new_xml.unshift(parts.first + "</w:t>\n</w:r>")
      new_xml << next_reference(xref)
      new_xml.push('<w:r>\n<w:t xml:space="preserve">' + parts.last)
      new_xml = new_xml.join
      new_xml
    else
      xml
    end
  end
  
  def self.clear_references
    @used_reference_ids = []
  end

  def self.use_reference_id(val)
    @used_reference_ids << val
    val.to_s
  end
  
  def self.next_reference_id
    val = rand(1_000..9_999)
    if @used_reference_ids.include?(val)
      next_reference_id
    else
      use_reference_id(val)
    end
  end
  
  def self.next_reference(reference_val, text = '')
    get_xml('field_Char').gsub('_REFERENCE_', reference_val).gsub('_TEXT_', text)
  end
end