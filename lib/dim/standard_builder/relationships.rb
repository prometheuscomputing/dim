module StandardBuilder
  def self.relationships
    @relationships ||= []
  end
  
  def self.clear_relationships
    @relationships         = nil
    # @next_relationship_id  = nil
    @used_relationship_ids = []
    @media                 = []
  end
  
  def self.collect_relationships
    relationships.join
  end
  
  def self.add_relationship(xml)
    relationships << xml
  end
  
  def self.use_relationship_id(val)
    @used_relationship_ids << val
    val
  end
  
  def self.next_relationship_id
    # @next_relationship_id ||= 1
    # ret = @next_relationship_id
    # @next_relationship_id += 1
    # ret
    val = rand(1_000..9_999)
    if @used_relationship_ids.include?(val)
      next_relationship_id
    else
      use_relationship_id(val)
    end
  end
  
  def self.media
    # This will be a list of Gui_Builder_Profile::File objects
    @media
  end
  
  def self.add_media(file)
    @media ||= []
    @media << file
  end
end