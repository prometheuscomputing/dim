# require 'zip'
require 'docx'
require 'nokogiri'
require 'fileutils'
require 'tempfile'
require 'yaml'

require_relative 'templates'
load relative('templates.rb')
require_relative 'formats'
load relative('formats.rb')
require_relative 'tables'
load relative('tables.rb')
require_relative 'footnotes'
load relative('footnotes.rb')
require_relative 'relationships'
load relative('relationships.rb')
require_relative('references.rb')
load relative('references.rb')

module StandardBuilder
  
  # Meh, this should really be generalized beyond StandardBuilder FIXME
  def self.build(original, replacement, options = {})
    # File.open(StandardBuilder.directory(:replacement_content), 'w') { |f| f.write(Nokogiri::XML(replacement)) }
    # We wanted to get the body and replace just that but Nokogiri is a POS and totally screws things up so instead we are manually building the entire document.xml string and replacing it as such.  The contents of what follows are an adaptation of docx#save.
    docx = Docx::Document.open(original)
    
    # docx.send(:update)
    buffer = Zip::OutputStream.write_buffer do |out|
      # add_images(out) # TODO doesn't work, even after trying a lot
      docx.zip.entries.each do |entry|
        # puts "ZIP: #{entry.name}"
        case entry.name
        when 'word/document.xml'
          out.put_next_entry('word/document.xml')
          out.write(replacement)
        when 'word/footnotes.xml'
          add_footnotes(out, options)
        when 'word/_rels/document.xml.rels'
          add_relationships(out, options)
        else
          out.put_next_entry(entry.name)
          out.write(entry.get_input_stream.read)
        end
      end
    end
    docx.zip.close
    if options[:filename]
      FileUtils.mkdir_p(File.dirname(options[:filename]))
      # File.open(options[:filename], "w") { |f| f.write(buffer.string) }
      f = File.open(options[:filename], "w")
      n = f.path
      f.write(buffer.string)
      f.close
      n
    elsif options[:tempfile]
      f = Tempfile.new
      n = f.path
      f.write(buffer.string)
      f.close
      f.unlink
      n
    else
      # do nothing, nice for testing...
    end
  end
  
  # TODO add footnote_rels stuff
  def self.add_footnotes(out, options = {})
    return unless options[:footnotes_template]
    xml = options[:footnotes_template]
    xml = xml.gsub('_FOOTNOTES_', StandardBuilder.collect_footnotes)
    # puts; puts_yellow xml; puts
    xml = StandardBuilder.minimize(xml)
    out.put_next_entry('word/footnotes.xml')
    out.write(xml)
  end
  
  def self.add_relationships(out, options = {})
    return unless options[:relationships_template]
    xml = options[:relationships_template]
    added_rels = StandardBuilder.collect_relationships
    xml = xml.gsub('_RELATIONSHIPS_', added_rels)
    # puts; puts_cyan added_rels
    xml = StandardBuilder.minimize(xml)
    out.put_next_entry('word/_rels/document.xml.rels')
    out.write(xml)
  end
  
  def self.add_images(output_stream)
    media.each do |image|
      output_stream.put_next_entry("word/media/#{image.filename}")
      output_stream.write(image.data)
    end
  end
  
  def self.minimize(str)
    str.gsub(/^ */,'').gsub(/ *$/,'').gsub(/\n/,'')
  end
  
  # FIXME handle all pPr elements, not just style
  # style_elements can take a string or an array
  def self.paragraph(runs, opts = {})
    properties = paragraph_properties(opts)
    xml  = get_xml('paragraph').gsub('_PROPERTIES_', properties).gsub('_RUNS_', Array(runs).collect{ |r| text_run(r, opts) }.join)
    xref = opts[:xref]
    if xref
      bk_id = next_reference_id
      bk_start = get_xml('bookmark_start').sub('_ID_', bk_id).sub('_REFERENCE_', "_#{xref}")
      bk_end   = get_xml('bookmark_end').sub('_ID_', bk_id)
    else
      bk_start = ''
      bk_end   = ''
    end
    xml.gsub!('_BOOKMARK_START_', bk_start)
    xml.gsub!('_BOOKMARK_END_', bk_end)
    xml = remove_blank_lines(xml)
    # if xref; puts; puts xml; end
    xml
  end
  
  def self.skip_line
    paragraph(nil)
  end
  
  # FIXME this needs to handle properties which should wrap style
  def self.run(content, opts = {})
    # If this is already a run then just return it as is.  One example of when this might happen is when #paragraph receives an array for the runs parameter and that array contains both plain strings as well as runs that have already been built because they are somehow special (e.g. footnote references)
    return content if content =~ /<w:r>/
    get_xml('run').gsub('_PROPERTIES_', run_properties(opts)).gsub('_CONTENT_', content)
  end
  
  def self.superscript_run(content, opts = {})
    opts.merge!({:superscript => true})
    text_run(content, opts)
  end
  
  def self.text_run(txt, opts = {})
    # If this is already a run then just return it as is.  One example of when this might happen is when #paragraph receives an array for the runs parameter and that array contains both plain strings as well as runs that have already been built because they are somehow special (e.g. footnote references)
    if txt =~ /<w:r>/
      return txt
    end
    run(text(txt, opts), opts)
  end
  
  def self.text(txt = '', opts = {})
    attrs = ['']
    if opts[:preserve_space]
      attrs << ' xml:space="preserve"'
    end
    get_xml('text').gsub('_ATTRIBUTES_', attrs.join).gsub('_TEXT_', txt)
  end
  
  def self.paragraph_properties(opts = {})
    return '' if opts.empty?
    properties = ['']
    # HACK this is quick and dirty
    properties << opts[:style] if opts[:style]
    liv = opts[:left_indent]
    if liv
      properties << get_xml('left_indent').gsub('_LEFT_', liv.to_s)
    end
    liv = opts[:hanging_left_indent]
    if liv
      # WARNING Huge assumption that these will need to be replaced w/ the same value
      properties << get_xml('left_indent_hanging').gsub('_LEFT_', liv.to_s).gsub('_HANGING_', liv.to_s)
    end
    spacing_after = opts[:spacing_after]
    if spacing_after
      properties << get_xml('spacing_after').gsub('_SPACING_', spacing_after.to_s)
    end
    get_xml('paragraph_properties').gsub('_PROPERTIES_', properties.join)
  end
  
  def self.run_properties(opts = {})
    return '' if opts.empty?
    get_xml('run_properties').gsub('_PROPERTIES_', _run_properties(opts))
  end
  
  def self._run_properties(opts)
    properties = ['']
    properties += wvals(opts)
    # HACK this is quick and dirty
    properties << get_xml('bold')      if opts[:bold]
    properties << get_xml('underline') if opts[:underline]
    properties << get_xml('superscript') if opts[:superscript]
    properties << get_xml('highlight_yellow') if opts[:highlight_yellow]
    properties.join(" ")
  end
  private_class_method :_run_properties
  
  def self.inject_run_properties(xml, opts)
    xml = xml.gsub(/<w:r>.*?<\/w:r>/m) do |run|
      if run =~ /<w:pPr>.*?<\/w:pPr>/m
        run.gsub(/<w:pPr>/, "<w:pPr>\n#{_run_properties(opts)}\n")
      else
        run.gsub(/<w:r>/, "<w:r>\n#{run_properties(opts)}\n")
      end
    end
    xml
  end
  
  def self.wvals(opts = {})
    return [] unless opts[:wvals]
    opts[:wvals].collect { |tag, val| %Q{<w:#{tag} w:val="#{val}"/>} }#.join("\n")
  end

  def self.remove_blank_lines(str)
    str.gsub(/^\s*$\n/, '')
  end
end

=begin
# from https://github.com/rubyzip/rubyzip/blob/05916bf89181e1955118fd3ea059f18acac28cc8/samples/example_recursive.rb
class ZipFileGenerator
  # Zip.on_exists_proc = true
  # Initialize with the directory to zip and the location of the output archive.
  def initialize(input_dir, output_file)
    @input_dir = input_dir
    @output_file = output_file
  end

  # Zip the input directory.
  def write
    entries = Dir.entries(@input_dir) - %w(. ..)
    # io = Zip::File.open(@output_file, Zip::File::CREATE);
    # write_entries(entries, "", io)
    # io.close()
    ::Zip::File.open(@output_file, ::Zip::File::CREATE) do |io|
      write_entries entries, '', io
    end
  end

  private

  # A helper method to make the recursion work.
  def write_entries(entries, path, io)
    entries.each do |e|
      zip_file_path = path == '' ? e : File.join(path, e)
      disk_file_path = File.join(@input_dir, zip_file_path)
      puts "Deflating #{disk_file_path}"

      if File.directory? disk_file_path
        recursively_deflate_directory(disk_file_path, io, zip_file_path)
      else
        put_into_archive(disk_file_path, io, zip_file_path)
      end
    end
  end

  def recursively_deflate_directory(disk_file_path, io, zip_file_path)
    io.mkdir zip_file_path
    subdir = Dir.entries(disk_file_path) - %w(. ..)
    write_entries subdir, zip_file_path, io
  end

  def put_into_archive(disk_file_path, io, zip_file_path)
    io.get_output_stream(zip_file_path) do |f|
      f.puts(File.open(disk_file_path, 'rb').read)
    end
  end
end
=end

