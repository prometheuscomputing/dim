module StandardBuilder
   
  def self.table(headers:, rows:, table_format:, **opts)
    xml = get_xml('table')
    xml = xml.gsub('_PROPERTIES_', table_properties(opts))
    xml = xml.gsub('_GRID_', table_grid(table_format))
    xml = xml.gsub('_HEADER_', headers)
    xml = xml.gsub('_ROWS_', rows)
    if @table_footnotes
      xml += @table_footnotes.join
      clear_table_footnotes
    end
    xml
  end

  def self.table_properties(opts = {})
    xml = get_xml('table_properties')
    xml = xml.gsub('_INDENT_', table_indent(opts))
    xml = xml.gsub('_BORDERS_', table_borders(opts))
    xml = xml.gsub('_FIXED_WIDTH_', table_layout_fixed(opts))
  end
  
  # TODO allow for other layouts -- generalize method....
  def self.table_layout_fixed(opts)
    opts[:fixed_layout] ? get_xml('table_layout_fixed') : ''
  end
  
  def self.table_borders(opts = {})
    # at this point the client code is responsible for sending all of the xml for non-default table borders
    opts[:table_borders] ? opts[:table_borders] : get_xml('table_borders')
  end

  def self.table_indent(opts = {})
    opts[:table_indent] ? get_xml('table_indent').gsub('_VAL_', opts[:table_indent].to_s) : ''
  end
  
  def self.table_grid(table_format)
    column_widths = format(table_format)[:column_widths]
    columns       = column_widths.collect{|cw| table_grid_column(cw)}.join
    get_xml('table_grid').gsub('_COLUMNS_', columns)
  end

  def self.table_grid_column(width)
    get_xml('table_grid_column').gsub('_WIDTH_', width.to_s)
  end

  # This really needs to be defined on the specific builder in order to get the right styling for the header.
  # TODO there are more elegant ways of doing this.  Check methods on Array
  def self.table_headers(format)
    names  = format[:names]
    widths = format[:column_widths]
    raise unless names.size == widths.size
    cells = []
    names.each_with_index do |name, i|
      paragraph = paragraph(name)
      cells << table_cell(widths[i], paragraph)
    end
    xml = table_row(cells.join, :header => true)
  end

  def self.table_row(cells, opts = {})
    xml = get_xml('table_row')
    if opts[:header]
      cells = table_row_properties(opts) + cells
    end
    if opts[:cant_split]
      cells = get_xml('table_row_cant_split') + cells
    end
    xml.gsub('_CELLS_', cells)
  end
  
  # Note that the template has <w:cantSplit/> baked into it.
  def self.table_row_properties(opts = {})
    return '' unless opts.any?
    properties = ['']
    if opts[:header]
      properties << get_xml('table_header')
    end
    get_xml('table_row_properties').gsub('_PROPERTIES_', properties.join)
  end

  # FIXME are there opts we need to handle?
  def self.table_cell(width, lines, opts = {})
    get_xml('table_cell').gsub('_WIDTH_', width.to_s).gsub('_LINES_', Array(lines).join)
  end

  def self.max_cell_line_size(width)
    (width - format(:cell_buffer))/format(:twips_adjuster)
  end

  def self.table_footnotes
    @table_footnotes
  end

  def self.clear_table_footnotes
    @table_footnotes = nil
    @table_footnote_symbol = 'a'
  end
  
  # send it the footnote and it returns the symbol that needs to be inserted into the point of reference
  def self.add_table_footnote(content, opts = {})
    runs = Array(content).collect { |c| text_run(c) }
    footnote_symbol_run = superscript_run(@table_footnote_symbol)
    runs.unshift(footnote_symbol_run)
    @table_footnotes ||= []
    @table_footnotes << paragraph(runs, opts)
    @table_footnote_symbol = @table_footnote_symbol.succ # advance it so it is ready for the next note
    footnote_symbol_run
  end
end
