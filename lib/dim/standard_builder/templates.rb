module StandardBuilder
  
  def self.directory(key, val = nil)
    @directories ||= {}
    if val
      @directories[key.to_sym] = val
    else
      @directories[key.to_sym]
    end
  end
  
  directory(:standard, File.expand_path('~/projects/dim/lib/dim/standard'))
  directory(:docx_templates, File.join(directory(:standard), 'docx_templates'))
  
  def self.get_xml(template_name)
    @xml ||= {}
    return @xml[template_name] if @xml[template_name]
    @xml[template_name] = File.read(File.join(directory(:docx_templates), "#{template_name}.xml"))
    @xml[template_name]
  end
  
end