module Standard
  class IEEEStandard
    # TODO obviously this isn't an attribute...SSA needs to be extended to allow actions / behaviors
    # It also isn't possible to set a derived attribute on Sequel::Model because it doesn't have the ssa plugin...
    derived_attribute(:set_tree_view_root, ::NilClass)
    def set_tree_view_root
      Gui::Controller.session[:tree_view_roots] ||= []
      Gui::Controller.session[:tree_view_roots].unshift(self)
      Gui::Controller.session[:tree_view_roots].uniq!
    end
    
    def on_gui_root_load
      set_tree_view_root
    end
    def display_name
      dn = "IEEE Standard"
      dn << " - #{title}" if title && !title.empty?
      dn << " - #{subtitle1}" if subtitle1 && !subtitle1.empty?
      dn << " - #{subtitle2}" if subtitle2 && !subtitle2.empty?
      dn
    end
    
  end
    
  class Clause
    alias_attribute(:content_display, :title)
    # add_attribute_getter(:content_display) { |title| title_paths }
    def display_name
      dn = title
      return dn if dn && !dn.empty?
      nil
    end
    
    def _title_paths
      paths = _parent_titles
      paths.collect { |p| p << "> #{title}" }
    end
    
    def title_paths
      _parent_titles.join("\n")
    end
    
    def _parent_titles
      paths = (ieee_standards.collect(&:title) + containers.collect(&:_parent_titles)).compact
      if paths.any?
        paths.collect { |p| p << "> #{title}" }
      else
        ["#{title}"]
      end
    end
    
    def parent_titles
      (ieee_standards.collect(&:title) + containers_titles = containers.collect(&:_parent_titles)).join("\n")
    end
  end
  
  class Abbreviation
    def display_name
      dn = symbol
      return dn if dn && !dn.empty?
      nil
    end
  end
  class NormativeReference
    def display_name
      dn = source
      return dn if dn && !dn.empty?
      nil
    end
  end
  class Definition
    def display_name
      dn = term
      return dn if dn && !dn.empty?
      nil
    end
  end
  class AbbreviationSection
    alias_attribute(:content_display, :title)
    add_attribute_getter(:content_display) do |title|
      title || "Abbreviations and acronyms"
    end
    def display_name; title; end
  end
  class DefinitionSection
    alias_attribute(:content_display, :title)
    add_attribute_getter(:content_display) do |title|
      title || "Definitions"
    end
    def display_name; title; end
  end
  class NormativeReferenceSection
    alias_attribute(:content_display, :title)
    add_attribute_getter(:content_display) do |title|
      title || "Normative References"
    end
    def display_name; title; end
  end

  Standard::Content.implementors.each do |klass|
    klass.class_eval do
      derived_attribute(:parent_titles, ::String)
      def parent_titles
        _parent_titles.join("<br>")
      end
      def _parent_titles
        containers.collect(&:_parent_titles)
      end
    end
  end

  class Note
    alias_column(:content_display, :content_content, String)
    add_attribute_getter(:content_display) { |c| safe_content_display(c) }
    def display_name
      dn = ('note - ' + content&.safe_content)
    end
  end
  class Example
    alias_column(:content_display, :content_content, String)
    add_attribute_getter(:content_display) { |c| safe_content_display(c) }
    def display_name
      dn = ('example - ' + content&.safe_content[0..99])
    end
  end
  class Code
    alias_attribute(:content_display, :title)
    def display_name
      dn = ('code - ' + title)
    end
  end
  class Figure
    alias_attribute(:content_display, :caption)
    def display_name
      dn = ('figure - ' + caption)
    end
  end
  class Table
    alias_attribute(:content_display, :title)
    def display_name
      dn = ('table - ' + title)
    end
  end
  class List
    alias_attribute(:content_display, :title)
    def display_name
      dn = ('list - ' + title)
    end
  end
  class Text
    def safe_content_display(str)
      str  ||= ''
      html ||= str.include?("<p>")
      (html ? Nokogiri::HTML(str).text : str)[0..99]
    end
    alias_column(:content_display, :content_content, String)
    add_attribute_getter(:content_display) { |c| safe_content_display(c) }
    def display_name
      dn = ('text - ' + content&.safe_content)
    end
  end
  class Footnote
    def display_name
      dn = ('footnote - ' + content&.safe_content)
    end
  end
end