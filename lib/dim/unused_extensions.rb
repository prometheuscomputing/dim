# Not used at all.  A nice learning exercise but a waste of time that is acheived by requiring json_graph/sequel_marshalling

# Get All Classes
def get_classes_from_module a_module
  classes_array = []
  
  class_symbols = a_module.constants.select {|c| Class === a_module.const_get(c)}
  classes_array << class_symbols.collect{|sym| (a_module.to_s + "::" + sym.to_s).to_const} if class_symbols.any?
  
  child_module_symbols = a_module.constants.select {|c| Module === a_module.const_get(c) && !(Class === a_module.const_get(c))}
  child_modules = child_module_symbols.collect{|sym| (a_module.to_s + "::" + sym.to_s).to_const}
  classes_array << child_modules.collect{|cm| get_classes_from_module(cm)}
  
  classes_array.flatten!
  classes_array.uniq!
  classes_array
end

json_modules = [Nomenclature, MyDevice, DIM, MetaInfo, PHD]
json_classes = json_modules.collect {|rm| get_classes_from_module(rm)}.flatten.uniq
json_classes.each{|klass| klass.send(:include, JSON_Graph::Serializeable)}