module DIM
  def self.asn1_classifiers
    klass_instances = []
    MetaInfo::Classifier.children.each { |child_class| next unless DB.table_exists?(child_class.table_name); klass_instances += child_class.where(:is_asn1 => true).all }
  end
  
  def self.generate_asn1
    asn1s = MetaInfo::Class.where(:is_asn1 => true).all + MetaInfo::Enumeration.where(:is_asn1 => true).all  + MetaInfo::Interface.where(:is_asn1 => true).all
    # asn1s = MetaInfo::Class.where(:name => 'CHOICE').first.sub_classifiers
    asn1_classifiers.sort_by! { |x| x.name }
    dir  = File.expand_path("~/projects/dim_reference")
    fn   = 'DIM_generated.asn1'
    path = File.join(dir, fn)
    File.open(path, "w") do |output_file|
      asn1s.each do |k|
        output_file.puts k.to_definition
        output_file.puts
      end
    end
    nil
  end
  
  def self.generate_pseudocode
    dim  = MetaInfo::Package.where(:name => 'DIM').first
    top  = MetaInfo::Package.where(:name => 'Top').first
    pkgs = dim.child_packages.reject { |p| p.name == 'CommonDataTypes' }
    pkgs.unshift(pkgs.delete(top))
    dir  = File.expand_path("~/projects/dim_reference")
    fn   = 'DIM_class_definitions_generated.txt'
    path = File.join(dir, fn)
    File.open(path, "w") do |output_file|
      pkgs.each do |pkg|
        output_file.puts pkg.to_definition
        output_file.puts
      end
    end
    nil
  end  
end
