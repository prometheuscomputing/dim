# load 'lib/dim/word_generation.rb'; system 'clear'; s = Standard::IEEEStandard.first; s.build

require_relative 'standard_builder/ieee'
load relative('standard_builder/ieee.rb')
require_relative 'standard/formatting_help/word_size'
load relative('standard/formatting_help/word_size.rb')

# Dir.glob(File.join(relative('docx_extensions'), '**', '*.rb'), &method(:require))
Dir.glob(File.join(relative('docx_extensions'), '**', '*.rb'), &method(:load))


def wordtest(arg = nil)
  load File.expand_path(__FILE__)
  s = arg ? Standard::IEEEStandard[arg] : Standard::IEEEStandard.last
  s.open_docx
end
def docxit(arg = nil); wordtest(arg); end

module Htmltoword
  class Document      
    def self.snippet(content, opts)
      # puts "*"*10
      # puts content.inspect#.pretty_inspect
      # puts "/"*10
      content_with_breaks = ''
      counter = 0
      content.each_line do |line|
        if line.strip.empty?
          content_with_breaks << '<br>' if counter == 0
        else
          content_with_breaks << line.chomp
        end
        counter += 1
      end
      # puts content_with_breaks.inspect
      # puts "/"*10
      
      source = Nokogiri::HTML(content_with_breaks)
      apply_xslt(source, numbering_xslt)
      apply_xslt(source, relations_xslt)
      cleaned_source = Nokogiri::XSLT(File.open(File.join(Htmltoword.config.default_xslt_path, 'inline_elements.xslt'))).transform(source)
      transformed = apply_xslt(cleaned_source, xslt_template(nil))

      # puts transformed
      # puts "*"*10
      
      transformed     
    end
    
    def self.apply_xslt(source, xslt)
      xslt    = Nokogiri::XSLT(File.open(xslt))
      content = xslt.apply_to(source)
      content.gsub!(/\s*xmlns:(\w+)="(.*?)\s*"/,'')
      content
    end
  end
end
