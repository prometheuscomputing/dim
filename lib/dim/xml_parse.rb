require 'active_support/core_ext/hash'
require 'json'
require_relative 'rch_tags'

#*PHD*
# module PHD
#   class MDS
#     alias_method :system_type_term,  :type_term
#     alias_method :system_type_term=, :type_term=
#   end
# end

module MyDevice
  class DeviceProfileBuilder
    attr_reader :file, :submitted_name, :submitted_type, :device_type, :xml_format, :json, :profile_root_keys
    def initialize(file, opts)
      @file           = file
      @submitted_name = opts[:submitted_name]
      @submitted_type = opts[:submitted_type]
      @device_type    = opts[:device_type]
      @xml_format     = opts[:xml_format]
      begin
        @json = Hash.from_xml(file.read)
      rescue REXML::ParseException => e
        # The line number and position that comes back in the message does not seem to correspond well to the original XML so we remove it
        useful_message = e.message.gsub(/Line: \d+\nPosition: \d+\nLast 80 unconsumed characters:/, 'near: ')
        return {:error => useful_message}
      end
    end
    
    # FIXME this belongs in a more generalized place
    def find_first(klass, params = {})
      if klass.interface?
        klasses = klass.implementors
      else
        klasses = klass.children.unshift(klass)
      end
      klasses.reject{|k| k.abstract?}.each do |k|
        found = k.where(params).first
        return found if found
      end
      nil
    end
    
    def get_type_from_node(node, prefix = 'DIM')
      if xsi_type = node["xsi:type"]
        str = xsi_type.split(/:+/).unshift(prefix).join('::')
        # puts str
        str.to_const
      end
    end
    
    def get_device_with_metadata
      case xml_format
      when :full
        wrapper = json["PCDProfile"] || json["PHDProfile"]
      when :rch
        device = json["RCH"]
        return {:error => "XML problem: The device must be contained within the <RCH/> element."} unless device
        device# pp json;puts "%"*30
      else
        raise 'xml format specified is unknown or unspecified'
      end
    end
    
    def get_device
      {@profile_root_key => get_device_with_metadata[@profile_root_key]}
    end
    
    def get_element_klass(tag, contents)
      case xml_format
      when :rch
        klass = lookup_class_by_tag(tag)
        error_message = "Can't find a class for tag: #{tag}"
      when :full
        klass = get_type_from_node(contents, 'DIM')
        error_message = "Can't find a class associated with tag: #{tag}. It should have a valid value for the 'xsi:type' attribute."
      else
        raise 'unknown xml format'
      end
      unless klass
        return {:error => error_message}
        # FIXME exactly how to handle bad XML is a bit up in the air...once it is hammered out this whole file probably needs to be cleaned up
        results[:warnings] << "Could not parse unknown tag &lt#{tag}&gt"
      end
      {:klass => klass}
    end
    
    def _ignored_keys
      []
    end
    
    def set_profile_root_keys
      case xml_format
      when :rch
        profile_root_keys = get_device_with_metadata.keys.select{|tag| lookup_class_by_tag(tag)}
      when :full
        # pp device;puts "%"*30
        profile_root = get_device_with_metadata.select do |tag, contents|
          get_type_from_node(contents, 'DIM')
        end
        profile_root_keys = profile_root.keys
      else
        raise 'This was unexpected'
      end
      # puts "profile_root_keys are #{profile_root_keys.inspect}"
      return {:error => "Too many tags left to have just one profile_root -- (#{profile_root_keys})"} if profile_root_keys.count > 1
      return {:error => "No valid tag for profile root"} if profile_root_keys.count < 1
      @profile_root_key = profile_root_keys.first
    end
    
    def _start_profile
      ChangeTracker.start unless ChangeTracker.started?
      profile = self.class::PROFILE_CLASS.new
      profile.name         = submitted_name
      profile.intended_use = submitted_type
      case xml_format
      when :full
        data = json[profile.class.name.demodulize] || {}
      when :rch
        data = json["RCH"] || {}
      end
      ChangeTracker.commit; ChangeTracker.start
      # name        = device.delete('name')
      # keys_to_delete = []
      data.each do |k,v|
        next if k == @profile_root_key
        next if _ignored_keys.include?(k)
        # puts k;puts v;puts
        # keys_to_delete << k
      end
      profile
      # keys_to_delete.each{|k| device.delete(k)}
    end
    
    def build
      set_profile_root_keys
      device = get_device

      profile  = _start_profile
      begin
        result   = case xml_format
                   when :rch
                     build_profile_from_rch(profile, device)
                   when :full
                     build_profile_from_full_xml(profile, device['profile_root'], 'profile_root')
                   end
      rescue Exception => e
        result = {:error => "Unhandled error in XML parsing.\nPlease contact developer with this information:\n#{e.message}\n#{e.backtrace[0..4].pretty_inspect}"}
        puts e.message
        puts e.backtrace
      end
      puts result.inspect
      warnings = result[:warnings]
      error    = result[:error]
      if error
        ChangeTracker.start
        profile.destroy
        ChangeTracker.commit
        return {:error => error}
      end
      ChangeTracker.start unless ChangeTracker.started?
      profile.source_xml = Gui_Builder_Profile::File.instantiate(:filename => "#{submitted_name}.xml", :data => ::File.binread(file), :mime_type => 'text/xml')
      profile.save
      ChangeTracker.commit if ChangeTracker.started?
      {:profile => profile, :warnings => warnings.uniq}
    end
    
    def build_profile_from_rch(container_node, contained_elements, results = {:warnings => []})
      # puts "\nContainer Node: #{container_node.inspect}"
      # FIXME this needs generalized error handling so that the page doesn't just go sour.  Example, if there is garbage in the xml then contained_elements can be a String which, of course, does not respond to #each.  Maybe there should be a specific error if contained_elements is a String as well as general/broad scope error handling.
      contained_elements.each do |tag,value|
        # puts "  <#{tag}>"
        # puts "    #{value.inspect}"
        # puts
        # FIXME this is a quick patch and needs to be replaced with generalized capability for more attributes
        if tag == 'card' || tag == 'cardinality'
          container_node.cardinality = value
          next
        end
        
        element_klass_result = get_element_klass(tag, value)
        if element_klass_result[:error]
          return element_klass_result
          # FIXME exactly how to handle bad XML is a bit up in the air...once it is hammered out this whole file probably needs to be cleaned up
          results[:warnings] << "Could not parse unknown tag &lt#{tag}&gt"
          next
        end
        element_klass = element_klass_result[:klass]
        # find the appropriate_getter.  we really hope there is only one!!
        correct_property = container_node.class.properties.find do |getter, property_info|
          # FIXME and another one... calling #derived_attribute(should also instantiate the <attribute>_type method.  It does not.  Also check to see if there are any other methods that should get created in order to facilitate iteration of a classes properties.)
          # FIXME the check for :complex_attribute is a workaround for a weird thing in SSA.  Revise this once that is fixed.
          next if property_info[:derived] || (property_info[:type] == :complex_attribute) # assuming that this does not 'find' anything
          type_getter = (getter.to_s + "_type").to_sym
          # puts "type_getter: #{type_getter}"
          # FIXME we call Array() because some type_getters, e.g. #cardinality_type, are not returning an array but instead just a single class.  WHY IS THAT HAPPENING?  I don't think it should be happening.
          types = Array(container_node.send(type_getter))
          # puts "Checking #{container_node.class}.#{getter}_type for #{element_klass} -- #{types.inspect}"
          # puts types.inspect
          # puts_red property_info if types.include?(element_klass)
          types.include?(element_klass)
        end
    
        if correct_property
          adder = (correct_property.first.to_s + "_add").to_sym
        else
          puts "Can't find a property/getter to add a #{element_klass} to a #{container_node.class}"
          message = "The DIM does not allow you to add a #{element_klass.to_s.demodulize} to a #{container_node.class.to_s.demodulize}.  Please review your device structure."
          return {:error => message}
          next # FIXME need to do something more useful than just skipping this.  raise or log error or message.
        end
    
        values = []
        if value.is_a?(Hash)
          values << value
        elsif value.is_a?(Array)
          values += value
        else
          raise "value was a #{value.class} -- #{value.inspect}"
        end
        values.each do |value|
          # puts element_klass
          # pp value
          ChangeTracker.start
          element_node = element_klass.new
          element_node.save
          ChangeTracker.commit
          type_getter = (element_node.respond_to?(:system_type) ? "system_type" : "type").to_sym
          type_setter = (type_getter.to_s + "=").to_sym
          term_setter = (type_getter.to_s + "_term=").to_sym
          term        = nil
          refid       = value.delete("refid")
          code        = value.delete("code")
          if refid && !refid.empty?
            term = Nomenclature.find_term_by_ref_id(refid)
          elsif code
            term = Nomenclature.find_term_by_cf_code(code)
            puts "Code: #{code.inspect} -- #{term.inspect}"
          else
            results[:warnings] << "No refid or code provided for &lt#{tag}&gt #{value}"
          end
          # CLEAR_KEYS.each{|key| value.delete key}
          # TODO if the refid was bogus but the code was good then we should let the user know!
          if term
            partition = get_partition(term.cf_term_code)
            oid       = get_code10(term.cf_term_code)
            ChangeTracker.start
            type           = type_class.new
            type.code      = oid
            type.partition = nom_partition_class.where(Sequel.ilike(:value, "%(#{partition})")).first
            type.save
            element_node.send(type_setter, type)
            element_node.send(term_setter, term)
            element_node.save
            ChangeTracker.commit
          elsif refid
            if term = Nomenclature.find_proposed_term_by_ref_id(refid)
              results[:warnings] << "#{refid} is not available in the Device Profiling Tool. An existing Proposed Term was used"
            else
              term = Nomenclature::ProposedType.new(:reference_id => refid)
              term.note_creation
              results[:warnings] << "#{refid} is not available in the Device Profiling Tool. A new Proposed Term was created"
            end
            ChangeTracker.start
            element_node.proposed_type = term
            element_node.save
            ChangeTracker.commit
          end

            
          # Populate Allowed Units / Enums
          # rtmms_ok means legal according to RTMMS
          # proposed means the refid does not map to any term in RTMMS
          if element_node.respond_to?(:allowed_enumerations)
            rtmms_ok_contraints  = term.respond_to?(:collected_literals) ? term.collected_literals : []
            contraints           = []
            proposed_constraints = []
            if e = value.delete("e")
              e.split(/ +/).each do |refid_or_token|
                if found = Nomenclature.find_term_by_ref_id(refid_or_token) || Nomenclature.find_token(refid_or_token)
                  contraints << found
                elsif found = Nomenclature::ProposedLiteral.where(:reference_id => refid_or_token).first
                  results[:warnings] << "#{refid_or_token} is not available in the Device Profiling Tool. An existing Proposed Enumeration Value was used for this #{refid}."
                  proposed_constraints << found
                else
                  ChangeTracker.start
                  proposed_literal = Nomenclature::ProposedLiteral.new(:reference_id => refid_or_token, :proposed_on => Date.today.to_s, :proposed_by => 'fixme')
                  term.note_creation(proposed_literal)
                  proposed_literal.save
                  results[:warnings] << "#{refid_or_token} is not available in the Device Profiling Tool. A new Proposed Enumeration Value was created for this #{refid}."
                  ChangeTracker.commit
                  proposed_constraints << proposed_literal
                end
              end
            else
              contraints = rtmms_ok_contraints
            end
            if contraints.any? || proposed_constraints.any?
              ChangeTracker.start
              element_node.allowed_enumerations = contraints           if contraints.any?
              element_node.proposed_enums       = proposed_constraints if proposed_constraints.any?
              element_node.save
              if term.is_a?(Nomenclature::ProposedType)
                term.enums          = contraints           if contraints.any?
                term.proposed_enums = proposed_constraints if proposed_constraints.any?
                term.save
              end
              ChangeTracker.commit
            end
          else
            results[:error] = "You can not use the @e attribute for an #{element_node.class.to_s.demodulize}" if value.delete("e")
          end

          if element_node.respond_to?(:allowed_units)
            rtmms_ok_contraints  = term.respond_to?(:collected_units) ? term.collected_units : []
            contraints           = []
            proposed_constraints = []
            if u = value.delete("u")
              u.split(/ +/).each do |unit_refid|
                if found = Nomenclature.find_term_by_ref_id(unit_refid)
                  contraints << found
                elsif found = Nomenclature::ProposedUnit.where(:reference_id => unit_refid).first
                  proposed_constraints << found
                  results[:warnings] << "#{unit_refid} is not available in the Device Profiling Tool. An existing Proposed Unit was used for this #{refid}."
                else
                  ChangeTracker.start
                  proposed_unit = Nomenclature::ProposedUnit.new(:reference_id => unit_refid, :proposed_on => Date.today.to_s, :proposed_by => 'fixme')
                  # FIXME the commented out line was obviously wrong.  I think the line below is what it should have been.  Just make sure that that is correct.
                  # term.note_creation(proposed_unit)
                  proposed_unit.note_creation
                  proposed_unit.save
                  results[:warnings] << "#{unit_refid} is not available in the Device Profiling Tool. A new Proposed Unit was created for this #{refid}."
                  ChangeTracker.commit
                  proposed_constraints << proposed_unit
                end
              end
            else
              contraints = rtmms_ok_contraints
            end
            if contraints.any? || proposed_constraints.any?
              ChangeTracker.start
              element_node.allowed_units  = contraints          if contraints.any?
              element_node.proposed_units = proposed_constraints if proposed_constraints.any?
              element_node.save
              if term.is_a?(Nomenclature::ProposedType)
                term.units          = contraints           if contraints.any?
                term.proposed_units = proposed_constraints if proposed_constraints.any?
                term.save
              end
              ChangeTracker.commit
            end
          else
            results[:error] = "You can not use the @u attribute for an #{element_node.class.to_s.demodulize}" if value.delete("u")
          end

          # puts "NO TYPE" unless element_node.send(type_getter)
          ChangeTracker.start
          # puts "Adding a #{element_node.class} to a #{container_node.class} with #{adder}"
          container_node.send(adder, element_node)
          container_node.save
          ChangeTracker.commit
          # puts "*"*5 + " #{element_klass} -- #{refid}"
          results = build_profile_from_rch(element_node, value, results)
          return results if results[:error] # recursively exits. i.e. climbs back up the stack with the error.
        end
      end
      results
    end
    
    def get_klass_from_xsi_type(xsi_type_value)
      return nil unless xsi_type_value
      domain = case device_type
               when :phd; 'PHD'
               when :pcd; 'DIM'
               else; raise 'Unknown device type'
               end
      # FIXME find a better way!  This is an ugly hack.  Done because of the way that json_graph creates namespaces.  Is it possible that json_graph is doing a bad job of creating namespaces? Yes, probably.  It is probably creating the first ASN1 namespace just fine (the one inside CommonDataTypes) and then hyphenating the rest of them because there is already an 'ASN1' namespace.  If they are siblings then maybe they should all be namespaced as such.
      str = xsi_type_value.split(/:+|-/).join('::')
      if str =~ /^ASN1/
        ('IEEE20101::' + str).to_const
      else
        (domain + '::' + str).to_const
      end
    end
    
    def build_profile_from_full_xml(container_node, contents, role, results = {:warnings => []})
      info = container_node.class.info_for role.to_sym
      raise "No info found for #{container_node.class}##{role.to_sym}" unless info
      to_many = info[:type] =~ /to_many/
      contents = [contents] if contents.is_a?(Hash)
      # raise "no value for #{role} found" unless role_fulfillers
      contents.each do |rf|
        # pp rf
        element_klass = get_klass_from_xsi_type(rf["xsi:type"])
        # FIXME this is a bit on the strict side.  Other option is to just skip it and warn.
        return {:error => "Invalid or missing type for the #{role} element in a #{container_node.class}"} unless element_klass
        ChangeTracker.start unless ChangeTracker.started?
        element = element_klass.new
        ChangeTracker.commit
        rf.delete("xsi:type")
        rf.each do |property_key, property_values|
          element_info = element_klass.info_for property_key
          # FIXME this next bit needs to be more intelligent if possible
          unless element_info
            element_info = element_klass.info_for (property_key.to_s + '_dim').to_sym
          end
          unless element_info
            message = "Skipping #{property_key} element(s).  &lt#{property_key}&gt is an invalid tag.  Contact developer for assistance."
            puts message
            puts "No info for #{element_klass}.#{property_key} among #{element_klass.properties.keys}"
            pp rf;puts
            results[:warnings] << message
          end
          if property_values.is_a?(Hash)
            build_profile_from_full_xml(element, property_values, property_key, results)
          elsif property_values.is_a?(Array)
            property_values.each{|property_value| build_profile_from_full_xml(element, property_value, property_key, results)}
          else
            value        = property_values
            getter       = element_info[:getter]
            attr_to_many = element_info[:type] =~ /to_many/
            setter       = (getter.to_s + '=').to_sym
            value_type   = (element_info[:wraps] || element_info[:class]).to_const
            
            # FIXME handle proposed types
            if (value_type <= DIM::CommonDataTypes::ASN1::OID_Type || value_type <= DIM::CommonDataTypes::ASN1::TYPE)
              proxy_getter = element_info.dig(:additional_data, :prometheus, "proxy_property")&.first.to_s.gsub('-','_').to_sym
              unless proxy_getter
                # next if value_type == DIM::Communication::ASN1::CC_Oid # FIXME make sure next version supports CC-OID proxies
                puts "No proxy for #{element_klass}.#{getter}(#{value_type})"
                message = "XML parsing error!  Contact developer with this information:\nNo proxy for #{element_klass}.#{getter}(#{value_type}) \n#{element_info.pretty_inspect}"
                puts message
                results[:error] = message
                return results
              end
              # puts "#{element_klass} Element INFO"; pp element_info; puts
              # puts "#{element_klass} Proxy Element INFO"; pp element_klass.info_for(proxy_getter); puts
              refids = value.split(/\s+/).collect{|val| val.gsub(/::.*/, '')}
              values = refids.collect do |refid|
                term = find_first(Nomenclature::RTMMSTerm, {:reference_id => refid})
                results[:warnings] << "Could not find refid matching #{refid}.  Term skipped" unless term
                term
              end.compact
              next if values.empty?
              setter = (proxy_getter.to_s + '=').to_sym
              if attr_to_many
                value = values
                # setter = (proxy_getter.to_s + '_add').to_sym
              else
                value = values.first
                # setter = (proxy_getter.to_s + '=').to_sym
              end
            elsif value_type <= Numeric
              if attr_to_many
                value = value.split(/\s+/).collect{|v| v.to_i}
              else
                value = value.to_i
              end
            elsif value_type <= String
              if attr_to_many
                value = value.split(/\s+/)
              end
            elsif value_type.enumeration?
              # puts "#{value_type} is an enum"
              # puts "  values: #{value}"
              enum_values = value.split(/\s+/).collect do |val|
                enum = value_type.where(:value => val).first
                unless enum
                  results[:warnings] << "Could not find '#{val}' among enumeration literals for #{value_type}"
                  puts "Could not find an enum for #{val} in:\n#{value_type.all.pretty_inspect}\n\n"
                end
                enum
              end.compact
              # puts "  matching values: #{enum_values.pretty_inspect}"
              if attr_to_many
                setter = (getter.to_s + '_add').to_sym
                # puts "#{element.class}.#{setter} #{value}(value.class)"
                enum_values.each do |value|
                  ChangeTracker.start unless ChangeTracker.started?
                  element.send(setter, value)
                  element.save
                  ChangeTracker.commit
                end
              else
                # puts "#{element.class}.#{setter} #{value}(value.class)"
                ChangeTracker.start unless ChangeTracker.started?
                element.send(setter, enum_values.first)
                element.save
                ChangeTracker.commit
              end
              # puts "  **** tried to set #{setter} to #{enum_values.pretty_inspect}"
              next # bail out here and go to next tag/value
            else
              raise "#{element_klass}.#{getter} is #{property_values.inspect} -- #{element_info[:class]}"
            end
            # puts "#{element.class}.#{setter} #{value}(value.class)"
            ChangeTracker.start unless ChangeTracker.started?
            begin
              element.send(setter, value) if value
            rescue StandardError => e
              error_info    = "\n#{e.message} \n#{e.backtrace[0..4].pretty_inspect}"
              error_message = "XML parsing error!  Contact developer with this information:\n#{element.class}.#{setter} #{value.inspect}(#{value.class}) \n#{element.class.info_for(getter).pretty_inspect}"
              puts error_message
              puts e.message
              puts e.backtrace
              results[:error] = error_message + error_info
              return results
            end
            # puts "SAVING #{element.class}.#{setter}"
            element.save
            ChangeTracker.commit
          end
        end
        return results if results[:error]
        begin
          ChangeTracker.start unless ChangeTracker.started?
          setter = to_many ? (info[:getter].to_s + '_add').to_sym : (info[:getter].to_s + '=').to_sym
          container_node.send(setter, element)
          ChangeTracker.commit
          # FIXME This has to be done...but I have no idea why it isn't being done correctly during the first commit.
          if MyDevice::ALL_DIM_CLASSES.include?(container_node.class)
            ChangeTracker.start
            container_node.set_used_attributes
            container_node.save
            ChangeTracker.commit
          end
        rescue StandardError => e
          error_message = "XML parsing error!  Contact developer with this information:\n#{container_node.class}.#{setter} #{element.inspect} \n#{container_node.class.info_for(info[:getter]).pretty_inspect}"
          error_info = "\n#{e.message} \n#{e.backtrace[0..4].pretty_inspect}"
          puts error_message
          puts e.message
          puts e.backtrace
          results[:error] = error_message + error_info
          return results
        end
      end
      results
    end
    
    def deconstruct_cf_code10(cf_code10)
      binary_string = "%b" % cf_code10.to_i
      # this returns nil if the cfcode10 results in a binary string of less than 16 chars
      least_significant_16 = binary_string[-16..-1] || ""
      # Oddly enough, this will return empty string even if there are only 16 or fewer chars in the string
      most_significant_16  = binary_string[0..-17]
      {:partition => most_significant_16.to_i(2), :oid => least_significant_16.to_i(2)}
    end

    def get_partition(cf_code10)
      deconstruct_cf_code10(cf_code10)[:partition]
      # binary_string = "%b" % cf_code10.to_i
      # most_significant_16  = binary_string[0..-17]
      # most_significant_16.to_i(2)
    end

    def get_code10(cf_code10)
      deconstruct_cf_code10(cf_code10)[:oid]
      # binary_string = "%b" % cf_code10.to_i
      # least_significant_16 = binary_string[-16..-1]
      # least_significant_16.to_i(2)
    end
    
    def lookup_class_by_tag(tag)
      self.class::LOOKUP_CLASS_BY_TAG[tag]
    end
    
    def type_class
      self.class::TYPE_CLASS
    end
    
    def nom_partition_class
      self.class::NomPartition_CLASS
    end

  end
  #*PHD*
  # class PHDProfileBuilder < DeviceProfileBuilder
  #   VALID_CLASSES = PHD.classes(:no_imports => true).select{|klass| klass < Sequel::Model && klass.to_s =~ /PHD/}.reject{|klass| (klass.to_s =~ /ASN1|_Association/) || klass.join_class? || klass.interface? || klass.abstract?}
  #   LOOKUP_TAG_BY_CLASS = {}
  #   VALID_CLASSES.each{|c| LOOKUP_TAG_BY_CLASS[c] = ([c.name.demodulize.downcase] + Array(ALIASES[c]).uniq)}
  #   LOOKUP_CLASS_BY_TAG = {}
  #   LOOKUP_TAG_BY_CLASS.each {|klass,tags| tags.each{|tag| LOOKUP_CLASS_BY_TAG[tag] = klass}}
  #   PROFILE_CLASS = MyDevice::PHDProfile
  #   TYPE_CLASS         = PHD::ASN1::CommonDataTypes::TYPE
  #   NomPartition_CLASS = PHD::ASN1::CommonDataTypes::NomPartition
  # end
  class PCDProfileBuilder < DeviceProfileBuilder
    # CLEAR_KEYS = ['code', 'refid']
    # VALID_CLASSES = DIM.classes(:no_imports => true).reject{|klass| (klass.to_s =~ /ASN1|_Association/) || klass.join_class? || klass.interface? || klass.abstract?}
    # FIXME the above should work fine but I am having version skew problems on the server so I'm adding this crap as a ham-fisted work around for now.
    VALID_CLASSES = DIM.classes(:no_imports => true).select{|klass| klass < Sequel::Model && klass.to_s =~ /DIM/}
    VALID_CLASSES.reject! do |klass|
      # (klass.to_s =~ /ASN1|_Association|SingleBed|MultipleBed/) ||
      (klass.to_s =~ /ASN1|_Association/) ||
      klass.join_class? || 
      klass.interface? || 
      klass.abstract?
    end
    LOOKUP_TAG_BY_CLASS = {}
    VALID_CLASSES.each{|c| LOOKUP_TAG_BY_CLASS[c] = ([c.name.demodulize.downcase] + Array(ALIASES[c]).uniq)}
    LOOKUP_CLASS_BY_TAG = {}
    LOOKUP_TAG_BY_CLASS.each do |klass, tags|
      tags.each { |tag| LOOKUP_CLASS_BY_TAG[tag] = klass }
    end
    PROFILE_CLASS = MyDevice::PCDProfile
    TYPE_CLASS         = DIM::CommonDataTypes::ASN1::TYPE
    NomPartition_CLASS = DIM::CommonDataTypes::ASN1::NomPartition
    IGNORED_KEYS = ['xmlns', 'xmlns:xsi', 'xsi:type', 'profile_root']
        
    def _ignored_keys
      super + IGNORED_KEYS
    end
    
  end # PCDProfile
end # MyDevice
