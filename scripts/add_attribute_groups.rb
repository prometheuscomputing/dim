this_dir = File.expand_path(File.dirname __FILE__)
load File.join(this_dir, 'get_raw_attribute_group_hash.rb')
raw_hash = get_raw_attribute_group_hash

raw_hash.each do |klass_string, attr_hash|
  # puts "*"*20
  # puts klass_string
  # pp attr_hash
  klass = klass_string.to_const
  metainfo_classifier = klass.metainfo_classifier
  attr_hash.each do |attribute, array|
    # It is possible that the attribute names are capitalized in the metainfo in the database
    property = metainfo_classifier.properties.find{|p| p.name.strip =~ /^#{attribute.strip}$/i}
    raise unless property
    groups_with_classes = {}
    array.each do |g|
      grp, cls = g.split("__")
      if cls.nil? || cls.empty?
        # puts "**#{g} must be for #{klass_string}"
        cls = klass_string.demodulize
      end
      groups_with_classes[grp] ||= []
      cls_name = cls.gsub(/-|_/, "").gsub("Multipatient", "MultiPatient")
      metainfo_cls = MetaInfo::Class.where(:name => cls_name).first
      groups_with_classes[grp] << metainfo_cls if metainfo_cls
      puts "NO metainfo_cls for #{g}" unless metainfo_cls
    end
    # pp groups_with_classes
    groups_with_classes.each do |group, classes|
      # puts "  #{group}"
      # no we have to order the classes in order of ancestral hierarchy...holy crap...or maybe we just have to find the root classes.
      root_classes = []
      attribute_groups_for_classes = []
      classes.each do |c|
        # puts "    #{c.name}"
        # find or create this attribute group for this metainfo class
        attribute_group_for_class = c.attribute_groups.find{|g| g.attribute_group_id == group}
        # puts "      found #{group}" if attribute_group_for_class
        unless attribute_group_for_class
          ChangeTracker.start unless ChangeTracker.started?
          attribute_group_for_class = MetaInfo::Group.new(:attribute_group_id => group)
          c.add_attribute_group(attribute_group_for_class)
          c.save
          # puts "      created #{group}"
          ChangeTracker.commit
        end
        root_classes << c unless classes.include?(c.super_classifier)
      end
      root_classes.each do |c|
        # puts "Root for #{group}: #{c.name}"
        attribute_group_for_class = c.attribute_groups.find{|g| g.attribute_group_id == group}
        corresponding_property = c.properties.find{|p| p.name.strip  =~ /^#{attribute.strip}$/i}
        unless corresponding_property
          puts "**Could not find #{attribute} amongst: #{c.properties.collect{|p| p.name}}"
        end
        unless attribute_group_for_class.properties.include?(corresponding_property)
          ChangeTracker.start unless ChangeTracker.started?
          attribute_group_for_class.properties_add corresponding_property 
          attribute_group_for_class.save
          ChangeTracker.commit
        end
      end
      classes.each do |c|
        attribute_group_for_class = c.attribute_groups.find{|g| g.attribute_group_id == group}
        if sc = c.super_classifier
          if attribute_group_for_superclass = sc.attribute_groups.find{|g| g.attribute_group_id == group}
            ChangeTracker.start unless ChangeTracker.started?
            attribute_group_for_class.parent_group = attribute_group_for_superclass
            attribute_group_for_class.save
            ChangeTracker.commit
          end
        end
      end

    end
    
  end
end

load File.join(this_dir, 'group_id2name.rb')
