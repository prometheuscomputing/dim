MetaInfo::Class.each do |klass|
  next if klass.is_asn1? || klass.is_abstract
  model = klass.ruby_model
  props = klass.all_properties.select{|p| p.is_asn1?}
  model_props = model.properties
  props.each do |p| 
    # Now find the corresponding property in the ssa properties hash
    model_prop = model_props.find{|k,v| v[:getter].to_s == p.name.downcase.gsub('-', '_')}
    if p.name == "Class" && model_prop.nil?
      model_prop = model_props.find{|k,v| v[:getter].to_s == "class_dim"}
    end
    if model_prop
      model_props[model_prop.first][:attribute_label] = p.name
    else
      puts "Could not find property matching #{model} #{p.name} (#{p.name.downcase.gsub('-', '_')}) amongst:"
      puts "  #{model_props.collect{|k,v| v[:getter].to_s}.sort}"
      puts
    end
  end
end