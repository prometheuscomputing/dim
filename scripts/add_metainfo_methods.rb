
def add_methods(metainfo_class, events)
  events.each do |e|
    ChangeTracker.start
    met = MetaInfo::Method.new(:name => e[:name], :action_id => e[:action_id])
    met.mode = e[:mode]
    pt = MetaInfo::Class.where(:name => e[:parameter_type]).first
    met.parameter_type = pt if pt
    unless pt
      puts "No parameter type '#{e[:parameter_type]}' for method #{e[:name]} of #{metainfo_class.name}"
    end
    rt = MetaInfo::Class.where(:name => e[:result_type]).first
    met.result_type = rt if rt
    unless rt
      puts "No result type '#{e[:result_type]}' for method #{e[:name]} of #{metainfo_class.name}"
    end
    metainfo_class.add_defined_method met
    metainfo_class.save
    ChangeTracker.commit
  end
end

pmstoremethods = [
  {
  :name => 'Clear-Segments',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SEG_ CLEAR',
  :parameter_type => 'SegmSelection',
  :result_type => '(empty)'
  },
  {
  :name => 'Get-Segments',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SEG_GET',
  :parameter_type => 'SegmSelection',
  :result_type => 'SegmentAttrList'
  },
  {
  :name => 'Get-Segment-Info',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SEG_GET_ INFO',
  :parameter_type => 'SegmSelection',
  :result_type => 'SegmentInfoList'
  }
]
pmstore = MetaInfo::Class.where(:name => 'PMStore').first  
add_methods(pmstore, pmstoremethods)

mdsmethods = [{:name => 'Mds-Set-Status',
:mode => 'Confirmed',
:action_id => 'MDC_ACT_SET_MDS_ STATE',
:parameter_type => 'MdsSetStateInvoke',
:result_type => 'MdsSetState-Result'
}]
mds = MetaInfo::Class.where(:name => 'MDS').first
add_methods(mds, mdsmethods)


logmethods = [{
  :name => 'Clear-Log',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_CLEAR_ LOG',
  :parameter_type => 'ClearLogRangeInvoke',
  :result_type => 'ClearLogRangeResult'}
]
log = MetaInfo::Class.where(:name => 'Log').first
add_methods(log, logmethods)

eventlogmethods = [{
  :name => 'Get-Event-Log-Entries',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_GET_EVENT_LOG_ENTRIES',
  :parameter_type => 'GetEventLogEntryInvoke',
  :result_type => 'GetEventLogEntryResult'}
]
eventlog = MetaInfo::Class.where(:name => 'EventLog').first
add_methods(log, eventlogmethods)

clockmethods = 
[{
  :name => 'Set-Time',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SET_TIME',
  :parameter_type => 'SetTimeInvoke',
  :result_type => 'None'},
{
  :name => 'Set-Time-Zone',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SET_TIME_ZONE',
  :parameter_type => 'SetTimeZoneInvoke',
  :result_type => 'None'},
{
  :name => 'Set-Leap-Seconds',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SET_LEAP_SECONDS',
  :parameter_type => 'SetLeapSecondsInvoke',
  :result_type => 'None'},
{
  :name => 'Set-Time-ISO',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SET_TIME_ISO',
  :parameter_type => 'AbsoluteTimeISO',
  :result_type => 'None'}
]
clock = MetaInfo::Class.where(:name => 'Clock').first
add_methods(clock, clockmethods)

scomethods = [{
  :name => 'Operation-Invoke',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_SCO_OP_ INVOKE',
  :parameter_type => 'OperationInvoke',
  :result_type => 'OperationInvokeResult'},
{
  :name => 'Get-Ctxt-Help',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_GET_CTXT _HELP',
  :parameter_type => 'CtxtHelpRequest',
  :result_type => 'CtxtHelpResult'}]
sco = MetaInfo::Class.where(:name => 'SCO').first
add_methods(sco, scomethods)

ecsmethods = [{
  :name => 'Refresh-Episodic-Data',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_REFR_EPI_ DATA',
  :parameter_type => 'RefreshObjList',
  :result_type => 'none'}
]
ecs = MetaInfo::Class.where(:name => 'EpiCfgScanner').first
add_methods(ecs, ecsmethods)

csmethods = [{
  :name => 'Refresh-Context',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_REFR_CTXT',
  :parameter_type => 'RefreshObjList',
  :result_type => 'ObjCreateInfo'} #  (scan report no is 0)
]
cs = MetaInfo::Class.where(:name => 'ContextScanner').first
add_methods(cs, csmethods)

osmethods = [{
  :name => 'Refresh-Operation-Context',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_REFR_OP_CTXT',
  :parameter_type => 'RefreshObjList',
  :result_type => 'OpCreateInfo'},  # (scan report no is 0)
{
  :name => 'Refresh-Operation-Attributes',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_REFR_OP_ ATTR',
  :parameter_type => 'RefreshObjList',
  :result_type => '—'}]
os = MetaInfo::Class.where(:name => 'OperatingScanner').first
add_methods(os, osmethods)

pdmethods = [{
  :name => 'Discharge-Patient',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_DISCH_PT',
  :parameter_type => '—',
  :result_type => 'PatDemoState'},
{
  :name => 'Admit-Patient',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_ADMIT_PT',
  :parameter_type => 'AdmitPatInfo',
  :result_type => 'PatDemoState'
},
{
  :name => 'Pre-Admit-Patient',
  :mode => 'Confirmed',
  :action_id => 'MDC_ACT_PRE_ ADMIT_PT',
  :parameter_type => 'AdmitPatInfo',
  :result_type => 'PatDemoState'
}]
pd = MetaInfo::Class.where(:name => 'PatientDemographics').first
add_methods(pd, pdmethods)
