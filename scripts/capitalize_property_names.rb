props = MetaInfo::Property.all
props.each do |p|
  ChangeTracker.start unless ChangeTracker.started?
  p.name = p.name.split("-").collect{|i| i.capitalize}.join("-")
  p.save
  ChangeTracker.commit
end