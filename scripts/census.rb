# Standard.classes(no_imports:true).each { |k| puts "#{k}:  #{k.count}" if !(k.abstract? || k.interface? || k.join_class? || k.enumeration?) && DB.table_exists?(k.table_name) };

out = {}
DIM.classes.select{|c| c.name =~ /DIM/}.sort{|a,b| a.name <=> b.name}.each do |klass|
  out[klass] = {}
  if klass.enumeration?
    out[klass] = {:enumeration_literals => klass.literal_values}
  elsif klass.respond_to? :immediate_properties
    klass.immediate_properties.each do |k,v|
      # out[klass][v[:getter]] = [v[:class], v[:composition], v[:enumeration]] if (v[:class].to_s =~ /DIM/) && (v[:composition] || v[:enumeration])
      out[klass][v[:getter]] = v[:class] if (v[:class].to_s =~ /DIM|IEEE/) && (v[:composition] || v[:enumeration] || (v[:additional_data] && v[:additional_data][:modeled_as] && v[:additional_data][:modeled_as] == :attribute))
      # puts "#{klass}.#{k} -- #{v.reject{|k,v| k == :methods}}"
    end
  else
    out[klass] = :primitive
  end
end
pp out;nil

# Standard.classes(no_imports:true).each { |k| k.delete if DB.table_exists?(k.table_name) && !(k.enumeration?)};