MetaInfo::Property.all do |prop|
  unless prop.containing_classifier
    puts_cyan prop.inspect; puts
    ChangeTracker.start; prop.destroy; ChangeTracker.commit
    next
  end
  unless prop.type
    puts_magenta prop.inspect; puts
    next
  end
  next unless prop.type.is_asn1 && !prop.containing_classifier.is_asn1
  str = prop.attribute_id
  refid = prop.term&.reference_id
  next if str && !str.strip.empty? && refid && !refid.strip.empty? && str == refid
  if str.nil? || str.empty?
    puts_yellow "#{prop.containing_classifier.name}  -- #{prop.name} -- #{prop.attribute_id}"
    next
  end
  found_refid = Nomenclature.find_term_by_ref_id(str)
  next if found_refid
  if found_refid && !prop.term
    ChangeTracker.start
    prop.term == found_refid
    prop.save
    ChangeTracker.commit
    next
  end
  puts_red "#{prop.containing_classifier.name}  -- #{prop.name}: DIM- #{prop.attribute_id}; RTMMS- #{refid}"
end;