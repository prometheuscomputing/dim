term_classes = Nomenclature::RTMMSTerm.children
known_refids = []
duplicate_refids = []
empty_refids = []
term_classes.each do |klass|
  klass.each do |inst|
    r = inst.reference_id
    if r.nil? || r.empty? || r =~ /\s+/
      empty_refids << inst.inspect
    end
    if known_refids.include?(r)
      duplicate_refids << r
    else
      known_refids << r
    end
  end
end
puts_yellow "Duplicates:"
puts duplicate_refids
puts ; puts_red "Problems:"
puts empty_refids


term_classes.each do |k|
  x = k.where(Sequel.ilike(:reference_id, '%MDC_VENT_AWAY_CO2_EXP%')).all
  puts_cyan x.inspect if x.any?
end
