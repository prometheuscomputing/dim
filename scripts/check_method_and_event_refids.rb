MetaInfo::Method.all do |met|
  unless met.containing_classifier
    puts_cyan met.inspect; puts
    # ChangeTracker.start; met.destroy; ChangeTracker.commit
    next
  end
  str   = met.action_id
  if str =~ / /
    ChangeTracker.start
    met.action_id = str.gsub(' ', '')
    met.save
    ChangeTracker.commit
    str = met.action_id
  end
  refid = met.term&.reference_id
  next if str && !str.strip.empty? && refid && !refid.strip.empty? && str == refid
  found_refid = Nomenclature.find_term_by_ref_id(str)
  if found_refid && !met.term
    ChangeTracker.start
    met.term == found_refid
    met.save
    ChangeTracker.commit
    next
  end
  puts_red "#{met.containing_classifier.name} -- #{met.name}: DIM- #{met.action_id}; RTMMS- #{refid}"
end;

MetaInfo::Event.all do |ev|
  unless ev.containing_classifier
    puts_cyan ev.inspect; puts
    # ChangeTracker.start; ev.destroy; ChangeTracker.commit
    next
  end
  str   = ev.event_id
  if str =~ / /
    ChangeTracker.start
    ev.event_id = str.gsub(' ', '')
    ev.save
    ChangeTracker.commit
    str = ev.event_id
  end
  refid = ev.term&.reference_id
  next if str && !str.strip.empty? && refid && !refid.strip.empty? && str == refid
  found_refid = Nomenclature.find_term_by_ref_id(str)
  if found_refid && !ev.term
    ChangeTracker.start
    ev.term == found_refid
    ev.save
    ChangeTracker.commit
    next
  end
  puts_red "#{ev.containing_classifier.name} -- #{ev.name}: DIM- #{ev.event_id}; RTMMS- #{refid}"
end;