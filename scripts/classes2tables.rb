require 'yaml'
def classes2tables version_name=nil
  version_name ||= DimGenerated::VERSION.to_s
  hash = {}
  # Does not get ChangeTracker classes
  modules = [DIM, ICS, Gui_Builder_Profile, MetaInfo, PHD, Standard, Nomenclature, IEEE20101, MyDevice, DIM_Stereotypes]
  modules.each do |mod|
    mod.classes(no_imports:true).each do |klass|
      next unless klass < Sequel::Model
      hash[klass.to_s] = klass.table_name rescue nil
    end;nil
  end;nil
  # pp hash;nil
  File.open(File.expand_path("~/projects/dim_reference/classes2tables_#{version_name}.yaml"), "w+") do |file|
    file.puts YAML::dump(hash)
  end
end

# hash.keys.count
# Sequel::Model.children.select{|c| !hash.keys.include?(c.to_s)}

def construct_table_migration_mapping v1, v2
  source_hash = YAML::load(File.open(File.expand_path("~/Prometheus/data/dim_generated/class_table_map_#{v1}.yaml")))
  target_hash = YAML::load(File.open(File.expand_path("~/Prometheus/data/dim_generated/class_table_map_#{v2}.yaml")))
  source_only = source_hash.keys - target_hash.keys
  target_only = target_hash.keys - source_hash.keys
  puts "source_only: #{source_only.inspect}"
  puts "target_only: #{target_only.inspect}"
  mapping = {}
  source_hash.each do |k,v|
    mapping[k] = [v,target_hash[k]] if target_hash[k]
  end
  pp mapping
  map_file = File.expand_path("~/projects/dim_reference/table_mapping_#{v1}_to_#{v2}.yaml")
  File.open(map_file, "w+") {|file| file.puts YAML::dump(mapping)}
end
# classes2tables
construct_table_migration_mapping("6.7.1","6.7.4")
