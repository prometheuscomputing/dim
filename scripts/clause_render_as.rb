Standard::Clause.exclude(:render_as_id => nil).all.each do |c|
  if c.render_as.value == 'ignore'
    ChangeTracker.start
    c.render_as = nil
    ChangeTracker.commit
  end
end

  