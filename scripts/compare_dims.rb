def doit(ks, ip = false)
  klasses = ks.is_a?(Array) ? ks : [ks]
  stuff = klasses.collect do |klass| 
    if ip
      klass.immediate_properties.select { |k,v| v[:class] =~ /ASN/ }.collect { |k,v| "%-40s %s" % [v[:getter], v[:class].demodulize] };
    else
      klass.properties.select { |k,v| v[:class] =~ /ASN/ }.collect { |k,v| "%-40s %s" % [v[:getter], v[:class].demodulize] };
    end
  end
  puts stuff.flatten.sort
end

doit([DIM::Medical::SampleArray, DIM::Medical::RealTimeSampleArray], true);

# Standard.classes(no_imports:true).each { |k| puts "#{k}:  #{k.count}" if !(k.abstract? || k.interface? || k.join_class? || k.enumeration?) && DB.table_exists?(k.table_name) };

dim_out = {}
DIM.classes.select { |c| (c.name =~ /DIM/) && !(c.name =~ /ASN/) }
           .sort { |a,b| a.name <=> b.name}
           .each do |klass|
  dim_out[klass] = {}
  if klass.enumeration?
    dim_out[klass] = {:enumeration_literals => klass.literal_values}
  elsif klass.respond_to? :immediate_properties
    klass.immediate_properties.each do |k,v|
      # dim_out[klass][v[:getter]] = [v[:class], v[:composition], v[:enumeration]] if (v[:class].to_s =~ /DIM/) && (v[:composition] || v[:enumeration])
      dim_out[klass][v[:getter]] = v[:class] if (v[:class].to_s =~ /DIM|IEEE/) && (v[:composition] || v[:enumeration] || (v[:additional_data] && v[:additional_data][:modeled_as] && v[:additional_data][:modeled_as] == :attribute))
      # puts "#{klass}.#{k} -- #{v.reject{|k,v| k == :methods}}"
    end
  else
    dim_out[klass] = :primitive
  end
end
# pp dim_out;nil

phd_out = {}
PHD.classes.select { |c| (c.name =~ /PHD/) && !(c.name =~ /ASN/) }
           .sort { |a,b| a.name <=> b.name}
           .each do |klass|
  phd_out[klass] = {}
  if klass.enumeration?
    phd_out[klass] = {:enumeration_literals => klass.literal_values}
  elsif klass.respond_to? :immediate_properties
    klass.immediate_properties.each do |k,v|
      # phd_out[klass][v[:getter]] = [v[:class], v[:composition], v[:enumeration]] if (v[:class].to_s =~ /PHD/) && (v[:composition] || v[:enumeration])
      phd_out[klass][v[:getter]] = v[:class] if (v[:class].to_s =~ /PHD|IEEE/) && (v[:composition] || v[:enumeration] || (v[:additional_data] && v[:additional_data][:modeled_as] && v[:additional_data][:modeled_as] == :attribute))
      # puts "#{klass}.#{k} -- #{v.reject{|k,v| k == :methods}}"
    end
  else
    phd_out[klass] = :primitive
  end
end
pp phd_out;nil

# Standard.classes(no_imports:true).each { |k| k.delete if DB.table_exists?(k.table_name) && !(k.enumeration?)};