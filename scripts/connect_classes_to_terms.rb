# load in irb
Hirb.disable if defined? Hirb

def ct
  ChangeTracker.commit
  ChangeTracker.start
end
ChangeTracker.start unless ChangeTracker.started?

# DO NOT CREATE NEW TERMS LIKE THIS!!!!! Use terms from RTMMS!!
# module DIM
#   module Top
#     class Top
#       def set_ref_id term
#         self.ref_id = Nomenclature::Term.create unless self.ref_id
#         self.ref_id.reference_id = term
#         self.ref_id.save
#         self.save
#       end
#     end
#   end
# end
def hook_up_terms models, getter, terms_hash
  models.each do |obj|
    reg_as = obj[getter]
    if reg_as.nil? || reg_as.empty?
      puts "#{obj.name} had no registered_as"
      next
    end
    reg_as = reg_as.gsub('_', '').gsub(/ .*$/, '')
    term = terms_hash[reg_as]
    if term
      obj.term = term
      unless obj[getter] == term.reference_id
        parenthetical = obj[getter].slice /\(.*\)/
        obj[getter] = term.reference_id + " #{parenthetical}"
      end
      obj.save
      ct
    else
      puts "could not find a term for #{obj.name} which was registered as #{obj[getter]}.  We searched for #{reg_as}"
    end
  end
end

terms = Nomenclature::RTMMSTerm.all
terms_hash = {}
terms.each do |term|
  ref_id = term[:reference_id]
  next unless ref_id
  terms_hash[ref_id.gsub('_', '')] = term
end
term_keys = terms_hash.keys
dupes = term_keys.find_all { |e| term_keys.count(e) > 1 } 
puts dupes.inspect

klasses = MetaInfo::Class.all
properties = MetaInfo::Property.all

hook_up_terms klasses, :registered_as, terms_hash

hook_up_terms properties, :attribute_id, terms_hash

# And now any others that we know the mapping for...
handle = MetaInfo::Class.where(name: 'HANDLE').first
handle.registered_as = Nomenclature::Term.where(reference_id: "MDC_ATTR_ID_HANDLE").first.reference_id
handle.save
ct

