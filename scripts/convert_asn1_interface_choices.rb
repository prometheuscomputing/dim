names = {
  "EnumObsValueIface"                      => "EnumObsValueType",
  "NuObsValueIface"                        => "NuObsValueType",
  "SaCalibrationData_ANON_CHOICE"          => "SaCalData",
  "SaMarkerList_ANON_CHOICE"               => "MarkerListSaVal",
  "SaObsValueIface"                        => "SaObsValueType",
  "SaPhysiologicalRange_ANON_CHOICE"       => "ScaledRange",
  "ScaleAndRangeSpecification_ANON_CHOICE" => "ScaleRangeSpec",
  "SegmentData_ANON_CHOICE"                => "SegData",
  "SiteList_ANON_CHOICE"                   => "SiteListType",
  "VisualGrid_ANON_CHOICE"                 => "SaVisualGrid"
}

names.values.each do |n|
  k = MetaInfo::Class.where(:name => n).first
  k.properties.each_with_index do |prop, index|
    ChangeTracker.start
    prop.context_specific_tag = index + 1
    prop.save
    ChangeTracker.commit
  end
end
# ifaces = MetaInfo::Interface.where(:is_asn1 => true).all;
# ifaces.each do |iface|
#   ChangeTracker.start
#   puts iface.name
#   klass = MetaInfo::Class.where(:name => names[iface.name], :is_asn1 => true).first || MetaInfo::Class.create(:name => names[iface.name], :is_asn1 => true)
#
#   iface.properties_of_type.each do |pot|
#     pot.type = klass
#     pot.save
#   end
#   klass.containing_package = iface.containing_package
#   klass.super_classifier = iface.super_classifier
#   iface.documents.each do |doc|
#     doc.model_element = klass
#     doc.title = "Data Type: #{klass.name}"
#     doc.save
#   end
#   desc = iface.description
#   klass.description = desc if desc
#
#   iface.implementing_classes.each do |ic|
#     pname = ic.name.gsub(/(?<=[a-z])(?=[A-Z])/, '-').downcase
#     prop = klass.properties.find { |prp| prp.name == pname}&.first || MetaInfo::Property.create(:name => pname)
#     prop.type = ic
#     prop.save
#     klass.properties_add prop
#     puts "  #{prop.name}  #{prop.type.name}"
#   end
#   ChangeTracker.commit
#   puts
# end;