def delete_profiles profiles
  profile_ids = Array(profiles)
  profile_ids.each do |id|
    prof = MyDevice::PCDProfile[id]
    next unless prof
    root = prof.profile_root
    root.vmds.each do |vmd|
      vmd.channels.each do |channel|
        channel.metrics.each do |metric|
          ChangeTracker.commit {metric.destroy}
        end
        ChangeTracker.commit {channel.destroy}
      end
      ChangeTracker.commit {vmd.destroy}
    end
    ChangeTracker.commit {root.destroy}
    ChangeTracker.commit {prof.destroy}
  end
end
delete_profiles [31,32,33]