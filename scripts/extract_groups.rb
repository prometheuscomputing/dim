answer = []
groups = MetaInfo::Group.all
groups.each do |g|
  entry = {:owning_class => g.owner_class.name, :name => g.name, :attribute_group_id => g.attribute_group_id, :extensible => g.extensible, :properties => g.properties.collect{|p| p.name}}
  if pg = g.parent_group
    entry[:parent_group] = {:name => pg.name, :owning_class => pg.owner_class.name}
  end
  answer << entry
end
require 'yaml'
filename = File.join(File.expand_path(File.dirname __FILE__), "extracted_groups.yml")
File.open(filename, 'w') {|f| f.write answer.to_yaml}
# data=YAML::load(File.read(filename));
# pp data
# puts data.class

