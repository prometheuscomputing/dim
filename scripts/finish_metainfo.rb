ChangeTracker.start

MetaInfo::Class.all.each{|p| p.is_asn1?}
MetaInfo::Enumeration.all.each{|p| p.is_asn1?}
MetaInfo::Property.all.each{|p| p.is_asn1?}
MetaInfo::Interface.all.each do |iface|
  # puts "#{iface.qualified_name}"
  iface.is_asn1?
  corresponding_model = iface.ruby_model
  implementor_models = corresponding_model.implementors
  implementor_metainfo_instances = implementor_models.collect{|im| MetaInfo::Class.where(:name => im.name.demodulize).first}
  # puts "  implemented by: #{implementor_metainfo_instances.collect{|i| i.name}}"
  iface.implementing_classes = implementor_metainfo_instances
  iface.save
end;nil

ChangeTracker.commit

ChangeTracker.start
MetaInfo::Package.where(:name => 'IEEE20101').first.child_packages.first.classifiers.each do |c|
  c.is_asn1?
  corresponding_model = c.ruby_model
  subclass_models     = corresponding_model.immediate_children
  subclass_metainfo_instances = subclass_models.collect{|sm| MetaInfo::Class.where(:name => sm.name.demodulize.gsub("_", "-")).first}
  c.sub_classifiers = subclass_metainfo_instances
  c.save
end
ChangeTracker.commit

# Make all inheritance connections # FIXME do Enums and Interfaces too (Interfaces should inherit from CHOICE)
ChangeTracker.start
MetaInfo::Class.all.each do |klass|
  corresponding_model = klass.ruby_model
  unless corresponding_model
    puts "Failed to find corresponding_model for #{klass.qualified_name}"
    next
  end
  
  if ruby_parent = corresponding_model.superclass
    parent_name = ruby_parent.name.demodulize
    next if ["Model", "Fixnum", "String"].include?(parent_name)
    metainfo_parent = MetaInfo::Class.where(Sequel.ilike(:name, parent_name)).first
    if metainfo_parent
      klass.super_classifier = metainfo_parent
      klass.save
    else
      puts "failed to find metainfo parent for #{klass.name} where ruby parent is #{ruby_parent.name}.  Searched for #{parent_name.inspect}"
    end
  end  
  klass.save
end;nil
MetaInfo::Enumeration.all.each do |klass|
  corresponding_model = klass.ruby_model
  unless corresponding_model
    puts "Failed to find corresponding_model for #{klass.qualified_name}"
    next
  end
  ad = corresponding_model.additional_data
  # puts "#{corresponding_model.name}: #{ad}"
  type = ad[:prometheus]["implemented_as"].first if ad
  if type 
    metainfo_parent = MetaInfo::Class.where(Sequel.ilike(:name, type)).first || MetaInfo::Enumeration.where(Sequel.ilike(:name, type)).first
    if metainfo_parent
      klass.super_classifier = metainfo_parent
      klass.save
    else
      puts "failed to find metainfo parent for #{klass.name} where parent is #{type}."
    end
  else
    puts "failed to find type for #{klass.name}."
  end  
  klass.save
end;nil
ChangeTracker.start unless ChangeTracker.started?
MetaInfo::Interface.all.each do |klass|
  next unless klass.is_asn1?
  corresponding_model = klass.ruby_model
  unless corresponding_model
    puts "Failed to find corresponding_model for #{klass.qualified_name}"
    next
  end
  sc = MetaInfo::Class.where(Sequel.ilike(:name, "CHOICE")).first
  klass.super_classifier = sc
  klass.save
end;nil
ChangeTracker.commit

# FIXME it appears that the ASN.1 CHOICE datatypes included in the classic are not captured as MetaInfo::Interface instances but are instead captured as MetaInfo::Class instances.  What to do?  It may be fine for now but it really is incorrect.  We shall see...

# Now to get rid or inherited properties...
ChangeTracker.start
MetaInfo::Class.all.each do |klass|
  ans = klass.ancestors
  ans_props = ans.collect{|a| a.properties}.flatten.collect{|p| [p.name, p.attribute_id]}.uniq
  klass.properties.each do |p|
    p.destroy if ans_props.include?([p.name, p.attribute_id])
  end
end
ChangeTracker.commit

# Must be last
ChangeTracker.start
names = {
  "SEQUENCE_OF"    => "SEQUENCE OF",
  "BIT_STRING"     => "BIT STRING",    
  "OCTET_STRING"   => "OCTET STRING",  
  "OCTET_STRING-8" => "OCTET STRING-8",
  "ANY_DEFINED_BY" => "ANY DEFINED BY"
}
names.each do |k,v|
  element = MetaInfo::Class.where(Sequel.ilike(:name, k)).first
  element.name = v if element
  element.save if element  
end
element = MetaInfo::Property.where(:name => "class_dim").first
element.name = "class" if element
element.save if element
ChangeTracker.commit