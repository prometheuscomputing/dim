def remove_test_enums mod
  mod.classes(:no_imports => true).each do |klass|
    next unless klass.enumeration?
    if oops = klass.where(:value => 'Test Value')
      ChangeTracker.start
      oops.destroy
      ChangeTracker.commit
    end
  end
end
[DIM, Nomenclature, MyDevice, MetaInfo].each{|k| remove_test_enums k}

# replace all enums that were accidentally deleted
ChangeTracker.start if defined?(ChangeTracker) && !ChangeTracker.started?

  Gui_Builder_Profile::FacadeImplementationLanguages.where(:value => 'Java').first || Gui_Builder_Profile::FacadeImplementationLanguages.create(:value => 'Java')
  Gui_Builder_Profile::FacadeImplementationLanguages.where(:value => 'Ruby').first || Gui_Builder_Profile::FacadeImplementationLanguages.create(:value => 'Ruby')


  Gui_Builder_Profile::LanguageType.where(:value => 'Ruby').first || Gui_Builder_Profile::LanguageType.create(:value => 'Ruby')
  Gui_Builder_Profile::LanguageType.where(:value => 'Javascript').first || Gui_Builder_Profile::LanguageType.create(:value => 'Javascript')
  Gui_Builder_Profile::LanguageType.where(:value => 'Clojure').first || Gui_Builder_Profile::LanguageType.create(:value => 'Clojure')
  Gui_Builder_Profile::LanguageType.where(:value => 'Groovy').first || Gui_Builder_Profile::LanguageType.create(:value => 'Groovy')
  Gui_Builder_Profile::LanguageType.where(:value => 'Python').first || Gui_Builder_Profile::LanguageType.create(:value => 'Python')


  Gui_Builder_Profile::MarkupType.where(:value => 'Markdown').first || Gui_Builder_Profile::MarkupType.create(:value => 'Markdown')
  Gui_Builder_Profile::MarkupType.where(:value => 'LaTeX').first || Gui_Builder_Profile::MarkupType.create(:value => 'LaTeX')
  Gui_Builder_Profile::MarkupType.where(:value => 'Plain').first || Gui_Builder_Profile::MarkupType.create(:value => 'Plain')
  Gui_Builder_Profile::MarkupType.where(:value => 'HTML').first || Gui_Builder_Profile::MarkupType.create(:value => 'HTML')
  Gui_Builder_Profile::MarkupType.where(:value => 'Textile').first || Gui_Builder_Profile::MarkupType.create(:value => 'Textile')
  Gui_Builder_Profile::MarkupType.where(:value => 'Kramdown').first || Gui_Builder_Profile::MarkupType.create(:value => 'Kramdown')


  Gui_Builder_Profile::RoleRegistrationType.where(:value => 'Open').first || Gui_Builder_Profile::RoleRegistrationType.create(:value => 'Open')
  Gui_Builder_Profile::RoleRegistrationType.where(:value => 'Role').first || Gui_Builder_Profile::RoleRegistrationType.create(:value => 'Role')
  Gui_Builder_Profile::RoleRegistrationType.where(:value => 'Role Manager').first || Gui_Builder_Profile::RoleRegistrationType.create(:value => 'Role Manager')
  Gui_Builder_Profile::RoleRegistrationType.where(:value => 'Administrator').first || Gui_Builder_Profile::RoleRegistrationType.create(:value => 'Administrator')


  Gui_Builder_Profile::UserRegistrationType.where(:value => 'Open').first || Gui_Builder_Profile::UserRegistrationType.create(:value => 'Open')
  Gui_Builder_Profile::UserRegistrationType.where(:value => 'Email').first || Gui_Builder_Profile::UserRegistrationType.create(:value => 'Email')
  Gui_Builder_Profile::UserRegistrationType.where(:value => 'Invitation').first || Gui_Builder_Profile::UserRegistrationType.create(:value => 'Invitation')


  Gui_Builder_Profile::Spec::SpecButtonResultType.where(:value => 'none').first || Gui_Builder_Profile::Spec::SpecButtonResultType.create(:value => 'none')
  Gui_Builder_Profile::Spec::SpecButtonResultType.where(:value => 'popup').first || Gui_Builder_Profile::Spec::SpecButtonResultType.create(:value => 'popup')
  Gui_Builder_Profile::Spec::SpecButtonResultType.where(:value => 'file').first || Gui_Builder_Profile::Spec::SpecButtonResultType.create(:value => 'file')
  Gui_Builder_Profile::Spec::SpecButtonResultType.where(:value => 'web_page').first || Gui_Builder_Profile::Spec::SpecButtonResultType.create(:value => 'web_page')


  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'disabled').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'disabled')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'default').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'default')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'chemical_system').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'chemical_system')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'boolean').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'boolean')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'case_sensitive_exact').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'case_sensitive_exact')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'case_insensitive_exact').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'case_insensitive_exact')
  Gui_Builder_Profile::Spec::SpecSearchFilterType.where(:value => 'case_insensitive_like').first || Gui_Builder_Profile::Spec::SpecSearchFilterType.create(:value => 'case_insensitive_like')


  DIM::CommonDataTypes::ASN1::AdministrativeState.where(:value => 'locked(0)').first || DIM::CommonDataTypes::ASN1::AdministrativeState.create(:value => 'locked(0)')
  DIM::CommonDataTypes::ASN1::AdministrativeState.where(:value => 'unlocked(1)').first || DIM::CommonDataTypes::ASN1::AdministrativeState.create(:value => 'unlocked(1)')
  DIM::CommonDataTypes::ASN1::AdministrativeState.where(:value => 'shuttingDown(2)').first || DIM::CommonDataTypes::ASN1::AdministrativeState.create(:value => 'shuttingDown(2)')


  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-unspec(0)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-unspec(0)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-10646-ucs-2(1000)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-10646-ucs-2(1000)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-10646-ucs-4(1001)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-10646-ucs-4(1001)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-1(4)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-1(4)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-2(5)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-2(5)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-3(6)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-3(6)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-4(7)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-4(7)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-5(8)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-5(8)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-6(9)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-6(9)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-7(10)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-7(10)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-8(11)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-8(11)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-9(12)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-9(12)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-10(13)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-10(13)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-13(109)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-13(109)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-14(110)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-14(110)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-8859-15(111)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-8859-15(111)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-2022-kr(37)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-2022-kr(37)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-ks-c-5601(36)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-ks-c-5601(36)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-2022-jp(39)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-2022-jp(39)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-2022-jp-2(40)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-2022-jp-2(40)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-jis-x0208(63)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-jis-x0208(63)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-iso-2022-cn(104)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-iso-2022-cn(104)')
  DIM::CommonDataTypes::ASN1::CharSet.where(:value => 'charset-gb-2312(2025)').first || DIM::CommonDataTypes::ASN1::CharSet.create(:value => 'charset-gb-2312(2025)')


  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-unspec(0)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-unspec(0)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-obj(1)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-obj(1)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-metric(2)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-metric(2)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-alert(3)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-alert(3)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-dim(4)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-dim(4)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-vattr(5)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-vattr(5)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-pgrp(6)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-pgrp(6)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-sites(7)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-sites(7)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-infrastruct(8)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-infrastruct(8)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-fef(9)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-fef(9)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-ecg-extn(10)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-ecg-extn(10)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-ext-nom(256)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-ext-nom(256)')
  DIM::CommonDataTypes::ASN1::NomPartition.where(:value => 'nom-part-priv(1024)').first || DIM::CommonDataTypes::ASN1::NomPartition.create(:value => 'nom-part-priv(1024)')


  DIM::CommonDataTypes::ASN1::OperationalState.where(:value => 'disabled(0)').first || DIM::CommonDataTypes::ASN1::OperationalState.create(:value => 'disabled(0)')
  DIM::CommonDataTypes::ASN1::OperationalState.where(:value => 'enabled(1)').first || DIM::CommonDataTypes::ASN1::OperationalState.create(:value => 'enabled(1)')
  DIM::CommonDataTypes::ASN1::OperationalState.where(:value => 'notAvailable(2)').first || DIM::CommonDataTypes::ASN1::OperationalState.create(:value => 'notAvailable(2)')


  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-black(0)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-black(0)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-red(1)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-red(1)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-green(2)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-green(2)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-yellow(3)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-yellow(3)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-blue(4)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-blue(4)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-magenta(5)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-magenta(5)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-cyan(6)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-cyan(6)')
  DIM::CommonDataTypes::ASN1::SimpleColour.where(:value => 'col-white(7)').first || DIM::CommonDataTypes::ASN1::SimpleColour.create(:value => 'col-white(7)')


  DIM::CommonDataTypes::ASN1::StringFlags.where(:value => 'str-flag-nt(0)').first || DIM::CommonDataTypes::ASN1::StringFlags.create(:value => 'str-flag-nt(0)')


  DIM::Medical::ASN1::ChannelStatus.where(:value => 'chan-off(0)').first || DIM::Medical::ASN1::ChannelStatus.create(:value => 'chan-off(0)')
  DIM::Medical::ASN1::ChannelStatus.where(:value => 'chan-not-ready(1)').first || DIM::Medical::ASN1::ChannelStatus.create(:value => 'chan-not-ready(1)')
  DIM::Medical::ASN1::ChannelStatus.where(:value => 'chan-standby(2)').first || DIM::Medical::ASN1::ChannelStatus.create(:value => 'chan-standby(2)')
  DIM::Medical::ASN1::ChannelStatus.where(:value => 'chan-transduc-discon(8)').first || DIM::Medical::ASN1::ChannelStatus.create(:value => 'chan-transduc-discon(8)')
  DIM::Medical::ASN1::ChannelStatus.where(:value => 'chan-hw-discon(9)').first || DIM::Medical::ASN1::ChannelStatus.create(:value => 'chan-hw-discon(9)')


  DIM::Medical::ASN1::CmplxFlags.where(:value => 'cmplx-flag-reserved(0)').first || DIM::Medical::ASN1::CmplxFlags.create(:value => 'cmplx-flag-reserved(0)')


  DIM::Medical::ASN1::CmplxObsElemFlags.where(:value => 'cm-obs-elem-flg-mplex(0)').first || DIM::Medical::ASN1::CmplxObsElemFlags.create(:value => 'cm-obs-elem-flg-mplex(0)')
  DIM::Medical::ASN1::CmplxObsElemFlags.where(:value => 'cm-obs-elem-flg-is-setting(2)').first || DIM::Medical::ASN1::CmplxObsElemFlags.create(:value => 'cm-obs-elem-flg-is-setting(2)')
  DIM::Medical::ASN1::CmplxObsElemFlags.where(:value => 'cm-obs-elem-flg-updt-episodic(4)').first || DIM::Medical::ASN1::CmplxObsElemFlags.create(:value => 'cm-obs-elem-flg-updt-episodic(4)')
  DIM::Medical::ASN1::CmplxObsElemFlags.where(:value => 'cm-obs-elem-flg-msmt-noncontinuous(5)').first || DIM::Medical::ASN1::CmplxObsElemFlags.create(:value => 'cm-obs-elem-flg-msmt-noncontinuous(5)')


  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'invalid(0)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'invalid(0)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'questionable(1)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'questionable(1)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'not-available(2)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'not-available(2)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'calibration-ongoing(3)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'calibration-ongoing(3)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'test-data(4)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'test-data(4)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'demo-data(5)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'demo-data(5)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'validated-data(8)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'validated-data(8)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'early-indication(9)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'early-indication(9)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'msmt-ongoing(10)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'msmt-ongoing(10)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'msmt-state-in-alarm(14)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'msmt-state-in-alarm(14)')
  DIM::Medical::ASN1::MeasurementStatus.where(:value => 'msmt-state-al-inhibited(15)').first || DIM::Medical::ASN1::MeasurementStatus.create(:value => 'msmt-state-al-inhibited(15)')


  DIM::Medical::ASN1::MetricAccess.where(:value => 'avail-intermittent(0)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'avail-intermittent(0)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'upd-periodic(1)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'upd-periodic(1)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'upd-episodic(2)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'upd-episodic(2)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'msmt-noncontinuous(3)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'msmt-noncontinuous(3)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'acc-evrep(4)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'acc-evrep(4)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'acc-get(5)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'acc-get(5)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'acc-scan(6)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'acc-scan(6)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'gen-opt-sync(8)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'gen-opt-sync(8)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'sc-opt-normal(10)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'sc-opt-normal(10)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'sc-opt-extensive(11)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'sc-opt-extensive(11)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'sc-opt-long-pd-avail(12)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'sc-opt-long-pd-avail(12)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'sc-opt-confirm(13)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'sc-opt-confirm(13)')
  DIM::Medical::ASN1::MetricAccess.where(:value => 'sc-opt-refresh(14)').first || DIM::Medical::ASN1::MetricAccess.create(:value => 'sc-opt-refresh(14)')


  DIM::Medical::ASN1::MetricCalState.where(:value => 'not-calibrated(0)').first || DIM::Medical::ASN1::MetricCalState.create(:value => 'not-calibrated(0)')
  DIM::Medical::ASN1::MetricCalState.where(:value => 'cal-required(1)').first || DIM::Medical::ASN1::MetricCalState.create(:value => 'cal-required(1)')
  DIM::Medical::ASN1::MetricCalState.where(:value => 'calibrated(2)').first || DIM::Medical::ASN1::MetricCalState.create(:value => 'calibrated(2)')


  DIM::Medical::ASN1::MetricCalType.where(:value => 'cal-unspec(0)').first || DIM::Medical::ASN1::MetricCalType.create(:value => 'cal-unspec(0)')
  DIM::Medical::ASN1::MetricCalType.where(:value => 'cal-offset(1)').first || DIM::Medical::ASN1::MetricCalType.create(:value => 'cal-offset(1)')
  DIM::Medical::ASN1::MetricCalType.where(:value => 'cal-gain(2)').first || DIM::Medical::ASN1::MetricCalType.create(:value => 'cal-gain(2)')
  DIM::Medical::ASN1::MetricCalType.where(:value => 'cal-two-point(3)').first || DIM::Medical::ASN1::MetricCalType.create(:value => 'cal-two-point(3)')


  DIM::Medical::ASN1::MetricCategory.where(:value => 'mcat-unspec(0)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'mcat-unspec(0)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'auto-measurement(1)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'auto-measurement(1)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'manual-measurement(2)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'manual-measurement(2)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'auto-setting(3)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'auto-setting(3)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'manual-setting(4)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'manual-setting(4)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'auto-calculation(5)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'auto-calculation(5)')
  DIM::Medical::ASN1::MetricCategory.where(:value => 'manual-calculation(6)').first || DIM::Medical::ASN1::MetricCategory.create(:value => 'manual-calculation(6)')


  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-unspec(0)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-unspec(0)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-internal(1)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-internal(1)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-store-only(2)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-store-only(2)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-no-recording(3)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-no-recording(3)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-phys-ev-ind(4)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-phys-ev-ind(4)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-btb-metric(5)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-btb-metric(5)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-app-specific(8)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-app-specific(8)')
  DIM::Medical::ASN1::MetricRelevance.where(:value => 'rv-service(9)').first || DIM::Medical::ASN1::MetricRelevance.create(:value => 'rv-service(9)')


  DIM::Medical::ASN1::MetricStatus.where(:value => 'metric-off(0)').first || DIM::Medical::ASN1::MetricStatus.create(:value => 'metric-off(0)')
  DIM::Medical::ASN1::MetricStatus.where(:value => 'metric-not-ready(1)').first || DIM::Medical::ASN1::MetricStatus.create(:value => 'metric-not-ready(1)')
  DIM::Medical::ASN1::MetricStatus.where(:value => 'metric-standby(2)').first || DIM::Medical::ASN1::MetricStatus.create(:value => 'metric-standby(2)')
  DIM::Medical::ASN1::MetricStatus.where(:value => 'metric-transduc-discon(8)').first || DIM::Medical::ASN1::MetricStatus.create(:value => 'metric-transduc-discon(8)')
  DIM::Medical::ASN1::MetricStatus.where(:value => 'metric-hw-discon(9)').first || DIM::Medical::ASN1::MetricStatus.create(:value => 'metric-hw-discon(9)')


  DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.where(:value => 'compound(1)').first || DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.create(:value => 'compound(1)')
  DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.where(:value => 'simple(0)').first || DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.create(:value => 'simple(0)')
  DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.where(:value => 'complex(2)').first || DIM::Medical::ASN1::MetricStructureMSStruct_ANON_ENUM.create(:value => 'complex(2)')


  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-other(0)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-other(0)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-chemical(1)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-chemical(1)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-electrical(2)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-electrical(2)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-impedance(3)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-impedance(3)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-nuclear(4)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-nuclear(4)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-optical(5)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-optical(5)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-thermal(6)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-thermal(6)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-biological(7)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-biological(7)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-mechanical(8)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-mechanical(8)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-acoustical(9)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-acoustical(9)')
  DIM::Medical::ASN1::MsmtPrinciple.where(:value => 'msp-manual(15)').first || DIM::Medical::ASN1::MsmtPrinciple.create(:value => 'msp-manual(15)')


  DIM::Medical::ASN1::SaCalDataType.where(:value => 'bar(0)').first || DIM::Medical::ASN1::SaCalDataType.create(:value => 'bar(0)')
  DIM::Medical::ASN1::SaCalDataType.where(:value => 'stair(1)').first || DIM::Medical::ASN1::SaCalDataType.create(:value => 'stair(1)')


  DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.where(:value => 'other(0)').first || DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.create(:value => 'other(0)')
  DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.where(:value => 'low-pass(1)').first || DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.create(:value => 'low-pass(1)')
  DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.where(:value => 'high-pass(2)').first || DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.create(:value => 'high-pass(2)')
  DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.where(:value => 'notch(3)').first || DIM::Medical::ASN1::SaFilterEntryFilterType_ANON_ENUM.create(:value => 'notch(3)')


  DIM::Medical::ASN1::SaFlags.where(:value => 'smooth-curve(0)').first || DIM::Medical::ASN1::SaFlags.create(:value => 'smooth-curve(0)')
  DIM::Medical::ASN1::SaFlags.where(:value => 'delayed-curve(1)').first || DIM::Medical::ASN1::SaFlags.create(:value => 'delayed-curve(1)')
  DIM::Medical::ASN1::SaFlags.where(:value => 'static-scale(2)').first || DIM::Medical::ASN1::SaFlags.create(:value => 'static-scale(2)')
  DIM::Medical::ASN1::SaFlags.where(:value => 'sa-ext-val-range(3)').first || DIM::Medical::ASN1::SaFlags.create(:value => 'sa-ext-val-range(3)')


  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-nos(0)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-nos(0)')
  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-moving-average(1)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-moving-average(1)')
  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-recursive(2)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-recursive(2)')
  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-min-pick(3)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-min-pick(3)')
  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-max-pick(4)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-max-pick(4)')
  DIM::Medical::ASN1::StoSampleAlg.where(:value => 'st-alg-median(5)').first || DIM::Medical::ASN1::StoSampleAlg.create(:value => 'st-alg-median(5)')


  DIM::Medical::ASN1::StorageFormat.where(:value => 'sto-t-nos(0)').first || DIM::Medical::ASN1::StorageFormat.create(:value => 'sto-t-nos(0)')
  DIM::Medical::ASN1::StorageFormat.where(:value => 'sto-t-gen(1)').first || DIM::Medical::ASN1::StorageFormat.create(:value => 'sto-t-gen(1)')
  DIM::Medical::ASN1::StorageFormat.where(:value => 'sto-t-nu-opt(2)').first || DIM::Medical::ASN1::StorageFormat.create(:value => 'sto-t-nu-opt(2)')
  DIM::Medical::ASN1::StorageFormat.where(:value => 'sto-t-rtsa-opt(3)').first || DIM::Medical::ASN1::StorageFormat.create(:value => 'sto-t-rtsa-opt(3)')


  DIM::Medical::ASN1::VMDStatus.where(:value => 'vmd-off(0)').first || DIM::Medical::ASN1::VMDStatus.create(:value => 'vmd-off(0)')
  DIM::Medical::ASN1::VMDStatus.where(:value => 'vmd-not-ready(1)').first || DIM::Medical::ASN1::VMDStatus.create(:value => 'vmd-not-ready(1)')
  DIM::Medical::ASN1::VMDStatus.where(:value => 'vmd-standby(2)').first || DIM::Medical::ASN1::VMDStatus.create(:value => 'vmd-standby(2)')
  DIM::Medical::ASN1::VMDStatus.where(:value => 'vmd-transduc-discon(8)').first || DIM::Medical::ASN1::VMDStatus.create(:value => 'vmd-transduc-discon(8)')
  DIM::Medical::ASN1::VMDStatus.where(:value => 'vmd-hw-discon(9)').first || DIM::Medical::ASN1::VMDStatus.create(:value => 'vmd-hw-discon(9)')


  DIM::Alert::ASN1::AlarmRepFlags_ANON_ENUM.where(:value => 'dyn-inst-contents(1)').first || DIM::Alert::ASN1::AlarmRepFlags_ANON_ENUM.create(:value => 'dyn-inst-contents(1)')
  DIM::Alert::ASN1::AlarmRepFlags_ANON_ENUM.where(:value => 'rep-all-inst(2)').first || DIM::Alert::ASN1::AlarmRepFlags_ANON_ENUM.create(:value => 'rep-all-inst(2)')


  DIM::Alert::ASN1::AlertControls.where(:value => 'ac-obj-off(0)').first || DIM::Alert::ASN1::AlertControls.create(:value => 'ac-obj-off(0)')
  DIM::Alert::ASN1::AlertControls.where(:value => 'ac-chan-off(1)').first || DIM::Alert::ASN1::AlertControls.create(:value => 'ac-chan-off(1)')
  DIM::Alert::ASN1::AlertControls.where(:value => 'ac-all-obj-al-off(3)').first || DIM::Alert::ASN1::AlertControls.create(:value => 'ac-all-obj-al-off(3)')
  DIM::Alert::ASN1::AlertControls.where(:value => 'ac-alert-off(4)').first || DIM::Alert::ASN1::AlertControls.create(:value => 'ac-alert-off(4)')
  DIM::Alert::ASN1::AlertControls.where(:value => 'ac-alert-muted(5)').first || DIM::Alert::ASN1::AlertControls.create(:value => 'ac-alert-muted(5)')


  DIM::Alert::ASN1::AlertFlags.where(:value => 'local-audible(1)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'local-audible(1)')
  DIM::Alert::ASN1::AlertFlags.where(:value => 'remote-audible(2)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'remote-audible(2)')
  DIM::Alert::ASN1::AlertFlags.where(:value => 'visual-latching(3)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'visual-latching(3)')
  DIM::Alert::ASN1::AlertFlags.where(:value => 'audible-latching(4)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'audible-latching(4)')
  DIM::Alert::ASN1::AlertFlags.where(:value => 'derived(6)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'derived(6)')
  DIM::Alert::ASN1::AlertFlags.where(:value => 'record-inhibit(8)').first || DIM::Alert::ASN1::AlertFlags.create(:value => 'record-inhibit(8)')


  DIM::Alert::ASN1::AlertState.where(:value => 'al-inhibited(0)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-inhibited(0)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-suspended(1)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-suspended(1)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-latched(2)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-latched(2)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-silenced-reset(3)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-silenced-reset(3)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-dev-in-test-mode(5)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-dev-in-test-mode(5)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-dev-in-standby-mode(6)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-dev-in-standby-mode(6)')
  DIM::Alert::ASN1::AlertState.where(:value => 'al-dev-in-demo-mode(7)').first || DIM::Alert::ASN1::AlertState.create(:value => 'al-dev-in-demo-mode(7)')


  DIM::Alert::ASN1::AlertType.where(:value => 'no-alert(0)').first || DIM::Alert::ASN1::AlertType.create(:value => 'no-alert(0)')
  DIM::Alert::ASN1::AlertType.where(:value => 'low-pri-t-al(1)').first || DIM::Alert::ASN1::AlertType.create(:value => 'low-pri-t-al(1)')
  DIM::Alert::ASN1::AlertType.where(:value => 'med-pri-t-al(2)').first || DIM::Alert::ASN1::AlertType.create(:value => 'med-pri-t-al(2)')
  DIM::Alert::ASN1::AlertType.where(:value => 'hi-pri-t-al(4)').first || DIM::Alert::ASN1::AlertType.create(:value => 'hi-pri-t-al(4)')
  DIM::Alert::ASN1::AlertType.where(:value => 'low-pri-p-al(256)').first || DIM::Alert::ASN1::AlertType.create(:value => 'low-pri-p-al(256)')
  DIM::Alert::ASN1::AlertType.where(:value => 'med-pri-p-al(512)').first || DIM::Alert::ASN1::AlertType.create(:value => 'med-pri-p-al(512)')
  DIM::Alert::ASN1::AlertType.where(:value => 'hi-pri-p-al(1024)').first || DIM::Alert::ASN1::AlertType.create(:value => 'hi-pri-p-al(1024)')


  DIM::System::ASN1::ApplicationArea.where(:value => 'area-unspec(0)').first || DIM::System::ASN1::ApplicationArea.create(:value => 'area-unspec(0)')
  DIM::System::ASN1::ApplicationArea.where(:value => 'area-operating-room(1)').first || DIM::System::ASN1::ApplicationArea.create(:value => 'area-operating-room(1)')
  DIM::System::ASN1::ApplicationArea.where(:value => 'area-intensive-care(2)').first || DIM::System::ASN1::ApplicationArea.create(:value => 'area-intensive-care(2)')


  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-discharged(0)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-discharged(0)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-full(1)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-full(1)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-discharging(2)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-discharging(2)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-chargingFull(8)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-chargingFull(8)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-chargingTrickle(9)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-chargingTrickle(9)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-malfunction(12)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-malfunction(12)')
  DIM::System::ASN1::BatteryStatus.where(:value => 'batt-needs-conditioning(13)').first || DIM::System::ASN1::BatteryStatus.create(:value => 'batt-needs-conditioning(13)')


  DIM::System::ASN1::ClearLogOptions.where(:value => 'log-clear-if-unchanged(1)').first || DIM::System::ASN1::ClearLogOptions.create(:value => 'log-clear-if-unchanged(1)')


  DIM::System::ASN1::ClearLogResult.where(:value => 'log-range-cleared(0)').first || DIM::System::ASN1::ClearLogResult.create(:value => 'log-range-cleared(0)')
  DIM::System::ASN1::ClearLogResult.where(:value => 'log-changed-clear-error(1)').first || DIM::System::ASN1::ClearLogResult.create(:value => 'log-changed-clear-error(1)')
  DIM::System::ASN1::ClearLogResult.where(:value => 'log-change-counter-not-supported(2)').first || DIM::System::ASN1::ClearLogResult.create(:value => 'log-change-counter-not-supported(2)')


  DIM::System::ASN1::DateTimeUsage.where(:value => 'dt-use-remote-sync(0)').first || DIM::System::ASN1::DateTimeUsage.create(:value => 'dt-use-remote-sync(0)')
  DIM::System::ASN1::DateTimeUsage.where(:value => 'dt-use-operator-set(1)').first || DIM::System::ASN1::DateTimeUsage.create(:value => 'dt-use-operator-set(1)')
  DIM::System::ASN1::DateTimeUsage.where(:value => 'dt-use-rtc-synced(2)').first || DIM::System::ASN1::DateTimeUsage.create(:value => 'dt-use-rtc-synced(2)')
  DIM::System::ASN1::DateTimeUsage.where(:value => 'dt-use-critical-use(3)').first || DIM::System::ASN1::DateTimeUsage.create(:value => 'dt-use-critical-use(3)')
  DIM::System::ASN1::DateTimeUsage.where(:value => 'dt-use-displayed(4)').first || DIM::System::ASN1::DateTimeUsage.create(:value => 'dt-use-displayed(4)')


  DIM::System::ASN1::EventLogInfo.where(:value => 'ev-log-clear-range-sup(0)').first || DIM::System::ASN1::EventLogInfo.create(:value => 'ev-log-clear-range-sup(0)')
  DIM::System::ASN1::EventLogInfo.where(:value => 'ev-log-get-act-sup(1)').first || DIM::System::ASN1::EventLogInfo.create(:value => 'ev-log-get-act-sup(1)')
  DIM::System::ASN1::EventLogInfo.where(:value => 'ev-log-binary-entries(8)').first || DIM::System::ASN1::EventLogInfo.create(:value => 'ev-log-binary-entries(8)')
  DIM::System::ASN1::EventLogInfo.where(:value => 'ev-log-full(16)').first || DIM::System::ASN1::EventLogInfo.create(:value => 'ev-log-full(16)')
  DIM::System::ASN1::EventLogInfo.where(:value => 'ev-log-wrap-detect(17)').first || DIM::System::ASN1::EventLogInfo.create(:value => 'ev-log-wrap-detect(17)')


  DIM::System::ASN1::LineFrequency.where(:value => 'line-f-unspec(0)').first || DIM::System::ASN1::LineFrequency.create(:value => 'line-f-unspec(0)')
  DIM::System::ASN1::LineFrequency.where(:value => 'line-f-50hz(1)').first || DIM::System::ASN1::LineFrequency.create(:value => 'line-f-50hz(1)')
  DIM::System::ASN1::LineFrequency.where(:value => 'line-f-60hz(2)').first || DIM::System::ASN1::LineFrequency.create(:value => 'line-f-60hz(2)')


  DIM::System::ASN1::MDSStatus.where(:value => 'disconnected(0)').first || DIM::System::ASN1::MDSStatus.create(:value => 'disconnected(0)')
  DIM::System::ASN1::MDSStatus.where(:value => 'unassociated(1)').first || DIM::System::ASN1::MDSStatus.create(:value => 'unassociated(1)')
  DIM::System::ASN1::MDSStatus.where(:value => 'associating(2)').first || DIM::System::ASN1::MDSStatus.create(:value => 'associating(2)')
  DIM::System::ASN1::MDSStatus.where(:value => 'associated(3)').first || DIM::System::ASN1::MDSStatus.create(:value => 'associated(3)')
  DIM::System::ASN1::MDSStatus.where(:value => 'configuring(4)').first || DIM::System::ASN1::MDSStatus.create(:value => 'configuring(4)')
  DIM::System::ASN1::MDSStatus.where(:value => 'configured(5)').first || DIM::System::ASN1::MDSStatus.create(:value => 'configured(5)')
  DIM::System::ASN1::MDSStatus.where(:value => 'operating(6)').first || DIM::System::ASN1::MDSStatus.create(:value => 'operating(6)')
  DIM::System::ASN1::MDSStatus.where(:value => 're-initializing(7)').first || DIM::System::ASN1::MDSStatus.create(:value => 're-initializing(7)')
  DIM::System::ASN1::MDSStatus.where(:value => 'terminating(8)').first || DIM::System::ASN1::MDSStatus.create(:value => 'terminating(8)')
  DIM::System::ASN1::MDSStatus.where(:value => 'disassociating(9)').first || DIM::System::ASN1::MDSStatus.create(:value => 'disassociating(9)')
  DIM::System::ASN1::MDSStatus.where(:value => 'disassociated(10)').first || DIM::System::ASN1::MDSStatus.create(:value => 'disassociated(10)')
  DIM::System::ASN1::MDSStatus.where(:value => 're-configuring(11)').first || DIM::System::ASN1::MDSStatus.create(:value => 're-configuring(11)')




  DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.where(:value => 'majorVersion1(0)').first || DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.create(:value => 'majorVersion1(0)')
  DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.where(:value => 'majorVersion2(1)').first || DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.create(:value => 'majorVersion2(1)')
  DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.where(:value => 'majorVersion3(2)').first || DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.create(:value => 'majorVersion3(2)')
  DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.where(:value => 'majorVersion4(3)').first || DIM::System::ASN1::NomenclatureVersionNomMajorVersion_ANON_ENUM.create(:value => 'majorVersion4(3)')


  DIM::System::ASN1::PowerStatus.where(:value => 'onMains(0)').first || DIM::System::ASN1::PowerStatus.create(:value => 'onMains(0)')
  DIM::System::ASN1::PowerStatus.where(:value => 'onBattery(1)').first || DIM::System::ASN1::PowerStatus.create(:value => 'onBattery(1)')
  DIM::System::ASN1::PowerStatus.where(:value => 'chargingFull(8)').first || DIM::System::ASN1::PowerStatus.create(:value => 'chargingFull(8)')
  DIM::System::ASN1::PowerStatus.where(:value => 'chargingTrickle(9)').first || DIM::System::ASN1::PowerStatus.create(:value => 'chargingTrickle(9)')
  DIM::System::ASN1::PowerStatus.where(:value => 'chargingOff(10)').first || DIM::System::ASN1::PowerStatus.create(:value => 'chargingOff(10)')


  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'unspecified(0)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'unspecified(0)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'serial-number(1)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'serial-number(1)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'part-number(2)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'part-number(2)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'hw-revision(3)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'hw-revision(3)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'sw-revision(4)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'sw-revision(4)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'fw-revision(5)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'fw-revision(5)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'protocol-revision(6)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'protocol-revision(6)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'prod-spec-gmdn(7)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'prod-spec-gmdn(7)')
  DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'fda-udi(8)').first || DIM::System::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'fda-udi(8)')


  DIM::System::ASN1::SystemCapability.where(:value => 'sc-multiple-context(0)').first || DIM::System::ASN1::SystemCapability.create(:value => 'sc-multiple-context(0)')
  DIM::System::ASN1::SystemCapability.where(:value => 'sc-dyn-configuration(1)').first || DIM::System::ASN1::SystemCapability.create(:value => 'sc-dyn-configuration(1)')
  DIM::System::ASN1::SystemCapability.where(:value => 'sc-dyn-scanner-create(2)').first || DIM::System::ASN1::SystemCapability.create(:value => 'sc-dyn-scanner-create(2)')
  DIM::System::ASN1::SystemCapability.where(:value => 'sc-auto-init-scan-list(3)').first || DIM::System::ASN1::SystemCapability.create(:value => 'sc-auto-init-scan-list(3)')
  DIM::System::ASN1::SystemCapability.where(:value => 'sc-auto-updt-scan-list(4)').first || DIM::System::ASN1::SystemCapability.create(:value => 'sc-auto-updt-scan-list(4)')


  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-real-time-clock(0)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-real-time-clock(0)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-ebww(1)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-ebww(1)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-leap-second-aware(2)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-leap-second-aware(2)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-time-zone-aware(3)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-time-zone-aware(3)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-internal-only(4)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-internal-only(4)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-time-displayed(5)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-time-displayed(5)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-patient-care(6)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-patient-care(6)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-rtsa-time-sync-annotations(7)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-rtsa-time-sync-annotations(7)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-rtsa-time-sync-high-precision(8)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-rtsa-time-sync-high-precision(8)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-set-time-action-sup(16)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-set-time-action-sup(16)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-set-time-zone-action-sup(17)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-set-time-zone-action-sup(17)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-set-leap-sec-action-sup(18)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-set-leap-sec-action-sup(18)')
  DIM::System::ASN1::TimeCapability.where(:value => 'time-capab-set-time-iso-sup(19)').first || DIM::System::ASN1::TimeCapability.create(:value => 'time-capab-set-time-iso-sup(19)')


  DIM::Control::ASN1::AlOpCapab.where(:value => 'low-limit-sup(1)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'low-limit-sup(1)')
  DIM::Control::ASN1::AlOpCapab.where(:value => 'high-limit-sup(2)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'high-limit-sup(2)')
  DIM::Control::ASN1::AlOpCapab.where(:value => 'auto-limit-sup(5)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'auto-limit-sup(5)')
  DIM::Control::ASN1::AlOpCapab.where(:value => 'low-lim-on-off-sup(8)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'low-lim-on-off-sup(8)')
  DIM::Control::ASN1::AlOpCapab.where(:value => 'high-lim-on-off-sup(9)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'high-lim-on-off-sup(9)')
  DIM::Control::ASN1::AlOpCapab.where(:value => 'lim-on-off-sup(10)').first || DIM::Control::ASN1::AlOpCapab.create(:value => 'lim-on-off-sup(10)')


  DIM::Control::ASN1::CurLimAlStat.where(:value => 'lim-alert-off(0)').first || DIM::Control::ASN1::CurLimAlStat.create(:value => 'lim-alert-off(0)')
  DIM::Control::ASN1::CurLimAlStat.where(:value => 'lim-low-off(1)').first || DIM::Control::ASN1::CurLimAlStat.create(:value => 'lim-low-off(1)')
  DIM::Control::ASN1::CurLimAlStat.where(:value => 'lim-high-off(2)').first || DIM::Control::ASN1::CurLimAlStat.create(:value => 'lim-high-off(2)')


  DIM::Control::ASN1::OpInvResult.where(:value => 'op-successful(0)').first || DIM::Control::ASN1::OpInvResult.create(:value => 'op-successful(0)')
  DIM::Control::ASN1::OpInvResult.where(:value => 'op-failure(1)').first || DIM::Control::ASN1::OpInvResult.create(:value => 'op-failure(1)')


  DIM::Control::ASN1::OpLevel.where(:value => 'op-level-basic(0)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-level-basic(0)')
  DIM::Control::ASN1::OpLevel.where(:value => 'op-level-advanced(1)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-level-advanced(1)')
  DIM::Control::ASN1::OpLevel.where(:value => 'op-level-professional(2)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-level-professional(2)')
  DIM::Control::ASN1::OpLevel.where(:value => 'op-item-normal(8)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-item-normal(8)')
  DIM::Control::ASN1::OpLevel.where(:value => 'op-item-config(9)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-item-config(9)')
  DIM::Control::ASN1::OpLevel.where(:value => 'op-item-service(10)').first || DIM::Control::ASN1::OpLevel.create(:value => 'op-item-service(10)')


  DIM::Control::ASN1::OpModType.where(:value => 'op-replace(0)').first || DIM::Control::ASN1::OpModType.create(:value => 'op-replace(0)')
  DIM::Control::ASN1::OpModType.where(:value => 'op-setToDefault(3)').first || DIM::Control::ASN1::OpModType.create(:value => 'op-setToDefault(3)')
  DIM::Control::ASN1::OpModType.where(:value => 'op-invokeAction(10)').first || DIM::Control::ASN1::OpModType.create(:value => 'op-invokeAction(10)')
  DIM::Control::ASN1::OpModType.where(:value => 'op-invokeActionWithArgs(15)').first || DIM::Control::ASN1::OpModType.create(:value => 'op-invokeActionWithArgs(15)')


  DIM::Control::ASN1::OpOptions.where(:value => 'needs-confirmation(0)').first || DIM::Control::ASN1::OpOptions.create(:value => 'needs-confirmation(0)')
  DIM::Control::ASN1::OpOptions.where(:value => 'supports-default(1)').first || DIM::Control::ASN1::OpOptions.create(:value => 'supports-default(1)')
  DIM::Control::ASN1::OpOptions.where(:value => 'sets-sco-lock(2)').first || DIM::Control::ASN1::OpOptions.create(:value => 'sets-sco-lock(2)')
  DIM::Control::ASN1::OpOptions.where(:value => 'is-setting(3)').first || DIM::Control::ASN1::OpOptions.create(:value => 'is-setting(3)')
  DIM::Control::ASN1::OpOptions.where(:value => 'op-dependency(6)').first || DIM::Control::ASN1::OpOptions.create(:value => 'op-dependency(6)')
  DIM::Control::ASN1::OpOptions.where(:value => 'op-auto-repeat(7)').first || DIM::Control::ASN1::OpOptions.create(:value => 'op-auto-repeat(7)')
  DIM::Control::ASN1::OpOptions.where(:value => 'op-ctxt-help(8)').first || DIM::Control::ASN1::OpOptions.create(:value => 'op-ctxt-help(8)')


  DIM::Control::ASN1::ScoActivityIndicator.where(:value => 'act-ind-off(0)').first || DIM::Control::ASN1::ScoActivityIndicator.create(:value => 'act-ind-off(0)')
  DIM::Control::ASN1::ScoActivityIndicator.where(:value => 'act-ind-on(1)').first || DIM::Control::ASN1::ScoActivityIndicator.create(:value => 'act-ind-on(1)')
  DIM::Control::ASN1::ScoActivityIndicator.where(:value => 'act-ind-blinking(2)').first || DIM::Control::ASN1::ScoActivityIndicator.create(:value => 'act-ind-blinking(2)')


  DIM::Control::ASN1::ScoCapability.where(:value => 'act-indicator(0)').first || DIM::Control::ASN1::ScoCapability.create(:value => 'act-indicator(0)')
  DIM::Control::ASN1::ScoCapability.where(:value => 'sco-locks(1)').first || DIM::Control::ASN1::ScoCapability.create(:value => 'sco-locks(1)')
  DIM::Control::ASN1::ScoCapability.where(:value => 'sco-ctxt-help(8)').first || DIM::Control::ASN1::ScoCapability.create(:value => 'sco-ctxt-help(8)')


  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'op-err-unspec(0)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'op-err-unspec(0)')
  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'checksum-error(1)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'checksum-error(1)')
  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'sco-lock-violation(2)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'sco-lock-violation(2)')
  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'unknown-operation(3)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'unknown-operation(3)')
  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'invalid-value(4)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'invalid-value(4)')
  DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.where(:value => 'invalid-mod-type(5)').first || DIM::Control::ASN1::ScoOperInvokeError_op_error_ANON_ENUM.create(:value => 'invalid-mod-type(5)')


  DIM::Control::ASN1::SetStrOpt.where(:value => 'setstr-null-terminated(0)').first || DIM::Control::ASN1::SetStrOpt.create(:value => 'setstr-null-terminated(0)')
  DIM::Control::ASN1::SetStrOpt.where(:value => 'setstr-displayable(1)').first || DIM::Control::ASN1::SetStrOpt.create(:value => 'setstr-displayable(1)')
  DIM::Control::ASN1::SetStrOpt.where(:value => 'setstr-var-length(2)').first || DIM::Control::ASN1::SetStrOpt.create(:value => 'setstr-var-length(2)')
  DIM::Control::ASN1::SetStrOpt.where(:value => 'setstr-hidden-val(3)').first || DIM::Control::ASN1::SetStrOpt.create(:value => 'setstr-hidden-val(3)')


  DIM::Control::ASN1::ToggleState.where(:value => 'tog-state0(0)').first || DIM::Control::ASN1::ToggleState.create(:value => 'tog-state0(0)')
  DIM::Control::ASN1::ToggleState.where(:value => 'tog-state1(1)').first || DIM::Control::ASN1::ToggleState.create(:value => 'tog-state1(1)')


  DIM::ExtendedServices::ASN1::ConfirmMode.where(:value => 'unconfirmed(0)').first || DIM::ExtendedServices::ASN1::ConfirmMode.create(:value => 'unconfirmed(0)')
  DIM::ExtendedServices::ASN1::ConfirmMode.where(:value => 'confirmed(1)').first || DIM::ExtendedServices::ASN1::ConfirmMode.create(:value => 'confirmed(1)')


  DIM::ExtendedServices::ASN1::ContextMode.where(:value => 'static-mode(0)').first || DIM::ExtendedServices::ASN1::ContextMode.create(:value => 'static-mode(0)')
  DIM::ExtendedServices::ASN1::ContextMode.where(:value => 'dynamic-mode(1)').first || DIM::ExtendedServices::ASN1::ContextMode.create(:value => 'dynamic-mode(1)')


  DIM::ExtendedServices::ASN1::ScanConfigLimit.where(:value => 'no-scan-delete(0)').first || DIM::ExtendedServices::ASN1::ScanConfigLimit.create(:value => 'no-scan-delete(0)')
  DIM::ExtendedServices::ASN1::ScanConfigLimit.where(:value => 'no-scan-list-mod(1)').first || DIM::ExtendedServices::ASN1::ScanConfigLimit.create(:value => 'no-scan-list-mod(1)')
  DIM::ExtendedServices::ASN1::ScanConfigLimit.where(:value => 'auto-init-scan-list(3)').first || DIM::ExtendedServices::ASN1::ScanConfigLimit.create(:value => 'auto-init-scan-list(3)')
  DIM::ExtendedServices::ASN1::ScanConfigLimit.where(:value => 'auto-updt-scan-list(4)').first || DIM::ExtendedServices::ASN1::ScanConfigLimit.create(:value => 'auto-updt-scan-list(4)')


  DIM::ExtendedServices::ASN1::ScanExtend.where(:value => 'extensive(0)').first || DIM::ExtendedServices::ASN1::ScanExtend.create(:value => 'extensive(0)')
  DIM::ExtendedServices::ASN1::ScanExtend.where(:value => 'superpositive(1)').first || DIM::ExtendedServices::ASN1::ScanExtend.create(:value => 'superpositive(1)')
  DIM::ExtendedServices::ASN1::ScanExtend.where(:value => 'superpositive-avg(2)').first || DIM::ExtendedServices::ASN1::ScanExtend.create(:value => 'superpositive-avg(2)')


  DIM::Communication::ASN1::CcCapability.where(:value => 'cc-sup-ext-mgmt-protocol(0)').first || DIM::Communication::ASN1::CcCapability.create(:value => 'cc-sup-ext-mgmt-protocol(0)')


  DIM::Communication::ASN1::CcExtMgmtProto.where(:value => 'mgmt-proto-snmp-v1(1)').first || DIM::Communication::ASN1::CcExtMgmtProto.create(:value => 'mgmt-proto-snmp-v1(1)')
  DIM::Communication::ASN1::CcExtMgmtProto.where(:value => 'mgmt-proto-snmp-v2(2)').first || DIM::Communication::ASN1::CcExtMgmtProto.create(:value => 'mgmt-proto-snmp-v2(2)')
  DIM::Communication::ASN1::CcExtMgmtProto.where(:value => 'mgmt-proto-snmp-v3(3)').first || DIM::Communication::ASN1::CcExtMgmtProto.create(:value => 'mgmt-proto-snmp-v3(3)')
  DIM::Communication::ASN1::CcExtMgmtProto.where(:value => 'mgmt-proto-cmip(16)').first || DIM::Communication::ASN1::CcExtMgmtProto.create(:value => 'mgmt-proto-cmip(16)')


  DIM::Communication::ASN1::DifMibPortState.where(:value => 'difmib-port-enabled(0)').first || DIM::Communication::ASN1::DifMibPortState.create(:value => 'difmib-port-enabled(0)')
  DIM::Communication::ASN1::DifMibPortState.where(:value => 'difmib-port-connected(1)').first || DIM::Communication::ASN1::DifMibPortState.create(:value => 'difmib-port-connected(1)')
  DIM::Communication::ASN1::DifMibPortState.where(:value => 'difmib-port-associated(2)').first || DIM::Communication::ASN1::DifMibPortState.create(:value => 'difmib-port-associated(2)')
  DIM::Communication::ASN1::DifMibPortState.where(:value => 'difmib-port-failure(15)').first || DIM::Communication::ASN1::DifMibPortState.create(:value => 'difmib-port-failure(15)')


  DIM::Communication::ASN1::MibCcCommMode.where(:value => 'comm-mode-simplex(0)').first || DIM::Communication::ASN1::MibCcCommMode.create(:value => 'comm-mode-simplex(0)')
  DIM::Communication::ASN1::MibCcCommMode.where(:value => 'comm-mode-half-duplex(1)').first || DIM::Communication::ASN1::MibCcCommMode.create(:value => 'comm-mode-half-duplex(1)')
  DIM::Communication::ASN1::MibCcCommMode.where(:value => 'comm-mode-full-duplex(2)').first || DIM::Communication::ASN1::MibCcCommMode.create(:value => 'comm-mode-full-duplex(2)')


  DIM::Patient::ASN1::PatDemoState.where(:value => 'empty(0)').first || DIM::Patient::ASN1::PatDemoState.create(:value => 'empty(0)')
  DIM::Patient::ASN1::PatDemoState.where(:value => 'pre-admitted(1)').first || DIM::Patient::ASN1::PatDemoState.create(:value => 'pre-admitted(1)')
  DIM::Patient::ASN1::PatDemoState.where(:value => 'admitted(2)').first || DIM::Patient::ASN1::PatDemoState.create(:value => 'admitted(2)')
  DIM::Patient::ASN1::PatDemoState.where(:value => 'discharged(8)').first || DIM::Patient::ASN1::PatDemoState.create(:value => 'discharged(8)')


  DIM::Patient::ASN1::PatientRace.where(:value => 'race-unspecified(0)').first || DIM::Patient::ASN1::PatientRace.create(:value => 'race-unspecified(0)')
  DIM::Patient::ASN1::PatientRace.where(:value => 'race-caucasian(1)').first || DIM::Patient::ASN1::PatientRace.create(:value => 'race-caucasian(1)')
  DIM::Patient::ASN1::PatientRace.where(:value => 'race-black(2)').first || DIM::Patient::ASN1::PatientRace.create(:value => 'race-black(2)')
  DIM::Patient::ASN1::PatientRace.where(:value => 'race-oriental(3)').first || DIM::Patient::ASN1::PatientRace.create(:value => 'race-oriental(3)')


  DIM::Patient::ASN1::PatientSex.where(:value => 'sex-unknown(0)').first || DIM::Patient::ASN1::PatientSex.create(:value => 'sex-unknown(0)')
  DIM::Patient::ASN1::PatientSex.where(:value => 'male(1)').first || DIM::Patient::ASN1::PatientSex.create(:value => 'male(1)')
  DIM::Patient::ASN1::PatientSex.where(:value => 'female(2)').first || DIM::Patient::ASN1::PatientSex.create(:value => 'female(2)')
  DIM::Patient::ASN1::PatientSex.where(:value => 'sex-unspecified(9)').first || DIM::Patient::ASN1::PatientSex.create(:value => 'sex-unspecified(9)')


  DIM::Patient::ASN1::PatientType.where(:value => 'pt-unspecified(0)').first || DIM::Patient::ASN1::PatientType.create(:value => 'pt-unspecified(0)')
  DIM::Patient::ASN1::PatientType.where(:value => 'adult(1)').first || DIM::Patient::ASN1::PatientType.create(:value => 'adult(1)')
  DIM::Patient::ASN1::PatientType.where(:value => 'pediatric(2)').first || DIM::Patient::ASN1::PatientType.create(:value => 'pediatric(2)')
  DIM::Patient::ASN1::PatientType.where(:value => 'neonatal(3)').first || DIM::Patient::ASN1::PatientType.create(:value => 'neonatal(3)')


  ICS::CSCategory.where(:value => 'DIM Statement').first || ICS::CSCategory.create(:value => 'DIM Statement')
  ICS::CSCategory.where(:value => 'Attribute Statement').first || ICS::CSCategory.create(:value => 'Attribute Statement')
  ICS::CSCategory.where(:value => 'Behavior Statement').first || ICS::CSCategory.create(:value => 'Behavior Statement')
  ICS::CSCategory.where(:value => 'Notification Statement').first || ICS::CSCategory.create(:value => 'Notification Statement')
  ICS::CSCategory.where(:value => 'Baseline Statement').first || ICS::CSCategory.create(:value => 'Baseline Statement')
  ICS::CSCategory.where(:value => 'Polling Statement').first || ICS::CSCategory.create(:value => 'Polling Statement')
  ICS::CSCategory.where(:value => 'General Statement').first || ICS::CSCategory.create(:value => 'General Statement')
  ICS::CSCategory.where(:value => 'Interoperability Statement').first || ICS::CSCategory.create(:value => 'Interoperability Statement')
  ICS::CSCategory.where(:value => 'Service Statement').first || ICS::CSCategory.create(:value => 'Service Statement')
  ICS::CSCategory.where(:value => 'Top Level Statement').first || ICS::CSCategory.create(:value => 'Top Level Statement')


  ICS::StatusAndSupport.where(:value => 'mandatory').first || ICS::StatusAndSupport.create(:value => 'mandatory')
  ICS::StatusAndSupport.where(:value => 'optional').first || ICS::StatusAndSupport.create(:value => 'optional')
  ICS::StatusAndSupport.where(:value => 'prohibited').first || ICS::StatusAndSupport.create(:value => 'prohibited')
  ICS::StatusAndSupport.where(:value => 'conditional').first || ICS::StatusAndSupport.create(:value => 'conditional')
  ICS::StatusAndSupport.where(:value => 'not applicable').first || ICS::StatusAndSupport.create(:value => 'not applicable')


  MetaInfo::Decoration.where(:value => 'aggregation').first || MetaInfo::Decoration.create(:value => 'aggregation')
  MetaInfo::Decoration.where(:value => 'composition').first || MetaInfo::Decoration.create(:value => 'composition')


  MetaInfo::Mode.where(:value => 'Confirmed').first || MetaInfo::Mode.create(:value => 'Confirmed')
  MetaInfo::Mode.where(:value => 'Unconfirmed').first || MetaInfo::Mode.create(:value => 'Unconfirmed')
  MetaInfo::Mode.where(:value => 'Confirmed/Unconfirmed').first || MetaInfo::Mode.create(:value => 'Confirmed/Unconfirmed')


  MetaInfo::Optionality.where(:value => 'mandatory').first || MetaInfo::Optionality.create(:value => 'mandatory')
  MetaInfo::Optionality.where(:value => 'optional').first || MetaInfo::Optionality.create(:value => 'optional')
  MetaInfo::Optionality.where(:value => 'conditional').first || MetaInfo::Optionality.create(:value => 'conditional')
  MetaInfo::Optionality.where(:value => 'mandatory (11073)').first || MetaInfo::Optionality.create(:value => 'mandatory (11073)')


  MetaInfo::UpdateType.where(:value => 'Static').first || MetaInfo::UpdateType.create(:value => 'Static')
  MetaInfo::UpdateType.where(:value => 'Dynamic').first || MetaInfo::UpdateType.create(:value => 'Dynamic')
  MetaInfo::UpdateType.where(:value => 'Observational').first || MetaInfo::UpdateType.create(:value => 'Observational')


  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-unspec(0)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-unspec(0)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-obj(1)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-obj(1)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-metric(2)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-metric(2)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-alert(3)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-alert(3)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-dim(4)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-dim(4)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-vattr(5)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-vattr(5)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-pgrp(6)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-pgrp(6)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-sites(7)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-sites(7)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-infrastruct(8)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-infrastruct(8)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-fef(9)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-fef(9)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-ecg-extn(10)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-ecg-extn(10)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-idco-extn(11)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-idco-extn(11)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-phd-dim(128)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-phd-dim(128)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-phd-hf(129)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-phd-hf(129)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-phd-ai(130)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-phd-ai(130)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-ret-code(255)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-ret-code(255)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-ext-nom(256)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-ext-nom(256)')
  PHD::ASN1::CommonDataTypes::NomPartition.where(:value => 'nom-part-priv(1024)').first || PHD::ASN1::CommonDataTypes::NomPartition.create(:value => 'nom-part-priv(1024)')


  PHD::ASN1::CommonDataTypes::OperationalState.where(:value => 'disabled(0)').first || PHD::ASN1::CommonDataTypes::OperationalState.create(:value => 'disabled(0)')
  PHD::ASN1::CommonDataTypes::OperationalState.where(:value => 'enabled(1)').first || PHD::ASN1::CommonDataTypes::OperationalState.create(:value => 'enabled(1)')
  PHD::ASN1::CommonDataTypes::OperationalState.where(:value => 'notAvailable(2)').first || PHD::ASN1::CommonDataTypes::OperationalState.create(:value => 'notAvailable(2)')


  PHD::ASN1::AuthBody.where(:value => 'auth-body-empty(0)').first || PHD::ASN1::AuthBody.create(:value => 'auth-body-empty(0)')
  PHD::ASN1::AuthBody.where(:value => 'auth-body-ieee-11073(1)').first || PHD::ASN1::AuthBody.create(:value => 'auth-body-ieee-11073(1)')
  PHD::ASN1::AuthBody.where(:value => 'auth-body-continua(2)').first || PHD::ASN1::AuthBody.create(:value => 'auth-body-continua(2)')
  PHD::ASN1::AuthBody.where(:value => 'auth-body-experimental(254)').first || PHD::ASN1::AuthBody.create(:value => 'auth-body-experimental(254)')
  PHD::ASN1::AuthBody.where(:value => 'auth-body-reserved(255)').first || PHD::ASN1::AuthBody.create(:value => 'auth-body-reserved(255)')


  PHD::ASN1::ConfigurationId.where(:value => 'manager-config-response(0)').first || PHD::ASN1::ConfigurationId.create(:value => 'manager-config-response(0)')
  PHD::ASN1::ConfigurationId.where(:value => 'standard-config-start(1)').first || PHD::ASN1::ConfigurationId.create(:value => 'standard-config-start(1)')
  PHD::ASN1::ConfigurationId.where(:value => 'standard-config-end(16383)').first || PHD::ASN1::ConfigurationId.create(:value => 'standard-config-end(16383)')
  PHD::ASN1::ConfigurationId.where(:value => 'extended-config-start(16384)').first || PHD::ASN1::ConfigurationId.create(:value => 'extended-config-start(16384)')
  PHD::ASN1::ConfigurationId.where(:value => 'extended-config-end(32767)').first || PHD::ASN1::ConfigurationId.create(:value => 'extended-config-end(32767)')
  PHD::ASN1::ConfigurationId.where(:value => 'reserved-start(32768)').first || PHD::ASN1::ConfigurationId.create(:value => 'reserved-start(32768)')
  PHD::ASN1::ConfigurationId.where(:value => 'reserved-end(65535)').first || PHD::ASN1::ConfigurationId.create(:value => 'reserved-end(65535)')


  PHD::ASN1::ConfirmMode.where(:value => 'unconfirmed(0)').first || PHD::ASN1::ConfirmMode.create(:value => 'unconfirmed(0)')
  PHD::ASN1::ConfirmMode.where(:value => 'confirmed(1)').first || PHD::ASN1::ConfirmMode.create(:value => 'confirmed(1)')


  PHD::ASN1::CurLimAlStat.where(:value => 'lim-alert-off(0)').first || PHD::ASN1::CurLimAlStat.create(:value => 'lim-alert-off(0)')
  PHD::ASN1::CurLimAlStat.where(:value => 'lim-low-off(1)').first || PHD::ASN1::CurLimAlStat.create(:value => 'lim-low-off(1)')
  PHD::ASN1::CurLimAlStat.where(:value => 'lim-high-off(2)').first || PHD::ASN1::CurLimAlStat.create(:value => 'lim-high-off(2)')


  PHD::ASN1::MS_Struct.where(:value => 'ms-struct-simple(0)').first || PHD::ASN1::MS_Struct.create(:value => 'ms-struct-simple(0)')
  PHD::ASN1::MS_Struct.where(:value => 'ms-struct-compound(1)').first || PHD::ASN1::MS_Struct.create(:value => 'ms-struct-compound(1)')
  PHD::ASN1::MS_Struct.where(:value => 'ms-struct-reserved(2)').first || PHD::ASN1::MS_Struct.create(:value => 'ms-struct-reserved(2)')
  PHD::ASN1::MS_Struct.where(:value => 'ms-struct-compound-fix(3)').first || PHD::ASN1::MS_Struct.create(:value => 'ms-struct-compound-fix(3)')


  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-real-time-clock(0)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-real-time-clock(0)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-set-clock(1)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-set-clock(1)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-relative-time(2)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-relative-time(2)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-high-res-relative-time(3)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-high-res-relative-time(3)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-sync-abs-time(4)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-sync-abs-time(4)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-sync-rel-time(5)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-sync-rel-time(5)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-bo-time(7)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-bo-time(7)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-state-abs-time-synced(8)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-state-abs-time-synced(8)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-state-rel-time-synced(9)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-state-rel-time-synced(9)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-state-hi-res-relative-time-synced(10)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-state-hi-res-relative-time-synced(10)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-mgr-set-time(11)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-mgr-set-time(11)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-capab-sync-bo-time(12)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-capab-sync-bo-time(12)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-state-bo-time-synced(13)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-state-bo-time-synced(13)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-state-bo-time-UTC-aligned(14)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-state-bo-time-UTC-aligned(14)')
  PHD::ASN1::MdsTimeCapState.where(:value => 'mds-time-dst-rules-enabled(15)').first || PHD::ASN1::MdsTimeCapState.create(:value => 'mds-time-dst-rules-enabled(15)')


  PHD::ASN1::MeasurementStatus.where(:value => 'invalid(0)').first || PHD::ASN1::MeasurementStatus.create(:value => 'invalid(0)')
  PHD::ASN1::MeasurementStatus.where(:value => 'questionable(1)').first || PHD::ASN1::MeasurementStatus.create(:value => 'questionable(1)')
  PHD::ASN1::MeasurementStatus.where(:value => 'not-available(2)').first || PHD::ASN1::MeasurementStatus.create(:value => 'not-available(2)')
  PHD::ASN1::MeasurementStatus.where(:value => 'calibration-ongoing(3)').first || PHD::ASN1::MeasurementStatus.create(:value => 'calibration-ongoing(3)')
  PHD::ASN1::MeasurementStatus.where(:value => 'test-data(4)').first || PHD::ASN1::MeasurementStatus.create(:value => 'test-data(4)')
  PHD::ASN1::MeasurementStatus.where(:value => 'demo-data(5)').first || PHD::ASN1::MeasurementStatus.create(:value => 'demo-data(5)')
  PHD::ASN1::MeasurementStatus.where(:value => 'validated-data(8)').first || PHD::ASN1::MeasurementStatus.create(:value => 'validated-data(8)')
  PHD::ASN1::MeasurementStatus.where(:value => 'early-indication(9)').first || PHD::ASN1::MeasurementStatus.create(:value => 'early-indication(9)')
  PHD::ASN1::MeasurementStatus.where(:value => 'msmt-ongoing(10)').first || PHD::ASN1::MeasurementStatus.create(:value => 'msmt-ongoing(10)')
  PHD::ASN1::MeasurementStatus.where(:value => 'msmt-value-exceed-boundaries(14)').first || PHD::ASN1::MeasurementStatus.create(:value => 'msmt-value-exceed-boundaries(14)')
  PHD::ASN1::MeasurementStatus.where(:value => 'msmt-state-ann-inhibited(15)').first || PHD::ASN1::MeasurementStatus.create(:value => 'msmt-state-ann-inhibited(15)')


  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-avail-intermittent(0)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-avail-intermittent(0)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-avail-stored-data(1)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-avail-stored-data(1)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-upd-aperiodic(2)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-upd-aperiodic(2)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-msmt-aperiodic(3)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-msmt-aperiodic(3)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-msmt-phys-ev-id(4)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-msmt-phys-ev-id(4)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-msmt-btb-metric(5)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-msmt-btb-metric(5)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-acc-manager-initiated (8)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-acc-manager-initiated (8)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-acc-agent-initiated(9)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-acc-agent-initiated(9)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-cat-manual(12)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-cat-manual(12)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-cat-setting(13)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-cat-setting(13)')
  PHD::ASN1::MetricSpecSmall.where(:value => 'mss-cat-calculation(14)').first || PHD::ASN1::MetricSpecSmall.create(:value => 'mss-cat-calculation(14)')


  PHD::ASN1::PowerStatus.where(:value => 'onMains(0)').first || PHD::ASN1::PowerStatus.create(:value => 'onMains(0)')
  PHD::ASN1::PowerStatus.where(:value => 'onBattery(1)').first || PHD::ASN1::PowerStatus.create(:value => 'onBattery(1)')
  PHD::ASN1::PowerStatus.where(:value => 'chargingFull(8)').first || PHD::ASN1::PowerStatus.create(:value => 'chargingFull(8)')
  PHD::ASN1::PowerStatus.where(:value => 'chargingTrickle(9)').first || PHD::ASN1::PowerStatus.create(:value => 'chargingTrickle(9)')
  PHD::ASN1::PowerStatus.where(:value => 'chargingOff(10)').first || PHD::ASN1::PowerStatus.create(:value => 'chargingOff(10)')


  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'unspecified(0)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'unspecified(0)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'serial-number(1)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'serial-number(1)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'part-number(2)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'part-number(2)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'hw-revision(3)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'hw-revision(3)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'sw-revision(4)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'sw-revision(4)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'fw-revision(5)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'fw-revision(5)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'protocol-revision(6)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'protocol-revision(6)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'prod-spec-gmdn(7)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'prod-spec-gmdn(7)')
  PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.where(:value => 'fda-udi(8)').first || PHD::ASN1::ProdSpecEntrySpecType_ANON_ENUM.create(:value => 'fda-udi(8)')


  PHD::ASN1::SaFlags.where(:value => 'smooth-curve(0)').first || PHD::ASN1::SaFlags.create(:value => 'smooth-curve(0)')
  PHD::ASN1::SaFlags.where(:value => 'delayed-curve(1)').first || PHD::ASN1::SaFlags.create(:value => 'delayed-curve(1)')
  PHD::ASN1::SaFlags.where(:value => 'static-scale(2)').first || PHD::ASN1::SaFlags.create(:value => 'static-scale(2)')
  PHD::ASN1::SaFlags.where(:value => 'sa-ext-val-range(3)').first || PHD::ASN1::SaFlags.create(:value => 'sa-ext-val-range(3)')


  PHD::ASN1::SegmSelectionType.where(:value => 'all-segments').first || PHD::ASN1::SegmSelectionType.create(:value => 'all-segments')
  PHD::ASN1::SegmSelectionType.where(:value => 'segm-id-list').first || PHD::ASN1::SegmSelectionType.create(:value => 'segm-id-list')
  PHD::ASN1::SegmSelectionType.where(:value => 'abs-time-range').first || PHD::ASN1::SegmSelectionType.create(:value => 'abs-time-range')
  PHD::ASN1::SegmSelectionType.where(:value => 'bo-time-range').first || PHD::ASN1::SegmSelectionType.create(:value => 'bo-time-range')


  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-nos(0)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-nos(0)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-moving-average(1)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-moving-average(1)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-recursive(2)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-recursive(2)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-min-pick(3)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-min-pick(3)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-max-pick(4)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-max-pick(4)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-median(5)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-median(5)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-trended(512)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-trended(512)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-no-downsampling(1024)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-no-downsampling(1024)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-manuf-specific-start(61440)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-manuf-specific-start(61440)')
  PHD::ASN1::StoSampleAlg.where(:value => 'st-alg-manuf-specific-end(65535)').first || PHD::ASN1::StoSampleAlg.create(:value => 'st-alg-manuf-specific-end(65535)')


  Standard::RenderingType.where(:value => 'default').first || Standard::RenderingType.create(:value => 'default')
  Standard::RenderingType.where(:value => 'inherit').first || Standard::RenderingType.create(:value => 'inherit')
  Standard::RenderingType.where(:value => 'overview').first || Standard::RenderingType.create(:value => 'overview')
  Standard::RenderingType.where(:value => 'class_definition').first || Standard::RenderingType.create(:value => 'class_definition')
  Standard::RenderingType.where(:value => 'ASN1_code').first || Standard::RenderingType.create(:value => 'ASN1_code')


  Nomenclature::TermSource.where(:value => 'RTM').first || Nomenclature::TermSource.create(:value => 'RTM')
  Nomenclature::TermSource.where(:value => 'HRTM').first || Nomenclature::TermSource.create(:value => 'HRTM')
  Nomenclature::TermSource.where(:value => 'X73').first || Nomenclature::TermSource.create(:value => 'X73')
  Nomenclature::TermSource.where(:value => 'Profile Editor').first || Nomenclature::TermSource.create(:value => 'Profile Editor')


  Nomenclature::TermStatus.where(:value => 'Approved').first || Nomenclature::TermStatus.create(:value => 'Approved')
  Nomenclature::TermStatus.where(:value => 'Proposed').first || Nomenclature::TermStatus.create(:value => 'Proposed')
  Nomenclature::TermStatus.where(:value => 'Mapped').first || Nomenclature::TermStatus.create(:value => 'Mapped')
  Nomenclature::TermStatus.where(:value => 'Deleted').first || Nomenclature::TermStatus.create(:value => 'Deleted')
  Nomenclature::TermStatus.where(:value => 'Invalid').first || Nomenclature::TermStatus.create(:value => 'Invalid')


  Nomenclature::TermType.where(:value => 'metric').first || Nomenclature::TermType.create(:value => 'metric')
  Nomenclature::TermType.where(:value => 'unit').first || Nomenclature::TermType.create(:value => 'unit')
  Nomenclature::TermType.where(:value => 'enumeration').first || Nomenclature::TermType.create(:value => 'enumeration')
  Nomenclature::TermType.where(:value => 'literal').first || Nomenclature::TermType.create(:value => 'literal')
  Nomenclature::TermType.where(:value => 'token').first || Nomenclature::TermType.create(:value => 'token')
  Nomenclature::TermType.where(:value => 'unknown').first || Nomenclature::TermType.create(:value => 'unknown')
  Nomenclature::TermType.where(:value => 'term').first || Nomenclature::TermType.create(:value => 'term')


  MyDevice::ChoiceHelper::CtxtHelpType.where(:value => 'text-string').first || MyDevice::ChoiceHelper::CtxtHelpType.create(:value => 'text-string')
  MyDevice::ChoiceHelper::CtxtHelpType.where(:value => 'oid').first || MyDevice::ChoiceHelper::CtxtHelpType.create(:value => 'oid')


  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-obj-id').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-obj-id')
  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-text-string').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-text-string')
  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-external-code').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-external-code')
  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-bit-str').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-bit-str')
  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-record-oo').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-record-oo')
  MyDevice::ChoiceHelper::EnumValType.where(:value => 'enum-numeral').first || MyDevice::ChoiceHelper::EnumValType.create(:value => 'enum-numeral')


  MyDevice::ChoiceHelper::SegmSelectionType.where(:value => 'all-segments').first || MyDevice::ChoiceHelper::SegmSelectionType.create(:value => 'all-segments')
  MyDevice::ChoiceHelper::SegmSelectionType.where(:value => 'segm-id-list').first || MyDevice::ChoiceHelper::SegmSelectionType.create(:value => 'segm-id-list')
  MyDevice::ChoiceHelper::SegmSelectionType.where(:value => 'abs-time-range').first || MyDevice::ChoiceHelper::SegmSelectionType.create(:value => 'abs-time-range')


  MyDevice::ChoiceHelper::SelectListType.where(:value => 'oid-list').first || MyDevice::ChoiceHelper::SelectListType.create(:value => 'oid-list')
  MyDevice::ChoiceHelper::SelectListType.where(:value => 'value-list').first || MyDevice::ChoiceHelper::SelectListType.create(:value => 'value-list')
  MyDevice::ChoiceHelper::SelectListType.where(:value => 'value-u-lst').first || MyDevice::ChoiceHelper::SelectListType.create(:value => 'value-u-lst')
  MyDevice::ChoiceHelper::SelectListType.where(:value => 'string-list').first || MyDevice::ChoiceHelper::SelectListType.create(:value => 'string-list')


  MyDevice::DeviceType.where(:value => 'PCD').first || MyDevice::DeviceType.create(:value => 'PCD')
  MyDevice::DeviceType.where(:value => 'PHD').first || MyDevice::DeviceType.create(:value => 'PHD')


  MyDevice::IntendedUse.where(:value => 'User Defined').first || MyDevice::IntendedUse.create(:value => 'User Defined')
  MyDevice::IntendedUse.where(:value => 'Normative (11073)').first || MyDevice::IntendedUse.create(:value => 'Normative (11073)')


  MyDevice::ProfileMode.where(:value => 'Baseline').first || MyDevice::ProfileMode.create(:value => 'Baseline')
  MyDevice::ProfileMode.where(:value => 'Polling').first || MyDevice::ProfileMode.create(:value => 'Polling')


  MyDevice::ProfileType.where(:value => 'Agent').first || MyDevice::ProfileType.create(:value => 'Agent')
  MyDevice::ProfileType.where(:value => 'Manager').first || MyDevice::ProfileType.create(:value => 'Manager')

ChangeTracker.commit if defined?(ChangeTracker)