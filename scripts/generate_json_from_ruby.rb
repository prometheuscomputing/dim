#!/usr/bin/env ruby
require 'json_graph'  # Must be first to have JSON_Graph::Serializeable
require './ElectionResults'

o1 = ElectionResults::Office.new
o1.name = 'Mayor'
c1 = ElectionResults::CandidateChoice.new
c1.votesAllowed = 1
c1.office = o1

o2 = ElectionResults::Office.new
o2.name = 'Comptroller'
c2 = ElectionResults::CandidateChoice.new
c2.votesAllowed = 4
c2.numberElected = 2
c2.office = o2

e = ElectionResults::Election.new
e.contests = [c1, c2]
e.name = 'Special Election'
e.date = Time.now
e.uRL = 'www.specialelectionresults-fakewebsite.com'

options = {:max_nesting => 1000}
content = JSON_Graph.pretty_generate(e, options)
File.open('./example.json', 'w') {|f| f << content }