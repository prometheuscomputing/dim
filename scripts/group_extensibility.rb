group_extensibility = {
  "MDC_ATTR_GRP_AL"              => false,
  "MDC_ATTR_GRP_AL_MON"          => false,
  "MDC_ATTR_GRP_AL_STAT"         => false,
  "MDC_ATTR_GRP_ARCHIVE"         => false,
  "MDC_ATTR_GRP_BATT"            => false,
  "MDC_ATTR_GRP_CC"              => true,
  "MDC_ATTR_GRP_CLOCK"           => false,
  "MDC_ATTR_GRP_METRIC_VAL_OBS"  => true,
  "MDC_ATTR_GRP_OP_DYN_CTXT"     => true,
  "MDC_ATTR_GRP_OP_STATIC_CTXT"  => true,
  "MDC_ATTR_GRP_PHYSICIAN"       => false,
  "MDC_ATTR_GRP_PMSTORE"         => false,
  "MDC_ATTR_GRP_PT_DEMOG"        => false,
  "MDC_ATTR_GRP_RELATION"        => false,
  "MDC_ATTR_GRP_SCAN"            => true,
  "MDC_ATTR_GRP_SCO_TRANSACTION" => false,
  "MDC_ATTR_GRP_SYS_APPL"        => true,
  "MDC_ATTR_GRP_SYS_ID"          => true,
  "MDC_ATTR_GRP_SYS_PROD"        => true,
  "MDC_ATTR_GRP_VMD_APPL"        => false,
  "MDC_ATTR_GRP_VMD_PROD"        => false,
  "MDC_ATTR_GRP_VMO_DYN"         => true,
  "MDC_ATTR_GRP_VMO_STATIC"      => true
}

def make_extensible attribute_grp_id, bool
  ChangeTracker.commit {
    gg = MetaInfo::Group.where(:attribute_group_id => attribute_grp_id).all
    gg.each{|g|
      g.extensible = bool
      g.save
    }
  }
end
group_extensibility.each {|id,bool| make_extensible id, bool}