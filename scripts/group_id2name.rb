id2name = {
  "MDC_ATTR_GRP_AL"              => "Alert Group",
  "MDC_ATTR_GRP_AL_MON"          => "Alert Monitor Group",
  "MDC_ATTR_GRP_AL_STAT"         => "Alert Status Group",
  "MDC_ATTR_GRP_ARCHIVE"         => "Archival Attribute Group",
  "MDC_ATTR_GRP_BATT"            => "Battery Attribute Group",
  "MDC_ATTR_GRP_CC"              => "Communication Controller Attribute Group",
  "MDC_ATTR_GRP_CLOCK"           => "Clock Attribute Group",
  "MDC_ATTR_GRP_METRIC_VAL_OBS"  => "Metric Observed Value Group",
  "MDC_ATTR_GRP_OP_DYN_CTXT"     => "Operation Dynamic Context Group",
  "MDC_ATTR_GRP_OP_STATIC_CTXT"  => "Operation Static Context Group",
  "MDC_ATTR_GRP_PHYSICIAN"       => "Physician Attribute Group",
  "MDC_ATTR_GRP_PMSTORE"         => "PM-Store Attribute Group",
  "MDC_ATTR_GRP_PT_DEMOG"        => "Patient Demographics Attribute Group",
  "MDC_ATTR_GRP_RELATION"        => "Relationship Attribute Group",
  "MDC_ATTR_GRP_SCAN"            => "Scanner Attribute Group",
  "MDC_ATTR_GRP_SCO_TRANSACTION" => "SCO Transaction Group",
  "MDC_ATTR_GRP_SYS_APPL"        => "System Application Attribute Group",
  "MDC_ATTR_GRP_SYS_ID"          => "System Identification Attribute Group",
  "MDC_ATTR_GRP_SYS_PROD"        => "System Production Attribute Group",
  "MDC_ATTR_GRP_VMD_APPL"        => "VMD Application Attribute Group",
  "MDC_ATTR_GRP_VMD_PROD"        => "VMD Production Attribute Group",
  "MDC_ATTR_GRP_VMO_DYN"         => "VMO Dynamic Context Group",
  "MDC_ATTR_GRP_VMO_STATIC"      => "VMO Static Context Group"
}
id2name.each do |id,name|
  groups = MetaInfo::Group.where(:attribute_group_id => id).all
  groups.each{|g| ChangeTracker.commit {g.name = name; g.save}}
end