raise 'OLD.  DO NOT DO THIS! Use the RTMMS webservice, not local files.'
# for use in irb
Hirb.disable if defined? Hirb
def ct
  ChangeTracker.commit
  ChangeTracker.start
end
ChangeTracker.start unless ChangeTracker.started?

term = Nomenclature::Term

file_name1 = File.expand_path "~/projects/dim_reference/rtmms_from_ics_generator/dim_terms.rtmms"
file_name2 = File.expand_path "~/projects/dim_reference/rtmms_from_ics_generator/11073_nomenclature.rtmms"
file_name3 = File.expand_path "~/projects/dim_reference/rtmms_from_ics_generator/HRosetta2011.rtmms"

f1 = File.open(file_name1, "r").each_line do |l|
  next if l =~ /^ *#/
  name, refid, part_code, clause = l.split(",")
  clause.chomp!
  # partition, code = part_code.split("::")
  # clause should not be on the terminology.  it is stored on the class itself
  t = term.create(:reference_id => refid, :term_code => part_code, :common_term => name)
  # TermPartition only has a RichText, which doesn't make sense to me.  It needs to have a place to record the partition
  # p = term_partition.where(:partition_description => partition).first || term_partition.create(:partition_description => partition)
  # t.termpartition = p
  t.save
  ct
end

f2 = File.open(file_name2, "r").each_line do |l|
  next if l =~ /^ *#/
  ref_id, part_code = l.split(",")
  unless t = term.where(:reference_id => ref_id).first
    t = term.create(:reference_id => ref_id, :term_code => part_code)
    t.save
    ct
  end
end

f3 = File.open(file_name3, "r").each_line do |l|
  next if l =~ /^ *#/
  ref_id, cf_code10, group, dim, uom_mdc, uom_ucum, cf_ucode10, enum_values, vendor_descriptions = l.split(",")
  # The model looks like a total mess.  No point in stuff all of this stuff into it while it is in this shape
  unless t = term.where(:reference_id => ref_id).first
    t = term.create(:reference_id => ref_id)
    t.save
    ct
  end
end