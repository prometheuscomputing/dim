puts "ChangeTracker mode is #{ChangeTracker.mode}"
load File.expand_path('~/projects/dim/scripts/dim_standard.rb')
input_dir = File.expand_path("~/projects/dim_latex/latex")

load File.expand_path('~/projects/dim/scripts/insert_abbreviations.rb')
load File.expand_path('~/projects/dim/scripts/insert_definitions.rb')
load File.expand_path('~/projects/dim/scripts/insert_references.rb')
load File.expand_path('~/projects/dim/scripts/insert_dim_clause.rb')
ChangeTracker.start unless ChangeTracker.started?

input_filename = "general_requirements.tex"
input = File.read(File.join(input_dir, input_filename))
input.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
input.encode!('UTF-8', 'UTF-16')
input = input.fix_cr
rt = Gui_Builder_Profile::RichText.new(content:input)
gr = Standard::GeneralRequirements.create(content:rt)
gr.save
DIM_STANDARD.general_requirements = gr

input_filename = "ieee_introduction.tex"
input = File.read(File.join(input_dir, input_filename))
input.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
input.encode!('UTF-8', 'UTF-16')
input = input.fix_cr
rt = Gui_Builder_Profile::RichText.new(content:input)
i = Standard::Introduction.create(content:rt)
i.save
DIM_STANDARD.introduction = i

input_filename = "preamble.tex"
input = File.read(File.join(input_dir, input_filename))
input.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
input.encode!('UTF-8', 'UTF-16')
input = input.fix_cr
rt = Gui_Builder_Profile::RichText.new(content:input)
p = Standard::Preamble.create(content:rt)
p.save
DIM_STANDARD.preamble = p

input_filename = "scope.tex"
input = File.read(File.join(input_dir, input_filename))
input.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
input.encode!('UTF-8', 'UTF-16')
input = input.fix_cr
rt = Gui_Builder_Profile::RichText.new(content:input)
s =  Standard::Scope.create(content:rt)  
s.save
DIM_STANDARD.scope = s

DIM_STANDARD.save
ChangeTracker.commit if ChangeTracker.started?

def erase_all_standard
  Standard.classes.each do |klass|
    next unless klass.name =~ /Standard::/
    next if klass.enumeration?
    klass.each {|inst| inst.delete}
  end
end
