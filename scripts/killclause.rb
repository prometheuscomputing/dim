def killclause(c)
  c.subclauses.each{|c| killclause(c)}
  ChangeTracker.start
  c.content.each{|ce| ce.destroy}
  c.destroy
  ChangeTracker.commit
end
killclause c