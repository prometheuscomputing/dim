raise "DO NOT USE THIS SCRIPT!!  If you need a test profile then upload XML for it!"
# load in irb
Hirb.disable if defined? Hirb

def ct
  ChangeTracker.commit
  ChangeTracker.start
end
ChangeTracker.start unless ChangeTracker.started?

module DIM
  module Top
    class Top
      def set_ref_id term
        if self.respond_to? :type
          getter = :type
          setter = :type=
        elsif self.respond_to? :system_type
          getter = :system_type
          setter = :system_type=
        else
          raise "#{self.class} does not respond to either :type or :system_type"
        end
        unless self.send(getter)
          turm = Nomenclature::Term.where(reference_id: term).first
          if turm.nil?
            turm = Nomenclature::Term.create(reference_id: term, source: "Profile Editor", status: "Invalid", term_type: "unknown")
            turm.save
          end
          oid = turm.oid
          typ = DIM::CommonDataTypes::ASN1::TYPE.create
          typ.code = oid
          typ.save
          self.send(setter, typ)
          self.save
        end
      end
    end
  end
end

p = MyDevice::PCDProfile.create(name: "Infusion Pump from Todd Cooper", intended_use: 'Normative (11073)', purpose: 'Demonstration')
mds = DIM::System::SingleBedMDS.create(name: "Infusion Pump MDS")
mds.set_ref_id "MDC_DEV_PUMP_INFUS_MDS"
p.profile_root = mds
p.save
v = DIM::Medical::VMD.create(name: "Infusion Pump VMD")
v.set_ref_id "MDC_DEV_PUMP_INFUS_VMD"
mds.vmds_add v
mds.save
delivery_ch   = DIM::Medical::Channel.create(:name => "Delivery Channel")
primary_ch    = DIM::Medical::Channel.create(:name => "Primary Channel")
secondary_ch  = DIM::Medical::Channel.create(:name => "Secondary Channel")
syringe_ch    = DIM::Medical::Channel.create(:name => "Syringe Channel")
clin_dose_ch  = DIM::Medical::Channel.create(:name => "Clinician Dose Channel")
load_dose_ch  = DIM::Medical::Channel.create(:name => "Loading Dose Channel")
pca_ch        = DIM::Medical::Channel.create(:name => "PCA Channel")
v.channels_add delivery_ch 
v.channels_add primary_ch 
v.channels_add secondary_ch 
v.channels_add syringe_ch 
v.channels_add clin_dose_ch 
v.channels_add load_dose_ch 
v.channels_add pca_ch 
delivery_ch.set_ref_id "MDC_DEV_PUMP_INFUS_CHAN_DELIVERY"
primary_ch.set_ref_id "MDC_DEV_PUMP_INFUS_CHAN_PRIMARY"
secondary_ch.set_ref_id "MDC_DEV_PUMP_INFUS_CHAN_SECONDARY"
syringe_ch.set_ref_id "MDC_DEV_PUMP_INFUS_SYRINGE_CHAN"
syringe_ch.set_ref_id "MDC_DEV_PUMP_INFUS_CHAN_CLINICIAN_DOSE"
syringe_ch.set_ref_id "MDC_DEV_PUMP_INFUS_CHAN_LOADING_DOSE"
syringe_ch.set_ref_id "MDC_DEV_PUMP_INFUS_PCA_CHAN"
v.save
delivery_ch.save
primary_ch.save 
secondary_ch.save
syringe_ch.save 
syringe_ch.save 
syringe_ch.save 
syringe_ch.save
ct
puts "Created MDS, VMD, and Channels"


# Delivery Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Version")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_VERSION"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Pump Mode - Set")
  e.set_ref_id "MDC_PUMP_MODE_SET"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Pump Mode - Current")
  e.set_ref_id "MDC_PUMP_MODE_CURRENT"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Pump Status")
  e.set_ref_id "MDC_PUMP_STAT"
  e.save
  delivery_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Rate")
  n.set_ref_id "MDC_FLOW_FLUID_PUMP"
  n.save
  delivery_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  delivery_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Device Operational Event")
  e.set_ref_id "MDC_DEV_EVENT_COND"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Infusion Pump Alerts")
  e.set_ref_id "MDC_ATTR_AL_COND"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Communication Status")
  e.set_ref_id "MDC_COMM_STATUS"
  e.save
  delivery_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Pump Not Infusing Reason")
  e.set_ref_id "MDC_PUMP_NOT_INFUSING_REASON"
  e.save
  delivery_ch.metric_add e
  delivery_ch.save
end
ct; puts "Created Metrics for Delivery Channel"
# Primary Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug ID")
  e.set_ref_id "MDC_DRUG_ID"
  e.save
  primary_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Name")
  e.set_ref_id "MDC_DRUG_NAME_LABEL"
  e.save
  primary_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Drug Concentration")
  n.set_ref_id "MDC_CONC_DRUG"
  n.save
  primary_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  primary_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Rate")
  n.set_ref_id "MDC_FLOW_FLUID_PUMP"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Rate")
  n.set_ref_id "MDC_RATE_DOSE"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Programmed")
  n.set_ref_id "MDC_VOL_FLUID_TBI"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume (at start)")
  n.set_ref_id "MDC_VOL_FLUID_RESERVOIR_START"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered in Current Segment")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_SEGMENT"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Remaining")
  n.set_ref_id "MDC_VOL_FLUID_TBI_REMAIN"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume Remaining")
  n.set_ref_id "MDC_VOL_REMAIN_RESERVOIR"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Dose To Be Infused")
  n.set_ref_id "MDC_DOSE_DRUG_TBI"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Remaining")
  n.set_ref_id "MDC_DOSE_DRUG_TBI_REMAIN"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered in Current Segment")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_SEGMENT"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_TOTAL"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Drug Amount Remaining")
  n.set_ref_id "MDC_MASS_DRUG_REMAIN_RESERVOIR"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time (Programmed)")
  n.set_ref_id "MDC_TIME_PD_PROG"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN_RESERVOIR"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Delivery Start Interval")
  n.set_ref_id "MDC_TIME_PD_DOSE_START_INTERVAL"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Weight")
  n.set_ref_id "MDC_ATTR_PT_WEIGHT"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Height")
  n.set_ref_id "MDC_ATTR_PT_HEIGHT"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose BSA")
  n.set_ref_id "MDC_ATTR_PT_BSA"
  n.save
  primary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose LBM")
  n.set_ref_id "MDC_ATTR_PT_LBM"
  n.save
  primary_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Concurrent Mode Channel Label")
  e.set_ref_id "MDC_PUMP_MODE_CONCURRENT_CHAN_LABEL"
  e.save
  primary_ch.metric_add e
  primary_ch.save
end
ct; puts "Created Metrics for Primary Channel"
# Secondary Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug ID")
  e.set_ref_id "MDC_DRUG_ID"
  e.save
  secondary_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Name")
  e.set_ref_id "MDC_DRUG_NAME_LABEL"
  e.save
  secondary_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Drug Concentration")
  n.set_ref_id "MDC_CONC_DRUG"
  n.save
  secondary_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  secondary_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Rate")
  n.set_ref_id "MDC_FLOW_FLUID_PUMP"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Rate")
  n.set_ref_id "MDC_RATE_DOSE"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Programmed")
  n.set_ref_id "MDC_VOL_FLUID_TBI"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume (at start)")
  n.set_ref_id "MDC_VOL_FLUID_RESERVOIR_START"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered in Current Segment")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_SEGMENT"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Remaining")
  n.set_ref_id "MDC_VOL_FLUID_TBI_REMAIN"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume Remaining")
  n.set_ref_id "MDC_VOL_REMAIN_RESERVOIR"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Dose To Be Infused")
  n.set_ref_id "MDC_DOSE_DRUG_TBI"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Remaining")
  n.set_ref_id "MDC_DOSE_DRUG_TBI_REMAIN"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered in Current Segment")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_SEGMENT"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_TOTAL"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Drug Amount Remaining")
  n.set_ref_id "MDC_MASS_DRUG_REMAIN_RESERVOIR"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time (Programmed)")
  n.set_ref_id "MDC_TIME_PD_PROG"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN_RESERVOIR"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Delivery Start Interval")
  n.set_ref_id "MDC_TIME_PD_DOSE_START_INTERVAL"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Weight")
  n.set_ref_id "MDC_ATTR_PT_WEIGHT"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Height")
  n.set_ref_id "MDC_ATTR_PT_HEIGHT"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose BSA")
  n.set_ref_id "MDC_ATTR_PT_BSA"
  n.save
  secondary_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose LBM")
  n.set_ref_id "MDC_ATTR_PT_LBM"
  n.save
  secondary_ch.metric_add n
  secondary_ch.save
end
ct; puts "Created Metrics for Secondary Channel"
# Syringe Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Type")
  e.set_ref_id "MDC_SYRINGE_TYPE"
  e.save
  syringe_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Manufacturer")
  n.set_ref_id "MDC_SYRINGE_MANUFACTURER"
  n.save
  syringe_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Syringe Size")
  n.set_ref_id "MDC_VOL_SYRINGE"
  n.save
  syringe_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Syringe Volume")
  e.set_ref_id "MDC_VOL_SYRINGE_ACTUAL"
  e.save
  syringe_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Syringe Pump Alerts")
  e.set_ref_id "MDC_ATTR_AL_COND"
  e.save
  syringe_ch.metric_add e
end
ct; puts "Created Metrics for Syringe Channel"
# Clinician Dose Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug ID")
  e.set_ref_id "MDC_DRUG_ID"
  e.save
  clin_dose_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Name")
  e.set_ref_id "MDC_DRUG_NAME_LABEL"
  e.save
  clin_dose_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Drug Concentration")
  n.set_ref_id "MDC_CONC_DRUG"
  n.save
  clin_dose_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  clin_dose_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Rate")
  n.set_ref_id "MDC_FLOW_FLUID_PUMP"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Rate")
  n.set_ref_id "MDC_RATE_DOSE"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Programmed")
  n.set_ref_id "MDC_VOL_FLUID_TBI"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume (at start)")
  n.set_ref_id "MDC_VOL_FLUID_RESERVOIR_START"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered in Current Segment")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_SEGMENT"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Remaining")
  n.set_ref_id "MDC_VOL_FLUID_TBI_REMAIN"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume Remaining")
  n.set_ref_id "MDC_VOL_REMAIN_RESERVOIR"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Dose To Be Infused")
  n.set_ref_id "MDC_DOSE_DRUG_TBI"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Remaining")
  n.set_ref_id "MDC_DOSE_DRUG_TBI_REMAIN"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered in Current Segment")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_SEGMENT"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_TOTAL"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Drug Amount Remaining")
  n.set_ref_id "MDC_MASS_DRUG_REMAIN_RESERVOIR"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time (Programmed)")
  n.set_ref_id "MDC_TIME_PD_PROG"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN_RESERVOIR"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Delivery Start Interval")
  n.set_ref_id "MDC_TIME_PD_DOSE_START_INTERVAL"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Weight")
  n.set_ref_id "MDC_ATTR_PT_WEIGHT"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Height")
  n.set_ref_id "MDC_ATTR_PT_HEIGHT"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose BSA")
  n.set_ref_id "MDC_ATTR_PT_BSA"
  n.save
  clin_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose LBM")
  n.set_ref_id "MDC_ATTR_PT_LBM"
  n.save
  clin_dose_ch.metric_add n
  clin_dose_ch.save
end
ct; puts "Created Metrics for Clinician Dose Channel"
# Loading Dose Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug ID")
  e.set_ref_id "MDC_DRUG_ID"
  e.save
  load_dose_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Name")
  e.set_ref_id "MDC_DRUG_NAME_LABEL"
  e.save
  load_dose_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Drug Concentration")
  n.set_ref_id "MDC_CONC_DRUG"
  n.save
  load_dose_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  load_dose_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Rate")
  n.set_ref_id "MDC_FLOW_FLUID_PUMP"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Rate")
  n.set_ref_id "MDC_RATE_DOSE"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Programmed")
  n.set_ref_id "MDC_VOL_FLUID_TBI"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume (at start)")
  n.set_ref_id "MDC_VOL_FLUID_RESERVOIR_START"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Delivered in Current Segment")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_SEGMENT"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Remaining")
  n.set_ref_id "MDC_VOL_FLUID_TBI_REMAIN"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume Remaining")
  n.set_ref_id "MDC_VOL_REMAIN_RESERVOIR"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Dose To Be Infused")
  n.set_ref_id "MDC_DOSE_DRUG_TBI"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Remaining")
  n.set_ref_id "MDC_DOSE_DRUG_TBI_REMAIN"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Drug Delivered in Current Segment")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_SEGMENT"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_TOTAL"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Drug Amount Remaining")
  n.set_ref_id "MDC_MASS_DRUG_REMAIN_RESERVOIR"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time (Programmed)")
  n.set_ref_id "MDC_TIME_PD_PROG"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN_RESERVOIR"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Delivery Start Interval")
  n.set_ref_id "MDC_TIME_PD_DOSE_START_INTERVAL"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Weight")
  n.set_ref_id "MDC_ATTR_PT_WEIGHT"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Height")
  n.set_ref_id "MDC_ATTR_PT_HEIGHT"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose BSA")
  n.set_ref_id "MDC_ATTR_PT_BSA"
  n.save
  load_dose_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose LBM")
  n.set_ref_id "MDC_ATTR_PT_LBM"
  n.save
  load_dose_ch.metric_add n
  load_dose_ch.save
end
ct; puts "Created Metrics for Loading Dose Channel"
# PCA Channel
if true
  e = DIM::Medical::Enumeration.create(:name => "Drug ID")
  e.set_ref_id "MDC_DRUG_ID"
  e.save
  pca_ch.metric_add e
  e = DIM::Medical::Enumeration.create(:name => "Drug Name")
  e.set_ref_id "MDC_DRUG_NAME_LABEL"
  e.save
  pca_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Drug Concentration")
  n.set_ref_id "MDC_CONC_DRUG"
  n.save
  pca_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "Drug Library Care Area")
  e.set_ref_id "MDC_PUMP_DRUG_LIBRARY_CARE_AREA"
  e.save
  pca_ch.metric_add e
  n = DIM::Medical::Numeric.create(:name => "Dose Limit")
  n.set_ref_id "MDC_DOSE_PCA_LIMIT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Volume Limit")
  n.set_ref_id "MDC_VOL_PCA_DOSE_LIMIT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Time Period")
  n.set_ref_id "MDC_TIME_PD_PCA_DOSE_LIMIT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "PCA Lockout")
  n.set_ref_id "MDC_TIME_PCA_LOCKOUT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Max Doses per Hour")
  n.set_ref_id "MDC_RATE_PCA_MAX_DOSES_PER_HOUR"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Patient Dose Amount")
  n.set_ref_id "MDC_DOSE_DRUG_TBI"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Patient Dose Volume")
  n.set_ref_id "MDC_VOL_FLUID_TBI"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Patient Doses Given (Total)")
  n.set_ref_id "MDC_NUM_PATIENT_DOSES_GIVEN_TOTAL"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Patient Doses Attempted (Total)")
  n.set_ref_id "MDC_NUM_PATIENT_DOSES_ATTEMPTED_TOTAL"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Volume Delivered")
  n.set_ref_id "MDC_VOL_FLUID_DELIV_TOTAL"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Cumulative Drug Delivered")
  n.set_ref_id "MDC_DOSE_DRUG_DELIV_TOTAL"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Drug Amount Remaining")
  n.set_ref_id "MDC_MASS_DRUG_REMAIN_RESERVOIR"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume (at start)")
  n.set_ref_id "MDC_VOL_FLUID_RESERVOIR_START"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Volume Remaining")
  n.set_ref_id "MDC_VOL_REMAIN_RESERVOIR"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Reservoir Time Remaining")
  n.set_ref_id "MDC_TIME_PD_REMAIN_RESERVOIR"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Weight")
  n.set_ref_id "MDC_ATTR_PT_WEIGHT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose Height")
  n.set_ref_id "MDC_ATTR_PT_HEIGHT"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose BSA")
  n.set_ref_id "MDC_ATTR_PT_BSA"
  n.save
  pca_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Dose LBM")
  n.set_ref_id "MDC_ATTR_PT_LBM"
  n.save
  pca_ch.metric_add n
  e = DIM::Medical::Enumeration.create(:name => "PCA Pump Alerts")
  e.set_ref_id "MDC_ATTR_AL_COND"
  e.save
  pca_ch.metric_add e
  pca_ch.save
end
ct; puts "Created Metrics for PCA Channel"