raise "DO NOT USE THIS SCRIPT!!  If you need a test profile then upload XML for it!"
# load in irb
Hirb.disable if defined? Hirb

def ct
  ChangeTracker.commit
  ChangeTracker.start
end
ChangeTracker.start unless ChangeTracker.started?

module DIM
  module Top
    class Top
      def set_ref_id term
        if self.respond_to? :type
          getter = :type
          setter = :type=
        elsif self.respond_to? :system_type
          getter = :system_type
          setter = :system_type=
        else
          raise "#{self.class} does not respond to either :type or :system_type"
        end
        unless self.send(getter)
          turm = Nomenclature::Term.where(reference_id: term).first
          if turm.nil?
            turm = Nomenclature::Term.create(reference_id: term, source: "Profile Editor", status: "Invalid", term_type: "unknown")
            turm.save
          end
          oid = turm.oid
          typ = DIM::CommonDataTypes::ASN1::TYPE.create
          typ.code = oid
          typ.save
          self.send(setter, typ)
          self.save
        end
      end
    end
  end
end

p = MyDevice::PCDProfile.create(name: "C4MI Pulse Ox", intended_use: 'User Defined', purpose: 'Demonstration')
mds = DIM::System::SingleBedMDS.create(name: "Pulse Oximeter MDS")
mds.set_ref_id "MDC_DEV_ANALY_SAT_O2_MDS"
p.profile_root = mds
p.save
v = DIM::Medical::VMD.create(name: "Pulse Oximeter VMD")
v.set_ref_id "MDC_DEV_ANALY_SAT_O2_VMD"
mds.vmds_add v
mds.save

mds.mds_status = "disconnected"
nv = DIM::System::ASN1::NomenclatureVersion.create(nom_major_version: "majorVersion1")
nv.nom_minor_version_proxy = 0
nv.save
mds.nomenclature_version = nv
sm = DIM::System::ASN1::SystemModel.create
sm.manufacturer_proxy = "Center4MI"
sm.model_number_proxy = "2-0-14"
mds.save

v.vmd_status = 'vmd-standby'
v.save

first_ch   = DIM::Medical::Channel.create(:name => "First Channel")
second_ch    = DIM::Medical::Channel.create(:name => "Second Channel")
v.channels_add first_ch 
v.channels_add second_ch
first_ch.set_ref_id "MDC_DEV_ANALY_SAT_O2_CHAN"
second_ch.set_ref_id "MDC_DEV_PULS_CHAN"
v.save
first_ch.save
second_ch.save
ct
puts "Created MDS, VMD, and Channels"

# first Channel
if true
  first_ch.physical_channel_no_proxy = 1
  first_ch.logical_channel_no_proxy = 1
  n = DIM::Medical::Numeric.create(:name => "Oxygen Saturation")
  n.set_ref_id "MDC_PULS_OXIM_SAT_O2"
  n.save
  first_ch.metric_add n
  n = DIM::Medical::Numeric.create(:name => "Blood Perfusion Index")
  n.set_ref_id "MDC_BLD_PERF_INDEX"
  n.save
  first_ch.metric_add n
  first_ch.save
end
ct; puts "Created Metrics for First Channel"
# second Channel
if true
  second_ch.physical_channel_no_proxy = 1
  second_ch.logical_channel_no_proxy = 1
  n = DIM::Medical::Numeric.create(:name => "Pulse Rate")
  n.set_ref_id "MDC_PULS_OXIM_PULS_RATE"
  n.save
  second_ch.metric_add n
  second_ch.save
end
ct; puts "Created Metrics for Second Channel"