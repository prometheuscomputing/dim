ChangeTracker.start unless ChangeTracker.started?
mp = MetaInfo::Package
mc = MetaInfo::Class
pkg = mp.where(:name => "IEEE20101").first || mp.create(:name => "IEEE20101")
pkg.save
concrete_classes = ["SEQUENCE", "SEQUENCE_OF", "CHOICE", "INTEGER", "ANY_DEFINED_BY", "OCTET_STRING", "BIT_STRING"]
concrete_classes.each do |klass|
  unless mc.where(:name => klass).first
    k = mc.create(:name => klass, :is_abstract => false)
    pkg.classifiers_add k
    k.save
  end
end
ChangeTracker.commit; ChangeTracker.start
