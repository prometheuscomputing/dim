Nomenclature::ProposedTerm.children.each do |klass|
  klass.each do |instance|
    refid = instance.reference_id
    instance.found_in_rtmms = Nomenclature.find_term_by_ref_id(refid)
  end
end