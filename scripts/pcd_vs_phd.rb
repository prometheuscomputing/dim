phd = {}
pcd = {}
both = {}
PHD.classes(:no_imports => true).each do |k|
  if k.name =~ /ASN/
    n = k.name.demodulize    
    phd[n] = k
  end
end

DIM.classes(:no_imports => true).each do |k|
  if k.name =~ /ASN/
    n = k.name.demodulize    
    pcd[n] = k
    if phd[n]
      both[n] = true
    end
  end
end



puts (phd.keys - both.keys)
