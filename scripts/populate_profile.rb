# load '/Users/mfaughn/projects/sequel_change_tracker/lib/sequel_change_tracker/model/change.rb'
# load 'sequel_specific_associations/plugin_instance_methods/set.rb'

# ChangeTracker.start; DIM::Medical::Numeric.last.destroy; ChangeTracker.commit

# load(relative('fix_enums.rb'))
# load(relative("../lib/dim/sequel_model_extensions.rb"))

# for now, this sort of assumes that all compositions are empty...at least it works best that way...
require 'random-word'
def populate_obj_containment(obj, opts = {})
  # return unless obj.is_a?(DIM::System::SingleBedMDS) # for testing
  # proxy_report obj
  opts[:used_classes] ||= []
  return if opts[:single_instances] && opts[:used_classes].include?(obj.class)
  opts[:used_classes] << obj.class
  ignored_getters = opts[:ignored_getters] || []
  ignored_modules = opts[:ignored_modules] || []
  # collection_size = opts[:collection_size] || 2
  props  = obj.class.tree_properties
  props  = props + (obj.class.properties.select{|k,v| _class_is_kind_of(v[:class], opts[:include_props_typed_as]) }).values
  props.uniq!
  # puts "#{obj.class} -- #{props.collect{|v| v[:getter]}.sort.inspect}"
  props.each do |info|
    next if ignored_getters.include?(info[:getter])
    next if ignored_modules.find{|mod| info[:class] =~ /#{mod}::/} # a bit kludgey...
    next if info[:derived]
    to_one = info[:type].to_s =~ /to_one/
    possible_types = Array(obj.send((info[:getter].to_s + '_type').to_sym))
    if info[:wraps]
      # This only works if info[:wraps] lists a single type.  In the general case it is conceivable that it could wrap a primitive that had subclasses, in which case you would need to add the child types to the array.  In the case of the DIM this distinctly undesirable...so we aren't going to do it.
      possible_types = [info[:wraps].to_const]
    end
    # unless info[:type].to_s =~ /to_one/
    #   proxy_report(obj, info)
    # end
    # puts;pp obj.class.info_for info[:getter]
    # puts possible_types.inspect
    case
    when possible_types.first <= TrueClass
      usable_types = [TrueClass, FalseClass]
    when possible_types.first <= String
      usable_types = possible_types
    when possible_types.first <= Time
      usable_types = possible_types
    when possible_types.first <= Integer
      usable_types = possible_types
    else
      usable_types = possible_types.collect{|klass_string| klass_string.to_const}.reject{|klass| klass.abstract? || klass.interface? || opts[:ignored_types].include?(klass)}
    end
    usable_types.delete(obj.class) # avoids cycles created by self associations.  larger loops are going to be harder to stop unless using opts[:single_instances].  this should be superfluous if you are using opts[:single_instances]
    if usable_types.empty?
      if (possible_types - opts[:ignored_types]).any?
        puts "WARNING -- no usable types for #{obj.class}##{info[:getter]} -- #{possible_types}"
      end
      next
    end
    if info[:association] && !info[:wraps]
      if to_one
        ptype = usable_types.sample
        _add_test_obj(obj, ptype, info, opts)
      else
        usable_types.each do |ptype|
          _add_test_obj(obj, ptype, info, opts)
        end
      end
    else
      _add_primitive(obj, usable_types, info)
    end
  end
  # puts "populated #{obj}"
end

def _add_primitive(obj, usable_types, info, opts = {})
  type_class = usable_types.sample
  # puts "  doing #{obj.class}##{info[:getter]} -- #{type_class}"
  case
  when type_class <= Numeric        
    new_obj = rand(100_000_000..999_999_999)
  when type_class <= String
    new_obj = type_class.new('Test_Value_' + RandomWord.nouns(not_shorter_than: 4, not_longer_than: 12).next)      
  when type_class <= Time
    new_obj = type_class.now
  when type_class == TrueClass || type_class == FalseClass
    new_obj = false # this is tricky...at this point I think that only ProposedType.found_in_rtmmms is a boolean and it should be false for now.
  else
    raise "Unhandled type: #{type_class} for #{obj.class}##{info[:getter]}"
  end
  setter = (info[:getter].to_s + '=').to_sym
  adder  = (info[:getter].to_s + '_add').to_sym
  sent_method = info[:wraps] ? adder : setter
  ChangeTracker.start
  begin
    obj.send(sent_method, new_obj)
  rescue
    puts obj
    pp obj.class.info_for(info[:getter])
    raise
  end
  obj.save
  ChangeTracker.commit
end

def _add_test_obj(parent_obj, type, info, opts)
  return _add_primitive(parent_obj, [type], info, opts) if type.primitive? # this handles a corner case that occurs when there is an association to an interface and that interface is implemented by both a primitive and complex type.
  to_one = info[:type].to_s =~ /to_one/
  setter = (info[:getter].to_s + '=').to_sym
  adder  = (info[:getter].to_s + '_add').to_sym
  test_obj = nil
  opts[:preset_associations].each do |klass_with_preset_values, preset_values|
    if type <= klass_with_preset_values
      preset_values = Array(preset_values)
      proxy_data = parent_obj.class.properties.find do |key,data|
        data.dig(:additional_data, :prometheus, "proxy_property")&.first&.gsub('-','_')&.to_sym == info[:getter].to_sym
      end
      if to_one
        available_values = preset_values
      else
        existing_values  = parent_obj.send(info[:getter])
        available_values = preset_values - existing_values
      end
      value = available_values.sample
      return unless value
      ChangeTracker.start
      sent_method = to_one ? setter : adder
      parent_obj.send(sent_method, value)
      ChangeTracker.commit
      return # we are assuming here that preset_value (an object) does not need any further population
    end
  end
  if test_obj.nil? && type.enumeration?
    # We are going to treat enumerations a bit differently so that we can populate many_to_many enum values in a more representative way.
    if to_one
      value = type.all.sample # get one at random
    else
      value = type.all.sample(rand(type.count) + 1) # get a random sample. gets at least one
    end
    ChangeTracker.start
    parent_obj.send(setter, value)
    ChangeTracker.commit
    return
  end
  begin
    ChangeTracker.start
    test_obj ||= type.new
    test_obj.save
    ChangeTracker.commit
  rescue
    puts parent_obj
    pp parent_obj.class.info_for(info[:getter])
    raise
  end
  sent_method = to_one ? setter : adder
  ChangeTracker.start
  parent_obj.send(sent_method, test_obj)
  ChangeTracker.commit
  populate_obj_containment(test_obj, opts)
end

def _class_is_kind_of(klass, types)
  !!types.find{|type| klass.to_const <= type.to_const}
end

def proxy_report(obj, info = nil)
  if info
    if proxied = info.dig(:additional_data, :prometheus, "proxy_property")&.first
      pp obj.class.info_for(info[:getter])
      puts "*** #{obj.class}[#{obj.id}].#{info[:getter]} = #{obj.send(info[:getter]).inspect}  --- #{proxied}"
      puts
    end
  else
    proxy_data = obj.class.properties.select do |key,data|
      if proxied = data.dig(:additional_data, :prometheus, "proxy_property")&.first
        puts "#{obj.class}[#{obj.id}].#{data[:getter]} = #{obj.send(data[:getter]).inspect}  --- #{proxied}"
        puts
      end
    end
  end
end

########################  BEGIN EXECUTED CODE ##############################
ChangeTracker.start
t1 = Nomenclature::Term.where(:reference_id => 'TEST_REFID_ONE', :term_code => '1001').first || Nomenclature::Term.create(:reference_id => 'TEST_REFID_ONE', :term_code => '1001')
t2 = Nomenclature::Term.where(:reference_id => 'TEST_REFID_TWO', :term_code => '1002').first || Nomenclature::Term.create(:reference_id => 'TEST_REFID_TWO', :term_code => '1002')
t3 = Nomenclature::Term.where(:reference_id => 'TEST_REFID_THREE', :term_code => '1003').first || Nomenclature::Term.create(:reference_id => 'TEST_REFID_THREE', :term_code => '1003')
t4 = Nomenclature::Term.where(:reference_id => 'TEST_REFID_FOUR', :term_code => '1004').first || Nomenclature::Term.create(:reference_id => 'TEST_REFID_FOUR', :term_code => '1004')
t5 = Nomenclature::Term.where(:reference_id => 'TEST_REFID_FIVE', :term_code => '1005').first || Nomenclature::Term.create(:reference_id => 'TEST_REFID_FIVE', :term_code => '1005').save
t1.status = 'Approved'; t1.save
t2.status = 'Proposed'; t2.save
t3.status = 'Approved'; t3.save
t4.status = 'Proposed'; t4.save
ChangeTracker.commit
test_terms = [t1, t2, t3, t4, t5]


ChangeTracker.start
test_profile = MyDevice::PCDProfile.new(:name => 'Autotest')
test_profile.save
ChangeTracker.commit
ChangeTracker.start
root = DIM::System::SingleBedMDS.new
root.save
test_profile.profile_root = root
ChangeTracker.commit

# ChangeTracker.start
# root = DIM::Medical::Numeric.new
# ChangeTracker.commit

opts = {}
opts[:ignored_getters]        = [:cardinality, :model_class, :available_attribute_list, :cardinality, :mandatory_attribute_list, :used_attribute_list, :notes_markup_language, :profile_summary, :class_dim, :class_term, :name_binding, :name_binding_term, :type, :proposed_type]
opts[:include_props_typed_as] = [Nomenclature::AbstractTerm] # adds it even if it isn't a composition or attribute
opts[:ignored_modules]        = [Gui_Builder_Profile]
opts[:ignored_types]          = [DIM::CommonDataTypes::ASN1::OID_Type, Nomenclature::ProposedType]
opts[:preset_associations]    = {Nomenclature::RTMMSTerm => test_terms} # any association to a type that is responds 'true' to #kind_of?(key) will be given the hash value for the association instead of creating a new instance for that association.

populate_obj_containment root, opts
puts root
puts root.id


  