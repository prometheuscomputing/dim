# delete all attribute specializations and attribute inclusions
DB[:attribute_specializations].delete
DB[:attribute_inclusions].delete
# rebuild attribute specializations and usage
ChangeTracker.cancel
DIM.classes(no_imports:true).each do |k|
  next unless k < Sequel::Model
  next if k.name =~ /::ASN1::/
  next if k.abstract? || k.interface?
  begin
    next unless k.any?
    k.each do |i|
      ChangeTracker.start
      i.setup_instance if i.respond_to?(:setup_instance)
      i.set_used_attributes if i.respond_to?(:set_used_attributes)
      ChangeTracker.commit
      puts "done with a #{k}"
    end
  rescue Exception => e
    puts "Failed on #{k}"
    puts e.message
    puts e.backtrace
  end
end

