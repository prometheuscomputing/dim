def replace_value original_value, replacement_value
  tables = DB.tables
  klasses = Sequel::Model.children - ChangeTracker.classes
  tables  = tables & klasses.collect{|k| k.table_name}
  tables.each do |table|
    columns = DB[table].columns
    columns.each do |col|
      DB[table].where(col => original_value).update(col => replacement_value)
    end
  end
end

replace_value "DIM::System::SinglePatientMDS", "DIM::System::SingleBedMDS"
replace_value "DIM::System::MultiplePatientMDS", "DIM::System::MultipleBedMDS"


# def replace_value value_type, original_value_regex, replacement_value
#   tables = DB.tables
#   klasses = Sequel::Model.children - ChangeTracker.classes
#   klasses.each do |klass|
#     next unless tables.include?(klass.table_name)
#     columns = klass.columns
#     klass.each do |instance|
#       flag = false
#       columns.each do |c|
#         if instance[c].is_a?(value_type) && instance[c] =~ /#{original_value_regex}/
#           # flag = true
#           if klass.change_tracked?
#             ChangeTracker.cancel
#             ChangeTracker.start
#           end
#           instance[c] = instance[c].gsub(original_value_regex, replacement_value)
#           instance.save
#           if klass.change_tracked?
#             ChangeTracker.commit
#           end
#         end
#       end
#       puts instance.inspect if flag
#     end
#   end
#   nil
# end