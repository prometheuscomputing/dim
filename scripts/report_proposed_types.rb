fn = File.expand_path("~/Prometheus/data/dim_generated/proposed_type_report_#{Time.now.strftime('%F_%H%M')}.csv")
File.open(fn, 'w+') {|f|
  f.puts('Type, RefID, Description, Proposed On, Proposed By, Units, Enums, Found in RTTMS')
  Nomenclature::ProposedTerm.all.each do |t|
    e = ' '
    u = ' '
    case 
    when t.class == Nomenclature::ProposedUnit
      kind = "unit"
    when t.class == Nomenclature::ProposedLiteral
      kind = "enumeration"
    when t.class == Nomenclature::ProposedType
      if t.units || t.proposed_units
        kind = "metric - numeric"
        u = (t.units + t.proposed_units).collect{|x| x.reference_id}. join(" ")
      elsif t.enums || t.proposed_enums
        kind = "metric - enumeration"
        e = (t.enums + t.proposed_enums).collect{|x| x.reference_id}. join(" ")
      else
        kind = ' '
      end
    else
      puts t.inspect
      raise
    end
    entry = kind
    entry << ", #{t.reference_id}, #{t.term_description}, #{t.proposed_by}, #{t.proposed_on}, #{u}, #{e}, #{!!t.found_in_rtmms}"
    f.puts(entry)
  end
}