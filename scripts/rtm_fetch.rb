require "net/https"
require "uri"
require "json"
require "time"
require 'pp'

def fetch_rtm_terms date = nil
  date = Time.parse(date) if date.is_a? String
  date ||= Time.parse("Jan 1, 1970 1:01:01 AM")
  date = date.strftime("%Y%m%d%H%M%S")
  url = "https://rtmms.nist.gov/rtmms/getTermsJson.do?fromDate=#{date}"
  puts url
  uri = URI.parse(url)
  # uri = URI.parse("https://rtmms.nist.gov/rtmms/getTermsJson.do")
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  request = Net::HTTP::Get.new(uri.request_uri)
  res = http.request(request)
  response = JSON.parse(res.body)
end

puts fetch_rtm_terms;nil