require "yaml"

MyDevice::PCDProfile.each do |profile|
  serialized_object = YAML::dump(profile)
  puts serialized_object
  puts "*"*40
end


serialized_object = YAML::dump(MyDevice::PCDProfile.last)
puts serialized_object

File.open(File.expand_path("~/projects/dim_reference/yaml_test.yaml"), "w") do |file|
  MyDevice::PCDProfile.all.each do |profile|
    file.puts YAML::dump(profile)
    file.puts ""
  end
end;