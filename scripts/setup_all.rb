this_dir = File.expand_path(File.dirname __FILE__)
delete_meta_info = MetaInfo.classes(:ignore_class_namespaces => true).each{|c| tn = c.table_name.to_sym; DB[tn].delete if DB.table_exists? tn}
# load File.join(this_dir, 'manually_create_20101_classes.rb')
# load File.join(this_dir, '../bin/dim_metainfo_inject')
load File.join(this_dir, '../bin/dim_metainfo_inject_PHD')

ChangeTracker.extra_limited_mode = true
ChangeTracker.start unless ChangeTracker.started?
MetaInfo::Property.where(:type_id => nil).delete # get rid of everything that isn't DIM or IEEE20101
spmds = MetaInfo::Class.where(:name => "SingleBedMDS").first
spmds.name_binding = "Handle"
spmds.save
ChangeTracker.commit

load File.join(this_dir, 'finish_metainfo.rb')
load File.join(this_dir, 'capitalize_property_names.rb')

# FIXME some asn1 attributes (metainfo properties of metainfo classifiers) are 0..* rather than 0..1 or 1..1.  This means you probably have to go and manually fix this in the model....but maybe not.  Really, the only ones that should be multiple are those where the metainfo class is a subclass of SEQUENCE OF.
load File.join(this_dir, 'sync.rb')
# load File.join(this_dir, 'replace_values.rb')
# load File.join(this_dir, 'connect_classes_to_terms.rb')
# load File.join(this_dir, 'rebuild_used_attrs.rb')
# ChangeTracker.start
# Nomenclature.renew_terms
# # Nomenclature.update_terms
#
# load File.join(this_dir, 'connect_classes_to_terms.rb')
# # load File.join(this_dir, 'make_profile.rb')
# ChangeTracker.commit
# load File.join(this_dir, 'populate_standard.rb')
