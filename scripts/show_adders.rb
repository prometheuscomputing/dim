DIM.classes.select{|c| c.name =~ /DIM/}.sort{|a,b| a.name <=> b.name}.each do |klass|
  custom_adders = klass.properties.select{|k,v| v.keys.include? :custom_adder}.collect{|k,v| k}
  puts "#{klass}: #{custom_adders}" if custom_adders.any?
end;nil