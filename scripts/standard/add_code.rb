def add_code(name, clause_num)
  me = MetaInfo::Class.where(:name => name).all.find{|e| e.qualified_name =~ /DIM::/}
  clause = Standard::Clause[clause_num]
  raise "Can't find model #{name}" unless me
  raise "Can't find clause #{clause_num}" unless clause
  ChangeTracker.start
  code = Standard::Code.new(:title => name)
  code.render_as = Standard::RenderingType[18]
  code.model_element = me
  code.save
  ChangeTracker.commit
  ChangeTracker.start
  clause.content_add(code)
  clause.save
  ChangeTracker.commit
end
# add_code('SaCalData32',1266)
