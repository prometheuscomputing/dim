added_content = Standard::Text[2166]

dim = MetaInfo::Package.where(:name => "DIM").first;
dims = dim.child_packages.collect(&:classifiers).flatten;
clauses = []
dims.each do |d|
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Attributes"}
  puts "No Attribes for #{d.name}" unless clz
  clauses << clz if clz  
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Behavior"}
  puts "No Behavior for #{d.name}" unless clz
  clauses << clz if clz  
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Notifications"}
  puts "No Notification for #{d.name}" unless clz
  clauses << clz if clz  
end;

clauses.each do |clz|
  first_code_index = clz.content.index{|c| c.is_a?(Standard::Code)}
  next unless first_code_index
  ChangeTracker.start
  clz.content = clz.content.insert(first_code_index, added_content)
  clz.save
  ChangeTracker.commit
end
