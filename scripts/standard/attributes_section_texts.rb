attr_clauses = Standard::Clause.where(:title => 'Attributes').all;
puts attr_clauses.count
attr_clauses.each do |c|
  me = c.model_element
  unless me
    puts "WARNING, no model for #{c.containers.first.title}"
    next
  end
  stuff = c.content
  ChangeTracker.start
  a = Standard::Text.new
  a_text = "The #{me.name} class defines the attributes in Table Attributes_Table_Number."
  a.content = Gui_Builder_Profile::RichText.new(:content => a_text)
  a.save
  b = Standard::Text.new
  b_text = "The #{me.name} class defines in Table Attribute_Groups_Table_Number the attribute groups or extensions to inherited attribute groups."
  b.content = Gui_Builder_Profile::RichText.new(:content => b_text)
  b.save
  stuff.insert(0, a)
  stuff.insert(2, b)
  c.content = stuff
  c.save
  ChangeTracker.commit
end;

# ChangeTracker.start
# asn1_enum = Standard::RenderingType.where(:value => 'ASN.1').first || Standard::RenderingType.create(:value => 'ASN.1')
# ChangeTracker.commit;
# ChangeTracker.start
# Standard::Code.all.each { |code| code.render_as = asn1_enum; code.save };
# ChangeTracker.commit;

