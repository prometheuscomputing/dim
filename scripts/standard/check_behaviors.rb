dim = MetaInfo::Package.where(:name => "DIM").first
dims = dim.child_packages.collect(&:classifiers).flatten
dims.each do |klass|
  dm = klass.defined_methods
  next if dm.empty?
  # puts_green "#{klass.name}  #{klass.class}[#{klass.id}]"
  dm.each do |m|
    unless m.term
      found = Nomenclature::Term.where(:reference_id => m.action_id).first
      if found
        ChangeTracker.start
        m.term = found
        m.save
        ChangeTracker.commit
      else
        puts_red "#{klass.name} -- #{m.action_id}\n"
      end
    end
  end
end
    