ChangeTracker.start unless ChangeTracker.started?
DIM_STANDARD = Standard::IEEEStandard.where(:title=>"Health informatics -- Point-of-care medical device communication --", :subtitle1=>"Part 10201", :subtitle2=>"Domain information model").first || Standard::IEEEStandard.new(:title=>"Health informatics -- Point-of-care medical device communication --", :subtitle1=>"Part 10201", :subtitle2=>"Domain information model")
DIM_STANDARD.save
ChangeTracker.commit

class String
  def fix_cr
    gsub /[^\r]\n/, "\r\n"
  end
end