# README: This was initially written for getting the ASN.1 from the 2010 MS Word draft of 11073:10201.  The draft was saved as plaintext and then everything but the ASN.1 was manually removed.  Other manipulations are listed below
#  * comments on the alternatives of CHOICE types were manually consolidated to be on the same line as the alternative declaration

fn = File.expand_path("~/projects/dim_reference/phd/asn1_from_doc.asn1")
File.exist? fn
doc = File.read fn;
@defs = {}

IEEE20101PRIMITIVES = MetaInfo::Package[20].all_classifiers
DIMTYPES = MetaInfo::Package[1].all_classifiers

def reset
  @identifier = nil
  @comment = []
  @comment_open = true
  @def_complete = false
  @type = nil
  @properties = []
  @property = nil
  # @property_comment = []
  @anonymous_type_on = false
  @current_identifier = nil
  # @enum = false
  # @show = false
end

def complete_def
  hsh = {:identifier => @identifier}
  
  ieee_identifier = IEEE20101PRIMITIVES.find { |prim| prim.name == @identifier }
  hsh[:ieee_identifier] = ieee_identifier if ieee_identifier
  
  dim_identifier = DIMTYPES.find { |t| t.name == @identifier }
  hsh[:dim_identifier] = dim_identifier if dim_identifier
      
  if @type =~ /SEQUENCE OF/
    hsh[:sequence_of] = true
    @type = @type.gsub('SEQUENCE OF', '').strip
  end
  if @type =~ /INTEGER|BIT STRING/
    constraint = @type.slice!(/\(.+\)/)
    @type.strip!
    hsh[:constraint] = constraint if constraint
  end
  if @type =~ /--/
    comment = @type.slice!(/(?<=--).+/)
    @comment ||= []
    @comment << comment if comment
  end
  @type = @type.gsub('--', '').strip
  @type = nil if @type.empty?
  
  ieee_type = IEEE20101PRIMITIVES.find { |prim| prim.name == @type }
  hsh[:ieee_type] = ieee_type if ieee_type
  
  dim_type = DIMTYPES.find { |t| t.name == @type }
  hsh[:dim_type] = dim_type if dim_type
  
  hsh[:type]    = @type if @type
  hsh[:comment] = @comment.join(' ').strip if @comment && @comment.any?
  if @properties.any?
    @properties.each do |prop|
      prop[:comment] = prop[:comment].join(' ').strip if prop[:comment]
      if prop[:type] =~ /ANY DEFINED BY/
        prop[:any_defined_by] = true
        prop[:type] = prop[:type].gsub('ANY DEFINED BY', '').strip
      end
    end
    hsh[:properties] = @properties
  end
  @defs[@identifier] = hsh
  # puts_cyan hsh.pretty_inspect
  reset
end

reset
doc.each_line do |l|
  next if l.strip.empty? || l.strip == '--'
  if l =~ /^--/ && @comment_open
    @comment << /(?<=--).+/.match(l).to_s.strip
    next
  end
  if l =~ /::=/
    parts = l.split('::=')
    @identifier = parts.first.strip
    @type       = parts.last.strip
    if @type =~ /{/
      # simplecolor has a comment after the opening curly brace.  this deals with it.
      badly_placed_comment = @type.slice!(/(?<=--).+/)&.strip
      @comment << badly_placed_comment if badly_placed_comment
      @type = @type.delete('{').strip
      @comment_open = false
    else
      complete_def
    end
    next
  end
  # Into the body here
  # @show = true
  if l =~ /}/
    if l =~ /,/
      @anonymous_type_on = false
      next
    else
      complete_def
      next
    end
  end
  if @type == 'CHOICE'
    prop_name = /.+(?=\[)/.match(l).to_s.strip.delete(',')
    tag       = /(?<=\[)\d+(?=\])/.match(l).to_s
    remainder = /(?<=\[).+/.match(l).to_s
    pcomment  = remainder.slice!(/--.+/)&.strip
    ptype     = remainder.strip.delete(',').gsub(/\[?\d+\]/,'').strip
    phash     = {:identifier => prop_name, :tag => tag, :type => ptype}

    ieee_type = IEEE20101PRIMITIVES.find { |prim| prim.name == ptype }
    phash[:ieee_type] = ieee_type if ieee_type
  
    dim_type = DIMTYPES.find { |t| t.name == ptype }
    phash[:dim_type] = dim_type if dim_type

    phash[:comment] = [pcomment.slice(/(?<=--).+/).strip] if pcomment
    @properties << phash
  else
    if l.strip[0..1] == '--'
      # then this is a continuation of a comment on a property
      # TODO make this handle comments that come before properties
      raise "uhhhh, why is this a problem??" unless @properties.last[:identifier] == @current_identifier# && @properties.last[:comment]
      pcomment = l.slice(/(?<=--).+/)&.strip
      if pcomment && !pcomment.empty?
        @properties.last[:comment] ||= []
        @properties.last[:comment] << pcomment
      end
      next
    else
      @property = nil
    end
    l.gsub!(/\t/, ' ')
    prop_name = /^[a-z][^\s]+/.match(l.strip).to_s.strip
    # prop_name = l.strip if prop_name.nil? || prop_name.empty
    raise unless prop_name
    if l =~ /\{(\s*--.+)?$/
      @anonymous_type_on = true
    end
    if prop_name =~ /^[a-z][\w-]+\(\d+\),?/
      # then this is an enum, either a BIT-STRING or an INT of some sort
      # if prop_name.include?(',')
      #   puts "#{prop_name.inspect} -- #{prop_name.strip[-1]} -- #{prop_name.strip[0..-2]}"
      # end
      prop_name = prop_name.strip[0..-2] if prop_name.strip[-1] == ','
      if prop_name.include?(',')
        puts "#{l.inspect}"
      end
      @property ||= {}
      @property[:identifier] = prop_name
      @current_identifier = @property[:identifier]
      pcomment = l.slice(/(?<=--).+/)&.strip
      @property[:comment] = [pcomment] if pcomment && !pcomment.empty?
      if @anonymous_type_on
        @properties.last[:anonymous_type_properties] << @property
      else
        @properties << @property
      end
    else
      @property ||= {}
      @property[:identifier] = prop_name
      @current_identifier = @property[:identifier]
      remainder = /(?<= ).+/.match(l.strip).to_s
      pcomment  = remainder.slice!(/--.+/)&.strip
      @property[:comment] = pcomment.split(/-- ?/) if pcomment && !pcomment.empty?
      ptype     = remainder.strip.delete(',')
      if @anonymous_type_on && ptype =~ /\{$/
        ptype = ptype[0..-2].strip
        @property[:type] = ptype
        @property[:anonymous] = true
        @property[:anonymous_type_properties] = []
        @properties << @property
      elsif @anonymous_type_on
        @properties.last[:anonymous_type_properties] << @property
      else
        @property[:type] = ptype
        @properties << @property
      end
    end
  end
end

pp @defs