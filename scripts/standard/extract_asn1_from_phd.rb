require 'uml_metamodel'
load File.expand_path "~/projects/uml_metamodel/lib/uml_metamodel/dsl.rb"
# README: This was initially written for getting the ASN.1 from the 2010 MS Word draft of 11073:10201.  The draft was saved as plaintext and then everything but the ASN.1 was manually removed.  Other manipulations are listed below
#  * comments on the alternatives of CHOICE types were manually consolidated to be on the same line as the alternative declaration

IEEE20101PRIMITIVES = MetaInfo::Package[20].all_classifiers unless defined?(IEEE20101PRIMITIVES)
# DIMTYPES = MetaInfo::Package[1].all_classifiers

def reset
  @identifier   = nil
  @comment      = []
  @comment_open = true
  @def_complete = false
  @type         = nil
  @properties   = []
  @property     = nil
  # @property_comment = []
  @anonymous_type_on  = false
  @current_identifier = nil
  # @enum = false
  # @show = false
end

def complete_def
  hsh = {:identifier => @identifier}

  ieee_identifier = IEEE20101PRIMITIVES.find { |prim| prim.name == @identifier }
  hsh[:ieee_identifier] = ieee_identifier if ieee_identifier

  # dim_identifier = DIMTYPES.find { |t| t.name == @identifier }
  # hsh[:dim_identifier] = dim_identifier if dim_identifier

  if @type =~ /SEQUENCE OF/
    hsh[:sequence_of] = true
    @type = @type.gsub('SEQUENCE OF', '').strip
  end
  if @type =~ /INTEGER|BIT STRING/
    constraint = @type.slice!(/\(.+\)/)
    @type.strip!
    hsh[:constraint] = constraint if constraint
  end
  if @type =~ /--/
    comment = @type.slice!(/(?<=--).+/)
    @comment ||= []
    @comment << comment if comment
  end
  @type = @type.gsub('--', '').strip
  @type = nil if @type.empty?

  ieee_type = IEEE20101PRIMITIVES.find { |prim| prim.name == @type }
  # hsh[:ieee_type] = ieee_type if ieee_type

  # dim_type = DIMTYPES.find { |t| t.name == @type }
  # hsh[:dim_type] = dim_type if dim_type

  hsh[:type]    = @type if @type
  hsh[:comment] = @comment.join(' ').strip if @comment && @comment.any?
  if @properties.any?
    @properties.each do |prop|
      prop[:comment] = prop[:comment].join(' ').strip if prop[:comment]
      if prop[:type] =~ /ANY DEFINED BY/
        prop[:any_defined_by] = true
        prop[:type] = prop[:type].gsub('ANY DEFINED BY', '').strip
      end
    end
    hsh[:properties] = @properties
  end
  @defs[@identifier] = hsh
  # puts_cyan hsh.pretty_inspect
  reset
end

def extract(doc)
  reset
  doc.each_line do |l|
    next if l.strip.empty? || l.strip == '--'
    if l =~ /^--/ && @comment_open
      @comment << /(?<=--).+/.match(l).to_s.strip
      next
    end
    if l =~ /::=/
      parts = l.split('::=')
      @identifier = parts.first.strip
      @type       = parts.last.strip
      if @type =~ /{/
        # simplecolor has a comment after the opening curly brace.  this deals with it.
        badly_placed_comment = @type.slice!(/(?<=--).+/)&.strip
        @comment << badly_placed_comment if badly_placed_comment
        @type = @type.delete('{').strip
        @comment_open = false
      else
        complete_def
      end
      next
    end
    # Into the body here
    # @show = true
    if l =~ /}/
      if l =~ /,/
        @anonymous_type_on = false
        next
      else
        complete_def
        next
      end
    end
    if @type == 'CHOICE'
      /(.+?)\[/.match(l)
      prop_name = $1.strip
      /\[(\d+)\]/.match(l)
      tag       = $1.strip
      remainder = /(?<=\[).+/.match(l).to_s
      pcomment  = remainder.slice!(/--.+/)&.strip
      ptype     = remainder.strip.delete(',').gsub(/\[?\d+\]/,'').strip
      phash     = {:identifier => prop_name, :tag => tag, :type => ptype}

      ieee_type = IEEE20101PRIMITIVES.find { |prim| prim.name == ptype }
      phash[:ieee_type] = ieee_type if ieee_type

      # dim_type = DIMTYPES.find { |t| t.name == ptype }
      # phash[:dim_type] = dim_type if dim_type

      phash[:comment] = [pcomment.slice(/(?<=--).+/).strip] if pcomment
      pp phash; puts
      @properties << phash
    else
      if l.strip[0..1] == '--'
        # then this is a continuation of a comment on a property
        # TODO make this handle comments that come before properties
        raise "uhhhh, why is this a problem??" unless @properties.last[:identifier] == @current_identifier# && @properties.last[:comment]
        pcomment = l.slice(/(?<=--).+/)&.strip
        if pcomment && !pcomment.empty?
          @properties.last[:comment] ||= []
          @properties.last[:comment] << pcomment
        end
        next
      else
        @property = nil
      end
      l.gsub!(/\t/, ' ')
      prop_name = /^[a-z][^\s]+/.match(l.strip).to_s.strip
      # prop_name = l.strip if prop_name.nil? || prop_name.empty
      raise unless prop_name
      if l =~ /\{(\s*--.+)?$/
        @anonymous_type_on = true
      end
      if prop_name =~ /^[a-z][\w-]+\(\d+\),?/
        # then this is an enum, either a BIT-STRING or an INT of some sort
        # if prop_name.include?(',')
        #   puts "#{prop_name.inspect} -- #{prop_name.strip[-1]} -- #{prop_name.strip[0..-2]}"
        # end
        prop_name = prop_name.strip[0..-2] if prop_name.strip[-1] == ','
        if prop_name.include?(',')
          puts "#{l.inspect}"
        end
        @property ||= {}
        @property[:identifier] = prop_name
        @current_identifier = @property[:identifier]
        pcomment = l.slice(/(?<=--).+/)&.strip
        @property[:comment] = [pcomment] if pcomment && !pcomment.empty?
        if @anonymous_type_on
          @properties.last[:anonymous_type_properties] << @property
        else
          @properties << @property
        end
      else
        @property ||= {}
        @property[:identifier] = prop_name
        @current_identifier = @property[:identifier]
        remainder = /(?<= ).+/.match(l.strip).to_s
        pcomment  = remainder.slice!(/--.+/)&.strip
        @property[:comment] = pcomment.split(/-- ?/) if pcomment && !pcomment.empty?
        ptype     = remainder.strip.delete(',')
        if @anonymous_type_on && ptype =~ /\{$/
          ptype = ptype[0..-2].strip
          @property[:type] = ptype
          @property[:anonymous] = true
          @property[:anonymous_type_properties] = []
          @properties << @property
        elsif @anonymous_type_on
          @properties.last[:anonymous_type_properties] << @property
        else
          @property[:type] = ptype
          @properties << @property
        end
      end
    end
  end
end

def set_id(element)
  element.id ||= SecureRandom.hex(4) + "_#{Time.now.strftime('%e%m%Y_%H:%M')}_MF"
end

def make_uml(phd, ieee20101, stereotype, receiver_pkg = nil)
  receiver_pkg ||= phd
  phd_asn1 = phd.contents
  prims_20101 = ieee20101.contents
  any_defined_by = prims_20101.find { |uml| uml.name == 'ANY_DEFINED_BY' }
  sequence_of    = prims_20101.find { |uml| uml.name == 'SEQUENCE_OF' }
  sequence       = prims_20101.find { |uml| uml.name == 'SEQUENCE' }
  # bit_string   = prims_20101.find { |uml| uml.name == 'BIT_STRING' }
  choice       = prims_20101.find { |uml| uml.name == 'CHOICE' }
  # integer      = prims_20101.find { |uml| uml.name == 'INTEGER' }
  # octet_string = prims_20101.find { |uml| uml.name == 'OCTET_STRING' }
  while (phd.classifiers + receiver_pkg.classifiers).uniq.size < @defs.size
    count = 0
    @defs.each do |name, data|
      classifier = (phd.contents + receiver_pkg.contents).find { |uml| uml.name == name }
      if classifier
        # puts_yellow "Skip #{name}"
      else
        # puts_green "Make #{name}"
      end
      type_name = data[:type].gsub(/ *\(SIZE\(\d+\)\)/, '').gsub(' ', '_')
      parent ||= prims_20101.find { |uml| uml.name == type_name }
      parent ||= (phd.contents + receiver_pkg.contents).find   { |uml| uml.name == type_name }
      if parent
        if data[:sequence_of]
          # parent is NOT REALLY the parent in this case because my hash format is badly structured / overloaded. it is really the thing that this is a sequence of (i.e. an ordered collection of)
          unless classifier
            classifier = UmlMetamodel::Datatype.new(data[:identifier])
            sequence_of.add_child(classifier)
            classifier.add_parent(sequence_of)
            list_property = UmlMetamodel::Property.new(nil, :aggregation => :composite, :is_ordered => true, :upper => Infinity, :owner => classifier)
            list_property.type = parent
            set_id(list_property)
            # puts_cyan "Made list property #{list_property.inspect}"
            item_property = UmlMetamodel::Property.new(nil, :owner => parent)
            item_property.type = classifier
            set_id(item_property)
            # puts_cyan "Made item property #{item_property.inspect}"
            assoc         = UmlMetamodel::Association.new(:properties => [list_property, item_property])
            set_id(assoc)
            # puts_cyan "Made assoc #{assoc.inspect}"
            receiver_pkg.add_content(assoc)
            assoc.package = receiver_pkg
          end
        elsif type_name =~ /BITS-|INT-/ && data[:properties]
          classifier ||= UmlMetamodel::Enumeration.new(data[:identifier])
          data[:properties].each do |prop|
            literal = classifier.literals.find {|lit| lit.name == prop[:identifier]}
            unless literal
              # puts_magenta "    literal #{classifier.name}::#{prop[:identifier]}"
              literal = UmlMetamodel::EnumerationLiteral.new(prop[:identifier])
              literal.documentation = prop[:comment] if prop[:comment]
              classifier.add_literal(literal)
              literal.enumeration = classifier
              set_id(literal)
            end
            prop[:uml] = literal
          end
          classifier.apply_stereotype(stereotype)
          classifier.set_tag_value(stereotype.name, 'implemented_as', type_name)
          classifier.set_tag_value(stereotype.name, 'constraint', "SIZE(#{type_name.slice(/\d+/)})")
        else
          unless classifier
            classifier = parent.class.new(name)
            classifier.add_parent(parent)
            parent.add_child(classifier)
          end
          (data[:properties] || []).each do |prop|
            uml_property = classifier.properties.find { |uml_prop| uml_prop.name == prop[:identifier] }
            unless uml_property
              # puts_red "    property #{classifier.name}::#{prop[:identifier]}"
              uml_property = UmlMetamodel::Property.new(prop[:identifier])
              # TODO We are going to add types to all of the properties later!  If it is typed as a BIT STRING type then Property#upper must be Infinity.
              classifier.add_property(uml_property)
              uml_property.owner = classifier
              set_id(uml_property)
              if prop[:tag]
                uml_property.apply_stereotype(stereotype)
                uml_property.set_tag_value(stereotype.name, 'asn1_tag', prop[:tag])
              end
            end
            prop[:uml] = uml_property
          end
          constraint = data[:constraint] || data[:type].slice(/SIZE\(\d+\)/)
          if constraint
            classifier.apply_stereotype(stereotype)
            classifier.set_tag_value(stereotype.name, 'constraint', constraint)
          end
        end
        classifier.documentation = data[:comment] if data[:comment]
        set_id(classifier)
        receiver_pkg.add_content(classifier) unless phd.contents.include?(classifier) || receiver_pkg.contents.include?(classifier)
        data[:uml] = classifier
        classifier.package = receiver_pkg unless classifier.package
        count += 1
        # pp data;puts
      end
    end
    remainder = (@defs.keys - phd.contents.collect(&:name) - receiver_pkg.contents.collect(&:name))
    remainder.each { |r| pp @defs[r] } && raise if count < 1
  end
  puts "Orphan defs:" + @defs.select{|_,data| data[:uml].nil? || (data[:properties] && data[:properties].select{|prop| prop[:uml].nil?}.any?)}.to_s
  (choice.children + sequence.children).each do |child|
    deff = @defs.find { |d,_| d == child.name }
    raise child.inspect unless deff
    deff_props = deff.last[:properties]
    # puts deff_props;puts
    child.properties.each do |uml_prop|
      if uml_prop.association
        # then this was involved in a SEQUENCE_OF and the types have already been set!
      else
      # FIXME if this is a property that is new or has a type change but it belongs to a classifier that already existed then we are still not going to write it out into receiver_pkg
        # next if uml_prop.type # Do not skip this because the type may have changed!
        deff_prop = deff_props.find { |dp| dp[:identifier] == uml_prop.name }
        unless deff_prop
          puts_yellow uml_prop.inspect
          pp deff
        end
        if deff_prop[:any_defined_by]
          uml_prop.type = any_defined_by
          uml_prop.apply_stereotype(stereotype)
          uml_prop.set_tag_value(stereotype.name, 'any_defined_by', deff_prop[:type])
          next
        end
        type = (phd.contents + ieee20101.contents + receiver_pkg.contents).find { |uml| uml.name == deff_prop[:type].gsub(' ', '_') }
        unless type
          puts child.inspect
          puts "    NO TYPE FOR #{uml_prop.name.inspect}"
          pp deff_props
          puts
          next
        end
        if uml_prop.type && (uml_prop.type != type)
          puts_yellow "Changing #{uml_prop.qualified_name} type from #{uml_prop.type.qualified_name} to #{type.qualified_name}"
          uml_prop.type = type
        end
        uml_prop.type ||= type
      end
    end
  end
end

ieee_20101_uml_filepath  = File.expand_path('../../../uml/ieee20101.uml.rb', __FILE__)
stereotypes_uml_filepath = File.expand_path('../../../uml/stereotypes.uml.rb', __FILE__)
phd_uml_filepath = File.expand_path('../../../uml/phd.uml.base.rb', __FILE__)
project = UmlMetamodel.from_dsl_file(phd_uml_filepath)
stereotype = project.contents[0].contents.first
ieee20101  = project.contents[1].contents.first
phd_package = project.contents[2]
# puts_cyan ieee20101.inspect
# puts_yellow stereotype.inspect
# puts_green phd_package.inspect

# HERE WE GO!
fn = File.expand_path("~/projects/dim_reference/phd/asn1_from_doc.modified.asn1")
# fn = File.expand_path("~/projects/dim_reference/phd/schedule-segment-datatypes.asn1")
raise unless File.exist?(fn)
@defs = {}
extract(File.read(fn))
# receiver_pkg = UmlMetamodel::Package.new('NewPHD')
make_uml(phd_package.contents.first, ieee20101, stereotype)
# wrapper = UmlMetamodel::Project.new('PHD Import')
wrapper = project
# wrapper.contents = [phd_package, UmlMetamodel.from_dsl_file(ieee_20101_uml_filepath).first, UmlMetamodel.from_dsl_file(stereotypes_uml_filepath).first]
# wrapper.contents = [receiver_pkg]
# output_filepath = File.expand_path('../../../uml/phd2.uml.rb', __FILE__)
output_filepath = File.expand_path('../../../uml/phd.uml.rb', __FILE__)
# load File.expand_path "~/projects/uml_metamodel/lib/uml_metamodel/elements/classifier.rb"
# load File.expand_path "~/projects/uml_metamodel/lib/uml_metamodel/elements/property.rb"
File.open(output_filepath, 'w+') { |f| f.puts wrapper.to_dsl(:unsorted => true) }
# puts @defs.values.collect(&:keys).flatten.uniq

# pp phd_package.contents



























# @defs.each do |id, info|
#   dimid = info[:dim_identifier]
#   puts id unless dimid
#   if dimid.is_a?(MetaInfo::Enumeration)
#     dim_props = dimid.literals.collect { |lit| [lit.name.strip] }
#   else
#     dim_props = dimid.properties.select { |prop| prop.decoration&.value =~ /comp/ }.collect { |prop| [prop.name.strip, prop.type.name.strip].compact }
#   end
#   phd_props = Array(info[:properties]).collect { |prop| [prop[:identifier], prop[:type]].compact }
#   dim_only = (dim_props - phd_props)
#   phd_only = (phd_props - dim_props)
#   if dim_only.any? || phd_only.any?
#     puts_magenta info[:identifier]
#     puts_light_blue  dim_only.pretty_inspect if dim_only.any?
#     puts_yellow phd_only.pretty_inspect if phd_only.any?
#     puts dim_props.pretty_inspect
#     puts phd_props.pretty_inspect
#     # puts dimid.pretty_inspect
#     puts_cyan "*"*30
#   else
#     puts_green info[:identifier]
#   end
# end;