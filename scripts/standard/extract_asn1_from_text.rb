# README: This was initially written for getting the ASN.1 from the 2010 MS Word draft of 11073:10201.  The draft was saved as plaintext and then everything but the ASN.1 was manually removed.  Other manipulations are listed below
#  * comments on the alternatives of CHOICE types were manually consolidated to be on the same line as the alternative declaration

fn = File.expand_path("~/projects/dim_reference/test_asn1_from_2010_draft.txt")
File.exist? fn
doc = File.read fn;
@defs = {}

def reset
  @identifier = nil
  @comment = []
  @comment_open = true
  @def_complete = false
  @type = nil
  @properties = []
  @property = nil
  # @property_comment = []
  @anonymous_type_on = false
  @current_identifier = nil
  # @enum = false
  # @show = false
end

def complete_def
  hsh = {:identifier => @identifier}      
  if @type =~ /SEQUENCE OF/
    hsh[:sequence_of] = true
    @type = @type.gsub('SEQUENCE OF', '').strip
  end
  if @type =~ /INTEGER|BIT STRING/
    constraint = @type.slice!(/\(.+\)/)
    @type.strip!
    hsh[:constraint] = constraint if constraint
  end
  if @type =~ /--/
    comment = @type.slice!(/(?<=--).+/)
    @comment ||= []
    @comment << comment if comment
  end
  @type = @type.gsub('--', '').strip
  hsh[:type]    = @type if @type && !@type.empty?
  hsh[:comment] = @comment.join(' ').strip if @comment && @comment.any?
  if @properties.any?
    @properties.each do |prop|
      prop[:comment] = prop[:comment].join(' ').strip if prop[:comment]
      if prop[:type] =~ /ANY DEFINED BY/
        prop[:any_defined_by] = true
        prop[:type] = prop[:type].gsub('ANY DEFINED BY', '').strip
      end
    end
    hsh[:properties] = @properties
  end
  @defs[@identifier] = hsh
  # puts_cyan hsh.pretty_inspect
  reset
end

reset
doc.each_line do |l|

  next if l.strip.empty? || l.strip == '--'
  if l =~ /^--/ && @comment_open
    @comment << /(?<=--).+/.match(l).to_s.strip
    next
  end
  if l =~ /::=/
    parts = l.split('::=')
    @identifier = parts.first.strip
    @type       = parts.last.strip
    if @type =~ /{/
      # simplecolor has a comment after the opening curly brace.  this deals with it.
      badly_placed_comment = @type.slice!(/(?<=--).+/)&.strip
      @comment << badly_placed_comment if badly_placed_comment
      @type = @type.delete('{').strip
      @comment_open = false
    else
      complete_def
    end
    next
  end
  # Into the body here
  # @show = true
  if l =~ /}/
    if l =~ /,/
      @anonymous_type_on = false
      next
    else
      complete_def
      next
    end
  end
  if @type == 'CHOICE'
    prop_name = /.+(?=\[)/.match(l).to_s.strip.delete(',')
    tag       = /(?<=\[)\d+(?=\])/.match(l).to_s
    remainder = /(?<=\[).+/.match(l).to_s
    pcomment  = remainder.slice!(/--.+/)&.strip
    ptype     = remainder.strip.delete(',').gsub(/\[?\d+\]/,'').strip
    phash     = {:identifier => prop_name, :tag => tag, :type => ptype}
    phash[:comment] = [pcomment.slice(/(?<=--).+/).strip] if pcomment
    @properties << phash
  else
    if l.strip[0..1] == '--'
      # then this is a continuation of a comment on a property
      raise "uhhhh, why is this a problem??" unless @properties.last[:identifier] == @current_identifier# && @properties.last[:comment]
      pcomment = l.slice(/(?<=--).+/)&.strip
      if pcomment && !pcomment.empty?
        @properties.last[:comment] ||= []
        @properties.last[:comment] << pcomment
      end
      next
    else
      @property = nil
    end
    l.gsub!(/\t/, ' ')
    prop_name = /^[a-z][^\s]+/.match(l).to_s.strip
    # prop_name = l.strip if prop_name.nil? || prop_name.empty
    raise unless prop_name
    if l =~ /\{(\s*--.+)?$/
      @anonymous_type_on = true
    end
    if prop_name =~ /^[a-z][\w-]+\(\d+\),?/
      # then this is an enum, either a BIT-STRING or an INT of some sort
      # if prop_name.include?(',')
      #   puts "#{prop_name.inspect} -- #{prop_name.strip[-1]} -- #{prop_name.strip[0..-2]}"
      # end
      prop_name = prop_name.strip[0..-2] if prop_name.strip[-1] == ','
      if prop_name.include?(',')
        puts "#{l.inspect}"
      end
      @property ||= {}
      @property[:identifier] = prop_name
      @current_identifier = @property[:identifier]
      pcomment = l.slice(/(?<=--).+/)&.strip
      @property[:comment] = [pcomment] if pcomment && !pcomment.empty?
      if @anonymous_type_on
        @properties.last[:anonymous_type_properties] << @property
      else
        @properties << @property
      end
    else
      @property ||= {}
      @property[:identifier] = prop_name
      @current_identifier = @property[:identifier]
      remainder = /(?<= ).+/.match(l.strip).to_s
      pcomment  = remainder.slice!(/--.+/)&.strip
      @property[:comment] = pcomment.split(/-- ?/) if pcomment && !pcomment.empty?
      ptype     = remainder.strip.delete(',')
      if @anonymous_type_on && ptype =~ /\{$/
        ptype = ptype[0..-2].strip
        @property[:type] = ptype
        @property[:anonymous] = true
        @property[:anonymous_type_properties] = []
        @properties << @property
      elsif @anonymous_type_on
        @properties.last[:anonymous_type_properties] << @property
      else
        @property[:type] = ptype
        @properties << @property
      end
    end
  end
end



# AUDIT starting from scraped defs
@defs.each do |identifier, info|
  model = MetaInfo::Class.where(:name => identifier).first || MetaInfo::Enumeration.where(:name => identifier).first
  mtype = model.super_classifier.name
  if mtype == 'SEQUENCE OF'
    seqprop = model.properties.find { |mp| mp.decoration&.value == 'composition' }
    mtype = seqprop.type.name
  end
  puts "Type Mismatch: #{identifier} ::= #{info[:type]} -- #{mtype}" unless info[:type] == mtype
  if model.is_a?(MetaInfo::Class)
    model_comment = model.description_content
    model_comment2 = model_comment.gsub(/[\n\r]+(?=.+)/m, ' ').gsub(/\s+/, ' ') if model_comment
    unless (model_comment2 || '').strip == (info[:comment]&.gsub(/\s+/, ' ') || '').strip
      puts_cyan "#{identifier} comment differs"
      puts_green model_comment2&.strip
      puts_yellow info[:comment]&.strip
      puts
    end
    mprops = model.properties.select { |mp| mp.decoration&.value == 'composition' }
    if info[:properties]
      info[:properties].each do |prop|
        matching_mprop = mprops.find { |mprop| mprop.name.downcase.gsub(/_dim$/, '') == prop[:identifier] }
        # existence check
        unless matching_mprop
          puts "#{identifier}.#{prop[:identifier]} not found among #{mprops.collect { |mp| mp.name }}"
          puts_cyan identifier
          puts_yellow info.pretty_inspect
          next
        end
        # property type check
        mproptype = matching_mprop.type.name
        if mproptype.strip == 'ANY DEFINED BY'
          # HACK the MetaInfo::Property does not reference the property that ANY DEFINED BY should reference.  This is derived completely by the order of the properties.  We aren't actually checking what we want to check here but it is hard to do so.
          mproptype = prop[:type]
        end
        unless mproptype == prop[:type]
          puts "#{identifier}.#{prop[:identifier]}(#{prop[:type]}) differs from #{mproptype}"
          # puts turn_cyan(identifier) + ' ' + turn_yellow(info.pretty_inspect) unless mproptype =~ /ANON/ || mproptype =~ /List$/
        end
        # comments check
        mprop_comment = matching_mprop.description_content
        
        unless (mprop_comment || '').strip == (prop[:comment] || '').strip
        #   puts_cyan "#{identifier} -- #{prop[:identifier]}"
        #   puts_green prop[:comment]
        #   puts_yellow mprop_comment
        #   puts
        end
        
      end
      
      # And now we will concern ourselves with the order of attributes
      prop_idents = info[:properties].collect { |prop| prop[:identifier] }
      mprop_names = mprops.collect(&:name)
      zip = prop_idents.zip(mprop_names)
      ok = true
      zip.each { |one, two| ok = false if one != two }
      unless ok
        puts_cyan identifier; puts prop_idents.inspect; puts mprop_names.inspect; puts
      end
    end
        
    # if info[:properties]
    # puts info.inspect
    # info.each do |prop|
    #   puts prop.inspect
    #   # puts "#{identifier} -- #{info}"
    # end
  end
  if model.is_a?(MetaInfo::Enumeration)
    model_comment  = model.description_content
    model_comment2 = model_comment.gsub(/[\n\r]+(?=.+)/m, ' ').gsub(/\s+/, ' ') if model_comment
    unless (model_comment2 || '').strip == (info[:comment]&.gsub(/\s+/, ' ') || '').strip
      puts_cyan "#{identifier} comment differs"
      puts_green model_comment2&.strip
      puts_yellow info[:comment]&.strip
      puts
    end
    lits = model.literals
    if info[:properties]
      info[:properties].each do |prop|
        matching_lit = lits.find { |lit| lit.name.gsub(/_dim$/, '') == prop[:identifier] }
        # existence check
        unless matching_lit
          puts_cyan identifier
          puts "#{prop[:identifier]} not found among #{lits.collect { |lit| lit.name }}"
          puts_yellow info.pretty_inspect
          lits.each do |lit|
            puts_red lit.name
            pchars = prop[:identifier].split('')
            zap = pchars.zip(lit.split(''))
            zap.each do |one, two|
              puts "#{one} !== #{two}" unless one == two
            end
          end
          next
        end
        # comments check
        lit_comment = matching_lit.description_content
        
        unless (lit_comment || '').strip == (prop[:comment] || '').strip
          puts_cyan "#{identifier} -- #{prop[:identifier]}"
          puts_green prop[:comment]
          puts_yellow lit_comment
          puts
        end
        
      end
      
      # And now we will concern ourselves with the order of attributes
      prop_idents = info[:properties].collect { |prop| prop[:identifier] }
      lit_names = lits.collect(&:name)
      zip = prop_idents.zip(lit_names)
      ok = true
      zip.each { |one, two| ok = false if one != two }
      unless ok
        puts_cyan identifier; puts prop_idents.inspect; puts lit_names.inspect; puts
      end
    end
  end
end


# AUDIT starting from models
mclasses = MetaInfo::Class.where(:is_asn1 => true).all
menums   = MetaInfo::Enumeration.where(:is_asn1 => true).all
masn1 = mclasses + menums
base_types = ['BIT STRING', 'INTEGER', 'OCTET STRING', 'CHOICE', 'SEQUENCE', 'SEQUENCE OF', 'ANY DEFINED BY']

masn1.each do |mdt|
  # next
  next if base_types.include?(mdt.name.strip)
  matching_def = @defs.find { |k,v| k.strip == mdt.name }
  unless matching_def
    puts mdt.to_definition # "#{mdt.name}"
    puts
  end
end

@defs.values.each do |d|
  if d[:properties]
    d[:properties].each do |prop|
      if prop[:anonymous]
        pp prop
        puts
      end
    end
  end
end

# puts @defs.keys.sort



# out = File.expand_path("~/projects/dim/lib/dim/standard/all_text.txt")
# File.open(out, 'w') {|f| f.puts lines};
