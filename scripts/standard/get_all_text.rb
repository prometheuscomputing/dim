fn = File.expand_path("~/projects/dim/lib/dim/standard/ieee_boilerplate/_original_document_readable.xml")
File.exist? fn
doc = File.read fn;
lines = []
doc.each_line do |l|
  text = l.slice(/(?<=<w:t>).+(?=<\/w:t>)/)
  lines << text if text
end
out = File.expand_path("~/projects/dim/lib/dim/standard/all_text.txt")
File.open(out, 'w') {|f| f.puts lines};
