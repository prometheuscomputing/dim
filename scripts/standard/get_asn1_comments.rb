fn = File.expand_path("~/projects/dim/lib/dim/standard/all_text.txt")
File.exist? fn
doc = File.read fn;
lines = []
doc.each_line do |l|
  lines << l if l =~ /(--|::=)/
end
comment_open = false
comments     = []
no_comments     = []
type         = nil
buffer       = []
lines.each do |line|
  case
  when line.strip == '--'
    comment_open = true
  when line =~ /--.+/
    if comment_open
      buffer << line.slice(/(?<=--).+/).strip
    end
  when line =~ /::=/
    type = line.slice(/.+(?= ::)/)
    entry = "  #{type.inspect} => #{buffer.join(' ').gsub('- ', '').inspect},"
    comments << entry
    comment_open = false
    buffer = []
  end
end
out = File.expand_path("~/projects/dim/lib/dim/standard/asn_header_comments.rb")
File.open(out, 'w') do |f|
  f.puts '{'
  f.puts comments
  f.puts '}'
end;
