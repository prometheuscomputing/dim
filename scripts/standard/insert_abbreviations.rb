abbrev_hash = {
  'ACSE' => 'association control service element',
  'ASN.1' => 'Abstract Syntax Notation One',
  'BCC' => 'bedside communication controller',
  'BER' => 'basic encoding rules',
  'BMP' => 'basic multilingual plane',
  'CMDIP' => 'Common Medical Device Information Protocol',
  'CMDISE' => 'common medical device information service element',
  'CMIP' => 'Common Management Information Protocol',
  'CMISE' => 'common management information service element',
  'DCC' => 'device communication controller',
  'DICOM' => 'digital imaging and communications in medicine',
  'DIM' => 'domain information model',
  'ECG' => 'electrocardiogram',
  'EEG' => 'electroencephalogram',
  'EBWW' => 'eyeball and wristwatch',
  'FSM' => 'finite state machine',
  'GMDN' => 'Global Medical Device Nomenclature',
  'GMT' => 'Greenwich mean time',
  'IANA' => 'Internet Assigned Numbers Authority',
  'ICS' => 'implementation conformance statement',
  'ICU' => 'intensive care unit',
  'ID' => 'identifier or identification',
  'LAN' => 'local area network',
  'LSB' => 'least significant bit',
  'MDIB' => 'medical data information base',
  'MDS' => 'medical device system',
  'MEDICOM' => 'medical imaging communication',
  'MIB or Mib' => 'management information base',
  'MOC' => 'managed object class',
  'OID' => 'object identifier',
  'OR' => 'operating room',
  'OSI' => 'open systems interconnection',
  'PC' => 'personal computer',
  'PDU' => 'protocol data unit',
  'PM' => 'persistent metric',
  'SCADA' => 'supervisory control and data acquisition',
  'SCP ECG' => 'Standard Communications Protocol [for computer-assisted] Electrocardiography',
  'SNMP' => 'Simple Network Management Protocol',
  'SNTP' => 'Simple Network Time Protocol',
  'UML' => 'unified modeling language',
  'UTC' => 'coordinated universal time',
  'VMD' => 'virtual medical device',
  'VMO' => 'virtual medical object',
  'VMS' => 'virtual medical system'
}

def add_abbreviations_to_standard(standard, abbreviations_hash)
  abbreviations_hash.each do |s, m|
    abbrev = Standard::Abbreviation.where(:symbol => s).first
    ChangeTracker.start
    abbrev ||= Standard::Abbreviation.create(:symbol => s, :meaning => m)
    standard.add_abbreviation abbrev
    standard.save
    ChangeTracker.commit
  end
end
standard = Standard::IEEEStandard.first
add_abbreviations_to_standard(standard, abbrev_hash)