defs_hash = {
   'agent' => 'Device that provides data in a manager-agent communicating system.',
   'alarm' => 'Signal that indicates abnormal events occurring to the patient or the device system.',
   'alert' => 'Synonym for the combination of patient-related physiological alarms, technical alarms, and equip- ment-user advisory signals.',
   'alert monitor' => 'Object representing the output of a device or system alarm processor and as such the overall device or system alarm condition.',
   'alert status' => 'Object representing the output of an alarm process that considers all alarm conditions in a scope that spans one or more objects.',
   'archival' => 'Relating to the storage of data over a prolonged period.',
   'association control service element (ACSE)' => 'Method used to establish logical connections between medical device systems (MDSs).',
   'channel' => 'Umbrella object in the object model that groups together physiological measurement data and data derived from these data.',
   'class' => 'Description of one or more objects with a uniform set of attributes and services including a description of how to create new objects in the class.',
   'communication controller' => 'Part of a medical device system (MDS) responsible for communications.',
   'communication party' => 'Actor of the problem domain that participates in the communication in that domain.',
   'communication role' => 'Role of a party in a communication situation defining the party¡¯s behavior in the communication. Associated with a communication role is a set of services that the party provides to other parties.',
   'data agent' => 'As a medical device, a patient data acquisition system that provides the acquired data for other devices.',
   'data format' => 'Arrangement of data in a file or stream.',
   'data logger' => 'A medical device that is functioning in its capacity as a data storage and archival system.',
   'data structure' => 'Manner in which application entities construct the data set information resulting from the use of an information object.',
   'dictionary' => 'Description of the contents of the medical data information base (MDIB) containing vital signs information, device information, demographics, and other elements of the MDIB.',
   'discrete parameter' => 'Vital signs measurement that can be expressed as a single numeric or textual value.',
   'domain information model (DIM)' => 'The model describing common concepts and relationships for a problem domain.',
   'event' => ' A change in device status that is communicated by a notification reporting service.',
   'event report' => 'Service [provided by the common medical device information service element (CMDISE)] to report an event relating to a managed object instance.',
   'framework' => 'A structure of processes and specifications designed to support the accomplishment of a specific task.',
   'graphic parameter' => 'Vital signs measurement that requires a number of regularly sampled data points in order to be expressed properly.',
   'host system' => 'Term used as an abstraction of a medical system to which measurement devices are attached.',
   'information object' => 'An abstract data model applicable to the communication of vital signs information and related patient data. The attributes of an information object definition describe its properties. Each information object definition does not represent a specific instance of real-world data, but rather a class of data that share the same properties.',
   'information service element' => 'Instances in the medical data information base (MDIB).',
   'instance' => 'The realization of an abstract concept or specification, e.g., object instance, application instance, information service element instance, virtual medical device (VMD) instance, class instance, operating instance.',
   'intensive care unit (ICU)' => 'The unit within a medical facility in which patients are managed using multiple modes of monitoring and therapy.',
   'interchange format' => 'The representation of the data elements and the structure of the message containing those data elements while in transfer between systems. The interchange format consists of a data set of construction elements and a syntax. The representation is technology specific.',
   'interoperability' => 'Idealized scheme where medical devices of differing types, models, or manufacturers are capable of working with each other, whether connected to each other directly or through a communication system.',
   'latency' => 'In a communications scenario, the time delay between sending a signal from one device and receiving it by another device.',
   'lower layers' => 'Layer 1 to Layer 4 of the International Organization for Standardization (ISO)/open systems interconnection (OSI) reference model. These layers cover mechanical, electrical, and general communication protocol specifications.',
   'manager' => 'Device that receives data in a manager-agent communicating system.',
   'manager-agent model' => 'Communication model where one device (i.e., agent) provides data and another device (i.e., manager) receives data.',
   'medical data information base (MDIB)' => 'The concept of an object-oriented database storing (at least) vital signs information.',
   'medical device' => 'A device, apparatus, or system used for patient monitoring, treatment, or therapy, which does not normally enter metabolic pathways. For the purposes of this standard, the scope of medical devices is further limited to patient-connected medical devices that provide support for electronic communications.',
   'medical device system (MDS)' => 'Abstraction for system comprising one or more medical functions. In the context of this standard, the term is specifically used as an object-oriented abstraction of a device that provides medical information in the form of objects that are defined in this standard.',
   'monitor' => 'A medical device designed to acquire, display, record, and/or analyze patient data and to alert caregivers of events needing their attention.',
   'object' => 'A concept, an abstraction, or a thing with crisp boundaries and a meaning for the problem at hand.',
   'object attributes' => 'Data that, together with methods, define an object.',
   'object class' => 'A descriptor used in association with a group of objects with similar properties (i.e., attributes), common behavior (i.e., operations), common relationships to other objects, and common semantics.',
   'object diagram' => 'Diagram showing connections between objects in a system.',
   'object method' => 'A procedure or process acting upon the attributes and states of an object class.',
   'object-oriented analysis' => 'Method of analysis where the problem domain is modelled in the form of objects and their interactions.',
   'open system' => 'A set of protocols allowing computers of different origins to be linked together.',
   'operation' => 'A function or transformation that may be applied to or by objects in a class (sometimes also called service).',
   'problem domain' => ' The field of health care under consideration in a modeling process.',
   'protocol' => 'A standard set of rules describing the transfer of data between devices. It specifies the format of the data and specifies the signals to start, control, and end the transfer.',
   'scanner' => 'An observer and ¡°summarizer¡± of object attribute values.',
   'scenario' => 'A formal description of a class of business activities including the semantics of business agreements, conventions, and information content.',
   'service' => 'A specific behavior that a communication party in a specific role is responsible for exhibiting.',
   'syntax (i.e., of an interchange format)' => 'The rules for combining the construction elements of the interchange format.',
   'system' => 'The demarcated part of the perceivable universe, existing in time and space, that may be regarded as a set of elements and relationships between these elements.',
   'timestamp' => 'An attribute or field in data that denotes the time of data generation.',
   'top object' => 'The ultimate base class for all other objects belonging to one model.',
   'upper layers' => 'Layer 5 to Layer 7 of the International Organization for Standardization (ISO)/open systems interconnection (OSI) reference model. These layers cover application, presentation, and session specifications and functionalities.',
   'virtual medical device (VMD)' => 'An abstract representation of a medical-related subsystem of a medical device system (MDS).',
   'virtual medical object (VMO)' => 'An abstract representation of an object in the Medical Package of the domain information model (DIM).',
   'vital sign' => 'Clinical information, relating to one or more patients, that is measured by or derived from apparatus connected to the patient or otherwise gathered from the patient.',
   'waveform' => 'Graphic data, typically vital signs data values varying with respect to time, usually presented to the clinician in a graphical form.',
}

def add_definitions_to_standard(standard, defs_hash)
  defs_hash.each do |t, d|
    obj = Standard::Definition.where(:term => t).first
    ChangeTracker.start
    obj ||= Standard::Definition.create(:term => t, :definition => d)
    standard.add_definition obj
    standard.save
    ChangeTracker.commit
  end
end
standard = Standard::IEEEStandard.first
add_definitions_to_standard(standard, defs_hash)