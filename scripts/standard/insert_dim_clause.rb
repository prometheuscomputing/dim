require_relative 'dim_standard.rb'
input_dir ||= File.expand_path("~/projects/dim_latex/latex")
input_filename = "dim.tex"
input = File.read(File.join(input_dir, input_filename))
input.encode!('UTF-16', 'UTF-8', :invalid => :replace, :replace => '')
input.encode!('UTF-8', 'UTF-16')
input = input.fix_cr
ChangeTracker.start unless ChangeTracker.started?
dim_clause = Standard::Clause.create(:title => "DIM")
# sclauses = input.scan(/(?<=\\sclause).*/m)
# sclauses = input.split(/\\sclause/)
# sclauses.shift
# sclauses.each{|c| c = c.strip.chomp}
# sclauses.each do |sc|
#   sc.strip!
#   title = sc.slice!(/\{.*?\}.*\n/).gsub(/\{/, "").gsub(/\}.*/, "").chomp
#   sclause = Standard::Clause.create(title:title)
#   dim_clause.subclauses_add sclause
#   ssclauses = sc.split(/\\ssclause/)
#   ssclauses = ssclauses.select{|sc| sc =~ /\w/}
# end

DIM_STANDARD.clauses_add dim_clause

sclause = Standard::Clause.create(title:"General")
dim_clause.subclauses_add sclause
  ssclause = Standard::Clause.create(title:"Modeling concept")
  sclause.subclauses_add ssclause
    txt = <<-END
      The DIM is an object-oriented model that consists of objects, their attributes, and their methods, which are abstractions of real-world entities in the domain of (vital signs information communicating) medical devices.

      The information model and the service model for communicating systems defined and used in this standard are conceptually based on the International Organization for Standardization (ISO)/open systems interconnection (OSI) system management model. Objects defined in the information model are considered managed (here, medical) objects. For the most part, they are directly available to management (i.e., access) services provided by the common medical device information service element (CMDISE) as defined in this standard.

      For communicating systems, the set of object instances available on any medical device that complies with the definitions of this standard forms the medical data information base (MDIB). The MDIB is a structured collection of managed medical objects representing the vital signs information provided by a particular medical device. Attribute data types, hierarchies, and behavior of objects in the MDIB are defined in this standard.

      The majority of objects defined here represent generalized vital signs data and support information. Specialization of these objects is achieved by defining appropriate attributes. Object hierarchies and relations between objects are used to express device configuration and device capabilities.

      \\begin{anexample}
        A generalized object is defined to represent vital signs in the form of a real-time waveform. A set of object attributes is used to specify a particular waveform as an invasive arterial blood pressure. The position in the hierarchy of all objects defines the subsystem that derives the waveform.
      \\end{anexample}

      Figure 6.1 shows the relation between managed medical objects, MDIB, CMDISE, application processes, and communication systems.

      %TODO insert Figure 6.1 -- MDIB in communicating systems

      In the case of communicating systems, managed medical objects are accessible only through services provided by CMDISE. The way that these objects are stored in the MDIB in any specific system and the way applications and the CMDISE access these objects are implementation issues and as such not normative.

      In the case of a vital signs archived data format that complies with the definitions in this standard, object instances are stored, together with their dynamic attribute value changes, over a certain time period on archival media. Attribute data types and hierarchies of objects in the archival data format are again defined in this standard.

      Figure 6.2 shows the relationship between managed medical objects, the data archive, and archive access services.
      In the case of the archived data format, the way managed medical objects are stored on a medium is the subject of standardization. The access services are a local implementation issue and as such are not intended to be governed by this standard.

      %TODO insert Figure 6.2 -- Managed medical objects in a vital signs archive
    END
    text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
    ssclause.content_add text_element
    ssclause.save
    ssclause = Standard::Clause.create(title:"Scope of the DIM")
    sclause.subclauses_add ssclause
      sssclause = Standard::Clause.create(title:"General")
      ssclause.subclauses_add sssclause
        txt = <<-END
          Vital signs information objects that are defined in this standard encompass digitized biosignals that are derived by medical measurement devices used, for example, in anaesthesia, surgery, infusion therapy, intensive care, and obstetrical care.

          Biosignal data within the scope of this standard include direct and derived, quantitative and qualitative measurements, technical and medical alarms, and control settings. Patient information relevant for the interpretation of these signals is also defined in the DIM.
        END
      text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
      sssclause.content_add text_element
      sssclause.save
      sssclause = Standard::Clause.create(title:"Communicating systems")
      ssclause.subclauses_add sssclause
        txt = <<-END
          Communicating systems within the scope of this standard include physiological meters and analysers, especially systems providing real-time or continuous monitoring. Data processing capabilities are required for these systems.

          Information management objects that provide capabilities and concepts for cost-effective communication (specifically data summarization objects) and objects necessary to enable real-time communication are also within the scope of the information model in this standard.
        
          Interoperability issues, specifically lower communication layers, temporal synchronization between multiple devices, etc., are outside the scope of this standard.
        END
      text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
      sssclause.content_add text_element
      sssclause.save
      sssclause = Standard::Clause.create(title:"Archived vital signs")
      ssclause.subclauses_add sssclause
        txt = <<-END
          Context information objects that describe the data acquisition process and organize a vital signs archive are within the scope of this standard.
        END
      text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
      sssclause.content_add text_element
      sssclause.save
      
    ssclause.save
    ssclause = Standard::Clause.create(title:"Approach")
    sclause.subclauses_add ssclause
      txt = <<-END
        For the object-oriented modeling, the unified modeling language (UML) technique is used. The domain is first subdivided into different packages, and this subdivision permits the organization of the model into smaller packages. Each package is then defined in the form of object diagrams. Objects are briefly introduced, and their relations and hierarchies are defined in the object diagram.
      
        For the object definitions, a textual approach is followed. Attributes are defined in attribute definition tables. Attribute data types are defined using Abstract Syntax Notation One (ASN.1). Object behavior and notifications generated by objects are also defined in definition tables. These definitions directly relate to the service model specified in Clause 8.
      END
    text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
    ssclause.content_add text_element
    ssclause.save

    ssclause = Standard::Clause.create(title:"Extension of the model")
    sclause.subclauses_add ssclause
      txt = <<-END
        It is expected that over time extensions of the model may be needed to account for new developments in the area of medical devices. Also, in special implementations, there may be a requirement to model data that are specific for a particular device or a particular application (and that are, therefore, not covered by the general model).
        In some cases, it may be possible to use the concept of external object relations. Most objects defined in this standard provide an attribute group (e.g., the Relationship Attribute Group) that can be used to supply information about related objects that are not defined in the DIM. Supplying such information can be done by specifying a relation to an external object and assigning attributes to this relation (see 7.1.2.20). 
        %TODO fix the above to point to the correct section -- External object relation list data type
      
        In other cases, it may be necessary to define completely new objects or to add new attributes, new methods, or new events to already defined objects. These extensions are considered private or manufacturer-specific extensions. Dealing with these extensions is primarily a matter of an interoperability standard that is based on this standard on vital signs representation.

        In general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. The nomenclature code space (i.e., code values) leaves room for private extensions. As a general rule, an interoperability standard that is based on this DIM should be able to deal with private or manufacturer-specific extensions by ignoring objects, attributes, etc., with unknown identifiers (i.e., nomenclature codes).
      END
    text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
    ssclause.content_add text_element
    ssclause.save

    sclause.save
    sclause = Standard::Clause.create(title:"Package diagram-overview")
    dim_clause.subclauses_add sclause
      txt = <<-END
        The package diagram organizes the problem domain into separate groups. It shows the major objects inside each package and defines the relationships between these packets.
        % TODO Fix figure reference to Figure 6.3 -- Packages of the DIM
        The package diagram depicted in Figure 6.3 contains only a small subset of all objects defined in the DIM. Common base objects except the Top object are not shown in this diagram. Also, not all relations are shown between the different packages. Refer to the detailed package diagrams for more information.
        % TODO insert Figure -- Packages of the DIM
        % TODO fix reference to "Clause 7"
        The numbers in the packages refer to the corresponding subclauses in this clause about models and in Clause 7 about the object definitions.

        The Top object is an abstract base class and at the same time the ultimate base class for all objects defined in the model. For editorial convenience, the modeling diagrams in this standard do not show this inheritance hierarchy.
        % TODO fix references to 6.3-6.10
        The more detailed models for these packages are contained in 6.3 through 6.10.
      END
    text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
    sclause.content_add text_element

    sclause.save
    sclause = Standard::Clause.create(title:"Model for the Medical Package")
    dim_clause.subclauses_add sclause
      txt = <<-END
        The Medical Package deals with the derivation and representation of biosignals and contextual information that is important for the interpretation of measurements.
        % TODO fix fig ref 6.4 - Medical Package model
        Figure 6.4 shows the object model of the Medical Package.

        \begin{anote}
          Instances of the Channel object and the PM-Store object shall be contained in exactly one superior object instance. Refer to the Alert Package for information about alarm-related objects.\footnote{Notes in text, tables, and figures are given for information only, and do not contain requirements needed to implement the standard.}
        \end{anote}
    
        % TODO fix refs
        The Medical Package model contains the objects described in 6.3.1 through 6.3.13.
      END
    text = Gui_Builder_Profile::RichText.create(content:txt)
    text_element = Standard::Text.create(content:text)
    sclause.content_add text_element
    sclause.save
    dim_clause.save
  #
  #   \ssclause{VMO (i.e., virtual medical object)}
  #     The VMO is the base class for all medical-related objects in the model. It provides consistent naming and identification across the Medical Package model.
  #
  #     As a base class, the VMO cannot be instantiated.
  #
  #   \ssclause{VMD (i.e., virtual medical device) object}
  #     The VMD object is an abstraction for a medical-related subsystem (e.g., hardware or even pure software) of a medical device. Characteristics of this subsystem (e.g., modes, versions) are captured in this object. At the same time, the VMD object is a container for objects representing measurement and status information.
  #     \begin{anexample}
  #       A modular patient monitor provides measurement modalities in the form of plug-in modules. Each module is represented by a VMD object.
  #     \end{anexample}
  #
  #   \ssclause{Channel object}
  #     The Channel object is used for grouping Metric objects and, thus, allows hierarchical information organization. The Channel object is not mandatory for representation of Metric objects in a VMD.
  #
  #     \begin{anexample}
  #       A blood pressure VMD may define a Channel object to group together all metrics that deal with the blood pressure (e.g., pressure value, pressure waveform). A second Channel object can be used to group together metrics that deal with heart rate.
  #     \end{anexample}
  #
  #   \ssclause{Metric object}
  #     The Metric object is the base class for all objects representing direct and derived, quantitative and qualitative biosignal measurement, status, and context data.
  #
  #     Specializations of the Metric object are provided to deal with common representations (e.g., single values, array data, status indications) and presentations (e.g., on a display) of measurement data.
  #
  #     As a base class, the Metric object cannot be instantiated.
  #
  #   \ssclause{Numeric object}
  #     The Numeric object represents numerical measurements and status information, e.g., amplitude measures, counters.
  #     \begin{anexample}
  #       A heart rate measurement is represented by a Numeric object.
  #     \end{anexample}
  #     \begin{anote}
  #       A compound Numeric object is defined as an efficient model, for example, for arterial blood pressure, which usually has three associated values (i.e., systolic, diastolic, mean). The availability of multiple values in a single Numeric (or other Metric) object can be indicated in a special structure attribute in the Metric object.
  #     \end{anote}
  #
  #   \ssclause{Sample Array object}
  #     The Sample Array object is the base class for metrics that have a graphical, curve type presentation and, therefore, have their observation values reported as arrays of data points by communicating systems.
  #
  #     As a base class, the Sample Array object cannot be instantiated.
  #
  #   \ssclause{Real Time Sample Array object}
  #     The Real Time Sample Array object is a sample array that represents a real-time continuous waveform. As such, it has special requirements in communicating systems, e.g., processing power, low latency, high bandwidth. Therefore, it requires the definition of a specialized object.
  #     \begin{anexample}
  #       An electrocardiogram (ECG) real-time wave is represented as a Real Time Sample Array object.
  #     \end{anexample}
  #
  #   \ssclause{Time Sample Array object}
  #     The Time Sample Array object is a sample array that represents noncontinuous waveforms (i.e., a wave snippet). Within a single observation (i.e., a single array of sample values), samples are equidistant in time.
  #     \begin{anexample}
  #       Software for ST segment analysis may use the Time Sample Array object to represent snippets of ECG real-time waves that contain only a single QRS complex. Within this wave snippet, the software can locate the ST measurement points. It generates a new snippet, for example, every 15 s.
  #     \end{anexample}
  #
  #   \ssclause{Distribution Sample Array object}
  #     The Distribution Sample Array object is a sample array that represents linear value distributions in the form of arrays containing scaled sample values. The index of a value within an observation array denotes a spatial value, not a time point.
  #
  #     \begin{anexample}
  #       Example: An electroencephalogram (EEG) application may use a Fourier transformation to derive a frequency distribution (i.e., a spectrum) from the EEG signal. It then uses the Distribution Sample Array object to represent that spectrum in the MDIB.
  #     \end{anexample}
  #
  #   \ssclause{Enumeration object}
  #     The Enumeration object represents status information and/or annotation information. Observation values may be presented in the form of normative codes (that are included in the nomenclature defined in this standard or in some other nomenclature scheme) or in the form of free text.
  #     \begin{anexample}
  #       A ventilator device may provide extensive breath analysis capabilities. For each breath, it calculates various numerical values (e.g., volumes, I:E ratio, timing information) as well as enumerated information (e.g., breath type classification, annotation data). For efficiency, all this information is grouped together in one Complex Metric object instance, which is updated upon each breath.
  #     \end{anexample}
  #
  #   \ssclause{PM-Store (i.e., persistent metric) object}
  #     The PM-Store object provides long-term storage capabilities for metric data. It contains a variable number of PM-Segment objects that can be accessed only through the PM-Store object. Without further specialization, the PM-Store object is intended to store data of a single Metric object only.
  #     \begin{anexample}
  #       A device stores the numerical value of an invasive blood pressure on a disk. It uses the PM-Store object to represent this persistent information. Attributes of the PM-Store object describe the sampling period, the sampling algorithm, and the storage format. When the label of the pressure measurement is changed (e.g., during a wedge procedure), the storage process opens a new PM-Segment to store the updated context data (here: the label).
  #     \end{anexample}
  #
  #   \ssclause{PM-Segment object}
  #     The PM-Segment object represents a continuous time period in which a metric is stored without any changes of relevant metric context attributes (e.g., scales, labels).
  #
  #     The PM-Segment object is accessible only through the PM-Store object (e.g., for retrieving stored data, the PM-Store object has to be accessed).
  #
  # \sclause{Model for the Alert Package}
  #   The Alert Package deals with objects that represent status information about patient condition and/or technical conditions influencing the measurement or device functioning. Alert-related information is often subject to normative regulations and, therefore, requires special handling.
  #   In the model, all alarm-related object-oriented items are identified by the term \textit{alert}. The term \textit{alert} is used in this standard as a synonym for the combination of patient-related physiological alarms, technical alarms, and equipment user-advisory signals.
  #   An alarm is a signal that indicates abnormal events occurring to the patient or the device system. A physiological alarm is a signal that either indicates that a monitored physiological parameter is out of specified limits or indicates an abnormal patient condition. A technical alarm is a signal that indicates a device system is either not capable of accurately monitoring the patient's condition or no longer monitoring the patient's condition.
  #   The model defines three different levels of alarming. These levels represent different sets of alarm processing steps, ranging from a simple context-free alarm event detection to an intelligent device system alarm process. This process is required to prioritize all device alarms, to latch alarms if needed (a latched alarm does not stop when the alarm condition goes away), and to produce audible and visual alarm indications for the user.
  #   For consistent system-wide alarming, a particular medical device may provide either no alarming capability or exactly one level of alarming, which is dependent on the capabilities of the device. Each level is represented by one specific object class. In other words, either zero or one alarm object class (e.g., only Alert or only Alert Status or only Alert Monitor; no combinations) is instantiated in the device containment tree. Multiple instances of a class are allowed.
  #   \begin{anote}
  #     Medical device alarming is subject to various national and international safety standards (e.g., IEC 60601 series, ISO 9703 series). Considering requirements of current safety standards, objects in this standard define information contents only. Any implementation shall, therefore, follow appropriate standards for dynamic alarming behavior.
  #   \end{anote}
  #   % TODO fix ref
  #   Figure 6.5 shows the object model of the Alert Package.
  #   % TODO insert figure 6.5 -- Alert Package model
  #   \begin{anote}
  #     Instances of objects in the Alert Package area shall be contained in exactly one superior object.
  #   \end{anote}
  #   % TODO fix refs
  #   The Alert Package model contains the objects described in 6.4.1 through 6.4.3.
  #
  #   \ssclause{Alert object}
  #     The Alert object stands for the status of a simple alarm condition check. As such, it represents a single alarm only. The alarm can be either a physiological alarm or a technical alarm condition of a related object [e.g., MDS (i.e., medical device system), VMD, Metric]. If a device instantiates an Alert object, it shall not instantiate the Alert Status or the Alert Monitor object. A single Alert object is needed for each alarm condition that the device is able to detect.
  #     The Alert object has a reference to an object instance in the Medical Package to which the alarm condition relates.
  #     \begin{anote}
  #       An Alert object instance is not dynamically created or deleted in cases where alarm conditions start or stop. Rather, an existing Alert object instance changes attribute values in these cases.
  #     \end{anote}
  #     \begin{anexample}
  #       An Alert object may represent the status of a process that checks for a limit violation physiological alarm of the heart rate signal. In the case of a violation of the limit, the object generates an event (i.e., attribute update) that represents this alarm condition in the form of attribute value changes.
  #     \end{anexample}
  #
  #   \ssclause{Alert Status object}
  #     The Alert Status object represents the output of an alarm process that considers all alarm conditions in a scope that spans one or more objects. In contrast to the Alert object, the Alert Status object collects all alarm conditions related to a VMD object hierarchy or related to an MDS object and provides this information in list-structured attributes. Collecting all alarms together allows the implementation of first-level alarm processing where knowledge about the VMD or MDS can be used to prioritize alarm conditions and to suppress known false alarm indications.
  #     For larger scale devices without complete alarm processing, the Alert Status object greatly reduces the overhead of a large number of Alert object instances.
  #     If a device instantiates an Alert Status object, it shall not instantiate the Alert or the Alert Monitor object. Each VMD or MDS in the MDIB is able to contain at most one Alert Status object instance.
  #     \begin{anexample}
  #       An ECG VMD derives a heart rate value. As the VMD is able to detect that the ECG leads are disconnected from the patient, its Alert Status object reports only a technical alarm and suppresses a heart rate limit violation alarm in this case.
  #     \end{anexample}
  #
  #   \ssclause{Alert Monitor object}
  #     The Alert Monitor object represents the output of a device or system alarm processor. As such, it represents the overall device or system alarm condition and provides a list of all alarm conditions of the system in its scope. This list includes global state information and individual alarm state information that allows the implementation of a safety-standard-compliant alarm display on a remote system.
  #     If a device instantiates an Alert Monitor object, it shall not instantiate the Alert or the Alert Status object. An MDS shall contain not more than one Alert Monitor object instance.
  #     \begin{anexample}
  #       A patient-monitoring system provides alarm information in the form of an Alert Monitor object to a central station. Alert information includes the current global maximum severity of audible and visual alarm conditions on the monitor display as well as a list of active technical and physiological alarm conditions. The alarm processor operates in a latching mode where physiological alarm conditions are buffered until they are explicitly acknowledged by a user.
  #     \end{anexample}
  #
  # \sclause{Model for the System Package}
  #   The System Package deals with the representation of devices that derive or process vital signs information and comply with the definitions in this standard.
  #   % TODO fix refs
  #   Figure 6.6 shows the object model for the System Package.
  #   % TODO insert Figure 6.6¡ªSystem Package model
  #   % TODO fix refs
  #   The System Package model contains the objects described in 6.5.1 through 6.5.10.
  #
  #   \ssclause{VMS (i.e., virtual medical system) object}
  #     The VMS object is the abstract base class for all System Package objects in this model. It provides consistent naming and identification of system-related objects.
  #     As a base class, the VMS object cannot be instantiated.
  #
  #   \ssclause{MDS object}
  #     The MDS object is an abstraction of a device that provides medical information in the form of objects that are defined in the Medical Package of the DIM.
  #     The MDS object is the top-level object in the device's MDIB and represents the instrument itself. Composite devices may contain additional MDS objects in the MDIB.
  #     Further specializations of this class are used to represent differences in complexity and scope.
  #     As a base class, the MDS object cannot be instantiated.
  #
  #   \ssclause{Simple MDS object}
  #     The Simple MDS object represents a medical device that contains a single VMD instance only (i.e., a single-purpose device).
  #
  #   \ssclause{Hydra MDS object}
  #     The Hydra MDS object represents a device that contains multiple VMD instances (i.e., a multipurpose device).
  #
  #   \ssclause{Composite Single Bed MDS object}
  #     The Composite Single Bed MDS object represents a device that contains (or interfaces with) one or more Simple or Hydra MDS objects at one location (i.e., a bed).
  #
  #   \ssclause{Composite Multiple Bed MDS object}
  #     The Composite Multiple Bed MDS object represents a device that contains (or interfaces with) multiple MDS objects at multiple locations (i.e., multiple beds).
  #
  #   \ssclause{Event Log object}
  #     The Event Log object is a general Log object that stores system events in a free-text representation.
  #     \begin{anexample}
  #       An infusion device may want to keep track of mode and rate changes by remote systems. When a remote operation is invoked, it creates an entry in its event log.
  #     \end{anexample}
  #
  #   \ssclause{Battery object}
  #     For battery-powered devices, some battery information is contained in the MDS object in the form of attributes. If the battery subsystem is either capable of providing more information (i.e., a smart battery) or manageable, then a special Battery object is provided.
  #
  #   \ssclause{Clock object}
  #     The Clock object provides additional capabilities for handling date-related and time-related information beyond the basic capabilities of an MDS object. It models the real-time clock capabilities of an MDS object.
  #     The Clock object is used in applications where precise time synchronization of medical devices is needed. This object provides resolution and accuracy information so that applications can synchronize real-time data streams between devices.
  #
  # \sclause{Model for the Control Package}
  #   The Control Package contains objects that allow remote measurement control and device control.
  #   The model for remote control defined in this standard provides the following benefits:
  #   \begin{itemize}
  #     \item A system that allows remote control is able to explicitly register which attributes or features can be accessed or modified by a remote system.
  #     \item For attributes that can be remotely modified, a list of possible legal attribute values is provided to the controlling system.
  #     \item It is not mandatory that a remote-controllable item correspond to an attribute of an medical object.
  #     \item Dependence of a controllable item on internal system states is modeled.
  #     \item A simple locking transaction scheme allows the handling of transient states during remote control.
  #   \end{itemize}
  #   At least two different uses of remote control are considered:
  #   \begin{itemize}
  #     \item Automatic control may be done by some processes running on the controlling device. Such a process has to be able to discover automatically how it can modify or access the controllable items to provide its function.
  #     \item It is also possible to use remote control to present some form of control interface to a human operator. For this use, descriptions of functions, and possibly help information, need to be provided.
  #   \end{itemize}
  #   The basic concept presented here is based on Operation objects. An Operation object allows modification of a virtual attribute. This virtual attribute may, for example, be a measurement label, a filter state (on/off), or a gain factor. The attribute is called virtual because it need not correspond to any attribute in other objects instantiated in the system.
  #   Different specializations of the Operation object define how the virtual attribute is modified. A Select Item operation, for example, allows the selection of an item from a given list of possible item values for the attribute. A Set Value operation allows the setting of the attribute to a value from a defined range with a specific step width (i.e., resolution).
  #   The idea is that the Operation object provides all necessary information about legal attribute values. Furthermore, the Operation object defines various forms of text string to support a human user of the operation. It also contains grouping information that allows logical grouping of multiple Operation objects together when they are presented as part of a human interface.
  #   Operation objects cannot directly be accessed by services defined in the service model in Clause 8. Instead, all controls shall be routed through the SCO (i.e., service and control object). This object supports a simple locking mechanism to prevent side effects caused by simultaneous calls.
  #   The SCO groups together all Operation objects that belong to a specific entity (i.e., MDS, VMD). The SCO also allows feedback to a controlled device, for example, for a visual indication that the device is currently remote-controlled.
  #   % TODO fix refs
  #   Figure 6.7 shows the object model for the Control Package:
  #   % TODO insert Figure 6.7¡ªControl Package model
  #   % TODO fix refs
  #   The Control Package model contains the objects described in 6.6.1 through 6.6.9.
  #
  #   \ssclause{SCO}
  #     The SCO is responsible for managing all remote-control capabilities that are supported by a medical device.
  #     Remote control in medical device communication is sensitive to safety and security issues. The SCO provides means for the following:
  #     \begin{itemize} % TODO -- this list used to be an enumeration with a) and b) -- is this change alright or do we need to figure out how to do a special enumeration here in order to handle this?
  #       \item Simple transaction processing, which prevents inconsistencies when a device is controlled from multiple access points (e.g., local and remote) and during the processing of control commands.
  #      \item State indications, which allows local and remote indication of ongoing controls.
  #     \end{itemize}
  #
  #   \ssclause{Operation object}
  #     The Operation object is the abstract base class for classes that represent remote-controllable items. Each Operation object allows the system to modify some specific item (i.e., a virtual attribute) in a specific way defined by the Operation object. Operation objects are not directly accessible by services defined in the service model in Clause 8. All controls shall be routed through the SCO object (i.e., the parent) to allow a simple form of transaction processing.
  #     The set of Operation objects instantiated by a particular medical device defines the complete remote control interface of the device. This way a host system is able to discover the remote control capabilities of a device in the configuration phase.
  #
  #   \ssclause{Select Item Operation object}
  #     The Select Item Operation object allows the selection of one item out of a given list.
  #     \begin{anexample}
  #       The invasive pressure VMD may allow modification of its label. It uses a Select Item Operation object for this function. The list of legal values supplied by the operation may be, for example, {ABP, PAP, CVP, LAP}. By invoking the operation, a user is able to select one value out of this list.
  #     \end{anexample}
  #
  #   \ssclause{Set Value Operation object}
  #     The Set Value Operation object allows the adjustment of a value within a given range with a given resolution.
  #     \begin{anexample}
  #       The Set Value Operation object allows the adjustment of a value within a given range with a given resolution.
  #     \end{anexample}
  #
  #   \ssclause{Set String Operation object}
  #     The Set String Operation object allows the system to set the contents of an opaque string variable of a given maximum length and format.
  #     \begin{anexample}
  #       An infusion device may allow a remote system to set the name of the infused drug in free-text form to show it on a local display. It defines an instance of the Set String Operation object for this function. The operation specifies the maximum string length and the character format so that the device is able to show the drug name on a small display.
  #     \end{anexample}
  #
  #   \ssclause{Toggle Flag Operation object}
  #     The Toggle Flag Operation object allows operation of a toggle switch (with two states, e.g., on/off).
  #     \begin{anexample}
  #       An ECG VMD may support a line frequency filter. It uses the Toggle Flag Operation object for switching the filter on or off.
  #     \end{anexample}
  #
  #   \ssclause{Activate Operation object}
  #     The Activate Operation object allows a defined activity to be started (e.g., a zero pressure).
  #     \begin{anexample}
  #       The zero procedure of an invasive pressure VMD may be started with an Activate Operation object.
  #     \end{anexample}
  #
  #   \ssclause{Limit Alert Operation object}
  #     The Limit Alert Operation object allows adjustment of the limits of a limit alarm detector and the switching of the limit alarm to on or off.
  #
  #   \ssclause{Set Range Operation object}
  #     The Set Range Operation object allows the selection of a value range by the simultaneous adjustment of a low and high value within defined boundaries.
  #     \begin{anexample}
  #       A measurement VMD may provide an analog signal input for which the signal input range can be adjusted with a Set Range Operation object.
  #     \end{anexample}
  #
  # \sclause{Model for the Extended Services Package}
  #   The Extended Services Package contains objects that provide extended medical object management services that allow efficient access to medical information in communicating systems. Such access is achieved by a set of objects that package attribute data from multiple objects in a single event message.
  #   The objects providing extended services are conceptually derived from ISO/OSI system management services defined in the ISO/IEC 10164 family of standards (specifically Part 5 and Part 13). The definitions have been adapted to and optimized for specific needs in the area of vital signs communication between medical devices.
  #   % TODO fix refs
  #   Figure 6.8 shows the object model for the Extended Services Package.
  #   % TODO insert Figure 6.8¡ªExtended Services Package model
  #   % TODO fix refs
  #   The Extended Services Package model contains the objects described in 6.7.1 through 6.7.9.
  #
  #   \ssclause{Scanner object}
  #     A Scanner object is a base class that is an observer and ¡°summarizer¡± of object attribute values. It observes attributes of managed medical objects and generates summaries in the form of notification event reports. These event reports contain data from multiple objects, which provide a better communication performance compared to separate polling commands (e.g., GET service) or multiple individual event reports from all object instances.
  #     Objects derived from the Scanner object may be instantiated either by the agent system itself or by the manager system (e.g., dynamic scanner creation by using the CREATE service).
  #     As a base class, the Scanner object cannot be instantiated.
  #
  #   \ssclause{CfgScanner (i.e., configurable scanner) object}
  #     The CfgScanner object is a base class that has a special attribute (i.e., the ScanList attribute) that allows the system to configure which object attributes are scanned. The ScanList attribute may be modified either by the agent system (i.e., auto-configuration or pre-configuration) or by the manager system (i.e., full dynamic configuration by using the SET service).
  #     A CfgScanner object may support different granularity for scanning:
  #     \begin{itemize} % TODO -- this list used to be an enumeration with a) and b) -- is this change alright or do we need to figure out how to do a special enumeration here in order to handle this?
  #       \item Attribute group (i.e., a defined set of attributes): The ScanList attribute contains the identifiers (IDs) of attribute groups, and all attributes in the group are scanned.
  #       \item Individual attribute: The ScanList attribute contains the IDs of all attributes that are scanned.
  #     \end{itemize}
  #     In order to deal efficiently with optional object attributes, the attribute group scan granularity is recommended for CfgScanner objects.
  #     As a base class, the CfgScanner object cannot be instantiated.
  #
  #   \ssclause{EpiCfgScanner (i.e., episodic configurable scanner) object}
  #     The EpiCfgScanner object is responsible for observing attributes of managed medical objects and for reporting attribute changes in the form of unbuffered event reports.
  #     The unbuffered event report is triggered only by object attribute value changes. If the EpiCfgScanner object uses attribute group scan granularity, the event report contains all attributes of the scanned object that belong to this attribute group if one or more of these attributes changed their value.
  #     \begin{anexample}
  #       A medical device provides heart beat detect events in the form of an Enumeration object. A display application creates an instance of the EpiCfgScanner object and adds the observed value of the Enumeration object to the ScanList attribute. The scanner instance afterwards sends a notification when the Enumeration object reports a heart beat.
  #     \end{anexample}
  #
  #   \ssclause{PeriCfgScanner (i.e., periodic configurable scanner) object}
  #     The PeriCfgScanner object is responsible for observing attributes of managed medical objects and for periodically reporting attribute values in the form of buffered event reports. A buffered event report contains the attribute values of all available attributes that are specified in the scan list, independent of attribute value changes.
  #     If the scanner operates in a special superpositive mode, the buffered event report contains all value changes of attributes that occurred in the reporting period; otherwise, the report contains only the most recent attribute values.
  #     \begin{anexample}
  #       A data logger creates an instance of the PeriCfgScanner object and configures the scanner so that it sends an update of the observed value attributes of all Numeric objects in the MDIB every 15 s.
  #     \end{anexample}
  #
  #   \ssclause{FastPeriCfgScanner (i.e., fast periodic configurable scanner) object}
  #     The FastPeriCfgScanner object is a specialized object class for scanning the observed value attribute of the Real Time Sample Array object. This special scanner object is further optimized for low-latency reporting and efficient communication bandwidth utilization, which is required to access real-time waveform data.
  #     \begin{anexample}
  #       A real-time display application (e.g., manager system) wants to display ECG waveforms. It creates a FastPeriCfgScanner object on the agent system (e.g., server device) and requests periodic updates of all ECG leads.
  #     \end{anexample}
  #
  #   \ssclause{UcfgScanner (i.e., unconfigurable scanner) object}
  #     The UcfgScanner object is a base class that scans a predefined set of managed medical objects that cannot be modified. In other words, an UcfgScanner object typically is a reporting object that is specialized for one specific purpose.
  #     As a base class, the UcfgScanner object cannot be instantiated.
  #
  #   \ssclause{Context Scanner object}
  #     The Context Scanner object is responsible for observing device configuration changes. After instantiation, the Context Scanner object is responsible for announcing the object instances in the device¡¯s MDIB. The scanner provides the object instance containment hierarchy and static object attribute values.
  #     In case of dynamic configuration changes, the Context Scanner object sends notifications about new object instances or deleted object instances.
  #     \begin{anexample}
  #       A data logger creates an instance of the Context Scanner object in an agent MDIB to receive notifications about MDS configuration changes when new measurement modules are plugged in (i.e., new VMD instance) or when such a module is unplugged (i.e., VMD instance deleted).
  #     \end{anexample}
  #
  #   \ssclause{Alert Scanner object}
  #     The Alert Scanner object is responsible for observing the alert-related attribute groups of objects in the Alert Package. As alarming in general is security-sensitive, the scanner is not configurable (i.e., all or no Alert objects are scanned).
  #     The Alert Scanner object sends event reports periodically so that timeout conditions can be checked.
  #
  #   \ssclause{Operating Scanner object}
  #     The Operating Scanner object is responsible for providing all information about the operating and control system of a medical device.
  #     In other words, the scanner maintains the configuration of Operation objects contained in SCOs (by sending CREATE notifications for Operation objects), it scans transaction-handling-related SCO attributes, and it scans Operation object attributes. Because SCOs and Operation objects may have dependencies, the scanner is not configurable.
  #
  # \sclause{Model for the Communication Package}
  #   The Communication Package deals with objects that enable and support basic communication.
  #   % TODO fix refs
  #   Figure 6.9 shows the object model for the Communication Package.
  #   % TODO insert Figure 6.9¡ªCommunication Package model
  #   % TODO fix refs
  #   The Communication Package model contains the objects described in 6.8.1 through 6.8.6.
  #
  #   \ssclause{Communication Controller object}
  #     The Communication Controller object represents the upper layer and lower layer communication profile of a medical device.
  #     The Communication Controller object is the access point for retrieving Device Interface attributes and management information base element (MibElement) attributes for obtaining management information related to data communications.
  #     As a base class, the Communication Controller object cannot be instantiated. Two instantiable specializations, however, are defined: BCC and DCC. A medical device MDIB contains either no Communication Controller object or one BCC object or one DCC object (depending on its role).
  #
  #   \ssclause{DCC (i.e., device communication controller) object}
  #     The DCC object is a Communication Controller object used by medical devices operating as agent systems (i.e., association responders).
  #     The DCC object shall contain one or more Device Interface objects.
  #
  #   \ssclause{BCC (i.e., bedside communication controller) object}
  #     The BCC object is a Communication Controller object used by medical devices operating as manager systems (i.e., association requestors).
  #     The BCC object shall contain one or more Device Interface objects.
  #
  #   \ssclause{Device Interface object}
  #     The Device Interface object represents a particular interface, i.e., port. The port is either a logical or a physical end point of an association for which (e.g., statistical) data captured in the MibElement objects can be independently collected.
  #     Both an agent system and a manager system can have multiple logical or physical ports, depending on the selected implementation of the lower layer communication system.
  #     The Device Interface object is not accessible by CMDISE services. This object contains at least one Mib-Element object (i.e., the Device Interface MibElement object, which represents device interface properties), which can be accessed by a special method defined by the Communication Controller object.
  #
  #   \ssclause{MibElement object}
  #     The MibElement object contains statistics and performance data for one Device Interface object. The MibElement object is a base class for specialized MibElement objects only. It cannot be instantiated.
  #     Various MibElement object types are defined to group management information in defined packages, which can be generic or dependent on specific transport profiles.
  #     The MibElement object is not directly accessible. Its attributes can be accessed only through a Communication Controller object. The MibElement object is not part of the device¡¯s MDIB.
  #
  #   \ssclause{Specialized MibElement object}
  #     Management information for a communication link is dependent on the lower layers of the communication stack (i.e., lower layers profile).
  #     This standard, however, defines only two generic MibElement objects:
  #     \begin{itemize}
  #       \item The mandatory Device Interface MibElement object, which describes properties of the device interface
  #       \item An optional general communication statistics MibElement object, which models typical communication statistics that are generally applicable
  #     \end{itemize}
  #     Specialized MibElement objects are defined in IEEE P1073.2.1.2.
  #
  # \sclause{Model for the Archival Package}
  #   The Archival Package deals with storage and representation of biosignals, status, and context information in an on-line or an off-line archive.
  #   % TODO fix refs
  #   Figure 6.10 shows the object model of the Archival Package.
  #   % TODO insert Figure 6.10¡ªArchival Package model
  #   % TODO fix refs
  #   The Archival Package model contains the objects described in 6.9.1 through 6.9.7.
  #
  #   \ssclause{Multipatient Archive object}
  #     The Multipatient Archive object groups together multiple Patient Archive ojbects referring to different patients.
  #     \begin{anexample}
  #       A drug study may be documented in the form of a Multipatient Archive object containing multiple Patient Archive objects that show how the drug affected the monitored vital signs.
  #     \end{anexample}
  #
  #   \ssclause{Patient Archive object}
  #     The Patient Archive object groups patient-related information (e.g., vital signs data, treatment data, and patient demographics) together in a single archive object. This object relates to static (i.e., invariant) data in a Patient Demographics object only.
  #     \begin{anexample}
  #       A hospital may store data about multiple visits of a single patient in a Patient Archive object that contains a number of Session Archive objects, each documenting vital signs information recorded during a specific visit in a hospital department.
  #     \end{anexample}
  #
  #   \ssclause{Session Archive object}
  #     The Session Archive object represents a patient visit or a continuous stay in the a hospital or hospital department. Diagnostic treatments performed during this time period are represented by Session Test objects contained in the Session Archive object. The Session Archive object refers to dynamic (i.e., variant) data in a Patient Demographics object.
  #
  #   \ssclause{Physician object}
  #     The Physician object represents the physician responsible for the set of diagnostic and therapeutic activities during the time period represented by the Session Archive object.
  #
  #   \ssclause{Session Test object}
  #     The Session Test object contains vital signs information of a single patient that is recorded during a single examination or diagnostic treatment. This object contains vital signs metrics in form of PM-Store objects. It also may contain information about equipment that was used for recording (in the form of relations to MDS and Ancillary objects).
  #     \begin{anexample}
  #       Vital signs information recorded during a ECG stress test examination is organized in a Session Test object.
  #     \end{anexample}
  #
  #   \ssclause{Session Notes object}
  #     The Session Notes object is a container for diagnostic data, patient care details, and treatment-related information in the form of textual data.
  #
  #   \ssclause{Ancillary object}
  #     The Ancillary object is not further defined in this standard. This object is present in the model to indicate that information from sources other than devices within the scope of this standard are permitted to be included in (or referenced by) the Session Test object.
  #     \begin{anexample}
  #       Image data that complies with the MEDICOM (CEN ENV 12052) or DICOM (NEMA PS 3) standard are permitted to be included in the Session Test object as ancillary data.
  #     \end{anexample}
  #
  # \sclause{Model for the Patient Package}
  #   The Patient Package deals with all patient-related information that is relevant in the scope of this standard, but is not vital signs information modeled in the Medical Package.
  #   % TODO fix refs
  #   Figure 6.11 shows the object model for the Patient Package.
  #   % TODO insert Figure 6.11¡ªPatient Package model
  #   % TODO fix refs
  #   The Patient Package model contains one object (see 6.10.1).
  #
  #   \ssclause{Patient Demographics object}
  #     The Patient Demographics object stores patient census data.
  #     This standard provides minimal patient information as typically required by medical devices. A complete patient record is outside the scope of this standard.
  # \sclause{DIM¡ªdynamic model}
  #   \ssclause{General}
  #     % TODO fix ref
  #     Subclause 6.11 defines global dynamic system behavior.
  #     % TODO fix ref
  #     Note that dynamic object behavior resulting from invocation of object management services is part of the object definitions in Clause 7.
  #
  #   \ssclause{MDS communication finite state machine (FSM)}
  #     % TODO fix ref
  #     Figure 6.12 shows the MDS FSM for a communicating medical device that complies with the definitions in this standard. The FSM is used to synchronize the operational behavior of manager (i.e., client) systems and agent (i.e., server) systems.
  #     % TODO insert Figure 6.12¡ªMDS FSM
  #     After power-up, the device performs all necessary local initializations (i.e, boot phase) and ends up in the disconnected state, where it waits for connection events.
  #     When a connection event is detected, the device tries to establish a logical connection (i.e, an association) with the other device. A manager (i.e., client) system is the association requester, and an agent (i.e, server) system is the association responder. Basic compatibility checks are performed in the associating state.
  #     After successful association, configuration data (i.e., the MDIB structure) is exchanged by the use of services and extended services (in particular, the Context Scanner object) as defined in this standard. Additional information (e.g., MDS attributes) is supplied that allows further compatibility and state checks.
  #     After configuration, medical data are exchanged by using services and extended services as defined in this standard. Dynamic reconfiguration is allowed in the operating state. If the device or the type of reconfiguration does not allow dynamic handling in the operating state, a special ¡°reconfiguring¡± state is provided.
  #     If an event indicates an intention to disconnect, the disassociating state is entered.
  #     The diagram does not show error events. Fatal error events take the state machine out of the operating state.
  #     \begin{anote}
  #       This state machine describes the behavior of the MDS communication system only. Usually the device must perform its medical function independent of the communication system.
  #     \end{anote}
  #     The FSM is considered a part of the MDS object. The MDS Status attribute reflects the state of the machine. The MDS may announce state changes in the form of attribute change event reports.
  #     Specific application profiles shall use this state machine as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.
  #
  #   \ssclause{Communicating systems¡ªstartup object interaction diagram}
  #     % TODO Fix refs
  #     Figure 6.13 presents the object interaction diagram that visualizes the startup phase after connecting two devices.
  #     % TODO insert Figure 6.13¡ªStartup after connection
  #     Some form of connection indication is necessary to make the manager system aware of a new agent on the network. This mechanism is dependent on the specific lower layer implementation; therefore, it is not further defined in this standard.
  #     Specific application profiles shall use this interaction diagram as a general guideline, but they may define specific deviations to fulfill specific profile-dependent requirements or assumptions.
  #
  #   \ssclause{Communication Package¡ªMibElement data access}
  #     % TODO Fix refs
  #     Figure 6.14 presents the object interaction diagram that shows how a manager system accesses the MibElement data using the objects defined in the Communication Package (see 6.8).
  #     % TODO insert Figure 6.14¡ªMibElement data access
  #     The diagram assumes the following:
  #     \begin{itemize}
  #       \item An association is established between the agent and the manager.
  #       \item The configuration phase is finished, and the manager has an image of the agent¡¯s MDIB.
  #       \item The DCC object is part of the agent¡¯s MDIB.
  #     \end{itemize}
  #     The manager first uses the GET service to retrieve all DCC object attributes and their values. The attributes specify how many Device Interface objects exist.
  #     The manager uses the ACTION service with the Communication Controller object-defined method to retrieve the attributes of the mandatory Device Interface MibElement object. The MibElement attribute variables specify if any additional MibElement objects are available for the interface.
  #     If so, the manager can use the same ACTION command to retrieve the additional management information represented in the MibElement objects.
  #
  #   \ssclause{Dynamic object relations}
  #     This subclause deals with relations between managed medical objects (i.e., objects that are defined as managed objects in this standard).
  #     Generally, the relationships between object instances that are defined in the package models are dynamic.
  #     \begin{anexample}
  #       Example: A modular patient monitor is modeled as an MDS. Measurement modules are modeled as VMDs. If a new module is connected to the monitor, there is also a new relationship between the MDS and the new VMD instance.
  #     \end{anexample}
  #     Communicating agent systems (i.e., agents) use services defined in this standard to announce configuration change events to other connected systems. These manager systems (i.e., managers) modify their view of the agent MDIB.
  #     Not only does a vital signs information archive have to update its configuration, but it also has to permanently store these connection and disconnection events.
  #     \begin{anexample}
  #       An instance of the Session Archive object represents the stay of a patient in the intensive care unit (ICU). During that period, new devices are connected to the patient to increase the number of recorded vital signs. They are removed again as soon as the patient¡¯s condition stabilizes. The Session Archive object shall not delete recorded data when the recording device is disconnected.
  #     \end{anexample}
  #     Thus, in certain applications (e.g., archival applications), object instance relationships have associated information that must be captured.
  #     % TODO fix refs
  #     When required, the relationships themselves can be considered to be special managed objects as shown in Figure 6.15.
  #     % TODO insert Figure 6.15¡ªExample of a relationship represented by an object
  #     % TODO fix refs
  #     The example in Figure 6.15 shows a relation between a Session Archive object and an MDS object. The relation is represented as an object. This object has attributes that provide information, for example, about time of connection and disconnection.
  #     Modeling the relations as objects has the advantage that information can be defined in the form of attributes. It is not necessary to assign the attributes to one or both objects that are related.
  #     % TODO fix refs
  #     How dynamic object relations are handled by systems that comply with the definitions in this standard is defined in 6.11.5.1 and 6.11.5.2.
  #
  #     \sssclause{Dynamic object relations in communicating systems}
  #       Relations between object instances that are defined in the package models are considered configuration information. The Context Scanner object provides configuration information in the form of object create notifications and object delete notifications. No means for persistent storage of past (i.e., old) configuration information is defined for communicating systems.
  #       Other relations between object instances (e.g., to reference a source signal in derived data) are specified in the form of attributes of the corresponding objects (e.g., the Vmo-Source-List attribute of the Metric object). Dynamic changes of these attributes are announced by attribute change notification events.
  #
  #     \sssclause{Dynamic object relations in archival systems}
  #       % TODO fix refs
  #       An archival system may need to provide persistent storage for configuration information. In this case, the corresponding relations are considered to be represented by objects, as shown in 6.11.5.
  #       An archival system that uses a data format in compliance with the definitions in this standard has to provide means to store (i.e., archive) the attributes of dynamic relationship objects.
  DIM_STANDARD.save
  ChangeTracker.commit