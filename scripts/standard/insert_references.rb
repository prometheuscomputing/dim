refs_hash = {
  'CEN EN 1064' => {:description => 'Medical informatics —  Standard communication protocol —  computer-assisted electrocardiography.', :footnote => 'CEN publications are available from the European Committee for Standardization (CEN), 36, rue de Stassart, B-1050 Brussels, Belgium (http://www.cenorm.be).'},
  'CEN ENV 12052' => {:description => 'Medical informatics —  Medical imaging communication (MEDICOM).'},
  'IEEE Std 1073\texttrademark' => {:description => 'IEEE Standard for Medical Device Communications— Overview and Framework.', :footnote => 'IEEE publications are available from the Institute of Electrical and Electronics Engineers, 445 Hoes Lane, Piscataway, NJ 08854, USA (http://www.standards.ieee.org/).'},
  'IETF RFC 1155' => {:description => 'Structure and Identification of Management Information for TCP/IP-Based Internets.', :footnote => 'Internet requests for comment (RFCs) are available from the Internet Engineering Task Force at http://www.ietf.org/.'},
  'ISO 639-1' => {:description => 'Code for the representation of names of languages —  Part 1: Alpha-2 code.', :footnote => 'ISO publications are available from the ISO Central Secretariat, Case Postale 56, 1 rue de Varemb\'{e}, CH-1211, Gen\`{e}ve 20, Switzerland/Suisse (http://www.iso.ch/). ISO publications are also available in the United States from the Sales Department, American National Standards Institute, 25 West 43rd Street, 4th Floor, New York, NY 10036, USA (http://www.ansi.org/).'},
  'ISO 639-2' => {:description => 'Codes for the representation of names of languages —  Part 2: Alpha-3 code.'},
  'ISO 3166-1' => {:description => 'Codes for the representation of names of countries and their subdivisions —  Part 1: Country codes.'},
  'ISO 3166-2' => {:description => 'Codes for the representation of names of countries and their subdivisions —  Part 2: Country subdivision code.'},
  'ISO 3166-3' => {:description => 'Codes for the representation of names of countries and their subdivisions —  Part 3: Code for formerly used names of countries.'},
  'ISO 8601' => {:description => 'Data elements and interchange formats —  Information interchange —  Representation of dates and times.'},
  'ISO 15225' => {:description => 'Nomenclature —  Specification for a nomenclature system for medical devices for the purpose of regulatory data exchange.'},
  'ISO/IEC 646' => {:description => 'Information technology —  ISO 7-bit coded character set for information interchange.', :footnote => 'ISO/IEC documents can be obtained from the ISO office, 1 rue de Varemb\'{e}, Case Postale 56, CH-1211, Gen\`{e}ve 20, Switzerland/ Suisse (http://www.iso.ch/) and from the IEC office, 3 rue de Varemb\'{e}, Case Postale 131, CH-1211, Gen\`{e}ve 20, Switzerland/Suisse (http://www.iec.ch/). ISO/IEC publications are also available in the United States from the Sales Department, American National Standards Institute, 25 West 43rd Street, 4th Floor, New York, NY 10036, USA (http://www.ansi.org/).'},
  'ISO/IEC 2022' => {:description => 'Information technology —  Character code structure and extension techniques.'},
  'ISO/IEC 5218' => {:description => 'Information technology —  Codes for the representation of human sexes.'},
  'ISO/IEC 7498-1' => {:description => 'Information technology —  Open systems interconnection —  Basic reference model —  Part 1: The basic model.'},
  'ISO/IEC 7498-2' => {:description => 'Information processing systems —  Open systems interconnection —  Basic reference model —  Part 2: Security architecture.'},
  'ISO/IEC 7498-3' => {:description => 'Information processing systems —  Open systems interconnection —  Basic reference model —  Part 3: Naming and addressing.'},
  'ISO/IEC 7498-4' => {:description => 'Information processing systems —  Open systems interconnection —  Basic reference model —  Part 4: Management framework.'},
  'ISO/IEC 8649' => {:description => 'Information processing systems —  Open systems interconnection —  Service definition for the Association Control Service Element.'},
  'ISO/IEC 8650-1' => {:description => 'Information technology —  Open systems interconnection —  Connection-Oriented Protocol for the Association Control Service Element —  Part 1: Protocol.'},
  'ISO/IEC 8650-2' => {:description => 'Information technology —  Open systems interconnection —  Protocol Specification for Association Control Service Element —  Part 2: Protocol Implementation Conformance Statements (PICS) proforma.'},
  'ISO/IEC 8824-1' => {:description => 'Information technology —  Abstract Syntax Notation One (ASN.1) —  Part 1: Specification of basic notation.'},
  'ISO/IEC 8824-2' => {:description => 'Information technology —  Abstract Syntax Notation One (ASN.1) —  Part 2: Information object specification.'},
  'ISO/IEC 8859-n' => {:description => 'Information processing —  8-bit single-byte coded graphic character sets —  Part 1 to Part 15: Various alphabets.'},
  'ISO/IEC 9545' => {:description => 'Information technology —  Open systems interconnection —  Application layer structure.'},
  'ISO/IEC 9595' => {:description => 'Information technology —  Open systems interconnection —  Common management information service definition.'},
  'ISO/IEC 9596-1' => {:description => 'Information technology —  Open systems interconnection —  Common Management Information Protocol —  Part 1: Specification.'},
  'ISO/IEC 10040' => {:description => 'Information technology —  Open systems interconnection —  Systems management overview.'},
  'ISO/IEC 10164-1' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 1: Object management function.'},
  'ISO/IEC 10164-2' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 2: State management function.'},
  'ISO/IEC 10164-3' => {:description => 'Information technology —  Open systems interconnection —  System management —  Part 3: Attributes for representing relationships.'},
  'ISO/IEC 10164-4' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 4: Alarm reporting function.'},
  'ISO/IEC 10164-5' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 5: Event management function.'},
  'ISO/IEC 10164-6' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 6: Log control function.'},
  'ISO/IEC 10164-7' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 7: Security alarm reporting function.'},
  'ISO/IEC 10164-8' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 8: Security audit trail function.'},
  'ISO/IEC 10164-9' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 9: Objects and attributes for access control.'},
  'ISO/IEC 10164-10' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 10: Usage metering function for accounting purposes.'},
  'ISO/IEC 10164-11' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 11: Metric objects and attributes.'},
  'ISO/IEC 10164-12' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 12: Test management function.'},
  'ISO/IEC 10164-13' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 13: Summarization function.'},
  'ISO/IEC 10164-14' => {:description => 'Information technology —  Open systems interconnection —  Systems management —  Part 14: Confidence and diagnostic test categories.'},
  'ISO/IEC 10165-1' => {:description => 'Information technology —  Open systems interconnection —  Structure of management information —  Part 1: Management information model.'},
  'ISO/IEC 10165-2' => {:description => 'Information technology —  Open systems interconnection —  Structure of management information —  Part 2: Definition of management information.'},
  'ISO/IEC 10646-1' => {:description => 'Information technology —  Universal multiple-octet coded character set (UCS) —  Part 1: Architecture and basic multilingual plane.'},
  'ISO/IEEE 11073-10101' => {:description => 'Health informatics —  Point-of-care medical device communication —  Part 10101: Nomenclature.'},
  'ISO/IEEE 11073-20101' => {:description => 'Health informatics —  Point-of-care medical device communication —  Part 20101: Application profiles ¨C Base standard.'},
  'NEMA PS 3' => {:description => 'Digital imaging and communications in medicine (DICOM).', :footnote => 'NEMA publications are available from Global Engineering Documents, 15 Inverness Way East, Englewood, Colorado 80112, USA (http://global.ihs.com/).'}
}
def add_references_to_standard(standard, refs_hash)
  refs_hash.each do |s, data|
    obj = Standard::NormativeReference.where(:source => s).first
    ChangeTracker.start
    unless obj
      obj = Standard::NormativeReference.new(:source => s)
      obj.description = Gui_Builder_Profile::RichText.new(:content => data[:description])
      if data[:footnote]
        fn = Standard::Footnote.new
        fn.content = Gui_Builder_Profile::RichText.new(:content => data[:footnote])
        fn.save
        obj.add_footnote fn
      end
      obj.save
    end
    standard.add_normative_reference obj
    standard.save
    ChangeTracker.commit
  end
end

standard = Standard::IEEEStandard.first
add_references_to_standard(standard, refs_hash)