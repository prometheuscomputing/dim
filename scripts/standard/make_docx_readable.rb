require 'nokogiri'
def readify_xml(input_path, output_path)
  input_path = File.expand_path(input_path)
  return nil unless File.exist?(input_path)
  output_path = File.expand_path(output_path)
  File.open(output_path, 'w') { |f| f.puts Nokogiri::XML(File.read input_path) };
end

readify_xml("~/projects/dim/lib/dim/standard/document_original_unreadable.xml", "~/projects/dim/lib/dim/standard/document_readable.xml")

# readify_xml("~/projects/dim_reference/toc_footer_header/word/document.xml", "~/projects/dim_reference/toc_footer_header/word/document_readable.xml")