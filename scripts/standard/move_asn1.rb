codes = Standard::Code.all
info = codes.collect{|c| "#{c.title.gsub(/Data Type:\s/, '').strip} -- #{c.id}"}.sort;
code_ids = {}
codes.each{|c| code_ids["#{c.title.gsub(/Data Type:\s/, '').strip}"] = c.id};
pp code_ids;

check = codes.collect{|c| c.title.gsub(/Data Type:\s/, '')};
puts_green check.uniq.count == codes.count

# class_clauses = Standard::Clause.where(Sequel.ilike(:title, "% class%"))
dim = MetaInfo::Package.where(:name => "DIM").first;
dims = dim.child_packages.collect(&:classifiers).flatten;
attr_clauses = dims.collect do |d|
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Attributes"}
  "#{d.name.inspect} => {:clause_id => #{clz.id}, :codes => [ ]}," if clz # Ancillary isn't in the standard...
end;

bclauses = {}
eclauses = {}
dims.each do |d|
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Behavior"}
  puts "No Behavior for #{d.name}" unless clz
  bclauses[d.name] = clz if clz
  clz = d.documents.find {|doc| doc.is_a?(Standard::Clause) && doc.title.strip == "Notifications"}
  puts "No Notification for #{d.name}" unless clz
  eclauses[d.name] = clz if clz
end;

asn1_def_orders = {
  "Channel" => {:clause_id => 1223, :codes => [ "ChannelStatus"  ]},
  "ComplexMetric" => {:clause_id => 1255, :codes => ["CmplxMetricInfo", "CmplxElemInfoList", "CmplxElemInfo", "CmplxObsValue", "CmplxObsValue", "CmplxFlags", "CmplxObsElemList", "CmplxObsElem", "CmplxObsElemFlags", "CmplxDynAttr", "CmplxDynAttrElemList", "CmplxDynAttrElem", "CmplxStaticAttr", "CmplxStaticAttrElemList", "CmplxStaticAttrElem" ]},
  "DistributionSampleArray" => {:clause_id => 1247, :codes => ["DsaRangeSpec", "MarkerListIndex", "MarkerEntryIndex" ]},
  "Enumeration" => {:clause_id => 1251, :codes => ["EnumObsValue", "EnumVal", "EnumRecordMetric", "EnumRecordOo", "EnumObsValueCmp", "EnumMsmtRange", "EnumMsmtRangeLabels", "EnumMsmtRangeLabel" ]},
  "Metric" => {:clause_id => 1227, :codes => [ "MetricStatus", "MetricSpec", "MetricStructure", "MetricAccess", "MetricCategory", "MetricRelevance", "MetricCalibration", "MetricCalEntry", "MetricCalType", "MetricCalState", "SiteList", "SiteListExt", "MetricSourceList", "VmoSourceList", "VmoSourceEntry", "MeasurementStatus", "AbsoluteRange", "MetricMeasure" ]},
  "Numeric" => {:clause_id => 1231, :codes => [ "NuObsValue", "NuObsValueCmp", "DispResolution" ]},
  "PM-Segment" => {:clause_id => 1259, :codes => ["SegDataGen", "SegDataNuOpt", "SegDataRtsaOpt" ]},
  "PM-Store" => {:clause_id => 1263, :codes => [ "StorageFormat", "StorageFormat" ], :bcodes => [ "SegmSelection", "SegmIdList", "AbsTimeRange", "AbsTimeRange", "SegmentAttr", "SegmentInfoList", "SegmentInfo" ]},
  "SampleArray" => {:clause_id => 1235, :codes => [ "SaObsValue", "SaObsValueCmp", "SaSpec", "SampleType", "SaFlags", "SaFilterSpec", "SaFilterEntry", "ScaleRangeSpec8", "ScaleRangeSpec16", "ScaleRangeSpec32", "SaCalData8", "SaCalData16", "SaCalData32", "SaCalDataType", "SaSignalFrequency", "ScaledRange8", "ScaledRange16", "ScaledRange32", "MarkerListSaVal8", "MarkerEntrySaVal8", "MarkerListSaVal16", "MarkerEntrySaVal16", "MarkerListSaVal32", "MarkerEntrySaVal32" ]},
  "TimeSampleArray" => {:clause_id => 1243, :codes => ["MarkerListRelTim", "MarkerEntryRelTim" ]},
  "VMD" => {:clause_id => 1219, :codes => ["VMDStatus", "MsmtPrinciple" ]},
  "Alert" => {:clause_id => 1277, :codes => [ "AlertCondition", "AlertControls", "AlertFlags", "AlertType", "LimitSpecEntry" ]},
  "AlertStatus" => {:clause_id => 1281, :codes => [ "AlertList", "AlertEntry", "AlertCapabList", "AlertCapabEntry", "LimitSpecList" ]},
  "AlertMonitor" => {:clause_id => 1285, :codes => ["LimitSpecList", "DevAlertCondition", "AlertState", "AlStatChgCnt", "DevAlarmList", "DevAlarmEntry" ]},
  "VMS" => {:clause_id => 1291, :codes => [ "SystemModel", "SystemModel", "SystemCapability", "SystemSpec", "SystemSpecEntry", "ProductionSpec", "ProdSpecEntry", "NomenclatureVersion" ]},
  "MDS" => {:clause_id => 1295, :codes => ["MDSStatus", "ApplicationArea", "PowerStatus", "PowerStatus", "LineFrequency" ], :bcodes => [ "MdsSetStateInvoke", "MdsSetStateResult" ], :ecodes => [ "MdsErrorInfo", "MdsCreateInfo", "MdsAttributeChangeInfo" ]},
  "Log" => {:clause_id => 1307, :bcodes => [ "ClearLogRangeInvoke", "ClearLogRangeResult", "ClearLogOptions", "ClearLogResult" ]},
  "EventLog" => {:clause_id => 1311, :codes => ["EventLogEntryList", "EventLogEntry", "EventLogInfo" ], :bcodes => [ "GetEventLogEntryInvoke", "GetEventLogEntryResult" ]},
  "Battery" => {:clause_id => 1315, :codes => ["BatteryStatus", "BatMeasure" ]},
  "Clock" => {:clause_id => 1319, :codes => [ "TimeSupport", "TimeCapability", "TimeProtocolId", "TimeStampId", "TimeStampId", "ExtTimeStampList", "DateTimeStatus", "DateTimeUsage", "AbsoluteTimeISO", "SNTPTimeStamp", "AbsoluteRelativeTimeSync", "UTCTimeZone", "UTCTimeZone", "LeapSecondsTransition" ], :bcodes => [ "SetTimeInvoke", "SetTimeZoneInvoke", "SetLeapSecondsInvoke" ], :ecodes => [ "ClockStatusUpdateInfo" ]},
  "ActivateOperation" => {:clause_id => 1349, :codes => [ ]},
  "LimitAlertOperation" => {:clause_id => 1353, :codes => [ "AlOpCapab", "CurLimAlStat", "CurLimAlVal", "AlertOpTextString" ]},
  "Operation" => {:clause_id => 1329, :codes => [ "OperSpec", "OperTextStrings", "OpOptions", "OpLevel", "OpGrouping" ]},
  "SCO" => {:clause_id => 1325, :codes => ["ScoActivityIndicator", "ScoCapability" ], :bcodes=> [ "OperationInvoke", "OpInvokeList", "OpInvokeElement", "OpModType", "OperationInvokeResult", "OpInvResult", "CtxtHelpRequest", "CtxtHelpResult", "CtxtHelp" ], :ecodes => [ "ScoOperReqSpec", "ScoOperInvokeError" ]},
  "SelectItemOperation" => {:clause_id => 1333, :codes => [ "SelectList", "SelectUValueEntry" ]},
  "SetRangeOperation" => {:clause_id => 1357, :codes => ["CurrentRange", "RangeOpText" ]},
  "SetStringOperation" => {:clause_id => 1341, :codes => ["SetStringSpec", "SetStrOpt" ]},
  "SetValueOperation" => {:clause_id => 1337, :codes => ["OpSetValueRange", "OpValStepWidth", "StepWidthEntry" ]},
  "ToggleFlagOperation" => {:clause_id => 1345, :codes => ["ToggleState", "ToggleLabelStrings" ]},
  "AlertScanner" => {:clause_id => 1391, :codes => [ ]},
  "CfgScanner" => {:clause_id => 1367, :codes => [ "ScanList", "ScanEntry", "ConfirmMode", "ScanConfigLimit" ]},
  "ContextScanner" => {:clause_id => 1387, :codes => [ "ContextMode" ], :ecodes => [ "ObjCreateInfo", "CreateEntry", "CreatedObject", "ObjDeleteInfo" ]},
  "EpiCfgScanner" => {:clause_id => 1371, :codes => [ ]},
  "FastPeriCfgScanner" => {:clause_id => 1379, :ecodes => [ "FastScanReportInfo", "SingleCtxtFastScan", "RtsaObservationScan" ]},
  "OperatingScanner" => {:clause_id => 1395, :ecodes => [ "OpElemAttr", "OpElemAttrList", "OpCreateInfo", "OpCreateEntry", "OpDeleteInfo", "OpDeleteEntry", "OpAttributeInfo", "SingleCtxtOperScan", "OpAttributeScan" ]},
  "PeriCfgScanner" => {:clause_id => 1375, :codes => [ "ScanExtend" ]},
  "Scanner" => {:clause_id => 1363, :bcodes => [ "RefreshObjList", "RefreshObjEntry" ], :ecodes => ["ScanReportInfo", "SingleCtxtScanObservationScan" ]},
  "UcfgScanner" => {:clause_id => 1383, :codes => [ ]},
  "CommunicationController" => {:clause_id => 1401, :codes => [ "CcCapability", "CC-Oid", "CcExtMgmtProto" ], :bcodes => [ "GetMibDataRequest", "MibIdList", "GetMibDataResult", "MibDataList", "MibDataEntry" ]},
  "DeviceInterface" => {:clause_id => 1413, :codes => [ ]},
  "DeviceInterfaceMibElement" => {:clause_id => 1421, :codes => [ "SupportedProfileList", "MibElementList", "DifMibPortState" ]},
  "GeneralCommunicationStatisticsMibElement" => {:clause_id => 1425, :codes => [ "MibCcGauge", "MibCcCounter", "MibCcCommMode" ]},
  "MultiPatientArchive" => {:clause_id => 1431, :codes => [ ]},
  "PatientArchive" => {:clause_id => 1435, :codes => [ "ArchiveProtection" ]},
  "Physician" => {:clause_id => 1443, :codes => [ "Authorization" ]},
  "SessionArchive" => {:clause_id => 1439, :codes => [ ]},
  "SessionNotes" => {:clause_id => 1451, :codes => [ ]},
  "SessionTest" => {:clause_id => 1447, :codes => [ ]},
  "PatientDemographics" => {:clause_id => 1457, :codes => [ "PatDemoState", "PatMeasure", "PatientSex", "PatientType", "PatientRace" ], :bcodes => [ "AdmitPatInfo" ]},
  "Top" => {:clause_id => 1210, :codes => [ ]}

}

mover = {}

asn1_def_orders.each do |klass_name, data|
  move_to = Standard::Clause[data[:clause_id]]
  next unless data[:codes]
  data[:codes].each do |str|
    code = Standard::Code[code_ids[str]]
    if code
      ChangeTracker.start
      cont = code.containers.first
      cont.content_remove(code) if cont
      cont.save
      ChangeTracker.commit
      ChangeTracker.start
      move_to.content_add code
      move_to.save
      ChangeTracker.commit
    end
  end
end;

asn1_def_orders.each do |klass_name, data|
  move_to = bclauses[klass_name]
  if data[:bcodes]
    data[:bcodes].each do |str|
      code = Standard::Code[code_ids[str]]
      if code
        ChangeTracker.start
        cont = code.containers.first
        cont.content_remove(code) if cont
        cont.save
        ChangeTracker.commit
        ChangeTracker.start
        move_to.content_add code
        move_to.save
        ChangeTracker.commit
      end
    end
  end
  move_to = eclauses[klass_name]
  if data[:ecodes]
    data[:ecodes].each do |str|
      code = Standard::Code[code_ids[str]]
      if code
        ChangeTracker.start
        cont = code.containers.first
        cont.content_remove(code) if cont
        cont.save
        ChangeTracker.commit
        ChangeTracker.start
        move_to.content_add code
        move_to.save
        ChangeTracker.commit
      end
    end
  end
end;
