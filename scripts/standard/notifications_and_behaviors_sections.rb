clauses = Standard::Clause.where(:title => 'Notifications').all;
clauses.each do |c|
  me = c.model_element
  unless me
    # puts "WARNING, no model for #{c.containers.first.title}"
    next
  end
  text = c.content.first
  ChangeTracker.start
  text[:content_markup_language_id] = 4
  if me.defined_events.any?
    # puts "#{c.containers.first.title} has events"
    text.content = Gui_Builder_Profile::RichText.new(:content => "<p>The #{me.name} class defines the events in Table Events_Table_Number.</p>")
    text.save
    # table = Standard::Table.new(:title => "#{me.name} class events")
    # table.save
    # table.render_as = 'notifications'
    # table.save
    # c.content_add(table)
  else
    text.content = Gui_Builder_Profile::RichText.new(:content => "<p>The #{me.name} class does not generate any special notifications.</p>")
    text.save
  end
  c.save
  ChangeTracker.commit
end;

clauses = Standard::Clause.where(:title => 'Behavior').all;
clauses.each do |c|
  me = c.model_element
  unless me
    # puts "WARNING, no model for #{c.containers.first.title}"
    next
  end
  text = c.content.first
  ChangeTracker.start
  text[:content_markup_language_id] = 4
  if me.defined_methods.any?
    # puts "#{c.containers.first.title} has behavior"
    text.content = Gui_Builder_Profile::RichText.new(:content => "<p>The #{me.name} class defines the methods in Table Methods_Table_Number.</p>")
    text.save
    # table = Standard::Table.new(:title => "#{me.name} class methods")
    # table.save
    # table.render_as = 'behaviors'
    # table.save
    # c.content_add(table)
  else
    text.content = Gui_Builder_Profile::RichText.new(:content => "<p>The #{me.name} class does not define any special methods.</p>")
    text.save
  end
  c.save
  ChangeTracker.commit
end;

  # b = Standard::Text.new
  # b_text = "The #{me.name} class defines in Table Attribute_Groups_Table_Number the attribute groups or extensions to inherited attribute groups."
  # b.content = Gui_Builder_Profile::RichText.new(:content => b_text)
  # b.save
