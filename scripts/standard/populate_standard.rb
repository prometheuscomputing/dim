module Standard
  class Clause
    def add_clause clause; add_subclause clause; end
    def clauses; subclauses; end
  end
end
def ct
  ChangeTracker.commit if ChangeTracker.started?
  ChangeTracker.start
end
  
$clauses = Standard::Clause.all
def build_standard_package package, parent
  ct
  clause = $clauses.find{|c| c.model_element == package}
  unless clause
    clause = new_clause(package)
    clause.title = "Package #{package.name}"
  end
  clause.save
  ct
  parent.add_clause(clause) unless parent.clauses.include?(clause)
  ct
  package.child_packages.each{|p| build_standard_package p, clause}
  package.classifiers.each{|c| build_standard_class c, clause}
end

def build_standard_class klass, parent
  ct
  clause = $clauses.find{|c| c.model_element == klass}
  unless clause
    clause = new_clause(klass)
  end
  if klass.qualified_name =~ /ASN/
    clause.title = "Data Type: #{klass.name}"
  else
    clause.title = "Class #{klass.name}"
  end
  clause.save
  ct
  parent.add_clause(clause) unless parent.clauses.include?(clause)
  ct
end

def new_clause model_element
  ct
  clause = Standard::Clause.new
  clause.model_element = model_element
  $clauses << clause
  ct
  clause
end
  
def wipe_standard
  ChangeTracker.cancel
  Standard.classes(no_imports:true).each do |klass|
    if klass < Sequel::Model
      DB[klass.table_name].delete if DB.table_exists?(klass.table_name)
    else 
      puts "#{klass} is not a Sequel::Model"
    end
  end
  DB[ChangeTracker::Identity.table_name].delete
end

wipe_standard
ChangeTracker.cancel
ct
# standard = Standard::IEEEStandard.first || Standard::IEEEStandard.new
standard = Standard::IEEEStandard.new
standard.save
ct
root_packages = [
  MetaInfo::Package.where(:name => 'CommonDataTypes').first,
  MetaInfo::Package.where(:name => 'Top').first,
  MetaInfo::Package.where(:name => 'Medical').first,
  MetaInfo::Package.where(:name => 'Alert').first,
  MetaInfo::Package.where(:name => 'System').first,
  MetaInfo::Package.where(:name => 'Control').first,
  MetaInfo::Package.where(:name => 'ExtendedServices').first,
  MetaInfo::Package.where(:name => 'Communication').first,
  MetaInfo::Package.where(:name => 'Archival').first,
  MetaInfo::Package.where(:name => 'Patient').first,
]

root_packages.each{|p| build_standard_package p, standard}
ChangeTracker.commit if ChangeTracker.started?