module Standard
  class Clause
    def add_clause clause; add_subclause clause; end
    def clauses; subclauses; end
  end
end
def ct
  ChangeTracker.commit if ChangeTracker.started?
  ChangeTracker.start
end
  
$clauses = Standard::Clause.all
def build_standard_package package, parent, asn1_packages
  ct
  clause = $clauses.find{|c| c.model_element == package}
  unless clause
    clause = new_clause(package)
    clause.title = "Package #{package.name}"
  end
  clause.save
  ct
  parent.add_clause(clause) unless parent.clauses.include?(clause)
  ct
  package.child_packages.each do |p|
    if p.qualified_name =~ /ASN/
      asn1_packages << p
    else
      build_standard_package p, clause, asn1_packages
    end
  end
  package.classifiers.each{|c| build_standard_class c, clause}
end

def build_standard_class klass, parent
  ct
  clause = $clauses.find{|c| c.model_element == klass}
  unless clause
    clause = new_clause(klass)
  end
  if klass.qualified_name =~ /ASN/
    clause.title = "Data Type: #{klass.name}"
  else
    clause.title = "Class #{klass.name}"
  end
  clause.save
  ct
  parent.add_clause(clause) unless parent.clauses.include?(clause)
  ct
end

def build_asn1_packages(asn1_packages, standard)
  ct
  # clause = $clauses.find{|c| c.model_element == package}
  # unless clause
    clause = new_clause
    clause.title = "ASN.1 Definitions"
  # end
  # clause.save
  # ct
  standard.add_clause(clause)
  standard.save
  ct
  asn1_classifiers = []
  asn1_packages.each do |asn1p|
    raise "oops I have kids.  #{asn1p.inspect}" if asn1p.child_packages.any?
    # in Sequence_A we do them in package order.  in Sequence_B we get them all, sort alphabetically and then go that way.  Comment out one or the other.
    # Sequence_A
    # asn1p.classifiers.each{|c| build_asn1_class c, clause}
    # Sequence_B
    asn1p.classifiers.each{|c| asn1_classifiers << c}
  end
  # Sequence_B
  asn1_classifiers.sort_by{|c| c.name.downcase}.each{|c| build_standard_class c, clause}
end

def new_clause model_element = nil
  ct
  clause = Standard::Clause.new
  clause.model_element = model_element if model_element
  clause.save
  ct
  $clauses << clause
  clause
end
  
def wipe_standard
  ChangeTracker.cancel
  DB[Standard::IEEEStandard.table_name].delete
  DB[Standard::Clause.table_name].delete
  DB[ChangeTracker::Identity.table_name].delete
end

# wipe_standard
ChangeTracker.cancel
ct
standard = Standard::IEEEStandard.new
standard.title = "ASN1 at the end"
standard.save
ct
root_packages = [
  MetaInfo::Package.where(:name => 'Top').first,
  MetaInfo::Package.where(:name => 'Medical').first,
  MetaInfo::Package.where(:name => 'Alert').first,
  MetaInfo::Package.where(:name => 'System').first,
  MetaInfo::Package.where(:name => 'Control').first,
  MetaInfo::Package.where(:name => 'ExtendedServices').first,
  MetaInfo::Package.where(:name => 'Communication').first,
  MetaInfo::Package.where(:name => 'Archival').first,
  MetaInfo::Package.where(:name => 'Patient').first,
]
asn1_packages = MetaInfo::Package.where(:name => 'CommonDataTypes').first.child_packages
root_packages.each{|p| build_standard_package p, standard, asn1_packages}

ct
build_asn1_packages(asn1_packages.uniq, standard)

ChangeTracker.commit if ChangeTracker.started?