def dim_class_properties
  {
    "ActivateOperation" => [
    ],
    "Alert" => [
      "Alert-Condition",
      "Limit-Specification",
      "Vmo-Reference"
    ],
    "AlertMonitor" => [
      "Device-Alert-Condition",
      "Device-P-Alarm-List",
      "Device-T-Alarm-List",
      "Device-Sup-Alarm-List",
      "Limit-Spec-List",
      "Suspension-Period"
    ],
    "AlertScanner" => [
      "Reporting-Interval"
    ],
    "AlertStatus" => [
      "Alert-Capab-List",
      "Tech-Alert-List",
      "Physio-Alert-List",
      "Limit-Spec-List"
    ],
    "Ancillary" => [
    ],
    "BCC" => [
    ],
    "Battery" => [
      "Handle",
      "Battery-Status",
      "Production-Specification",
      "Capacity-Remaining",
      "Capacity-Full-Charge",
      "Capacity-Specified",
      "Remaining-Battery-Time",
      "Voltage",
      "Voltage-Specified",
      "Current",
      "Battery-Temperature",
      "Charge-Cycles"
    ],
    "CfgScanner" => [
      "Scan-List",
      "Confirm-Mode",
      "Confirm-Timeout",
      "Transmit-Window",
      "Scan-Config-Limit"
    ],
    "Channel" => [
      "Channel-Id",
      "Channel-Status",
      "Physical-Channel-No",
      "Logical-Channel-No",
      "Parameter-Group",
      "Measurement-Principle",
      "Color"
    ],
    "Clock" => [
      "Handle",
      "Time-Support",
      "Date-Time-Status",
      "Date-and-Time",
      "ISO-Date-and-Time",
      "Relative-Time",
      "HiRes-Relative-Time",
      "Ext-Time-Stamp-List",
      "Absolute-Relative-Sync",
      "Time-Zone",
      "Daylight-Savings-Transition",
      "Cumulative-Leap-Seconds",
      "Next-Leap-Seconds"
    ],
    "CommunicationController" => [
      "Handle",
      "Capability",
      "CC-Type",
      "Number-Of-Difs",
      "This-Connection-Dif-Index",
      "Cc-Ext-Mgmt-Proto-Id"
    ],
    "ComplexMetric" => [
      "Cmplx-Metric-Info",
      "Cmplx-Observed-Value",
      "Cmplx-Dyn-Attr",
      "Cmplx-Static-Attr",
      "Cmplx-Recursion-Depth"
    ],
    "ContextScanner" => [
      "Context-Mode"
    ],
    "DCC" => [
    ],
    "DeviceInterface" => [
    ],
    "DeviceInterfaceMibElement" => [
      "Dif-Id",
      "Port-State",
      "Port-Number",
      "Dif-Type",
      "Active-Profile",
      "Supported-Profiles",
      "MTU",
      "Link-Speed",
      "Mib-Element-List"
    ],
    "DistributionSampleArray" => [
      "Absolute-Time-Stamp",	
      "Relative-Time-Stamp",
      "HiRes-Time-Stamp",
      "Distribution-Range-Specification",
      "x-Unit-Code",
      "x-Unit-Label-String",
      "Dsa-Marker-List"
    ],
    "Enumeration" => [
      "Enum-Observed-Value",
      "Compound-Enum-Observed-Value",
      "Absolute-Time-Stamp",
      "Relative-Time-Stamp",
      "HiRes-Time-Stamp",
      "Enum-Measure-Range",
      "Enum-Measure-Range-Bit-String",
      "Enum-Measure-Range-Labels"
    ],
    "EpiCfgScanner" => [
    ],
    "EventLog" => [
      "Type",
      "Event-Log-Entry-List",
      "Event-Log-Info"
    ],
    "FastPeriCfgScanner" => [
    ],
    "GeneralCommunicationStatisticsMibElement" => [
      "Packets-In",
      "Packets-Out",
      "Octets-In",
      "Octets-Out",
      "Discarded-Packets-In",
      "Discarded-Packets-Out",
      "Unknown-Protocol-Packets-In",
      "Queue-Len-In",
      "Queue-Len-Out",
      "Dif-Admin-Status",
      "Dif-Oper-Status",
      "Dif-Last-Change",
      "Errors-In",
      "Errors-Out",
      "Generic-Mode",
      "Average-Speed",
      "Maximum-Speed"
    ],
    "LimitAlertOperation" => [
      "Alert-Op-Capability",
      "Alert-Op-State",
      "Current-Limits",
      "Alert-Op-Text-String",
      "Set-Value-Range",
      "Unit-Code",
      "Metric-Id"
    ],
    "Log" => [
      "Handle",
      "Max-Log-Entries",
      "Current-Log-Entries",
      "Log-Change-Count"
    ],
    "MDS" => [
      "Mds-Status",
      "Bed-Label",
      "Soft-Id",
      "Operating-Mode",
      "Application-Area",
      "Patient-Type",
      "Date-and-Time",
      "Relative-Time",
      "HiRes-Relative-Time",
      "Power-Status",
      "Altitude",
      "Battery-Level",
      "Remaining-Battery-Time",
      "Line-Frequency",
      "Association-Invoke-Id",
      "Locale"
    ],
    "Metric" => [
      "Metric-Specification",
      "Max-Delay-Time",
      "Metric-Status",
      "Measurement-Status",
      "Metric-Id-Partition",
      "Metric-Id",
      "Metric-Id-Ext",
      "Unit-Code",
      "Unit-LabelString",
      "Vmo-Source-List",
      "Metric-Source-List",
      "Msmt-Site-List",
      "Body-Site-List",
      "Metric-Calibration",
      "Color",
      "Measure-Mode",
      "Measure-Period",
      "Averaging-Period",
      "Start-Time",
      "Stop-Time",
      "Metric-Info-LabelString",
      "Substance",
      "Substance-LabelString"
    ],
    "MibElement" => [
      "Mib-Ext-Oid"
    ],
    "MultiPatientArchive" => [
      "Handle",
      "System-Id",
      "Location",
      "Study-Name",
      "Version"
    ],
    "Numeric" => [
      "Nu-Observed-Value",
      "Compound-Nu-Observed-Value",
      "Absolute-Time-Stamp",
      "Relative-Time-Stamp",
      "HiRes-Time-Stamp",
      "Nu-Measure-Range",
      "Nu-Physiological-Range",
      "Nu-Measure-Resolution",
      "Display-Resolution",
      "Accuracy"
    ],
    "OperatingScanner" => [
    ],
    "Operation" => [
      "Instance-Number",
      "Operation-Spec",
      "Operation-Text-Strings",
      "Operation-Text-Strings-Dyn",
      "Vmo-Reference",
      "Operational-State"
    ],
    "PM-Segment" => [
      "Instance-Number",
      "Metric-Id",
      "Metric-Id-Ext",
      "Vmo-Global-Reference",
      "Segment-Start-Abs-Time",
      "Segment-End-Abs-Time",
      "Segment-Usage-Count",
      "Segment-Data",
      "Context Attributes"
    ],
    "PM-Store" => [
      "Metric-Class",
      "Store-Sample-Algorithm",
      "Storage-Format",
      "Store-Capacity-Count",
      "Store-Usage-Count",
      "Sample-Period",
      "Number-Of-Segments"
    ],
    "PatientArchive" => [
      "Handle",
      "System-Id",
      "System-Name",
      "Processing-History",
      "Protection"
    ],
    "PatientDemographics" => [
      "Handle",
      "Pat-Demo-State",
      "Patient-Id",
      "Name",
      "Given-Name",
      "Family-Name",
      "Middle-Name",
      "Birth-Name",
      "Title-Name",
      "Sex",
      "Race",
      "Patient-Type",
      "Date-Of-Birth",
      "Patient-Gen-Info",
      "Patient-Age",
      "Gestational-Age",
      "Patient-Height",
      "Patient-Weight",
      "Patient-Birth-Length",
      "Patient-Birth-Weight",
      "Mother-Patient-Id",
      "Mother-Name",
      "Patient-Head-Circumference",
      "Patient-Bsa",
      "Patient-Lbm",
      "Bed-Id",
      "Diagnostic-Info",
      "Diagnostic-Codes",
      "Admitting-Physician",
      "Attending-Physician",
      "Date-Of-Procedure",
      "Procedure-Description",
      "Procedure-Codes",
      "Anaesthetist",
      "Surgeon"
    ],
    "PeriCfgScanner" => [
      "Scan-Extensibility",
      "Reporting-Interval"
    ],
    "Physician" => [
      "Handle",
      "Physician-Id",
      "Authorization-Level",
      "Name",
      "Given-Name",
      "Family-Name",
      "Middle-Name",
      "Title-Name"
    ],
    "RealTimeSampleArray" => [
      "Sample-Period",
      "Sweep-Speed",
      "Average-Reporting-Delay",
      "Sample-Time-Sync",
      "HiRes-Sample-Time-Sync"
    ],
    "SCO" => [
      "Sco-Capability",
      "Sco-Help-Text-String",
      "Vmo-Reference",
      "Activity-Indicator",
      "Lock-State",
      "Invoke-Cookie"
    ],
    "SampleArray" => [
      "Sa-Observed-Value",
      "Compound-Sa-Observed-Value",
      "Sa-Specification",
      "Compression",
      "Scale-and-Range-Specification",
      "Sa-Physiological-Range",
      "Visual-Grid",
      "Sa-Calibration-Data",
      "Filter-Specification",
      "Filter-Label-String",
      "Sa-Signal-Frequency",
      "Sa-Measure-Resolution",
      "Sa-Marker-List"
    ],
    "Scanner" => [
      "Handle",
      "Instance-Number",
      "Operational-State"
    ],
    "SelectItemOperation" => [
      "Selected-Item-Index",
      "Nom-Partition",
      "Select-List"
    ],
    "SessionArchive" => [
      "Handle",
      "S-Archive-Id",
      "S-Archive-Name",
      "S-Archive-Comments",
      "Start-Time",
      "Stop-Time",
      "Protection"
    ],
    "SessionNotes" => [
      "Handle",
      "Sn-Id",
      "Sn-Name",
      "Sn-Comments",
      "Start-Time",
      "Stop-Time",
      "Findings",
      "Diagnostic-Codes",
      "Diagnosis-Description",
      "Procedure-Code",
      "Procedure-Description",
      "Protection"
    ],
    "SessionTest" => [
      "Handle",
      "St-Archive-Id",
      "St-Archive-Name",
      "St-Archive-Comments",
      "Start-Time",
      "Stop-Time",
      "Protection"
    ],
    "SetRangeOperation" => [
      "Current-Range",
      "Range-Op-Text",
      "Set-Value-Range",
      "Step-Width",
      "Unit-Code"
    ],
    "SetStringOperation" => [
      "Current-String",
      "Set-String-Spec"
    ],
    "SetValueOperation" => [
      "Current-Value",
      "Set-Value-Range",
      "Step-Width",
      "Unit-Code"
    ],
    "TimeSampleArray" => [
      "Absolute-Time-Stamp",
      "Relative-Time-Stamp",
      "HiRes-Time-Stamp",
      "Sample-Period",
      "Sweep-Speed",
      "Tsa-Marker-List"
    ],
    "ToggleFlagOperation" => [
      "Toggle-State",
      "Toggle-Label-Strings"
    ],
    "Top" => [
      "Class",
      "Name-Binding"
    ],
    "UcfgScanner" => [
      "Confirm-Mode",
      "Confirm-Timeout",
      "Transmit-Window"
    ],
    "VMD" => [
      "VMD-Status",
      "VMD-Model",
      "Instance-Number",
      "Production-Specification",
      "Compatibility-Id",
      "Parameter-Group",
      "Position",
      "Operating-Hours",
      "Operation-Cycles",
      "Measurement-Principle",
      "Locale"
    ],
    "VMO" => [
      "Type",
      "Handle",
      "Label-String",
      "Ext-Obj-Relations"
    ],
   "VMS" => [
      "Handle",
      "System-Type",
      "System-Model",
      "System-Id",
      "Compatibility-Id",
      "Nomenclature-Version",
      "System-Capability",
      "System-Specification",
      "Production-Specification",
      "Ext-Obj-Relations"
    ]
  }
end
dim = MetaInfo::Package.where(:name => "DIM").first
dims = dim.child_packages.collect(&:classifiers).flatten
dims.each do |klass|
  to_order, remainder = klass.properties.partition { |prop| prop.is_asn1 }
  dcps = dim_class_properties[klass.name]
  unless dcps
    next if to_order.empty?
    raise "ARGGGHHH"
  end
  # puts klass.name
  # puts_green dcp.inspect
  # puts_yellow to_order.collect(&:name).inspect
  # puts
  new_order = []
  unfound   = []
  dcps.each do |dcp|
    fp = to_order.find {|prop| prop.name.sub(/_dim$/, '').downcase == dcp.downcase }
    if fp
      new_order << fp
    else
      unfound << dcp
    end
  end

  # check unfound
  if unfound.any?
    puts klass.name
    puts "unfound: #{unfound.inspect}"
  end
  # check all to_order are accounted for
  unused = to_order - new_order
  if unused.any?
    puts klass.name unless unfound.any?
    puts "unused: #{unused.collect(&:name).inspect}"
  end
  
  replacement = new_order + unused + remainder
  if replacement.count == klass.properties_count
    # ChangeTracker.start
    # klass.properties = replacement
    # klass.save
    # ChangeTracker.commit
  else
    puts_red klass.name
    puts to_order.collect(&:name).inspect
    puts_cyan dcps.inspect
  end
  
end;


