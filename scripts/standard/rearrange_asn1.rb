# dim = MetaInfo::Package.where(:name => 'DIM').first
# pkgs = dim.child_packages.select do |pkg|
#   pkg.child_packages.any? &&
#   pkg.classifiers.select{|cl| (cl.is_asn1 == false)}.count > 0
# end;
# pkgs.each do |pkg|
#   asn1_names = pkg.child_packages.first.classifiers.collect{|cl| cl.name.inspect}.sort
#   puts asn1_names
#   pkg.classifiers.each{|cl| puts "\"#{cl.name}\" => [],"}
#   puts
# end;

data = {
  "Channel" => ["ChannelStatus"],
  "ComplexMetric" => ["CmplxMetricInfo", "CmplxElemInfoList", "CmplxElemInfo", "CmplxObsValue", "CmplxFlags", "CmplxObsElemList", "CmplxObsElem", "CmplxObsElemFlags", "CmplxDynAttr", "CmplxDynAttrElemList", "CmplxDynAttrElem", "CmplxStaticAttr", "CmplxStaticAttrElemList", "CmplxStaticAttrElem"],
  "DistributionSampleArray" => ["DsaRangeSpec", "MarkerListIndex", "MarkerEntryIndex"],
  "Enumeration" => ["EnumObsValue", "EnumVal", "EnumRecordMetric", "EnumRecordOo", "EnumObsValueCmp", "EnumMsmtRange", "EnumMsmtRangeLabels", "EnumMsmtRangeLabel"],
  "Metric" => ["MetricStatus", "MetricSpec", "MetricStructure", "MetricComplexity", "MetricAccess", "MetricCategory", "MetricRelevance", "MetricCalibration", "MetricCalEntry", "MetricCalType", "MetricCalState", "SiteList", "SiteListExt", "MetricSourceList", "VmoSourceList", "VmoSourceEntry", "MeasurementStatus", "AbsoluteRange",  "MetricMeasure"],
  "Numeric" => ["NuObsValue", "NuObsValueCmp", "DispResolution"],
  "PM-Segment" => ["SegDataGen", "SegDataNuOpt", "SegDataRtsaOpt"],
  "PM-Store" => ["StorageFormat", "StoSampleAlg", "SegmSelection", "SegmIdList", "AbsTimeRange", "SegmentAttrList", "SegmentAttr", "SegmentInfoList", "SegmentInfo"],
  "RealTimeSampleArray" => [],
  "SampleArray" => ["SaObsValue", "SaObsValueCmp", "SaSpec", "SampleType", "SaFlags", "SaFilterSpec", "SaFilterEntry", "SaFilterType", "ScaleRangeSpec8", "ScaleRangeSpec16", "ScaleRangeSpec32", "SaVisualGrid8", "SaGridEntry8", "SaVisualGrid16", "SaGridEntry16", "SaVisualGrid32", "SaGridEntry32", "SaCalData8", "SaCalData16", "SaCalData32", "SaCalDataType", "SaSignalFrequency", "ScaledRange8", "ScaledRange16", "ScaledRange32", "MarkerListSaVal8", "MarkerEntrySaVal8", "MarkerListSaVal16", "MarkerEntrySaVal16", "MarkerListSaVal32", "MarkerEntrySaVal32"],
  "TimeSampleArray" => ["MarkerListRelTim", "MarkerEntryRelTim"],
  "VMD" => ["VMDStatus", "MsmtPrinciple"],

  "Alert" => ["AlertCondition", "AlertControls", "AlertFlags", "AlertType", "LimitSpecEntry"],
  "AlertStatus" => ["AlertList", "AlertEntry", "AlertCapabList", "AlertCapabEntry", "AlertRepFlags", "LimitSpecList"],
  "AlertMonitor" => ["DevAlertCondition", "AlertState", "AlStatChgCnt", "DevAlarmList", "DevAlarmEntry"],

  "Battery" => ["BatteryStatus", "BatMeasure"],
  "Clock" => ["TimeSupport", "TimeCapability", "TimeProtocolIdList", "TimeProtocolId", "TimeStampId", "ExtTimeStamp", "ExtTimeStampList", "DateTimeStatus", "DateTimeUsage", "AbsoluteTimeISO", "SNTPTimeStamp", "AbsoluteRelativeTimeSync", "UTCTimeZone", "DaylightSavingsTransition", "LeapSecondsTransition", "SetTimeInvoke", "SetTimeZoneInvoke", "SetLeapSecondsInvoke", "ClockStatusUpdateInfo"],
  "EventLog" => ["EventLogEntryList", "EventLogEntry", "EventLogInfo", "GetEventLogEntryInvoke", "GetEventLogEntryResult"],
  "Log" => ["ClearLogRangeInvoke", "ClearLogRangeResult", "ClearLogOptions", "ClearLogResult"],
  "MDS" => ["MDSStatus", "ApplicationArea", "PowerStatus", "LineFrequency", "MdsSetStateInvoke", "MdsSetStateResult", "MdsErrorInfo", "MdsCreateInfo", "MdsAttributeChangeInfo"],
  "MultipleBedMDS" => [],
  "SingleBedMDS" => [],
  "VMS" => ["SystemModel", "SystemCapability", "SystemSpec", "SystemSpecEntry", "ProductionSpec", "ProdSpecEntry", "ProdSpecType", "NomenclatureVersion", "NomenclatureMajorVersion"],

  "ActivateOperation" => [],
  "LimitAlertOperation" => ["AlOpCapab", "CurLimAlStat", "CurLimAlVal", "AlertOpTextString"],
  "Operation" => ["OperSpec", "OperTextStrings", "OpOptions", "OpLevel", "OpGrouping"],
  "SCO" => ["ScoActivityIndicator", "ScoCapability", "OperationInvoke", "OpInvokeList", "OpInvokeElement", "OpModType", "OperationInvokeResult", "OpInvResult", "CtxtHelpRequest", "CtxtHelpResult", "CtxtHelp", "ScoOperReqSpec", "ScoOperInvokeError", "OpErrorType", "InstNumberList"],
  "SelectItemOperation" => ["SelectList", "OID-TypeList", "FLOAT-TypeList", "SelectUValueEntryList", "SelectUValueEntry", "OCTET STRING-List"],
  "SetRangeOperation" => ["CurrentRange", "RangeOpText"],
  "SetStringOperation" => ["SetStringSpec", "SetStrOpt"],
  "SetValueOperation" => ["OpSetValueRange", "OpValStepWidth", "StepWidthEntry"],
  "ToggleFlagOperation" => ["ToggleState", "ToggleLabelStrings"],

  "AlertScanner" => [],
  "CfgScanner" => ["ScanList", "ScanEntry", "ConfirmMode", "ScanConfigLimit"],
  "ContextScanner" => ["ContextMode", "ObjCreateInfo", "CreateEntryList", "CreateEntry", "CreatedObjectList", "CreatedObject", "ObjDeleteInfo", "ManagedObjectIdList"],
  "EpiCfgScanner" => [],
  "FastPeriCfgScanner" => ["FastScanReportInfo", "SingleCtxtFastScanList", "SingleCtxtFastScan", "RtsaObservationScanList", "RtsaObservationScan"],
  "OperatingScanner" => ["OpElemAttr", "OpElemAttrList", "OpElemList", "OpElem", "OpCreateInfo", "OpCreateEntryList", "OpCreateEntry", "OpDeleteInfo", "OpDeleteEntryList", "OpDeleteEntry", "OpAttributeInfo", "SingleCtxtOperScanList", "SingleCtxtOperScan", "OpAttributeScanList", "OpAttributeScan"],
  "PeriCfgScanner" => ["ScanExtend"],
  "Scanner" => ["RefreshObjList", "RefreshObjEntry", "ScanReportInfo", "SingleCtxtScanList", "SingleCtxtScan", "ObservationScanList", "ObservationScan"],
  "UcfgScanner" => [],
  
  "BCC" => [],
  "CommunicationController" => ["CcCapability", "CC-Oid", "CcExtMgmtProto", "GetMibDataRequest", "MibIdList", "GetMibDataResult", "MibDataList", "MibDataEntry"],
  "DCC" => [],
  "DeviceInterface" => [],
  "DeviceInterfaceMibElement" => ["SupportedProfileList", "MibElementList", "DifMibPortState"],
  "GeneralCommunicationStatisticsMibElement" => ["MibCcGauge", "MibCcCounter", "MibCcCommMode"],
  "MibElement" => [],

  "MultiPatientArchive" => [],
  "PatientArchive" => ["ArchiveProtection"], 
  "Physician" => ["Authorization"],
  "SessionArchive" => [],
  "SessionNotes" => ["ExtNomenRefList"],
  "SessionTest" => [],

  "PatientDemographics" => ["PatDemoState", "PatMeasure", "PatientSex", "PatientType", "PatientRace", "AdmitPatInfo"]
  };

data.each do |klass, asn1s|
  clawz     = Standard::Clause.where(:title => "Class #{klass}").first
  datatypes = asn1s.each do |asn1|
    code = Standard::Code.where(:title => "Data Type: #{asn1}").first
    ChangeTracker.start
    clawz.add_content(code); clawz.save
    ChangeTracker.commit
  end
end;

data.each do |klass, asn1s|
  clawz = Standard::Clause.where(:title => "Class #{klass}").first
  noti  = clawz.content.find{ |c| c.is_a?(Standard::Clause) && c.title =~ /Notifications/ }
  beha  = clawz.content.find{ |c| c.is_a?(Standard::Clause) && c.title =~ /Behavior/ }
  ChangeTracker.start
  clawz.move_content(noti,clawz.content.size - 1)
  clawz.save
  ChangeTracker.commit
  # ChangeTracker.start
  # clawz.move_content(beha,clawz.content.size - 1)
  # clawz.save
  # ChangeTracker.commit
  end
end;
