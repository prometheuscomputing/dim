# load File.expand_path("~/projects/json_graph/lib/json_graph.rb");
# GraphState.clear
# GraphState.clear_config
# GraphState.output_type = :ruby
# st = Standard::IEEEStandard.first;
# puts JSON_Graph.pretty_generate(st);

load File.expand_path("~/projects/dim/lib/dim/standard/dsl/standard_to_dsl.rb")

standard = Standard::IEEEStandard.first
fn = File.expand_path("~/projects/dim/lib/dim/standard/dsl/ieee_standard_#{standard.id}.dsl.rb")
dsl = standard.to_dsl;
File.open(fn, 'w+') {|f| f.puts dsl};
# puts dsl