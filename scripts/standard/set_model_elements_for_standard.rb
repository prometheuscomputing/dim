ChangeTracker.start
AGTable = Standard::RenderingType.where(:value => 'attribute groups table').first || Standard::RenderingType.create(:value => 'attribute groups table')
AttrsTable = Standard::RenderingType.where(:value => 'attributes table').first || Standard::RenderingType.create(:value => 'attributes table')
BEHAV = Standard::RenderingType.where(:value => 'behaviors').first || Standard::RenderingType.create(:value => 'behaviors')
NOTIF = Standard::RenderingType.where(:value => 'notifications').first || Standard::RenderingType.create(:value => 'notifications')
ChangeTracker.commit
def recursively_set_model_element(obj, me)
  ChangeTracker.start
  obj.model_element = me
  ChangeTracker.commit
  if obj.is_a?(Standard::Clause)
    obj.content.each_with_index do |item, index|
      recursively_set_model_element(item, me)
    end
    if obj.title =~ /Behaviors/
      ChangeTracker.start; obj&.content&.first.render_as = BEHAV; ChangeTracker.commit
    end 
    if obj.title =~ /Notifications/
      ChangeTracker.start; obj&.content&.first.render_as = NOTIF; ChangeTracker.commit
    end 
  end
  if obj.is_a?(Standard::Table)
    if obj.title =~ /Attribute Groups Table/
      ChangeTracker.start; obj.render_as = AGTable; ChangeTracker.commit
    end
    if obj.title =~ /Attributes Table/
      ChangeTracker.start; obj.render_as = AttrsTable; ChangeTracker.commit
    end
  end
end
dclasses = Standard::Clause.where(Sequel.ilike(:title, 'class %')).all;
dclasses.each do |dc|
  classname = dc.title.slice(/(?<=Class ).*/)
  modul = MetaInfo::Class.where(:name => classname).first
  unless modul
    puts "WARNING, no model for #{dc.title}"
    next
  end
  recursively_set_model_element(dc, modul)
  def_text = dc.content.first
  if def_text.is_a?(Standard::Text)
    ChangeTracker.start
    def_text.render_as = 'class definition'
    def_text.save
    ChangeTracker.commit
  end
end;

# ChangeTracker.start
# asn1_enum = Standard::RenderingType.where(:value => 'ASN.1').first || Standard::RenderingType.create(:value => 'ASN.1')
# ChangeTracker.commit;
# ChangeTracker.start
# Standard::Code.all.each { |code| code.render_as = asn1_enum; code.save };
# ChangeTracker.commit;

