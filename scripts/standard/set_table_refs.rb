xrefs = {}
Standard::Table.each do |table|
  me = table.model_element
  me ||= table.containers.first.model_element
  me ||= table.containers&.first&.containers&.first&.model_element
  unless me
    puts "DANGIT #{table.id}"
    next
  end
  klass = me.name
  puts table.id unless table.title
  fixed_title = table.title.gsub(klass, '')
  fixed_title = fixed_title.slice(0,1).capitalize + fixed_title.slice(1..-1)
  val = (klass + fixed_title).gsub(/\s|[Tt]able/, '').gsub('events', 'Events').gsub('methods', 'Methods')
  xrefs[val] ||= 0
  xrefs[val] = xrefs[val].succ
  val += xrefs[val]
  # puts val
  ChangeTracker.start; table.xref = val; table.save; ChangeTracker.commit
end

Standard::Clause.each do |clawz|
  clawz.content.each_with_index do |item, index|
    if item.is_a?(Standard::Table)
      text = clawz.content[index - 1]
      words = text.content_content
      if words =~ /Table.*_Number/
        puts_green words#.gsub(/Table.*_Number/, "\\xref_#{item.xref}\\")
        ChangeTracker.start
        rt = text.content.clone
        rt.content = words.gsub(/Table.*_Number/, "\\xref_#{item.xref}\\")
        text.content = rt
        text.save
        ChangeTracker.commit
      else
        puts_yellow words
      end
    end
  end
end
