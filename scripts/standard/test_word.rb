def wordtest(arg = nil)
  load relative ('../../lib/dim/word_generation.rb')
  s = arg ? Standard::IEEEStandard[arg] : Standard::IEEEStandard.last
  s.build
  nil
end
