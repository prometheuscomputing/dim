#!/usr/bin/env ruby

require 'Foundation/load_path_management'
require 'sequel_specific_associations'
require 'sequel_change_tracker'
require 'dim_generated'
require 'dim_generated/model/driver'
require 'yaml'
require 'fileutils'

module DIM_MigrationMetadata
  extend self
  dir = File.expand_path "~/projects/dim/migration/"
  file_name = "dim_2_3_6"
  @marshal_file = File.join dir, (file_name + ".bin")
  @yaml_file = File.join dir, (file_name + ".yaml")
  
  def store_meta_data
    h = {}
    Sequel::Model.children.each do |m|
      h[m.table_name] = {:class => m.to_s}
      h[m.table_name] = h[m.table_name].merge(m.properties) if m.respond_to?(:properties)
    end
    FileUtils::mkdir_p @dir
    File.open(@yaml_file, 'w') {|f| f.write(YAML.dump(h)) }
    File.open(@marshal_file, 'w') {|f| f.write(Marshal.dump(h)) }
  end
  def retrieve_yaml
    YAML.load (File.read @yaml_file)
  end
  def retrieve_bin
    Marshal.load (File.read @marshal_file)
  end
end

# only happens if this file is invoked from CLI
if caller.count < 1
  DIM_MigrationMetadata.store_meta_data
end
@old_properties = DIM_MigrationMetadata.retrieve_yaml