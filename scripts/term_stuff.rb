require 'json'
json = File.read(File.expand_path("~/projects/dim_reference/rtmms_raw.json"));nil
begin
  terms = JSON.parse(json);nil
rescue Exception => e
  puts e.message
  puts e.backtrace
end

keyz = terms.collect{|t| t.keys}.flatten.uniq
# ["referenceId", "updateDate", "status", "sources", "code10", "cfcode10", "partition", "systematicName", "commonTerm", "acronym", "termDescription", "type", "units", "enums"]
terms_without_cfcode10 = terms.reject{|t| t["cfcode10"]}
terms_without_code10 = terms.reject{|t| t["code10"]}
terms_without_any_code = terms.reject{|t| t["code10"] || t["cfcode10"]}