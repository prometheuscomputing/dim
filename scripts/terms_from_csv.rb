require 'csv'
source_dir = File.expand_path("~/projects/dim_reference/RTMMS_CSV_as_of_May2016")
units_csv = File.join(source_dir, 'units.csv')
rosetta_csv = File.join(source_dir, 'rosetta_terms.csv')
enums_csv = File.join(source_dir, 'enums.csv')
x73_csv = File.join(source_dir, 'x73nomenclature.csv')
hrtm_csv = File.join(source_dir, 'harmonizedRosetta.csv')

x73_terms = []
CSV.foreach(x73_csv) {|row| x73_terms << row[1]}
x73_terms.shift # get rid of header line
x73_terms.compact!
x73_terms.uniq!

rosetta_terms = []
CSV.foreach(rosetta_csv) {|row| rosetta_terms << row[1]}
rosetta_terms.shift # get rid of header line
rosetta_terms.compact!
rosetta_terms.uniq!

hrtm_terms = []
CSV.foreach(hrtm_csv) {|row| hrtm_terms << row[1]}
hrtm_terms.shift # get rid of header line
hrtm_terms.compact!
hrtm_terms.uniq!
hrtm_terms.count

unit_terms = []
unit_groups = []
CSV.foreach(units_csv) {|row| unit_terms << row[6]; unit_groups << row[0]}
unit_terms.shift # get rid of header line
unit_groups.shift # get rid of header line
unit_terms.compact!
unit_terms.uniq!
unit_terms.count
unit_groups.compact!
unit_groups.uniq!

enum_terms = []
enum_groups = []
token_terms = []
CSV.foreach(enums_csv) {|row| enum_terms << row[2]; enum_groups << row[0]; token_terms << row[1]}
enum_terms.shift # get rid of header line
enum_groups.shift # get rid of header line
token_terms.shift # get rid of header line
enum_terms.uniq!
enum_terms.count
enum_groups.compact!
enum_groups.uniq!
token_terms.compact!
token_terms.uniq!

hrtm_x = hrtm_terms - rosetta_terms
x73_minus_rosetta = x73_terms - rosetta_terms
rosetta_minus_x73 = rosetta_terms - x73_terms

unit_terms.count
units_in_rosetta = unit_terms & rosetta_terms