require 'common/color_output'

def models_for_tables
  if @models_for_tables
    @models_for_tables
  else
    @models_for_tables = {}
    DB.tables.each do |t|
      model = Sequel::Model.children.find do |child|
        next unless child.table_name
        t == child.table_name
      end
      @models_for_tables[t] = model if model
    end
    @models_for_tables
  end
end

def model_for_table(table_symbol)
  models_for_tables[table_symbol]
end

def table_for_model(model)
  entry = models_for_tables.find { |_,v| v.to_s == model.to_s}
  entry.first if entry
end

# Can take regexps, symbols, class name strings, or classes
def convert_to_regexp(val, opts = {})
  case val
  when Regexp
    val
  when String
    /^#{val.delete(":")}$/
  when Symbol
    /^#{val}$/
  when Class
    if val < Sequel::Model
      /^#{table_for_model(val)}$/
    else
      nil
    end        
  else
    nil
  end
end

def get_regexps(inputs, opts = {})
  inputs = [inputs] unless inputs.is_a?(Array)
  inputs.collect do |val|
    raise "Classes are not an allowed exclusion type!" if val.is_a?(Class) && !opts[:classes]
    convert_to_regexp(val, opts)
  end.compact
end

def all_db_columns_of_type(db_types, opts = {})
  table_exclusions  = get_regexps(opts[:table_exclusions], :classes => true)
  column_exclusions = get_regexps(opts[:column_exclusions])
  only_columns      = get_regexps(opts[:only_columns])
  types             = [db_types] unless db_types.is_a?(Array)
  column_data = Struct.new(:table, :column, :schema)
  answer = []
  DB.tables.each do |table|
    skip = false
    table_exclusions.each { |regex| skip = table.to_s =~ regex; break if skip }
    next if skip
    DB.schema(table).each do |name, col_schema|
      if only_columns.any?
        next unless only_columns.any? { |regex| name.to_s =~ regex }
      end
      skip = false
      column_exclusions.each { |regex| skip = name.to_s =~ regex; break if skip }
      next if skip
      answer << column_data.new(table, name, col_schema) if types.include?(col_schema[:db_type])
    end
  end
  answer
end

def all_db_varchars(opts = {})
  all_db_columns_of_type("varchar(255)", opts)
end

def all_db_varchars_matching(patterns, opts = {})
  all_db_varchars(opts.merge({:only_columns => patterns}))
end

# Not guaranteed to only get richtext content.  Will get any column ending in '_content'
def all_db_richtext_content(opts = {})
  all_db_varchars_matching("_content$", opts)
end

def highlight_record(r, col_data, string, opts = {})
  # col_text   = r[col_data.column]
  col_string = ":#{col_data.column.to_s}="
  regex      = opts[:case_insensitive] ? /#{string}/i : /#{string}/
  text       = r.inspect.gsub(regex) { |s| turn_green(s)}.gsub(col_string, turn_yellow(col_string))
  puts_cyan "#{model_for_table(col_data.table)} (table: #{col_data.table})"
  puts text
end

# find and show all records with strings that match
def examine_string(string, opts = {})
  remember = []
  all_db_varchars(opts).each do |col_data|
    matcher = opts[:case_insensitive] ? Sequel.ilike(col_data.column, "%#{string}%") : Sequel.like(col_data.column, "%#{string}%")
    records = DB[col_data.table].where(matcher).all
    records.each do |r|
      highlight_record(r, col_data, string, opts)
      if opts[:with_pause]
        print "<(q)uit|(r)emember> "
        case gets.chomp.downcase
        when 'q', 'quit'
          puts remember if remember.any?
          return
        when 'r'
          remember << "#{model_for_table(col_data.table)}[#{r[:id]}]"
        end
      end
      puts
    end
  end
  puts remember if remember.any?
end

# replace all strings
# TODO this could be extended to take a regex, array of strings, or array of regex for first param
def replace_string(string, replacement, opts = {})
  raise "Second argument must be a String for #replace_string" if replacement.is_a?(Hash) # Stop user from doing something bad and unintentional
  remember = []
  all_db_varchars(opts).each do |col_data|
    matcher = opts[:case_insensitive] ? Sequel.ilike(col_data.column, "%#{string}%") : Sequel.like(col_data.column, "%#{string}%")
    records = DB[col_data.table].where(matcher).all
    records.each do |r|
      highlight_record(r, col_data, string, opts) unless opts[:silent]
      old_val = r[col_data.column]
      new_val = old_val.gsub(string, replacement)
      unless opts[:silent]
        input = ''
        until (input.strip =~ /^(y|n|q|yes|no|quit)$/)
          print "Replace? <(y)es|(n)o|(r)emember|(q)uit> "
          input = gets.chomp.downcase
        end
        case input
        when "y", "yes"
          DB[col_data.table].where(:id => [r[:id]]).update(col_data.column => new_val)
          if opts[:verbose]
            puts_green DB[col_data.table].where(:id => [r[:id]]).first[col_data.column]
          end
        when "n", "no"
          next
        when "q", "quit"
          puts remember if remember.any?
          return
        when "r", "remember"
          remember << "#{model_for_table(col_data.table)}[#{r[:id]}]"
        end
      else
        DB[col_data.table].where(:id => [r[:id]]).update(col_data.column => new_val)
      end
    end
  end
  puts remember if remember.any?
end

# FIXME make DRY w/ #replace_string
# be careful!
def replace_by_regex(string, regex, replacement, opts)
  remember = []
  all_db_varchars(opts).each do |col_data|
    matcher = opts[:case_insensitive] ? Sequel.ilike(col_data.column, "%#{string}%") : Sequel.like(col_data.column, "%#{string}%")
    records = DB[col_data.table].where(matcher).all
    records.each do |r|
      old_val = r[col_data.column]
      next unless old_val =~ regex
      new_val = old_val.gsub(regex, replacement)
      highlight_record(r, col_data, string, opts) unless opts[:silent]
      # unless opts[:silent]
        input = ''
        until (input.strip =~ /^(y|n|q|yes|no|quit|r)$/)
          print "Replace? <(y)es|(n)o|(r)emember|(q)uit> "
          input = gets.chomp.downcase
        end
        case input
        when "y", "yes"
          DB[col_data.table].where(:id => [r[:id]]).update(col_data.column => new_val)
          if opts[:verbose]
            puts_green DB[col_data.table].where(:id => [r[:id]]).first[col_data.column]
          end
        when "n", "no"
          next
        when "q", "quit"
          puts remember if remember.any?
          return
        when "r", "remember"
          remember << "#{model_for_table(col_data.table)}[#{r[:id]}]"
        end
      # else
      #   DB[col_data.table].where(:id => [r[:id]]).update(col_data.column => new_val)
      # end
    end
  end
  puts remember if remember.any?
end


default_opts = {
  :table_exclusions  => [/_deleted/, /^ct_/],
  :column_exclusions => [/_class$/, /^ct_/]
}
# EXAMPLES
# Show all of the richtext columns and which table they occur in
# pp all_db_richtext_content(default_opts).collect{|c| [c.column, c.table]}

# The names of all of the richtext columns (may occur in multiple tables)
# pp all_db_richtext_content(default_opts).collect{|c| c.column}.uniq.sort

# examine_string("PMStore", :with_pause => true)
# examine_string("PMStore", :case_insensitive => true)
myopts = {
  :table_exclusions  => [/_deleted/, /^ct_/, MetaInfo::Property],
  :column_exclusions => [/_class$/, /^ct_/, /_attribute_list$/],
  :only_columns => [:title],
  :with_pause => true,
  :verbose => true
}

# examine_string("Class", myopts)

# replace_string("Complex Metric", "ComplexMetric", myopts)
replace_by_regex("Package", /(Package|package) (.*)/m, '\2 package', myopts)

# g = "PmStoreCapab".gsub(/store/i) { |s| turn_green(s) }
# puts g
