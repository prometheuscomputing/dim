ChangeTracker.cancel
ChangeTracker.start
klasses = MyDevice::INSTANTIABLE_DIM_CLASSES
klasses.each do |klass|
  instances = klass.all
  instances.each do |i|
    if i.respond_to?(:type)
      if t = i.type
        t.code_term = i.type_term
        t.save
      end
    elsif i.respond_to?(:system_type)
      if t = i.system_type
        t.code_term = i.system_type_term
        t.save
      end
    end
  end
end
ChangeTracker.commit