def create_ucums filepath
  strings = []
  File.open(filepath) {|f| f.each_line{|l| strings << l.strip}}
  # puts ucums_strings.inspect
  no_unit = []
  strings.each do |s|
    ChangeTracker.commit {
      ucums_string, unit_string = s.split(',')
      ucums = []
      ucums_string.split(/\s+/).each do |str|
        uu = Nomenclature::UCUMUnit.where(:code => str.strip).first || Nomenclature::UCUMUnit.create(:code => str, :version => 'From RTMMS on May 5, 2016')
        ucums << uu
      end
      if ucums.any?
        unit = Nomenclature::Unit.where(:reference_id => unit_string).first
        if unit
          ucums.each{|uu| unit.ucum_units_add(uu) unless unit.ucum_units.include?(uu); uu.save}
          unit.save
        else
          no_unit << unit_string
        end
      end
    }
  end
  puts no_unit.inspect
  puts no_unit.count
end
path = relative "ucums2units.csv"
if File.exists? path
  create_ucums path
else
  puts "could not find #{path}"
end