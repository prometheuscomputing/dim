DIM::Medical::Metric.children.each do |klass|
  klass.all.each do |metric|
    if term = metric.type_term
      if metric.respond_to?(:allowed_enumerations) && term.respond_to?(:collected_literals)
        ChangeTracker.commit {metric.remove_all_allowed_enumerations; metric.save}
        if metric.allowed_enumerations.empty?
          ChangeTracker.start unless ChangeTracker.started?
          metric.allowed_enumerations = term.collected_literals
          metric.save
          ChangeTracker.commit
        end
      end
      if metric.respond_to?(:allowed_units) && term.respond_to?(:collected_units)
        ChangeTracker.commit {metric.remove_all_allowed_units; metric.save}
        if metric.allowed_units.empty?
          ChangeTracker.start unless ChangeTracker.started?
          metric.allowed_units = term.collected_units
          metric.save
          ChangeTracker.commit
        end
      end
    end
  end
end