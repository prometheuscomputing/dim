covered_packages 'DIM'

homepage_item :'DIM::ANY'
homepage_item :'DIM::ANY_COMPLEX'
homepage_item :'DIM::Composite_Multiple_Bed_MDS'
homepage_item :'DIM::Composite_Single_Bed_MDS'
homepage_item :'DIM::Hydra_MDS'
homepage_item :'DIM::Multipatient_Archive'
homepage_item :'DIM::Simple_MDS'
homepage_item :'DIM::Top'


administrativestate = ['locked', 'shuttingDown', 'unlocked']

alopcapab = ['auto-limit-sup', 'high-lim-on-off-sup', 'high-limit-sup', 'lim-on-off-sup', 'low-lim-on-off-sup', 'low-limit-sup']

alertcontrols = ['ac-alert-muted', 'ac-alert-off', 'ac-all-obj-al-off', 'ac-chan-off', 'ac-obj-off']

alertflags = ['audible-latching', 'derived', 'local-audible', 'record-inhibit', 'remote-audible', 'visual-latching']

alertstate = ['al-dev-in-demo-mode', 'al-dev-in-standby-mode', 'al-dev-in-test-mode', 'al-inhibited', 'al-latched', 'al-silenced-reset', 'al-suspended']

alerttype = ['hi-pri-p-al', 'hi-pri-t-al', 'low-pri-p-al', 'low-pri-t-al', 'med-pri-p-al', 'med-pri-t-al', 'no-alert']

applicationarea = ['area-intensive-care', 'area-operating-room', 'area-unspec']

batterystatus = ['batt-chargingFull', 'batt-chargingTrickle', 'batt-discharged', 'batt-discharging', 'batt-full', 'batt-malfunction', 'batt-needs-conditioning']

cccapability = ['cc-sup-ext-mgmt-protocol']

ccextmgmtproto = ['mgmt-proto-cmip', 'mgmt-proto-snmp-v1', 'mgmt-proto-snmp-v2', 'mgmt-proto-snmp-v3']

channelstatus = ['chan-hw-discon', 'chan-not-ready', 'chan-off', 'chan-standby', 'chan-transduc-discon']

charset = ['charset-gb-2312', 'charset-iso-10646-ucs-2', 'charset-iso-10646-ucs-4', 'charset-iso-2022-cn', 'charset-iso-2022-jp', 'charset-iso-2022-jp-2', 'charset-iso-2022-kr', 'charset-iso-8859-1', 'charset-iso-8859-10', 'charset-iso-8859-13', 'charset-iso-8859-14', 'charset-iso-8859-15', 'charset-iso-8859-2', 'charset-iso-8859-3', 'charset-iso-8859-4', 'charset-iso-8859-5', 'charset-iso-8859-6', 'charset-iso-8859-7', 'charset-iso-8859-8', 'charset-iso-8859-9', 'charset-jis-x0208', 'charset-ks-c-5601', 'charset-unspec']

clearlogoptions = ['log-clear-if-unchanged']

clearlogresult = ['log-change-counter-not-supported', 'log-changed-clear-error', 'log-range-cleared']

cmplxflags = ['cmplx-flag-reserved']

cmplxobselemflags = ['cm-obs-elem-flg-is-setting', 'cm-obs-elem-flg-mplex', 'cm-obs-elem-flg-msmt-noncontinuous', 'cm-obs-elem-flg-updt-episodic']

confirmmode = ['confirmed', 'unconfirmed']

contextmode = ['dynamic-mode', 'static-mode']

curlimalstat = ['lim-alert-off', 'lim-high-off', 'lim-low-off']

datetimeusage = ['dt-use-critical-use', 'dt-use-displayed', 'dt-use-operator-set', 'dt-use-remote-sync', 'dt-use-rtc-synced']

difmibportstate = ['difmib-port-associated', 'difmib-port-connected', 'difmib-port-enabled', 'difmib-port-failure']

eventloginfo = ['ev-log-binary-entries', 'ev-log-clear-range-sup', 'ev-log-full', 'ev-log-get-act-sup', 'ev-log-wrap-detect']

facadeimplementationlanguages = ['Java', 'Ruby']

languagetype = ['Clojure', 'Groovy', 'Javascript', 'Python', 'Ruby']

linefrequency = ['line-f-50hz', 'line-f-60hz', 'line-f-unspec']

mdsstatus = ['associated', 'associating', 'configured', 'configuring', 'disassociated', 'disassociating', 'disconnected', 'operating', 're-configuring', 're-initializing', 'terminating', 'unassociated']

mdssetstateresult = ['associated', 'associating', 'configured', 'configuring', 'disassociated', 'disassociating', 'disconnected', 'operating', 're-configuring', 're-initializing', 'terminating', 'unassociated']

measurementstatus = ['calibration-ongoing', 'demo-data', 'early-indication', 'invalid', 'msmt-ongoing', 'msmt-state-al-inhibited', 'msmt-state-in-alarm', 'not-available', 'questionable', 'test-data', 'validated-data']

metricaccess = ['acc-evrep', 'acc-get', 'acc-scan', 'avail-intermittent', 'gen-opt-sync', 'msmt-noncontinuous', 'sc-opt-confirm', 'sc-opt-extensive', 'sc-opt-long-pd-avail', 'sc-opt-normal', 'sc-opt-refresh', 'upd-episodic', 'upd-periodic']

metriccalstate = ['cal-required', 'calibrated', 'not-calibrated']

metriccaltype = ['cal-gain', 'cal-offset', 'cal-two-point', 'cal-unspec']

metriccategory = ['auto-calculation', 'auto-measurement', 'auto-setting', 'manual-calculation', 'manual-measurement', 'manual-setting', 'mcat-unspec']

metricrelevance = ['rv-app-specific', 'rv-btb-metric', 'rv-internal', 'rv-no-recording', 'rv-phys-ev-ind', 'rv-service', 'rv-store-only', 'rv-unspec']

metricstatus = ['metric-hw-discon', 'metric-not-ready', 'metric-off', 'metric-standby', 'metric-transduc-discon']

metricstructure_ms_struct_anon_enum = ['complex', 'compound', 'simple']

mibcccommmode = ['comm-mode-full-duplex', 'comm-mode-half-duplex', 'comm-mode-simplex']

msmtprinciple = ['msp-acoustical', 'msp-biological', 'msp-chemical', 'msp-electrical', 'msp-impedance', 'msp-manual', 'msp-mechanical', 'msp-nuclear', 'msp-optical', 'msp-other', 'msp-thermal']

nompartition = ['nom-part-alert', 'nom-part-dim', 'nom-part-ecg-extn', 'nom-part-ext-nom', 'nom-part-fef', 'nom-part-infrastruct', 'nom-part-metric', 'nom-part-obj', 'nom-part-pgrp', 'nom-part-priv', 'nom-part-sites', 'nom-part-unspec', 'nom-part-vattr']

nomenclatureversion_nom_major_version_anon_enum = ['majorVersion1', 'majorVersion2', 'majorVersion3', 'majorVersion4']

opinvresult = ['op-failure', 'op-successful']

oplevel = ['op-item-config', 'op-item-normal', 'op-item-service', 'op-level-advanced', 'op-level-basic', 'op-level-professional']

opmodtype = ['op-invokeAction', 'op-invokeActionWithArgs', 'op-replace', 'op-setToDefault']

opoptions = ['is-setting', 'needs-confirmation', 'op-auto-repeat', 'op-ctxt-help', 'op-dependency', 'sets-sco-lock', 'supports-default']

operationalstate = ['disabled', 'enabled', 'notAvailable']

patdemostate = ['admitted', 'discharged', 'empty', 'pre-admitted']

patientrace = ['race-black', 'race-caucasian', 'race-oriental', 'race-unspecified']

patientsex = ['female', 'male', 'sex-unknown', 'sex-unspecified']

patienttype = ['adult', 'neonatal', 'pediatric', 'pt-unspecified']

powerstatus = ['chargingFull', 'chargingOff', 'chargingTrickle', 'onBattery', 'onMains']

prodspecentry_spec_type_anon_enum = ['fw-revision', 'hw-revision', 'part-number', 'prod-spec-gmdn', 'protocol-revision', 'serial-number', 'sw-revision', 'unspecified']

sacaldatatype = ['bar', 'stair']

safilterentry_filter_type_anon_enum = ['high-pass', 'low-pass', 'notch', 'other']

saflags = ['delayed-curve', 'sa-ext-val-range', 'smooth-curve', 'static-scale']

scanconfiglimit = ['auto-init-scan-list', 'auto-updt-scan-list', 'no-scan-delete', 'no-scan-list-mod']

scanextend = ['extensive', 'superpositive', 'superpositive-avg']

scoactivityindicator = ['act-ind-blinking', 'act-ind-off', 'act-ind-on']

scocapability = ['act-indicator', 'sco-ctxt-help', 'sco-locks']

scooperinvokeerror_op_error_anon_enum = ['checksum-error', 'invalid-mod-type', 'invalid-value', 'op-err-unspec', 'sco-lock-violation', 'unknown-operation']

setstropt = ['setstr-displayable', 'setstr-hidden-val', 'setstr-null-terminated', 'setstr-var-length']

simplecolour = ['col-black', 'col-blue', 'col-cyan', 'col-green', 'col-magenta', 'col-red', 'col-white', 'col-yellow']

stosamplealg = ['st-alg-max-pick', 'st-alg-median', 'st-alg-min-pick', 'st-alg-moving-average', 'st-alg-nos', 'st-alg-recursive']

storageformat = ['sto-t-gen', 'sto-t-nos', 'sto-t-nu-opt', 'sto-t-rtsa-opt']

stringflags = ['str-flag-nt']

systemcapability = ['sc-auto-init-scan-list', 'sc-auto-updt-scan-list', 'sc-dyn-configuration', 'sc-dyn-scanner-create', 'sc-multiple-context']

timecapability = ['time-capab-ebww', 'time-capab-internal-only', 'time-capab-leap-second-aware', 'time-capab-patient-care', 'time-capab-real-time-clock', 'time-capab-rtsa-time-sync-annotations', 'time-capab-rtsa-time-sync-high-precision', 'time-capab-set-leap-sec-action-sup', 'time-capab-set-time-action-sup', 'time-capab-set-time-iso-sup', 'time-capab-set-time-zone-action-sup', 'time-capab-time-displayed', 'time-capab-time-zone-aware']

togglestate = ['tog-state0', 'tog-state1']

vmdstatus = ['vmd-hw-discon', 'vmd-not-ready', 'vmd-off', 'vmd-standby', 'vmd-transduc-discon']


# DIM::ANY =================================================
collection(:Summary, DIM::ANY) {
  view_ref(:Details)
}

organizer(:Summary, DIM::ANY) {
  link(:Details)
}

organizer(:Details, DIM::ANY) {
}


# DIM::ANY_COMPLEX =========================================
collection(:Summary, DIM::ANY_COMPLEX) {
  view_ref(:Details)
}

organizer(:Summary, DIM::ANY_COMPLEX) {
  link(:Details)
}

organizer(:Details, DIM::ANY_COMPLEX) {
}


# DIM::ANY_SIMPLE ==========================================
collection(:Summary, DIM::ANY_SIMPLE) {
  view_ref(:Details)
}

organizer(:Summary, DIM::ANY_SIMPLE) {
  link(:Details)
}

organizer(:Details, DIM::ANY_SIMPLE) {
}


# DIM::AVA_Type ============================================
collection(:Summary, DIM::AVA_Type) {
  integer('attribute_id_dim', :label => 'Attribute Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::AVA_Type) {
  integer('attribute_id_dim', :label => 'Attribute Id Dim')
  link(:Details)
}

organizer(:Details, DIM::AVA_Type) {
  integer('attribute_id_dim', :label => 'Attribute Id Dim')
  view_ref(:Summary, 'attribute_value', :label => 'Attribute Value')
  
  
  
  
  
  
  
  
  
  
  
  
}


# DIM::AbsTimeRange ========================================
collection(:Summary, DIM::AbsTimeRange) {
  view_ref(:Details)
}

organizer(:Summary, DIM::AbsTimeRange) {
  link(:Details)
}

organizer(:Details, DIM::AbsTimeRange) {
  view_ref(:Summary, 'from_time', :label => 'From Time')
  view_ref(:Summary, 'to_time', :label => 'To Time')
}


# DIM::AbsoluteRange =======================================
collection(:Summary, DIM::AbsoluteRange) {
  integer('lower_value', :label => 'Lower Value')
  integer('upper_value', :label => 'Upper Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::AbsoluteRange) {
  integer('lower_value', :label => 'Lower Value')
  integer('upper_value', :label => 'Upper Value')
  link(:Details)
}

organizer(:Details, DIM::AbsoluteRange) {
  integer('lower_value', :label => 'Lower Value')
  integer('upper_value', :label => 'Upper Value')
  
  
}


# DIM::AbsoluteRelativeTimeSync ============================
collection(:Summary, DIM::AbsoluteRelativeTimeSync) {
  integer('high_res_rollovers', :label => 'High Res Rollovers')
  integer('relative_rollovers', :label => 'Relative Rollovers')
  integer('relative_time_mark', :label => 'Relative Time Mark')
  string('hires_time_mark', :label => 'Hires Time Mark')
  view_ref(:Details)
}

organizer(:Summary, DIM::AbsoluteRelativeTimeSync) {
  integer('high_res_rollovers', :label => 'High Res Rollovers')
  integer('relative_rollovers', :label => 'Relative Rollovers')
  integer('relative_time_mark', :label => 'Relative Time Mark')
  string('hires_time_mark', :label => 'Hires Time Mark')
  link(:Details)
}

organizer(:Details, DIM::AbsoluteRelativeTimeSync) {
  integer('high_res_rollovers', :label => 'High Res Rollovers')
  integer('relative_rollovers', :label => 'Relative Rollovers')
  integer('relative_time_mark', :label => 'Relative Time Mark')
  string('hires_time_mark', :label => 'Hires Time Mark')
  view_ref(:Summary, 'absolute_time_mark', :label => 'Absolute Time Mark')
  view_ref(:Summary, 'ext_time_marks', :label => 'Ext Time Marks')
  
  
}


# DIM::AbsoluteTime ========================================
collection(:Summary, DIM::AbsoluteTime) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('hour', :label => 'Hour')
  integer('minute', :label => 'Minute')
  integer('month', :label => 'Month')
  integer('sec_fractions', :label => 'Sec Fractions')
  integer('second', :label => 'Second')
  integer('year', :label => 'Year')
  view_ref(:Details)
}

organizer(:Summary, DIM::AbsoluteTime) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('hour', :label => 'Hour')
  integer('minute', :label => 'Minute')
  integer('month', :label => 'Month')
  integer('sec_fractions', :label => 'Sec Fractions')
  integer('second', :label => 'Second')
  integer('year', :label => 'Year')
  link(:Details)
}

organizer(:Details, DIM::AbsoluteTime) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('hour', :label => 'Hour')
  integer('minute', :label => 'Minute')
  integer('month', :label => 'Month')
  integer('sec_fractions', :label => 'Sec Fractions')
  integer('second', :label => 'Second')
  integer('year', :label => 'Year')
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
}


# DIM::Activate_Operation ==================================
collection(:Summary, DIM::Activate_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Activate_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Activate_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
}


# DIM::AlStatChgCnt ========================================
collection(:Summary, DIM::AlStatChgCnt) {
  integer('al_new_chg_cnt', :label => 'Al New Chg Cnt')
  integer('al_stack_chg_cnt', :label => 'Al Stack Chg Cnt')
  view_ref(:Details)
}

organizer(:Summary, DIM::AlStatChgCnt) {
  integer('al_new_chg_cnt', :label => 'Al New Chg Cnt')
  integer('al_stack_chg_cnt', :label => 'Al Stack Chg Cnt')
  link(:Details)
}

organizer(:Details, DIM::AlStatChgCnt) {
  integer('al_new_chg_cnt', :label => 'Al New Chg Cnt')
  integer('al_stack_chg_cnt', :label => 'Al Stack Chg Cnt')
  
}


# DIM::Alert ===============================================
collection(:Summary, DIM::Alert) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Alert) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::Alert) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Summary, 'alert_condition', :label => 'Alert Condition')
  
  
  
  view_ref(:Summary, 'limit_specification', :label => 'Limit Specification')
}


# DIM::AlertCapabEntry =====================================
collection(:Summary, DIM::AlertCapabEntry) {
  choice('max_p_severity', alerttype, :label => 'Max P_severity')
  choice('max_t_severity', alerttype, :label => 'Max T_severity')
  integer('alert_group', :label => 'Alert Group')
  integer('max_p_obj_al', :label => 'Max P_obj Al')
  integer('max_t_obj_al', :label => 'Max T_obj Al')
  integer('obj_class', :label => 'Obj Class')
  integer('obj_reference', :label => 'Obj Reference')
  string('al_rep_flags', :label => 'Al Rep Flags')
  view_ref(:Details)
}

organizer(:Summary, DIM::AlertCapabEntry) {
  choice('max_p_severity', alerttype, :label => 'Max P_severity')
  choice('max_t_severity', alerttype, :label => 'Max T_severity')
  integer('alert_group', :label => 'Alert Group')
  integer('max_p_obj_al', :label => 'Max P_obj Al')
  integer('max_t_obj_al', :label => 'Max T_obj Al')
  integer('obj_class', :label => 'Obj Class')
  integer('obj_reference', :label => 'Obj Reference')
  string('al_rep_flags', :label => 'Al Rep Flags')
  link(:Details)
}

organizer(:Details, DIM::AlertCapabEntry) {
  choice('max_p_severity', alerttype, :label => 'Max P_severity')
  choice('max_t_severity', alerttype, :label => 'Max T_severity')
  integer('alert_group', :label => 'Alert Group')
  integer('max_p_obj_al', :label => 'Max P_obj Al')
  integer('max_t_obj_al', :label => 'Max T_obj Al')
  integer('obj_class', :label => 'Obj Class')
  integer('obj_reference', :label => 'Obj Reference')
  string('al_rep_flags', :label => 'Al Rep Flags')
  
}


# DIM::AlertCondition ======================================
collection(:Summary, DIM::AlertCondition) {
  choice('alert_flags', alertflags, :label => 'Alert Flags')
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('obj_reference', :label => 'Obj Reference')
  view_ref(:Details)
}

organizer(:Summary, DIM::AlertCondition) {
  choice('alert_flags', alertflags, :label => 'Alert Flags')
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('obj_reference', :label => 'Obj Reference')
  link(:Details)
}

organizer(:Details, DIM::AlertCondition) {
  choice('alert_flags', alertflags, :label => 'Alert Flags')
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('obj_reference', :label => 'Obj Reference')
  view_ref(:Summary, 'alert_info', :label => 'Alert Info')
  
}


# DIM::AlertEntry ==========================================
collection(:Summary, DIM::AlertEntry) {
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('instance', :label => 'Instance')
  integer('obj_reference', :label => 'Obj Reference')
  view_ref(:Details)
}

organizer(:Summary, DIM::AlertEntry) {
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('instance', :label => 'Instance')
  integer('obj_reference', :label => 'Obj Reference')
  link(:Details)
}

organizer(:Details, DIM::AlertEntry) {
  choice('alert_type', alerttype, :label => 'Alert Type')
  choice('controls', alertcontrols, :label => 'Controls')
  integer('alert_code1', :label => 'Alert Code1')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  integer('alert_source', :label => 'Alert Source')
  integer('instance', :label => 'Instance')
  integer('obj_reference', :label => 'Obj Reference')
  view_ref(:Summary, 'alert_info', :label => 'Alert Info')
  
  
}


# DIM::AlertOpTextString ===================================
collection(:Summary, DIM::AlertOpTextString) {
  string('lower_text', :label => 'Lower Text')
  string('upper_text', :label => 'Upper Text')
  view_ref(:Details)
}

organizer(:Summary, DIM::AlertOpTextString) {
  string('lower_text', :label => 'Lower Text')
  string('upper_text', :label => 'Upper Text')
  link(:Details)
}

organizer(:Details, DIM::AlertOpTextString) {
  string('lower_text', :label => 'Lower Text')
  string('upper_text', :label => 'Upper Text')
  
}


# DIM::Alert_Monitor =======================================
collection(:Summary, DIM::Alert_Monitor) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('suspension_period', :label => 'Suspension Period')
  string('device_alert_condition', :label => 'Device Alert Condition')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Alert_Monitor) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('suspension_period', :label => 'Suspension Period')
  string('device_alert_condition', :label => 'Device Alert Condition')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::Alert_Monitor) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('suspension_period', :label => 'Suspension Period')
  string('device_alert_condition', :label => 'Device Alert Condition')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Summary, 'device_p_alarm_list', :label => 'Device P_Alarm List')
  view_ref(:Summary, 'device_sup_alarm_list', :label => 'Device Sup Alarm List')
  view_ref(:Summary, 'device_t_alarm_list', :label => 'Device T_Alarm List')
  
  view_ref(:Summary, 'limit_spec_list', :label => 'Limit Spec List')
}


# DIM::Alert_Scanner =======================================
collection(:Summary, DIM::Alert_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Alert_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Alert_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
}


# DIM::Alert_Status ========================================
collection(:Summary, DIM::Alert_Status) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Alert_Status) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::Alert_Status) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Summary, 'alert_capab_list', :label => 'Alert Capab List')
  
  
  view_ref(:Summary, 'limit_spec_list', :label => 'Limit Spec List')
  view_ref(:Summary, 'physio_alert_list', :label => 'Physio Alert List')
  view_ref(:Summary, 'tech_alert_list', :label => 'Tech Alert List')
}


# DIM::ArchiveProtection ===================================
collection(:Summary, DIM::ArchiveProtection) {
  integer('protection_type', :label => 'Protection Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::ArchiveProtection) {
  integer('protection_type', :label => 'Protection Type')
  link(:Details)
}

organizer(:Details, DIM::ArchiveProtection) {
  integer('protection_type', :label => 'Protection Type')
  
  
  
  
  view_ref(:Summary, 'protection_key', :label => 'Protection Key')
}


# DIM::Authorization =======================================
collection(:Summary, DIM::Authorization) {
  integer('authorization_type', :label => 'Authorization Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Authorization) {
  integer('authorization_type', :label => 'Authorization Type')
  link(:Details)
}

organizer(:Details, DIM::Authorization) {
  integer('authorization_type', :label => 'Authorization Type')
  view_ref(:Summary, 'authorization_key', :label => 'Authorization Key')
  
}


# DIM::BCC =================================================
collection(:Summary, DIM::BCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::BCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  link(:Details)
}

organizer(:Details, DIM::BCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  view_ref(:Summary, 'device_interface', :label => 'Device Interface')
  
}


# DIM::BatMeasure ==========================================
collection(:Summary, DIM::BatMeasure) {
  integer('unit', :label => 'Unit')
  integer('value', :label => 'Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::BatMeasure) {
  integer('unit', :label => 'Unit')
  integer('value', :label => 'Value')
  link(:Details)
}

organizer(:Details, DIM::BatMeasure) {
  integer('unit', :label => 'Unit')
  integer('value', :label => 'Value')
  
  
  
  
  
  
  
  
  
}


# DIM::Battery =============================================
collection(:Summary, DIM::Battery) {
  choice('battery_status', batterystatus, :label => 'Battery Status')
  integer('charge_cycles', :label => 'Charge Cycles')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  view_ref(:Details)
}

organizer(:Summary, DIM::Battery) {
  choice('battery_status', batterystatus, :label => 'Battery Status')
  integer('charge_cycles', :label => 'Charge Cycles')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  link(:Details)
}

organizer(:Details, DIM::Battery) {
  choice('battery_status', batterystatus, :label => 'Battery Status')
  integer('charge_cycles', :label => 'Charge Cycles')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  view_ref(:Summary, 'battery_temperature', :label => 'Battery Temperature')
  view_ref(:Summary, 'capacity_full_charge', :label => 'Capacity Full Charge')
  view_ref(:Summary, 'capacity_remaining', :label => 'Capacity Remaining')
  view_ref(:Summary, 'capacity_specified', :label => 'Capacity Specified')
  view_ref(:Summary, 'current', :label => 'Current')
  
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'voltage', :label => 'Voltage')
  view_ref(:Summary, 'voltage_specified', :label => 'Voltage Specified')
}


# DIM::CfgScanner ==========================================
collection(:Summary, DIM::CfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::CfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::CfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'scan_list', :label => 'Scan List')
}


# DIM::Channel =============================================
collection(:Summary, DIM::Channel) {
  choice('channel_status', channelstatus, :label => 'Channel Status')
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  integer('channel_id_dim', :label => 'Channel Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('logical_channel_no', :label => 'Logical Channel No')
  integer('name_binding', :label => 'Name Binding')
  integer('parameter_group', :label => 'Parameter Group')
  integer('physical_channel_no', :label => 'Physical Channel No')
  string('color', :label => 'Color')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Channel) {
  choice('channel_status', channelstatus, :label => 'Channel Status')
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  integer('channel_id_dim', :label => 'Channel Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('logical_channel_no', :label => 'Logical Channel No')
  integer('name_binding', :label => 'Name Binding')
  integer('parameter_group', :label => 'Parameter Group')
  integer('physical_channel_no', :label => 'Physical Channel No')
  string('color', :label => 'Color')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::Channel) {
  choice('channel_status', channelstatus, :label => 'Channel Status')
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  integer('channel_id_dim', :label => 'Channel Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('logical_channel_no', :label => 'Logical Channel No')
  integer('name_binding', :label => 'Name Binding')
  integer('parameter_group', :label => 'Parameter Group')
  integer('physical_channel_no', :label => 'Physical Channel No')
  string('color', :label => 'Color')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  
  view_ref(:Summary, 'metric', :label => 'Metric')
  view_ref(:Summary, 'pm_store', :label => 'P M Store')
}


# DIM::ClearLogRangeInvoke =================================
collection(:Summary, DIM::ClearLogRangeInvoke) {
  choice('clear_log_option', clearlogoptions, :label => 'Clear Log Option')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::ClearLogRangeInvoke) {
  choice('clear_log_option', clearlogoptions, :label => 'Clear Log Option')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
  link(:Details)
}

organizer(:Details, DIM::ClearLogRangeInvoke) {
  choice('clear_log_option', clearlogoptions, :label => 'Clear Log Option')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
}


# DIM::ClearLogRangeResult =================================
collection(:Summary, DIM::ClearLogRangeResult) {
  choice('clear_log_result', clearlogresult, :label => 'Clear Log Result')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::ClearLogRangeResult) {
  choice('clear_log_result', clearlogresult, :label => 'Clear Log Result')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
  link(:Details)
}

organizer(:Details, DIM::ClearLogRangeResult) {
  choice('clear_log_result', clearlogresult, :label => 'Clear Log Result')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('from_log_entry_index', :label => 'From Log Entry Index')
  integer('log_change_count', :label => 'Log Change Count')
  integer('to_log_entry_index', :label => 'To Log Entry Index')
}


# DIM::Clock ===============================================
collection(:Summary, DIM::Clock) {
  integer('class_reserved', :label => 'Class')
  integer('cumulative_leap_seconds', :label => 'Cumulative Leap Seconds')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time', :label => 'Relative Time')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('iso_date_and_time', :label => 'I SO Date And Time')
  view_ref(:Details)
}

organizer(:Summary, DIM::Clock) {
  integer('class_reserved', :label => 'Class')
  integer('cumulative_leap_seconds', :label => 'Cumulative Leap Seconds')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time', :label => 'Relative Time')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('iso_date_and_time', :label => 'I SO Date And Time')
  link(:Details)
}

organizer(:Details, DIM::Clock) {
  integer('class_reserved', :label => 'Class')
  integer('cumulative_leap_seconds', :label => 'Cumulative Leap Seconds')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time', :label => 'Relative Time')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('iso_date_and_time', :label => 'I SO Date And Time')
  view_ref(:Summary, 'absolute_relative_sync', :label => 'Absolute Relative Sync')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  view_ref(:Summary, 'date_time_status', :label => 'Date Time Status')
  view_ref(:Summary, 'daylight_savings_transition', :label => 'Daylight Savings Transition')
  view_ref(:Summary, 'ext_time_stamp_list', :label => 'Ext Time Stamp List')
  
  view_ref(:Summary, 'next_leap_seconds', :label => 'Next Leap Seconds')
  view_ref(:Summary, 'time_support', :label => 'Time Support')
  view_ref(:Summary, 'time_zone', :label => 'Time Zone')
}


# DIM::ClockStatusUpdateInfo ===============================
collection(:Summary, DIM::ClockStatusUpdateInfo) {
  view_ref(:Details)
}

organizer(:Summary, DIM::ClockStatusUpdateInfo) {
  link(:Details)
}

organizer(:Details, DIM::ClockStatusUpdateInfo) {
  view_ref(:Summary, 'date_time_status', :label => 'Date Time Status')
  view_ref(:Summary, 'time_sync', :label => 'Time Sync')
}


# DIM::CmplxDynAttr ========================================
collection(:Summary, DIM::CmplxDynAttr) {
  integer('cm_dyn_cnt', :label => 'Cm Dyn Cnt')
  integer('unused', :label => 'Unused')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxDynAttr) {
  integer('cm_dyn_cnt', :label => 'Cm Dyn Cnt')
  integer('unused', :label => 'Unused')
  link(:Details)
}

organizer(:Details, DIM::CmplxDynAttr) {
  integer('cm_dyn_cnt', :label => 'Cm Dyn Cnt')
  integer('unused', :label => 'Unused')
  view_ref(:Summary, 'cm_dyn_elem_list', :label => 'Cm Dyn Elem List')
  
}


# DIM::CmplxDynAttrElem ====================================
collection(:Summary, DIM::CmplxDynAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxDynAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  link(:Details)
}

organizer(:Details, DIM::CmplxDynAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
}


# DIM::CmplxElemInfo =======================================
collection(:Summary, DIM::CmplxElemInfo) {
  integer('class_id_dim', :label => 'Class Id Dim')
  integer('max_comp_no', :label => 'Max Comp No')
  integer('max_inst', :label => 'Max Inst')
  integer('max_inst_comp', :label => 'Max Inst Comp')
  integer('max_inst_mplex', :label => 'Max Inst Mplex')
  integer('max_str_size', :label => 'Max Str Size')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxElemInfo) {
  integer('class_id_dim', :label => 'Class Id Dim')
  integer('max_comp_no', :label => 'Max Comp No')
  integer('max_inst', :label => 'Max Inst')
  integer('max_inst_comp', :label => 'Max Inst Comp')
  integer('max_inst_mplex', :label => 'Max Inst Mplex')
  integer('max_str_size', :label => 'Max Str Size')
  link(:Details)
}

organizer(:Details, DIM::CmplxElemInfo) {
  integer('class_id_dim', :label => 'Class Id Dim')
  integer('max_comp_no', :label => 'Max Comp No')
  integer('max_inst', :label => 'Max Inst')
  integer('max_inst_comp', :label => 'Max Inst Comp')
  integer('max_inst_mplex', :label => 'Max Inst Mplex')
  integer('max_str_size', :label => 'Max Str Size')
  
}


# DIM::CmplxMetricInfo =====================================
collection(:Summary, DIM::CmplxMetricInfo) {
  integer('max_mplex_dyn', :label => 'Max Mplex Dyn')
  integer('max_mplex_obs', :label => 'Max Mplex Obs')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxMetricInfo) {
  integer('max_mplex_dyn', :label => 'Max Mplex Dyn')
  integer('max_mplex_obs', :label => 'Max Mplex Obs')
  link(:Details)
}

organizer(:Details, DIM::CmplxMetricInfo) {
  integer('max_mplex_dyn', :label => 'Max Mplex Dyn')
  integer('max_mplex_obs', :label => 'Max Mplex Obs')
  view_ref(:Summary, 'cm_elem_info_list', :label => 'Cm Elem Info List')
  
}


# DIM::CmplxObsElem ========================================
collection(:Summary, DIM::CmplxObsElem) {
  choice('cm_obs_elem_flgs', cmplxobselemflags, :label => 'Cm Obs Elem Flgs')
  integer('cm_elem_idx', :label => 'Cm Elem Idx')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxObsElem) {
  choice('cm_obs_elem_flgs', cmplxobselemflags, :label => 'Cm Obs Elem Flgs')
  integer('cm_elem_idx', :label => 'Cm Elem Idx')
  link(:Details)
}

organizer(:Details, DIM::CmplxObsElem) {
  choice('cm_obs_elem_flgs', cmplxobselemflags, :label => 'Cm Obs Elem Flgs')
  integer('cm_elem_idx', :label => 'Cm Elem Idx')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
}


# DIM::CmplxObsValue =======================================
collection(:Summary, DIM::CmplxObsValue) {
  choice('cm_obs_flags', cmplxflags, :label => 'Cm Obs Flags')
  integer('cm_obs_cnt', :label => 'Cm Obs Cnt')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxObsValue) {
  choice('cm_obs_flags', cmplxflags, :label => 'Cm Obs Flags')
  integer('cm_obs_cnt', :label => 'Cm Obs Cnt')
  link(:Details)
}

organizer(:Details, DIM::CmplxObsValue) {
  choice('cm_obs_flags', cmplxflags, :label => 'Cm Obs Flags')
  integer('cm_obs_cnt', :label => 'Cm Obs Cnt')
  view_ref(:Summary, 'cm_obs_elem_list', :label => 'Cm Obs Elem List')
  
}


# DIM::CmplxStaticAttr =====================================
collection(:Summary, DIM::CmplxStaticAttr) {
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxStaticAttr) {
  link(:Details)
}

organizer(:Details, DIM::CmplxStaticAttr) {
  view_ref(:Summary, 'cm_static_elem_list', :label => 'Cm Static Elem List')
  
}


# DIM::CmplxStaticAttrElem =================================
collection(:Summary, DIM::CmplxStaticAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  view_ref(:Details)
}

organizer(:Summary, DIM::CmplxStaticAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  link(:Details)
}

organizer(:Details, DIM::CmplxStaticAttrElem) {
  integer('cm_elem_idx_1', :label => 'Cm Elem Idx_1')
  integer('cm_elem_idx_2', :label => 'Cm Elem Idx_2')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
}


# Gui_Builder_Profile::Code ================================
collection(:Summary, Gui_Builder_Profile::Code) {
  code('code', :choice => ['language', languagetype], :label => 'Code')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::Code) {
  code('code', :choice => ['language', languagetype], :label => 'Code')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::Code) {
  code('code', :choice => ['language', languagetype], :label => 'Code')
}


# DIM::Communication_Controller ============================
collection(:Summary, DIM::Communication_Controller) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::Communication_Controller) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  link(:Details)
}

organizer(:Details, DIM::Communication_Controller) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  
}


# DIM::Complex_Metric ======================================
collection(:Summary, DIM::Complex_Metric) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('unit_code', :label => 'Unit Code')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('cmplx_recursion_depth', :label => 'Cmplx Recursion Depth')
  integer('class_reserved', :label => 'Class')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Complex_Metric) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('cmplx_recursion_depth', :label => 'Cmplx Recursion Depth')
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('handle', :label => 'Handle')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Complex_Metric) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('cmplx_recursion_depth', :label => 'Cmplx Recursion Depth')
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('handle', :label => 'Handle')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('measure_mode', :label => 'Measure Mode')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'cmplx_dyn_attr', :label => 'Cmplx Dyn Attr')
  view_ref(:Summary, 'cmplx_metric_info', :label => 'Cmplx Metric Info')
  view_ref(:Summary, 'cmplx_observed_value', :label => 'Cmplx Observed Value')
  view_ref(:Summary, 'cmplx_static_attr', :label => 'Cmplx Static Attr')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric', :label => 'Metric')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::Composite_Multiple_Bed_MDS ==========================
collection(:Summary, DIM::Composite_Multiple_Bed_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('battery_level', :label => 'Battery Level')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('altitude', :label => 'Altitude')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Composite_Multiple_Bed_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('operating_mode', :label => 'Operating Mode')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::Composite_Multiple_Bed_MDS) {
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('power_status', powerstatus, :label => 'Power Status')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('relative_time', :label => 'Relative Time')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alert_monitor', :label => 'Alert Monitor')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'battery', :label => 'Battery')
  view_ref(:Summary, 'clock', :label => 'Clock')
  view_ref(:Summary, 'communication_controller', :label => 'Communication Controller')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  
  
  view_ref(:Summary, 'log', :label => 'Log')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'scanner', :label => 'Scanner')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
  view_ref(:Summary, 'vmd', :label => 'V MD')
}


# DIM::Composite_Single_Bed_MDS ============================
collection(:Summary, DIM::Composite_Single_Bed_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('battery_level', :label => 'Battery Level')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('altitude', :label => 'Altitude')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Composite_Single_Bed_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('operating_mode', :label => 'Operating Mode')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::Composite_Single_Bed_MDS) {
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('power_status', powerstatus, :label => 'Power Status')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('relative_time', :label => 'Relative Time')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alert_monitor', :label => 'Alert Monitor')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'battery', :label => 'Battery')
  view_ref(:Summary, 'clock', :label => 'Clock')
  view_ref(:Summary, 'communication_controller', :label => 'Communication Controller')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  
  
  view_ref(:Summary, 'log', :label => 'Log')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'scanner', :label => 'Scanner')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
  view_ref(:Summary, 'vmd', :label => 'V MD')
}


# DIM::Context_Scanner =====================================
collection(:Summary, DIM::Context_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('context_mode', contextmode, :label => 'Context Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Context_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('context_mode', contextmode, :label => 'Context Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Context_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('context_mode', contextmode, :label => 'Context Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
}


# DIM::CreateEntry =========================================
collection(:Summary, DIM::CreateEntry) {
  view_ref(:Details)
}

organizer(:Summary, DIM::CreateEntry) {
  link(:Details)
}

organizer(:Details, DIM::CreateEntry) {
  view_ref(:Summary, 'created_object', :label => 'Created Object')
  
  view_ref(:Summary, 'superior_object', :label => 'Superior Object')
}


# DIM::CreatedObject =======================================
collection(:Summary, DIM::CreatedObject) {
  view_ref(:Details)
}

organizer(:Summary, DIM::CreatedObject) {
  link(:Details)
}

organizer(:Details, DIM::CreatedObject) {
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  view_ref(:Summary, 'class_id_dim', :label => 'Class Id Dim')
  
}


# DIM::CtxtHelpRequest =====================================
collection(:Summary, DIM::CtxtHelpRequest) {
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::CtxtHelpRequest) {
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::CtxtHelpRequest) {
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
}


# DIM::CtxtHelpResult ======================================
collection(:Summary, DIM::CtxtHelpResult) {
  integer('hold_time', :label => 'Hold Time')
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::CtxtHelpResult) {
  integer('hold_time', :label => 'Hold Time')
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::CtxtHelpResult) {
  integer('hold_time', :label => 'Hold Time')
  integer('op_instance_no', :label => 'Op Instance No')
  integer('type', :label => 'Type')
}


# DIM::CurLimAlVal =========================================
collection(:Summary, DIM::CurLimAlVal) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  view_ref(:Details)
}

organizer(:Summary, DIM::CurLimAlVal) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  link(:Details)
}

organizer(:Details, DIM::CurLimAlVal) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  
  
}


# DIM::CurrentRange ========================================
collection(:Summary, DIM::CurrentRange) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  view_ref(:Details)
}

organizer(:Summary, DIM::CurrentRange) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  link(:Details)
}

organizer(:Details, DIM::CurrentRange) {
  integer('lower', :label => 'Lower')
  integer('upper', :label => 'Upper')
  
}


# DIM::DCC =================================================
collection(:Summary, DIM::DCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::DCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  link(:Details)
}

organizer(:Details, DIM::DCC) {
  choice('capability', cccapability, :label => 'Capability')
  choice('cc_ext_mgmt_proto_id_dim', ccextmgmtproto, :label => 'Cc Ext Mgmt Proto Id Dim')
  integer('cc_type', :label => 'C C Type')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_difs', :label => 'Number Of Difs')
  integer('this_connection_dif_index', :label => 'This Connection Dif Index')
  view_ref(:Summary, 'device_interface', :label => 'Device Interface')
  
}


# DIM::Date ================================================
collection(:Summary, DIM::Date) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('month', :label => 'Month')
  integer('year', :label => 'Year')
  view_ref(:Details)
}

organizer(:Summary, DIM::Date) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('month', :label => 'Month')
  integer('year', :label => 'Year')
  link(:Details)
}

organizer(:Details, DIM::Date) {
  integer('century', :label => 'Century')
  integer('day', :label => 'Day')
  integer('month', :label => 'Month')
  integer('year', :label => 'Year')
  
  
  
}


# DIM::DateTimeStatus ======================================
collection(:Summary, DIM::DateTimeStatus) {
  choice('usage_status', datetimeusage, :label => 'Usage Status')
  integer('active_sync_protocol', :label => 'Active Sync Protocol')
  integer('clock_accuracy', :label => 'Clock Accuracy')
  view_ref(:Details)
}

organizer(:Summary, DIM::DateTimeStatus) {
  choice('usage_status', datetimeusage, :label => 'Usage Status')
  integer('active_sync_protocol', :label => 'Active Sync Protocol')
  integer('clock_accuracy', :label => 'Clock Accuracy')
  link(:Details)
}

organizer(:Details, DIM::DateTimeStatus) {
  choice('usage_status', datetimeusage, :label => 'Usage Status')
  integer('active_sync_protocol', :label => 'Active Sync Protocol')
  integer('clock_accuracy', :label => 'Clock Accuracy')
  view_ref(:Summary, 'clock_last_set', :label => 'Clock Last Set')
  
  
}


# DIM::DaylightSavingsTransition ===========================
collection(:Summary, DIM::DaylightSavingsTransition) {
  view_ref(:Details)
}

organizer(:Summary, DIM::DaylightSavingsTransition) {
  link(:Details)
}

organizer(:Details, DIM::DaylightSavingsTransition) {
  
  
  view_ref(:Summary, 'next_offset', :label => 'Next Offset')
  view_ref(:Summary, 'transition_date', :label => 'Transition Date')
}


# DIM::DevAlarmEntry =======================================
collection(:Summary, DIM::DevAlarmEntry) {
  choice('al_state', alertstate, :label => 'Al State')
  choice('al_type', alerttype, :label => 'Al Type')
  integer('al_code1', :label => 'Al Code1')
  integer('al_source', :label => 'Al Source')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::DevAlarmEntry) {
  choice('al_state', alertstate, :label => 'Al State')
  choice('al_type', alerttype, :label => 'Al Type')
  integer('al_code1', :label => 'Al Code1')
  integer('al_source', :label => 'Al Source')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  link(:Details)
}

organizer(:Details, DIM::DevAlarmEntry) {
  choice('al_state', alertstate, :label => 'Al State')
  choice('al_type', alerttype, :label => 'Al Type')
  integer('al_code1', :label => 'Al Code1')
  integer('al_source', :label => 'Al Source')
  integer('alert_info_id_dim', :label => 'Alert Info Id Dim')
  view_ref(:Summary, 'alert_info', :label => 'Alert Info')
  
  
  
  view_ref(:Summary, 'object', :label => 'Object')
}


# DIM::DevAlertCondition ===================================
collection(:Summary, DIM::DevAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  view_ref(:Details)
}

organizer(:Summary, DIM::DevAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  link(:Details)
}

organizer(:Details, DIM::DevAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  view_ref(:Summary, 'al_stat_chg_cnt', :label => 'Al Stat Chg Cnt')
}


# DIM::DeviceAlertCondition ================================
collection(:Summary, DIM::DeviceAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  view_ref(:Details)
}

organizer(:Summary, DIM::DeviceAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  link(:Details)
}

organizer(:Details, DIM::DeviceAlertCondition) {
  choice('device_alert_state', alertstate, :label => 'Device Alert State')
  choice('max_aud_alarm', alerttype, :label => 'Max Aud Alarm')
  choice('max_p_alarm', alerttype, :label => 'Max P_alarm')
  choice('max_t_alarm', alerttype, :label => 'Max T_alarm')
  view_ref(:Summary, 'al_stat_chg_cnt', :label => 'Al Stat Chg Cnt')
}


# DIM::Device_Interface ====================================
collection(:Summary, DIM::Device_Interface) {
  view_ref(:Details)
}

organizer(:Summary, DIM::Device_Interface) {
  link(:Details)
}

organizer(:Details, DIM::Device_Interface) {
  
  
  view_ref(:Summary, 'mib_element', :label => 'Mib Element')
}


# DIM::Device_Interface_MibElement =========================
collection(:Summary, DIM::Device_Interface_MibElement) {
  choice('port_state', difmibportstate, :label => 'Port State')
  integer('active_profile', :label => 'Active Profile')
  integer('dif_id_dim', :label => 'Dif Id Dim')
  integer('dif_type', :label => 'Dif Type')
  integer('link_speed', :label => 'Link Speed')
  integer('mtu', :label => 'M TU')
  integer('mib_element_list', :label => 'Mib Element List')
  integer('port_number', :label => 'Port Number')
  integer('supported_profiles', :label => 'Supported Profiles')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  view_ref(:Details)
}

organizer(:Summary, DIM::Device_Interface_MibElement) {
  choice('port_state', difmibportstate, :label => 'Port State')
  integer('active_profile', :label => 'Active Profile')
  integer('dif_id_dim', :label => 'Dif Id Dim')
  integer('dif_type', :label => 'Dif Type')
  integer('link_speed', :label => 'Link Speed')
  integer('mtu', :label => 'M TU')
  integer('mib_element_list', :label => 'Mib Element List')
  integer('port_number', :label => 'Port Number')
  integer('supported_profiles', :label => 'Supported Profiles')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  link(:Details)
}

organizer(:Details, DIM::Device_Interface_MibElement) {
  choice('port_state', difmibportstate, :label => 'Port State')
  integer('active_profile', :label => 'Active Profile')
  integer('dif_id_dim', :label => 'Dif Id Dim')
  integer('dif_type', :label => 'Dif Type')
  integer('link_speed', :label => 'Link Speed')
  integer('mtu', :label => 'M TU')
  integer('mib_element_list', :label => 'Mib Element List')
  integer('port_number', :label => 'Port Number')
  integer('supported_profiles', :label => 'Supported Profiles')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  
}


# DIM::DispResolution ======================================
collection(:Summary, DIM::DispResolution) {
  integer('post_point', :label => 'Post Point')
  integer('pre_point', :label => 'Pre Point')
  view_ref(:Details)
}

organizer(:Summary, DIM::DispResolution) {
  integer('post_point', :label => 'Post Point')
  integer('pre_point', :label => 'Pre Point')
  link(:Details)
}

organizer(:Details, DIM::DispResolution) {
  integer('post_point', :label => 'Post Point')
  integer('pre_point', :label => 'Pre Point')
  
}


# DIM::Distribution_Sample_Array ===========================
collection(:Summary, DIM::Distribution_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('compression', :label => 'Compression')
  integer('unit_code', :label => 'Unit Code')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('handle', :label => 'Handle')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('x_unit_code', :label => 'X Unit Code')
  integer('class_reserved', :label => 'Class')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('unit_labelstring', :label => 'Unit Label String')
  string('color', :label => 'Color')
  string('x_unit_label_string', :label => 'X Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Distribution_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('x_unit_code', :label => 'X Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('unit_code', :label => 'Unit Code')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('unit_labelstring', :label => 'Unit Label String')
  string('color', :label => 'Color')
  string('x_unit_label_string', :label => 'X Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Distribution_Sample_Array) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('x_unit_code', :label => 'X Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('unit_code', :label => 'Unit Code')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('unit_labelstring', :label => 'Unit Label String')
  string('color', :label => 'Color')
  string('x_unit_label_string', :label => 'X Unit Label String')
  view_ref(:Summary, 'absolute_time_stamp', :label => 'Absolute Time Stamp')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_sa_observed_value', :label => 'Compound Sa Observed Value')
  view_ref(:Summary, 'distribution_range_specification', :label => 'Distribution Range Specification')
  view_ref(:Summary, 'dsa_marker_list', :label => 'Dsa Marker List')
  view_ref(:Summary, 'filter_specification', :label => 'Filter Specification')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'sa_observed_value', :label => 'Sa Observed Value')
  view_ref(:Summary, 'sa_signal_frequency', :label => 'Sa Signal Frequency')
  view_ref(:Summary, 'sa_specification', :label => 'Sa Specification')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::DsaRangeSpec ========================================
collection(:Summary, DIM::DsaRangeSpec) {
  integer('first_element_value', :label => 'First Element Value')
  integer('last_element_value', :label => 'Last Element Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::DsaRangeSpec) {
  integer('first_element_value', :label => 'First Element Value')
  integer('last_element_value', :label => 'Last Element Value')
  link(:Details)
}

organizer(:Details, DIM::DsaRangeSpec) {
  integer('first_element_value', :label => 'First Element Value')
  integer('last_element_value', :label => 'Last Element Value')
  
}


# DIM::EnumMsmtRangeLabel ==================================
collection(:Summary, DIM::EnumMsmtRangeLabel) {
  string('label', :label => 'Label')
  view_ref(:Details)
}

organizer(:Summary, DIM::EnumMsmtRangeLabel) {
  string('label', :label => 'Label')
  link(:Details)
}

organizer(:Details, DIM::EnumMsmtRangeLabel) {
  string('label', :label => 'Label')
}


# DIM::EnumObsValue ========================================
collection(:Summary, DIM::EnumObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::EnumObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  link(:Details)
}

organizer(:Details, DIM::EnumObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  
  
}


# DIM::EnumRecordMetric ====================================
collection(:Summary, DIM::EnumRecordMetric) {
  integer('record_type_code', :label => 'Record Type Code')
  view_ref(:Details)
}

organizer(:Summary, DIM::EnumRecordMetric) {
  integer('record_type_code', :label => 'Record Type Code')
  link(:Details)
}

organizer(:Details, DIM::EnumRecordMetric) {
  integer('record_type_code', :label => 'Record Type Code')
  view_ref(:Summary, 'record_data', :label => 'Record Data')
}


# DIM::EnumRecordOo ========================================
collection(:Summary, DIM::EnumRecordOo) {
  integer('record_type_code', :label => 'Record Type Code')
  view_ref(:Details)
}

organizer(:Summary, DIM::EnumRecordOo) {
  integer('record_type_code', :label => 'Record Type Code')
  link(:Details)
}

organizer(:Details, DIM::EnumRecordOo) {
  integer('record_type_code', :label => 'Record Type Code')
  view_ref(:Summary, 'record_data', :label => 'Record Data')
}


# DIM::Enumeration =========================================
collection(:Summary, DIM::Enumeration) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('handle', :label => 'Handle')
  integer('unit_code', :label => 'Unit Code')
  integer('enum_measure_range', :label => 'Enum Measure Range')
  integer('measure_mode', :label => 'Measure Mode')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('class_reserved', :label => 'Class')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  string('unit_labelstring', :label => 'Unit Label String')
  string('type', :label => 'Type')
  string('substance_labelstring', :label => 'Substance Label String')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('color', :label => 'Color')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('enum_measure_range_labels', :label => 'Enum Measure Range Labels')
  string('enum_measure_range_bit_string', :label => 'Enum Measure Range Bit String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Enumeration) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('class_reserved', :label => 'Class')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('handle', :label => 'Handle')
  integer('measure_mode', :label => 'Measure Mode')
  integer('enum_measure_range', :label => 'Enum Measure Range')
  string('unit_labelstring', :label => 'Unit Label String')
  string('type', :label => 'Type')
  string('substance_labelstring', :label => 'Substance Label String')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('color', :label => 'Color')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('enum_measure_range_labels', :label => 'Enum Measure Range Labels')
  string('enum_measure_range_bit_string', :label => 'Enum Measure Range Bit String')
  link(:Details)
}

organizer(:Details, DIM::Enumeration) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('class_reserved', :label => 'Class')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('handle', :label => 'Handle')
  integer('measure_mode', :label => 'Measure Mode')
  integer('enum_measure_range', :label => 'Enum Measure Range')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  string('unit_labelstring', :label => 'Unit Label String')
  string('type', :label => 'Type')
  string('substance_labelstring', :label => 'Substance Label String')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('color', :label => 'Color')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('enum_measure_range_labels', :label => 'Enum Measure Range Labels')
  string('enum_measure_range_bit_string', :label => 'Enum Measure Range Bit String')
  view_ref(:Summary, 'absolute_time_stamp', :label => 'Absolute Time Stamp')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_enum_observed_value', :label => 'Compound Enum Observed Value')
  view_ref(:Summary, 'enum_observed_value', :label => 'Enum Observed Value')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::EpiCfgScanner =======================================
collection(:Summary, DIM::EpiCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::EpiCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::EpiCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'scan_list', :label => 'Scan List')
}


# DIM::EventLogEntry =======================================
collection(:Summary, DIM::EventLogEntry) {
  integer('entry_number', :label => 'Entry Number')
  string('event_entry', :label => 'Event Entry')
  view_ref(:Details)
}

organizer(:Summary, DIM::EventLogEntry) {
  integer('entry_number', :label => 'Entry Number')
  string('event_entry', :label => 'Event Entry')
  link(:Details)
}

organizer(:Details, DIM::EventLogEntry) {
  integer('entry_number', :label => 'Entry Number')
  string('event_entry', :label => 'Event Entry')
  view_ref(:Summary, 'abs_time', :label => 'Abs Time')
  
}


# DIM::Event_Log ===========================================
collection(:Summary, DIM::Event_Log) {
  choice('event_log_info', eventloginfo, :label => 'Event Log Info')
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Event_Log) {
  choice('event_log_info', eventloginfo, :label => 'Event Log Info')
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::Event_Log) {
  choice('event_log_info', eventloginfo, :label => 'Event Log Info')
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  string('type', :label => 'Type')
  view_ref(:Summary, 'event_log_entry_list', :label => 'Event Log Entry List')
  
}


# DIM::ExtNomenRef =========================================
collection(:Summary, DIM::ExtNomenRef) {
  integer('nomenclature_id_dim', :label => 'Nomenclature Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::ExtNomenRef) {
  integer('nomenclature_id_dim', :label => 'Nomenclature Id Dim')
  link(:Details)
}

organizer(:Details, DIM::ExtNomenRef) {
  integer('nomenclature_id_dim', :label => 'Nomenclature Id Dim')
  
  
  
  
  
  
  
  view_ref(:Summary, 'nomenclature_code', :label => 'Nomenclature Code')
}


# DIM::ExtObjRelationEntry =================================
collection(:Summary, DIM::ExtObjRelationEntry) {
  integer('related_object', :label => 'Related Object')
  integer('relation_type', :label => 'Relation Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::ExtObjRelationEntry) {
  integer('related_object', :label => 'Related Object')
  integer('relation_type', :label => 'Relation Type')
  link(:Details)
}

organizer(:Details, DIM::ExtObjRelationEntry) {
  integer('related_object', :label => 'Related Object')
  integer('relation_type', :label => 'Relation Type')
  view_ref(:Summary, 'relation_attributes', :label => 'Relation Attributes')
}


# DIM::ExtTimeStamp ========================================
collection(:Summary, DIM::ExtTimeStamp) {
  integer('time_stamp_id_dim', :label => 'Time Stamp Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::ExtTimeStamp) {
  integer('time_stamp_id_dim', :label => 'Time Stamp Id Dim')
  link(:Details)
}

organizer(:Details, DIM::ExtTimeStamp) {
  integer('time_stamp_id_dim', :label => 'Time Stamp Id Dim')
  
  
  view_ref(:Summary, 'time_stamp', :label => 'Time Stamp')
}


# DIM::FastPeriCfgScanner ==================================
collection(:Summary, DIM::FastPeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::FastPeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::FastPeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'scan_list', :label => 'Scan List')
}


# DIM::FastScanReportInfo ==================================
collection(:Summary, DIM::FastScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::FastScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::FastScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'glb_scan_info', :label => 'Glb Scan Info')
}


# Gui_Builder_Profile::File ================================
collection(:Summary, Gui_Builder_Profile::File) {
  string('filename', :label => 'Filename')
  string('mime_type', :label => 'Mime Type')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::File) {
  string('filename', :label => 'Filename')
  string('mime_type', :label => 'Mime Type')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::File) {
  string('filename', :label => 'Filename')
  string('mime_type', :label => 'Mime Type')
}


# DIM::GLB_HANDLE ==========================================
collection(:Summary, DIM::GLB_HANDLE) {
  integer('context_id_dim', :label => 'Context Id Dim')
  integer('handle', :label => 'Handle')
  view_ref(:Details)
}

organizer(:Summary, DIM::GLB_HANDLE) {
  integer('context_id_dim', :label => 'Context Id Dim')
  integer('handle', :label => 'Handle')
  link(:Details)
}

organizer(:Details, DIM::GLB_HANDLE) {
  integer('context_id_dim', :label => 'Context Id Dim')
  integer('handle', :label => 'Handle')
  
  
  
  
  
  
  
}


# DIM::General_Communication_Statistics_MibElement =========
collection(:Summary, DIM::General_Communication_Statistics_MibElement) {
  choice('generic_mode', mibcccommmode, :label => 'Generic Mode')
  integer('unknown_protocol_packets_in', :label => 'Unknown Protocol Packets In')
  integer('queue_len_out', :label => 'Queue Len Out')
  integer('discarded_packets_in', :label => 'Discarded Packets In')
  integer('discarded_packets_out', :label => 'Discarded Packets Out')
  integer('errors_in', :label => 'Errors In')
  integer('errors_out', :label => 'Errors Out')
  integer('maximum_speed', :label => 'Maximum Speed')
  integer('queue_len_in', :label => 'Queue Len In')
  integer('octets_in', :label => 'Octets In')
  integer('octets_out', :label => 'Octets Out')
  integer('packets_in', :label => 'Packets In')
  integer('packets_out', :label => 'Packets Out')
  integer('average_speed', :label => 'Average Speed')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  string('dif_oper_status', :label => 'Dif Oper Status')
  string('dif_admin_status', :label => 'Dif Admin Status')
  view_ref(:Details)
}

organizer(:Summary, DIM::General_Communication_Statistics_MibElement) {
  choice('generic_mode', mibcccommmode, :label => 'Generic Mode')
  integer('discarded_packets_in', :label => 'Discarded Packets In')
  integer('discarded_packets_out', :label => 'Discarded Packets Out')
  integer('errors_in', :label => 'Errors In')
  integer('errors_out', :label => 'Errors Out')
  integer('maximum_speed', :label => 'Maximum Speed')
  integer('queue_len_in', :label => 'Queue Len In')
  integer('octets_in', :label => 'Octets In')
  integer('octets_out', :label => 'Octets Out')
  integer('packets_in', :label => 'Packets In')
  integer('packets_out', :label => 'Packets Out')
  integer('average_speed', :label => 'Average Speed')
  integer('unknown_protocol_packets_in', :label => 'Unknown Protocol Packets In')
  integer('queue_len_out', :label => 'Queue Len Out')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  string('dif_oper_status', :label => 'Dif Oper Status')
  string('dif_admin_status', :label => 'Dif Admin Status')
  link(:Details)
}

organizer(:Details, DIM::General_Communication_Statistics_MibElement) {
  choice('generic_mode', mibcccommmode, :label => 'Generic Mode')
  integer('discarded_packets_out', :label => 'Discarded Packets Out')
  integer('errors_in', :label => 'Errors In')
  integer('errors_out', :label => 'Errors Out')
  integer('maximum_speed', :label => 'Maximum Speed')
  integer('queue_len_in', :label => 'Queue Len In')
  integer('octets_in', :label => 'Octets In')
  integer('octets_out', :label => 'Octets Out')
  integer('packets_in', :label => 'Packets In')
  integer('packets_out', :label => 'Packets Out')
  integer('average_speed', :label => 'Average Speed')
  integer('unknown_protocol_packets_in', :label => 'Unknown Protocol Packets In')
  integer('queue_len_out', :label => 'Queue Len Out')
  integer('discarded_packets_in', :label => 'Discarded Packets In')
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  string('dif_oper_status', :label => 'Dif Oper Status')
  string('dif_admin_status', :label => 'Dif Admin Status')
  view_ref(:Summary, 'dif_last_change', :label => 'Dif Last Change')
  
}


# DIM::GetMibDataRequest ===================================
collection(:Summary, DIM::GetMibDataRequest) {
  integer('dif_index', :label => 'Dif Index')
  integer('mib_id_list', :label => 'Mib Id List')
  view_ref(:Details)
}

organizer(:Summary, DIM::GetMibDataRequest) {
  integer('dif_index', :label => 'Dif Index')
  integer('mib_id_list', :label => 'Mib Id List')
  link(:Details)
}

organizer(:Details, DIM::GetMibDataRequest) {
  integer('dif_index', :label => 'Dif Index')
  integer('mib_id_list', :label => 'Mib Id List')
}


# DIM::GetMibDataResult ====================================
collection(:Summary, DIM::GetMibDataResult) {
  integer('dif_index', :label => 'Dif Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::GetMibDataResult) {
  integer('dif_index', :label => 'Dif Index')
  link(:Details)
}

organizer(:Details, DIM::GetMibDataResult) {
  integer('dif_index', :label => 'Dif Index')
  view_ref(:Summary, 'mib_data_list', :label => 'Mib Data List')
}


# DIM::Hydra_MDS ===========================================
collection(:Summary, DIM::Hydra_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('battery_level', :label => 'Battery Level')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('altitude', :label => 'Altitude')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Hydra_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('operating_mode', :label => 'Operating Mode')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::Hydra_MDS) {
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('power_status', powerstatus, :label => 'Power Status')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('relative_time', :label => 'Relative Time')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alert_monitor', :label => 'Alert Monitor')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'battery', :label => 'Battery')
  view_ref(:Summary, 'clock', :label => 'Clock')
  view_ref(:Summary, 'communication_controller', :label => 'Communication Controller')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  
  
  view_ref(:Summary, 'log', :label => 'Log')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'scanner', :label => 'Scanner')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
  view_ref(:Summary, 'vmd', :label => 'V MD')
}


# DIM::LeapSecondsTransition ===============================
collection(:Summary, DIM::LeapSecondsTransition) {
  integer('next_cum_leap_seconds', :label => 'Next Cum Leap Seconds')
  view_ref(:Details)
}

organizer(:Summary, DIM::LeapSecondsTransition) {
  integer('next_cum_leap_seconds', :label => 'Next Cum Leap Seconds')
  link(:Details)
}

organizer(:Details, DIM::LeapSecondsTransition) {
  integer('next_cum_leap_seconds', :label => 'Next Cum Leap Seconds')
  
  
  view_ref(:Summary, 'transition_date', :label => 'Transition Date')
}


# DIM::LimitSpecEntry ======================================
collection(:Summary, DIM::LimitSpecEntry) {
  choice('lim_al_stat', curlimalstat, :label => 'Lim Al Stat')
  integer('al_source_id_dim', :label => 'Al Source Id Dim')
  integer('object_handle', :label => 'Object Handle')
  integer('unit_code', :label => 'Unit Code')
  view_ref(:Details)
}

organizer(:Summary, DIM::LimitSpecEntry) {
  choice('lim_al_stat', curlimalstat, :label => 'Lim Al Stat')
  integer('al_source_id_dim', :label => 'Al Source Id Dim')
  integer('object_handle', :label => 'Object Handle')
  integer('unit_code', :label => 'Unit Code')
  link(:Details)
}

organizer(:Details, DIM::LimitSpecEntry) {
  choice('lim_al_stat', curlimalstat, :label => 'Lim Al Stat')
  integer('al_source_id_dim', :label => 'Al Source Id Dim')
  integer('object_handle', :label => 'Object Handle')
  integer('unit_code', :label => 'Unit Code')
  
  
  
  view_ref(:Summary, 'lim_al_val', :label => 'Lim Al Val')
}


# DIM::Limit_Alert_Operation ===============================
collection(:Summary, DIM::Limit_Alert_Operation) {
  choice('alert_op_capability', alopcapab, :label => 'Alert Op Capability')
  choice('alert_op_state', curlimalstat, :label => 'Alert Op State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Limit_Alert_Operation) {
  choice('alert_op_capability', alopcapab, :label => 'Alert Op Capability')
  choice('alert_op_state', curlimalstat, :label => 'Alert Op State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Limit_Alert_Operation) {
  choice('alert_op_capability', alopcapab, :label => 'Alert Op Capability')
  choice('alert_op_state', curlimalstat, :label => 'Alert Op State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Summary, 'alert_op_text_string', :label => 'Alert Op Text String')
  view_ref(:Summary, 'current_limits', :label => 'Current Limits')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
  view_ref(:Summary, 'set_value_range', :label => 'Set Value Range')
}


# DIM::Locale ==============================================
collection(:Summary, DIM::Locale) {
  choice('charset', charset, :label => 'Charset')
  integer('country', :label => 'Country')
  integer('language', :label => 'Language')
  view_ref(:Details)
}

organizer(:Summary, DIM::Locale) {
  choice('charset', charset, :label => 'Charset')
  integer('country', :label => 'Country')
  integer('language', :label => 'Language')
  link(:Details)
}

organizer(:Details, DIM::Locale) {
  choice('charset', charset, :label => 'Charset')
  integer('country', :label => 'Country')
  integer('language', :label => 'Language')
  view_ref(:Summary, 'str_spec', :label => 'Str Spec')
}


# DIM::Log =================================================
collection(:Summary, DIM::Log) {
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  view_ref(:Details)
}

organizer(:Summary, DIM::Log) {
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  link(:Details)
}

organizer(:Details, DIM::Log) {
  integer('class_reserved', :label => 'Class')
  integer('current_log_entries', :label => 'Current Log Entries')
  integer('handle', :label => 'Handle')
  integer('log_change_count', :label => 'Log Change Count')
  integer('max_log_entries', :label => 'Max Log Entries')
  integer('name_binding', :label => 'Name Binding')
  
}


# DIM::MDS =================================================
collection(:Summary, DIM::MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('battery_level', :label => 'Battery Level')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('altitude', :label => 'Altitude')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('operating_mode', :label => 'Operating Mode')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::MDS) {
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('power_status', powerstatus, :label => 'Power Status')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('relative_time', :label => 'Relative Time')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alert_monitor', :label => 'Alert Monitor')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'battery', :label => 'Battery')
  view_ref(:Summary, 'clock', :label => 'Clock')
  view_ref(:Summary, 'communication_controller', :label => 'Communication Controller')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  
  
  view_ref(:Summary, 'log', :label => 'Log')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'scanner', :label => 'Scanner')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
  view_ref(:Summary, 'vmd', :label => 'V MD')
}


# DIM::ManagedObjectId =====================================
collection(:Summary, DIM::ManagedObjectId) {
  integer('m_obj_class', :label => 'M Obj Class')
  view_ref(:Details)
}

organizer(:Summary, DIM::ManagedObjectId) {
  integer('m_obj_class', :label => 'M Obj Class')
  link(:Details)
}

organizer(:Details, DIM::ManagedObjectId) {
  integer('m_obj_class', :label => 'M Obj Class')
  
  
  
  
  
  view_ref(:Summary, 'm_obj_inst', :label => 'M Obj Inst')
}


# DIM::MarkerEntryIndex ====================================
collection(:Summary, DIM::MarkerEntryIndex) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_index', :label => 'Marker Index')
  view_ref(:Details)
}

organizer(:Summary, DIM::MarkerEntryIndex) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_index', :label => 'Marker Index')
  link(:Details)
}

organizer(:Details, DIM::MarkerEntryIndex) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_index', :label => 'Marker Index')
  
}


# DIM::MarkerEntryRelTim ===================================
collection(:Summary, DIM::MarkerEntryRelTim) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_time', :label => 'Marker Time')
  view_ref(:Details)
}

organizer(:Summary, DIM::MarkerEntryRelTim) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_time', :label => 'Marker Time')
  link(:Details)
}

organizer(:Details, DIM::MarkerEntryRelTim) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_time', :label => 'Marker Time')
  
}


# DIM::MarkerEntrySaVal16 ==================================
collection(:Summary, DIM::MarkerEntrySaVal16) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  view_ref(:Details)
}

organizer(:Summary, DIM::MarkerEntrySaVal16) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  link(:Details)
}

organizer(:Details, DIM::MarkerEntrySaVal16) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
}


# DIM::MarkerEntrySaVal32 ==================================
collection(:Summary, DIM::MarkerEntrySaVal32) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  view_ref(:Details)
}

organizer(:Summary, DIM::MarkerEntrySaVal32) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  link(:Details)
}

organizer(:Details, DIM::MarkerEntrySaVal32) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
}


# DIM::MarkerEntrySaVal8 ===================================
collection(:Summary, DIM::MarkerEntrySaVal8) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  integer('unused', :label => 'Unused')
  view_ref(:Details)
}

organizer(:Summary, DIM::MarkerEntrySaVal8) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  integer('unused', :label => 'Unused')
  link(:Details)
}

organizer(:Details, DIM::MarkerEntrySaVal8) {
  integer('marker_id_dim', :label => 'Marker Id Dim')
  integer('marker_val', :label => 'Marker Val')
  integer('unused', :label => 'Unused')
}


# DIM::MdsCreateInfo =======================================
collection(:Summary, DIM::MdsCreateInfo) {
  view_ref(:Details)
}

organizer(:Summary, DIM::MdsCreateInfo) {
  link(:Details)
}

organizer(:Details, DIM::MdsCreateInfo) {
  view_ref(:Summary, 'attribute_list', :label => 'Attribute List')
  view_ref(:Summary, 'class_id_dim', :label => 'Class Id Dim')
}


# DIM::MdsErrorInfo ========================================
collection(:Summary, DIM::MdsErrorInfo) {
  integer('error_type', :label => 'Error Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::MdsErrorInfo) {
  integer('error_type', :label => 'Error Type')
  link(:Details)
}

organizer(:Details, DIM::MdsErrorInfo) {
  integer('error_type', :label => 'Error Type')
  view_ref(:Summary, 'error_info', :label => 'Error Info')
}


# DIM::MdsSetStateInvoke ===================================
collection(:Summary, DIM::MdsSetStateInvoke) {
  choice('new_state', mdsstatus, :label => 'New State')
  integer('authorization', :label => 'Authorization')
  view_ref(:Details)
}

organizer(:Summary, DIM::MdsSetStateInvoke) {
  choice('new_state', mdsstatus, :label => 'New State')
  integer('authorization', :label => 'Authorization')
  link(:Details)
}

organizer(:Details, DIM::MdsSetStateInvoke) {
  choice('new_state', mdsstatus, :label => 'New State')
  integer('authorization', :label => 'Authorization')
}


# DIM::Metric ==============================================
collection(:Summary, DIM::Metric) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Metric) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('measure_mode', :label => 'Measure Mode')
  integer('max_delay_time', :label => 'Max Delay Time')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Metric) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('measure_mode', :label => 'Measure Mode')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::MetricCalEntry ======================================
collection(:Summary, DIM::MetricCalEntry) {
  choice('cal_state', metriccalstate, :label => 'Cal State')
  choice('cal_type', metriccaltype, :label => 'Cal Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::MetricCalEntry) {
  choice('cal_state', metriccalstate, :label => 'Cal State')
  choice('cal_type', metriccaltype, :label => 'Cal Type')
  link(:Details)
}

organizer(:Details, DIM::MetricCalEntry) {
  choice('cal_state', metriccalstate, :label => 'Cal State')
  choice('cal_type', metriccaltype, :label => 'Cal Type')
  view_ref(:Summary, 'cal_time', :label => 'Cal Time')
  
}


# DIM::MetricMeasure =======================================
collection(:Summary, DIM::MetricMeasure) {
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::MetricMeasure) {
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  link(:Details)
}

organizer(:Details, DIM::MetricMeasure) {
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  
  
  
  
}


# DIM::MetricSpec ==========================================
collection(:Summary, DIM::MetricSpec) {
  choice('access', metricaccess, :label => 'Access')
  choice('category', metriccategory, :label => 'Category')
  choice('relevance', metricrelevance, :label => 'Relevance')
  integer('update_period', :label => 'Update Period')
  view_ref(:Details)
}

organizer(:Summary, DIM::MetricSpec) {
  choice('access', metricaccess, :label => 'Access')
  choice('category', metriccategory, :label => 'Category')
  choice('relevance', metricrelevance, :label => 'Relevance')
  integer('update_period', :label => 'Update Period')
  link(:Details)
}

organizer(:Details, DIM::MetricSpec) {
  choice('access', metricaccess, :label => 'Access')
  choice('category', metriccategory, :label => 'Category')
  choice('relevance', metricrelevance, :label => 'Relevance')
  integer('update_period', :label => 'Update Period')
  
  view_ref(:Summary, 'structure', :label => 'Structure')
}


# DIM::MetricStructure =====================================
collection(:Summary, DIM::MetricStructure) {
  choice('ms_struct', metricstructure_ms_struct_anon_enum, :label => 'Ms Struct')
  integer('ms_comp_no', :label => 'Ms Comp No')
  view_ref(:Details)
}

organizer(:Summary, DIM::MetricStructure) {
  choice('ms_struct', metricstructure_ms_struct_anon_enum, :label => 'Ms Struct')
  integer('ms_comp_no', :label => 'Ms Comp No')
  link(:Details)
}

organizer(:Details, DIM::MetricStructure) {
  choice('ms_struct', metricstructure_ms_struct_anon_enum, :label => 'Ms Struct')
  integer('ms_comp_no', :label => 'Ms Comp No')
  
}


# DIM::MibDataEntry ========================================
collection(:Summary, DIM::MibDataEntry) {
  integer('mib_id_dim', :label => 'Mib Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::MibDataEntry) {
  integer('mib_id_dim', :label => 'Mib Id Dim')
  link(:Details)
}

organizer(:Details, DIM::MibDataEntry) {
  integer('mib_id_dim', :label => 'Mib Id Dim')
  
  view_ref(:Summary, 'mib_attributes', :label => 'Mib Attributes')
}


# DIM::MibElement ==========================================
collection(:Summary, DIM::MibElement) {
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  view_ref(:Details)
}

organizer(:Summary, DIM::MibElement) {
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  link(:Details)
}

organizer(:Details, DIM::MibElement) {
  string('mib_ext_oid', :label => 'Mib Ext Oid')
  
}


# DIM::Multipatient_Archive ================================
collection(:Summary, DIM::Multipatient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('location', :label => 'Location')
  string('study_name', :label => 'Study Name')
  string('system_id_dim', :label => 'System Id Dim')
  string('version_reserved', :label => 'Version')
  view_ref(:Details)
}

organizer(:Summary, DIM::Multipatient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('location', :label => 'Location')
  string('study_name', :label => 'Study Name')
  string('system_id_dim', :label => 'System Id Dim')
  string('version_reserved', :label => 'Version')
  link(:Details)
}

organizer(:Details, DIM::Multipatient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('location', :label => 'Location')
  string('study_name', :label => 'Study Name')
  string('system_id_dim', :label => 'System Id Dim')
  string('version_reserved', :label => 'Version')
  view_ref(:Summary, 'patient_archive', :label => 'Patient Archive')
}


# Gui_Builder_Profile::MultivocabularySubstitution =========
collection(:Summary, Gui_Builder_Profile::MultivocabularySubstitution) {
  string('master_word', :label => 'Master Word')
  string('replacement_word', :label => 'Replacement Word')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::MultivocabularySubstitution) {
  string('master_word', :label => 'Master Word')
  string('replacement_word', :label => 'Replacement Word')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::MultivocabularySubstitution) {
  string('master_word', :label => 'Master Word')
  string('replacement_word', :label => 'Replacement Word')
  view_ref(:Summary, 'role', :label => 'Role')
}


# DIM::NomenclatureVersion =================================
collection(:Summary, DIM::NomenclatureVersion) {
  choice('nom_major_version', nomenclatureversion_nom_major_version_anon_enum, :label => 'Nom Major Version')
  integer('nom_minor_version', :label => 'Nom Minor Version')
  view_ref(:Details)
}

organizer(:Summary, DIM::NomenclatureVersion) {
  choice('nom_major_version', nomenclatureversion_nom_major_version_anon_enum, :label => 'Nom Major Version')
  integer('nom_minor_version', :label => 'Nom Minor Version')
  link(:Details)
}

organizer(:Details, DIM::NomenclatureVersion) {
  choice('nom_major_version', nomenclatureversion_nom_major_version_anon_enum, :label => 'Nom Major Version')
  integer('nom_minor_version', :label => 'Nom Minor Version')
  
}


# DIM::NuObsValue ==========================================
collection(:Summary, DIM::NuObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::NuObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  link(:Details)
}

organizer(:Details, DIM::NuObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('value', :label => 'Value')
  
  
}


# DIM::Numeric =============================================
collection(:Summary, DIM::Numeric) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('unit_code', :label => 'Unit Code')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('handle', :label => 'Handle')
  integer('nu_measure_resolution', :label => 'Nu Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('accuracy', :label => 'Accuracy')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Numeric) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('accuracy', :label => 'Accuracy')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('unit_code', :label => 'Unit Code')
  integer('nu_measure_resolution', :label => 'Nu Measure Resolution')
  integer('handle', :label => 'Handle')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Numeric) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('accuracy', :label => 'Accuracy')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('unit_code', :label => 'Unit Code')
  integer('nu_measure_resolution', :label => 'Nu Measure Resolution')
  integer('handle', :label => 'Handle')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('name_binding', :label => 'Name Binding')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'absolute_time_stamp', :label => 'Absolute Time Stamp')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_nu_observed_value', :label => 'Compound Nu Observed Value')
  view_ref(:Summary, 'display_resolution', :label => 'Display Resolution')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'nu_measure_range', :label => 'Nu Measure Range')
  view_ref(:Summary, 'nu_observed_value', :label => 'Nu Observed Value')
  view_ref(:Summary, 'nu_physiological_range', :label => 'Nu Physiological Range')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::ObjCreateInfo =======================================
collection(:Summary, DIM::ObjCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::ObjCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::ObjCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'scan_report_info', :label => 'Scan Report Info')
}


# DIM::ObjDeleteInfo =======================================
collection(:Summary, DIM::ObjDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::ObjDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::ObjDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'object_list', :label => 'Object List')
}


# DIM::ObservationScan =====================================
collection(:Summary, DIM::ObservationScan) {
  integer('obj_handle', :label => 'Obj Handle')
  view_ref(:Details)
}

organizer(:Summary, DIM::ObservationScan) {
  integer('obj_handle', :label => 'Obj Handle')
  link(:Details)
}

organizer(:Details, DIM::ObservationScan) {
  integer('obj_handle', :label => 'Obj Handle')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
}


# DIM::OpAttributeInfo =====================================
collection(:Summary, DIM::OpAttributeInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpAttributeInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::OpAttributeInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'glb_scan_info', :label => 'Glb Scan Info')
}


# DIM::OpAttributeScan =====================================
collection(:Summary, DIM::OpAttributeScan) {
  choice('lock_state', administrativestate, :label => 'Lock State')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('sco_handle', :label => 'Sco Handle')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpAttributeScan) {
  choice('lock_state', administrativestate, :label => 'Lock State')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('sco_handle', :label => 'Sco Handle')
  link(:Details)
}

organizer(:Details, DIM::OpAttributeScan) {
  choice('lock_state', administrativestate, :label => 'Lock State')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('sco_handle', :label => 'Sco Handle')
  
  view_ref(:Summary, 'op_elem_updt_list', :label => 'Op Elem Updt List')
}


# DIM::OpCreateEntry =======================================
collection(:Summary, DIM::OpCreateEntry) {
  view_ref(:Details)
}

organizer(:Summary, DIM::OpCreateEntry) {
  link(:Details)
}

organizer(:Details, DIM::OpCreateEntry) {
  view_ref(:Summary, 'created_op_list', :label => 'Created Op List')
  
  view_ref(:Summary, 'sco_glb_handle', :label => 'Sco Glb Handle')
}


# DIM::OpCreateInfo ========================================
collection(:Summary, DIM::OpCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::OpCreateInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'scan_info', :label => 'Scan Info')
}


# DIM::OpDeleteEntry =======================================
collection(:Summary, DIM::OpDeleteEntry) {
  view_ref(:Details)
}

organizer(:Summary, DIM::OpDeleteEntry) {
  link(:Details)
}

organizer(:Details, DIM::OpDeleteEntry) {
  view_ref(:Summary, 'deleted_op_list', :label => 'Deleted Op List')
  
  view_ref(:Summary, 'sco_glb_handle', :label => 'Sco Glb Handle')
}


# DIM::OpDeleteInfo ========================================
collection(:Summary, DIM::OpDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::OpDeleteInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'deleted_op_list', :label => 'Deleted Op List')
}


# DIM::OpElem ==============================================
collection(:Summary, DIM::OpElem) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpElem) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  link(:Details)
}

organizer(:Details, DIM::OpElem) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  
}


# DIM::OpElemAttr ==========================================
collection(:Summary, DIM::OpElemAttr) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpElemAttr) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  link(:Details)
}

organizer(:Details, DIM::OpElemAttr) {
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
  
}


# DIM::OpGrouping ==========================================
collection(:Summary, DIM::OpGrouping) {
  integer('group', :label => 'Group')
  integer('priority', :label => 'Priority')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpGrouping) {
  integer('group', :label => 'Group')
  integer('priority', :label => 'Priority')
  link(:Details)
}

organizer(:Details, DIM::OpGrouping) {
  integer('group', :label => 'Group')
  integer('priority', :label => 'Priority')
  
}


# DIM::OpInvokeElement =====================================
collection(:Summary, DIM::OpInvokeElement) {
  choice('op_mod_type', opmodtype, :label => 'Op Mod Type')
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpInvokeElement) {
  choice('op_mod_type', opmodtype, :label => 'Op Mod Type')
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  link(:Details)
}

organizer(:Details, DIM::OpInvokeElement) {
  choice('op_mod_type', opmodtype, :label => 'Op Mod Type')
  integer('op_class_id_dim', :label => 'Op Class Id Dim')
  integer('op_instance_no', :label => 'Op Instance No')
  view_ref(:Summary, 'attributes', :label => 'Attributes')
  
}


# DIM::OpSetValueRange =====================================
collection(:Summary, DIM::OpSetValueRange) {
  integer('maximum', :label => 'Maximum')
  integer('minimum', :label => 'Minimum')
  integer('resolution', :label => 'Resolution')
  view_ref(:Details)
}

organizer(:Summary, DIM::OpSetValueRange) {
  integer('maximum', :label => 'Maximum')
  integer('minimum', :label => 'Minimum')
  integer('resolution', :label => 'Resolution')
  link(:Details)
}

organizer(:Details, DIM::OpSetValueRange) {
  integer('maximum', :label => 'Maximum')
  integer('minimum', :label => 'Minimum')
  integer('resolution', :label => 'Resolution')
  
  
  
}


# DIM::OperSpec ============================================
collection(:Summary, DIM::OperSpec) {
  choice('level', oplevel, :label => 'Level')
  choice('options', opoptions, :label => 'Options')
  integer('op_target', :label => 'Op Target')
  integer('vattr_id_dim', :label => 'Vattr Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::OperSpec) {
  choice('level', oplevel, :label => 'Level')
  choice('options', opoptions, :label => 'Options')
  integer('op_target', :label => 'Op Target')
  integer('vattr_id_dim', :label => 'Vattr Id Dim')
  link(:Details)
}

organizer(:Details, DIM::OperSpec) {
  choice('level', oplevel, :label => 'Level')
  choice('options', opoptions, :label => 'Options')
  integer('op_target', :label => 'Op Target')
  integer('vattr_id_dim', :label => 'Vattr Id Dim')
  view_ref(:Summary, 'grouping', :label => 'Grouping')
  
}


# DIM::OperTextStrings =====================================
collection(:Summary, DIM::OperTextStrings) {
  string('confirm', :label => 'Confirm')
  string('help', :label => 'Help')
  string('label', :label => 'Label')
  view_ref(:Details)
}

organizer(:Summary, DIM::OperTextStrings) {
  string('confirm', :label => 'Confirm')
  string('help', :label => 'Help')
  string('label', :label => 'Label')
  link(:Details)
}

organizer(:Details, DIM::OperTextStrings) {
  string('confirm', :label => 'Confirm')
  string('help', :label => 'Help')
  string('label', :label => 'Label')
  
  
}


# DIM::Operating_Scanner ===================================
collection(:Summary, DIM::Operating_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Operating_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Operating_Scanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
}


# DIM::Operation ===========================================
collection(:Summary, DIM::Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
}


# DIM::OperationInvoke =====================================
collection(:Summary, DIM::OperationInvoke) {
  integer('checksum', :label => 'Checksum')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  view_ref(:Details)
}

organizer(:Summary, DIM::OperationInvoke) {
  integer('checksum', :label => 'Checksum')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  link(:Details)
}

organizer(:Details, DIM::OperationInvoke) {
  integer('checksum', :label => 'Checksum')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  view_ref(:Summary, 'op_elem_list', :label => 'Op Elem List')
}


# DIM::OperationInvokeResult ===============================
collection(:Summary, DIM::OperationInvokeResult) {
  choice('result', opinvresult, :label => 'Result')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  view_ref(:Details)
}

organizer(:Summary, DIM::OperationInvokeResult) {
  choice('result', opinvresult, :label => 'Result')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  link(:Details)
}

organizer(:Details, DIM::OperationInvokeResult) {
  choice('result', opinvresult, :label => 'Result')
  integer('invoke_cookie', :label => 'Invoke Cookie')
}


# DIM::PM_Segment ==========================================
collection(:Summary, DIM::PM_Segment) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('segment_usage_count', :label => 'Segment Usage Count')
  view_ref(:Details)
}

organizer(:Summary, DIM::PM_Segment) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('segment_usage_count', :label => 'Segment Usage Count')
  link(:Details)
}

organizer(:Details, DIM::PM_Segment) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('name_binding', :label => 'Name Binding')
  integer('segment_usage_count', :label => 'Segment Usage Count')
  
  
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'segment_end_abs_time', :label => 'Segment End Abs Time')
  view_ref(:Summary, 'segment_start_abs_time', :label => 'Segment Start Abs Time')
  view_ref(:Summary, 'vmo_global_reference', :label => 'Vmo Global Reference')
}


# DIM::PM_Store ============================================
collection(:Summary, DIM::PM_Store) {
  choice('storage_format', storageformat, :label => 'Storage Format')
  choice('store_sample_algorithm', stosamplealg, :label => 'Store Sample Algorithm')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('metric_class', :label => 'Metric Class')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_segments', :label => 'Number Of Segments')
  integer('sample_period', :label => 'Sample Period')
  integer('store_capacity_count', :label => 'Store Capacity Count')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('store_usage_count', :label => 'Store Usage Count')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::PM_Store) {
  choice('storage_format', storageformat, :label => 'Storage Format')
  choice('store_sample_algorithm', stosamplealg, :label => 'Store Sample Algorithm')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('metric_class', :label => 'Metric Class')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_segments', :label => 'Number Of Segments')
  integer('sample_period', :label => 'Sample Period')
  integer('store_capacity_count', :label => 'Store Capacity Count')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('store_usage_count', :label => 'Store Usage Count')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::PM_Store) {
  choice('storage_format', storageformat, :label => 'Storage Format')
  choice('store_sample_algorithm', stosamplealg, :label => 'Store Sample Algorithm')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('metric_class', :label => 'Metric Class')
  integer('name_binding', :label => 'Name Binding')
  integer('number_of_segments', :label => 'Number Of Segments')
  integer('sample_period', :label => 'Sample Period')
  integer('store_capacity_count', :label => 'Store Capacity Count')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('store_usage_count', :label => 'Store Usage Count')
  string('type', :label => 'Type')
  
  
  
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
}


# DIM::PatMeasure ==========================================
collection(:Summary, DIM::PatMeasure) {
  integer('m_unit', :label => 'M Unit')
  integer('value', :label => 'Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::PatMeasure) {
  integer('m_unit', :label => 'M Unit')
  integer('value', :label => 'Value')
  link(:Details)
}

organizer(:Details, DIM::PatMeasure) {
  integer('m_unit', :label => 'M Unit')
  integer('value', :label => 'Value')
  
  
  
  
  
  
  
  
  
}


# DIM::Patient_Archive =====================================
collection(:Summary, DIM::Patient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('processing_history', :label => 'Processing History')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_name', :label => 'System Name')
  view_ref(:Details)
}

organizer(:Summary, DIM::Patient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('processing_history', :label => 'Processing History')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_name', :label => 'System Name')
  link(:Details)
}

organizer(:Details, DIM::Patient_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('processing_history', :label => 'Processing History')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_name', :label => 'System Name')
  
  
  view_ref(:Summary, 'protection', :label => 'Protection')
  view_ref(:Summary, 'session_archive', :label => 'Session Archive')
}


# DIM::Patient_Demographics ================================
collection(:Summary, DIM::Patient_Demographics) {
  string('name', :label => 'Name')
  choice('pat_demo_state', patdemostate, :label => 'Pat Demo State')
  choice('sex', patientsex, :label => 'Sex')
  choice('race', patientrace, :label => 'Race')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  string('given_name', :label => 'Given Name')
  string('middle_name', :label => 'Middle Name')
  string('mother_name', :label => 'Mother Name')
  string('mother_patient_id_dim', :label => 'Mother Patient Id Dim')
  string('patient_gen_info', :label => 'Patient Gen Info')
  string('patient_id_dim', :label => 'Patient Id Dim')
  string('procedure_description', :label => 'Procedure Description')
  string('surgeon', :label => 'Surgeon')
  string('title_name', :label => 'Title Name')
  string('family_name', :label => 'Family Name')
  string('diagnostic_info', :label => 'Diagnostic Info')
  string('birth_name', :label => 'Birth Name')
  string('bed_id_dim', :label => 'Bed Id Dim')
  string('attending_physician', :label => 'Attending Physician')
  string('anaesthetist', :label => 'Anaesthetist')
  string('admitting_physician', :label => 'Admitting Physician')
  view_ref(:Details)
}

organizer(:Summary, DIM::Patient_Demographics) {
  string('name', :label => 'Name')
  choice('pat_demo_state', patdemostate, :label => 'Pat Demo State')
  choice('sex', patientsex, :label => 'Sex')
  choice('race', patientrace, :label => 'Race')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  string('attending_physician', :label => 'Attending Physician')
  string('anaesthetist', :label => 'Anaesthetist')
  string('admitting_physician', :label => 'Admitting Physician')
  string('patient_id_dim', :label => 'Patient Id Dim')
  string('patient_gen_info', :label => 'Patient Gen Info')
  string('mother_patient_id_dim', :label => 'Mother Patient Id Dim')
  string('mother_name', :label => 'Mother Name')
  string('middle_name', :label => 'Middle Name')
  string('given_name', :label => 'Given Name')
  string('bed_id_dim', :label => 'Bed Id Dim')
  string('birth_name', :label => 'Birth Name')
  string('diagnostic_info', :label => 'Diagnostic Info')
  string('family_name', :label => 'Family Name')
  string('title_name', :label => 'Title Name')
  string('surgeon', :label => 'Surgeon')
  string('procedure_description', :label => 'Procedure Description')
  link(:Details)
}

organizer(:Details, DIM::Patient_Demographics) {
  string('name', :label => 'Name')
  choice('race', patientrace, :label => 'Race')
  choice('sex', patientsex, :label => 'Sex')
  choice('pat_demo_state', patdemostate, :label => 'Pat Demo State')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('class_reserved', :label => 'Class')
  string('mother_name', :label => 'Mother Name')
  string('middle_name', :label => 'Middle Name')
  string('given_name', :label => 'Given Name')
  string('bed_id_dim', :label => 'Bed Id Dim')
  string('birth_name', :label => 'Birth Name')
  string('diagnostic_info', :label => 'Diagnostic Info')
  string('family_name', :label => 'Family Name')
  string('title_name', :label => 'Title Name')
  string('surgeon', :label => 'Surgeon')
  string('procedure_description', :label => 'Procedure Description')
  string('mother_patient_id_dim', :label => 'Mother Patient Id Dim')
  string('patient_gen_info', :label => 'Patient Gen Info')
  string('patient_id_dim', :label => 'Patient Id Dim')
  string('admitting_physician', :label => 'Admitting Physician')
  string('anaesthetist', :label => 'Anaesthetist')
  string('attending_physician', :label => 'Attending Physician')
  view_ref(:Summary, 'date_of_birth', :label => 'Date Of Birth')
  view_ref(:Summary, 'date_of_procedure', :label => 'Date Of Procedure')
  view_ref(:Summary, 'diagnostic_codes', :label => 'Diagnostic Codes')
  view_ref(:Summary, 'gestational_age', :label => 'Gestational Age')
  view_ref(:Summary, 'mds', :label => 'M DS')
  view_ref(:Summary, 'patient_age', :label => 'Patient Age')
  view_ref(:Summary, 'patient_archive', :label => 'Patient Archive')
  view_ref(:Summary, 'patient_birth_length', :label => 'Patient Birth Length')
  view_ref(:Summary, 'patient_birth_weight', :label => 'Patient Birth Weight')
  view_ref(:Summary, 'patient_bsa', :label => 'Patient Bsa')
  view_ref(:Summary, 'patient_head_circumference', :label => 'Patient Head Circumference')
  view_ref(:Summary, 'patient_height', :label => 'Patient Height')
  view_ref(:Summary, 'patient_lbm', :label => 'Patient Lbm')
  view_ref(:Summary, 'patient_weight', :label => 'Patient Weight')
  view_ref(:Summary, 'procedure_codes', :label => 'Procedure Codes')
  view_ref(:Summary, 'session_archive', :label => 'Session Archive')
}


# DIM::PeriCfgScanner ======================================
collection(:Summary, DIM::PeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::PeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::PeriCfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  choice('scan_config_limit', scanconfiglimit, :label => 'Scan Config Limit')
  choice('scan_extensibility', scanextend, :label => 'Scan Extensibility')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('reporting_interval', :label => 'Reporting Interval')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'scan_list', :label => 'Scan List')
}


# DIM::Physician ===========================================
collection(:Summary, DIM::Physician) {
  string('name', :label => 'Name')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('family_name', :label => 'Family Name')
  string('given_name', :label => 'Given Name')
  string('middle_name', :label => 'Middle Name')
  string('physician_id_dim', :label => 'Physician Id Dim')
  string('title_name', :label => 'Title Name')
  view_ref(:Details)
}

organizer(:Summary, DIM::Physician) {
  string('name', :label => 'Name')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('family_name', :label => 'Family Name')
  string('given_name', :label => 'Given Name')
  string('middle_name', :label => 'Middle Name')
  string('physician_id_dim', :label => 'Physician Id Dim')
  string('title_name', :label => 'Title Name')
  link(:Details)
}

organizer(:Details, DIM::Physician) {
  string('name', :label => 'Name')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('family_name', :label => 'Family Name')
  string('given_name', :label => 'Given Name')
  string('middle_name', :label => 'Middle Name')
  string('physician_id_dim', :label => 'Physician Id Dim')
  string('title_name', :label => 'Title Name')
  view_ref(:Summary, 'authorization_level', :label => 'Authorization Level')
  view_ref(:Summary, 'session_archive', :label => 'Session Archive')
}


# DIM::ProdSpecEntry =======================================
collection(:Summary, DIM::ProdSpecEntry) {
  choice('spec_type', prodspecentry_spec_type_anon_enum, :label => 'Spec Type')
  integer('component_id_dim', :label => 'Component Id Dim')
  string('prod_spec', :label => 'Prod Spec')
  view_ref(:Details)
}

organizer(:Summary, DIM::ProdSpecEntry) {
  choice('spec_type', prodspecentry_spec_type_anon_enum, :label => 'Spec Type')
  integer('component_id_dim', :label => 'Component Id Dim')
  string('prod_spec', :label => 'Prod Spec')
  link(:Details)
}

organizer(:Details, DIM::ProdSpecEntry) {
  choice('spec_type', prodspecentry_spec_type_anon_enum, :label => 'Spec Type')
  integer('component_id_dim', :label => 'Component Id Dim')
  string('prod_spec', :label => 'Prod Spec')
  
  
  
}


# DIM::RangeOpText =========================================
collection(:Summary, DIM::RangeOpText) {
  string('high_text', :label => 'High Text')
  string('low_text', :label => 'Low Text')
  view_ref(:Details)
}

organizer(:Summary, DIM::RangeOpText) {
  string('high_text', :label => 'High Text')
  string('low_text', :label => 'Low Text')
  link(:Details)
}

organizer(:Details, DIM::RangeOpText) {
  string('high_text', :label => 'High Text')
  string('low_text', :label => 'Low Text')
  
}


# DIM::Real_Time_Sample_Array ==============================
collection(:Summary, DIM::Real_Time_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('unit_code', :label => 'Unit Code')
  integer('compression', :label => 'Compression')
  integer('sample_time_sync', :label => 'Sample Time Sync')
  integer('sample_period', :label => 'Sample Period')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('average_reporting_delay', :label => 'Average Reporting Delay')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_sample_time_sync', :label => 'Hi Res Sample Time Sync')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Real_Time_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('average_reporting_delay', :label => 'Average Reporting Delay')
  integer('compression', :label => 'Compression')
  integer('unit_code', :label => 'Unit Code')
  integer('sample_period', :label => 'Sample Period')
  integer('sample_time_sync', :label => 'Sample Time Sync')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_sample_time_sync', :label => 'Hi Res Sample Time Sync')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Real_Time_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('class_reserved', :label => 'Class')
  integer('average_reporting_delay', :label => 'Average Reporting Delay')
  integer('compression', :label => 'Compression')
  integer('unit_code', :label => 'Unit Code')
  integer('sample_period', :label => 'Sample Period')
  integer('sample_time_sync', :label => 'Sample Time Sync')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('handle', :label => 'Handle')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_sample_time_sync', :label => 'Hi Res Sample Time Sync')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_sa_observed_value', :label => 'Compound Sa Observed Value')
  view_ref(:Summary, 'filter_specification', :label => 'Filter Specification')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'sa_observed_value', :label => 'Sa Observed Value')
  view_ref(:Summary, 'sa_signal_frequency', :label => 'Sa Signal Frequency')
  view_ref(:Summary, 'sa_specification', :label => 'Sa Specification')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'sweep_speed', :label => 'Sweep Speed')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::RefreshObjEntry =====================================
collection(:Summary, DIM::RefreshObjEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  view_ref(:Details)
}

organizer(:Summary, DIM::RefreshObjEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  link(:Details)
}

organizer(:Details, DIM::RefreshObjEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  view_ref(:Summary, 'object_glb_handle', :label => 'Object Glb Handle')
}


# Gui_Builder_Profile::RichText ============================
collection(:Summary, Gui_Builder_Profile::RichText) {
  string('content', :label => 'Content')
  string('markup_language', :label => 'Markup Language')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::RichText) {
  string('content', :label => 'Content')
  string('markup_language', :label => 'Markup Language')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::RichText) {
  string('content', :label => 'Content')
  string('markup_language', :label => 'Markup Language')
}


# DIM::RtsaObservationScan =================================
collection(:Summary, DIM::RtsaObservationScan) {
  integer('handle', :label => 'Handle')
  view_ref(:Details)
}

organizer(:Summary, DIM::RtsaObservationScan) {
  integer('handle', :label => 'Handle')
  link(:Details)
}

organizer(:Details, DIM::RtsaObservationScan) {
  integer('handle', :label => 'Handle')
  
  view_ref(:Summary, 'observation', :label => 'Observation')
}


# DIM::SCO =================================================
collection(:Summary, DIM::SCO) {
  choice('activity_indicator', scoactivityindicator, :label => 'Activity Indicator')
  choice('lock_state', administrativestate, :label => 'Lock State')
  choice('sco_capability', scocapability, :label => 'Sco Capability')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('sco_help_text_string', :label => 'Sco Help Text String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::SCO) {
  choice('activity_indicator', scoactivityindicator, :label => 'Activity Indicator')
  choice('lock_state', administrativestate, :label => 'Lock State')
  choice('sco_capability', scocapability, :label => 'Sco Capability')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('sco_help_text_string', :label => 'Sco Help Text String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::SCO) {
  choice('activity_indicator', scoactivityindicator, :label => 'Activity Indicator')
  choice('lock_state', administrativestate, :label => 'Lock State')
  choice('sco_capability', scocapability, :label => 'Sco Capability')
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('sco_help_text_string', :label => 'Sco Help Text String')
  string('type', :label => 'Type')
  
  
  view_ref(:Summary, 'operation', :label => 'Operation')
}


# DIM::SNTPTimeStamp =======================================
collection(:Summary, DIM::SNTPTimeStamp) {
  integer('fraction', :label => 'Fraction')
  integer('seconds', :label => 'Seconds')
  view_ref(:Details)
}

organizer(:Summary, DIM::SNTPTimeStamp) {
  integer('fraction', :label => 'Fraction')
  integer('seconds', :label => 'Seconds')
  link(:Details)
}

organizer(:Details, DIM::SNTPTimeStamp) {
  integer('fraction', :label => 'Fraction')
  integer('seconds', :label => 'Seconds')
}


# DIM::SaCalData16 =========================================
collection(:Summary, DIM::SaCalData16) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaCalData16) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaCalData16) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::SaCalData32 =========================================
collection(:Summary, DIM::SaCalData32) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaCalData32) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaCalData32) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::SaCalData8 ==========================================
collection(:Summary, DIM::SaCalData8) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaCalData8) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaCalData8) {
  choice('cal_type', sacaldatatype, :label => 'Cal Type')
  integer('increment', :label => 'Increment')
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::SaFilterEntry =======================================
collection(:Summary, DIM::SaFilterEntry) {
  choice('filter_type', safilterentry_filter_type_anon_enum, :label => 'Filter Type')
  integer('filter_order', :label => 'Filter Order')
  integer('frequency', :label => 'Frequency')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaFilterEntry) {
  choice('filter_type', safilterentry_filter_type_anon_enum, :label => 'Filter Type')
  integer('filter_order', :label => 'Filter Order')
  integer('frequency', :label => 'Frequency')
  link(:Details)
}

organizer(:Details, DIM::SaFilterEntry) {
  choice('filter_type', safilterentry_filter_type_anon_enum, :label => 'Filter Type')
  integer('filter_order', :label => 'Filter Order')
  integer('frequency', :label => 'Frequency')
  
}


# DIM::SaGridEntry16 =======================================
collection(:Summary, DIM::SaGridEntry16) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaGridEntry16) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaGridEntry16) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
}


# DIM::SaGridEntry32 =======================================
collection(:Summary, DIM::SaGridEntry32) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaGridEntry32) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaGridEntry32) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
}


# DIM::SaGridEntry8 ========================================
collection(:Summary, DIM::SaGridEntry8) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaGridEntry8) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::SaGridEntry8) {
  integer('absolute_value', :label => 'Absolute Value')
  integer('level', :label => 'Level')
  integer('scaled_value', :label => 'Scaled Value')
}


# DIM::SaObsValue ==========================================
collection(:Summary, DIM::SaObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  string('array', :label => 'Array')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  string('array', :label => 'Array')
  link(:Details)
}

organizer(:Details, DIM::SaObsValue) {
  choice('state', measurementstatus, :label => 'State')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  string('array', :label => 'Array')
  
  
  
}


# DIM::SaSignalFrequency ===================================
collection(:Summary, DIM::SaSignalFrequency) {
  integer('high_edge_freq', :label => 'High Edge Freq')
  integer('low_edge_freq', :label => 'Low Edge Freq')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaSignalFrequency) {
  integer('high_edge_freq', :label => 'High Edge Freq')
  integer('low_edge_freq', :label => 'Low Edge Freq')
  link(:Details)
}

organizer(:Details, DIM::SaSignalFrequency) {
  integer('high_edge_freq', :label => 'High Edge Freq')
  integer('low_edge_freq', :label => 'Low Edge Freq')
  
}


# DIM::SaSpec ==============================================
collection(:Summary, DIM::SaSpec) {
  choice('flags', saflags, :label => 'Flags')
  integer('array_size', :label => 'Array Size')
  view_ref(:Details)
}

organizer(:Summary, DIM::SaSpec) {
  choice('flags', saflags, :label => 'Flags')
  integer('array_size', :label => 'Array Size')
  link(:Details)
}

organizer(:Details, DIM::SaSpec) {
  choice('flags', saflags, :label => 'Flags')
  integer('array_size', :label => 'Array Size')
  
  view_ref(:Summary, 'sample_type', :label => 'Sample Type')
}


# DIM::SampleType ==========================================
collection(:Summary, DIM::SampleType) {
  integer('sample_size', :label => 'Sample Size')
  integer('significant_bits', :label => 'Significant Bits')
  view_ref(:Details)
}

organizer(:Summary, DIM::SampleType) {
  integer('sample_size', :label => 'Sample Size')
  integer('significant_bits', :label => 'Significant Bits')
  link(:Details)
}

organizer(:Details, DIM::SampleType) {
  integer('sample_size', :label => 'Sample Size')
  integer('significant_bits', :label => 'Significant Bits')
  
}


# DIM::Sample_Array ========================================
collection(:Summary, DIM::Sample_Array) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('compression', :label => 'Compression')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Sample_Array) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('measure_mode', :label => 'Measure Mode')
  integer('max_delay_time', :label => 'Max Delay Time')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_sa_observed_value', :label => 'Compound Sa Observed Value')
  view_ref(:Summary, 'filter_specification', :label => 'Filter Specification')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'sa_observed_value', :label => 'Sa Observed Value')
  view_ref(:Summary, 'sa_signal_frequency', :label => 'Sa Signal Frequency')
  view_ref(:Summary, 'sa_specification', :label => 'Sa Specification')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::ScaleRangeSpec16 ====================================
collection(:Summary, DIM::ScaleRangeSpec16) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaleRangeSpec16) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaleRangeSpec16) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScaleRangeSpec32 ====================================
collection(:Summary, DIM::ScaleRangeSpec32) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaleRangeSpec32) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaleRangeSpec32) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScaleRangeSpec8 =====================================
collection(:Summary, DIM::ScaleRangeSpec8) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaleRangeSpec8) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaleRangeSpec8) {
  integer('lower_absolute_value', :label => 'Lower Absolute Value')
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_absolute_value', :label => 'Upper Absolute Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScaledRange16 =======================================
collection(:Summary, DIM::ScaledRange16) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaledRange16) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaledRange16) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScaledRange32 =======================================
collection(:Summary, DIM::ScaledRange32) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaledRange32) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaledRange32) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScaledRange8 ========================================
collection(:Summary, DIM::ScaledRange8) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScaledRange8) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
  link(:Details)
}

organizer(:Details, DIM::ScaledRange8) {
  integer('lower_scaled_value', :label => 'Lower Scaled Value')
  integer('upper_scaled_value', :label => 'Upper Scaled Value')
}


# DIM::ScanEntry ===========================================
collection(:Summary, DIM::ScanEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScanEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  link(:Details)
}

organizer(:Details, DIM::ScanEntry) {
  integer('scanned_attribute', :label => 'Scanned Attribute')
  
  view_ref(:Summary, 'object_glb_handle', :label => 'Object Glb Handle')
}


# DIM::ScanReportInfo ======================================
collection(:Summary, DIM::ScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  link(:Details)
}

organizer(:Details, DIM::ScanReportInfo) {
  integer('scan_report_no', :label => 'Scan Report No')
  view_ref(:Summary, 'glb_scan_info', :label => 'Glb Scan Info')
}


# DIM::Scanner =============================================
collection(:Summary, DIM::Scanner) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Scanner) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Scanner) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  string('operational_state', :label => 'Operational State')
  
}


# DIM::ScoOperInvokeError ==================================
collection(:Summary, DIM::ScoOperInvokeError) {
  choice('op_error', scooperinvokeerror_op_error_anon_enum, :label => 'Op Error')
  integer('failed_operation_list', :label => 'Failed Operation List')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScoOperInvokeError) {
  choice('op_error', scooperinvokeerror_op_error_anon_enum, :label => 'Op Error')
  integer('failed_operation_list', :label => 'Failed Operation List')
  integer('invoke_cookie', :label => 'Invoke Cookie')
  link(:Details)
}

organizer(:Details, DIM::ScoOperInvokeError) {
  choice('op_error', scooperinvokeerror_op_error_anon_enum, :label => 'Op Error')
  integer('failed_operation_list', :label => 'Failed Operation List')
  integer('invoke_cookie', :label => 'Invoke Cookie')
}


# DIM::ScoOperReqSpec ======================================
collection(:Summary, DIM::ScoOperReqSpec) {
  integer('op_req_id_dim', :label => 'Op Req Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::ScoOperReqSpec) {
  integer('op_req_id_dim', :label => 'Op Req Id Dim')
  link(:Details)
}

organizer(:Details, DIM::ScoOperReqSpec) {
  integer('op_req_id_dim', :label => 'Op Req Id Dim')
  view_ref(:Summary, 'op_req_info', :label => 'Op Req Info')
}


# DIM::SegmentAttr =========================================
collection(:Summary, DIM::SegmentAttr) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  view_ref(:Details)
}

organizer(:Summary, DIM::SegmentAttr) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  link(:Details)
}

organizer(:Details, DIM::SegmentAttr) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  view_ref(:Summary, 'seg_attr', :label => 'Seg Attr')
}


# DIM::SegmentInfo =========================================
collection(:Summary, DIM::SegmentInfo) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  view_ref(:Details)
}

organizer(:Summary, DIM::SegmentInfo) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  link(:Details)
}

organizer(:Details, DIM::SegmentInfo) {
  integer('seg_inst_no', :label => 'Seg Inst No')
  view_ref(:Summary, 'seg_info', :label => 'Seg Info')
}


# DIM::SelectUValueEntry ===================================
collection(:Summary, DIM::SelectUValueEntry) {
  integer('m_units', :label => 'M Units')
  integer('value', :label => 'Value')
  view_ref(:Details)
}

organizer(:Summary, DIM::SelectUValueEntry) {
  integer('m_units', :label => 'M Units')
  integer('value', :label => 'Value')
  link(:Details)
}

organizer(:Details, DIM::SelectUValueEntry) {
  integer('m_units', :label => 'M Units')
  integer('value', :label => 'Value')
}


# DIM::Select_Item_Operation ===============================
collection(:Summary, DIM::Select_Item_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('selected_item_index', :label => 'Selected Item Index')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('nom_partition', :label => 'Nom Partition')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Select_Item_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('selected_item_index', :label => 'Selected Item Index')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('nom_partition', :label => 'Nom Partition')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Select_Item_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('selected_item_index', :label => 'Selected Item Index')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('nom_partition', :label => 'Nom Partition')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
}


# DIM::Session_Archive =====================================
collection(:Summary, DIM::Session_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('s_archive_comments', :label => 'S Archive Comments')
  string('s_archive_id_dim', :label => 'S Archive Id Dim')
  string('s_archive_name', :label => 'S Archive Name')
  view_ref(:Details)
}

organizer(:Summary, DIM::Session_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('s_archive_comments', :label => 'S Archive Comments')
  string('s_archive_id_dim', :label => 'S Archive Id Dim')
  string('s_archive_name', :label => 'S Archive Name')
  link(:Details)
}

organizer(:Details, DIM::Session_Archive) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('s_archive_comments', :label => 'S Archive Comments')
  string('s_archive_id_dim', :label => 'S Archive Id Dim')
  string('s_archive_name', :label => 'S Archive Name')
  
  
  
  view_ref(:Summary, 'protection', :label => 'Protection')
  view_ref(:Summary, 'session_notes', :label => 'Session Notes')
  view_ref(:Summary, 'session_test', :label => 'Session Test')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
}


# DIM::Session_Notes =======================================
collection(:Summary, DIM::Session_Notes) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('diagnosis_description', :label => 'Diagnosis Description')
  string('findings', :label => 'Findings')
  string('procedure_description', :label => 'Procedure Description')
  string('sn_comments', :label => 'Sn Comments')
  string('sn_id_dim', :label => 'Sn Id Dim')
  string('sn_name', :label => 'Sn Name')
  view_ref(:Details)
}

organizer(:Summary, DIM::Session_Notes) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('diagnosis_description', :label => 'Diagnosis Description')
  string('findings', :label => 'Findings')
  string('procedure_description', :label => 'Procedure Description')
  string('sn_comments', :label => 'Sn Comments')
  string('sn_id_dim', :label => 'Sn Id Dim')
  string('sn_name', :label => 'Sn Name')
  link(:Details)
}

organizer(:Details, DIM::Session_Notes) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('diagnosis_description', :label => 'Diagnosis Description')
  string('findings', :label => 'Findings')
  string('procedure_description', :label => 'Procedure Description')
  string('sn_comments', :label => 'Sn Comments')
  string('sn_id_dim', :label => 'Sn Id Dim')
  string('sn_name', :label => 'Sn Name')
  view_ref(:Summary, 'diagnostic_codes', :label => 'Diagnostic Codes')
  
  view_ref(:Summary, 'procedure_code', :label => 'Procedure Code')
  view_ref(:Summary, 'protection', :label => 'Protection')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
}


# DIM::Session_Test ========================================
collection(:Summary, DIM::Session_Test) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('st_archive_comments', :label => 'St Archive Comments')
  string('st_archive_id_dim', :label => 'St Archive Id Dim')
  string('st_archive_name', :label => 'St Archive Name')
  view_ref(:Details)
}

organizer(:Summary, DIM::Session_Test) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('st_archive_comments', :label => 'St Archive Comments')
  string('st_archive_id_dim', :label => 'St Archive Id Dim')
  string('st_archive_name', :label => 'St Archive Name')
  link(:Details)
}

organizer(:Details, DIM::Session_Test) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('st_archive_comments', :label => 'St Archive Comments')
  string('st_archive_id_dim', :label => 'St Archive Id Dim')
  string('st_archive_name', :label => 'St Archive Name')
  
  view_ref(:Summary, 'mds', :label => 'M DS')
  view_ref(:Summary, 'pm_store', :label => 'P M Store')
  view_ref(:Summary, 'protection', :label => 'Protection')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
}


# DIM::SetLeapSecondsInvoke ================================
collection(:Summary, DIM::SetLeapSecondsInvoke) {
  integer('leap_seconds_cum', :label => 'Leap Seconds Cum')
  view_ref(:Details)
}

organizer(:Summary, DIM::SetLeapSecondsInvoke) {
  integer('leap_seconds_cum', :label => 'Leap Seconds Cum')
  link(:Details)
}

organizer(:Details, DIM::SetLeapSecondsInvoke) {
  integer('leap_seconds_cum', :label => 'Leap Seconds Cum')
  view_ref(:Summary, 'next_leap_seconds', :label => 'Next Leap Seconds')
}


# DIM::SetStringSpec =======================================
collection(:Summary, DIM::SetStringSpec) {
  choice('set_str_opt', setstropt, :label => 'Set Str Opt')
  integer('char_size', :label => 'Char Size')
  integer('max_str_len', :label => 'Max Str Len')
  view_ref(:Details)
}

organizer(:Summary, DIM::SetStringSpec) {
  choice('set_str_opt', setstropt, :label => 'Set Str Opt')
  integer('char_size', :label => 'Char Size')
  integer('max_str_len', :label => 'Max Str Len')
  link(:Details)
}

organizer(:Details, DIM::SetStringSpec) {
  choice('set_str_opt', setstropt, :label => 'Set Str Opt')
  integer('char_size', :label => 'Char Size')
  integer('max_str_len', :label => 'Max Str Len')
  
}


# DIM::SetTimeInvoke =======================================
collection(:Summary, DIM::SetTimeInvoke) {
  integer('accuracy', :label => 'Accuracy')
  view_ref(:Details)
}

organizer(:Summary, DIM::SetTimeInvoke) {
  integer('accuracy', :label => 'Accuracy')
  link(:Details)
}

organizer(:Details, DIM::SetTimeInvoke) {
  integer('accuracy', :label => 'Accuracy')
  view_ref(:Summary, 'date_time', :label => 'Date Time')
}


# DIM::SetTimeZoneInvoke ===================================
collection(:Summary, DIM::SetTimeZoneInvoke) {
  view_ref(:Details)
}

organizer(:Summary, DIM::SetTimeZoneInvoke) {
  link(:Details)
}

organizer(:Details, DIM::SetTimeZoneInvoke) {
  view_ref(:Summary, 'next_time_zone', :label => 'Next Time Zone')
  view_ref(:Summary, 'time_zone', :label => 'Time Zone')
}


# DIM::Set_Range_Operation =================================
collection(:Summary, DIM::Set_Range_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Set_Range_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Set_Range_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Summary, 'current_range', :label => 'Current Range')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
  view_ref(:Summary, 'range_op_text', :label => 'Range Op Text')
  view_ref(:Summary, 'set_value_range', :label => 'Set Value Range')
  view_ref(:Summary, 'step_width', :label => 'Step Width')
}


# DIM::Set_String_Operation ================================
collection(:Summary, DIM::Set_String_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('current_string', :label => 'Current String')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Set_String_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('current_string', :label => 'Current String')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Set_String_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('current_string', :label => 'Current String')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
  view_ref(:Summary, 'set_string_spec', :label => 'Set String Spec')
}


# DIM::Set_Value_Operation =================================
collection(:Summary, DIM::Set_Value_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('current_value', :label => 'Current Value')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Set_Value_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('current_value', :label => 'Current Value')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Set_Value_Operation) {
  integer('class_reserved', :label => 'Class')
  integer('current_value', :label => 'Current Value')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('unit_code', :label => 'Unit Code')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
  view_ref(:Summary, 'set_value_range', :label => 'Set Value Range')
  view_ref(:Summary, 'step_width', :label => 'Step Width')
}


# DIM::Simple_MDS ==========================================
collection(:Summary, DIM::Simple_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('battery_level', :label => 'Battery Level')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('altitude', :label => 'Altitude')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::Simple_MDS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('patient_type', patienttype, :label => 'Patient Type')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('power_status', powerstatus, :label => 'Power Status')
  integer('operating_mode', :label => 'Operating Mode')
  integer('name_binding', :label => 'Name Binding')
  integer('handle', :label => 'Handle')
  integer('relative_time', :label => 'Relative Time')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('class_reserved', :label => 'Class')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::Simple_MDS) {
  choice('mds_status', mdsstatus, :label => 'Mds Status')
  choice('application_area', applicationarea, :label => 'Application Area')
  choice('system_capability', systemcapability, :label => 'System Capability')
  choice('power_status', powerstatus, :label => 'Power Status')
  choice('line_frequency', linefrequency, :label => 'Line Frequency')
  choice('patient_type', patienttype, :label => 'Patient Type')
  integer('altitude', :label => 'Altitude')
  integer('association_invoke_id_dim', :label => 'Association Invoke Id Dim')
  integer('battery_level', :label => 'Battery Level')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('relative_time', :label => 'Relative Time')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_mode', :label => 'Operating Mode')
  string('locale', :label => 'Locale')
  string('hires_relative_time', :label => 'Hi Res Relative Time')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('soft_id_dim', :label => 'Soft Id Dim')
  string('bed_label', :label => 'Bed Label')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alert_monitor', :label => 'Alert Monitor')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'battery', :label => 'Battery')
  view_ref(:Summary, 'clock', :label => 'Clock')
  view_ref(:Summary, 'communication_controller', :label => 'Communication Controller')
  view_ref(:Summary, 'date_and_time', :label => 'Date And Time')
  
  
  view_ref(:Summary, 'log', :label => 'Log')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'remaining_battery_time', :label => 'Remaining Battery Time')
  view_ref(:Summary, 'scanner', :label => 'Scanner')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
  view_ref(:Summary, 'vmd', :label => 'V MD')
}


# DIM::SingleCtxtFastScan ==================================
collection(:Summary, DIM::SingleCtxtFastScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::SingleCtxtFastScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  link(:Details)
}

organizer(:Details, DIM::SingleCtxtFastScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  
  view_ref(:Summary, 'scan_info', :label => 'Scan Info')
}


# DIM::SingleCtxtOperScan ==================================
collection(:Summary, DIM::SingleCtxtOperScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::SingleCtxtOperScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  link(:Details)
}

organizer(:Details, DIM::SingleCtxtOperScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  
  view_ref(:Summary, 'scan_info', :label => 'Scan Info')
}


# DIM::SingleCtxtScan ======================================
collection(:Summary, DIM::SingleCtxtScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::SingleCtxtScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  link(:Details)
}

organizer(:Details, DIM::SingleCtxtScan) {
  integer('context_id_dim', :label => 'Context Id Dim')
  
  view_ref(:Summary, 'scan_info', :label => 'Scan Info')
}


# DIM::StepWidthEntry ======================================
collection(:Summary, DIM::StepWidthEntry) {
  integer('step_width', :label => 'Step Width')
  integer('upper_edge', :label => 'Upper Edge')
  view_ref(:Details)
}

organizer(:Summary, DIM::StepWidthEntry) {
  integer('step_width', :label => 'Step Width')
  integer('upper_edge', :label => 'Upper Edge')
  link(:Details)
}

organizer(:Details, DIM::StepWidthEntry) {
  integer('step_width', :label => 'Step Width')
  integer('upper_edge', :label => 'Upper Edge')
  
  
}


# DIM::StringSpec ==========================================
collection(:Summary, DIM::StringSpec) {
  choice('str_flags', stringflags, :label => 'Str Flags')
  integer('str_max_len', :label => 'Str Max Len')
  view_ref(:Details)
}

organizer(:Summary, DIM::StringSpec) {
  choice('str_flags', stringflags, :label => 'Str Flags')
  integer('str_max_len', :label => 'Str Max Len')
  link(:Details)
}

organizer(:Details, DIM::StringSpec) {
  choice('str_flags', stringflags, :label => 'Str Flags')
  integer('str_max_len', :label => 'Str Max Len')
  
}


# DIM::SystemModel =========================================
collection(:Summary, DIM::SystemModel) {
  string('manufacturer', :label => 'Manufacturer')
  string('model_number', :label => 'Model Number')
  view_ref(:Details)
}

organizer(:Summary, DIM::SystemModel) {
  string('manufacturer', :label => 'Manufacturer')
  string('model_number', :label => 'Model Number')
  link(:Details)
}

organizer(:Details, DIM::SystemModel) {
  string('manufacturer', :label => 'Manufacturer')
  string('model_number', :label => 'Model Number')
  
  
}


# DIM::SystemSpecEntry =====================================
collection(:Summary, DIM::SystemSpecEntry) {
  integer('component_capab_id_dim', :label => 'Component Capab Id Dim')
  view_ref(:Details)
}

organizer(:Summary, DIM::SystemSpecEntry) {
  integer('component_capab_id_dim', :label => 'Component Capab Id Dim')
  link(:Details)
}

organizer(:Details, DIM::SystemSpecEntry) {
  integer('component_capab_id_dim', :label => 'Component Capab Id Dim')
  view_ref(:Summary, 'component_spec', :label => 'Component Spec')
  
}


# DIM::TYPE ================================================
collection(:Summary, DIM::TYPE) {
  choice('partition', nompartition, :label => 'Partition')
  integer('code', :label => 'Code')
  view_ref(:Details)
}

organizer(:Summary, DIM::TYPE) {
  choice('partition', nompartition, :label => 'Partition')
  integer('code', :label => 'Code')
  link(:Details)
}

organizer(:Details, DIM::TYPE) {
  choice('partition', nompartition, :label => 'Partition')
  integer('code', :label => 'Code')
}


# DIM::TimeSupport =========================================
collection(:Summary, DIM::TimeSupport) {
  choice('time_capability', timecapability, :label => 'Time Capability')
  integer('relative_accuracy', :label => 'Relative Accuracy')
  integer('relative_resolution', :label => 'Relative Resolution')
  integer('time_protocols', :label => 'Time Protocols')
  view_ref(:Details)
}

organizer(:Summary, DIM::TimeSupport) {
  choice('time_capability', timecapability, :label => 'Time Capability')
  integer('relative_accuracy', :label => 'Relative Accuracy')
  integer('relative_resolution', :label => 'Relative Resolution')
  integer('time_protocols', :label => 'Time Protocols')
  link(:Details)
}

organizer(:Details, DIM::TimeSupport) {
  choice('time_capability', timecapability, :label => 'Time Capability')
  integer('relative_accuracy', :label => 'Relative Accuracy')
  integer('relative_resolution', :label => 'Relative Resolution')
  integer('time_protocols', :label => 'Time Protocols')
  
}


# DIM::Time_Sample_Array ===================================
collection(:Summary, DIM::Time_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('compression', :label => 'Compression')
  integer('sample_period', :label => 'Sample Period')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('handle', :label => 'Handle')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Details)
}

organizer(:Summary, DIM::Time_Sample_Array) {
  choice('metric_status', metricstatus, :label => 'Metric Status')
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  integer('name_binding', :label => 'Name Binding')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('sample_period', :label => 'Sample Period')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  link(:Details)
}

organizer(:Details, DIM::Time_Sample_Array) {
  choice('measurement_status', measurementstatus, :label => 'Measurement Status')
  choice('metric_status', metricstatus, :label => 'Metric Status')
  integer('max_delay_time', :label => 'Max Delay Time')
  integer('measure_mode', :label => 'Measure Mode')
  integer('metric_id_dim', :label => 'Metric Id Dim')
  integer('metric_source_list', :label => 'Metric Source List')
  integer('unit_code', :label => 'Unit Code')
  integer('class_reserved', :label => 'Class')
  integer('sample_period', :label => 'Sample Period')
  integer('compression', :label => 'Compression')
  integer('handle', :label => 'Handle')
  integer('sa_measure_resolution', :label => 'Sa Measure Resolution')
  integer('name_binding', :label => 'Name Binding')
  integer('relative_time_stamp', :label => 'Relative Time Stamp')
  string('metric_info_labelstring', :label => 'Metric Info Label String')
  string('metric_id_partition', :label => 'Metric Id Partition')
  string('label_string', :label => 'Label String')
  string('hires_time_stamp', :label => 'Hi Res Time Stamp')
  string('filter_label_string', :label => 'Filter Label String')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('substance_labelstring', :label => 'Substance Label String')
  string('type', :label => 'Type')
  string('color', :label => 'Color')
  string('unit_labelstring', :label => 'Unit Label String')
  view_ref(:Summary, 'absolute_time_stamp', :label => 'Absolute Time Stamp')
  view_ref(:Summary, 'averaging_period', :label => 'Averaging Period')
  view_ref(:Summary, 'compound_sa_observed_value', :label => 'Compound Sa Observed Value')
  view_ref(:Summary, 'filter_specification', :label => 'Filter Specification')
  
  
  view_ref(:Summary, 'measure_period', :label => 'Measure Period')
  view_ref(:Summary, 'metric_calibration', :label => 'Metric Calibration')
  view_ref(:Summary, 'metric_id_ext', :label => 'Metric Id Ext')
  view_ref(:Summary, 'metric_specification', :label => 'Metric Specification')
  view_ref(:Summary, 'pm_segment', :label => 'P M Segment')
  view_ref(:Summary, 'sa_observed_value', :label => 'Sa Observed Value')
  view_ref(:Summary, 'sa_signal_frequency', :label => 'Sa Signal Frequency')
  view_ref(:Summary, 'sa_specification', :label => 'Sa Specification')
  view_ref(:Summary, 'start_time', :label => 'Start Time')
  view_ref(:Summary, 'stop_time', :label => 'Stop Time')
  view_ref(:Summary, 'substance', :label => 'Substance')
  view_ref(:Summary, 'sweep_speed', :label => 'Sweep Speed')
  view_ref(:Summary, 'tsa_marker_list', :label => 'Tsa Marker List')
  view_ref(:Summary, 'vmo_source_list', :label => 'Vmo Source List')
}


# DIM::ToggleLabelStrings ==================================
collection(:Summary, DIM::ToggleLabelStrings) {
  string('lbl_state0', :label => 'Lbl State0')
  string('lbl_state1', :label => 'Lbl State1')
  view_ref(:Details)
}

organizer(:Summary, DIM::ToggleLabelStrings) {
  string('lbl_state0', :label => 'Lbl State0')
  string('lbl_state1', :label => 'Lbl State1')
  link(:Details)
}

organizer(:Details, DIM::ToggleLabelStrings) {
  string('lbl_state0', :label => 'Lbl State0')
  string('lbl_state1', :label => 'Lbl State1')
  
}


# DIM::Toggle_Flag_Operation ===============================
collection(:Summary, DIM::Toggle_Flag_Operation) {
  choice('toggle_state', togglestate, :label => 'Toggle State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::Toggle_Flag_Operation) {
  choice('toggle_state', togglestate, :label => 'Toggle State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::Toggle_Flag_Operation) {
  choice('toggle_state', togglestate, :label => 'Toggle State')
  integer('class_reserved', :label => 'Class')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('vmo_reference', :label => 'Vmo Reference')
  string('operational_state', :label => 'Operational State')
  
  view_ref(:Summary, 'operation_spec', :label => 'Operation Spec')
  view_ref(:Summary, 'operation_text_strings', :label => 'Operation Text Strings')
  view_ref(:Summary, 'operation_text_strings_dyn', :label => 'Operation Text Strings Dyn')
  view_ref(:Summary, 'toggle_label_strings', :label => 'Toggle Label Strings')
}


# DIM::Top =================================================
collection(:Summary, DIM::Top) {
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  view_ref(:Details)
}

organizer(:Summary, DIM::Top) {
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
  link(:Details)
}

organizer(:Details, DIM::Top) {
  integer('class_reserved', :label => 'Class')
  integer('name_binding', :label => 'Name Binding')
}


# DIM::UTCTimeZone =========================================
collection(:Summary, DIM::UTCTimeZone) {
  integer('time_zone_offset_hours', :label => 'Time Zone Offset Hours')
  integer('time_zone_offset_minutes', :label => 'Time Zone Offset Minutes')
  string('time_zone_label', :label => 'Time Zone Label')
  view_ref(:Details)
}

organizer(:Summary, DIM::UTCTimeZone) {
  integer('time_zone_offset_hours', :label => 'Time Zone Offset Hours')
  integer('time_zone_offset_minutes', :label => 'Time Zone Offset Minutes')
  string('time_zone_label', :label => 'Time Zone Label')
  link(:Details)
}

organizer(:Details, DIM::UTCTimeZone) {
  integer('time_zone_offset_hours', :label => 'Time Zone Offset Hours')
  integer('time_zone_offset_minutes', :label => 'Time Zone Offset Minutes')
  string('time_zone_label', :label => 'Time Zone Label')
  
  
  
}


# DIM::UcfgScanner =========================================
collection(:Summary, DIM::UcfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  view_ref(:Details)
}

organizer(:Summary, DIM::UcfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  link(:Details)
}

organizer(:Details, DIM::UcfgScanner) {
  choice('confirm_mode', confirmmode, :label => 'Confirm Mode')
  integer('class_reserved', :label => 'Class')
  integer('confirm_timeout', :label => 'Confirm Timeout')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('transmit_window', :label => 'Transmit Window')
  string('operational_state', :label => 'Operational State')
  
}


# Gui_Builder_Profile::User ================================
collection(:Summary, Gui_Builder_Profile::User) {
  boolean('use_accessibility', :label => 'Use Accessibility')
  string('login', :label => 'Login')
  string('password_hash', :label => 'Password Hash')
  string('salt', :label => 'Salt')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::User) {
  boolean('use_accessibility', :label => 'Use Accessibility')
  string('login', :label => 'Login')
  string('password_hash', :label => 'Password Hash')
  string('salt', :label => 'Salt')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::User) {
  boolean('use_accessibility', :label => 'Use Accessibility')
  string('login', :label => 'Login')
  string('password_hash', :label => 'Password Hash')
  string('salt', :label => 'Salt')
  view_ref(:Summary, 'roles', :label => 'Roles')
}


# Gui_Builder_Profile::UserRole ============================
collection(:Summary, Gui_Builder_Profile::UserRole) {
  string('name', :label => 'Name')
  view_ref(:Details)
}

organizer(:Summary, Gui_Builder_Profile::UserRole) {
  string('name', :label => 'Name')
  link(:Details)
}

organizer(:Details, Gui_Builder_Profile::UserRole) {
  string('name', :label => 'Name')
  view_ref(:Summary, 'substitutions', :label => 'Substitutions')
  view_ref(:Summary, 'users', :label => 'User')
}


# DIM::VMD =================================================
collection(:Summary, DIM::VMD) {
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  choice('vmd_status', vmdstatus, :label => 'V MD Status')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_hours', :label => 'Operating Hours')
  integer('operation_cycles', :label => 'Operation Cycles')
  integer('parameter_group', :label => 'Parameter Group')
  integer('position', :label => 'Position')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('locale', :label => 'Locale')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::VMD) {
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  choice('vmd_status', vmdstatus, :label => 'V MD Status')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_hours', :label => 'Operating Hours')
  integer('operation_cycles', :label => 'Operation Cycles')
  integer('parameter_group', :label => 'Parameter Group')
  integer('position', :label => 'Position')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('locale', :label => 'Locale')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::VMD) {
  choice('measurement_principle', msmtprinciple, :label => 'Measurement Principle')
  choice('vmd_status', vmdstatus, :label => 'V MD Status')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('instance_number', :label => 'Instance Number')
  integer('name_binding', :label => 'Name Binding')
  integer('operating_hours', :label => 'Operating Hours')
  integer('operation_cycles', :label => 'Operation Cycles')
  integer('parameter_group', :label => 'Parameter Group')
  integer('position', :label => 'Position')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('locale', :label => 'Locale')
  string('type', :label => 'Type')
  view_ref(:Summary, 'alert', :label => 'Alert')
  view_ref(:Summary, 'alertstatus', :label => 'Alert Status')
  view_ref(:Summary, 'channel', :label => 'Channel')
  
  view_ref(:Summary, 'pm_store', :label => 'P M Store')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'service_and_control', :label => 'Service And Control')
  view_ref(:Summary, 'vmd_model', :label => 'V MD Model')
}


# DIM::VMO =================================================
collection(:Summary, DIM::VMO) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::VMO) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
  link(:Details)
}

organizer(:Details, DIM::VMO) {
  integer('class_reserved', :label => 'Class')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('label_string', :label => 'Label String')
  string('type', :label => 'Type')
}


# DIM::VMS =================================================
collection(:Summary, DIM::VMS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::VMS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  link(:Details)
}

organizer(:Details, DIM::VMS) {
  choice('system_capability', systemcapability, :label => 'System Capability')
  integer('class_reserved', :label => 'Class')
  integer('compatibility_id_dim', :label => 'Compatibility Id Dim')
  integer('handle', :label => 'Handle')
  integer('name_binding', :label => 'Name Binding')
  string('ext_obj_relations', :label => 'Ext Obj Relations')
  string('system_id_dim', :label => 'System Id Dim')
  string('system_type', :label => 'System Type')
  view_ref(:Summary, 'nomenclature_version', :label => 'Nomenclature Version')
  view_ref(:Summary, 'production_specification', :label => 'Production Specification')
  view_ref(:Summary, 'system_model', :label => 'System Model')
  view_ref(:Summary, 'system_specification', :label => 'System Specification')
}


# DIM::VmoSourceEntry ======================================
collection(:Summary, DIM::VmoSourceEntry) {
  integer('vmo_type', :label => 'Vmo Type')
  view_ref(:Details)
}

organizer(:Summary, DIM::VmoSourceEntry) {
  integer('vmo_type', :label => 'Vmo Type')
  link(:Details)
}

organizer(:Details, DIM::VmoSourceEntry) {
  integer('vmo_type', :label => 'Vmo Type')
  view_ref(:Summary, 'glb_handle', :label => 'Glb Handle')
  
}


# Details ======================================================================
organizer(:Details, Home) {
  view_ref(:Summary, 'anies', :label => 'ANY')
  view_ref(:Summary, 'any_complices', :label => 'ANY COMPLEX')
  view_ref(:Summary, 'composite_multiple_bed_mds', :label => 'Composite Multiple Bed MDS')
  view_ref(:Summary, 'composite_single_bed_mds', :label => 'Composite Single Bed MDS')
  view_ref(:Summary, 'hydra_mds', :label => 'Hydra MDS')
  view_ref(:Summary, 'multipatient_archives', :label => 'Multipatient Archive')
  view_ref(:Summary, 'simple_mds', :label => 'Simple MDS')
  view_ref(:Summary, 'tops', :label => 'Top')
}


# AltRoots =====================================================================
organizer(:AltRoots, Home) {
}


defaults :Details, 'GuiBuilder::Home.new', :Markdown


# Creation/Deletion Semantics (Compositions) ===================================
composition Home, 'anies'
composition Home, 'any_complices'
composition Home, 'composite_multiple_bed_mds'
composition Home, 'composite_single_bed_mds'
composition Home, 'hydra_mds'
composition Home, 'multipatient_archives'
composition Home, 'simple_mds'
composition Home, 'tops'
composition DIM::Alert, 'alert_condition'
composition DIM::Alert_Status, 'alert_capab_list'
composition DIM::BCC, 'device_interface'
composition DIM::CfgScanner, 'scan_list'
composition DIM::Channel, 'alert'
composition DIM::Channel, 'metric'
composition DIM::Channel, 'pm_store'
composition DIM::Clock, 'time_support'
composition DIM::CmplxDynAttr, 'cm_dyn_elem_list'
composition DIM::CmplxMetricInfo, 'cm_elem_info_list'
composition DIM::CmplxObsValue, 'cm_obs_elem_list'
composition DIM::CmplxStaticAttr, 'cm_static_elem_list'
composition DIM::Complex_Metric, 'cmplx_dyn_attr'
composition DIM::Complex_Metric, 'cmplx_metric_info'
composition DIM::Complex_Metric, 'cmplx_observed_value'
composition DIM::Complex_Metric, 'cmplx_static_attr'
composition DIM::CreateEntry, 'created_object'
composition DIM::DCC, 'device_interface'
composition DIM::DeviceAlertCondition, 'al_stat_chg_cnt'
composition DIM::Device_Interface, 'mib_element'
composition DIM::Distribution_Sample_Array, 'distribution_range_specification'
composition DIM::Distribution_Sample_Array, 'dsa_marker_list'
composition DIM::Event_Log, 'event_log_entry_list'
composition DIM::FastScanReportInfo, 'glb_scan_info'
composition DIM::GetMibDataResult, 'mib_data_list'
composition DIM::Limit_Alert_Operation, 'alert_op_text_string'
composition DIM::Locale, 'str_spec'
composition DIM::MDS, 'alert'
composition DIM::MDS, 'alertstatus'
composition DIM::MDS, 'alert_monitor'
composition DIM::MDS, 'battery'
composition DIM::MDS, 'clock'
composition DIM::MDS, 'communication_controller'
composition DIM::MDS, 'log'
composition DIM::MDS, 'scanner'
composition DIM::MDS, 'service_and_control'
composition DIM::MDS, 'vmd'
composition DIM::Metric, 'metric_calibration'
composition DIM::Metric, 'metric_specification'
composition DIM::Metric, 'vmo_source_list'
composition DIM::MetricSpec, 'structure'
composition DIM::Multipatient_Archive, 'patient_archive'
composition Gui_Builder_Profile::MultivocabularySubstitution, 'role'
composition DIM::Numeric, 'display_resolution'
composition DIM::ObjCreateInfo, 'scan_report_info'
composition DIM::OpAttributeInfo, 'glb_scan_info'
composition DIM::OpCreateInfo, 'scan_info'
composition DIM::OpDeleteEntry, 'deleted_op_list'
composition DIM::OpDeleteInfo, 'deleted_op_list'
composition DIM::OperSpec, 'grouping'
composition DIM::Operation, 'operation_spec'
composition DIM::OperationInvoke, 'op_elem_list'
composition DIM::PM_Store, 'pm_segment'
composition DIM::Patient_Archive, 'session_archive'
composition DIM::Physician, 'authorization_level'
composition DIM::SCO, 'operation'
composition DIM::SaSpec, 'sample_type'
composition DIM::Sample_Array, 'filter_specification'
composition DIM::Sample_Array, 'sa_signal_frequency'
composition DIM::Sample_Array, 'sa_specification'
composition DIM::ScanReportInfo, 'glb_scan_info'
composition DIM::Session_Archive, 'session_notes'
composition DIM::Session_Archive, 'session_test'
composition DIM::Session_Test, 'pm_store'
composition DIM::Set_Range_Operation, 'current_range'
composition DIM::Set_Range_Operation, 'range_op_text'
composition DIM::Set_String_Operation, 'set_string_spec'
composition DIM::SingleCtxtFastScan, 'scan_info'
composition DIM::SingleCtxtOperScan, 'scan_info'
composition DIM::SingleCtxtScan, 'scan_info'
composition DIM::Time_Sample_Array, 'tsa_marker_list'
composition DIM::Toggle_Flag_Operation, 'toggle_label_strings'
composition DIM::VMD, 'alert'
composition DIM::VMD, 'alertstatus'
composition DIM::VMD, 'channel'
composition DIM::VMD, 'pm_store'
composition DIM::VMD, 'service_and_control'
composition DIM::VMS, 'nomenclature_version'
composition DIM::VMS, 'system_specification'
