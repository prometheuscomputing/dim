PAT_INVERSE = /view_ref\(:Summary, 'inverse_[^)]+\)/
PAT_RESERVED = ' RESERVED'

# This backs up the input file, and then overwrites the input file.
def tweak(input_file_path)
  output_file_path = input_file_path + '.bak'
  content = File.read(input_file_path)
  File.open(output_file_path, 'w') {|f| f << content}
  content.gsub!(PAT_INVERSE, '')
  content.gsub!(PAT_RESERVED, '')
  File.open(input_file_path, 'w') {|f| f << content}
end

tweak 'spec.rb'